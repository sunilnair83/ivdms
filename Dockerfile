###########################################################
# Sushumna Application Docker Image
# ---------------------------------------------------------
#
# *** WARNING *** DO NOT MAKE UNNECESARY CHANGES ***
#
# ---------------------------------------------------------
#
# > Inherits the base Odoo 13 Image
# > Adds the following to the image:
#       - Custom Addons
#       - Third Party Addons
#       - PIP dependencies for the addons
#       - Configurations
#       - Environment Variables for Dev/QA/Prod support
###########################################################
# Author:   Indradeep Biswas
# Date:     Nov 15, 2019
###########################################################

FROM odoo:13.0

# Variables
ARG APP_ROOT=/sushumna

# Create a sushumna folder
USER root
RUN mkdir -p ${APP_ROOT}
WORKDIR ${APP_ROOT}

# Copy Isha addons
COPY custom_addons  custom_addons
COPY third_party_addons third_party_addons
# Install apt packages needed for Python module depdenencies
RUN apt update && \
    apt install -y \
    build-essential \
    libpython3.7-dev


# Install all Python module depdenencies recursively
RUN pip3 install wheel;
RUN find ${APP_ROOT}/ -name requirements.txt -exec pip3 install -r {} \;

# Copy the configurations and assets
RUN mkdir -p etc
COPY etc/conf  etc/conf
COPY etc/assets etc/assets

# Copy the scripts to root
COPY etc/assets/scripts/entrypoint.sh .

# Set ownership to all sushumna items to 'odoo' user
RUN chown -R odoo ${APP_ROOT}
USER odoo

# Set Default Configuration Path
ENV ODOO_RC=conf/odoo.conf
ENV SUSHUMNA_CONFIG=conf/sushumna.conf

# Set Custom Entrypoint for the Sushumna Application
ENTRYPOINT ["sh", "-c", "${APP_ROOT}/entrypoint.sh"]