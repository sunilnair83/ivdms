# -*- coding: utf-8 -*-
import logging
from datetime import timedelta
from pprint import pprint

import pytz
import requests

from odoo import models, api, fields
from requests_toolbelt.utils import formdata

_logger = logging.getLogger("VRO SMS API")


# -----------------------------------------------------------------
# VRO Registration: SMS API Model
# -----------------------------------------------------------------
class SMS(models.AbstractModel):
    _name = 'vro.registration.sms.api'
    _description = 'Isha VRO Registration SMS API'

    @api.model
    def send_smcountry_sms(self, data):
        """
        Send SMS

        This method is used to send VRO Registration followup SMS(es) to registrants.
        :param data:
        :return:
        """

        # SMS Log Data
        sms_log_data = {
            'followup_id': data.get('followup_id'),
            'mobile': data.get('number'),
            'message': data.get('message'),
            'send_date': fields.Datetime.now()  # before making api call
        }

        sms_config = self.env['vro.reg.sms.configuration'].default_config()
        if not sms_config:
            _logger.error('No SMS Configuration Found - Could not send SMS to SMS Provider API Server', exc_info=True)
        else:
            url = sms_config.sms_provider_base_url
            params = {
                'User': sms_config.sms_provider_username,
                'passwd': sms_config.sms_provider_password,
                'mobilenumber': data.get('number'),
                'message': data.get('message'),
                'sid': sms_config.sms_provider_sender_id,
                'mtype': 'N',   # Message Type
                'DR': 'Y'   # i.e. Delivery Required = Yes
            }

            response = None

            # Validate Params
            is_valid, error_list = self.__valid_params(url, params)
            if not is_valid:
                sms_log_data.update({
                    'response': '\n'.join(error_list)
                })
            else:
                try:
                    api_data = formdata.urlencode(params)
                    response = requests.post(
                        url,
                        data=api_data.encode('utf-8'),
                        headers={
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                    )
                except Exception as e:
                    _logger.error(f'Exception calling SMS api: {str(e)}', exc_info=True)
                    sms_log_data.update({
                        'response': str(e)
                    })

            # Extract some information
            if response:
                flag_sent = self.__sms_sent_to_server(response.text)

                # Get Job ID, example response text,  OK:515789649
                job_id = None
                if flag_sent and 'ok' in response.text.lower():
                    value_list = response.text.split(':')
                    if len(value_list) == 2:
                        job_id = value_list[1]

                sms_log_data.update({
                    'response': response.text,
                    'flag_sent': flag_sent,
                    'job_id': job_id
                })

        # Create a SMS Log and return it's id
        return self.__log_sms(sms_log_data)

    def __sms_sent_to_server(self, response_text):
        """
        Parse SMS Response and find if a SMS is sent?

        Ref: https://www.smscountry.com/bulk-smsc-api-documentation

        These are the responses when SMS message(s) not sent:
            a) Invalid User Name!!
            b) Your Account not activated. Please contact Webmaster!!
            c) Invalid Password!!
            d) Invalid Data
            e) Insufficient Balance!!! Please Buy SMS Credits
            f) Invalid mobile number(s) given
            g)SMS message(s) not sent
            h)Mobile number has opted not to receive any sms

        On Successful Sent, it will send the following response
            OK:JOB_ID
            For example, OK:515789649
        :param response_text:
        :return:
        """

        response_text = response_text.lower()
        error_texts = ["invalid", "not activated", "insufficient", "not sent", "opted not to receive"]
        if any(found in response_text for found in error_texts):
            return False
        else:
            return True

    def __valid_params(self, url, params):
        is_valid = True
        error_list = []
        if not url:
            is_valid = False
            error_list.append('SMS Config Error: "SMS Provider URL" empty!')

        if not params.get('User'):
            is_valid = False
            error_list.append('SMS Config Error: "SMS Provider User" empty!')

        if not params.get('passwd'):
            is_valid = False
            error_list.append('SMS Config Error: "SMS Provider Password" empty!')

        if not params.get('mobilenumber'):
            is_valid = False
            error_list.append('SMS Config Error: "Mobile Number" empty!')

        if not params.get('message'):
            is_valid = False
            error_list.append('SMS Config Error: "Message" empty!')

        if not params.get('sid'):
            is_valid = False
            error_list.append('SMS Config Error: "SMS Provider Sender ID" empty!')

        return is_valid, error_list

    def __log_sms(self, data):
        return self.env['vro.registration.sms.log'].sudo().create(data)


# -----------------------------------------------------------------
# VRO Registration: SMS Log Model
# -----------------------------------------------------------------
class SMSLog(models.Model):
    _name = 'vro.registration.sms.log'
    _description = 'VRO Registration SMS LOG'
    _order = 'id DESC'

    followup_id = fields.Many2one('vro.reg.followup', string='VRO Followup')
    mobile = fields.Char(string="Mobile", help="SMS Sent to this number")
    message = fields.Char('SMS Message')
    response = fields.Char('SMS Response')
    job_id = fields.Char('SMS Job ID', help="If sent, the server will send OK:job_id")
    send_date = fields.Datetime(string='Sent Date', default=fields.Datetime.now())
    flag_sent = fields.Boolean('Sent to Server?', default=False, help="If a message is sent to server?")

    # Delivery Status
    # Please refer https://www.smscountry.com/bulk-smsc-api-documentation for more details
    delivery_status = fields.Selection([
        ('0', 'Message In Queue'),
        ('1', 'Submitted To Carrier'),
        ('2', 'Un Delivered'),
        ('3', 'Delivered'),
        ('4', 'Expired'),
        ('8', 'Rejected'),
        ('9', 'Message Sent'),
        ('10', 'Opted Out Mobile Number'),
        ('11', 'Invalid Mobile Number'),
    ], 'Delivery Status', default='0')

    def update_delivery_status(self):
        # If the sms has not been sent to server, we do not get delivery status
        if not self.flag_sent:
            _logger.info('SMS Delivery Status - Did not make api call as the sms was not sent to server.')
            return False

        fmt = "%Y-%m-%d %H:%M:%S"
        user_tz = pytz.timezone('Asia/Kolkata')
        from_date = user_tz.localize(self.send_date, fmt).astimezone(pytz.utc)
        dates = {
            'fromdate': from_date.strftime("%d/%m/%Y %H:%M:%S"),
            'todate': from_date.strftime("%d/%m/%Y 23:59:59")   # You must have 23:59:59, can't use timedelta
        }
        _logger.info(f'Query Dates(Localized Dates): {dates}')

        url = 'http://api.smscountry.com/smscwebservices_bulk_reports.aspx?'
        sms_config = self.env['vro.reg.sms.configuration'].default_config()
        if not sms_config:
            _logger.error('No SMS Configuration Found - Could not make api request for Delivery Status', exc_info=True)
        else:
            user = sms_config.sms_provider_username
            passwd = sms_config.sms_provider_password

            if not user or not passwd:
                _logger.info('SMS Config Error: SMS Provider User or Password empty!')
                return False

            params = {
                'User': user,
                'passwd': passwd,
            }
            params.update(dates)

            # Make API Request
            try:
                response = requests.get(url, params)
            except Exception as e:
                _logger.error(str(e), exc_info=True)
                return False

            # error_texts = ["invalid", "not activated", "insufficient", "not sent", "opted not to receive"]
            # if any(found in response.text.lower() for found in error_texts):
            #     raise Exception(f'API Request Error: {response.text}')

            # Success Response will look like:
            #   jobid~mobilenumber~messagestatus~donestamp~message_text~receivestamp#...
            if response.text:
                _logger.info(f'API Response Text:\n{response.text}')
                value_list = response.text.split('#')
                if len(value_list) > 0:
                    for val in value_list:
                        delivery_response = val.split('~')
                        if len(delivery_response) > 2:
                            job_id = delivery_response[0]
                            mobile = delivery_response[1]
                            delivery_status = delivery_response[2]
                            if self.job_id == job_id and self.mobile == mobile:
                                _logger.info(f'Found delivery response for JobID - {job_id}')
                                self.delivery_status = delivery_status

        return True


# -----------------------------------------------------------------
# VRO Registration: SMS Configurations
# -----------------------------------------------------------------
class VroRegSmsConfig(models.Model):
    _name = 'vro.reg.sms.configuration'
    _description = 'VRO Registration SMS Configuration'
    # _rec_name = 'sms_provider_username'

    sms_provider_base_url = fields.Char('SMS Provider API Base URL', size=300, required=True)
    sms_provider_sender_id = fields.Char('SMS Provider Sender ID', size=50, required=True)
    sms_provider_username = fields.Char('SMS Provider Username', size=50, required=True)
    sms_provider_password = fields.Char('SMS Provider Password', size=50, required=True)
    sms_test_number = fields.Char('SMS Test Number', size=50)

    def default_config(self):
        """
        Get the Default configuration
        :return:
        """
        return self.env['vro.reg.sms.configuration'].sudo().search([], order='id desc', limit=1)