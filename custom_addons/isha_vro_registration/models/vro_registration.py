# -*- coding: utf-8 -*-
import copy
import logging
import random
import string
import time
from pprint import pprint

from odoo import models, fields, api, _, exceptions
from odoo.exceptions import ValidationError
from datetime import date, datetime, timedelta

_logger = logging.getLogger("VRO Registration")


# -----------------------------------------------------------------
# VRO Registration: Helper Functions
# -----------------------------------------------------------------
def compute_phone(vals):
    """
    Compute the phone: remove mask value and clean phone number

    :param vals:
    :return: vals
    """
    dirty_phone = vals.get('phone')
    if dirty_phone:
        clean_phone = ''.join([n for n in dirty_phone if n.isdigit()])
        if clean_phone:
            vals['phone'] = clean_phone

    return vals


# -----------------------------------------------------------------
# VRO Registration: Main Model
# -----------------------------------------------------------------


class VroRegistration(models.Model):
    _name = 'vro.registration'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'generic.mixin.track.changes']
    _description = 'VRO Registration'
    _order = 'write_date desc, id desc'
    _rec_name = 'name'

    # Constants
    LOW_MATCH_PAIR_DOMAIN = [('match_level', 'in', ['high_sso', 'low']), ('active', '=', True)]

    #
    # Ishangam profile related fields
    #
    #  We will be inheriting from `isha.volunteer` which inherits `res.partner`
    #  i.e. when delegate=True, It will inherit from that model
    volunteer_id = fields.Many2one(string="Volunteer Profile", comodel_name='isha.volunteer', delegate=True, auto_join=True)
    # ishangam_partner_id is need for sulaba sync, so store = True
    ishangam_partner_id = fields.Many2one('res.partner', related='volunteer_id.partner_id', store=True)
    ishangam_name = fields.Char(related='partner_id.name')
    ishangam_dob = fields.Date(related='partner_id.dob')
    ishangam_gender = fields.Selection(related='partner_id.gender')
    ishangam_email = fields.Char(related='partner_id.email')
    ishangam_email2 = fields.Char(related='partner_id.email2')
    ishangam_email3 = fields.Char(related='partner_id.email3')
    ishangam_marital_status = fields.Selection(related='partner_id.marital_status')
    ishangam_address = fields.Char(related='partner_id.work_state')
    ishangam_nationality_id = fields.Many2one(related='partner_id.nationality_id', string="Nationality")
    ishangam_phone = fields.Char(related='partner_id.phone')
    ishangam_phone_country_code = fields.Char(related='partner_id.phone_country_code')
    ishangam_whatsapp_number = fields.Char(related='partner_id.whatsapp_number')
    ishangam_whatsapp_country_code = fields.Char(related='partner_id.whatsapp_country_code')
    ishangam_deceased = fields.Boolean(related='partner_id.deceased')
    ishangam_portal_verified = fields.Boolean(related='volunteer_id.portal_verified')
    ishangam_is_meditator = fields.Boolean(string='Meditator', related='partner_id.is_meditator', store=True)

    #
    # Is starmarked?
    #
    is_starmarked = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No')],
        compute="check_star_mark_status",
        store=True,
        string="Is Starmarked?")
    starmark_name = fields.Char(related="starmark_category_id.name", readonly=True)
    starmark_color = fields.Char(related="starmark_category_id.sm_color", readonly=True)

    #
    # VRO related additional DB Fields
    #

    # Sequence Field - VRO ID
    vro_id = fields.Char(string='VRO ID', required=True, copy=False, readonly=True, index=True,
                         default=lambda self: _('New'))

    # Visit Details
    expected_arrival_date = fields.Date(string='Expected Arrival Date', track_visibility='onchange', index=True)
    expected_departure_date = fields.Date(string='Expected Departure Date', track_visibility='onchange')
    seva_type = fields.Many2one('vro.reg.seva.type', string='Seva Type', track_visibility='onchange')
    seva_schedule = fields.Many2one('vro.reg.seva.schedule', string='Seva Schedule', track_visibility='onchange')
    registration_type = fields.Many2one('vro.reg.reg.type', string='Registration Type',
                                        track_visibility='onchange')
    #
    # Followups
    #
    followups = fields.One2many('vro.reg.followup', 'vro_registration_id', string="Followups")

    #
    # My Other Registrations
    #
    other_registration_ids = fields.Many2many('vro.registration', relation='vro_registration_other_registration_rel',
                                              column1='current_registration_id', column2='other_registration_id',
                                              compute='_compute_other_registration_ids')

    # SMS Related Information
    flag_sms_sent = fields.Boolean('SMS Sent?', default=False)
    sms_code = fields.Char('SMS Code', help='Latest SMS Confirmation Code if one of the sms has such information.')

    #
    # Registration Stage: Default 'New'
    #
    def _default_stage_id(self):
        # return self.env['vro.reg.reg.stage'].sudo().search([('name', '=', 'New')], limit=1).id
        return self.env.ref('isha_vro_registration.vro_reg_reg_stage_new').id

    stage_id = fields.Many2one('vro.reg.reg.stage', string="Registration Stage",
                               track_visibility='always', default=_default_stage_id)
    stage_name = fields.Char(related='stage_id.name', store=True, index=True)
    status_updated_on = fields.Datetime('Status Updated on', track_visibility='onchange', default=fields.Datetime.now)

    # is vro Draft?
    is_draft = fields.Selection(
        [
            ('N', 'No'),
            ('Y', 'Yes'),
        ], string='Is Draft?', index=True, required=True, default='N',
        help='This is to indicate that the record is created from frontend controller and the '
             'registration process is still incomplete')

    #
    # Comments
    #
    comments = fields.One2many('vro.reg.comment', 'vro_registration_id', string="Comments")

    #
    # Data Updated from Sulaba
    #
    category_id = fields.Many2one('vro.reg.category', string='Volunteer Category')
    check_in_date = fields.Datetime(string='Check-in Date')
    check_out_date = fields.Datetime(string='Check-out Date')

    #
    # Skip CICO API on Sulaba?
    #
    flag_skip_cico_on_sulaba = fields.Boolean('Skip CICO on Sulaba?', default=False)

    #
    # Low Match Pair Implementation
    #
    vro_low_matches_count = fields.Integer(string='Low Match Count', store=True, compute="_compute_low_match_count")

    #
    # Synced to ERP(Sulaba)
    #
    flag_synced_to_sulaba = fields.Boolean('Synced to Sulaba?', default=False, index=True)

    registration_date = fields.Datetime(string='Registration Date')

    @api.constrains('is_draft')
    @api.constrains('is_draft', 'stage_id', 'is_starmarked', 'ishangam_is_meditator')
    def _auto_verify(self):
        """
        Verify the records if it is eligible: Else put them in New Stage
        i.e. When a record is in New stage and Not Draft and Not Starmark and Meditator - We simply put
        it into verfied stage.

        If the record is in draft mode, we put the stage as New

        :return:
        """
        new_stage_id = self.env.ref('isha_vro_registration.vro_reg_reg_stage_new').id
        verified_stage_id = self.env.ref('isha_vro_registration.vro_reg_reg_stage_verified').id
        for rec in self:
            if rec.ishangam_is_meditator \
                    and rec.is_starmarked == 'no' \
                    and rec.is_draft == 'N' \
                    and rec.stage_id.id == new_stage_id:
                data = {
                    'stage_id': verified_stage_id,
                    'status_updated_on': datetime.now()
                }
                rec.update(data)
            # if is_draft state is there, put it to new stage
            elif rec.is_draft == 'Y':
                data = {
                    'stage_id': new_stage_id,
                    'status_updated_on': datetime.now()
                }
                rec.update(data)

    @api.depends('contact_low_match_pair_id_fkey')
    def _compute_low_match_count(self):
        """Count the number of low matches this partner has for Smart Button
        Don't count inactive matches.
        """
        for rec in self:
            rec.vro_low_matches_count = len(
                rec.contact_low_match_pair_id_fkey.filtered_domain(self.LOW_MATCH_PAIR_DOMAIN))

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        """
        Reset original low_matches_count field as we will be using a separate field `vro_low_matches_count`
        :param allfields:
        :param attributes:
        :return:
        """
        res = super(VroRegistration, self).fields_get(allfields, attributes=attributes)
        if res.get('low_matches_count'):
            res['low_matches_count']['searchable'] = False
        return res

    @api.depends('approved_incident_by_partner_ids')
    def check_star_mark_status(self):
        for rec in self.sudo():
            if rec.partner_id.approved_incident_by_partner_ids and len(
                    rec.partner_id.approved_incident_by_partner_ids) > 0:
                rec.is_starmarked = 'yes'
            else:
                rec.is_starmarked = 'no'

    @api.constrains('seva_schedule', 'expected_arrival_date', 'expected_departure_date')
    def _validate_date_and_schedule(self):
        """
        The method will be triggered if one of the following fields are changed in order to validate
        arrival/departure date and seva schedule:
            - expected_arrival_date
            - expected_departure_date
            - seva_schedule

        :return:
        """
        for record in self:
            if record.expected_arrival_date and record.expected_departure_date:
                # Case 1: departure must be greater than arrival
                if record.expected_arrival_date > record.expected_departure_date:
                    raise ValidationError(_("'Departure Date' must be greater than 'Arrival Date'"))

                # Case 2: arrival < seva start date < seva end date <= departure date
                if record.seva_schedule:
                    if record.expected_arrival_date >= record.seva_schedule.start_date:
                        raise ValidationError(_("Arrival Date can not be greater than or equal to Seva Schedule's "
                                                "Start Date"))

                    if record.expected_departure_date < record.seva_schedule.end_date:
                        raise ValidationError(_("Departure Date can not be less than Seva Schedule's End Date"))

    @api.constrains('seva_type', 'expected_arrival_date', 'expected_departure_date')
    def _validate_general_volunteering_covid_case(self):
        """
        In General Volunteering - Seva Type
        Arrival and Departure dates should have a minimum gap of 30 days.

        :return:
        """
        seva_type_id = self.env.ref('isha_vro_registration.vro_reg_seva_type_av').id
        for record in self:
            if record.seva_type and record.expected_arrival_date and record.expected_departure_date:
                delta = record.expected_departure_date - record.expected_arrival_date
                if seva_type_id == record.seva_type.id and delta.days < 30:
                    raise ValidationError(_('In an effort to keep the Isha Yoga Center safe and COVID-free, as of now, '
                                            'the opportunity to volunteer at the ashram is open only for those who can '
                                            'stay here for 30 days or more. For any queries call '
                                            '<a href="tel:+918300083111" class="btn btn-danger btn-sm">'
                                            '<i class="fa fa-phone mr-1"></i>83000 83111</a>'))

    @api.constrains('seva_type', 'seva_schedule')
    def _validate_seva_type_and_schedule(self):
        """
        If you have selected a seva type having seva schedule,
            you must supply one of the seva schedule.

        :return:
        """
        for record in self:
            if record.seva_type:
                if record.seva_type.flag_seva_schedule_required and not record.seva_schedule:
                    raise ValidationError(_("You must select one of 'Seva Schedules'"))

    @api.onchange('seva_type')
    def _onchange_seva_type(self):
        """
        On changing the seva_type, Nullify the seva_schedule
        :return:
        """
        if self.seva_type.id != self.seva_schedule.seva_type_id.id:
            self.seva_schedule = None

    @api.onchange('seva_schedule')
    def _onchange_seva_schedule(self):
        """
        On selecting on a specific seva schedule,
        change arrival and checkout date to start and end date
        :return:
        """
        if self.seva_schedule:
            self.expected_arrival_date = self.seva_schedule.start_date - timedelta(days=1)
            self.expected_departure_date = self.seva_schedule.end_date

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        """
        On selecting partner,
        compute the starmark information and other details
        :return:
        """
        self.check_star_mark_status()
        self.name = self.partner_id.name
        self.dob = self.partner_id.dob
        self.gender = self.partner_id.gender
        self.marital_status = self.partner_id.marital_status
        self.phone_country_code = self.partner_id.phone_country_code
        self.phone = self.partner_id.phone
        self.email = self.partner_id.email
        self.nationality_id = self.partner_id.nationality_id
        self.country_id = self.partner_id.country_id
        self.zip = self.partner_id.zip
        self.state_id = self.partner_id.state_id
        self.state = self.partner_id.state
        self.city = self.partner_id.city
        self.street = self.partner_id.street
        self.street2 = self.partner_id.street2
        self.vro_tag_ids = self.partner_id.vro_tag_ids

        # Photo
        self.image_1920 = self.partner_id.image_1920

        # ID and Address Proofs
        volunteer = self.env['isha.volunteer'].sudo().search([('partner_id', '=', self.partner_id.id)],
                                                             order='id desc', limit=1)
        if volunteer:
            self.id_proof = volunteer.id_proof
            self.id_proof_type = volunteer.id_proof_type
            self.address_proof = volunteer.address_proof
            self.address_proof_type = volunteer.address_proof_type
            self.emergency_contact_ids = volunteer.emergency_contact_ids

    def _compute_other_registration_ids(self):
        # Fix for Followup/Frontdesk User - Do not include New Stage Records
        #   As they wont be able to list a record which has one fo other registrations in
        #   New stage.
        # Issues:
        #   * https://jira.isha.in/browse/VRO-196
        #   * https://jira.isha.in/browse/VRO-199
        #
        # NOTE: Only visit completed will be in "My Other Visits" list.
        visit_completed_id = self.env.ref('isha_vro_registration.vro_reg_reg_stage_visit_completed').id
        for record in self:
            other_registration_ids = self.env['vro.registration'].sudo().search([
                ('partner_id', '=', record.partner_id.id),
                ('stage_id', '=', visit_completed_id),
                ('id', '!=', record.id)])
            record.other_registration_ids = other_registration_ids

    @api.model
    def create(self, vals):
        # Phone Number Clean mask
        vals = compute_phone(vals)

        # Partner
        partner_id = vals.get('partner_id')

        # Get the volunteer ID if already exists, else create one
        volunteer = self.env['isha.volunteer'].sudo().search([('partner_id', '=', partner_id)],
                                                             order='id desc', limit=1)
        if not volunteer:
            volunteer = self.env['isha.volunteer'].sudo().create({'partner_id': partner_id})

        # Update Volunteer in vals
        vals['volunteer_id'] = volunteer.id

        # Add registration date if this is not a draft
        if 'is_draft' in vals and vals.get('is_draft') == 'N':
            vals['registration_date'] = datetime.now()

        # Update Last Txn Date - Fix for CaCa doner only or alike
        vals['last_txn_date'] = date.today()

        # Get the new Sequence for VRO ID
        if vals.get('vro_id', _('New')) == _('New'):
            vals['vro_id'] = self.env['ir.sequence'].next_by_code('vro.registration.sequence') or _('New')

        #
        # !!! IMPORTANT !!!
        # Remove Partner ID - Otherwise it will blanks the phone, email, vro_tags,
        # age, gender, address etc. fields which are inherited ones
        #
        if partner_id:
            vals.pop('partner_id')

        #
        # Default Stage: New or Verified
        #
        stage_data = self.__default_stage_data_on_create(volunteer, vals)
        vals.update(stage_data)

        # Let's Create VRO record
        record = super(VroRegistration, self).create(vals)

        return record

    def __default_stage_data_on_create(self, volunteer, vals):
        """
        Compute the Stage ID during record creation.

        !!! This is important as FrontDesk User and Followup user are not allowed to view record in New Stage.
        So they wont be able to create a record first and call _auto_verify() constraint method hence this is the
        hack.

        :param volunteer:
        :param vals:
        :return:
        """
        data = {}
        if volunteer.partner_id.is_meditator \
                and self.__is_partner_starmarked(volunteer.partner_id) == 'no' \
                and vals.get('is_draft') == 'N':
            data = {
                'stage_id': self.env.ref('isha_vro_registration.vro_reg_reg_stage_verified').id,
                'status_updated_on': datetime.now()
            }
        return data

    def __is_partner_starmarked(self, partner):
        partner_sudo = partner.sudo()
        is_starmarked = 'no'
        if partner_sudo.approved_incident_by_partner_ids and len(
                partner_sudo.approved_incident_by_partner_ids) > 0:
            is_starmarked = 'yes'

        return is_starmarked

    def write(self, vals):
        """
        Overwrite Model write method

        On every update, set sync flag to false

        :param vals:
        :return:
        """
        if not 'flag_synced_to_sulaba' in vals:
            vals.update({
                'flag_synced_to_sulaba': False
            })

        # Add registration date if this is set as not-draft
        if not self.registration_date and 'is_draft' in vals and vals.get('is_draft') == 'N':
            vals['registration_date'] = datetime.now()

        return super(VroRegistration, self).write(vals)

    def __is_verifiable_by_default(self):
        """
        Check if the record is verifiable by default
        i.e. It has to be meditator, without starmark and NOT draft.
        :return:
        """
        return self.ishangam_is_meditator and self.is_starmarked == 'no' and self.is_draft == 'N'

    def action_finalize_draft(self):
        """
        Finalize VRO Registration

        Update is_draft from "No" to "Yes" so that record stage could be changed.

        :return:
        """
        # self.registration_date = datetime.now()
        self.is_draft = 'N'

    def action_verify(self):
        """
        Verify VRO Registration

        This is a manual verification of VRO registration if the corresponding
        meditator is starmarked.

        :return:
        """
        stage_id = self.env.ref('isha_vro_registration.vro_reg_reg_stage_verified').id
        self.update_stage_id(stage_id)

    def action_reject(self):
        """
        Reject VRO Registration

        This is a manual Rejection of VRO registration

        :return:
        """
        stage_id = self.env.ref('isha_vro_registration.vro_reg_reg_stage_rejected').id
        self.update_stage_id(stage_id)

    def action_close(self):
        """
        Close VRO Registration

        This is a manual Close of VRO registration

        :return:
        """
        stage_id = self.env.ref('isha_vro_registration.vro_reg_reg_stage_closed').id
        self.update_stage_id(stage_id)

    def action_cancel(self):
        """
        Cancel VRO Registration

        This is a manual Cancellation of VRO registration

        :return:
        """
        stage_id = self.env.ref('isha_vro_registration.vro_reg_reg_stage_canceled').id
        self.update_stage_id(stage_id)

    def action_reopen(self):
        """
        Re-open VRO Registration

        This is a manual Re-open of VRO registration

        If the record is verifiable by default, it will set as verified else as New

        :return:
        """
        if self.__is_verifiable_by_default():
            self.action_verify()
        else:
            stage_id = self.env.ref('isha_vro_registration.vro_reg_reg_stage_new').id
            self.update_stage_id(stage_id)

    def update_stage_id(self, new_stage_id):
        """
        Update Stage_ID of a Registration

        NOTE: We only update the stage of Non-Draft Record.
        :param new_stage_id: New Stage ID to be updated
        :return:
        """
        if self.is_draft == 'N':
            data = {
                'stage_id': new_stage_id,
                'status_updated_on': datetime.now()
            }
            return self.write(data)

    def action_followup(self):
        """
        Followup VRO Registration

        This method open followup form to make a call and proceed further.
        :return:
        """
        ctx = dict()
        ctx.update({
            'default_vro_registration_id': self.id,
            'default_registration_type': self.registration_type.id,
            'default_expected_arrival_date': self.expected_arrival_date,
            'default_expected_departure_date': self.expected_departure_date,
            'default_seva_type': self.seva_type.id,
            'default_seva_schedule': self.seva_schedule.id,
        })
        return {
            'name': _('Registration Followup'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'vro.reg.followup',
            'view_id': False,
            'context': ctx,
            # 'domain': [],
            'target': 'new'
        }

    def action_comment(self):
        """
        Comment VRO Registration

        This method open comment form to make a call and proceed further.
        :return:
        """
        ctx = dict()
        ctx.update({
            'default_vro_registration_id': self.id,
        })
        return {
            'name': _('Registration Comment'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'vro.reg.comment',
            'view_id': False,
            'context': ctx,
            # 'domain': [],
            'target': 'new'
        }

    def action_low_match_pair(self):
        local_context = dict(
            self.env.context,
            search_default_id2=self.partner_id.id,
            default_id2=self.partner_id.id,
            active_id=self.partner_id.id,
            active_ids=[self.partner_id.id]
        )
        return {
            'name': 'Low Match Pairs',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_id': False,
            'views': [(False, 'tree'), (False, 'form')],
            'res_model': 'low.match.pairs',
            'target': 'current',
            'context': local_context,
            'domain': self.LOW_MATCH_PAIR_DOMAIN
        }

# -----------------------------------------------------------------
# VRO Registration: Registration Types
# -----------------------------------------------------------------
class VroRegRegType(models.Model):
    _name = 'vro.reg.reg.type'
    _description = 'VRO Registration Types'
    _order = 'name asc'
    _sql_constraints = [('name', 'UNIQUE (name)', 'You can not have two types with the same name!')]

    name = fields.Char('Name', required=True)

    #
    # Synced to ERP(Sulaba)
    #
    flag_synced_to_sulaba = fields.Boolean('Synced to Sulaba?', default=False, index=True)


# -----------------------------------------------------------------
# VRO Registration: Questionnaire/Followups
# -----------------------------------------------------------------
class VroRegFollowup(models.Model):
    _name = 'vro.reg.followup'
    _description = 'VRO Registration Questionnaire/Followups'
    _order = 'id desc'

    vro_registration_id = fields.Many2one('vro.registration', string='Volunteer')
    vro_registration_type = fields.Many2one(related='vro_registration_id.registration_type')
    vro_expected_arrival_date = fields.Date(related='vro_registration_id.expected_arrival_date')
    vro_expected_departure_date = fields.Date(related='vro_registration_id.expected_departure_date')
    vro_seva_type = fields.Many2one(related='vro_registration_id.seva_type')
    vro_seva_schedule = fields.Many2one(related='vro_registration_id.seva_schedule')

    q1_coming_on_expected_date = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No'),
        ('cancel', 'Cancel Visit')
    ], string='Are you coming to the Ashram on expected arrival date?')

    #
    # New Visit Information
    #
    expected_arrival_date = fields.Date(string='Expected Arrival Date')
    expected_departure_date = fields.Date(string='Expected Departure Date')
    seva_type = fields.Many2one('vro.reg.seva.type', string='Seva Type')
    seva_schedule = fields.Many2one('vro.reg.seva.schedule', string='Seva Schedule')

    q3_days_to_volunteer = fields.Integer(string='How many days do you plan to volunteer?',
                                          compute='_compute_days_to_volunteer')
    q4_is_somebody_accompanying = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No')
    ], string='Is anyone accompanying you to ashram?')
    q5_accompanying_count = fields.Integer(string='How many people are accompanying you to ashram?')
    q6_is_accompanying_meditator = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No')
    ], string='Have all accompanying volunteers completed their Inner Engineering/Shambavi?')
    q7_is_some_of_accompanying_child = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No')
    ], string='Is there anyone over the age of 7 and under 14?')
    q8_is_accompanying_child_same_gender = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No')
    ], string='Is child same gender as person registering?')
    q9_are_you_coming_for_rituals = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No')
    ], string='Are you coming for rituals?')
    call_status = fields.Selection([('ring_no_action', 'Ringing, No answer'),
                                    ('call_completed', 'Call Completed'),
                                    ('call_later', 'Call Later'),
                                    ('wrong_num', 'Wrong Number'),
                                    ('dnd', 'Don\'t Disturb'),
                                    ('not_interested', 'Not Interested'),
                                    ('not_reachable', 'Not Reachable'),
                                    ('switched_off', 'Swtiched Off')], string='Call Status', required=True)
    followup_action_id = fields.Many2one('vro.reg.followup.action', string='Followup Action', required=True)
    call_remarks = fields.Text(string='Call Remarks')

    # SMS Code if SMS Sent from this followup
    sms_log_ids = fields.One2many('vro.registration.sms.log', 'followup_id', string="SMS Logs")
    flag_sms_sent = fields.Boolean('Sent to Server?', default=False, help="If a message is sent to server?")
    sms_code = fields.Char('SMS Code', help='SMS Code if the associated followup action wants to send an SMS '
                                            'with some Code.')
    sms_message = fields.Text('SMS Message', help='SMS Message if associated followup action want to send an SMS.')
    flag_send_sms = fields.Boolean(related='followup_action_id.flag_send_sms',
                                   help='Does this followup action send an SMS?')

    @api.depends('expected_arrival_date', 'expected_departure_date')
    def _compute_days_to_volunteer(self):
        for record in self:
            delta = record.expected_departure_date - record.expected_arrival_date
            record.q3_days_to_volunteer = delta.days

    @api.constrains('seva_type', 'seva_schedule', 'expected_arrival_date', 'expected_departure_date')
    def _validate_date_and_schedule(self):
        """
        The method will be triggered if one of the following fields are changed in order to validate
        arrival/departure date and seva schedule:
            - expected_arrival_date
            - expected_departure_date
            - seva_schedule
            - seva_type

        :return:
        """
        for record in self:
            if record.expected_arrival_date and record.expected_departure_date:
                # Case 1: departure must be greater than arrival
                if record.expected_arrival_date > record.expected_departure_date:
                    raise ValidationError(_("'Expected Departure Date' must be greater than 'Expected Arrival Date'"))

                # Case 2: arrival < seva start date < seva end date <= departure date
                if record.seva_schedule:
                    if record.expected_arrival_date >= record.seva_schedule.start_date or record.expected_departure_date < record.seva_schedule.end_date:
                        raise ValidationError(_("'Seva Schedule' must be between your 'Arrival' and 'Departure' Dates"))

    @api.constrains('seva_type', 'seva_schedule')
    def _validate_seva_type_and_schedule(self):
        """
        If you have selected a seva type having seva schedule,
            you must supply one of the seva schedule.

        :return:
        """
        for record in self:
            if record.seva_type:
                if record.seva_type.seva_schedule_ids and not record.seva_schedule:
                    raise ValidationError(_("You must supply one of 'Seva schedule'"))

    @api.onchange('seva_type')
    def _onchange_seva_type(self):
        """
        On changing the seva_type, Nullify the seva_schedule
        :return:
        """
        if self.seva_type.id != self.seva_schedule.seva_type_id.id:
            self.seva_schedule = None

    @api.onchange('seva_schedule')
    def _onchange_seva_schedule(self):
        """
        On selecting on a specific seva schedule,
        change arrival and checkout date to start and end date
        :return:
        """
        if self.seva_schedule:
            self.expected_arrival_date = self.seva_schedule.start_date - timedelta(days=1)
            self.expected_departure_date = self.seva_schedule.end_date

    @api.constrains('call_status')
    def _validate_call_status(self):
        for record in self:
            if not record.call_status:
                raise ValidationError(_("'Call Status' field is required."))

    @api.model
    def create(self, vals):
        """
        Create a Followup

        This method also updates VRO registration details based on the data
        passed on from followup form.

        :param vals:
        :return:
        """
        # Create the followup
        record = super(VroRegFollowup, self).create(vals)

        # Update new Visit Information
        # Now we have to load the seva type and seva schedule for that
        reg_data = {}
        if record.q1_coming_on_expected_date == 'no':
            seva_type = vals.get('seva_type')
            seva_schedule = vals.get('seva_schedule') if seva_type else None
            reg_data.update({
                'expected_arrival_date': vals.get('expected_arrival_date'),
                'expected_departure_date': vals.get('expected_departure_date'),
                'seva_type': seva_type,
                'seva_schedule': seva_schedule
            })

        if reg_data:
            record.vro_registration_id.write(reg_data)

        # Update Registration Stage
        if record.followup_action_id.stage_id:
            record.vro_registration_id.update_stage_id(record.followup_action_id.stage_id)

        # Send SMS
        if record.followup_action_id.flag_send_sms:
            record.send_sms()

        return record

    def send_sms(self):
        self.ensure_one()

        if self.followup_action_id.flag_send_sms and self.followup_action_id.sms_template:
            message = self.followup_action_id.sms_template
            code_name = self.followup_action_id.sms_code_name
            sms_code = None

            # Do we need to generate a SMS Confirmation Code?
            if code_name:
                code_length = 10
                sms_code = ''.join(random.choices(string.ascii_uppercase + string.digits, k=code_length))

                # replae code_name by actual sms_code
                message = message.replace(code_name, sms_code)

            # Send Message
            self.__send_sms(message, sms_code)

    def action_resend_sms(self):
        """
        Resend SMS

        Resend same sms message if failed before or participant has not received.
        :return:
        """
        message = self.sms_message
        sms_code = self.sms_code

        if message:
            self.__send_sms(message, sms_code)

    def __send_sms(self, message, sms_code):
        # Send SMS
        receiver = self.__get_receiver_mobile()
        sms_log = self.env['vro.registration.sms.api'].send_smcountry_sms({
            'number': receiver,
            'message': message,
            'followup_id': self.id
        })

        # Update SMS info on Registration
        self.vro_registration_id.write({
            'flag_sms_sent': sms_log.flag_sent,
            'sms_code': sms_code
        })

        # Update SMS info on Followup
        self.write({
            'flag_sms_sent': sms_log.flag_sent,
            'sms_code': sms_code,
            'sms_message': message
        })

    def __get_receiver_mobile(self):
        """
        Get SMS Receiver Number

        If sms_test_number is present, it will return this else actual registrant number
        :return:
        """
        sms_config = self.env['vro.reg.sms.configuration'].default_config()
        sms_test_number = sms_config.sms_test_number
        if sms_test_number:
            return sms_test_number

        phone_country_code = self.vro_registration_id.phone_country_code if self.vro_registration_id.phone_country_code else ''
        phone = self.vro_registration_id.phone if self.vro_registration_id.phone else ''
        registrant_phone = (str(phone_country_code) + str(phone)).strip()
        return registrant_phone

    def action_update_sms_delivery_status(self):
        """
        Update SMS Delivery Status for all the attempted sms for this followup
        :return:
        """
        if self.sms_log_ids:
            total = len(self.sms_log_ids)
            counter = 0
            for sms_log in self.sms_log_ids:
                sms_log.update_delivery_status()
                counter += 1
                if counter < total:
                    # wait for one second before making next api call
                    _logger.info('Waiting for one second before making another api call')
                    time.sleep(1)


# -----------------------------------------------------------------
# VRO Registration: Seva Types
# -----------------------------------------------------------------
class VroRegSevaType(models.Model):
    _name = 'vro.reg.seva.type'
    _description = 'VRO Registration Seva Types'
    _order = 'name asc'
    _sql_constraints = [('name', 'UNIQUE (name)', 'You can not have two types with the same name!')]

    name = fields.Char('Name', required=True)
    description = fields.Text('Description')
    flag_dept_approval_required = fields.Boolean('Dept Approval Required?', default=False,
                                                 help='Some seva types required department approval. '
                                                      'For example, Devi Seva, Linga Seva')
    tag_validity_period = fields.Integer('Tag Validity Period (days)', required=True,
                                         help='Wristband Tag Validity Period')

    # Seva Schedules
    seva_schedule_ids = fields.One2many('vro.reg.seva.schedule', 'seva_type_id', string="Seva Schedules")
    flag_seva_schedule_required = fields.Boolean('Seva Schedule Required?', default=False,
                                                 help='Is seva schedule required/compulsory '
                                                      'if this seva type is selected while registering?'
                                                      'For example, Devi Seva, Linga Seva')
    #
    # Synced to ERP(Sulaba)
    #
    flag_synced_to_sulaba = fields.Boolean('Synced to Sulaba?', default=False, index=True)

    active = fields.Boolean('Active')

    def write(self, vals):
        """
        Overwrite Model write method

        On every update, set sync flag to false

        :param vals:
        :return:
        """
        if not 'flag_synced_to_sulaba' in vals:
            vals.update({
                'flag_synced_to_sulaba': False
            })

        return super(VroRegSevaType, self).write(vals)

    def action_add_schedule(self):
        """
        Add seva schedules

        This method open seve schedule form for this seva type.
        :return:
        """
        ctx = dict()
        ctx.update({
            'default_seva_type_id': self.id,
        })
        return {
            'name': _('Seva Schedule'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'vro.reg.seva.schedule',
            'view_id': False,
            'context': ctx,
            'target': 'new'
        }


# -----------------------------------------------------------------
# VRO Registration: Seva Schedules
# -----------------------------------------------------------------
class VroRegSevaSchedules(models.Model):
    _name = 'vro.reg.seva.schedule'
    _description = 'VRO Registration Seva Schedules'
    _order = 'start_date desc'
    _sql_constraints = [('schedule_unique', 'UNIQUE (seva_type_id, start_date)', 'This schedule already exists.')]
    _rec_name = 'start_date'

    # Relation with Seva Type
    seva_type_id = fields.Many2one('vro.reg.seva.type', string="Seva Type", required=True)

    # Schedule
    start_date = fields.Date(string='Start Date', required=True, index=True)
    end_date = fields.Date(string='End Date', required=True, index=True)
    notes = fields.Text(string='Notes')

    #
    # Synced to ERP(Sulaba)
    #
    flag_synced_to_sulaba = fields.Boolean('Synced to Sulaba?', default=False, index=True)

    @api.constrains('end_date')
    def _validate_end_date(self):
        for record in self:
            if record.start_date and record.end_date:
                if record.start_date > record.end_date:
                    raise ValidationError(_("'Start Date' can not be greater than 'End Date'."))

    def name_get(self):
        """
        ORM Overwrite Method

        Get the name with start and end date formatted.
        This will be available in Many2one field dropdown or wherever displayed

        For example: 14 Jun 2020 - 24 Jun 2020
        :return: fromatted name
        """
        res = []
        for rec in self:
            text = rec.get_formatted_name()
            res.append((rec.id, text))
        return res

    def get_formatted_name(self):
        st_dt = self.start_date.strftime('%Y %b %d')
        ed_dt = self.end_date.strftime('%Y %b %d')
        text = f'{st_dt} - {ed_dt}'
        return text


# -----------------------------------------------------------------
# VRO Registration: Registration Status Types
# -----------------------------------------------------------------
class VroRegRegStage(models.Model):
    _name = 'vro.reg.reg.stage'
    _description = 'VRO Registration Stages'
    _order = 'display_order asc'
    _sql_constraints = [('name', 'UNIQUE (name)', 'You can not have two types with the same name!')]

    name = fields.Char('Name', required=True)
    display_order = fields.Integer('Display Order')


# -----------------------------------------------------------------
# VRO Registration: Followup Status Types
# -----------------------------------------------------------------
class VroRegFollowupAction(models.Model):
    _name = 'vro.reg.followup.action'
    _description = 'VRO Registration Followup Actions'
    _order = 'name asc'
    _sql_constraints = [('name', 'UNIQUE (name)', 'You can not have two types with the same name!')]

    name = fields.Char('Name', required=True)
    sms_template = fields.Text('SMS Template')
    sms_code_name = fields.Char('SMS Code Name',
                                help='If sms template needs to send some confirmation code, '
                                     'mention that code name inside your sms template.\n\n'
                                     'For example, \n'
                                     'If Code Name is CONFIRMATION_CODE\n'
                                     'Please mention in your SMS Template as\n'
                                     'Namaskaram,...Your code is CONFIRMATION_CODE ... some other message')
    flag_send_sms = fields.Boolean('Send SMS?', required=True,
                                   help='Flag to check whether to send SMS upon having a registration assigned this '
                                        'action')
    # Relation with Registration Stage Type
    stage_id = fields.Many2one('vro.reg.reg.stage', string="Registration Stage")

    @api.constrains('flag_send_sms', 'sms_template')
    def _validate_flag_send_sms(self):
        if self.flag_send_sms and not self.sms_template:
            raise ValidationError('"SMS Template" field is required.')

    def name_get(self):
        """
        ORM Overwrite Method

        Get the name with sms info.
        This will be available in Many2one field dropdown or wherever displayed

        For example: Approve Application - Send SMS
        :return: fromatted name
        """
        res = []
        for field in self:
            sms_info = 'with SMS' if field.flag_send_sms else ''
            if sms_info:
                text = f'{field.name} {sms_info}'
            else:
                text = field.name
            res.append((field.id, text))
        return res


# -----------------------------------------------------------------
# VRO Registration: Comments/Remarks
# -----------------------------------------------------------------
class VroRegComments(models.Model):
    _name = 'vro.reg.comment'
    _description = 'VRO Registration Comments/Remarks'
    _order = 'id desc'

    vro_registration_id = fields.Many2one('vro.registration', string='Volunteer')
    name = fields.Text('Comments', required=True)


# -----------------------------------------------------------------
# VRO Registration: Category
# -----------------------------------------------------------------
class VroRegCategory(models.Model):
    _name = 'vro.reg.category'
    _description = 'VRO Registration Category'
    _order = 'name asc'
    _sql_constraints = [('name', 'UNIQUE (name)', 'You can not have two categories with the same name!')]

    name = fields.Char('Name', required=True)

    #
    # Synced to ERP(Sulaba)
    #
    flag_synced_to_sulaba = fields.Boolean('Synced to Sulaba?', default=False, index=True)


# -----------------------------------------------------------------
# VRO Registration: Tags
# -----------------------------------------------------------------
class VroRegTag(models.Model):
    _name = 'vro.tag'
    _description = 'VRO Tags'
    _order = 'name asc'
    _sql_constraints = [('name', 'UNIQUE (name)', 'You can not have two tags with the same name!')]

    name = fields.Char('Name', required=True)

    #
    # Synced to ERP(Sulaba)
    #
    flag_synced_to_sulaba = fields.Boolean('Synced to Sulaba?', default=False, index=True)


# -----------------------------------------------------------------
# Res Partner: Add VRO Tags on Res Partner
# -----------------------------------------------------------------
class ResPartner(models.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    #
    # VRO Tags
    #
    vro_tag_ids = fields.Many2many('vro.tag', string='VRO Tags')