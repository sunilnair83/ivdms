# -*- coding: utf-8 -*-
import logging

from ..isha_importer import Configuration, replaceEmptyString, sso_get_full_data

_logger = logging.getLogger(__name__)


def get_string_value(field, form_fields):
    str_value = form_fields.get(field)
    # remove spaces from start and end of string
    if str_value:
        str_value = str_value.strip()
    else:
        str_value = ''
    return str_value


def get_numeric_value(field, form_fields, positive_only=True):
    # return int(form_fields.get(field)) if form_fields.get(field) is not None else 0
    value = form_fields.get(field)
    if value:
        value = value.strip()

    if not value:
        value = 0

    # Convert to Integer
    value = int(value)

    # Positive only
    if positive_only:
        value = abs(value)

    return value


def get_fkey_id_value(field, form_fields):
    return int(form_fields.get(field)) if form_fields.get(field) is not None else None


def get_boolean_value(field, form_fields):
    if get_string_value(field, form_fields) is not None:
        return get_string_value(field, form_fields) == 'yes'


def render_alert_template(request, options):
    values = {
        'title': options.get('title'),
        'message': options.get('message'),
        'dismissable': options.get('dismissable'),  # dismissable: [True | False]

        # alert_type: [alert-danger | alert-warning | alert-info | alert-success | alert-primary]
        'alert_type': options.get('alert_type'),
    }
    return request.render("isha_vro_registration.vro_generic_alert_template", values)


def get_vro_record(request):
    # Let's find the partner record first
    p_id = request.env.user.partner_id.id
    volunteer = request.env['isha.volunteer'].sudo().search([('partner_id', '=', p_id)], limit=1)

    # Create one if no volunteer is found
    if not volunteer:
        volunteer = request.env['isha.volunteer'].sudo().create({
            'partner_id': p_id
        })

    # Get the VRO record with is_draft = True, if not found, create one
    stage_id = request.env.ref('isha_vro_registration.vro_reg_reg_stage_new').id
    vro = request.env['vro.registration'].sudo().search([
        ('volunteer_id', '=', volunteer.id), ('stage_id', '=', stage_id), ('is_draft', '=', 'Y')], order='id desc', limit=1)

    if not vro:
        vro = request.env['vro.registration'].sudo().create({
            'partner_id': p_id,
            'is_draft': 'Y'
        })

    return vro
