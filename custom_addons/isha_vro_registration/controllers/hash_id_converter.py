# -*- coding: utf-8 -*-
import logging
import hashids
from ..isha_importer import Configuration

#
# Reference:
#   https://dev.to/ndrbrt/obfuscate-django-models-ids-by-encoding-them-as-non-sequential-non-predictable-strings-4pa
#
_logger = logging.getLogger(__name__)


HASHIDS_PARAMS = {
    # SECURITY WARNING: keep the salt used in production secret!
    'salt': 'my_long_salt_string',
    'min_length': 12
}

def get_params():
    # Get from Configuration
    try:
        hash_config = Configuration('ISHA_VRO')
    except Exception as e:
        hash_config = {}
        # raise ValueError('Hash ID Configuration not found.')
        _logger.error(f'Hash ID Configuration not found - {str(e)}', exc_info=True)

    salt = hash_config.get('ISHA_VRO_HASH_ID_SALT')
    min_length = hash_config.get('ISHA_VRO_HASH_ID_MIN_LENGTH')
    res = {}
    if salt:
        res['salt'] = salt
    if min_length:
        res['min_length'] = min_length

    return res


def get_regex(params):
    min_length = params.get('min_length')
    if min_length is not None:
        return f'[0-9a-zA-Z]{{{min_length},}}'
    return '[0-9a-zA-Z]+'


PARAMS = get_params()
MY_HASHIDS = hashids.Hashids(**PARAMS)


def encode_id(_id: int) -> str:
    return MY_HASHIDS.encode(_id)


def decode_id(_id: str) -> int:
    decoded_values = MY_HASHIDS.decode(_id)
    # output of hashids.decode is always a tuple
    if len(decoded_values) != 1:
        raise ValueError('Invalid reference.')
    return decoded_values[0]


class HashidsConverter:

    def decode(self, value: str) -> int:
        return decode_id(value)

    def encode(self, value: int) -> str:
        return encode_id(value)
