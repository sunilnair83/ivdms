# -*- coding: utf-8 -*-
import logging
import re

from datetime import datetime, timedelta
from odoo import http, exceptions
from odoo.exceptions import ValidationError
from odoo.http import request, Response
from pprint import pprint
from dateutil.relativedelta import relativedelta
from .utils import *
from .hash_id_converter import HashidsConverter

_logger = logging.getLogger(__name__)
_save_logger = logging.getLogger('VRO_SAVE_LOGGER')

def is_valid_email(email):
    """
    Is Valid Email?
    Checks if given string is a valid email address.

    :param email: string
    :return: boolean
    """
    regex = '^(\w|\.|\_|\-)+[@](\w|\_|\-|\.)+[.]\w{2,3}$'
    return re.search(regex, email)


class VroRegistration(http.Controller):

    @http.route([
        '/vro-registration/',
        '/vro-registration/<string:seva_type>',
        '/vro-registration/edit/<string:hashed_id>'
    ], type='http', auth='public', website=True)
    def register(self, seva_type=None, hashed_id=None, **kw):
        """
        Register or Edit VRO Registration for Current SSO user

        :param seva_type: Seva Type [LingaSeva|DeviSeva]
        :param hashed_id: Hashed Id to edit record
        :param kw:
        :return:
        """
        try:
            # Auto redirect Public users to SSO Login page
            if request.env.user.has_group('base.group_public'):
                return request.render('isha_crm.sso_auto_redirect', {})

            # Authorized user???  Validating if user is a meditator
            # if not request.env.user.partner_id.is_meditator:
            #     return self.__render_not_meditator()

            form_save_url = '/vro-registration/save/'

            # Get Hashed ID
            if hashed_id:
                try:
                    id_hasher = HashidsConverter()
                    my_id = id_hasher.decode(hashed_id)
                    vro_record = self.__editable_vro_record(my_id)
                    form_save_url = f'/vro-registration/save/{hashed_id}'
                except Exception as e:
                    return render_alert_template(request, {
                        'title': 'Oops!',
                        'message': str(e),
                        'alert_type': 'alert-warning'
                    })
            else:
                # Get the VRO Registration Record for this Meditator (either draft or new one)
                vro_record = get_vro_record(request)
                vro_record.registration_type = request.env.ref('isha_vro_registration.vro_reg_reg_type_online').id

            values = self._get_form_selection_values(vro_record)
            values.update({
                'vro_record': vro_record,
                'form_save_url': form_save_url
            })

            # SevaTypes Supplied in URL?
            selected_seva_type = None
            if seva_type and seva_type.lower() in ['lingaseva', 'deviseva']:
                if seva_type.lower() == 'deviseva':
                    selected_seva_type = request.env.ref('isha_vro_registration.vro_reg_seva_type_devi_seva').id
                else:
                    selected_seva_type = request.env.ref('isha_vro_registration.vro_reg_seva_type_linga_seva').id
            elif vro_record.seva_type:
                selected_seva_type = vro_record.seva_type.id

            # Note: Cant do vro_record.seva_type = selected_seva_type --> will throw validation constraint
            values.update({
                'selected_seva_type': selected_seva_type
            })

            # Update seva schedule options
            if selected_seva_type:
                values.update({
                    'seva_schedule_options': self.__seva_schedule_list(selected_seva_type),
                })

            # Emergency Contacts
            for record in vro_record.emergency_contact_ids:
                if record.emergency_type == 'primary':
                    values.update({
                        'primary_emergency_contact': record
                    })
                if record.emergency_type == 'secondary':
                    values.update({
                        'secondary_emergency_contact': record
                    })

            return request.render('isha_vro_registration.isha_vro_registration_form_template', values)
        except Exception as e:
            _logger.error("VRO registration exception", exc_info=True)
            return render_alert_template(request, {
                'title': 'Oops!',
                'message': f'Sorry something went wrong. Please try again. - {str(e)}'
            })

    @http.route([
        '/vro-registration/save/',
        '/vro-registration/save/<string:hashed_id>',
    ], type='json', auth="user", website=True)
    def save_form(self, hashed_id=None, **post):
        start_time = datetime.now()
        _save_logger.info(f'SAVE START - {start_time}')

        # Authorized user???  Validating if user is a meditator
        # if not request.env.user.partner_id.is_meditator:
        #     return self.__render_not_meditator(is_json=True)

        # Get Encoded ID
        if hashed_id:
            try:
                id_hasher = HashidsConverter()
                my_id = id_hasher.decode(hashed_id)
                vro_record = self.__editable_vro_record(my_id)
            except Exception as e:
                raise exceptions.UserError(str(e))
        else:
            # Get the VRO Registration Record for this Meditator (either draft or new one)
            vro_record = get_vro_record(request)

        if vro_record.id:
            self.__save(post, vro_record)
        else:
            raise exceptions.UserError('Something went wrong. Please contact administrator.')

        end_time = datetime.now()
        _save_logger.info(f'SAVE END - {end_time}')

        delta = end_time - start_time
        _save_logger.info(f'TOTAL SAVE TIME: {delta.seconds}')

    @http.route('/vro-registration/cancel/<string:hashed_id>', type='json', auth="user", website=True)
    def cancel(self, hashed_id=None, **post):
        # Authorized user???  Validating if user is a meditator
        # if not request.env.user.partner_id.is_meditator:
        #     return self.__render_not_meditator(is_json=True)

        try:
            id_hasher = HashidsConverter()
            my_id = id_hasher.decode(hashed_id)
            vro_record = self.__cancelable_vro_record(my_id)
        except Exception as e:
            raise exceptions.UserError(str(e))

        done = vro_record.sudo().write({
            'stage_id': request.env.ref('isha_vro_registration.vro_reg_reg_stage_canceled').id,
            'status_updated_on': datetime.now()
        })
        if not done:
            raise exceptions.UserError('Oops! Some problem occurred during cancellation. Please contact administrator.')

    @http.route('/vro-registration/seva-schedules-json/<int:seva_type_id>', type='json', auth="user", website=True)
    def seva_schedules_json(self, seva_type_id, **kw):

        # Authorized user???  Validating if user is a meditator
        # if not request.env.user.partner_id.is_meditator:
        #     return self.__render_not_meditator(is_json=True)

        return self.__seva_schedule_list(seva_type_id)

    @http.route('/vro-registration/country-states-json/<int:country_id>', type='json', auth="user", website=True)
    def country_state_json(self, country_id, **kw):
        # Authorized user???  Validating if user is a meditator
        # if not request.env.user.partner_id.is_meditator:
        #     return self.__render_not_meditator(is_json=True)

        states = request.env["res.country.state"].sudo().search([('country_id', '=', country_id)])
        state_list = []
        if states:
            for s in states:
                state_list.append({'key': s.id, 'value': s.name})
        return state_list

    # @http.route('/vro-registration/validate_phone_number', auth='user', type='json')
    # def validate_phone_number(self, **kw):
    #     # Authorized user???  Validating if user is a meditator
    #     # if not request.env.user.partner_id.is_meditator:
    #     #     return self.__render_not_meditator(is_json=True)
    #
    #     is_valid_number = request.env['res.partner'].sudo().is_phone_valid(
    #         {'phone': kw.get('phone_number'), 'phone_country_code': kw.get('country_code')},
    #         'phone_country_code', 'phone')
    #     return is_valid_number

    @http.route('/my/vro-visits/', type='http', auth="user", website=True)
    def vro_visits(self, **kw):
        # Authorized user???  Validating if user is a meditator
        # if not request.env.user.partner_id.is_meditator:
        #     return self.__render_not_meditator()

        # Logged in User Partner ID (after SSO Login)
        partner_id = request.env.user.partner_id.id

        volunteer_record = request.env["isha.volunteer"].sudo().search(
            [('partner_id', '=', partner_id)],
            order='id desc',
            limit=1
        )
        visits = request.env["vro.registration"].sudo().search(
            [('partner_id', '=', partner_id)],
            order='expected_arrival_date desc, write_date desc'
        )

        formatted_visits = []
        id_hasher = HashidsConverter()
        for single in visits:
            key = id_hasher.encode(single.id)
            formatted_visits.append({
                'key': key,
                'record': single
            })

        return request.render('isha_vro_registration.isha_vro_registration_list_template', {
            'volunteer_record': volunteer_record,
            'visits': formatted_visits,
            'is_draft_options': dict(request.env['vro.registration'].sudo()._fields['is_draft'].selection)
        })

    def __editable_vro_record(self, my_id):
        record = request.env["vro.registration"].sudo().search(
            [('id', '=', my_id)],
            limit=1
        )
        # Case 1: No record, show error
        if not record:
            raise Exception('Oops! No Registration Record Found!')

        # Does this record belong to me?
        if record.partner_id.id != request.env.user.partner_id.id:
            raise Exception('Oops! No such record found!')

        # Case 2: Editable?
        # Editable Status: 'New', 'Verified'
        if not record.stage_name in ['New', 'Verified']:
            raise Exception('Oops! You can not edit this record!')

        return record

    def __cancelable_vro_record(self, my_id):
        record = request.env["vro.registration"].sudo().search(
            [('id', '=', my_id)],
            limit=1
        )
        # Case 1: No record, show error
        if not record:
            raise Exception('Oops! No Registration Record Found!')

        # Does this record belong to me?
        if record.partner_id.id != request.env.user.partner_id.id:
            raise Exception('Oops! No such record found!')

        # Case 2: Editable?
        # Editable Status: 'New', 'Verified'
        if not record.stage_name in ['New', 'Verified', 'Approved']:
            raise Exception('Oops! You can not cancel this record!')

        return record

    def __render_not_meditator(self, is_json=False):
        """
        Check if the request is coming from Meditaor

        If not meditator, we simply show them warning message
        :return:
        """
        message = 'Namaskaram, <br/><br/>' \
                  'Based on the details you provided, we were unable to find you in our database.<br/>' \
                  'If you have completed the Isha Yoga Program or Inner Engineering program, please contact ' \
                  '<a href="tel:+918300083111" class="btn btn-sm btn-warning">' \
                  '<i class="fa fa-phone mr-1"></i>83000 83111</a><br/><br/>' \
                  'If you have not yet completed any Isha Yoga Program, please visit the link below:' \
                  '<br/><br/>' \
                  '<a class="btn btn-success"' \
                  'href="https://isha.sadhguru.org/in/en/center/isha-yoga-center-coimbatore/upcoming-programs">' \
                  'Upcoming Programs</a>'
        if is_json:
            raise exceptions.UserError(message)  # Jsonrpc will catch this error and show alert

        return render_alert_template(request, {
            'message': message,
            'alert_type': 'main-card'
        })

    def _get_form_selection_values(self, vro_record):
        country_ids = request.env["res.country"].sudo().search([('name', '=', 'India')]) + \
                      request.env["res.country"].sudo().search([('name', '!=', 'India')])

        proof_type_options = request.env['proof.type'].sudo().search([])

        # For Address Proof, PAN CARD is not required
        pan_card_id = request.env.ref('isha_volunteer.sp_proof_type_pan_card').id
        address_proof_type_options = []
        for single in proof_type_options:
            if single.id != pan_card_id:
                address_proof_type_options.append(single)

        values = {
            'gender_options': request.env['res.partner'].sudo()._fields['gender'].selection,
            'marital_status_options': request.env['res.partner'].sudo()._fields['marital_status'].selection,
            'emergency_type_options': request.env['isha.volunteer.emergency.contact'].sudo()._fields[
                'emergency_type'].selection,
            'country_options': country_ids,
            'state_options': request.env["res.country.state"].sudo().search([]),
            'proof_type_options': proof_type_options,
            'address_proof_type_options': address_proof_type_options,
            'seva_type_options': request.env['vro.reg.seva.type'].sudo().search([]),
            'seva_schedule_options': [],
        }
        return values

    def __seva_schedule_list(self, seva_type_id):
        """
        Get the seva schedule list of given seva type

        NOTE: Only future schedule list will be presented to the registration form.
        :param seva_type_id: Integer Seva Type ID
        :return: List of Seva schedules
        """
        schedule_list = []
        schedules = request.env['vro.reg.seva.schedule'].sudo().search([
            ('seva_type_id', '=', seva_type_id),
            ('start_date', '>=', datetime.today().date())
        ])
        if schedules:
            for s in schedules:
                schedule_list.append({
                    'id': s.id,
                    'name': s.get_formatted_name(),
                    'arrival_date': (s.start_date - timedelta(days=1)).strftime('%Y-%m-%d'),
                    'departure_date': s.end_date.strftime('%Y-%m-%d')
                })
        return schedule_list

    def __save(self, post, vro_record):
        #
        # Task 1: Validate Mandatory Fields
        #
        _save_logger.info(f'VALIDATION STARTs: {datetime.now()}')
        self.__validate(post, vro_record)
        _save_logger.info(f'VALIDATION ENDS: {datetime.now()}')

        #
        # Task 2: Save Registration Record and Emergency Contact
        #
        _save_logger.info(f'DATA BUILD STARTS: {datetime.now()}')
        data = self.__get_post_data(post, vro_record)
        _save_logger.info(f'DATA BUILD ENDS: {datetime.now()}')
        try:
            # Save Registration Data
            _save_logger.info(f'WRITE VRO RECORD STARTS: {datetime.now()}')
            vro_record.write(data['registration_data'])
            _save_logger.info(f'WRITE VRO RECORD ENDS: {datetime.now()}')

            # Save Emergency Contacts
            _save_logger.info(f'UPDATE EMERGENCY INFO STARTS: {datetime.now()}')
            emergency_contact_data = data['emergency_contact_data']
            ec_model = request.env['isha.volunteer.emergency.contact'].sudo()
            ec_model.search([('volunteer_id', '=', vro_record.volunteer_id.id)]).unlink()
            ec_model.create(emergency_contact_data)
            _save_logger.info(f'UPDATE EMERGENCY INFO ENDS: {datetime.now()}')
        except ValidationError as e:
            ex_msg = f'<p><strong>Validation Error!</strong></p>{e.name}'
            raise exceptions.ValidationError(ex_msg)
        except Exception as e:
            ex_msg = str(e)
            img_error = 'This file could not be decoded as an image file'
            # Overwrite image validation error
            if img_error in ex_msg:
                ex_msg = '<p><strong>Invalid Upload!</strong></p>' \
                         'Only Image Files with extension .jpg, .jpeg, or .png are accepted.'
            raise exceptions.ValidationError(ex_msg)

    def __get_attachment_data(self, post):
        data = {}
        if post.get('photo'):
            data['image_1920'] = post.get('photo').get('data')

        if post.get('idProof'):
            data['id_proof'] = post.get('idProof').get('data')

        if post.get('addressProof'):
            data['address_proof'] = post.get('addressProof').get('data')

        return data

    def _get_section_mapping(self, section_submitted):
        section_mapping = {
            'visit-info': self._save_visit_info,
            'emergency-info': self._save_emergency_info,
            'personal-info': self._save_personal_info,
        }
        return section_mapping.get(section_submitted)

    def __validate(self, post, vro_record):

        # Validate Compulsory Fields
        self.__validate_mandatory_fields(post, vro_record)

        # Validate Arrival Date
        self.__validate_arrival_date(post)

        # Validate DOB
        self.__validate_dob(post)

        # Validate Emails
        self.__validate_emails(post)

        # Validate Phones
        self.__validate_phones(post)

        # Validate Attachment
        self.__validate_attachments(post, vro_record)

    def __validate_mandatory_fields(self, post, vro_record):
        mandatory_fields = self.__get_mandatory_fields()
        missing_field_list = []
        for field_key, field_label in mandatory_fields.items():
            field_value = post.get(field_key)
            field_value = field_value.strip() if field_value else None  # Remove spaces from start and end if any
            if not field_value:
                missing_field_list.append(field_label)

        if len(missing_field_list) > 0:
            error_text = ''
            for field_label in missing_field_list:
                error_text += f'<span><i class="fa fa-arrow-right mr-2"></i> <strong>{field_label}</strong></span><br/>'
            raise exceptions.ValidationError(
                '<p><strong>Oops! you missed some mandatory fields</strong></p>'
                f'<p>Please enter values for following mandatory fields:</p>{error_text}')

    def __validate_nationality_and_cor(self, post):
        nationality_id = get_numeric_value('nationality', post)
        country_id = get_numeric_value('countryOfResidence', post)

        message = '<p><strong>Invalid Nationality or Country of Residence</strong></p>' \
                  '<p>Sorry, Only Indian nationals are allowed!</p>' \
                  'If your Nationality or Country of Residence is other than <strong>India</strong>, ' \
                  'please click the following button for Overseas Registration.<br/><br/>' \
                  ' <a href="https://suvya.isha.in/" class="btn btn-danger">' \
                  '<i class="fa fa-paper-plane-o mr-1"></i>Overseas Registration</a><br/><br/>' \
                  'Pranam'
        if nationality_id != country_id:
            raise exceptions.ValidationError(message)

        country_record = request.env['res.country'].sudo().search([('id', '=', country_id)], limit=1)
        if not country_record or country_record.name.strip() != 'India':
            raise exceptions.ValidationError(message)

    def __validate_phones(self, post):
        phones = [
            {
                'label': 'Phone Number',
                'cc_field': 'phoneCountryCode',
                'phone_field': 'phoneNumber',
            },
            {
                'label': 'Primary Emergency Contact - Phone',
                'cc_field': 'emergencyContactPhoneCountryCodePrimary',
                'phone_field': 'emergencyContactPhonePrimary',
            },
            {
                'label': 'Secondary Emergency Contact - Phone',
                'cc_field': 'emergencyContactPhoneCountryCodeSecondary',
                'phone_field': 'emergencyContactPhoneSecondary',
            }
        ]
        invalid_field_list = []
        for single in phones:
            label = single.get('label')
            cc_field = single.get('cc_field')
            phone_field = single.get('phone_field')
            phone = post.get(phone_field)
            phone_country_code = post.get(cc_field)
            if phone:
                is_valid_number = request.env['res.partner'].sudo().is_phone_valid(
                    {'phone': phone, 'phone_country_code': phone_country_code},
                    'phone_country_code', 'phone')
                if not is_valid_number:
                    invalid_field_list.append(label)

        if len(invalid_field_list) > 0:
            error_text = ''
            for field_label in invalid_field_list:
                error_text += f'<span><i class="fa fa-arrow-right mr-2"></i> <strong>{field_label}</strong></span><br/>'
            raise exceptions.ValidationError(
                '<p><strong>Oops! Invalid Phone(s)</strong></p>'
                f'<p>Please enter the valid phone number for the following fields:</p>{error_text}')

    def __validate_emails(self, post):
        email_fields = {
            'emergencyContactEmailPrimary': 'Primary Emergency Contact - Email',
            'emergencyContactEmailSecondary': 'Secondary Emergency Contact - Email'
        }
        missing_field_list = []
        for field_key, field_label in email_fields.items():
            field_value = post.get(field_key)
            field_value = field_value.strip() if field_value else None  # Remove spaces from start and end if any
            if field_value and not is_valid_email(field_value):
                missing_field_list.append(field_label)

        if len(missing_field_list) > 0:
            error_text = ''
            for field_label in missing_field_list:
                error_text += f'<span><i class="fa fa-arrow-right mr-2"></i> <strong>{field_label}</strong></span><br/>'
            raise exceptions.ValidationError(
                '<p><strong>Oops! Invalid Email Address(es)</strong></p>'
                f'<p>Please enter the correct email for the following fields:</p>{error_text}')

    def __validate_attachments(self, post, vro_record):
        missing_field_list = []
        if not vro_record.image_1920 and not post.get('photo'):
            missing_field_list.append('Recent Photograph')

        if not vro_record.id_proof and not post.get('idProof'):
            missing_field_list.append('ID Proof')

        if not vro_record.address_proof and not post.get('addressProof'):
            missing_field_list.append('Address Proof')

        if len(missing_field_list) > 0:
            error_text = ''
            for field_label in missing_field_list:
                error_text += f'<span><i class="fa fa-arrow-right mr-2"></i> <strong>{field_label}</strong></span><br/>'
            raise exceptions.ValidationError(
                '<p><strong>Oops! you missed uploading some file(s)</strong></p>'
                f'<p>Please upload file(s) for following mandatory fields:</p>{error_text}')

    def __validate_dob(self, post):
        """
        Validate DOB: Must be min of 14 years of age
        :param post:
        :return:
        """
        dob_str = get_string_value('dateOfBirth', post)
        dob = datetime.strptime(dob_str, '%Y-%m-%d').date()
        today = datetime.today().date()
        difference_in_years = relativedelta(today, dob).years
        if difference_in_years < 14:
            raise exceptions.ValidationError(
                '<p><strong>Oops! Age validation occured!</strong></p>'
                '<p>Minimum 14 years of age is required.</p>')

    def __validate_arrival_date(self, post):
        """
        Validate Dates: Do not allow past date
        :param post:
        :return:
        """
        arrival_str = get_string_value('dateOfArrival', post)
        dt_arrival = datetime.strptime(arrival_str, '%Y-%m-%d').date()
        today = datetime.today().date()
        if dt_arrival < today:
            raise exceptions.ValidationError(
                '<p><strong>Validation Error! </strong></p>'
                '<p>Arrival Date can not be past date.</p>')

    def __get_mandatory_fields(self):
        return {
            # Visit Info
            'sevaType': 'Seva Type',
            'dateOfArrival': 'Date of Arrival',
            'dateOfDeparture': 'Date of Departure',

            # Emergency Info
            'emergencyContactNamePrimary': 'Primary Emergency Contact - Name',
            'emergencyContactAddressPrimary': 'Primary Emergency Contact - Address',
            'emergencyContactPhoneCountryCodePrimary': 'Primary Emergency Contact - Phone Country Code',
            'emergencyContactPhonePrimary': 'Primary Emergency Contact - Phone',
            'emergencyContactRelationPrimary': 'Primary Emergency Contact - Relation',
            'emergencyTypePrimary': 'Primary Emergency Contact - Type',
            'emergencyTypeSecondary': 'Secondary Emergency Contact - Type',

            # Personal Info
            'dateOfBirth': 'Date of Birth',
            'gender': 'Gender',
            'maritalStatus': 'Marital Status',
            'nationality': 'Nationality',
            'phoneCountryCode': 'Phone Country Code',
            'phoneNumber': 'Phone Number',

            # Address Info
            'countryOfResidence': 'Country of Residence',
            'postalCode': 'Postal Code',
            'street': 'Address Line1',
            'city': 'City',
            'stateId': 'State',
            'idProofType': 'ID Proof Type',
            'addressProofType': 'Address Proof Type'
        }

    def __get_post_data(self, post, vro_record):
        # Set Registration Type as Online
        registration_type = request.env.ref('isha_vro_registration.vro_reg_reg_type_online').id
        registration_data = {
            # Visit Details
            'seva_type': get_fkey_id_value('sevaType', post),
            'seva_schedule': get_fkey_id_value('sevaSchedule', post),
            'expected_arrival_date': get_string_value('dateOfArrival', post),
            'expected_departure_date': get_string_value('dateOfDeparture', post),
            'registration_type': registration_type,

            # Personal Information
            'dob': get_string_value('dateOfBirth', post),
            'gender': get_string_value('gender', post),
            'marital_status': get_string_value('maritalStatus', post),
            'nationality_id': get_numeric_value('nationality', post),  # Only Indian are allowed
            'phone_country_code': get_string_value('phoneCountryCode', post),
            'phone': get_string_value('phoneNumber', post),

            # Address Details
            'country_id': get_numeric_value('countryOfResidence', post),
            'zip': get_string_value('postalCode', post),
            'state_id': get_numeric_value('stateId', post),  # state id
            # 'state': get_string_value('stateText', post),  # state text
            'city': get_string_value('city', post),
            'street': get_string_value('street', post),
            'street2': get_string_value('street2', post),

            # id and address proof
            'id_proof_type': get_fkey_id_value('idProofType', post),
            'address_proof_type': get_fkey_id_value('addressProofType', post),

            # Is Draft?
            'is_draft': 'N',
        }

        # Registration Date: Add only if this value is not already present
        if not vro_record.registration_date:
            registration_data.update({
                'registration_date': datetime.now()
            })

        # File Data
        files_data = self.__get_attachment_data(post)
        if files_data:
            registration_data.update(files_data)

        # Do we have state_id field selected?
        if post.get('stateId'):
            registration_data.update({
                'state_id': get_numeric_value('stateId', post)
            })

        return {
            'registration_data': registration_data,
            'emergency_contact_data': self.__get_emergency_contact_data(post, vro_record)
        }

    def __get_emergency_contact_data(self, post, vro_record):
        volunteer_id = vro_record.volunteer_id.id
        contact_list = []
        field_postfixes = ['Primary', 'Secondary']
        valid_types = ['primary', 'secondary']
        for t in field_postfixes:
            emergency_type = get_string_value(f'emergencyType{t}', post)
            if emergency_type not in valid_types:
                raise exceptions.ValidationError(
                    '<p><strong>Validation Error!</strong></p>'
                    f'<p>Invalid Emergency Type supplied for emergencyType{t} - {emergency_type}</p>')

            single = {
                'volunteer_id': volunteer_id,
                'name': get_string_value(f'emergencyContactName{t}', post),
                'address': get_string_value(f'emergencyContactAddress{t}', post),
                'email': get_string_value(f'emergencyContactEmail{t}', post),
                'phone_country_code': get_string_value(f'emergencyContactPhoneCountryCode{t}', post),
                'phone': get_string_value(f'emergencyContactPhone{t}', post),
                'relation': get_string_value(f'emergencyContactRelation{t}', post),
                'emergency_type': get_string_value(f'emergencyType{t}', post),
            }
            if single.get('name'):
                contact_list.append(single)
        return contact_list
