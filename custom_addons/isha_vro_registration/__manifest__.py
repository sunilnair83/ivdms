# -*- coding: utf-8 -*-
{
    'name': "Isha - VRO Registration",
    'summary': """VRO Registration""",
    'description': """
        VRO Registration
    """,
    'author': "IP Bastola",
    'website': "http://www.yourcompany.com",
    'category': 'CRM',
    'version': '0.1',
    'depends': [
        'base',
        'contacts',
        'website',
        'isha_crm',
        'isha_ims',
        'isha_volunteer',
        'tool_image_preview',
        'isha_website_utils',
    ],
    'data': [
        # Security
        'security/security.xml',
        'security/ir.model.access.csv',

        # Data
        'data/ir_sequence.xml',
        'data/seva_type_data.xml',
        'data/reg_stage_data.xml',
        'data/reg_type_data.xml',
        'data/reg_sms_configuration_data.xml',
        'data/reg_category_data.xml',

        # Views
        'views/vro_reg_menu_view.xml',
        'views/vro_reg_reg_type_view.xml',
        'views/vro_reg_seva_type_view.xml',
        'views/vro_reg_seva_schedule_view.xml',
        'views/vro_reg_reg_stage_view.xml',
        'views/vro_reg_followup_action_view.xml',
        'views/low_match_pairs.xml',
        'views/vro_registration_view.xml',
        'views/vro_reg_followup_view.xml',
        'views/vro_reg_sms_configuration_view.xml',
        'views/vro_reg_comment_view.xml',
        'views/vro_reg_category_view.xml',
        'views/vro_tag_view.xml',

        # Templates
        'views/vro_reg_templates.xml',
        'views/vro_reg_form_template.xml',

    ],
    'application': True,
}
