# -*- coding: utf-8 -*-

import datetime
from datetime import timedelta

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class AppointmentReschedule(models.TransientModel):
    _name = 'appointment.reschedule'
    _description = 'Appointment Reschedule'
    _order = 'id desc'

    appointment_id = fields.Many2one('sp.appointment', string='Appointment')
    assign_to_me = fields.Boolean(string='Assign to Me', default=True)
    appointment_date_time = fields.Datetime()
    interviewer_id = fields.Many2one('sp.interviewer', string='Interviewer')
    time_slot = fields.Many2one('calendar.event')
    reschedule_type = fields.Selection([('new_slot', 'Pick New Datetime'),
                                        ('existing_slot', 'Existing Slot')], default='new_slot')

    current_interviewer_id = fields.Many2one('sp.interviewer', readonly=1)
    current_time_slot = fields.Many2one('calendar.event', readonly=1)

    @api.onchange('interviewer_id', 'time_slot', 'assign_to_me')
    def onchange_domain(self):
        domain = {'time_slot': [('interviewer_id', '!=', False),
                                ('start', '>=', datetime.datetime.today()),
                                ('slot_state', 'not in', ['booked'])],
                  'interviewer_id': []}
        if self.interviewer_id and self.assign_to_me:
            domain['time_slot'] += [('interviewer_id', '=', self.interviewer_id.id)]
        if self.time_slot:
            domain['interviewer_id'] += [('id', '=', self.time_slot.interviewer_id.id)]
        # if self.current_interviewer_id:
            # domain['interviewer_id'] += [('id', '!=', self.current_interviewer_id.id)]
            # domain['time_slot'] += [('interviewer_id', '!=', self.current_interviewer_id.id)]
        return {'domain': domain}

    @api.onchange('reschedule_type')
    def onchange_reschedule_type(self):
        for record in self:
            if record.appointment_date_time:
                record.appointment_date_time = None
            if record.time_slot:
                record.time_slot = None

    @api.onchange('time_slot')
    def onchange_time_slot(self):
        for record in self:
            if record.time_slot:
                record.interviewer_id = record.time_slot.interviewer_id

    @api.onchange('appointment_date_time')
    @api.constrains('appointment_date_time')
    def _onchange_appointment_date_time(self):
        for record in self:
            if record.appointment_date_time and record.appointment_date_time.date() < fields.Datetime.today().date():
                raise ValidationError("Please select a 'Appointment Date' equal/greater than the current date")

    @api.model
    def create(self, values):
        # If the time slot (calendar.event) is virtual(recurring) event then it will detached and create real event
        if values.get('time_slot') and isinstance(values.get('time_slot'), str):
            recurring_virtual_event = self.env['calendar.event'].browse(values.get('time_slot'))
            recurring_actual_event = recurring_virtual_event.detach_recurring_event().id
            values.update({'time_slot': recurring_actual_event})
        old_time_slot = self.env['sp.appointment'].browse(values.get('appointment_id')).time_slot.id
        new_time_slot = values.get('time_slot')
        if new_time_slot and old_time_slot != new_time_slot:
            self.env['calendar.event'].browse(old_time_slot).slot_state = 'available'
            self.env['calendar.event'].browse(new_time_slot).slot_state = 'booked'
        return super(AppointmentReschedule, self).create(values)

    def action_reschedule_interviewer(self):
        appoint_id = self.env['sp.appointment'].browse(self.appointment_id.id)
        values = {}

        if self.assign_to_me:
            values['interviewer_id'] = self.env['sp.interviewer'].search([('user_id', '=', self.env.uid)], limit=1)
            values['type'] = 'regular'
        else:
            values['type'] = 'marathon'
            values['interviewer_id'] = None

        if self.time_slot:
            values['time_slot'] = self.time_slot
        else:
            date_time = self.appointment_date_time
            values['app_dt_start'] = date_time
            values['app_dt_stop'] = date_time + timedelta(minutes=appoint_id.duration)

        appoint_id.sudo().write(values)
        appoint_id.sudo().select_or_create_slot()
        appoint_id.registration_id.sudo().write({'appointment_state': 'rescheduled'})

    def action_reschedule_scheduler(self):
        appoint_id = self.env['sp.appointment'].browse(self.appointment_id.id)
        values = {}

        values['time_slot'] = self.time_slot.id
        values['interviewer_id'] = self.interviewer_id.id

        appoint_id.sudo().write(values)
        appoint_id.registration_id.sudo().write({'appointment_state': 'rescheduled'})
