# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError

from ..models import constants


class InterviewCommentsWizard(models.TransientModel):
    _name = 'interview.comments.wizard'
    _description = 'Interview Comment Wizard'
    _order = 'id desc'

    name = fields.Text('Comment', required=True)

    # HA Form Comment
    ha_additional_values_ids = fields.Many2many('ha.additional.values', string="HA Additional Values")
    any_highlights_for_seva = fields.Selection(list(constants.BOOLEAN_VALUES.items()),string='Any highlights for SP Team', default='no')
    extra_comments = fields.Text('Comments for SP Team')
    is_feedback = fields.Boolean()

    # Interview Form Reviewer's Comment
    interview_rejection_email_type = fields.Many2one('rejection.email.type', string='Rejection Email Type')

    def create_save(self):
        if self._context.get('screening_response') and self.any_highlights_for_seva == 'yes' \
            and not self.ha_additional_values_ids:
            raise ValidationError("Please select at least one checkbox!")
        recs = self.env[self._context.get('active_model')].sudo().browse(self._context.get('active_ids'))
        recs_value = {}
        if self._context.get('comment') and self.env.user.has_group(
            'isha_sp_registration.group_sp_appointment_interviewer'):
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_interviewer_comments').id
        elif self._context.get('comment') and self.env.user.has_group(
            'isha_sp_registration.group_sp_appointment_reviewer'):
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_reviewer_comments').id
        elif self._context.get('followup_required'):
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_reviewer_follow').id
            recs_value['interview_follow_up_status'] = "followup_required"
            recs_value['is_followup_required'] = True
            # recs_value['show_button_reviewer'] = False
        elif self._context.get('followup_response'):
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_interviewer_follow').id
            recs_value['interview_follow_up_status'] = "followup_completed"
            recs_value['sp_interview_status'] = "ready_for_review"
            recs_value['is_followup_required'] = False
            # recs_value['show_button_reviewer'] = True
        elif self._context.get('awaiting_info'):
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_interviewer_comments').id
            recs_value['interview_form_state'] = recs.previous_interview_form_state or 'new'
            # recs_value['sp_interview_status'] = "ready_for_review"
            if recs.previous_interview_form_state == 'feedback_received':
                recs_value['show_ready_for_review_button'] = True
        elif self._context.get('final_approval'):
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_final_approval').id
        elif self._context.get('interviewer_comment'):
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_interviewer_comments').id
            recs_value['interview_form_state'] = self._context.get('interview_form_state')
            recs_value['interviewer_opinion'] = self._context.get('interviewer_opinion')
        elif self._context.get('reviewer_comment'):
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_reviewer_comments').id
            recs_value['interview_form_state'] = self._context.get('interview_form_state')
            recs_value['interview_reviewer_decision'] = self._context.get('interview_reviewer_decision')
            if self._context.get('sp_interview_status'):
                recs_value['sp_interview_status'] = self._context.get('sp_interview_status')
            if self.interview_rejection_email_type:
                recs_value['interview_rejection_email_type'] = self.interview_rejection_email_type
        elif self._context.get('review_response') or self._context.get('review_doctor_comment'):
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_review_doctor_comments').id
        elif self._context.get('trial_drop_out'):
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_tp_dropout_comments').id
        elif self._context.get('individual_meeting_feedback'):
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_tp_individual_meeting_comments').id
        elif self._context.get('sdp_tagged_comments'):
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_sdp_tagged_comments').id
        elif self._context.get('screening_response') or self._context.get('screening_doctor_comment'):
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_screening_doctor_comments').id
        elif recs.sp_is_overseas:
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_oco').id
            recs_value['interview_form_state'] = "oco_feedback"
            recs_value['sp_interview_status'] = "oco_feedback"
            recs_value['is_feedback_received'] = True
            recs_value['show_button_reviewer'] = True
        elif not recs.sp_is_overseas:
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_vro').id
            recs_value['interview_form_state'] = "vro_feedback"
            recs_value['sp_interview_status'] = "vro_feedback"
            recs_value['is_feedback_received'] = True
            recs_value['show_button_reviewer'] = True
        else:
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_interviewer_comments').id
        values = {
            'feedback_type': feedback_type_id,
            'value': self.name,
        }
        if self.any_highlights_for_seva == 'no' and self._context.get('screening_response'):
            self.ha_additional_values_ids = None
        if self.extra_comments:
            feedback_type_id = self.env.ref('isha_sp_registration.feedback_type_screen_doc_seva_team_comments').id
            if self.is_feedback:
                last_comment = self.env['registration.feedback'].search(
                    [('feedback_type', '=', feedback_type_id),
                     ('health_assessment_id', '=', recs.id)], limit=1, order="id desc")
                last_comment.sudo().write({
                    'value': self.extra_comments
                })
            else:
                self.env['registration.feedback'].sudo().create({
                    "feedback_type": feedback_type_id,
                    "value": self.extra_comments,
                    "health_assessment_id": recs.id,
                })
            recs.write({
                'any_highlights_for_seva': self.any_highlights_for_seva,
            })
        if self._context.get('screening_response'):
            values.update({
                "health_assessment_id": recs.id,
            })
            recs_value["screening_doctor_decision"] = self._context.get('screening_doctor_decision')
            # recs_value["sp_health_assessment_form_status"] = self._context.get('sp_health_assessment_form_status')
            recs_value["sp_health_assessment_state"] = self._context.get('sp_health_assessment_state')
            recs_value["ha_screening_doctor"] = self.env.uid
            recs_value["ha_screening_doctor_datetime"] = fields.Datetime.now()
            recs_value["ha_additional_values_ids"] = self.ha_additional_values_ids
            if not recs.screening_doctor_decision_date:
                recs_value["screening_doctor_decision_date"] = fields.Datetime.now()
        elif self._context.get('review_response'):
            values.update({
                "health_assessment_id": recs.id,
            })
            recs_value["review_doctor_decision"] = self._context.get('review_doctor_decision')
            recs_value["sp_health_assessment_form_status"] = self._context.get('sp_health_assessment_form_status')
            recs_value["sp_health_assessment_state"] = self._context.get('sp_health_assessment_state')
            recs_value["ha_doctor_approval"] = self.env.uid
            recs_value["ha_doctor_approval_datetime"] = fields.Datetime.now()
            if not recs.review_doctor_decision_date:
                recs_value["review_doctor_decision_date"] = fields.Datetime.now()
        elif self._context.get('review_doctor_comment') or self._context.get('screening_doctor_comment'):
            values.update({
                "health_assessment_id": recs.id,
            })
        elif self._context.get('trial_drop_out') or self._context.get('individual_meeting_feedback'):
            values.update({
                "registration_id": recs.sp_reg_id.id,
            })
        elif self._context.get('sdp_tagged_comments'):
            values.update({
                "registration_id": recs.id,
            })
        else:
            values.update({
                "registration_id": recs.registration_profile_id.sp_reg_id.id,
            })
        registration_feedback_id = self.env['registration.feedback'].sudo().create(values)
        recs.sudo().write(recs_value)
        if self._context.get('ha_email') and self._context.get('interviewer_comment'):
            return recs.send_health_assessment_email()
        if self._context.get('awaiting_info') and recs.interviewer_opinion in \
            ["selected", "selected_with_tp", "selected_with_ha", "selected_with_tp_ha", "waiting_with_vro",
             "waiting_with_oco", "trial_is_must"] and recs.sp_health_assessment_form_status != 'email_sent':
            return recs.send_health_assessment_email()
