# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CancellationWizard(models.TransientModel):
    _name = 'cancellation.wizard'
    _description = 'Cancellation Wizard'
    _order = 'id asc'

    name = fields.Text('Comment')
    cancellation_reason = fields.Many2one('cancellation.reason', required=True)

    def cancel_application_button(self):
        active_model_id = self.env[self._context.get('active_model')].sudo().search([
            ('id', '=', self._context.get('active_id'))])
        if self._context.get('appointment') and not self._context.get('interview_form'):
            active_model_id = self.env['sp.registration'].sudo().search(
                [('id', '=', self._context.get('registration_id'))])
        if self._context.get('final_approval'):
            active_model_id = self.env['sp.interview.form'].sudo().search(
                [('id', '=', self._context.get('sp_interview_id'))])
        if active_model_id.appointment_ids and active_model_id.appointment_state and active_model_id.appointment_state != 'completed':
            active_model_id.appointment_state = 'cancelled'
        active_model_id.write(
            {
                'cancellation_reason': self.cancellation_reason.id,
                'application_stage': self.env.ref('isha_sp_registration.application_stage_cancelled').id,
            })
        if self._context.get('active_model') == 'sp.interview.form':
            return {
                'name': "Interview Form",
                'res_model': 'sp.interview.form',
                'type': 'ir.actions.act_window',
                'view_mode': 'tree,form',
                'views': False,
                'target': 'current',
                # 'context': context,
                # 'domain': domain,
            }
