# -*- coding: utf-8 -*-

import datetime

from odoo import models, fields, api


class ConfirmMailMessage(models.TransientModel):
    _name = 'confirm.mail.message'
    _description = 'Confirm Mail Message'
    _order = 'id desc'

    message = fields.Text(default="Are you sure you want to send email?", required=True, readonly=True)
    template_id = fields.Many2one('mail.template', string='Email Template', required=True)

    def send_mail(self):
        record = self.env[self._context.get('active_model')].sudo().browse(self._context.get('active_id'))
        if self._context.get('awaiting_info'):
            record = self.env[self._context.get('params').get('model')].sudo().browse(
                self._context.get('params').get('id'))
        if self._context.get('interviewer_comment'):
            record = self.env['sp.interview.form'].sudo().browse(self._context.get('sp_interview_id'))
        values = self.template_id.generate_email(record.registration_profile_id.sp_reg_id.id, fields=None)
        self.env['mail.mail'].sudo().create(values).send(False)
        if self._context.get('health_assessment'):
            if record.sp_health_assessment_form_status == 'not_sent_email':
                record.sp_health_assessment_form_status = "email_sent"
            if record.sp_health_assessment_state == 'not_sent_email':
                record.sp_health_assessment_state = "email_sent"
            # if not record.ha_email_sent_date:
            record.ha_email_sent_date = datetime.datetime.today()
            if not record.health_assessment_id.id:
                self.env['sp.reg.health.assessment'].sudo().create({
                    'sp_reg_id': record.registration_profile_id.sp_reg_id.id,
                    'sp_reg_profile_id': record.registration_profile_id.id,
                    'interview_form_id': record.id
                })

    def server_action_send_mail(self):
        records = self.env[self._context.get('active_model')].sudo().browse(self._context.get('active_ids'))
        for record in records:
            template_id = self.env['mail.template'].browse(self._context.get('template_id'))
            values = template_id.generate_email(record.registration_profile_id.sp_reg_id.id, fields=None)
            self.env['mail.mail'].sudo().create(values).send(False)
            if self._context.get('health_assessment'):
                if record.sp_health_assessment_form_status == 'not_sent_email':
                    record.sp_health_assessment_form_status = "email_sent"
                if record.sp_health_assessment_state == 'not_sent_email':
                    record.sp_health_assessment_state = "email_sent"
                # if not record.ha_email_sent_date:
                record.ha_email_sent_date = datetime.datetime.today()
                if not record.health_assessment_id.id:
                    self.env['sp.reg.health.assessment'].sudo().create({
                        'sp_reg_id': record.registration_profile_id.sp_reg_id.id,
                        'sp_reg_profile_id': record.registration_profile_id.id,
                        'interview_form_id': record.id
                    })
