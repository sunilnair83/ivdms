# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models
from odoo.exceptions import ValidationError


class TrialPeriodMeetingDates(models.TransientModel):
    _name = 'trial.period.meeting.dates'

    trial_period_ids = fields.Many2many('sp.reg.trial.period', string='Applicants', readonly=True)
    group_meeting_date = fields.Datetime()
    individual_meeting_date = fields.Datetime()

    @api.model
    def default_get(self, fields):
        result = super(TrialPeriodMeetingDates, self).default_get(fields)
        result['trial_period_ids'] = self._get_valid_trial_period_ids()
        return result

    def update_group_meeting_dates(self):
        if not self.trial_period_ids:
            raise ValidationError('Please select records to update group meeting date.')
        for record in self.trial_period_ids:
            record.group_meeting_date = self.group_meeting_date

    def update_individual_meeting_dates(self):
        if not self.trial_period_ids:
            raise ValidationError('Please select records to update individual meeting date.')
        for record in self.trial_period_ids:
            record.individual_meeting_date = self.individual_meeting_date

    def _get_valid_trial_period_ids(self):
        valid_trial_period_ids = []
        is_group_meeting = self._context.get('meeting_type') == 'group'
        is_individual_meeting = self._context.get('meeting_type') == 'individual'
        if is_group_meeting:
            valid_trial_period_ids = self._get_checked_in_applicants()
        elif is_individual_meeting:
            valid_trial_period_ids = self._get_grp_meeting_completed_applicants()
        if not valid_trial_period_ids:
            error_message = 'Please select records to update group meeting date.'
            if is_group_meeting:
                error_message = 'Group meeting can be scheduled only for participants in ashram. ' + error_message
            elif is_individual_meeting:
                error_message = 'Individual meeting can be scheduled only after completing group meeting. ' + error_message
            raise ValidationError(error_message)
        return valid_trial_period_ids

    def _get_checked_in_applicants(self):
        checked_in_applicant_ids = []
        if self._context.get('active_model') == 'sp.reg.trial.period':
            applicant_records = self.env['sp.reg.trial.period'].browse(self._context.get('active_ids'))
            checked_in_applicant_ids = [applicant.id for applicant in applicant_records
                                        if applicant['trial_period_status'] == 'vro_checked_in']
        return checked_in_applicant_ids

    def _get_grp_meeting_completed_applicants(self):
        applicant_ids = []
        if self._context.get('active_model') == 'sp.reg.trial.period':
            applicant_records = self.env['sp.reg.trial.period'].browse(self._context.get('active_ids'))
            applicant_ids = [applicant.id for applicant in applicant_records
                             if applicant['trial_period_status'] == 'gp_meeting_completed']
        return applicant_ids
