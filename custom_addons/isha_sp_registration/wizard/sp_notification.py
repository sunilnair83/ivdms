# -*- coding: utf-8 -*-

from odoo import models, fields, api


class SpNotification(models.TransientModel):
    _name = 'sp.notification'
    _description = 'SP Notification'

    name = fields.Text('Comment')
    sms = fields.Boolean(default=True)
    email = fields.Boolean(default=True)

    @api.model
    def create(self, values):
        res = super(SpNotification, self).create(values)
        record = self.env[self._context.get('active_model')].sudo().browse(self._context.get('active_id'))
        if self._context.get('no_response'):
            if res.sms:
                print('Send SMS')
            if res.email:
                template_id = self.env.ref('isha_sp_registration.email_template_appointment_no_response')
                mail_values = template_id.generate_email(record.registration_id.id, fields=None)
                self.env['mail.mail'].sudo().create(mail_values).send(False)
        return res
