# -*- coding: utf-8 -*-
import logging
import requests

from odoo import exceptions
from odoo.http import request
from requests.adapters import HTTPAdapter

from .isha_importer import Configuration

_logger = logging.getLogger(__name__)

sso_config = Configuration('SSO')

session = requests.Session()
session.mount(sso_config['SSO_PMS_ENDPOINT'], HTTPAdapter(max_retries=5))


# User creation
def ensure_user(env, partner_id, email, first_name, last_name, country_code):
    try:
        sso_user_profile_id, is_new_sso_user = ensure_sso_user(email, first_name, last_name, country_code)
        user = get_user_from_sso_id(env, sso_user_profile_id)
        if not user.id:
            template = {
                'login': sso_user_profile_id,
                'partner_id': partner_id,
                'company_id': 1,
                'website_published': True,
                'company_ids': [(6, 0, [1])]
            }
            user = create_user_with_template(env, template)
        user.ensure_one()
        user.special_portal_access = not user.is_meditator
        return user, is_new_sso_user
    except Exception as ex:
        error_message = 'SP reg: Failed to ensure user account'
        _logger.error(error_message, exc_info=True)
        raise ex


def get_user_from_sso_id(env, sso_user_profile_id):
    return env['res.users'].sudo().search([('login', '=', sso_user_profile_id)])


def create_user_with_template(env, template):
    user = env['res.users'].sudo()._signup_create_user(template)
    return user


def ensure_sso_user(email, first_name, last_name, country_code):
    is_new_sso_user = False
    sso_user = get_sso_user(email, country_code)
    if not sso_user or not sso_user['profileId']:
        sso_user = create_sso_user(email, first_name, last_name, country_code)
        is_new_sso_user = True

    sso_user_profile_id = sso_user['profileId']
    if not sso_user_profile_id:
        raise Exception('Failed to get SSO profile ID')
    return sso_user_profile_id, is_new_sso_user


def get_sso_user(email, country_code):
    # sso_config = Configuration('SSO')
    url = sso_config['SSO_PMS_ENDPOINT'] + '/api/admin/user-accounts'
    headers = {
        'authorization': 'Bearer ' + sso_config['SSO_API_SYSTEM_TOKEN'],
        'content-type': 'application/json',
        'X-user-country': country_code
    }
    res = session.get(url, headers=headers, params={
        'email': email
    })
    if res.status_code == requests.codes.ok:
        return res.json()


def create_sso_user(email, first_name, last_name, country_code):
    # sso_config = Configuration('SSO')
    url = sso_config['SSO_PMS_ENDPOINT'] + '/api/admin/user-accounts?skipMail=True'
    headers = {
        'authorization': 'Bearer ' + sso_config['SSO_API_SYSTEM_TOKEN'],
        'X-user-country': country_code
    }
    post_data = {
        "countryOfResidence": country_code,
        "email": email,
        "firstName": first_name,
        "lastName": last_name
    }
    res = session.post(url, json=post_data, headers=headers)
    res.raise_for_status()
    return res.json()


def get_sso_account_password_reset_link(email, country_code):
    # sso_config = Configuration('SSO')
    url = sso_config['SSO_PMS_ENDPOINT'] + '/api/admin/user-accounts/reset-password-link'
    headers = {
        'authorization': 'Bearer ' + sso_config['SSO_API_SYSTEM_TOKEN'],
        'content-type': 'application/json',
        'X-user-country': country_code
    }
    res = session.get(url, headers=headers, params={
        'email': email
    })
    res.raise_for_status()
    reset_link = res.json()['resetLink']
    return reset_link
