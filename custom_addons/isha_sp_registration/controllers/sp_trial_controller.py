# -*- coding: utf-8 -*-
import logging

from odoo import http, exceptions
from odoo.http import request

from .sp_reg_controller_util import get_latest_sp_record_for_logged_in_user, render_error_template


_logger = logging.getLogger(__name__)


class TrialForm(http.Controller):
    @http.route('/trial-form/', auth='public', website=True)
    def get(self, **kw):
        try:
            user = request.env.user
            if user.has_group('base.group_public'):
                return request.render('isha_crm.sso_auto_redirect', {})
            if not user.has_group('isha_sp_registration.group_sp_portal_trial'):
                user.sudo().write({
                    'groups_id': [(4, request.env.ref('isha_sp_registration.group_sp_portal_trial').id)],
                })
            trial_record = self._get_trial_record()
            template_context = {
                'trial_record': trial_record
            }
            return request.render('isha_sp_registration.trial_form', template_context)
        except exceptions.AccessError as ex:
            return render_error_template(ex.name)
        except Exception:
            _logger.error("Trial record fetch exception", exc_info=True)
            return render_error_template('Something went wrong. Please try again.')

    @http.route('/trial-form/save/', type='json', auth="user", website=True)
    def save(self, **post):
        try:
            trial_record = self._get_trial_record()
            if 'start_date' in post and 'end_date' in post:
                values = {
                    'start_date': post.get('start_date'),
                    'end_date': post.get('end_date'),
                    'trial_period_status': 'dates_received'
                }
                trial_record.write(values)
            else:
                raise exceptions.ValidationError('Missing required fields')
        except (exceptions.AccessError, exceptions.MissingError, exceptions.ValidationError) as ex:
            _logger.error("SP trial form save exception", exc_info=True)
            raise ex
        except Exception:
            _logger.error("SP trial form save exception", exc_info=True)
            raise exceptions.UserError('Failed to save the application. '
                                       'Please try again or contact us over email/phone.')

    def _get_trial_record(self):
        latest_sp_record = get_latest_sp_record_for_logged_in_user()
        if latest_sp_record.sp_profile_id and latest_sp_record.sp_profile_id.id:
            interview_record = request.env['sp.interview.form'].sudo().search([('registration_profile_id', '=',
                                                                                latest_sp_record.sp_profile_id.id)])
            if interview_record.id and interview_record.trial_period_id and interview_record.trial_period_id.id:
                return interview_record.trial_period_id

            raise exceptions.UserError('Failed to get record')
        raise exceptions.UserError('Failed to get SP profile record')
