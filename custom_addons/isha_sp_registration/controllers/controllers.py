# -*- coding: utf-8 -*-
import logging
import socket

from odoo import http
from odoo.http import request
from ...isha_crm.ContactProcessor import IshaVolunteer
from .sp_reg_controller_util import render_error_template_for_public_user
_logger = logging.getLogger(__name__)


class SpRegistration(http.Controller):
    @http.route(['/pre_registration/', '/sp_registration/'], auth='public', website=True)
    def sp_registration(self, **kw):
        # Auto redirect Public users to SSO Login page
        # if request.env.user.has_group('base.group_public'):
        #     return request.render('isha_crm.sso_auto_redirect', {})
        res_partner = request.env['res.partner'].sudo()
        sp_registration = request.env['sp.registration'].sudo()
        sp_registration_settings = request.env['sp.registration.settings'].sudo().search([('active', '=', True)],
                                                                                         limit=1, order="id desc")
        country_ids = request.env["res.country"].sudo().search([('name', '=', 'India')]) + \
                      request.env["res.country"].sudo().search([('name', '!=', 'India')])

        values = {
            'phone_country_codes': country_ids,
            'genders': res_partner._fields['gender'].selection,
            'sp_marital_status': sp_registration._fields['sp_marital_status'].selection,
            'why_sadhanapada': request.env["why.sadhanapada"].sudo().search([]),
            'sp_reference': request.env["about.sadhanapada"].sudo().search([]),
            'states': request.env["res.country.state"].sudo().search([]),
            'countries': country_ids,
            'ieps': sp_registration._fields['ie_status'].selection,
            'iep_month': sp_registration._fields['iep_month'].selection,
            'iep_year': sp_registration._fields['iep_year'].selection,
            'title': sp_registration_settings.name,
            'utm_source': kw.get('utm_source'),
            'utm_medium': kw.get('utm_medium'),
            'utm_campaign': kw.get('utm_campaign'),
            'utm_term': kw.get('utm_term'),
            'utm_content': kw.get('utm_content'),
        }
        return request.render("isha_sp_registration.isha_sp_registration", values)

    @http.route('/pre_registration/json_test_save', auth='public', type='json', csrf=False)
    def registration_json_test_save(self, **kw):
        if kw.get('registrations'):
            result = ''
            for registration in kw.get('registrations'):
                try:
                    result = self.registration_common_save(registration)
                except Exception as ex:
                    _logger.error(ex)
                    result = 'Exception error'
            return result

    @http.route('/pre_registration/http_test_save', auth='public', type='http', csrf=False)
    def registration_http_test_save(self, **kw):
        return self.registration_common_save(kw)

    @http.route('/pre_registration/save', auth='public', website=True)
    def registration_save(self, **kw):
        return self.registration_common_save(kw)

    def registration_common_save(self, kw):
        res_partner = request.env['res.partner'].sudo()
        sp_registration = request.env['sp.registration']
        sp_registration_settings = request.env['sp.registration.settings'].sudo().search([('active', '=', True)],
                                                                                         limit=1, order="id desc")
        values = self._get_pre_reg_form_values(kw, sp_registration)

        if request.httprequest.content_type == 'application/json':
            values['why_sadhanapada'] = kw.get('why_sadhanapada')
            values['sp_reference'] = kw.get('sp_reference')
        try:
            contact_processor = IshaVolunteer()
            contact_flag = res_partner.sudo().create_internal_flag(contact_processor.getOdooFormat(values,{},{}))
            res_partner_id = contact_flag['rec'][0]
            already_registered = sp_registration.sudo().search([('partner_id', '=', res_partner_id.id),
                                                                ('registration_batch.status', '=', 'current')])
            if already_registered:
                _logger.error("SP pre-reg Already registered in Current Batch")
                return render_error_template_for_public_user('You\'re already registered in Current Batch. '
                                                   'Please contact us over email/phone.')
            values['is_new_contact'] = sp_registration.check_new_contact(contact_flag['phase'])
            values['partner_id'] = res_partner_id.id
            sp_record = sp_registration.sudo().create(values)
            values['sp_id'] = int(sp_record.sp_id)
            values.update({
                'thank_message_1': sp_registration_settings.thank_message_1,
                'thank_message_2': sp_registration_settings.thank_message_2,
                'thank_message_3': sp_registration_settings.thank_message_3,
            })
            if request.httprequest.content_type == 'application/json':
                return 'success'
            return request.render("isha_sp_registration.sp_registration_success", values)

        except Exception:
            _logger.error("SP pre-reg record creation exception", exc_info=True)
            return render_error_template_for_public_user('Failed to submit the application. '
                                               'Please try again or contact us over email/phone.')

    @http.route('/pre_registration/already_registered', auth='public', website=True)
    def registration_already(self, **kw):
        values = {}
        sp_registration_settings = request.env['sp.registration.settings'].sudo().search([('active', '=', True)],
                                                                                         limit=1, order="id desc")
        values.update({
            'thank_message_already_1': sp_registration_settings.thank_message_already_1,
            'thank_message_2': sp_registration_settings.thank_message_2,
            'thank_message_already_end': sp_registration_settings.thank_message_already_end,
        })
        if kw.get('sp_id'):
            values.update({
                'sp_id': int(kw.get('sp_id'))
            })

        return request.render("isha_sp_registration.sp_registration_already_registered", values)

    @http.route('/pre_registration/check_email', auth='public', type='json')
    def check_email(self, **kw):
        already_registered = request.env['sp.registration'].sudo().search([('sp_email', '=', kw.get('email')),
                                                                           ('registration_batch.status', '=',
                                                                            'current')])
        return len(already_registered) > 0

    def _get_pre_reg_form_values(self, kw, sp_registration):
        values = {}
        if kw.get('email'):
            # already_registered = sp_registration.sudo().search([('sp_email', '=', kw.get('email').lower()), ('registration_batch_status', '=', 'current')])
            # if len(already_registered) > 0:
            #     return request.redirect('/pre_registration/already_registered?sp_id=' + already_registered.sp_id)
            values.update({
                'sp_email': kw.get('email').lower()
            })
        if kw.get('first_name') and kw.get('last_name'):
            values.update({
                'sp_name': kw.get('first_name') + " " + kw.get('last_name')
            })
        values.update({
            'first_name': self._get_value('first_name', kw),
            'last_name': self._get_value('last_name', kw),
            'dob': self._get_value('dob', kw),
            'gender': self._get_value('gender', kw),
            'sp_marital_status': self._get_value('sp_marital_status', kw),
            'sp_phone': self._get_value('phone', kw),
            'sp_phone_country_code': self._get_value('phone_country_code', kw),
            'sp_city': self._get_value('jsspregfmcity', kw),
            'sp_state': self._get_value('state', kw),
            'sp_state_id': int(self._get_value('jsspregfmstate', kw)
                               if self._get_value('jsspregfmstate', kw) else False),
            'sp_country_id': int(self._get_value('jsspregfmcountry', kw)),
            'sp_zip': self._get_value('jsspregfmpincode', kw),
            'sp_nationality': int(self._get_value('nationality', kw)),
            'ie_status': self._get_value('ie_status', kw),
            'iep_month': self._get_value('iep_month', kw),
            'iep_year': self._get_value('iep_year', kw),
            'iep_location': self._get_value('iep_location', kw),
            'iep_teacher': self._get_value('iep_teacher', kw),
            'user_agent': request.httprequest.environ.get('HTTP_USER_AGENT', ''),
            'user_registered_ip': request.httprequest.environ.get("HTTP_X_FORWARDED_FOR"),
            'registration_source': self._get_value('registrationSource', kw),
            'utm_campaign': self._get_value('utm_campaign', kw),
            'utm_source': self._get_value('utm_source', kw),
            'utm_medium': self._get_value('utm_medium', kw),
            'utm_term': self._get_value('utm_term', kw),
            'utm_content': self._get_value('utm_content', kw)
        })

        if kw.get('sp_acknowledgement'):
            sadhanapada_value = False
            if kw.get('sp_acknowledgement') == 'on':
                sadhanapada_value = True
            values.update({
                'sp_acknowledgement': sadhanapada_value
            })
        why_sadhanapada_ids = request.httprequest.form.getlist('why_sadhanapada_id')
        if why_sadhanapada_ids:
            values.update({
                'why_sadhanapada': [(6, 0, why_sadhanapada_ids)]
            })
        sp_reference_ids = request.httprequest.form.getlist('sp_reference_id')
        if sp_reference_ids:
            values.update({
                'sp_reference': [(6, 0, sp_reference_ids)]
            })
        if kw.get('sp_receive_update'):
            sp_receive_update_value = False
            if kw.get('sp_receive_update') == 'on':
                sp_receive_update_value = True
            values.update({
                'sp_receive_updates': sp_receive_update_value
            })
        return values

    def _get_value(self, field, form_fields):
        return form_fields.get(field) if form_fields.get(field) is not None else ''
