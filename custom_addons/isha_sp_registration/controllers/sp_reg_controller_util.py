# -*- coding: utf-8 -*-
import logging

from odoo import exceptions
from odoo.http import request
from ..isha_importer import Configuration, replaceEmptyString, sso_get_full_data

_logger = logging.getLogger(__name__)
_logger.setLevel(logging.DEBUG)


def get_string_value(field, form_fields):
    return form_fields.get(field, '')


def get_numeric_value(field, form_fields):
    value = form_fields.get(field)
    return int(value) if value else None


def get_boolean_value(field, form_fields):
    return get_string_value(field, form_fields) == 'yes'


def render_error_template(error_message):
    values = {
        'error_message': error_message
    }
    return request.render("isha_sp_registration.sp_generic_exception", values, status=500)


def render_error_template_for_public_user(error_message):
    values = {
        'error_message': error_message
    }
    return request.render("isha_sp_registration.sp_generic_exception_without_web_layout", values, status=500)


def get_latest_sp_record(email):
    sp_reg = request.env['sp.registration'].sudo()
    sp_records = sp_reg.search([('sp_email', '=', email)])
    for record in sp_records:
        if record.registration_batch_status == 'current':
            return record
    raise exceptions.AccessError('Could not find record for mentioned email address. '
                                 'Please ensure pre-registration form is submitted with email address used for login.')


def upload_file(ir_attachment, file, model, field_name, record_id, is_section_save):
    existing_file = ir_attachment.search([('res_field', '=', field_name),
                                          ('res_model', '=', model),
                                          ('res_id', '=', record_id)])
    if file and file.get('data'):
        options = {
            'name': file.get('filename', ''),
            'datas': file.get('data'),
            'res_field': field_name,
            'res_model': model,
            'res_id': record_id
        }
        if existing_file.id:
            existing_file.write(options)
        else:
            ir_attachment.create(options)

    elif not existing_file.id and is_section_save:
        raise exceptions.ValidationError('Please upload file for {} field'.format(field_name))


def get_latest_sp_record_for_logged_in_user():
    _logger.debug(request.session)
    partner_id = request.env.user.partner_id
    latest_sp_record = request.env['sp.registration'].sudo().search([('ishangam_partner_pkey', '=', partner_id.id),
                                                                     ('registration_batch_status', '=', 'current')])
    if latest_sp_record.id:
        return latest_sp_record
    else:
        raise exceptions.AccessError('Could not find record for mentioned email address. '
                                     'Please ensure pre-registration form is submitted with email address used for login.')


def get_sso_email_for_logged_in_user():
    sso_config = Configuration('SSO')
    full_profile_data = sso_get_full_data(sso_config, request.env.user.login)
    if 'no_consent' not in full_profile_data and replaceEmptyString(full_profile_data['basicProfile']):
        full_profile = replaceEmptyString(full_profile_data['basicProfile'])
        return full_profile['email']
    else:
        raise exceptions.AccessError('Unable to find Single Sign-On profile')
