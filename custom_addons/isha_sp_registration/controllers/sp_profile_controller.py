# -*- coding: utf-8 -*-
import logging

from odoo import http, exceptions
from odoo.http import request, Response
from .sp_reg_controller_util import *
from ..models.constants import NUMBER_OF_PROFILE_FORM_SECTIONS

_logger = logging.getLogger(__name__)


class SpRegistrationFullForm(http.Controller):

    @http.route('/sp-profile/', auth='public', website=True)
    def get_full_form(self, **kw):
        try:
            # Auto redirect Public users to SSO Login page
            user = request.env.user
            if user.has_group('base.group_public'):
                return request.render('isha_crm.sso_auto_redirect', {})
            self._update_access_rights_for_user(user)
            latest_sp_record = get_latest_sp_record_for_logged_in_user()
            latest_full_form_record = latest_sp_record.sp_profile_id
            if not latest_full_form_record.id:
                if latest_sp_record.application_stage.id == request.env.ref(
                'isha_sp_registration.application_stage_pre_registration_stage').id:
                    error_message = 'Application review pending. Please wait for confirmation link'
                else:
                    error_message = 'Failed to get profile information. Please contact us over email/phone.'
                return render_error_template(error_message)

            values = self._get_full_form_selection_values()
            for record in latest_full_form_record.emergency_contact_ids:
                if record.emergency_type == 'primary':
                    values.update({
                        'primary_emergency_contact': record
                    })
                if record.emergency_type == 'secondary':
                    values.update({
                        'secondary_emergency_contact': record
                    })
            values.update({
                'sp_profile_record': latest_full_form_record
            })
            return request.render('isha_sp_registration.isha_sp_registration_full_form', values)
        except exceptions.AccessError as ex:
            _logger.error("SP full-form record fetch exception", exc_info=True)
            return render_error_template(ex.name)
        except Exception:
            _logger.error("SP full-form record fetch exception", exc_info=True)
            return render_error_template('Sorry something went wrong. Please try again.')

    @http.route('/sp-profile/save/', type='json', auth="user", website=True)
    def save_full_form(self, **post):
        try:
            latest_sp_record = get_latest_sp_record_for_logged_in_user()
            latest_full_form_record = latest_sp_record.sp_profile_id
            form_values = self._get_and_update_full_form_values(post, latest_full_form_record)
            if form_values:
                if self._is_section_save(post):
                    self._validate_profile_form_values(form_values)
                # Section number won't be stored in case of partial save
                elif 'section_submitted' in form_values:
                    del form_values['section_submitted']
                latest_full_form_record.write(form_values)
                return Response(status=200)
        except (exceptions.AccessError, exceptions.UserError, exceptions.ValidationError) as ex:
            _logger.error("SP full-form save exception", exc_info=True)
            raise ex
        except Exception:
            _logger.error("SP full-form save exception", exc_info=True)
            raise exceptions.UserError('Failed to save the application. Please try again or contact us over email/phone.')

    def _get_full_form_selection_values(self):
        res_partner = request.env['res.partner'].sudo()
        sp_registration = request.env['sp.registration'].sudo()
        country_ids = request.env["res.country"].sudo().search([('name', '=', 'India')]) + \
                      request.env["res.country"].sudo().search([('name', '!=', 'India')])
        values = {
            'gender_options': res_partner._fields['gender'].selection,
            'family_relation_options': request.env['isha.volunteer.family.details'].sudo()._fields[
                'relation'].selection,
            'marital_status_options': sp_registration._fields['sp_marital_status'].selection,
            'emergency_type_options': request.env['isha.volunteer.emergency.contact']
                .sudo()._fields['emergency_type'].selection,
            'country_options': country_ids,
            'state_options': request.env["res.country.state"].sudo().search([]),
            'proof_type_options': request.env['proof.type'].sudo().search([]),
            'language_options': request.env['res.lang'].sudo().search([('active', '=', True)], order="name"),
            'isha_vol_family_gender_options': request.env['isha.volunteer.family.details'].sudo()._fields['gender'].selection,
            'can_read_options': request.env['isha.volunteer.languages'].sudo()._fields['can_read'].selection,
            'can_speak_options': request.env['isha.volunteer.languages'].sudo()._fields['can_speak'].selection,
            'can_write_options': request.env['isha.volunteer.languages'].sudo()._fields['can_write'].selection,
            'can_type_options': request.env['isha.volunteer.languages'].sudo()._fields['can_type'].selection,
            'iyc_iii_location_options': request.env['isha.volunteer.main.center.volunteering'].sudo()._fields['location'].selection,
            'edu_qualification_options': request.env['educational.qualification'].sudo().search([]),
            'work_industry_options': request.env['work.industry'].sudo().search([], order="name"),
            'program_type_options': request.env['isha.programs'].sudo().search([], order="name"),
            'event_type_options': request.env['isha.event.name'].sudo().search([], order="name"),
            'yatra_type_options': request.env['isha.yatra.name'].sudo().search([], order="name"),
            'local_volunteering_activity': request.env['local.volunteering.activity'].sudo().search([], order="name"),
            'main_center_volunteering_activity': request.env['main.center.volunteering.activity'].sudo().search([],
                                                                                                        order="name"),
        }
        return values

    def _get_and_update_full_form_values(self, post, latest_full_form_record):
        section_submitted = get_numeric_value('section_number', post)
        self._ensure_valid_section(section_submitted)
        mapped_function = self._get_section_mapping(section_submitted).get('form_values')
        form_values = mapped_function(post, latest_full_form_record)
        formatted_values_list = self._format_values([form_values])
        return formatted_values_list.pop(0)

    def _get_values_for_section_one(self, post, latest_full_form_record):
        values = {
            'section_submitted': get_numeric_value('section_number', post),
            'current_name': get_string_value('currentName', post),
            'sp_marital_status': get_string_value('maritalStatus', post),
            'spouses_name': get_string_value('spouseName', post),
            'fathers_name': get_string_value('fatherName', post),
            'mothers_name': get_string_value('motherName', post),
            'height': get_numeric_value('height', post),
            'weight': get_numeric_value('weight', post),
            'religion': get_string_value('religion', post),
            'address': get_string_value('address', post),
            'sp_city': get_string_value('address-city', post),
            'sp_state': get_string_value('state', post),
            'sp_state_id': get_numeric_value('state_id', post),
            'sp_zip': get_string_value('postalCode', post),
            'sp_phone': get_string_value('phoneNumber', post),
            'sp_phone_country_code': get_string_value('phoneCountryCode', post),
            'has_name_changed': get_string_value('nameChanged', post),
            'previous_name': get_string_value('otherName', post),
            'is_mobile_same_as_phone': get_string_value('sameWhatsappNumber', post),
            'birth_country_id': get_numeric_value('birth_country_id', post),
            'is_dual_nationality': post.get('is_dual_nationality'),
            'second_nationality_country_id': get_numeric_value('second_nationality_country_id', post)
        }
        # Is same as whatApp number
        if values.get('is_mobile_same_as_phone') == 'yes':
            values.update({
                'sp_mobile': values['sp_phone'],
                'sp_mobile_country_code': values['sp_phone_country_code']
            })
        elif values.get('is_mobile_same_as_phone') == 'no':
            values.update({
                'sp_mobile': get_string_value('phoneNumberWhatsapp', post),
                'sp_mobile_country_code': get_string_value('phoneCountryCodeWhatsapp', post)
            })

        if values.get('has_name_changed') == 'yes':
            self._update_details(post, values, latest_full_form_record, 'nameChangedInGazette', 'is_gazette_updated',
                                 self._update_gazette_details)
        elif values.get('has_name_changed') == 'no':
            # Remove Gazette details
            values.update({
                'is_gazette_updated': 'no'
            })
            request.env['sp.registration.profile.gazette.details'].sudo()\
                .search([('sp_profile_id', '=', latest_full_form_record.id)]).unlink()

        # Reset fields
        if values.get('sp_marital_status') == 'unmarried':
            values.update({
                'spouses_name': ''
            })
        if values.get('is_dual_nationality') == 'no':
            values.update({
                'second_nationality_country_id': None
            })
        if values.get('has_name_changed') == 'no':
            values.update({
                'previous_name': '',
            })
        # Update one2many fields
        self._update_family_details(post, latest_full_form_record)
        self._update_nationality_details(post, latest_full_form_record)

        return values

    def _update_nationality_details(self, post, latest_full_form_record):
        full_form_id = latest_full_form_record.id
        nationality_details = request.env['sp.registration.profile.nationality.details'].sudo()
        if latest_full_form_record.sp_is_overseas:
            self._update_nationality_details_helper('primary', full_form_id, post)

        is_dual_nationality = get_boolean_value('is_dual_nationality', post)
        if is_dual_nationality and latest_full_form_record.sp_nationality.id != 104:
            self._update_nationality_details_helper('dual', full_form_id, post)
        else:
            nationality_details.search(
                [('sp_profile_id', '=', full_form_id), ('nationality_type', '=', 'dual')]).unlink()

    def _update_nationality_details_helper(self, nationality_type, full_form_id, post):
        nationality_details = request.env['sp.registration.profile.nationality.details'].sudo()
        nationality_details_list = post.get('primary_passport_details') if nationality_type == 'primary' \
                                        else post.get('dual_passport_details')

        nationality_details_list = self._format_values(nationality_details_list)
        if nationality_details_list:
            for record in nationality_details_list:
                record['sp_profile_id'] = full_form_id
            nationality_details.search([('sp_profile_id', '=', full_form_id),
                                        ('nationality_type', '=', nationality_type)]).unlink()
            nationality_details.create(nationality_details_list)
        elif self._is_section_save(post):
            raise exceptions.ValidationError('Missing values for {} nationality passport details section'
                                             .format(nationality_type))

    def _update_gazette_details(self, post, latest_full_form_record, is_gazette_updated):
        full_form_id = latest_full_form_record.id
        gazette_details = request.env['sp.registration.profile.gazette.details'].sudo()
        if is_gazette_updated == 'no':
            gazette_details.search([('sp_profile_id', '=', full_form_id)]).unlink()
            return

        gazette_details_list = self._format_values(post.get('gazette_details'))
        if gazette_details_list:
            for record in gazette_details_list:
                record['sp_profile_id'] = full_form_id
            gazette_details.search([('sp_profile_id', '=', full_form_id)]).unlink()
            gazette_details.create(gazette_details_list)
        elif self._is_section_save(post):
            raise exceptions.ValidationError('Missing values for gazette details section')

    def _update_family_details(self, post, latest_full_form_record):
        volunteer_id = latest_full_form_record['volunteer_id'].id
        family_details_list = self._format_values(post.get('family_details'))

        if family_details_list:
            # Link family details record to volunteer record
            for record in family_details_list:
                record['volunteer_id'] = volunteer_id
            family_details = request.env['isha.volunteer.family.details'].sudo()
            family_details.search([('volunteer_id', '=', volunteer_id)]).unlink()
            family_details.create(family_details_list)
        elif self._is_section_save(post):
            raise exceptions.ValidationError('Missing values for family details section')

    def _get_values_for_section_two(self, post, latest_full_form_record):
        values = {
            'section_submitted': get_numeric_value('section_number', post),
            'mother_tongue': get_numeric_value('mother_tongue', post)
        }
        self._update_language_details(post, latest_full_form_record)
        self._update_education_details(post, latest_full_form_record)
        self._update_work_experience_details(post, latest_full_form_record)
        return values

    def _update_language_details(self, post, latest_full_form_record):
        volunteer_id = latest_full_form_record['volunteer_id'].id
        details_list = self._format_values(post.get('language_details'))

        if details_list:
            for record in details_list:
                record['volunteer_id'] = volunteer_id
            language_details = request.env['isha.volunteer.languages'].sudo()
            language_details.search([('volunteer_id', '=', volunteer_id)]).unlink()
            language_details.create(details_list)
        elif self._is_section_save(post):
            raise exceptions.ValidationError('Missing values for language details section')

    def _update_education_details(self, post, latest_full_form_record):
        volunteer_id = latest_full_form_record['volunteer_id'].id
        details_list = self._format_values(post.get('education_details'))

        if details_list:
            for record in details_list:
                record['volunteer_id'] = volunteer_id
            education_details = request.env['isha.volunteer.edu.qualification'].sudo()
            education_details.search([('volunteer_id', '=', volunteer_id)]).unlink()
            education_details.create(details_list)
        elif self._is_section_save(post):
            raise exceptions.ValidationError('Missing values for education details section')

    def _update_work_experience_details(self, post, latest_full_form_record):
        volunteer_id = latest_full_form_record['volunteer_id'].id
        details_list = self._format_values(post.get('experience_details'))
        work_details = request.env['isha.volunteer.work.experience'].sudo()
        work_details.search([('volunteer_id', '=', volunteer_id)]).unlink()

        if details_list:
            for record in details_list:
                record['volunteer_id'] = volunteer_id
            work_details.create(details_list)

    def _get_values_for_section_three(self, post, latest_full_form_record):
        values = {
            'section_submitted': get_numeric_value('section_number', post),
            'physical_ailment': get_string_value('physical-ailments', post),
            'psychological_ailment': get_string_value('psychiatric-ailments', post),
            'current_medication': get_string_value('current-medication', post),
            'past_medication': get_string_value('past-medication', post),
            'mental_ailments_in_family': get_string_value('family-members', post),
            'drug_usage': get_string_value('alcohol-drugs', post),
            'drug_treatment': get_string_value('drug-treatment', post),
            'physical_disabilities': get_string_value('disabilities', post),
            'surgery_details': get_string_value('surgery', post),
            'hospitalized': get_string_value('hospitalized', post)
        }
        return values

    def _get_values_for_section_four(self, post, latest_full_form_record):
        values = {
            'section_submitted': get_numeric_value('section_number', post)
        }
        self._update_details(post, values, latest_full_form_record, 'volunteeredCenter',
                             'has_volunteered_at_main_center', self._update_center_volunteering_details)
        self._update_details(post, values, latest_full_form_record, 'volunteeredLocal',
                             'has_volunteered_at_local_center', self._update_local_volunteering_details)
        self._update_details(post, values, latest_full_form_record, 'iycProgram', 'has_completed_program',
                             self._update_program_history_details)
        self._update_details(post, values, latest_full_form_record, 'eventParticipation', 'has_attended_event',
                             self._update_event_details)
        self._update_details(post, values, latest_full_form_record, 'sacredWalk', 'has_completed_yatra',
                             self._update_yatra_details)

        self._update_values_for_other_details_section(post, latest_full_form_record, values)
        return values

    def _update_values_for_other_details_section(self, post, latest_full_form_record, values):
        values.update({
            'other_programs': get_string_value('other-practices', post),
            'convicted_crime_details': get_string_value('convicted', post),
            'incarceration_details': get_string_value('incarcerated', post),
            'id_proof_type': get_numeric_value('idProofType', post),
            'address_proof_type': get_numeric_value('addressProofType', post),
        })
        self._update_emergency_contact_details(post, latest_full_form_record)
        self._upload_attachments(post, latest_full_form_record)

    def _update_emergency_contact_details(self, post, latest_full_form_record):
        volunteer_id = latest_full_form_record['volunteer_id'].id
        details_list = self._format_values(post.get('emergency_details'))

        if details_list:
            for record in details_list:
                record['volunteer_id'] = volunteer_id
            details = request.env['isha.volunteer.emergency.contact'].sudo()
            details.search([('volunteer_id', '=', volunteer_id)]).unlink()
            details.create(details_list)
        elif self._is_section_save(post):
            raise exceptions.ValidationError('Missing values for emergency contact details section')

    def _update_center_volunteering_details(self, post, latest_full_form_record, has_volunteered_at_main_center):
        volunteer_id = latest_full_form_record['volunteer_id'].id
        details = request.env['isha.volunteer.main.center.volunteering'].sudo()
        if has_volunteered_at_main_center == 'no':
            details.search([('volunteer_id', '=', volunteer_id)]).unlink()
            return

        details_list = self._format_values(post.get('iyc_volunteering_details'))
        if details_list:
            for record in details_list:
                record['volunteer_id'] = volunteer_id
            details.search([('volunteer_id', '=', volunteer_id)]).unlink()
            details.create(details_list)
        elif self._is_section_save(post):
            raise exceptions.ValidationError('Missing values for IYC/III volunteering section')

    def _update_local_volunteering_details(self, post, latest_full_form_record, has_volunteered_at_local_center):
        volunteer_id = latest_full_form_record['volunteer_id'].id
        details = request.env['isha.volunteer.local.volunteering'].sudo()
        if has_volunteered_at_local_center == 'no':
            details.search([('volunteer_id', '=', volunteer_id)]).unlink()
            return

        details_list = self._format_values(post.get('local_volunteering_details'))
        if details_list:
            for record in details_list:
                record['volunteer_id'] = volunteer_id
            details.search([('volunteer_id', '=', volunteer_id)]).unlink()
            details.create(details_list)
        elif self._is_section_save(post):
            raise exceptions.ValidationError('Missing values for local volunteering details section')

    def _update_program_history_details(self, post, latest_full_form_record, has_completed_program):
        volunteer_id = latest_full_form_record['volunteer_id'].id
        details = request.env['isha.volunteer.program.history'].sudo()
        if has_completed_program == 'no':
            details.search([('volunteer_id', '=', volunteer_id)]).unlink()
            return

        details_list = self._format_values(post.get('program_history_details'))
        if details_list:
            for record in details_list:
                record['volunteer_id'] = volunteer_id
            details.search([('volunteer_id', '=', volunteer_id)]).unlink()
            details.create(details_list)
        elif self._is_section_save(post):
            raise exceptions.ValidationError('Missing values for program details section')

    def _update_event_details(self, post, latest_full_form_record, has_attended_event):
        volunteer_id = latest_full_form_record['volunteer_id'].id
        details = request.env['isha.volunteer.event.history'].sudo()
        if has_attended_event == 'no':
            details.search([('volunteer_id', '=', volunteer_id)]).unlink()
            return

        details_list = self._format_values(post.get('event_details'))
        if details_list:
            for record in details_list:
                record['volunteer_id'] = volunteer_id
            details.search([('volunteer_id', '=', volunteer_id)]).unlink()
            details.create(details_list)
        elif self._is_section_save(post):
            raise exceptions.ValidationError('Missing values for event details section')

    def _update_yatra_details(self, post, latest_full_form_record, has_completed_yatra):
        volunteer_id = latest_full_form_record['volunteer_id'].id
        details = request.env['isha.volunteer.yatra.history'].sudo()
        if has_completed_yatra == 'no':
            details.search([('volunteer_id', '=', volunteer_id)]).unlink()
            return

        details_list = self._format_values(post.get('yatra_details'))
        if details_list:
            for record in details_list:
                record['volunteer_id'] = volunteer_id
            details.search([('volunteer_id', '=', volunteer_id)]).unlink()
            details.create(details_list)
        elif self._is_section_save(post):
            raise exceptions.ValidationError('Missing values for sacred walk/yatra details section')

    def _update_details(self, post, values, latest_full_form_record, form_flag_field, db_field, details_func):
        if get_string_value(form_flag_field, post):
            yes_or_no_value = get_string_value(form_flag_field, post)
            values.update({
                db_field: yes_or_no_value
            })
            details_func(post, latest_full_form_record, yes_or_no_value)

    def _upload_attachments(self, post, latest_full_form_record):
        ir_attachment = request.env['ir.attachment'].sudo()
        is_section_save = self._is_section_save(post)
        upload_file(ir_attachment, post.get('photo'), 'sp.registration', 'photo',
                    latest_full_form_record.sp_reg_id.id, is_section_save)
        upload_file(ir_attachment, post.get('id_proof'), 'isha.volunteer', 'id_proof',
                    latest_full_form_record['volunteer_id'].id, is_section_save)
        upload_file(ir_attachment, post.get('address_proof'), 'isha.volunteer', 'address_proof',
                    latest_full_form_record['volunteer_id'].id, is_section_save)

    def _get_section_mapping(self, section_submitted):
        section_values_mapping = {
            1: {
                'form_values': self._get_values_for_section_one,
                'validation': self._get_mandatory_fields_for_section_one
            },
            2: {
                'form_values': self._get_values_for_section_two,
                'validation': self._get_mandatory_fields_for_section_two
            },
            3: {
                'form_values': self._get_values_for_section_three,
                'validation': self._get_mandatory_fields_for_section_three
            },
            4: {
                'form_values': self._get_values_for_section_four,
                'validation': self._get_mandatory_fields_for_section_four
            }
        }
        return section_values_mapping.get(section_submitted)

    def _validate_profile_form_values(self, form_values):
        mapped_function = self._get_section_mapping(form_values['section_submitted']).get('validation')
        mandatory_field_list = mapped_function(form_values)
        missing_field_list = []
        for mandatory_field in mandatory_field_list:
            field_value = form_values.get(mandatory_field)
            if field_value is None:
                missing_field_list.append(mandatory_field)
        if len(missing_field_list) > 0:
            missing_fields = ', '.join(missing_field_list)
            raise exceptions.ValidationError('Please enter values for following mandatory fields: '
                                             '{}'.format(missing_fields))

    def _get_mandatory_fields_for_section_one(self, form_values):
        mandatory_field_list = ['sp_marital_status', 'fathers_name', 'mothers_name', 'height',
                                'weight', 'address', 'sp_city', 'sp_zip', 'sp_phone', 'sp_phone_country_code']
        if form_values.get('has_name_changed') == 'yes':
            mandatory_field_list.append('previous_name')
        return mandatory_field_list

    def _get_mandatory_fields_for_section_two(self, form_values):
        return []

    def _get_mandatory_fields_for_section_three(self, form_values):
        return []

    def _get_mandatory_fields_for_section_four(self, form_values):
        return ['id_proof_type', 'address_proof_type']

    def _update_access_rights_for_user(self, user):
        if not user.has_group('isha_sp_registration.group_sp_portal_profile'):
            user.sudo().write({
                'groups_id': [(4, request.env.ref('isha_sp_registration.group_sp_portal_profile').id)],
            })

    def _ensure_valid_section(self, section_number):
        is_valid = 0 < section_number <= NUMBER_OF_PROFILE_FORM_SECTIONS
        if not is_valid:
            _logger.error("SP full-form: Invalid section submitted", exc_info=True)
            raise exceptions.UserError('Invalid section number')

    def _format_values(self, details_list):
        # Replace empty string with None: In case of partial save, empty string for date fields will throw exception
        filtered_list = []
        if details_list:
            for details in details_list:
                filtered_details = {k: v if v else None for k, v in list(details.items())}
                if self._is_valid_dict(filtered_details):
                    filtered_list.append(filtered_details)
        return filtered_list

    def _is_valid_dict(self, details_dict):
        return details_dict and type(details_dict) is dict and any(val for val in details_dict.values())

    def _is_section_save(self, post):
        return post.get('is_section_save')
