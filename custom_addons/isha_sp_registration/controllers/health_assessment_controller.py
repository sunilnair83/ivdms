# -*- coding: utf-8 -*-
import logging

from odoo import http, models, api
from odoo.addons.website_form.controllers.main import WebsiteForm

from werkzeug.exceptions import BadRequest

from .sp_reg_controller_util import *
from ..models.constants import *

_logger = logging.getLogger(__name__)

MODEL_PREFIX = 'sp.reg.condition.details.'
TEMPLATE_PREFIX = 'isha_sp_registration.ha_form_details_fields_'


class HealthAssessment(WebsiteForm):

    # @http.route('/ha-form/', auth='user', website=True)
    @http.route('/ha-form/', auth='public', website=True)
    def get_ha_form(self, **kw):
        try:
            user = request.env.user
            if user.has_group('base.group_public'):
                return request.render('isha_crm.sso_auto_redirect', {})
            self._update_access_rights_for_user(user)
            health_assessment_record = self._get_ha_record()

            # Create medical condition applicability record for yes/no options in HA form
            self._create_applicability_record(health_assessment_record)

            template_context = self._get_ha_form_main_selection_values()
            template_context.update({
                'ha_record': health_assessment_record
            })
            return request.render('isha_sp_registration.ha_form', template_context)
        except (exceptions.AccessError, exceptions.UserError) as ex:
            return render_error_template(ex.name)
        except Exception:
            _logger.error("Health record fetch exception", exc_info=True)
            return render_error_template('Something went wrong. Please try again.')

    @http.route('/ha-form/details/', auth='user', website=True)
    def get_details_for_health_condition(self, **kw):
        try:
            health_assessment_record = self._get_ha_record()
            if kw.get('condition_applicability_id'):
                condition_applicability_id = get_numeric_value('condition_applicability_id', kw)
                condition_applicability_record = health_assessment_record.medical_condition_applicability_ids.\
                    search([('id', '=', condition_applicability_id)])
                # Multiple records in case of surgery/hospitalization/blood transfusion
                details_record_list = self._get_details_record_from_applicability(condition_applicability_record)
                condition_details = condition_applicability_record.condition_details_id
                template = TEMPLATE_PREFIX + condition_details.category.name
                template_context = self._get_ha_form_modal_selection_values()
                template_context.update({
                    'ha_details_record': details_record_list,
                    'condition': condition_details.condition.code,
                    'condition_details': condition_details,
                    'condition_applicability_record': condition_applicability_record
                })
                return request.render(template, template_context)
            else:
                _logger.error("Health details record fetch exception", exc_info=True)
                raise exceptions.UserError('Something went wrong. Please try again.')
        except (exceptions.AccessError, exceptions.UserError) as ex:
            _logger.error("Health record fetch exception", exc_info=True)
            return render_error_template(ex.name)
        except Exception:
            _logger.error("Health record fetch exception", exc_info=True)
            return render_error_template('Something went wrong. Please try again.')

    @http.route('/ha-form/section-save/', type='json', auth="user", website=True)
    def save_ha_form(self, **post):
        try:
            self._ensure_valid_session()
            health_assessment_record = self._get_ha_record()
            if not health_assessment_record.id:
                raise exceptions.MissingError('Health assessment record not found')
            # Save HA form data
            self._save_main_form_data(health_assessment_record, post)
        except (exceptions.AccessError, exceptions.UserError, exceptions.MissingError,
                exceptions.ValidationError) as ex:
            _logger.error("SP full-form save exception", exc_info=True)
            raise ex
        except Exception:
            _logger.error("Health assessment form record save exception", exc_info=True)
            raise exceptions.UserError('Failed to save the application. '
                                         'Please try again or contact us over email/phone.')

    @http.route('/ha-form/details-save/', type='json', auth="user", website=True)
    def save_ha_details(self, **post):
        try:
            self._ensure_valid_session()
            health_assessment_record = self._get_ha_record()
            modal_form_data = post.get('modal_form_data')
            main_form_data = post.get('main_form_data')
            condition_details_id = modal_form_data.get('condition_details_id')
            condition_details = self._get_condition_details_from_id(health_assessment_record, condition_details_id)
            # Extract data
            model_name = MODEL_PREFIX + self._get_model_suffix(condition_details)
            model_reference = request.env['ir.model'].sudo().search([('model', '=', model_name)])
            if not model_reference:
                raise exceptions.UserError('Failed to save the application. '
                                         'Please try again or contact us over email/phone.')

            updated_modal_form_data = {
                'health_assessment_id': health_assessment_record.id
            }
            updated_modal_form_data.update(modal_form_data)
            record_id = get_numeric_value('record_id', modal_form_data)
            existing_details_record = None
            if record_id:
                # Id for base details model won't be sent for F.E. re-assign for existing record as it's required field
                existing_details_record = request.env[model_name].sudo().search([('id', '=', record_id)])
                existing_details_record.ensure_one()
                updated_modal_form_data.update({
                    'base_details_id': existing_details_record.base_details_id.id
                })

            data = self.extract_data(model_reference, updated_modal_form_data)
            model_values = data['record']

            if model_values:
                if existing_details_record:
                    existing_details_record.write(model_values)
                else:
                    record_id = self.insert_record(request, model_reference, model_values, None)
            if record_id and data['attachments']:
                self.upload_attachments(model_reference, int(record_id), data['attachments'])

            self._save_main_form_data(health_assessment_record, main_form_data)
            return {'id': record_id}

        except (exceptions.AccessError, exceptions.UserError) as ex:
            _logger.error("Health details record save exception", exc_info=True)
            raise ex
        except Exception:
            _logger.error("SP HA: record save exception", exc_info=True)
            raise exceptions.UserError('Failed to save the application. '
                                         'Please try again or contact us over email/phone.')

    @http.route('/ha-form/details-delete/', type='json', auth="user", website=True)
    def delete_details_record(self, **post):
        try:
            self._ensure_valid_session()
            health_assessment_record = self._get_ha_record()
            condition_details_id = post.get('condition_details_id')
            condition_details = self._get_condition_details_from_id(health_assessment_record, condition_details_id)
            # Extract data
            model_name = MODEL_PREFIX + self._get_model_suffix(condition_details)
            model_record = request.env['ir.model'].sudo().search([('model', '=', model_name)])
            if not model_record:
                raise exceptions.UserError('Failed to save the application. '
                                  'Please try again or contact us over email/phone.')

            record_id = post.get('record_id')
            if record_id:
                existing_record = request.env[model_record.model].sudo().search([('id', '=', record_id)])
                existing_record.ensure_one()
                self._delete_base_records([existing_record.base_details_id.id])
            else:
                raise exceptions.UserError('Failed to find record to delete. '
                                  'Please try again or contact us over email/phone.')

        except (exceptions.AccessError, exceptions.UserError) as ex:
            _logger.error("Health details record delete exception", exc_info=True)
            raise ex
        except Exception:
            _logger.error("SP HA: record delete exception", exc_info=True)
            raise exceptions.UserError('Failed to delete details record.')

    @http.route('/ha-form/attachment-delete/', type='json', auth="user", website=True)
    def delete_attachment(self, **post):
        try:
            self._ensure_valid_session()
            record_id = post.get('record_id')
            if record_id:
                existing_record = request.env['ir.attachment'].sudo().search([('id', '=', record_id)])
                existing_record.ensure_one()
                existing_record.unlink()
            else:
                raise exceptions.UserError('Failed to find record to delete. '
                                 'Please try again or contact us over email/phone.')
        except Exception:
            _logger.error("SP HA: attachment delete exception", exc_info=True)
            raise exceptions.UserError('Failed to delete attachment record.')

    def _get_ha_form_main_selection_values(self):
        country_options = request.env["res.country"].sudo().search([('name', '=', 'India')]) + \
                          request.env["res.country"].sudo().search([('name', '!=', 'India')])
        sibling_options = [('brother', 'Brother'), ('sister', 'Sister')]
        children_options = [('son', 'Son'), ('daughter', 'Daughter')]
        covid_condition_options = request.env['sp.reg.covid.condition.name'].sudo().search([('code', '!=', 'none')]) + \
                          request.env['sp.reg.covid.condition.name'].sudo().search([('code', '=', 'none')])
        covid_test_name_options = request.env['sp.reg.medical.condition.reports'].sudo()\
            .search([('condition_name_id', '=', request.env.ref('isha_sp_registration.medical_condition_name_covid').id)])
        gender_options = request.env['res.partner'].sudo()._fields['gender'].selection

        values = {
            'country_options': country_options,
            'sibling_options': sibling_options,
            'children_options': children_options,
            'covid_condition_options': covid_condition_options,
            'covid_test_name_options': covid_test_name_options,
            'gender_options': gender_options
        }
        return values

    def _get_ha_form_modal_selection_values(self):
        values = {
            'alcohol_types': ALCOHOL_TYPES,
            'substance_types': SUBSTANCE_TYPES,
            'time_unit': TIME_UNIT
        }
        return values

    def _update_context(self, value):
        context = request.env.context.copy()
        context.update(value)
        request.env.context = context

    def upload_attachments(self, model, details_record_id, files):
        if model and details_record_id and files:
            record_id = int(details_record_id)
            model_name = model.sudo().model
            record = model.env[model_name].sudo().browse(record_id)
            record.ensure_one()
            for file in files:
                if file.get('filename') and file.get('field_name') and file.get('data'):
                    attachment_value = {
                        'name': file.get('filename', ''),
                        'datas': file.get('data'),
                        'res_model': model_name,
                        'res_field': file.get('field_name'),
                        'description': file.get('description', ''),
                        'res_id': record_id
                    }
                    ir_attachment = request.env['ir.attachment'].sudo()
                    attachment_id = ir_attachment.create(attachment_value)

                    if attachment_id.id:
                        record.sudo()[file.get('field_name')] = [(4, attachment_id.id)]
                else:
                    raise exceptions.ValidationError('Missing mandatory fields for file upload')

    def one2many(self, field_label, field_input):
        current_ids = []
        existing_ids = []
        authorized_fields = request.env.context.get('authorized_fields')

        parent_record = request.env.context.get('parent_record')
        if parent_record:
            existing_ids = parent_record[field_label].ids

        model = request.env['ir.model'].sudo().search([('model', '=', authorized_fields[field_label]['relation'])])
        self._update_context({'authorized_fields': self.get_authorized_fields(model.sudo().model)})

        for index, value in enumerate(field_input):
            existing_record = None
            record_id = None
            if 'record_id' in value:
                record_id = get_numeric_value('record_id', value)
                del value['record_id']

            if record_id:
                existing_record = request.env[model.model].sudo().search([('id', '=', record_id)])
                self._update_context({
                    'parent_record': existing_record
                })
            data = self.extract_data(model, value)
            if data['record']:
                if existing_record and existing_record.id:
                    existing_ids.append(existing_record.id)
                    existing_record.write(data['record'])
                else:
                    record_id = self.insert_record(request, model, data['record'], None)
            if record_id:
                self.upload_attachments(model, record_id, data['attachments'])
                current_ids.append(int(record_id))

        if existing_ids:
            deleted_ids = list(set(existing_ids) - set(current_ids))
            request.env[model.model].sudo().search([('id', 'in', deleted_ids)]).unlink()
        return current_ids

    def many2many(self, field_label, field_input, *args):
        ids_list = []
        if type(field_input) is str:
            ids_list.append(field_input)
        elif type(field_input) is list:
            ids_list = field_input
        ids_list = [int(i) for i in ids_list]
        return [(6, 0, ids_list)]

    _input_filters = {
        'char': WebsiteForm.identity,
        'text': WebsiteForm.identity,
        'html': WebsiteForm.identity,
        'date': WebsiteForm.date,
        'datetime': WebsiteForm.datetime,
        'many2one': WebsiteForm.integer,
        'one2many': one2many,
        'many2many': many2many,
        'selection': WebsiteForm.identity,
        'boolean': WebsiteForm.boolean,
        'integer': WebsiteForm.integer,
        'float': WebsiteForm.floating,
        'binary': WebsiteForm.binary,
        'monetary': WebsiteForm.floating,
    }

    def _get_model_suffix(self, condition_details):
        return condition_details.category.parent_category \
            if condition_details.category.parent_category else condition_details.category.name

    def _get_details_record_from_applicability(self, condition_applicability):
        model_name_suffix = self._get_model_suffix(condition_applicability.condition_details_id)
        condition_model = MODEL_PREFIX + model_name_suffix
        return request.env[condition_model].sudo().search([
            ('condition_applicability_id', '=', condition_applicability.id)])

    def _get_condition_applicability_record_from_id(self, condition_applicability_id):
        return request.env['sp.reg.medical.condition.applicability'].sudo().search(
            [('id', '=', condition_applicability_id)])

    def _get_condition_details_from_id(self, health_assessment_record, condition_details_id):
        condition_applicability = request.env['sp.reg.medical.condition.applicability'] \
            .sudo().search([('condition_details_id', '=', int(condition_details_id)),
                            ('health_assessment_id', '=', health_assessment_record.id)])
        return condition_applicability.condition_details_id

    def _create_applicability_record(self, health_assessment_record):
        medical_conditions = request.env['sp.reg.medical.condition'].sudo().search([])
        existing_applicability_records = health_assessment_record.medical_condition_applicability_ids
        existing_applicability_condition_ids = [record.condition_details_id.id for record in existing_applicability_records]
        applicability_records = []

        for condition_details in medical_conditions:
            if condition_details.id not in existing_applicability_condition_ids:
                record = {
                    'health_assessment_id': health_assessment_record.id,
                    'condition_details_id': condition_details.id
                }
                applicability_records.append(record)

        if (len(applicability_records)):
            medical_condition_applicability = request.env['sp.reg.medical.condition.applicability'].sudo()
            medical_condition_applicability.create(applicability_records)

    def _update_condition_applicability(self, health_assessment_record, main_form_values):
        for key in main_form_values:
            if self.__is_str_int(key):
                condition_details_id = int(key)
                condition_applicability = request.env['sp.reg.medical.condition.applicability'] \
                    .sudo().search([('condition_details_id', '=', condition_details_id),
                                    ('health_assessment_id', '=', health_assessment_record.id)])
                condition_applicability.is_applicable = main_form_values.get(key)

                details_record_list = self._get_details_record_from_applicability(condition_applicability)
                # details_record_list = condition_applicability.condition_modal_details_ids
                if condition_applicability.is_applicable == 'no':
                    base_details_ids = []
                    for details_record in details_record_list:
                        base_details_ids.append(details_record.base_details_id.id)
                    self._delete_base_records(base_details_ids)

                if not self._is_partial_save(main_form_values) and condition_applicability.is_applicable == 'yes' \
                    and not details_record_list.ids:
                    raise exceptions.ValidationError('Please fill details for {}'.format(
                        condition_applicability.condition_details_id.condition.name))

    def _delete_base_records(self, base_record_ids):
        base_details_records = request.env['sp.reg.condition.details.base'].sudo() \
            .search([('id', 'in', base_record_ids)])
        base_details_records.unlink()

    def __is_str_int(self, str):
        try:
            int(str)
            return True
        except ValueError:
            return False

    def _save_main_form_data(self, health_assessment_record, main_form_values):
        self._update_condition_applicability(health_assessment_record, main_form_values)
        model_reference = request.env['ir.model'].sudo().search([('model', '=', health_assessment_record._name)])
        section_fields_list = self._get_fields_for_submitted_section(main_form_values)
        data = self.extract_data(model_reference, main_form_values, section_fields_list=section_fields_list)
        model_values = data['record']
        if model_values:
            if not self._is_partial_save(main_form_values):
                self._validate_form_values(main_form_values)
            health_assessment_record.write(model_values)
        self.upload_attachments(model_reference, health_assessment_record.id, data['attachments'])

    def _get_fields_for_submitted_section(self, main_form_values, is_for_validation=False):
        section_submitted = get_numeric_value('section_number', main_form_values)
        if self._is_valid_section(section_submitted):
            section_fields_mapping = {
                1: self._get_fields_for_section_one,
                2: self._get_fields_for_section_two,
                3: self._get_fields_for_section_three,
                4: self._get_fields_for_section_four,
                5: self._get_fields_for_section_five,
            }
        mapped_function = section_fields_mapping.get(section_submitted, None)
        return mapped_function(main_form_values, is_for_validation) if mapped_function else []

    def _get_fields_for_section_one(self, main_form_values, is_for_validation):
        field_list = ['section_number', 'family_doctor', 'family_doctor_phone_country_code',
                      'family_doctor_phone_number']
        if is_for_validation:
            field_list.remove('family_doctor')
            field_list.remove('family_doctor_phone_country_code')
            field_list.remove('family_doctor_phone_number')
        return field_list

    def _get_fields_for_section_two(self, main_form_values, is_for_validation):
        field_list = ['section_number', 'dental_ailment', 'is_ongoing_dental_treatment', 'dental_treatment',
                      'is_psy_condition_depression', 'is_psy_condition_anxiety', 'is_psy_condition_other',
                      'psy_condition_depression_details', 'psy_condition_anxiety_details', 'psy_condition_other_details',
                      'additional_medical_information', 'additional_medical_documents']
        if is_for_validation:
            if main_form_values.get('dental_ailment') == 'no':
                field_list.remove('is_ongoing_dental_treatment')
                field_list.remove('dental_treatment')
            if main_form_values.get('is_ongoing_dental_treatment') == 'no':
                field_list.remove('dental_treatment')
            if main_form_values.get('is_psy_condition_depression') == 'no':
                field_list.remove('psy_condition_depression_details')
            if main_form_values.get('is_psy_condition_anxiety') == 'no':
                field_list.remove('psy_condition_anxiety_details')
            if main_form_values.get('is_psy_condition_other') == 'no':
                field_list.remove('psy_condition_other_details')
            field_list.remove('additional_medical_information')
            field_list.remove('additional_medical_documents')
        return field_list

    def _get_fields_for_section_three(self, main_form_values, is_for_validation):
        return ['section_number', 'family_medical_history_ids']

    def _get_fields_for_section_four(self, main_form_values, is_for_validation):
        return ['section_number', 'covid_details_id']

    def _get_fields_for_section_five(self, main_form_values, is_for_validation):
        return ['section_number', 'is_declaration_terms_accepted', 'is_signature_terms_agreed', 'signature_date',
                'signature_place', 'signature']

    def _is_partial_save(self, form_values):
        is_partial_save = form_values.get('is_partial_save')
        return is_partial_save

    def extract_data(self, model, values, **kwargs):
        data = {
            'record': {},        # Values to create record
            'attachments': [],  # Attached files
        }
        # Get model fields excluding fields with domain and record update timestamp/user fields.
        # Optionally, pass list of fields to fetch, needed for section save of main HA form
        authorized_fields = self.get_authorized_fields(model.sudo().model, **kwargs)
        error_fields = []
        for field_name, field_attributes in authorized_fields.items():
            if field_name in values:
                form_value = values[field_name]
                if self._is_attachment(form_value):
                    data['attachments'].extend(self._extract_attachment_data(form_value, field_name))
                else:
                    self._reset_context(authorized_fields, values, model.sudo().model)
                    try:
                        input_filter = self._input_filters[field_attributes['type']]
                        data['record'][field_name] = input_filter(self, field_name, form_value)
                    except exceptions.UserError:
                        error_fields.append(field_name)
            elif field_attributes['type'] != 'many2many' or field_attributes['relation'] != 'ir.attachment':
                data['record'][field_name] = self._get_reset_value_for_field_type(field_attributes['type'])

        # Field validations will be performed only in case of section save
        if not self._is_partial_save(values):
            missing_required_fields = [label for label, field in authorized_fields.items() if field['required']
                                       and not label in data['record']]
            if any(error_fields):
                raise exceptions.ValidationError(error_fields + missing_required_fields)
        # Section submitted won't be updated in case of partial save
        elif data['record'].get('section_number'):
            del data['record']['section_number']
        return data

    @api.model
    def get_authorized_fields(self, model_name, **kwargs):
        """ Return the fields of the given model name as a mapping like method `fields_get`. """
        # For section saving, get fields only in current section for Health Assessment model
        section_fields_list = kwargs.get('section_fields_list')
        model = request.env[model_name]
        fields_get = model.fields_get(section_fields_list)

        # Remove magic fields
        # Remove string domains which are supposed to be evaluated
        # (e.g. "[('product_id', '=', product_id)]")
        MAGIC_FIELDS = models.MAGIC_COLUMNS + [model.CONCURRENCY_CHECK_FIELD]
        for field in list(fields_get):
            if 'domain' in fields_get[field] and isinstance(fields_get[field]['domain'], str):
                del fields_get[field]['domain']
            if field in MAGIC_FIELDS:
                del fields_get[field]

        return fields_get

    def _is_attachment(self, attachment_values):
        attachment_values_list = []
        if type(attachment_values) is list:
            attachment_values_list.extend(attachment_values)
        else:
            attachment_values_list.append(attachment_values)
        for attachment in attachment_values_list:
            if type(attachment) is not dict or 'filename' not in attachment:
                return False
        return True

    def _extract_attachment_data(self, attachment_values, field_name):
        attachment_values_list = []
        attachment_data = []
        if type(attachment_values) is list:
            attachment_values_list.extend(attachment_values)
        else:
            attachment_values_list.append(attachment_values)
        for attachment in attachment_values_list:
            attachment['field_name'] = field_name
            attachment_data.append(attachment)
        return attachment_data

    def _reset_context(self, authorized_fields, form_data, model_name):
        self._update_context({
            'authorized_fields': authorized_fields,
        })
        if form_data.get('record_id'):
            details_record = request.env[model_name].sudo().search([('id', '=', form_data.get('record_id'))])
            self._update_context({
                'parent_record': details_record
            })
        else:
            self._update_context({
                'parent_record': None
            })

    def _get_reset_value_for_field_type(self, field_type):
        values = {
            'char': '',
            'text': '',
            'html': '',
            'datetime': False,
            'date': False,
            'boolean': False,
            'many2one': None,
            'one2many': [(5, 0, 0)],
            'many2many': [(5, 0, 0)],
            'selection': '',
            'integer': 0,
            'float': 0,
            'monetary': 0,
            'binary': None
        }
        return values.get(field_type)

    def _validate_form_values(self, form_values):
        section_submitted = get_numeric_value('section_number', form_values)
        if self._is_valid_section(section_submitted):
            mandatory_field_list = self._get_fields_for_submitted_section(form_values, True)
            missing_field_list = []
            for mandatory_field in mandatory_field_list:
                field_value = form_values.get(mandatory_field)
                if field_value is None or not str(field_value).strip():
                    missing_field_list.append(mandatory_field)
            if len(missing_field_list) > 0:
                missing_fields = ', '.join(missing_field_list)
                raise exceptions.ValidationError('Please enter values for following mandatory fields: '
                                                 '{}'.format(missing_fields))

    def _is_valid_section(self, section_submitted):
        is_valid = 0 < section_submitted <= NUMBER_OF_HEALTH_FORM_SECTIONS
        if not is_valid:
            raise exceptions.ValidationError('Invalid section submitted')
        return is_valid

    def _update_access_rights_for_user(self, user):
        if not user.has_group('isha_sp_registration.group_sp_portal_health_assessment'):
            user.sudo().write({
                'groups_id': [(4, request.env.ref('isha_sp_registration.group_sp_portal_health_assessment').id)],
            })

    def _ensure_valid_session(self):
        csrf_token = request.params.pop('csrf_token', None)
        if request.session.uid and not request.validate_csrf(csrf_token):
            raise BadRequest('Session expired (invalid CSRF token)')

    def _get_ha_record(self):
        latest_sp_record = get_latest_sp_record_for_logged_in_user()
        health_assessment_record = latest_sp_record.health_assessment_id
        if not health_assessment_record.id:
            raise exceptions.UserError('Failed to get Health Accessment record')
        return health_assessment_record
