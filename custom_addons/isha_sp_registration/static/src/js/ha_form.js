odoo.define('isha_sp_registration.HAForm', function (require) {
	"use strict";

	var ajax = require('web.ajax');
	var BaseForm = require('isha_website_utils.BaseForm');
	var HAForm = BaseForm.extend({
		getWizardOptions() {
			var wizardOptions = this._super.apply(this, arguments);
			wizardOptions.labels = {
				next: 'Save & Next',
				finish: 'Submit',
				cancel: 'Save'
			};
			wizardOptions.enableCancelButton = true;
			wizardOptions.onCanceled = function (event) {
				return this._onWizardStepCancel(event);
			}.bind(this)
			return wizardOptions;
		},
		_onWizardStepCancel(event) {
			$('.loader').show();
			let $form = $('.body.current').find('form');
			let data = this.getFormData($form);
			data.is_partial_save = true;
			let url = $form.data('url');
			return ajax.jsonRpc(url, 'call', data)
				.then((response) => {
					$('.loader').hide();
					flash('Details successfully saved 🎉');
					this.resetFileInputs($form);
				})
				.guardedCatch(function (reason) {
					$('.loader').hide();
				});
		},
		_onWizardStepChanged(event, currentIndex, priorIndex) {
			if (this._isWizardFinalStep()) {
				$('[href="#cancel"]').closest('li').hide();
			} else if (!this._isFormSubmitted()) {
				$('[href="#cancel"]').closest('li').show();
			}
			this._super.apply(this, arguments)
		},
		getFormData($form) {
			var params = this._super.apply(this, arguments);
			var modalData = {};
			params['health_assessment_id'] = $("[name='health_assessment_id']").val();
			// covid_condition checkboxes should be inside covid_details_id object
			if (params.hasOwnProperty('covid_condition')) {
				params['covid_details_id'][0].covid_condition = params['covid_condition'];
				delete params['covid_condition'];
			}
			if (params.hasOwnProperty('covid_report_ids')) {
				params['covid_details_id'][0].covid_report_ids = params['covid_report_ids'];
				delete params['covid_report_ids'];
			}
			if (params.hasOwnProperty('bt_start_date')) {
				let commonSectionKeys = ['medical_condition_name', 'present_condition', 'start_date', 'last_episode', 'is_treatment_taken', 'no_treatment_reason', 'is_medication_prescribed', 'prescribed_medication_ids', 'has_undergone_surgery', 'surgery_detail_ids', 'duration', 'frequency_of_occurrence', 'symptoms', 'medical_report_ids'];
				let commonSectionData = {};
				commonSectionKeys.forEach((key) => {
					if (params[key]) {
						commonSectionData[key] = params[key];
						delete params[key];
					}
				});
				if (params['common_section_id']) {
					commonSectionData['record_id'] = params['common_section_id'];
					delete params['common_section_id'];
				}
				if (!$.isEmptyObject(commonSectionData)) {
					params['common_condition_details_ids'] = [commonSectionData];
				}
			}
			// if it is a modal form, append the main_form data
			if ($form.hasClass('modal-form')) {
				let $main_form = $('.body.current').find('form.main-form');
				let main_form_data = this.getFormData($main_form);
				main_form_data.is_partial_save = true;

				modalData['main_form_data'] = main_form_data;
				modalData['modal_form_data'] = params;
				modalData.csrf_token = odoo.csrf_token;
				return modalData;
			}
			if (this._isWizardFinalStep()) {
				params['signature_date'] = moment($('#signature-date').val(), 'DD-MM-YYYY').format('YYYY-MM-DD');
			}
			params.csrf_token = odoo.csrf_token;
			return params;
		},
		customFormValidation($form) {
			let isValid = true;
			if ($form.hasClass('main-form')) {
				$form.find('input:checked').each((index, radio) => {
					let isModalFormValid = true;
					// ignore radio fields in modal(for these form attr is set)
					if ($(radio).val() == 'yes' && !$(radio).attr('form')) {
						let $form_group = $(radio).closest('.form-group');
						let conditionCode = $form_group.find('.js-condition-code').val();
						// Which means, user hasn't opened `please fill details` modal.
						if ($form_group.find('.modal-loader').length && !$form_group.find('[name=condition_modal_details_id]').val()) {
							isModalFormValid = false;
						}
						if (conditionCode) {
							let modalFormId = `form-${conditionCode}`;
							let modalForm = $(`#${modalFormId}`);
							if (modalForm.length && !modalForm[0].checkValidity()) {
								isModalFormValid = false;
							}
						}
						if (!isModalFormValid) {
							$form_group.find('.js-fill-details-error').addClass('show');
							isValid = false;
						}
					}
				});
			}
			if (!isValid) {
				var $details_error = $('.js-fill-details-error.show').eq(0);
				$('html, body').animate({scrollTop: $details_error.offset().top - 100}, 500);
			}
			if ($form.find('#covid-conditions').length) {
				let $covidConditions = $form.find('#covid-conditions');
				if (!$covidConditions.find('input:checked').length) {
					let $invalidFeedback = $covidConditions.find('.invalid-feedback');
					$invalidFeedback.show();
					$('html, body').animate({scrollTop: $invalidFeedback.offset().top - 100}, 200);
					isValid = false;
				}
			}
			return isValid;
		},
		_cloneCarouselForm(event) {
			let $modal = $(event.target).closest('.modal');
			let condition = $modal.find('.js-condition-code').val();
			let formId = `form-${condition}`;
			let $form = $(`#${formId}`);
			if ($form[0].checkValidity()) {
				this._super.apply(this, arguments);
			} else {
				$modal[0].classList.add('was-validated');
				setTimeout(() => {
					$modal.find('input:invalid, select:invalid').eq(0).focus();
				}, 100);
			}
		},
		_deleteCarouselForm(event) {
			var $modal = $(event.target).closest('.modal');
			var $carousel_item = $(event.target).closest('.carousel-item');
			var record_id = $carousel_item.find('[name=record_id]').val();
			if (record_id) {
				let data = {
					'csrf_token': window.odoo.csrf_token,
					'condition_details_id': $modal.find('[name="condition_details_id"]').val(),
					'record_id': record_id,
				}
				$('.loader').show();
				var _super = this._super;
				ajax.jsonRpc('/ha-form/details-delete/', 'call', data)
				.then((response) => {
					$('.loader').hide();
					_super.apply(this, arguments);
				})
				.guardedCatch(function (reason) {
					$('.loader').hide();
				});
			} else {
				this._super.apply(this, arguments);
			}
		},
		_deleteAttachment(event) {
			var $deleteAttachment = $(event.target);
			var attachmentId = $deleteAttachment.data('attachment-id');
			if (attachmentId) {
				let data = {
					'csrf_token': window.odoo.csrf_token,
					'record_id': attachmentId,
				}
				$('.loader').show();
				var _super = this._super;
				ajax.jsonRpc('/ha-form/attachment-delete/', 'call', data)
				.then((response) => {
					$('.loader').hide();
					_super.apply(this, arguments);
				})
				.guardedCatch(function (reason) {
					$('.loader').hide();
				});
			} else {
				this._super.apply(this, arguments);
			}
		},
		disableForm() {
			this._super.apply(this, arguments);
			$('.js-details-link').removeClass('is-mandatory').text('View Details')
		},
		_isFormSubmitted() {
			let status = $('input[type=hidden][name="form-status"]').val();
			return ['submitted', 'ready_screening'].includes(status);
		},
	});
	var haForm = new HAForm({
		isWizardForm: true,
		hasDynamicFieldsRow: true
	});

	_.each($('.js-condition-code'), function (conditionCodeField) {
		let conditionCode = $(conditionCodeField).val();
		haForm.toggleRadioDependentFields({
			'radioSelector': `.js-${conditionCode}-radio`,
			'dependantSelector': `.js-${conditionCode}-details-link`
		});
	});

	$('.modal').on('show.bs.modal', function (e) {
		let $modal = $(e.target);
		// if ($modal.find('.modal-loader').length) {
		let condition = $modal.find('.js-condition-code').val();
		let formId = `form-${condition}`;
		if (!$(`#${formId}`).length) {
			$(document.body).append($(`<form id="${formId}" class="modal-form" data-url="/ha-form/details-save/"></form>`));
		}
		let postData = {
			'csrf_token': window.odoo.csrf_token,
			'condition_details_id': $modal.find('[name="condition_details_id"]').val(),
			'condition_applicability_id': $modal.find('[name="condition_applicability_id"]').val(),
		}
		$.ajax({
			type: "post",
			url: '/ha-form/details/',
			data: postData,
			success: function (responseData) {
				$modal.find('.modal-body-content').html(responseData);
				$modal.find('input,select,textarea').attr('form', formId);
				// To fix scroll issue for model on keyboard open in mobile
				$modal.find('input,textarea').on('focus', function(event) {
					// $modal.find('.modal-body').animate({scrollTop: $(event.target).offset().top + 50}, 300);
					window.scrollTo(0, 0);
					document.body.scrollTop = 0;
				});
				$modal.find('input[type="radio"]').each((index, radio) => {
					// To fix firefox radios required validation
					if ($(radio).prop('required')) {
						$(radio).prop('required', true);
					}
				});
				let parentSelector = `.${condition}-container`;
				haForm.toggleRadioDependentFields({
					'radioSelector': '[name=is_treatment_taken]',
					'dependantSelector': '.js-treatment-taken',
					parentSelector
				});
				haForm.toggleRadioDependentFields({
					'radioSelector': '[name=is_treatment_taken]',
					'dependantSelector': '.js-no-treatment-taken',
					'showOnSelectingNo': true,
					parentSelector
				});
				haForm.toggleRadioDependentFields({
					'radioSelector': '[name=has_undergone_surgery]',
					'dependantSelector': '.js_surgery_for_condition',
					parentSelector
				});
				haForm.toggleRadioDependentFields({
					'radioSelector': '[name=is_medication_prescribed]',
					'dependantSelector': '.js-medication-prescribed',
					parentSelector
				});
				haForm.toggleRadioDependentFields({
					'radioSelector': '[name=is_physiotherapy_taken]',
					'dependantSelector': '.js-physiotherapy-taken',
					parentSelector
				});
				haForm.toggleRadioDependentFields({
					'radioSelector': '[name=bt_had_side_effects]',
					'dependantSelector': '.js-bt-had-side-effects',
					parentSelector
				});
				haForm.toggleRadioDependentFields({
					'radioSelector': '.js-drugs-is-treatment-taken-radio',
					'dependantSelector': '.js-elaborate',
					parentSelector
				});
				haForm.initUIComponents();
				$modal.find('.carousel-item').not('.active').find('input,select').attr('disabled', '');
				if ($modal.find('.carousel')) {
					$modal.find('.carousel').on('slide.bs.carousel', function (event) {
						if (!haForm._isFormSubmitted()) {
							let $currentItem = $(event.target).find('.carousel-item.active');
						let $modal = $(this).closest('.modal');
						let condition = $modal.find('.js-condition-code').val();
						let formId = `form-${condition}`;
						let $form = $(`#${formId}`);
						let $nextItem = $(event.relatedTarget);
						$nextItem.find('input, select, textarea').attr('disabled', '');
						// We are saving here because we want to save any new attachments added.
						if (!$currentItem.hasClass('is-deleted') && $form[0].checkValidity() && !$currentItem.find('[name="record_id"]').val()) {
							haForm.saveForm($form, false).then((response) => {
								if (response) {
									$currentItem.find('[name="record_id"]').val(response.id);
								}
							});
						}
						$nextItem.find('input, select, textarea').removeAttr('disabled');
						$nextItem.find('.hide').find('input, select, textarea').attr('disabled', '');
						$currentItem.find('input,select').attr('disabled', '');
						}
					});
				}
				if (haForm._isFormSubmitted()) {
					haForm.disableForm();
					$modal.find('input, select, textarea').attr('disabled', '');
				}
			}
		});
		// }
	});

	$('.modal').on('hide.bs.modal', function (e) {
		let $modal = $(e.target);
		$modal.removeClass('was-validated');
		let condition = $modal.find('.js-condition-code').val();
		let formId = `form-${condition}`;
		haForm.resetFileInputs($(`#${formId}`));
	});

	$(document).on('click', '.btn-modal-save', function (e) {
		e.preventDefault();
		let $modal = $(this).closest('.modal');
		let condition = $modal.find('.js-condition-code').val();
		let formId = `form-${condition}`;
		let $form = $(`#${formId}`);
		if ($form[0].checkValidity()) {
			haForm.saveForm($form, false).then(() => {
				$modal.modal('hide');
				$modal.siblings('.js-fill-details-error').removeClass('show');
			});
		} else {
			$modal[0].classList.add('was-validated');
			setTimeout(() => {
				$modal.find('input:invalid, select:invalid').eq(0).focus();
			}, 100);
		}
	});

	$(document).on('click', 'input[type=radio]', function (e) {
		if ($(this).val() == 'no') {
			let $form_group = $(this).closest('.form-group');
			$form_group.find('.js-fill-details-error').removeClass('show');
		}
	});

	$(document).on('click', 'input[name=covid_condition]', function (e) {
		if ($(this)[0].checked) {
			if ($(this).data('value') == 'none') {
				$('input[name=covid_condition]').not(this).prop('checked', false);
				$('.js-covid-condition-note').addClass('hide');
			} else {
				$('input[name=covid_condition]').filter('input[data-value=none]').prop('checked', false);
				$('.js-covid-condition-note').removeClass('hide');
			}
			$(this).closest('#covid-conditions').find('.invalid-feedback').hide();
		} else {
			if ($('input[name=covid_condition]').not('input[data-value=none]').filter(':checked').length) {
				$('input[name=covid_condition]').filter('input[data-value=none]').prop('checked', false);
				$('.js-covid-condition-note').removeClass('hide');
			} else {
				$('.js-covid-condition-note').addClass('hide');
			}
		}
	});

	haForm.toggleRadioDependentFields({
		'radioSelector': '.js-is-tested-positive-radio',
		'dependantSelector': '.js-covid-positive-section, .js-covid-mandatory-reports'
	});
	haForm.toggleRadioDependentFields({
		'radioSelector': '.js-is_covid_related_complication-radio',
		'dependantSelector': '.js-complication-active'
	});
	haForm.toggleRadioDependentFields({
		'radioSelector': '.js-is-exposed-to-covid-radio',
		'dependantSelector': '.js-covid-exposure-section'
	});
	haForm.toggleRadioDependentFields({
		'radioSelector': '.js-dentalAilment-radio',
		'dependantSelector': '.js-ongoing-dental-treatment'
	});
	haForm.toggleRadioDependentFields({
		'radioSelector': '.js-isOngoingDentalTreatment-radio',
		'dependantSelector': '.js-dental-treatment-details'
	});
	haForm.toggleRadioDependentFields({
		'radioSelector': '.js-depression-radio',
		'dependantSelector': '.js-psy-depression'
	});
	haForm.toggleRadioDependentFields({
		'radioSelector': '.js-anxiety-radio',
		'dependantSelector': '.js-psy-anxiety'
	});
	haForm.toggleRadioDependentFields({
		'radioSelector': '.js-psy-others-radio',
		'dependantSelector': '.js-psy-others'
	});

	if (!$('#signature-date').val()) {
		$('#signature-date').val(moment().format('DD-MM-YYYY'));
	}
});
