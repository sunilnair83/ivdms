function confirmEmail() {
    var email = document.getElementById("email").value
    var c_email = document.getElementById("c_email").value
    if (email !== "" && c_email !== "" && email != c_email) {
        $('#confirm_email_error').show();
        $('#btnSubmit').attr('disabled', true);
    } else {
        $('#confirm_email_error').hide();
        $('#btnSubmit').attr('disabled', false);
    }
}

function showextra() {
    if (document.getElementsByName('ie_status')[0].checked) {
        document.getElementById('iepshow').style = "color:black";
        document.getElementById("iep_month").required = "required";
        document.getElementById("iep_year").required = "required";
        document.getElementById("iep_location").required = "required";
        document.getElementById("iep_teacher").required = "required";
    } else {
        document.getElementById('iepshow').style = "display:none";
        document.getElementById("iep_month").removeAttribute('required');
        document.getElementById("iep_year").removeAttribute('required');
        document.getElementById("iep_location").removeAttribute('required');
        document.getElementById("iep_teacher").removeAttribute('required');
    }
}

function ValidateDOB() {
    var birth = document.getElementById('dob');
    if (birth != "") {
        var record = document.getElementById('dob').value.trim();
        var record_day1 = record.split("-");
        var record1 = new Date(record_day1[1] + '/' + record_day1[2] + '/' + record_day1[0]);

        var currentdate = new Date();
        var day1 = currentdate.getDate();
        var month1 = currentdate.getMonth() + 1;
        var year_end = currentdate.getFullYear() - 18;
        var year_start = currentdate.getFullYear() - 100;

        var d1 = new Date(month1 + '/' + day1 + '/' + year_end);
        var d2 = new Date(month1 + '/' + day1 + '/' + year_start);

        if (record1 > d1) {
            document.getElementById('dob_error').style = "color:red;";
            $('#btnSubmit').attr('disabled', true);
            return false;
        } else {
            document.getElementById('dob_error').style = "display:none";
            $('#btnSubmit').attr('disabled', false);
        }
        /*if (record1 < d2) {
            document.getElementById('dob_error').style = "color:red;";
            $('#btnSubmit').attr('disabled', true);
            return false;
        }*/
    }
}
$(function() {
    ValidatePhone();
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();

    if (month < 10)
        month = '0' + month.toString();
    if (day < 10)
        day = '0' + day.toString();

    var maxDate = year + '-' + month + '-' + day;
    $('#dob').attr('max', maxDate);

    // Checkbox required for input groups
    var allRequiredCheckboxes = $(':checkbox[required]');
    var checkboxNames = [];

    for (var i = 0; i < allRequiredCheckboxes.length; ++i) {
        var name = allRequiredCheckboxes[i].name;
        checkboxNames.push(name);
    }

    checkboxNames = checkboxNames.reduce(function(p, c) {
        if (p.indexOf(c) < 0) p.push(c);
        return p;
    }, []);

    for (var i in checkboxNames) {
        ! function() {
            var name = checkboxNames[i];
            var checkboxes = $('input[name="' + name + '"]');
            checkboxes.change(function() {
                if (checkboxes.is(':checked')) {
                    checkboxes.removeAttr('required');
                } else {
                    checkboxes.attr('required', 'required');
                }
            });
        }();
    }
});

function ValidatePhone() {
    if ($('#phone_country_code').val() == '91') {
        $('#phone').attr('maxlength', '10')
        $('#phone').attr('minlength', '10')
        $('#phone').attr('pattern', '[0-9]+')
    } else {
        $('#phone').attr('maxlength', '15')
        $('#phone').attr('pattern', '[1-9]{1}[0-9]+')
    }
}

odoo.define('sp_registration', function(require) {
    "use strict";
    $(document).ready(function() {
        var rpc = require('web.rpc');
        // NOTE: This code exists to prevent duplicate registrations with same email. There is a technical blocker in implementation.
        // TODO: Uncomment this once we figure out how to make rpc calls work for a public user.
        $('#email').on('blur', function(event) {
            var $email_input = $(event.target);
            var value = $email_input.val()
                // Ref: https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
            var email_regex = /^\S+@\S+$/;
            if (email_regex.test(value)) {
                rpc.query({
                    route: '/pre_registration/check_email/',
                    params: {
                        'email': value,
                    },
                }).then(function(result) {
                    if (result) {
                        document.getElementById('email_exists_error').style = "color:red;";
                        $('#btnSubmit').attr('disabled', true);
                    } else {
                        document.getElementById('email_exists_error').style = "display: none;";
                        $('#btnSubmit').attr('disabled', false);
                    }
                });
            }
        });
    });
});
