odoo.define('isha_sp_registration.FullForm', ['isha_website_utils.BaseForm', 'web.ajax'], function (require) {
	"use strict";
	
	var ajax = require('web.ajax');
	var BaseForm = require('isha_website_utils.BaseForm');
	var FullForm = BaseForm.extend({
		getWizardOptions() {
			var wizardOptions = this._super.apply(this, arguments);
			wizardOptions.labels = {
				next: 'Save & Next',
				finish: 'Submit',
				cancel: 'Save'
			};
			wizardOptions.enableCancelButton = true;
			wizardOptions.onCanceled = function (event) {
				return this._onWizardStepCancel(event);
			}.bind(this)
			return wizardOptions;
		},
		_onWizardStepCancel(event) {
			$('.loader').show();
			let $form = $('.body.current').find('form');
			let data = this.getFormData($form);
			data.is_section_save = false;
			// delete data['section_number'];
			let url = $form.data('url');
			return ajax.jsonRpc(url, 'call', data)
				.then((response) => {
					$('.loader').hide();
					flash('Details successfully saved 🎉');
				})
				.guardedCatch(function (reason) {
					$('.loader').hide();
				});
		},
		getFormData($form) {
			var params = this._super.apply(this, arguments);
			var alwaysSendInPayloadFields = ['#address-city', '#address-state'];
			alwaysSendInPayloadFields.forEach((fieldSelector) => {
				let $field = $(fieldSelector);
				if ($field.val() && $form.find(fieldSelector).length && !$field.hasClass('hide')) {
					params[$field.attr('name')] = $(fieldSelector).val();
				}
			});
			if (params.emergency_details) {
				let secondary_details = params.emergency_details[1];
				if (!secondary_details.name) {
					params.emergency_details = [params.emergency_details[0]];
				}
			}
			params.is_section_save = true;
			params.csrf_token = odoo.csrf_token;
			return params;
		},
		saveForm($form, isWizardSave = true) {
			if($('input[type=hidden][name="form-status"]').val() == 'submitted') {
				return false;
			}
			return this._super.apply(this, arguments);
		},
		showWizardSubmitConfirmation() {
			$('#saveConfirmation').modal('show');
			$('.btn-modal-save').unbind().on('click', function() {
				this.saveForm($('.body.current').find('form'));
			}.bind(this));
		}
	});
	var fullForm = new FullForm({
		isWizardForm: true,
		hasDynamicFieldsRow: true
	});

	fullForm.toggleRadioDependentFields({'radioSelector': '[name=nameChanged]','dependantSelector': '.js-name-changed-dependents'});
	fullForm.toggleRadioDependentFields({'radioSelector': '[name=nameChangedInGazette]','dependantSelector': '#gazette-details'});
	fullForm.toggleRadioDependentFields({'radioSelector': '[name=volunteeredCenter]','dependantSelector': '#volunteering-details'});
	fullForm.toggleRadioDependentFields({'radioSelector': '[name=volunteeredLocal]','dependantSelector': '#local-volunteering-details'});
	fullForm.toggleRadioDependentFields({'radioSelector': '[name=iycProgram]','dependantSelector': '#program-details'});
	fullForm.toggleRadioDependentFields({'radioSelector': '[name=eventParticipation]','dependantSelector': '#event-details'});
	fullForm.toggleRadioDependentFields({'radioSelector': '[name=sacredWalk]','dependantSelector': '#sacred-walk-details'});
	fullForm.toggleRadioDependentFields({'radioSelector': '.js-sameWhatsappNumber-radio','dependantSelector': '.js-whatsapp-phone', 'showOnSelectingNo': true});
	fullForm.toggleRadioDependentFields({'radioSelector': '[name=is_dual_nationality]','dependantSelector': '#secondary-passport-details'});
	fullForm.toggleRadioDependentFields({'radioSelector': '[name=is_dual_nationality]','dependantSelector': '#second-nationality-country'});

	$('input[name="postalCode"]').on('blur', async function () {
		var postalCode = $(this).val();
		if (postalCode) {
			let countryCode = $('#country-of-residence option:selected').attr('data-code');
			let address_details = await fetch(`https://cdi-gateway.isha.in/contactinfovalidation/api/countries/${countryCode}/pincodes/${postalCode}`)
			let address_details_json = await address_details.json()
			// We are matching with name becuase state id is different in API and in options
			let stateId = $(`#address-state option[data-name="${address_details_json.state}"]`).val();
			let $state_select = $('#state-select-input');
			let $address_city = $('input[name="address-city"]');
			let $state_text = $('#state-text-input');
			if (stateId) {
				$state_select.removeClass('hide').find('select').removeClass('hide');
				$state_text.addClass('hide').find('input').attr('disabled', 'true');
				$state_select.find('select').val(stateId).trigger('change');
				if (address_details_json.defaultcity) {
					$address_city.val(address_details_json.defaultcity);
					$address_city.attr('disabled', 'true');
				} else {
					$address_city.removeAttr('disabled');
				}
			} else {
				$state_text.removeClass('hide').find('input').removeAttr('disabled');
				$state_text.find('input').val('');
				$address_city.val('');
				$address_city.removeAttr('disabled');
				$state_select.addClass('hide').find('select').addClass('hide');
			}
		}
	});

	$('.marital-status-select').on('change', function(event) {
		if ($(this).val() == 'unmarried') {
			$('.js-spouse-name').addClass('hide');
			$('.js-spouse-name input').attr('disabled', '');
		} else {
			$('.js-spouse-name').removeClass('hide');
			$('.js-spouse-name input').removeAttr('disabled');
		}
	});

	$('[name=is_dual_nationality]').on('change', function(event) {
		if ($(this).val() == 'yes' && $('#nationality').find('option[selected="true"]').data('code') != 'IN') {
			$('#secondary-passport-details').show();
			$('#secondary-passport-details').removeAttr('disabled');
			$('#secondary-passport-details').find('input,select').removeAttr('disabled');
			$('#second-nationality-country').show();
			$('#second-nationality-country').removeAttr('disabled');
			$('#second-nationality-country').find('input,select').removeAttr('disabled');
		} else {
			$('#secondary-passport-details').hide();
			$('#secondary-passport-details').attr('disabled', '');
			$('#secondary-passport-details').find('input,select').attr('disabled', '');
			$('#second-nationality-country').hide();
			$('#second-nationality-country').attr('disabled', '');
			$('#second-nationality-country').find('input,select').attr('disabled', '');
		}
	});
});
