function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() {
            defer(method)
        }, 50);
    }
}
var jspgremstatecoll = [];
var jspgremcountrycoll = [];
defer(jspgmsatsangdocumentready);

function jspgmsatsangdocumentready() {
    $(document).ready(function() {
        jsspregfmwtsapp();
        jspgmreginitfunction();
        jsspregfmselectstates();
        jsspregfmcountryandcodechange();
        jsspregfmcountrypincode();
        jsspregfmcountryphonecode();
        jsspregfmwatsappcountryphonecode();
        jspgmregsetlangpref();
    });
}

function jspgmregsetlangpref() {
    try {
        var paramsvars = new URLSearchParams(location.search);
        var langvar = paramsvars.get('language').toUpperCase()
        $("select#jsspregfmpreflang option").each(function() {
            if ($(this).text().toUpperCase() == langvar) {
                $("select#jsspregfmpreflang").val($(this).val());
                return false;
            }
        });
    } catch (e) {}
}

function jspgmreginitfunction() {
    var initObjvar;
    var addobjvar;
    $('#jsspregfmstate option').each(function(ivar, jvar) {
        var objvar = {
            "valueattr": $(jvar).attr("value"),
            "statenameattr": $(jvar).attr("statename")
        };
        jspgremstatecoll.push(objvar);
    });
}

function jsspregfmselectstates() {
    var state = document.getElementById("jsspregfmstate");
    var countryname = document.getElementById("jsspregfmcountry");
    var countryid = countryname.value;
    var idvar = 0;
    Array.from(state.options).forEach(item => {
        var trend = item.getAttribute("countryid");
        if (trend == countryid) {
            item.style.display = "block";
            item.disabled = false;
            if (idvar == 0)
                idvar = item.index;
        } else {
            if (trend == 0) {
                item.style.display = "block";
                item.disabled = false;
            } else {
                item.style.display = "none";
                item.disabled = true;
            }
        }
    });
    if (idvar != 0) {
        // state.options.selectedIndex = idvar;
        state.options.selectedIndex = 0;
        state.disabled = false;
        state.height = 35;
        document.getElementById('jsspregfmstate').style = "";
        document.getElementById("jsspregfmstate").required = "required";
        document.getElementById('stateshow').style = "display:none";
        document.getElementById("stateshow").removeAttribute('required');
    } else if (idvar == 0) {
        state.options.selectedIndex = 0;
        state.disabled = true;
        document.getElementById('jsspregfmstate').style = "display:none";
        document.getElementById("jsspregfmstate").removeAttribute('required');
        document.getElementById('stateshow').style = "";
        document.getElementById("stateshow").required = "required";
    }
}

function jsspregfmwtsapp() {
    $('#jsspregfmwtsappyes').change(function() {
        if ($(this).is(":checked")) {
            $('.jsspregfmwtsappcls').css("display", "none");
            $(this).val('YES');
            //$('#jsspregfmwtsappcountrycode').prop('required', false);
            //$('#jsspregfmwtsappphone').prop('required', false);
        }
    });
    $('#jsspregfmwtsappno').change(function() {
        if ($(this).is(":checked")) {
            $('.jsspregfmwtsappcls').css("display", "block");
            $(this).val('NO');
            //$('#jsspregfmwtsappcountrycode').prop('required', true);
            //$('#jsspregfmwtsappphone').prop('required', true);
        }
    });
}

function jsspregfmcountryandcodechange() {
    $('#jsspregfmcountry').change(function() {
        jsspregfmcountrypincode()
    });

    $('#jsspregfmcountrycode').change(function() {
        jsspregfmcountryphonecode()
    });

    $('#jsspregfmwtsappcountrycode').change(function() {
        jsspregfmwatsappcountryphonecode()
    });
}

function jsspregfmwatsappcountryphonecode() {
    if ($('#jsspregfmwtsappcountrycode').val() == '91') {
        $('#jsspregfmwtsappphone').attr('maxlength', '10')
        $('#jsspregfmwtsappphone').attr('minlength', '10')
        $('#jsspregfmwtsappphone').attr('pattern', '[0-9]+')
    } else {
        $('#jsspregfmwtsappphone').attr('maxlength', '15')
        $('#jsspregfmwtsappphone').attr('minlength', '5')
        $('#jsspregfmwtsappphone').attr('pattern', '[1-9]{1}[0-9]+')
    }
}

function jsspregfmcountrypincode() {
    if ($('#jsspregfmcountry').val() == '104') {
        $('#jsspregfmpincode').attr('maxlength', '6')
        $('#jsspregfmpincode').attr('minlength', '6')
        $('#jsspregfmpincode').attr('pattern', '[1-9]{1}[0-9]+')
    } else {
        $('#jsspregfmpincode').attr('maxlength', '10')
        $('#jsspregfmpincode').attr('minlength', '4')
        $('#jsspregfmpincode').attr('pattern', '[A-Za-z0-9]*')
    }
}

function jsspregfmcountryphonecode() {
    if ($('#jsspregfmcountrycode').val() == '91') {
        $('#jsspregfmphone').attr('maxlength', '10')
        $('#jsspregfmphone').attr('minlength', '10')
        $('#jsspregfmphone').attr('pattern', '[0-9]+')
    } else {
        $('#jsspregfmphone').attr('maxlength', '15')
        $('#jsspregfmphone').attr('minlength', '5')
        $('#jsspregfmphone').attr('pattern', '[1-9]{1}[0-9]+')
    }
}

function jspgmsatsangfmvalidateform() {
    if (validateemptystring("#jsspregfmfirstname")) {
        alert($('#jsspregfmfirstnamewarning').val())
        return false;
    }
    if (validateemptystring("#jsspregfmlastname")) {
        alert($('#jsspregfmlastnamewarning').val())
        return false;
    }
    return true; //jsspregfmvalidateiswatsapp();
}

function jsspregfmvalidateiswatsapp() {
    if (!$('#jsspregfmwtsappyes').is(':checked') && !$('#jsspregfmwtsappno').is(':checked')) {
        //alert($('#jsspregfmwhatsappwarning'.val()))
        return false;
    }
    return true;
}

function validateemptystring(element) {
    return $.trim($(element).val()) == "";
}

$(document).ready(function() {

    document.getElementById('sp_registration_form').reset();

    $('#jsspregfmpincode').focusout(async() => {
        let selected_country = $('#jsspregfmcountry option:selected').text();
        let selected_country_iso2_code = $('#jsspregfmcountry option:selected').attr("ccode");
        let pincode = $('#jsspregfmpincode').val();
        pincode = pincode.trim();
        if (pincode != "") {
            let address_details = await fetch(`https://cdi-gateway.isha.in/contactinfovalidation/api/countries/${selected_country_iso2_code}/pincodes/${pincode}`)
            let address_details_json = await address_details.json();
            if (!_.isEmpty(address_details_json)) {
                let statevar = _.filter(jspgremstatecoll, (a) => a.statenameattr == address_details_json.state)[0]
                $('#jsspregfmstate').val(statevar["valueattr"]);
                $('#jsspregfmcity').val(address_details_json.defaultcity);
                document.getElementById('jsspregfmstate').style = "";
                document.getElementById("jsspregfmstate").required = "required";
                document.getElementById('stateshow').style = "display:none";
                document.getElementById("stateshow").removeAttribute('required');
            } else {
                $('#jsspregfmstate').val('');
                $('#jsspregfmcity').val('');
                document.getElementById('jsspregfmstate').style = "display:none";
                document.getElementById("jsspregfmstate").removeAttribute('required');
                document.getElementById('stateshow').style = "";
                document.getElementById("stateshow").required = "required";
            }
        } else {
            $('#jsspregfmstate').val('');
            $('#jsspregfmcity').val('');
            document.getElementById('jsspregfmstate').style = "display:none";
            document.getElementById("jsspregfmstate").removeAttribute('required');
            document.getElementById('stateshow').style = "";
            document.getElementById("stateshow").required = "required";
        }
    });
})