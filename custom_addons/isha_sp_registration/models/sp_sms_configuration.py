from odoo import fields, models, _


class SpSMSConfiguration(models.Model):
    _name = 'sp.sms.configuration'
    _description = 'SMS Configuration details'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = "configuration_name"
    _order = 'configuration_name DESC'

    _sql_constraints = [
        ("unique_configuration_name", "unique(configuration_name)", "Configuration name must be unique!")
    ]

    configuration_name = fields.Char("Configuration Name", required=True, default='SMS Country API')
    # url = 'http://api.smscountry.com/smscwebservices_bulk_reports.aspx?'
    provider_url = fields.Char("Provider API URL", required=True)
    provider_username = fields.Char("Provider UserName", required=True)
    provider_password = fields.Char("Provider Password", required=True)
    sender_id = fields.Char("Sender ID", required=True)
    provider_url_for_status = fields.Char("Provider API URL for Delivery Status")
    # message_type = fields.Char("Message Type", required=True,default="N")
    # Delivery_report_yn = fields.Char("Delievery Report Required", required=True,default="Y")
