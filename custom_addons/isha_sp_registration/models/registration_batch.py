# -*- coding: utf-8 -*-
from odoo import models, fields, api


class RegistrationBatch(models.Model):
    _name = 'registration.batch'
    _description = 'Registration Batch'

    _sql_constraints = [('name_unique', 'UNIQUE (name)', 'You can not have two batch with the same name!')]

    name = fields.Char(string='Batch', required=True)
    status = fields.Selection([('current', 'Current'),
                               ('alumni', 'Alumni'),
                               ('future', 'Future')], string='Status', default='current')
    active = fields.Boolean(string='Active', default=True)

    @api.depends('name', 'status')
    def name_get(self):
        res = []
        for record in self:
            if record.name and record.status:
                name = record.name + ' [' + record.status + ']'
            elif record.name:
                name = record.name
            res.append((record.id, name))
        return res

    @api.constrains('status')
    def _change_status(self):
        for record in self:
            if record.status and record.status == 'current':
                record.search([('id', '!=', record.id), ('status', '=', 'current')]).status = 'alumni'
                return {
                    'type': 'ir.actions.client',
                    'tag': 'reload'
                }
