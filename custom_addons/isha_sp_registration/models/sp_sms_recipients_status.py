from odoo import fields, models


class SpSMSRecipientsStatus(models.Model):
    _name = 'sp.sms.recipients.status'
    _description = 'SMS Recipients Status'
    _order = 'mobile DESC'

    sms_recipient_id = fields.Many2one('sp.sms.recipients', 'Recipient Details')
    # sms_detail_id = fields.Char();
    sp_reg_ids = fields.Many2one('sp.registration', string='SP Registration ID')
    mobile = fields.Char(string="Mobile")
    status_date = fields.Datetime(string='Status Date', default=fields.Datetime.now())
    response = fields.Char('Job Id')
    status = fields.Selection([
        ('0', 'Message In Queue'),
        ('1', 'Submitted To Carrier'),
        ('2', 'Un Delivered'),
        ('3', 'Delivered'),
        ('4', 'Expired'),
        ('8', 'Rejected'),
        ('9', 'Message Sent'),
        ('10', 'Opted Out Mobile Number'),
        ('11', 'Invalid Mobile Number'),
    ], 'Delivery Status', default='0')
