# -*- coding: utf-8 -*-

from odoo import models, fields


class SpInterviewerQuestion(models.Model):
    _name = 'sp.interviewer.question'
    _description = 'SP Interviewer Question'

    _sql_constraints = [('question_unique', 'UNIQUE (name)', 'Question already exists!')]

    name = fields.Char(string='Question', required=True)
    active = fields.Boolean(string='Active', default=True)
    is_mandatory = fields.Boolean()
