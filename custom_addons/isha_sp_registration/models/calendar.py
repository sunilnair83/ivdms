# -*- coding: utf-8 -*-

import datetime
import json
import logging

from lxml import etree

from odoo import models, fields, api
from odoo.exceptions import UserError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


class MeetingType(models.Model):
    _inherit = 'calendar.event.type'

    active = fields.Boolean(default=True)


class CalendarEvent(models.Model):
    _inherit = 'calendar.event'

    interviewer_id = fields.Many2one('sp.interviewer')
    appointment_ids = fields.One2many('sp.appointment', 'time_slot')
    slot_state = fields.Selection([('available', 'Available'),
                                   ('availed', 'Availed'),
                                   ('unavailed', 'Unavailed'),
                                   ('unavailable', 'Unavailable'),
                                   ('booked', 'Booked')], default='available', string='Slot Status')

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        if self._context.get('appointment'):
            if order and len(order) > 0:
                order = order + ", " + 'start asc'
            else:
                order = 'start asc'
        return super(CalendarEvent, self)._search(args, offset=offset, limit=limit, order=order, count=count,
                                                  access_rights_uid=access_rights_uid)

    def name_get(self):
        res = [(rec.id, rec.name) for rec in self]
        if self._context.get('appointment'):
            res = []
            for rec in self:
                if isinstance(rec.start, str):
                    start = fields.Datetime.context_timestamp(self, datetime.datetime.strptime(
                        rec.start, DEFAULT_SERVER_DATETIME_FORMAT)).replace(tzinfo=None)
                else:
                    start = (fields.Datetime.context_timestamp(self, rec.start)).replace(tzinfo=None)
                start_strftime = start.strftime("%d/%m/%Y %I:%M:%S %p")
                name = ''
                if rec.interviewer_id:
                    if rec.interviewer_id.name:
                        name = rec.interviewer_id.name
                res += [(rec.id, '%s - [%s]' % (name, start_strftime))]
        return res

    @api.model
    def sp_appointment_calendar_event_action(self):
        context = {
            "appointment": True,
            "default_filter_partner_ids_all": True,
            "search_default_filter_partner_ids_all": True
        }
        domain = [('interviewer_id', '!=', False)]

        if self.env.user.has_group('isha_sp_registration.group_sp_appointment_interviewer'):
            context.update({'search_default_my_slots': 1})

        search_view_id = self.env.ref('isha_sp_registration.view_calendar_appointment_slot').id
        form_view_id = self.env.ref('isha_sp_registration.view_sp_calendar_event_form').id
        tree_view_id = self.env.ref('isha_sp_registration.view_calendar_event_tree_appointment').id
        calendar_view_id = self.env.ref('calendar.view_calendar_event_calendar').id

        return {
            'type': 'ir.actions.act_window',
            'res_model': 'calendar.event',
            'name': "Slot",
            'view_mode': 'calendar,tree,form',
            'views': [
                (calendar_view_id, 'calendar'),
                (tree_view_id, 'tree'),
                (form_view_id, 'form'),
            ],
            'search_view_id': search_view_id,
            'target': 'current',
            'context': context,
            'domain': domain,
        }

    @api.model
    def create(self, values):
        res = super(CalendarEvent, self).create(values)
        if self._context.get('appointment'):
            if not res.interviewer_id:
                for partner_id in res.partner_ids:
                    res.interviewer_id = self.env['sp.interviewer'].sudo().search(
                        [('user_id', 'in', partner_id.user_ids.ids)], limit=1)
            if res.interviewer_id:
                # res.user_id = res.interviewer_id.user_id
                res.attendee_ids.unlink()
                values = {
                    'partner_id': res.interviewer_id.user_id.partner_id.id,
                    'email': res.interviewer_id.user_id.email,
                    'event_id': res.id,
                    'state': 'accepted',
                }
                self.env['calendar.attendee'].create(values)
        return res

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        result = super(CalendarEvent, self).fields_view_get(view_id=view_id, view_type=view_type,
                                                            toolbar=toolbar, submenu=submenu)

        is_interview_scheduler = self.user_has_groups('isha_sp_registration.group_sp_appointment_interview_scheduler')

        # For scheduler, make interviewer_id and start_datetime editable and non editable for Interviewer.
        if view_type == 'form':
            doc = etree.XML(result['arch'])
            for node in doc.xpath("//field"):
                name = node.get('name')
                if name == 'interviewer_id' or name == 'start_datetime':
                    if node.attrib.get('modifiers'):
                        attr = json.loads(node.attrib.get('modifiers'))
                        if is_interview_scheduler:
                            attr['readonly'] = 0
                        node.set('modifiers', json.dumps(attr))
            result['arch'] = etree.tostring(doc)
        return result

    @api.model
    def default_get(self, fields):
        res = super(CalendarEvent, self).default_get(fields)

        if self.is_only_interviewer():
            interviewer = self.env['sp.interviewer'].search([('user_id', '=', self.env.user.id)])
            res.update({
                'interviewer_id': interviewer.id
            })
        return res

    def is_only_interviewer(self):
        is_interview_scheduler = self.user_has_groups('isha_sp_registration.group_sp_appointment_interview_scheduler')
        is_interviewer = self.user_has_groups('isha_sp_registration.group_sp_appointment_interviewer')

        return is_interviewer and not (is_interview_scheduler)

    def write(self, values):
        for record in self:
            if self.is_only_interviewer() and not record._context.get('allow_calendar_edits', False) and \
                record.interviewer_id and record.interviewer_id.user_id.id != self.env.user.id:
                _logger.info("Interviewer id :- %s. record id :- %s", str(record.interviewer_id), str(record))
                raise UserError("You cannot edit another user's slot.")
            # if values.get('interviewer_id'):
            #     interviewer_id = self.env['sp.interviewer'].browse(values.get('interviewer_id'))
            #     values['user_id'] = interviewer_id.user_id.id
        res = super(CalendarEvent, self).write(values)
        for record in self:
            record.attendee_ids.unlink()
            values = {
                'partner_id': record.interviewer_id.user_id.partner_id.id,
                'email': record.interviewer_id.user_id.email,
                'event_id': record.id,
                'state': 'accepted',
            }
            self.env['calendar.attendee'].create(values)
        return res

    def unlink(self):
        for record in self:
            if self.is_only_interviewer() and \
                record.interviewer_id and record.interviewer_id.user_id.id != self.env.user.id:
                raise UserError("You cannot delete another user's slot.")
        return super(CalendarEvent, self).unlink()

    def action_select_slot(self):
        print("action_select_slot")

    def action_detach_recurring_event(self):
        if self.interviewer_id:
            view_id = self.env.ref('isha_sp_registration.view_sp_calendar_event_form').id
            meeting = self.detach_recurring_event()
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'calendar.event',
                'view_mode': 'form',
                'view_id': view_id,
                'res_id': meeting.id,
                'target': 'current',
                'flags': {'form': {'action_buttons': True, 'options': {'mode': 'edit'}}}
            }
        else:
            return super(CalendarEvent, self).action_detach_recurring_event()
