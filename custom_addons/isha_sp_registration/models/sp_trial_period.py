# -*- coding: utf-8 -*-

import datetime

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class SpTrialPeriod(models.Model):
    _name = 'sp.reg.trial.period'
    _description = 'SP Trial Period'

    sp_interview_id = fields.Many2one('sp.interview.form', string='Applicant', delegate=True, required=True,
                                      readonly=True)
    start_date = fields.Datetime(string='Trial start date', readonly=True)
    end_date = fields.Datetime(string='Trial end date', readonly=True)
    group_meeting_date = fields.Datetime()
    individual_meeting_date = fields.Datetime()
    vro_checked_in_date = fields.Datetime()
    vro_checked_out_date = fields.Datetime()
    trial_decision = fields.Selection([('approved', 'Approved'), ('rejected', 'Rejected'), ('pending', 'Pending')],
                                      string="Trial period status", default='pending', track_visibility="onchange")

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(SpTrialPeriod, self).fields_get(allfields, attributes=attributes)
        if res.get('low_matches_count'):
            res['low_matches_count']['searchable'] = False
        if res.get('sp_interview_status'):
            res['sp_interview_status']['searchable'] = False
        if res.get('interview_status'):
            res['interview_status']['searchable'] = False
        return res

    @api.model
    def create(self, values):
        return super(SpTrialPeriod, self).create(values)

    def write(self, values):
        # todo temp change to bypass vro approval and check-in
        if 'trial_period_status' in values and values['trial_period_status'] == 'dates_received':
            values['trial_period_status'] = 'vro_checked_in'
            values['vro_checked_in_date'] = values['start_date']
        return super(SpTrialPeriod, self).write(values)

    @api.constrains('group_meeting_date')
    def _validate_group_meeting_date(self):
        for record in self:
            if not record.group_meeting_date or record.group_meeting_date < record.vro_checked_in_date \
                or record.group_meeting_date > record.end_date:
                raise ValidationError("Group meeting should be between Check-in date and Trial End date. "
                                      "Please set a valid group meeting date")
            record.trial_period_status = 'gp_meeting_scheduled'

    @api.constrains('individual_meeting_date')
    def _validate_individual_meeting_date(self):
        for record in self:
            if not record.individual_meeting_date or record.individual_meeting_date < record.vro_checked_in_date \
                or record.individual_meeting_date > record.end_date:
                raise ValidationError("Individual meeting should be between Check-in date and Trial End date. "
                                      "Please set a valid individual meeting date")
            record.trial_period_status = 'ind_meeting_scheduled'

    def mark_as_group_meeting_completed(self):
        for record in self:
            if record.trial_period_status == 'gp_meeting_scheduled':
                record.trial_period_status = 'gp_meeting_completed'
            else:
                raise ValidationError('Marking as group meeting completed is not allowed '
                                      'without scheduling a group meeting.')

    def button_mark_as_dropped_out(self):
        return {
            'name': 'Mark as Dropped-Out',
            'view_mode': 'form',
            'res_model': 'interview.comments.wizard',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': {
                'trial_drop_out': True,
            },
            'target': 'new',
        }

    def button_update_individual_feedback(self):
        return {
            'name': 'Add individual meeting feedback',
            'view_mode': 'form',
            'res_model': 'interview.comments.wizard',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': {
                'individual_meeting_feedback': True,
            },
            'target': 'new',
        }

    def get_view_on_trial_decision_button(self):
        return {
            'name': 'Update Trial Decision',
            'view_mode': 'form',
            'res_id': self.id,
            'res_model': 'sp.reg.trial.period',
            'view_id': self.env.ref('isha_sp_registration.update_trial_decision').id,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    def update_trial_decision(self):
        return

    def cancel_registration(self):
        return {
            'name': 'Cancel Application',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'cancellation.wizard',
            'view_id': False,
            'target': 'new',
            'context': {
                'trial_period': True,
                'sp_trial_id': self.id,
            },
        }
