# -*- coding: utf-8 -*-

import datetime
import logging
import threading

from dateutil.relativedelta import relativedelta

from odoo import api, fields, models, registry, SUPERUSER_ID
from odoo.exceptions import ValidationError
from odoo.tools.misc import split_every
from . import constants

_logger = logging.getLogger(__name__)


class SpRegistrationHealthAssessment(models.Model):
    _name = 'sp.reg.health.assessment'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'generic.mixin.track.changes']
    _description = "SP Registration Health assessment"
    _rec_name = 'sp_name'
    _order = 'is_application_stage_cancelled asc, is_reviewer_decision_rejected asc,' \
             'application_processing_priority_sequence asc, ha_submission_date asc'

    section_number = fields.Integer(string='Health assessment form section submitted', default=0, readonly=True)
    # SP registration fields
    sp_reg_id = fields.Many2one('sp.registration', string='Name', required=True, delegate=True,
                                ondelete='cascade')
    # SP Interview Form
    interview_form_id = fields.Many2one('sp.interview.form', string='SP Interview Form')

    # SP Profile fields
    sp_reg_profile_id = fields.Many2one('sp.registration.profile', string='SP Full Form')
    photo = fields.Binary(string="Profile picture", related="sp_reg_profile_id.photo", readonly=True)
    height = fields.Integer(related="sp_reg_profile_id.height", readonly=True)
    weight = fields.Integer(related="sp_reg_profile_id.weight", readonly=True)
    bmi = fields.Float(related="sp_reg_profile_id.bmi", readonly=True, string='BMI')
    bmi_indicator = fields.Selection(related="sp_reg_profile_id.bmi_indicator", readonly=True, string='BMI Indicator')
    medical_comments = fields.Text(related="sp_reg_profile_id.medical_comments", readonly=True)

    physical_ailment = fields.Text(related="sp_reg_profile_id.physical_ailment", readonly=True)
    psychological_ailment = fields.Text(related="sp_reg_profile_id.psychological_ailment", readonly=True)
    mental_ailments_in_family = fields.Text(related="sp_reg_profile_id.mental_ailments_in_family", readonly=True)
    current_medication = fields.Text(related="sp_reg_profile_id.current_medication", readonly=True)
    past_medication = fields.Text(related="sp_reg_profile_id.past_medication", readonly=True)
    drug_usage = fields.Text(related="sp_reg_profile_id.drug_usage", readonly=True)
    drug_treatment = fields.Text(related="sp_reg_profile_id.drug_treatment", readonly=True)
    surgery_details = fields.Text(related="sp_reg_profile_id.surgery_details", readonly=True)
    physical_disabilities = fields.Text(related="sp_reg_profile_id.physical_disabilities", readonly=True)
    hospitalized = fields.Text(related="sp_reg_profile_id.hospitalized", readonly=True)

    # Health assessment fields
    family_doctor = fields.Char(string='Family Doctor Name', readonly=True)
    family_doctor_phone_country_code = fields.Char(string='Family Doctor Contact country code', readonly=True)
    family_doctor_phone_number = fields.Char(string='Family Doctor Contact number', readonly=True)

    dental_ailment = fields.Selection(list(constants.BOOLEAN_VALUES.items()), readonly=True)
    is_ongoing_dental_treatment = fields.Selection(list(constants.BOOLEAN_VALUES.items()), readonly=True,
                                                   string="Are you presently on any treatment which requires further follow ups?")
    dental_treatment = fields.Text(string='Dental treatment details', readonly=True)

    is_psy_condition_depression = fields.Selection(list(constants.BOOLEAN_VALUES.items()), string='Is/was depressed?',
                                                   readonly=True)
    psy_condition_depression_details = fields.Text(string='Depression details', readonly=True)
    is_psy_condition_anxiety = fields.Selection(list(constants.BOOLEAN_VALUES.items()), string='Has/had anxiety?',
                                                readonly=True)
    psy_condition_anxiety_details = fields.Text(string='Anxiety details', readonly=True)
    is_psy_condition_other = fields.Selection(list(constants.BOOLEAN_VALUES.items()),
                                              string='Has/had any other psychological condition?', readonly=True)
    psy_condition_other_details = fields.Text(string='Other psychological condition details', readonly=True)

    additional_medical_information = fields.Text(string='Any other medical information', readonly=True)
    additional_medical_documents = fields.Many2many('ir.attachment', string='Additional medical reports', readonly=True)

    family_medical_history_ids = fields.One2many('sp.reg.condition.details.family', 'health_assessment_id')
    covid_details_id = fields.One2many('sp.reg.condition.details.covid', 'health_assessment_id')

    # Declaration section
    is_declaration_terms_accepted = fields.Selection(list(constants.BOOLEAN_VALUES.items()),
                                                     string='Are terms and condition accepted?', readonly=True)
    is_signature_terms_agreed = fields.Selection(list(constants.BOOLEAN_VALUES.items()), readonly=True)
    signature_date = fields.Date(readonly=True)
    signature_place = fields.Char(readonly=True)
    signature = fields.Char(readonly=True)

    medical_condition_applicability_ids = fields.One2many('sp.reg.medical.condition.applicability',
                                                          'health_assessment_id')

    # Section 1 applicability for template mapping
    head_neck_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                        string='Head and Neck',
                                        domain=[('condition_details_id.condition.code', '=', 'head-and-neck')])
    eyes_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id', string='Eyes',
                                   domain=[('condition_details_id.condition.code', '=', 'eyes')])
    nose_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                   string='Nose ailment details',
                                   domain=[('condition_details_id.condition.code', '=', 'nose')])
    ears_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                   string='Ears ailment details',
                                   domain=[('condition_details_id.condition.code', '=', 'ears')])
    chest_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                    string='Chest ailment details',
                                    domain=[('condition_details_id.condition.code', '=', 'chest')])
    stomach_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                      string='Stomach ailment details',
                                      domain=[('condition_details_id.condition.code', '=', 'stomach')])
    intestines_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                         string='Intestines ailment details',
                                         domain=[('condition_details_id.condition.code', '=', 'intestines')])
    bowel_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                    string='Bowel ailment details',
                                    domain=[('condition_details_id.condition.code', '=', 'bowel')])
    skin_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                   string='Skin ailment details',
                                   domain=[('condition_details_id.condition.code', '=', 'skin')])
    circulation_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                          string='Circulation ailment details',
                                          domain=[('condition_details_id.condition.code', '=', 'circulation')])
    urinary_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                      string='Urinary ailment details',
                                      domain=[('condition_details_id.condition.code', '=', 'urinary-symptoms')])
    menstrual_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                        string='Menstrual Disorders details',
                                        domain=[('condition_details_id.condition.code', '=', 'menstrual-disorders')])

    # Section 2 applicability for template mapping
    knee_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                   string='Knee ailment details',
                                   domain=[('condition_details_id.condition.code', '=', 'knee')])
    neck_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                   string='Neck ailment details',
                                   domain=[('condition_details_id.condition.code', '=', 'neck')])
    spine_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                    string='Spine or Back ailment details',
                                    domain=[('condition_details_id.condition.code', '=', 'spine-or-back')])
    ankle_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                    string='Ankle ailment details',
                                    domain=[('condition_details_id.condition.code', '=', 'ankle')])
    hip_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                  string='Hip ailment details',
                                  domain=[('condition_details_id.condition.code', '=', 'hip')])
    shoulder_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                       string='Shoulder ailment details',
                                       domain=[('condition_details_id.condition.code', '=', 'shoulder')])
    muscle_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                     string='Muscle ailment details',
                                     domain=[('condition_details_id.condition.code', '=', 'muscle')])
    other_joints_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                           string='Other joints ailment details',
                                           domain=[('condition_details_id.condition.code', '=', 'joints-other')])
    epilepsy_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                       string='Epilepsy/Seizure (Fits) ailment details',
                                       domain=[('condition_details_id.condition.code', '=', 'epilepsy-seizure')])
    diabetes_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                       string='Diabetes',
                                       domain=[('condition_details_id.condition.code', '=', 'diabetes')])
    heart_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                    string='Heart', domain=[('condition_details_id.condition.code', '=', 'heart')])
    lungs_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                    string='Lungs', domain=[('condition_details_id.condition.code', '=', 'lungs')])
    anemia_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                     string='Anemia', domain=[('condition_details_id.condition.code', '=', 'anemia')])
    hypertension_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                           string='Hypertension',
                                           domain=[('condition_details_id.condition.code', '=', 'hypertension')])
    surgery_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                      string='Surgery',
                                      domain=[('condition_details_id.condition.code', '=', 'surgery')])
    hospitalization_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                              string='Hospitalization',
                                              domain=[('condition_details_id.condition.code', '=', 'hospitalization')])
    allergy_medications_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                                  string='Medications',
                                                  domain=[('condition_details_id.condition.code', '=',
                                                           'allergy-medications')])
    allergy_food_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                           string='Food',
                                           domain=[('condition_details_id.condition.code', '=', 'allergy-food')])
    allergy_environmental_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                                    string='Environmental',
                                                    domain=[('condition_details_id.condition.code', '=',
                                                             'allergy-environmental')])
    allergy_chemical_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                               string='Chemical',
                                               domain=[
                                                   ('condition_details_id.condition.code', '=', 'allergy-chemical')])
    allergy_anaphylaxis_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                                  string='Anaphylaxis',
                                                  domain=[('condition_details_id.condition.code', '=',
                                                           'allergy-anaphylaxis')])
    allergy_others_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                             string='Others',
                                             domain=[('condition_details_id.condition.code', '=', 'allergy-others')])
    blood_transfusion = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                        string='Blood Transfusion',
                                        domain=[('condition_details_id.condition.code', '=', 'blood-transfusion')])
    asthma_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id', string='Asthma',
                                     domain=[('condition_details_id.condition.code', '=', 'asthma')])
    eczema_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id', string='Eczema',
                                     domain=[('condition_details_id.condition.code', '=', 'eczema')])
    childhood_illness_others = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                               string='Others',
                                               domain=[('condition_details_id.condition.code', '=',
                                                        'childhood_illness_others')])
    pain_and_discomfort = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                          string='Pain & Discomfort',
                                          domain=[('condition_details_id.condition.code', '=', 'pain_and_discomfort')])
    sleep_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id', string='Sleep',
                                    domain=[('condition_details_id.condition.code', '=', 'sleep')])
    hearing_loss_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                           string='Hearing Loss',
                                           domain=[('condition_details_id.condition.code', '=', 'hearing_loss')])
    vision_loss_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                          string='Vision Loss',
                                          domain=[('condition_details_id.condition.code', '=', 'vision_loss')])
    physical_disability_ailment = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                                                  string='Physical Disability',
                                                  domain=[('condition_details_id.condition.code', '=',
                                                           'physical_disability')])
    alcohol = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                              string='Do you drink alcohol?',
                              domain=[('condition_details_id.condition.code', '=', 'alcohol')])
    tobacco = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                              string='Do you use tobacco/smoke?',
                              domain=[('condition_details_id.condition.code', '=', 'tobacco')])
    drugs = fields.One2many('sp.reg.medical.condition.applicability', 'health_assessment_id',
                            string='Have you ever used any of the intoxicants/ substances/  drugs/  (Marijuana, LSD etc.)',
                            domain=[('condition_details_id.condition.code', '=', 'drugs')])

    # Family history for template mapping
    father = fields.One2many('sp.reg.condition.details.family', 'health_assessment_id',
                             domain=[('relation', '=', 'father')])
    mother = fields.One2many('sp.reg.condition.details.family', 'health_assessment_id',
                             domain=[('relation', '=', 'mother')])
    grandmother_maternal = fields.One2many('sp.reg.condition.details.family', 'health_assessment_id',
                                           domain=[('relation', '=', 'grandmother_maternal')])
    grandfather_maternal = fields.One2many('sp.reg.condition.details.family', 'health_assessment_id',
                                           domain=[('relation', '=', 'grandfather_maternal')])
    grandmother_paternal = fields.One2many('sp.reg.condition.details.family', 'health_assessment_id',
                                           domain=[('relation', '=', 'grandmother_paternal')])
    grandfather_paternal = fields.One2many('sp.reg.condition.details.family', 'health_assessment_id',
                                           domain=[('relation', '=', 'grandfather_paternal')])
    siblings = fields.One2many('sp.reg.condition.details.family', 'health_assessment_id',
                               domain=[('relation', 'in', ['brother', 'sister'])])
    children = fields.One2many('sp.reg.condition.details.family', 'health_assessment_id',
                               domain=[('relation', 'in', ['daughter', 'son'])])

    # Details fields for backend view
    def _get_allergy_name_list(self):
        return [self.env.ref('isha_sp_registration.medical_condition_name_medications').id,
                self.env.ref('isha_sp_registration.medical_condition_name_food').id,
                self.env.ref('isha_sp_registration.medical_condition_name_chemical').id,
                self.env.ref('isha_sp_registration.medical_condition_name_anaphylaxis').id,
                self.env.ref('isha_sp_registration.medical_condition_name_allergy_other').id,
                self.env.ref('isha_sp_registration.medical_condition_name_environmental').id]

    common_details_ids = fields.One2many(
        'sp.reg.condition.details.common', 'health_assessment_id', domain=lambda self:
        [('condition_applicability_id.is_applicable', '=', 'yes'),
         ('condition_applicability_id.condition_details_id.category.name', '=', 'common'),
         ('condition_applicability_id.condition_details_id.condition.id', 'not in', self._get_allergy_name_list())])

    allergy_details_ids = fields.One2many(
        'sp.reg.condition.details.common', 'health_assessment_id', domain=lambda self:
        [('condition_applicability_id.is_applicable', '=', 'yes'),
         ('condition_applicability_id.condition_details_id.condition.id', 'in', self._get_allergy_name_list())])

    joints_details_ids = fields.One2many(
        'sp.reg.condition.details.common', 'health_assessment_id',
        domain=[('condition_applicability_id.is_applicable', '=', 'yes'),
                ('condition_applicability_id.condition_details_id.category.name', '=', 'joints')])
    bloodtransfusion_details_ids = fields.One2many(
        'sp.reg.condition.details.bloodtransfusion', 'health_assessment_id',
        domain=[('condition_applicability_id.is_applicable', '=', 'yes')])
    surgery_details_ids = fields.One2many(
        'sp.reg.condition.details.surgery', 'health_assessment_id',
        domain=[('condition_applicability_id.is_applicable', '=', 'yes')])
    hospitalization_details_ids = fields.One2many(
        'sp.reg.condition.details.hospitalization', 'health_assessment_id',
        domain=[('condition_applicability_id.is_applicable', '=', 'yes')])
    alcohol_details_ids = fields.One2many(
        'sp.reg.condition.details.alcohol', 'health_assessment_id',
        domain=[('condition_applicability_id.is_applicable', '=', 'yes')])
    tobacco_details_ids = fields.One2many(
        'sp.reg.condition.details.tobacco', 'health_assessment_id',
        domain=[('condition_applicability_id.is_applicable', '=', 'yes')])
    substance_details_ids = fields.One2many(
        'sp.reg.condition.details.substance', 'health_assessment_id',
        domain=[('condition_applicability_id.is_applicable', '=', 'yes')])

    # Covid fields for backend view
    is_tested_positive = fields.Selection(related='covid_details_id.is_tested_positive')
    is_exposed_to_covid = fields.Selection(related='covid_details_id.is_exposed_to_covid')
    covid_condition = fields.Many2many(related='covid_details_id.covid_condition')
    additional_details = fields.Text(related='covid_details_id.additional_details')
    # Details if tested positive for covid
    infection_date = fields.Date(related='covid_details_id.infection_date')
    is_covid_related_complication = fields.Selection(related='covid_details_id.is_covid_related_complication')
    is_complication_active = fields.Selection(related='covid_details_id.is_complication_active')
    treatment_details = fields.Text(related='covid_details_id.treatment_details')
    present_condition = fields.Text(related='covid_details_id.present_condition')
    covid_report_ids = fields.Many2many(related='covid_details_id.covid_report_ids')
    # Details if exposed to covid
    exposure_date = fields.Date(related='covid_details_id.exposure_date')
    measures_after_exposure = fields.Text(related='covid_details_id.measures_after_exposure')

    # HA approvals
    screening_doctor_decision = fields.Selection([('approved', 'Approved'),
                                                  ('rejected', 'Rejected'),
                                                  ('sp_to_decide', 'SP to Decide'),
                                                  # ('evaluate_after_trial', 'Evaluate after trial')
                                                  ], readonly=1)
    review_doctor_decision = fields.Selection([('approved', 'Approved'),
                                               ('rejected', 'Rejected'),
                                               ('sp_to_decide', 'SP to Decide'),
                                               # ('evaluate_after_trial', 'Evaluate after trial')
                                               ], 'Doctor Approval Decision', readonly=1)
    any_highlights_for_seva = fields.Selection(list(constants.BOOLEAN_VALUES.items()),
                                               string='Any highlights for SP Team', default='no')
    ha_additional_values_ids = fields.Many2many('ha.additional.values', string="HA Additional Values")
    seva_team_feedback_ids = fields.One2many('registration.feedback', 'health_assessment_id', ondelete='cascade',
                                             string='Comments for SP Team', domain=lambda self:
        [('feedback_type', '=', self.env.ref('isha_sp_registration.feedback_type_screen_doc_seva_team_comments').id)])
    ha_x_css = fields.Html(string='CSS', sanitize=False, compute='_ha_compute_css', store=False)
    show_in_progress_button = fields.Boolean(compute="_compute_show_in_progress_button")
    show_on_hold_button = fields.Boolean(compute="_compute_show_on_hold_button")
    show_ready_for_doctor_approval = fields.Boolean(compute="_compute_show_ready_for_doctor_approval")

    def send_email(self):
        ctx = dict()
        ctx.update({
            'default_composition_mode': 'mass_mail',
            'default_partner_to': self.partner_id.ids or '',
            'default_use_template': True,
            'default_email_from': '"Sadhanapada - Isha Clinic" <ishanandasg@gmail.com>',
            'default_email_to': [record.sp_email for record in self],
            'default_model': self._name,
            'active_ids': self.ids
        })
        return {
            'name': 'Send Email',  # Label
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_id': self.env.ref('isha_sp_registration.sp_health_compose_message_wizard_form').id,
            'target': 'new',
            'res_model': 'mail.compose.message',
            'context': ctx
        }

    doctor_feedback_ids = fields.One2many(
        'registration.feedback', 'health_assessment_id', ondelete='cascade',
        domain=lambda self: [
            '|',
            ('feedback_type', '=', self.env.ref('isha_sp_registration.feedback_type_review_doctor_comments').id),
            ('feedback_type', '=', self.env.ref('isha_sp_registration.feedback_type_screening_doctor_comments').id)])

    @api.constrains('any_highlights_for_seva')
    def _constrains_any_highlights_for_seva(self):
        for record in self:
            if self.any_highlights_for_seva == 'no':
                record.seva_team_feedback_ids.sudo().unlink()

    @api.onchange('tested_covid_positive')
    def _onchange_tested_covid_positive(self):
        for record in self:
            if record.tested_covid_positive == 'no':
                record.covid_positive_tested_date = None

    @api.constrains('tested_covid_positive', 'covid_positive_tested_date')
    def _validate_tested_positive_date(self):
        for record in self:
            if record.tested_covid_positive == 'yes' and not record.covid_positive_tested_date:
                raise ValidationError('Please enter valid date for covid positive testing')
            if record.tested_covid_positive == 'no':
                record.covid_positive_tested_date = None
            if record.tested_covid_positive == 'yes' and record.covid_positive_tested_date and record.covid_positive_tested_date > datetime.date.today():
                raise ValidationError("'Covid Positive Tested Date' can not be future date")

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        if self._context.get('my_ha_form_filter'):
            if self.env.user.has_group('isha_sp_registration.group_sp_ha_doctor_approval'):
                args = args + ['|', ('ha_screening_doctor', '=', self.env.uid),
                               ('ha_doctor_approval', '=', self.env.uid)]
            elif self.env.user.has_group('isha_sp_registration.group_sp_ha_screening_doctor'):
                args = args + [('ha_screening_doctor', '=', self.env.uid)]
        return super(SpRegistrationHealthAssessment, self).search(args, offset, limit, order, count)

    def write(self, values):
        if values.get('section_number') == constants.NUMBER_OF_HEALTH_FORM_SECTIONS:
            if self.interview_form_state == "ready_review":
                if self.sp_health_assessment_form_status != 'ready_screening':
                    values['sp_health_assessment_form_status'] = 'ready_screening'
                if self.sp_health_assessment_state != 'ready_screening':
                    values['sp_health_assessment_state'] = 'ready_screening'
            else:
                if self.sp_health_assessment_form_status != 'form_submitted':
                    values['sp_health_assessment_form_status'] = 'form_submitted'
                if self.sp_health_assessment_state != 'form_submitted':
                    values['sp_health_assessment_state'] = 'form_submitted'
        if values.get('section_number') and 0 < values.get('section_number') < constants.NUMBER_OF_HEALTH_FORM_SECTIONS:
            if self.sp_health_assessment_form_status != 'partial':
                values['sp_health_assessment_form_status'] = 'partial'
            if self.sp_health_assessment_state != 'partial':
                values['sp_health_assessment_state'] = 'partial'
        res = super(SpRegistrationHealthAssessment, self).write(values)
        return res

    # @api.constrains('section_number')
    # def _on_section_submitted_change(self):
    #     for record in self:
    #         if record.section_number == constants.NUMBER_OF_HEALTH_FORM_SECTIONS:
    #             if record.sp_health_assessment_form_status != 'form_submitted':
    #                 record.sp_health_assessment_form_status = 'form_submitted'
    #             if record.sp_health_assessment_state != 'form_submitted':
    #                 record.sp_health_assessment_state = 'form_submitted'
    #         if 0 < record.section_number < constants.NUMBER_OF_HEALTH_FORM_SECTIONS:
    #             if record.sp_health_assessment_form_status != 'partial':
    #                 record.sp_health_assessment_form_status = 'partial'
    #             if record.sp_health_assessment_state != 'partial':
    #                 record.sp_health_assessment_state = 'partial'

    def _compute_show_in_progress_button(self):
        for record in self:
            if record.is_application_stage_cancelled:
                record.show_in_progress_button = False
            elif record.sp_health_assessment_form_status in ['ready_screening', 'screening_on_hold']:
                record.show_in_progress_button = True
            elif record.sp_health_assessment_form_status in ['ready_review', 'review_on_hold']:
                if self.env.user.has_group('isha_sp_registration.group_sp_ha_doctor_approval'):
                    record.show_in_progress_button = True
                else:
                    record.show_in_progress_button = False
            else:
                record.show_in_progress_button = False

    def _compute_show_on_hold_button(self):
        for record in self:
            if record.is_application_stage_cancelled:
                record.show_on_hold_button = False
            elif record.sp_health_assessment_form_status in ['screening_in_progress']:
                record.show_on_hold_button = True
            elif record.sp_health_assessment_form_status in ['review_in_progress']:
                if self.env.user.has_group('isha_sp_registration.group_sp_ha_doctor_approval'):
                    record.show_on_hold_button = True
                else:
                    record.show_on_hold_button = False
            else:
                record.show_on_hold_button = False

    def _compute_show_ready_for_doctor_approval(self):
        for record in self:
            if record.is_application_stage_cancelled or not record.screening_doctor_decision:
                record.show_ready_for_doctor_approval = False
            elif record.sp_health_assessment_form_status in ['screening_in_progress', 'screening_on_hold']:
                record.show_ready_for_doctor_approval = True
            else:
                record.show_ready_for_doctor_approval = False

    def _ha_compute_css(self):
        for rec in self:
            if self.env.user.has_group('isha_sp_registration.group_sp_registration_admin'):
                rec.ha_x_css = None
            elif self.env.user.has_group('isha_sp_registration.group_sp_ha_screening_doctor') or \
                self.env.user.has_group('isha_sp_registration.group_sp_ha_doctor_approval') or \
                self.env.user.has_group('isha_sp_registration.group_sp_ha_manager'):
                rec.ha_x_css = '<style>.o_form_button_edit {display: none !important;}</style>'
            else:
                rec.ha_x_css = None

    def button_add_comment(self):
        ctx = {}
        if self.env.user.has_group('isha_sp_registration.group_sp_ha_doctor_approval'):
            ctx.update({
                'review_doctor_comment': True,
            })
        if self.env.user.has_group('isha_sp_registration.group_sp_ha_screening_doctor'):
            ctx.update({
                'screening_doctor_comment': True,
            })
        return {
            'name': 'Add Doctor\'s Comment',
            'view_mode': 'form',
            'res_model': 'interview.comments.wizard',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': ctx,
            'target': 'new',
        }

    def button_in_progress(self):
        # If already moved to 'In-Progress', just refresh the page
        if self.sp_health_assessment_form_status in ['screening_in_progress', 'review_in_progress']:
            return {
                'type': 'ir.actions.client',
                'tag': 'reload'
            }
        if self.sp_health_assessment_form_status in ['ready_screening', 'screening_on_hold']:
            self.sudo().write(
                {
                    'sp_health_assessment_form_status': 'screening_in_progress',
                    'sp_health_assessment_state': 'screening_in_progress',
                    'ha_screening_doctor_datetime': fields.Datetime.now(),
                    'ha_screening_doctor': self.env.uid,
                }
            )
        if self.sp_health_assessment_form_status in ['ready_review', 'review_on_hold']:
            self.sudo().write(
                {
                    'sp_health_assessment_form_status': 'review_in_progress',
                    'sp_health_assessment_state': 'review_in_progress',
                    'ha_doctor_approval_datetime': fields.Datetime.now(),
                    'ha_doctor_approval': self.env.uid,
                }
            )

    def button_on_hold(self):
        # If already moved to 'On-Hold', just refresh the page
        if self.sp_health_assessment_form_status in ['screening_on_hold', 'review_on_hold']:
            return {
                'type': 'ir.actions.client',
                'tag': 'reload'
            }
        if self.sp_health_assessment_form_status == 'screening_in_progress':
            self.sudo().write(
                {
                    'sp_health_assessment_form_status': 'screening_on_hold',
                }
            )
        if self.sp_health_assessment_form_status == 'review_in_progress':
            self.sudo().write(
                {
                    'sp_health_assessment_form_status': 'review_on_hold',
                }
            )

    def screening_comment(self, screening_doctor_decision, sp_health_assessment_state):
        seva_last_comment = self.env['registration.feedback'].search([
            ('feedback_type', '=', self.env.ref('isha_sp_registration.feedback_type_screen_doc_seva_team_comments').id),
            ('health_assessment_id', '=', self.id)], limit=1, order="id desc")
        ctx = {
            'screening_response': True,
            'screening_doctor_decision': screening_doctor_decision,
            # 'sp_health_assessment_form_status': sp_health_assessment_form_status,
            'sp_health_assessment_state': sp_health_assessment_state,
            'default_health_assessment_id': self.id,
            'default_is_feedback': True if self.seva_team_feedback_ids.ids else False,
            'default_any_highlights_for_seva': self.any_highlights_for_seva,
            'default_ha_additional_values_ids': self.ha_additional_values_ids.ids,
            'default_extra_comments': seva_last_comment.value,
        }
        return {
            'name': 'Screening Doctor\'s Comment',
            'view_mode': 'form',
            'res_model': 'interview.comments.wizard',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': ctx,
            'target': 'new',
        }

    def _check_ready_review(self):
        # If already moved to 'Ready-Review', just show User Error
        if self.sp_health_assessment_form_status == 'ready_review':
            raise ValidationError("This application was already moved to 'Ready for Review' by another Interviewer")

    def action_screening_approved(self):
        self._check_ready_review()
        return self.screening_comment('approved', 'screening_approved')

    def action_screening_rejected(self):
        self._check_ready_review()
        return self.screening_comment('rejected', 'screening_rejected')

    def action_screening_sp_to_decide(self):
        self._check_ready_review()
        return self.screening_comment('sp_to_decide', 'screening_sp_to_decide')

    def action_screening_evaluate_after_trial(self):
        return self.screening_comment('evaluate_after_trial', 'screening_evaluate_after_trial')

    def button_ready_for_doctor_approval(self):
        self.sudo().write({'sp_health_assessment_form_status': 'ready_review'})

    def reviewer_comment(self, review_doctor_decision, sp_health_assessment_form_status, sp_health_assessment_state):
        return {
            'name': 'Doctor Approval Comment',
            'view_mode': 'form',
            'res_model': 'interview.comments.wizard',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': {
                'review_response': True,
                'review_doctor_decision': review_doctor_decision,
                'sp_health_assessment_form_status': sp_health_assessment_form_status,
                'sp_health_assessment_state': sp_health_assessment_state,
            },
            'target': 'new',
        }

    def action_review_approved(self):
        return self.reviewer_comment('approved', 'reviewed', 'review_approved')

    def action_review_rejected(self):
        return self.reviewer_comment('rejected', 'reviewed', 'review_rejected')

    def action_review_sp_to_decide(self):
        return self.reviewer_comment('sp_to_decide', 'reviewed', 'review_sp_to_decide')

    def action_review_evaluate_after_trial(self):
        return self.reviewer_comment('evaluate_after_trial', 'evaluate_after_trial', 'review_evaluate_after_trial')

    def cancel_registration(self):
        return {
            'name': 'Cancel Application',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'cancellation.wizard',
            'view_id': False,
            'target': 'new',
        }

    def _notify_record_by_email(self, message, recipients_data, msg_vals=False,
                                model_description=False, mail_auto_delete=True, check_existing=False,
                                force_send=True, send_after_commit=True,
                                **kwargs):
        """ Method to send email linked to notified messages.

        :param message: mail.message record to notify;
        :param recipients_data: see ``_notify_thread``;
        :param msg_vals: see ``_notify_thread``;

        :param model_description: model description used in email notification process
          (computed if not given);
        :param mail_auto_delete: delete notification emails once sent;
        :param check_existing: check for existing notifications to update based on
          mailed recipient, otherwise create new notifications;

        :param force_send: send emails directly instead of using queue;
        :param send_after_commit: if force_send, tells whether to send emails after
          the transaction has been committed using a post-commit hook;
        """
        partners_data = [r for r in recipients_data['partners'] if r['notif'] == 'email']
        if not self._context.get('incoming_mail_server', None):
            # Sending mail from Portal directly with the email id as we don't create res_partner
            Mail = self.env['mail.mail'].sudo()
            mail_subject = message.subject or (
                message.record_name and 'Re: %s' % message.record_name)  # in cache, no queries
            # prepare notification mail values
            base_mail_values = {
                'mail_message_id': message.id,
                'mail_server_id': message.mail_server_id.id,
                # 2 query, check acces + read, may be useless, Falsy, when will it be used?
                'auto_delete': mail_auto_delete,
                # due to ir.rule, user have no right to access parent message if message is not published
                'references': message.parent_id.sudo().message_id if message.parent_id else False,
                'subject': mail_subject,
            }

            previous_mail = self.env['mail.message'].sudo().search(
                [('id', '!=', message.id), ('res_id', '=', self.id), ('model', '=', self._name)]).sorted(
                key=lambda m: m.create_date, reverse=True)
            base_mail_values = self._notify_by_email_add_values(base_mail_values)
            prev_body = ''
            if len(previous_mail) >= 1:
                prev_body = previous_mail[0].body
                base_mail_values['subject'] = previous_mail[0].subject
            message.body = message.body + '<br/><br/><blockquote style="margin:0px 0px 0px 0.8ex; padding-left:1ex" data-o-mail-quote="1">' + prev_body + '</blockquote>'
            mail_body = message.body
            mail_body = self._replace_local_links(mail_body)

            create_values = {
                'body_html': mail_body,
                'subject': mail_subject,
                'email_to': self.sp_email,
                'email_from': '"Sadhanapada - Isha Clinic" <ishanandasg@gmail.com>',
                'reply_to': '"Sadhanapada - Isha Clinic" <ishanandasg@gmail.com>'
            }

            create_values.update(base_mail_values)  # mail_message_id, mail_server_id, auto_delete, references, headers
            email = Mail.create(create_values)
            email.send()
            # return True

        model = msg_vals.get('model') if msg_vals else message.model
        model_name = model_description or (
            self.with_lang().env['ir.model']._get(model).display_name if model else False)  # one query for display name
        recipients_groups_data = self._notify_classify_recipients(partners_data, model_name)

        if not recipients_groups_data:
            return True
        force_send = self.env.context.get('mail_notify_force_send', force_send)

        template_values = self._notify_prepare_template_context(message, msg_vals, model_description=model_description)
        # 10 queries

        email_layout_xmlid = msg_vals.get('email_layout_xmlid') if msg_vals else message.email_layout_xmlid
        template_xmlid = email_layout_xmlid if email_layout_xmlid else 'mail.message_notification_email'
        try:
            base_template = self.env.ref(template_xmlid, raise_if_not_found=True).with_context(
                lang=template_values['lang'])  # 1 query
        except ValueError:
            _logger.warning(
                'QWeb template %s not found when sending notification emails. Sending without layouting.' % (
                    template_xmlid))
            base_template = False

        mail_subject = message.subject or (message.record_name and 'Re: %s' % message.record_name)
        # in cache, no queries
        # prepare notification mail values
        base_mail_values = {
            'mail_message_id': message.id,
            'mail_server_id': message.mail_server_id.id,
            # 2 query, check acces + read, may be useless, Falsy, when will it be used?
            'auto_delete': mail_auto_delete,
            # due to ir.rule, user have no right to access parent message if message is not published
            'references': message.parent_id.sudo().message_id if message.parent_id else False,
            'subject': mail_subject,
        }
        base_mail_values = self._notify_by_email_add_values(base_mail_values)

        Mail = self.env['mail.mail'].sudo()
        emails = self.env['mail.mail'].sudo()

        # loop on groups (customer, portal, user,  ... + model specific like group_sale_salesman)
        notif_create_values = []
        recipients_max = 50
        for recipients_group_data in recipients_groups_data:
            # generate notification email content
            recipients_ids = recipients_group_data.pop('recipients')
            render_values = {**template_values, **recipients_group_data}
            # {company, is_discussion, lang, message, model_description, record, record_name, signature, subtype, tracking_values, website_url}
            # {actions, button_access, has_button_access, recipients}

            # if base_template:
            #     mail_body = base_template.render(render_values, engine='ir.qweb', minimal_qcontext=True)
            # else:
            mail_body = message.body
            mail_body = self._replace_local_links(mail_body)

            # create email
            for recipients_ids_chunk in split_every(recipients_max, recipients_ids):
                recipient_values = self._notify_email_recipient_values(recipients_ids_chunk)
                email_to = recipient_values['email_to']
                recipient_ids = recipient_values['recipient_ids']

                create_values = {
                    'body_html': mail_body,
                    'subject': mail_subject,
                    'recipient_ids': [(4, pid) for pid in recipient_ids],
                }
                if email_to:
                    create_values['email_to'] = email_to
                create_values.update(base_mail_values)
                # mail_message_id, mail_server_id, auto_delete, references, headers
                email = Mail.create(create_values)

                if email and recipient_ids:
                    tocreate_recipient_ids = list(recipient_ids)
                    if check_existing:
                        existing_notifications = self.env['mail.notification'].sudo().search([
                            ('mail_message_id', '=', message.id),
                            ('notification_type', '=', 'email'),
                            ('res_partner_id', 'in', tocreate_recipient_ids)
                        ])
                        if existing_notifications:
                            tocreate_recipient_ids = [rid for rid in recipient_ids if
                                                      rid not in existing_notifications.mapped('res_partner_id.id')]
                            existing_notifications.write({
                                'notification_status': 'ready',
                                'mail_id': email.id,
                            })
                    notif_create_values += [{
                        'mail_message_id': message.id,
                        'res_partner_id': recipient_id,
                        'notification_type': 'email',
                        'mail_id': email.id,
                        'is_read': True,  # discard Inbox notification
                        'notification_status': 'ready',
                    } for recipient_id in tocreate_recipient_ids]
                emails |= email

        if notif_create_values:
            self.env['mail.notification'].sudo().create(notif_create_values)

        # NOTE:
        #   1. for more than 50 followers, use the queue system
        #   2. do not send emails immediately if the registry is not loaded,
        #      to prevent sending email during a simple update of the database
        #      using the command-line.
        test_mode = getattr(threading.currentThread(), 'testing', False)
        if force_send and len(emails) < recipients_max and (not self.pool._init or test_mode):
            # unless asked specifically, send emails after the transaction to
            # avoid side effects due to emails being sent while the transaction fails
            if not test_mode and send_after_commit:
                email_ids = emails.ids
                dbname = self.env.cr.dbname
                _context = self._context

                def send_notifications():
                    db_registry = registry(dbname)
                    with api.Environment.manage(), db_registry.cursor() as cr:
                        env = api.Environment(cr, SUPERUSER_ID, _context)
                        env['mail.mail'].browse(email_ids).send()

                self._cr.after('commit', send_notifications)
            else:
                emails.send()

        return True

    @api.model
    def message_new(self, msg_dict, custom_values=None):
        return False

    def message_update(self, msg, update_vals=None):
        if self.sp_health_assessment_form_status in ['awaiting_info', 'reminder_sent']:
            self.sudo().write({'sp_health_assessment_form_status': 'response_received'})
        return super(SpRegistrationHealthAssessment, self).message_update(msg, update_vals=update_vals)

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(SpRegistrationHealthAssessment, self).fields_get(allfields, attributes=attributes)
        if res.get('low_matches_count'):
            res['low_matches_count']['searchable'] = False
        if res.get('sp_interview_status'):
            res['sp_interview_status']['searchable'] = False
        if res.get('interview_status'):
            res['interview_status']['searchable'] = False
        return res

    @api.model
    def list_sp_health_assessment(self):
        context = {}
        domain = [('sp_health_assessment_form_status', 'not in',
                   ['not_sent_email', 'email_sent', 'form_submitted', 'partial']),
                  ('interview_form_state', 'not in', ['new', 'feedback_pending', 'feedback_received']),
                  '|', ('interview_reviewer_decision', '!=', 'not_selected'), ('ha_screening_doctor', '!=', False)]
        if self.env.user.has_group('isha_sp_registration.group_sp_ha_screening_doctor'):
            context = {'search_default_my_ha_form': 1}
        if self.env.user.has_group('isha_sp_registration.group_sp_ha_doctor_approval'):
            context = {'search_default_ready_for_da': 1}
        views = [(self.env.ref('isha_sp_registration.sp_reg_health_assessment_tree').id, 'tree'),
                 (self.env.ref('isha_sp_registration.sp_reg_health_assessment_form').id, 'form'),
                 (self.env.ref('isha_sp_registration.view_sp_reg_health_assessment_graph').id, 'graph'),
                 (self.env.ref('isha_sp_registration.view_sp_reg_health_assessment_pivot').id, 'pivot')]
        search_view_id = self.env.ref('isha_sp_registration.sp_reg_health_assessment_search').id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'sp.reg.health.assessment',
            'name': "Health Assessment",
            'view_mode': 'tree,form,graph,pivot',
            'views': views,
            'search_view_id': search_view_id,
            'target': 'current',
            'context': context,
            'domain': domain,
        }

    def get_view_on_covid_update_button(self):
        return {
            'name': 'Update Covid Information',
            'view_mode': 'form',
            'res_id': self.id,
            'res_model': 'sp.reg.health.assessment',
            'view_id': self.env.ref('isha_sp_registration.update_covid_information').id,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    def button_update_covid(self):
        return


class SpRegistrationBaseMedicalConditionDetails(models.Model):
    _name = 'sp.reg.condition.details.base'
    _description = "Base medical condition details"
    condition_applicability_id = fields.Many2one('sp.reg.medical.condition.applicability', ondelete='cascade',
                                                 string='Condition', readonly=True)
    health_assessment_id = fields.Many2one('sp.reg.health.assessment', string='Health Assessment', ondelete='cascade')


class SpRegistrationCommonConditionDetails(models.Model):
    _name = 'sp.reg.condition.details.common'
    _description = "Medical condition details"

    base_details_id = fields.Many2one('sp.reg.condition.details.base', string='Base details', required=True,
                                      delegate=True, ondelete='cascade')
    medical_condition_name = fields.Char(string='Medical Condition Name', required=True)
    symptoms = fields.Char(string='Symptoms', required=True)
    start_date = fields.Date(required=True)
    duration = fields.Char(required=True)
    frequency_of_occurrence = fields.Char(required=True, string='Frequency of Occurrence')
    last_episode = fields.Date(required=True, string='Last Episode')
    present_condition = fields.Char(required=True, string='Present Condition')
    is_treatment_taken = fields.Selection(list(constants.BOOLEAN_VALUES.items()), required=True)
    no_treatment_reason = fields.Char('Reason for not taking treatment')
    is_medication_prescribed = fields.Selection(list(constants.BOOLEAN_VALUES.items()),
                                                string='Is Medication Prescribed?')
    has_undergone_surgery = fields.Selection(list(constants.BOOLEAN_VALUES.items()), required=True)
    prescribed_medication_ids = fields.One2many('sp.reg.prescribed.medications', 'medical_condition_details_id',
                                                string='Prescribed Medications')
    medical_report_ids = fields.Many2many('ir.attachment', string='Medical Reports')
    surgery_detail_ids = fields.One2many('sp.reg.condition.details.surgery', 'medical_condition_details_id',
                                         string='Surgery Details')
    # Joints and muscles
    is_physiotherapy_taken = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    physiotherapy_details = fields.Text()
    # Blood transfusion
    blood_transfusion_details_id = fields.Many2one('sp.reg.condition.details.bloodtransfusion',
                                                   string='Blood Transfusion Details', ondelete='cascade')


class SpRegistrationBloodTransfusionConditionDetails(models.Model):
    _name = 'sp.reg.condition.details.bloodtransfusion'
    _description = "Medical condition details"

    base_details_id = fields.Many2one('sp.reg.condition.details.base', string='Base Details', required=True,
                                      delegate=True, ondelete='cascade')
    bt_start_date = fields.Date(required=True, string='Start Date')
    bt_reason = fields.Char(required=True, string='Reason')
    bt_packets_count = fields.Char(required=True, string='Packets Count')
    bt_had_side_effects = fields.Selection(list(constants.BOOLEAN_VALUES.items()), required=True,
                                           string='Had side effects?')
    common_condition_details_ids = fields.One2many('sp.reg.condition.details.common', 'blood_transfusion_details_id')


class SpRegistrationPrescribedMedications(models.Model):
    _name = 'sp.reg.prescribed.medications'
    _description = "Prescribed medications"
    _rec_name = 'medicine_name_dosage'

    medical_condition_details_id = fields.Many2one('sp.reg.condition.details.common', ondelete='cascade',
                                                   string='Medical Condition Details')
    hospitalization_details_id = fields.Many2one('sp.reg.condition.details.hospitalization', ondelete='cascade',
                                                 string='Hospitalization Details')
    treatment_details = fields.Char(required=True)
    medicine_name_dosage = fields.Char(required=True, string='Medicine Name and Dosage')
    medicine_frequency = fields.Char()
    medicine_duration = fields.Char(required=True)
    is_active = fields.Selection(list(constants.BOOLEAN_VALUES.items()),
                                 string='Are you still on this medication?', required=True)


class SpRegistrationSurgeryMedicalConditionDetails(models.Model):
    _name = 'sp.reg.condition.details.surgery'
    _description = "Surgery details"
    _rec_name = 'surgery_name'

    base_details_id = fields.Many2one('sp.reg.condition.details.base', string='Base Details', required=True,
                                      delegate=True, ondelete='cascade')
    medical_condition_details_id = fields.Many2one('sp.reg.condition.details.common',
                                                   string='Medical Condition Details')
    surgery_name = fields.Char(required=True, string='Surgery Name')
    surgery_date = fields.Date(string='Surgery Date')
    surgery_reason = fields.Char(string='Surgery Reason')
    hospitalization_duration = fields.Char(string='Hospitalization Duration')
    hospitalization_duration_unit = fields.Selection(list(constants.TIME_UNIT.items()),
                                                     string='Hospitalization Duration Unit')
    present_condition = fields.Char(string='Present Condition')
    reports_ids = fields.Many2many('ir.attachment', string='Medical Reports')


class SpRegistrationHospitalizationMedicalConditionDetails(models.Model):
    _name = 'sp.reg.condition.details.hospitalization'
    _description = "Hospitalization details"
    _rec_name = 'hospitalization_reason'

    base_details_id = fields.Many2one('sp.reg.condition.details.base', string='Base Details', required=True,
                                      delegate=True, ondelete='cascade')
    hospitalization_reason = fields.Char(required=True, string='Hospitalization Reason')
    hospitalization_date = fields.Date(required=True, string='Hospitalization Date')
    hospitalization_duration = fields.Char(required=True, string='Hospitalization Duration')
    hospitalization_duration_unit = fields.Selection(list(constants.TIME_UNIT.items()), required=True,
                                                     string='Hospitalization Duration Unit')
    present_condition = fields.Char(required=True, string='Present Condition')
    is_treatment_taken = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    no_treatment_reason = fields.Char('Reason for not taking treatment')
    is_medication_prescribed = fields.Selection(list(constants.BOOLEAN_VALUES.items()),
                                                string='Is Medication Prescribed?')
    prescribed_medication_ids = fields.One2many(
        'sp.reg.prescribed.medications', 'hospitalization_details_id', string='Prescribed Medications')
    reports_ids = fields.Many2many('ir.attachment', string='Medical Reports')


class SpRegistrationAbstractConsumptionDetails(models.AbstractModel):
    _name = 'sp.reg.condition.details.consumption'
    _description = "Abstract consumption details"

    base_details_id = fields.Many2one('sp.reg.condition.details.base', string='Base Details', required=True,
                                      delegate=True, ondelete='cascade')
    is_active = fields.Selection(list(constants.BOOLEAN_VALUES.items()), string='Is Usage Active?')
    consumption_start_date = fields.Date(required=True)
    last_consumption_date = fields.Date(required=True)
    consumption_duration = fields.Char(compute='compute_alcohol_consumption_duration', store=True)

    @api.depends('consumption_start_date', 'last_consumption_date')
    def compute_alcohol_consumption_duration(self):
        for rec in self.sudo():
            duration = relativedelta(rec.last_consumption_date, rec.consumption_start_date)
            rec.consumption_duration = str(duration.years) + ' years ' + str(duration.months) \
                                       + ' months ' + str(duration.days) + ' days'


class SpRegistrationAlcoholMedicalConditionDetails(models.Model):
    _name = 'sp.reg.condition.details.alcohol'
    _inherit = 'sp.reg.condition.details.consumption'
    _description = "Alcohol details"

    consumption_details_ids = fields.One2many(
        'sp.reg.alcohol.consumption.details', 'alcohol_condition_id', string='Consumption Details')


class SpRegistrationAlcoholConsumptionDetails(models.Model):
    _name = 'sp.reg.alcohol.consumption.details'
    _description = "Alcohol consumption details"
    _rec_name = 'drink_type'

    alcohol_condition_id = fields.Many2one('sp.reg.condition.details.alcohol', ondelete='cascade',
                                           string='Alcohol Condition')
    drink_type = fields.Selection(list(constants.ALCOHOL_TYPES.items()), required=True)
    consumption_frequency_count = fields.Char(required=True, string='Consumption Frequency')
    consumption_frequency_unit = fields.Selection(list(constants.TIME_UNIT.items()), required=True,
                                                  string='Consumption Frequency Unit')
    computed_frequency = fields.Char(compute='_compute_frequency_field', string='Consumption Frequency')
    quantity_in_ml = fields.Integer('Consumption Quantity in ml', required=True)

    @api.depends('consumption_frequency_count', 'consumption_frequency_unit')
    def _compute_frequency_field(self):
        for rec in self:
            rec.computed_frequency = str(rec.consumption_frequency_count) + ' ' + str(rec.consumption_frequency_unit)


class SpRegistrationTobaccoMedicalConditionDetails(models.Model):
    _name = 'sp.reg.condition.details.tobacco'
    _inherit = 'sp.reg.condition.details.consumption'
    _description = "Tobacco details"

    cigarette_smoking_frequency_count = fields.Char(required=True, string='Smoking Frequency')
    cigarette_smoking_frequency_unit = fields.Selection(
        constants.TIME_UNIT.items(), required=True, string='Smoking Frequency Unit')
    computed_cigarette_smoking_frequency = fields.Char(compute='_compute_frequency_field',
                                                       string='Cigarette Smoking Frequency')
    tobacco_consumption_frequency_count = fields.Char(required=True, string='Tobacco Consumption Frequency')
    tobacco_consumption_frequency_unit = fields.Selection(
        constants.TIME_UNIT.items(), required=True, string='Tobacco Frequency Unit')
    computed_tobacco_consumption_frequency = fields.Char(compute='_compute_frequency_field',
                                                         string='Tobacco Consumption Frequency')

    @api.depends('cigarette_smoking_frequency_count', 'cigarette_smoking_frequency_unit',
                 'tobacco_consumption_frequency_count', 'tobacco_consumption_frequency_unit')
    def _compute_frequency_field(self):
        for rec in self:
            rec.computed_cigarette_smoking_frequency = str(rec.cigarette_smoking_frequency_count) + ' ' \
                                                       + str(rec.cigarette_smoking_frequency_unit)
            rec.computed_tobacco_consumption_frequency = str(rec.tobacco_consumption_frequency_count) + ' ' \
                                                         + str(rec.tobacco_consumption_frequency_unit)


class SpRegistrationSubstanceConditionDetails(models.Model):
    _name = 'sp.reg.condition.details.substance'
    _inherit = 'sp.reg.condition.details.consumption'
    _description = "Substance consumption details"

    consumption_details_ids = fields.One2many(
        'sp.reg.substance.consumption.details', 'substance_condition_id', string='Consumption Details')
    consumption_experience = fields.Text(required=True)
    is_treatment_taken = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    treatment_details = fields.Text()


class SpRegistrationSubstanceConsumptionDetails(models.Model):
    _name = 'sp.reg.substance.consumption.details'
    _description = "Substance consumption details"
    _rec_name = 'substance_type'

    substance_condition_id = fields.Many2one('sp.reg.condition.details.substance', ondelete='cascade',
                                             string='Alcohol Condition')
    substance_type = fields.Selection(list(constants.SUBSTANCE_TYPES.items()), required=True)
    consumption_frequency_count = fields.Char(required=True, string='Consumption Frequency')
    consumption_frequency_unit = fields.Selection(list(constants.TIME_UNIT.items()), required=True,
                                                  string='Consumption Frequency Unit')
    computed_frequency = fields.Char(compute='_compute_frequency_field', string='Consumption Frequency')

    @api.depends('consumption_frequency_count', 'consumption_frequency_unit')
    def _compute_frequency_field(self):
        for rec in self:
            rec.computed_frequency = str(rec.consumption_frequency_count) + ' ' + str(rec.consumption_frequency_unit)


class SpRegistrationCovidDetails(models.Model):
    _name = 'sp.reg.condition.details.covid'
    _description = "Covid details"
    _rec_name = 'health_assessment_id'

    health_assessment_id = fields.Many2one('sp.reg.health.assessment', string='Health Assessment', readonly=True,
                                           ondelete='cascade')
    is_tested_positive = fields.Selection(list(constants.BOOLEAN_VALUES.items()), string='Tested Covid positive?')
    is_exposed_to_covid = fields.Selection(list(constants.BOOLEAN_VALUES.items()),
                                           string='Exposed to Covid positive patients/ healthcare workers?')
    covid_condition = fields.Many2many('sp.reg.covid.condition.name')
    additional_details = fields.Text()
    # Details if tested positive for covid
    infection_date = fields.Date()
    is_covid_related_complication = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    is_complication_active = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    treatment_details = fields.Text()
    present_condition = fields.Text()
    covid_report_ids = fields.Many2many('ir.attachment', string='Medical Reports')
    # Details if exposed to covid
    exposure_date = fields.Date(string='Date of Exposure to Covid patients/healthcare workers')
    measures_after_exposure = fields.Text(string='Measures Taken After Exposure')


class SpRegistrationCovidConditionName(models.Model):
    _name = 'sp.reg.covid.condition.name'
    _rec_name = 'name'
    _order = 'name'
    _sql_constraints = [('name_uniq', 'UNIQUE (code)', 'You can not have two conditions with the same name !')]

    name = fields.Char(string='Covid Condition Name', required=True)
    code = fields.Char(string='Covid Condition Code', required=True)
    active = fields.Boolean()


class SpRegistrationMedicalConditionReports(models.Model):
    _name = 'sp.reg.medical.condition.reports'
    _sql_constraints = [('name_uniq', 'UNIQUE (report_code)', 'You can not have two conditions with the same name !')]

    report_name = fields.Char(string='Medical Condition Report Name', required=True)
    report_code = fields.Char(string='Medical Condition Report Code', required=True)
    condition_name_id = fields.Many2one('sp.reg.medical.condition.name')
    active = fields.Boolean()


class SpRegistrationMedicalConditionApplicability(models.Model):
    _name = 'sp.reg.medical.condition.applicability'
    _description = "Medical condition applicability"
    _rec_name = 'condition_details_id'
    _sql_constraints = [('health_assessment_condition_uniq', 'UNIQUE (health_assessment_id, condition_details_id)',
                         'Violated unique constraint on Medical Condition Applicability Model')]

    health_assessment_id = fields.Many2one('sp.reg.health.assessment', string='Health Assessment',
                                           readonly=True, required=True, ondelete='cascade')
    # , delegate=True)
    condition_details_id = fields.Many2one('sp.reg.medical.condition', required=True, ondelete='cascade',
                                           string='Condition Details ID')
    is_applicable = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    condition_modal_details_ids = fields.One2many('sp.reg.condition.details.base',
                                                  inverse_name='condition_applicability_id')
    condition_report_name_ids = fields.Many2many('sp.reg.medical.condition.reports', compute="_compute_report_ids",
                                                 string='Condition Report Name IDs')

    @api.depends('condition_details_id')
    def _compute_report_ids(self):
        reports_model = self.env['sp.reg.medical.condition.reports'].sudo()
        for record in self:
            condition_report_name_ids = reports_model.search(
                [('condition_name_id', '=', record.condition_details_id.condition.id)])
            record.condition_report_name_ids = condition_report_name_ids


class SpRegistrationMedicalCondition(models.Model):
    _name = 'sp.reg.medical.condition'
    _description = "Medical condition"
    _rec_name = 'condition'
    # condition will be Knee, Food etc
    condition = fields.Many2one('sp.reg.medical.condition.name', string='Medical Condition Name', required=True,
                                ondelete='cascade')
    category = fields.Many2one('sp.reg.medical.condition.category', string='Medical Condition Category', required=True,
                               ondelete='cascade')


class SpRegistrationMedicalConditionName(models.Model):
    _name = 'sp.reg.medical.condition.name'
    _rec_name = 'name'
    _order = 'name'

    _sql_constraints = [('name_uniq', 'UNIQUE (code)', 'You can not have two conditions with the same name !')]

    name = fields.Char(string='Medical Condition Name')
    label = fields.Char(string='Medical Condition Label')
    code = fields.Char(string='Medical Condition Code')
    active = fields.Boolean()


class SpRegistrationMedicalConditionCategory(models.Model):
    _name = 'sp.reg.medical.condition.category'
    _rec_name = 'name'
    _order = 'name'

    _sql_constraints = [('name_uniq', 'UNIQUE (name)', 'You can not have two same type with the same name !')]

    name = fields.Char(string='Medical Condition Category')
    parent_category = fields.Char(string='Parent Model Category')
    active = fields.Boolean()


class SpRegistrationFamilyMedicalConditionDetails(models.Model):
    _name = 'sp.reg.condition.details.family'
    _description = "Family Medical Condition Details"

    health_assessment_id = fields.Many2one('sp.reg.health.assessment', string='SP Applicant', readonly=True,
                                           ondelete='cascade')
    relation = fields.Selection([('father', 'Father'), ('mother', 'Mother'), ('brother', 'Brother'),
                                 ('sister', 'Sister'), ('son', 'Son'), ('daughter', 'Daughter'),
                                 ('grandmother_maternal', 'Grandmother Maternal'),
                                 ('grandfather_maternal', 'Grandfather Maternal'),
                                 ('grandmother_paternal', 'Grandmother Paternal'),
                                 ('grandfather_paternal', 'Grandfather Paternal')], string='Relation')
    age = fields.Char(string='Age')
    significant_health_problems = fields.Text(string='Significant Health Problems')


class HaAdditionalValues(models.Model):
    _name = 'ha.additional.values'
    _description = "HA Additional Values"

    _sql_constraints = [('name_uniq', 'UNIQUE (name)', 'You can not have duplicate values!')]

    name = fields.Char(string='Value')
    active = fields.Boolean(string='Active', default=True)
