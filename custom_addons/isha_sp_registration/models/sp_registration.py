# -*- coding: utf-8 -*-

import json
import logging
import csv, os
import phpserialize
from lxml import etree

from datetime import datetime as dt
import datetime

from odoo import models, fields, api, _, exceptions
from odoo.exceptions import ValidationError
from . import constants
from ..sso_user_util import ensure_user, get_sso_account_password_reset_link
from ...isha_crm.ContactProcessor import IshaVolunteer
import threading
from odoo.tools import config, pytz
import odoo
import time, sys, datetime
from ..isha_importer import Configuration
_logger = logging.getLogger(__name__)


class SpRegistration(models.Model):
    _name = 'sp.registration'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'generic.mixin.track.changes']
    _description = 'SP Registrations'
    _order = 'id desc'
    _rec_name = 'sp_name'

    PHONE_COPY_PREFIX = '783'
    LOW_MATCH_PAIR_DOMAIN = [('match_level', 'in', ['high_sso', 'low']), ('active', '=', True)]

    sp_id = fields.Char('SP ID')
    person_id = fields.Integer('Person ID')
    interest_id = fields.Integer('Interest ID')
    is_new_contact = fields.Boolean(string='Is new contact', readonly=True)
    is_new_sso_user = fields.Boolean()
    sso_api_called_success = fields.Boolean(default=False)

    volunteer_id = fields.Many2one('isha.volunteer', delegate=True, required=True)
    active = fields.Boolean(string='Active', required=True, default=True)
    sp_name = fields.Char(compute='_compute_name', string='Name', store=True)
    first_name = fields.Char(string='First Name', required=True)
    last_name = fields.Char(string='Last Name')
    photo = fields.Binary(string='Profile photo', attachment=True)
    sp_email = fields.Char(string="Email")
    isha_email = fields.Char(string='Isha Email')
    sp_marital_status = fields.Selection(list(constants.MARITAL_STATUS.items()), string='Marital status')
    sp_city = fields.Char('City')
    sp_state = fields.Char('State')
    sp_state_id = fields.Many2one('res.country.state', string='State', domain="[('country_id', '=?', sp_country_id)]")
    state_id_show = fields.Boolean()
    sp_country_id = fields.Many2one('res.country', 'Country')
    sp_zip = fields.Char('Zip')
    age = fields.Char(compute="age_calc", store=False)

    @api.depends('dob')
    def age_calc(self):
        for record in self:
            if record.dob:
                age = (dt.today().date() - record.dob).days / 356
                if age and age > 0:
                    record.age = str(int(age)) + ' years'
            else:
                record.age = 0

    # Nationality
    sp_nationality = fields.Many2one('res.country', string='Nationality', required=True)
    sp_is_overseas = fields.Boolean(string="Is Overseas", readonly=True)

    # Phone details
    sp_phone = fields.Char(string='Phone', size=15)
    sp_phone_country_code = fields.Char(string='Phone Country Code')
    full_phone = fields.Char("Phone", compute="_compute_full_contact_number")
    copy_phone = fields.Char("Phone", compute="_compute_copy_phone_number")
    is_mobile_same_as_phone = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    sp_mobile_country_code = fields.Char(string='Mobile Country Code')
    sp_mobile = fields.Char(string="WhatsApp No.", size=15)
    full_mobile = fields.Char("WhatsApp No.", compute="_compute_full_contact_number")
    sp_isha_phone_country_code = fields.Char(string="CUG Country Code")
    sp_isha_phone = fields.Char(string="CUG", size=15)
    full_isha_phone = fields.Char("CUG", compute="_compute_full_contact_number")

    # Ishangam profile related fields
    ishangam_partner_pkey = fields.Integer(related='partner_id.id', readonly=True, string="Ishangam ID")
    sp_suvya_person_phone = fields.Char(string='Suvya Person Phone No')

    # Pre-registration program details
    ie_status = fields.Selection([('completed', 'Completed the program'),
                                  ('only_ie_online', 'Only IE Online completed, waiting for IE Completion'),
                                  ('not_completed', 'Not completed the program')], string='IE Status')
    ie_completed_post_registration = fields.Selection([('yes', 'Yes'),
                                                       ('incorrect', 'Incorrect Form Value')],
                                                      string='IE Completed Post Registration')
    ie_online_status = fields.Selection([('yes', 'Yes'), ('no', 'No')], string='IE Online Status')
    iep_month = fields.Selection([('1', 'January'),
                                  ('2', 'February'),
                                  ('3', 'March'),
                                  ('4', 'April'),
                                  ('5', 'May'),
                                  ('6', 'June'),
                                  ('7', 'July'),
                                  ('8', 'August'),
                                  ('9', 'September'),
                                  ('10', 'October'),
                                  ('11', 'November'),
                                  ('12', 'December')], string='IE Month complete')
    iep_year = fields.Selection([(str(num), str(num)) for num in range(1988, datetime.datetime.now().year + 1)],
                                'IE Year Completed')
    iep_location = fields.Char('IE Location')
    iep_teacher = fields.Char('IE Teacher Name')
    ie_status_mail_sent = fields.Boolean(default=False)
    is_overseas_mail_sent = fields.Boolean(default=False)
    ie_pending_mail_sent = fields.Boolean(default=False)

    # Call details
    date_called = fields.Datetime()
    call_back_date = fields.Datetime()
    person_called = fields.Many2one('res.users')
    call_count = fields.Integer(default=0)
    call_count_3_mail = fields.Boolean(default=False)
    call_count_6_mail = fields.Boolean(default=False)
    sp_pref_lang_ids = fields.Many2many('res.lang', 'res_lang_sp_registration_rel', 'res_lang_id', 'sp_registration_id',
                                        string='Language Preference',
                                        domain=['|', ('active', '=', False), ('active', '=', True)])

    # SP status
    sp_call_status = fields.Many2one('sp.call.status', string='Call Status')
    sp_pre_reg_status = fields.Many2one('sp.pre.reg.status', string='Pre-Registration Status',
                                        domain="[('call_status', '=', sp_call_status)]")
    sp_pre_reg_status_changed_datetime = fields.Datetime()
    # todo delete after data migration
    sp_pre_reg_email_send_datetime = fields.Datetime(string='Full profile form sent date', readonly=True)
    sp_profile_email_send_datetime = fields.Datetime(string='Full profile form sent date', readonly=True)

    pending_other_reason = fields.Many2one('pending.other.reason')
    is_pre_reg_pending_other = fields.Boolean(default=False)
    is_pre_reg_cancelled = fields.Boolean(default=False)

    sdp_tagged_status = fields.Many2one('registration.status', string="SDP Tagged - Status",
                                        default=lambda self:
                                        self.env.ref('isha_sp_registration.registration_status_yet_to_review').id,
                                        domain=lambda self: [('status_type', 'in', [self.env.ref(
                                            'isha_sp_registration.registration_status_type_sdp_tagged').id])])

    sdp_tagged_review_comments_ids = fields.One2many('registration.feedback', 'registration_id', ondelete='cascade',
                                                     domain=lambda self: [('feedback_type', '=', self.env.ref(
                                                         'isha_sp_registration.feedback_type_sdp_tagged_comments').id)])

    def button_add_sdp_tagged_comments(self):
        return {
            'name': 'Add SDP Tagged Comments',
            'view_mode': 'form',
            'res_model': 'interview.comments.wizard',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': {
                'sdp_tagged_comments': True,
            },
            'target': 'new',
        }

    def button_get_view_for_sdp_tagged_status(self):
        return {
            'name': 'Update SDP Tagged Status',
            'view_mode': 'form',
            'res_id': self.id,
            'res_model': 'sp.registration',
            'view_id': self.env.ref('isha_sp_registration.update_sdp_tagged_status').id,
            'type': 'ir.actions.act_window',
            'target': 'new',
        }

    def update_sdp_tagged_status(self):
        return

    doctor_screening_status = fields.Many2one('registration.status')
    trial_status = fields.Many2one('registration.status')
    cancel_date = fields.Date(compute="compute_cancel_date", store=True)
    cancellation_reason = fields.Many2one('cancellation.reason')
    show_cancel_button = fields.Boolean(compute="_compute_show_cancel_button")
    cancellation_mail_sent = fields.Boolean(default=False)
    application_stage = fields.Many2one('application.stage', ondelete='restrict', index=True, string="SP Status",
                                        track_visibility="onchange", copy=False)
    next_stage_ids = fields.Many2many('application.stage', compute="_compute_next_stage_ids", readonly=True)
    previous_status = fields.Many2one('application.stage', string='Previous Status')
    is_application_cancelled = fields.Boolean(default=False, compute='_is_application_cancelled',
                                              help='Used for Re-Open Button, '
                                                   'based on already reopen feedback given or not')
    is_application_stage_cancelled = fields.Boolean(default=False, compute='_is_application_stage_cancelled',
                                                    store=True, help='To find Application stage is Cancelled or not')

    # Pre-registration additional fields
    sp_acknowledgement = fields.Boolean('To participate in Sadhanapada,you will need to stay at the Isha Yoga Center '
                                        'for 7 months, from Guru Purnima to Mahashivratri.')
    why_sadhanapada = fields.Many2many('why.sadhanapada', string='Why do you want to take up Sadhanapada?')
    sp_reference = fields.Many2many('about.sadhanapada', string='How did you hear about Sadhanapada?')
    sp_receive_updates = fields.Boolean('Yes, please send me updates about Sadhanapada program')

    special_handling = fields.Boolean('Special Handling')
    remarks = fields.Text('Remarks')

    feedback_ids = fields.One2many(
        'registration.feedback', 'registration_id', ondelete='cascade',
        domain=lambda self: ['|', ('feedback_type', '=', self.env.ref('isha_sp_registration.feedback_type_call').id),
                             ('feedback_type', '=', self.env.ref('isha_sp_registration.feedback_type_reopen').id)])
    feedback_pre_reg_call_comment_ids = fields.One2many('registration.feedback', 'registration_id', ondelete='cascade',
                                                        domain=lambda self: [('feedback_type', '=', self.env.ref(
                                                            'isha_sp_registration.feedback_type_call').id)])
    vol_dept_interview_feedback_ids = fields.One2many(
        'registration.feedback', 'registration_id', ondelete='cascade',
        domain=lambda self: ['|', ('feedback_type', '=', self.env.ref('isha_sp_registration.feedback_type_oco').id),
                             ('feedback_type', '=', self.env.ref('isha_sp_registration.feedback_type_vro').id)])
    interview_general_comment_ids = fields.One2many(
        'registration.feedback', 'registration_id', ondelete='cascade',
        domain=lambda self: [
            '|', '|',
            ('feedback_type', '=', self.env.ref('isha_sp_registration.feedback_type_interviewer_comments').id),
            ('feedback_type', '=', self.env.ref('isha_sp_registration.feedback_type_reviewer_comments').id),
            '|', ('feedback_type', '=', self.env.ref('isha_sp_registration.feedback_type_reviewer_follow').id),
            ('feedback_type', '=', self.env.ref('isha_sp_registration.feedback_type_interviewer_follow').id)])
    final_approval_followup_comment_ids = fields.One2many(
        'registration.feedback', 'registration_id', ondelete='cascade',
        domain=lambda self: [
            ('feedback_type', '=', self.env.ref('isha_sp_registration.feedback_type_final_approval').id)])
    previous_application_ids = fields.Many2many('sp.registration', 'sp_registration_application_rel', 'reg_id',
                                                'app_id', compute="_compute_previous_applications")
    is_previous_application = fields.Boolean(string="Previous SP Applicant",
                                             compute="_compute_previous_applications", store=True)
    registration_tag = fields.Many2many('registration.tag')
    appointment_no_response_counter = fields.Integer()

    def _get_application_processing_priority(self):
        application_processing_priority_id = self.env['application.processing.priority'].search(
            [], order='sequence ASC', limit=1)
        if application_processing_priority_id:
            return application_processing_priority_id
        return None

    application_processing_priority = fields.Many2one('application.processing.priority',
                                                      default=_get_application_processing_priority)
    application_processing_priority_sequence = fields.Integer(compute='_compute_priority_sequence', store=True)
    # Interview form related fields
    interview_status = fields.Many2one('registration.status')
    sp_interview_status = fields.Selection([('participant_ready_for_interview', 'Participant - Ready for Interview'),
                                            ('participant_requested_for_time', 'Participant - Requested for Time'),
                                            ('requested_reschedule', 'Requested Reschedule'),
                                            ('no_response_from_participants', 'No response from participants'),
                                            ('appointment_scheduled', 'Appointment Scheduled'),
                                            ('selected', 'Selected'),
                                            ('rejected', 'Not Selected'),
                                            ('selected_with_tp', 'Approval after TP'),
                                            ('selected_with_ha', 'Approval after HA'),
                                            ('selected_with_tp_ha', 'Approval after TP & HA'),
                                            ('reviewer_decision_pending', 'Reviewer decision pending'),
                                            ('awaiting_vro', 'Awaiting VRO Feedback'),
                                            ('awaiting_oco', 'Awaiting OCO Feedback'),
                                            ('vro_feedback', 'VRO Feedback Received'),
                                            ('oco_feedback', 'OCO Feedback Received'),
                                            ('appointment_completed', 'Appointment Completed'),
                                            ('ready_for_review', 'Ready for review')], string='Interview Status')

    previous_interview_form_state = fields.Char()
    interview_form_state = fields.Selection([('new', 'New'),
                                             ('feedback_pending', 'Feedback Pending'),
                                             ('feedback_received', 'Feedback Received'),
                                             ('awaiting_info', 'Awaiting Info from PPT'),
                                             ('ready_review', 'Ready for Review'),
                                             ('decision_pending', 'Reviewer Decision Pending'),
                                             ('awaiting_vro', 'Awaiting VRO Feedback'),
                                             ('awaiting_oco', 'Awaiting OCO Feedback'),
                                             ('vro_feedback', 'VRO Feedback Received'),
                                             ('oco_feedback', 'OCO Feedback Received'),
                                             ('reviewed', 'Reviewed')], string="Interview State",
                                            track_visibility="always")
    person_interviewed = fields.Many2one('res.users')
    interview_datetime = fields.Datetime()
    interviewer_opinion = fields.Selection([('selected', 'Selected'),
                                            ('selected_with_tp', 'Selected With TP'),
                                            ('selected_with_ha', 'Selected With HA'),
                                            ('selected_with_tp_ha', 'Selected With TP & HA'),
                                            ('trial_is_must', 'Trial is a must'),
                                            ('waiting_with_vro', 'Waiting with VRO'),
                                            ('waiting_with_oco', 'Waiting with OCO'),
                                            ('not_selected', 'Not Selected'),
                                            ('no_response', 'No Response'),
                                            ('pending', 'Pending'),
                                            ('cannot_decide', 'Pending')],
                                           string='Interviewer Opinion', readonly=1, track_visibility="always")
    # TODO: cannot_decide value from 'interviewer_opinion' needs to be removed after data migration
    interview_reviewer_decision = fields.Selection([('new', 'New'),
                                                    ('selected', 'Selected'),
                                                    ('not_selected', 'Not Selected'),
                                                    ('approval_tp', 'Approval After TP'),
                                                    ('approval_ha', 'Approval After HA'),
                                                    ('approval_tp_ha', 'Approval After TP & HA'),
                                                    ('trial_is_must', 'Trial is a must'),
                                                    ('waiting_vro_feedback', 'Waiting VRO Feedback'),
                                                    ('waiting_oco_feedback', 'Waiting OCO Feedback')],
                                                   string='Reviewer Decision', readonly=1, track_visibility="always")
    is_reviewer_decision_rejected = fields.Boolean(default=False, compute='_is_reviewer_decision_rejected', store=True,
                                                   help="To find Reviewer Decision is Rejected(not_selected) or not")

    interview_done_by = fields.Many2one('res.users', readonly=True, string='Interview Done By')
    interview_done_on = fields.Datetime(readonly=True, string='Interview Done On')

    interview_opinion_by = fields.Many2one('res.users', readonly=True, string='Interview Opinion By')
    interview_opinion_on = fields.Datetime(readonly=True, string='Interview Opinion On')

    reviewed_by = fields.Many2one('res.users', readonly=True, string='Review Decision By')
    reviewed_on = fields.Datetime(readonly=True, string='Review Decision On')

    interview_concerns = fields.Selection([('none', 'None'),
                                           ('major', 'Major'),
                                           ('minor', 'Minor')], 'Concerns')
    interview_concerns_comment = fields.Text('Please enter any concerns here')
    interview_rejection_email_type = fields.Many2one('rejection.email.type', string='Rejection Email Type')
    break_practice = fields.Many2one('break.practice.reasons', 'If break in practices, Category?')
    break_practice_reason = fields.Text('If break in practices, reason?')
    previous_interview_record_ids = fields.Many2many('sp.interview.form', compute="_compute_previous_interview_records")

    def _compute_previous_interview_records(self):
        for record in self:
            previous_interview_record_ids = self.env['sp.interview.form'].sudo().search([
                # ('partner_id', '=', record.partner_id.id),
                ('registration_batch_status', '=', 'alumni')])
            record.previous_interview_record_ids = previous_interview_record_ids

    # Health assessment form
    health_assessment_ids = fields.One2many('sp.reg.health.assessment', 'sp_reg_id', string='Health Assessment')
    sp_health_assessment_state = fields.Selection([('not_sent_email', 'Email not Sent'),
                                                   ('email_sent', 'Email Sent'),
                                                   ('partial', 'Partial Filled'),
                                                   ('form_submitted', 'Form Submitted'),
                                                   ('ready_screening', 'Ready for Screening'),
                                                   ('screening_in_progress', 'Screening - In Progress'),
                                                   ('screening_on_hold', 'Screening - On Hold'),
                                                   ('awaiting_info', 'Awaiting Info from PPT'),
                                                   ('reminder_sent', 'Reminder Sent'),

                                                   ('screening_approved', 'Screening Doctor - Approved'),
                                                   ('screening_rejected', 'Screening Doctor - Rejected'),
                                                   ('screening_sp_to_decide', 'Screening Doctor - SP to Decide'),
                                                   # ('screening_evaluate_after_trial',
                                                   #  'Screening Doctor - Evaluate after trial'),

                                                   ('review_in_progress', 'Doctor Approval - In Progress'),
                                                   ('review_on_hold', 'Doctor Approval - On Hold'),
                                                   # ('evaluate_after_trial', 'Evaluate after Trial'),

                                                   ('review_approved', 'Doctor Approval - Approved'),
                                                   ('review_rejected', 'Doctor Approval - Rejected'),
                                                   ('review_sp_to_decide', 'Doctor Approval - SP to Decide'),
                                                   # ('review_evaluate_after_trial',
                                                   #  'Review Doctor - Evaluate after trial')
                                                   ], 'Health Assessment Status', default='not_sent_email')

    sp_health_assessment_form_status = fields.Selection([('not_sent_email', 'Email not Sent'),
                                                         ('email_sent', 'Email Sent'),
                                                         ('partial', 'Partial Filled'),
                                                         ('form_submitted', 'Form Submitted'),
                                                         ('ready_screening', 'Ready for Screening'),
                                                         ('screening_in_progress', 'Screening - In Progress'),
                                                         ('screening_on_hold', 'Screening - On Hold'),
                                                         ('awaiting_info', 'Awaiting Info from PPT'),
                                                         ('reminder_sent', 'Reminder Sent'),
                                                         ('response_received', 'Response Received'),
                                                         ('ready_review', 'Ready for Doctor Approval'),
                                                         ('review_in_progress', 'Doctor Approval - In Progress'),
                                                         ('review_on_hold', 'Doctor Approval - On Hold'),
                                                         # ('evaluate_after_trial', 'Evaluate after Trial'),
                                                         ('reviewed', 'Reviewed')], 'Health Assessment Form Status',
                                                        default='not_sent_email', track_visibility='always')

    ha_screening_doctor = fields.Many2one('res.users', string='Screening Doctor Name', readonly=True)
    ha_screening_doctor_datetime = fields.Datetime(string='Screening Doctor Datetime', readonly=True)

    ha_doctor_approval = fields.Many2one('res.users', string='Doctor Approval By', readonly=True)
    ha_doctor_approval_datetime = fields.Datetime(string='Doctor Approval Datetime', readonly=True)

    ha_email_sent_date = fields.Datetime(string='Health Assessment Email Send Date', readonly=True,
                                         track_visibility="onchange")
    ha_submission_date = fields.Datetime(string='Health Assessment Submission Date')
    awaiting_info_date = fields.Datetime()
    reminder_sent_date = fields.Datetime()
    screening_doctor_decision_date = fields.Datetime()
    review_doctor_decision_date = fields.Datetime('Doctor Approval decision date')

    # Aging fields
    ha_submission_pending_since = fields.Integer(string='Health Assessment Submission Pending Since',
                                                 compute='_compute_pending_since', store=True)
    screening_doctor_pending_since = fields.Integer(compute='_compute_pending_since', store=True,
                                                    string='Screening Doctor Pending Since')
    review_doctor_pending_since = fields.Integer('Doctor Approval Pending Since', compute='_compute_pending_since',
                                                 store=True)
    awaiting_info_pending_since = fields.Integer(compute='_compute_pending_since', store=True,
                                                 string='Awaiting Info Pending Since')
    reminder_sent_pending_since = fields.Integer(compute='_compute_pending_since', store=True,
                                                 string='Reminder Sent Pending Since')
    compute_pending_since = fields.Integer(compute='_compute_pending_since', string='Compute Pending Since')

    appointment_ids = fields.One2many('sp.appointment', 'registration_id', string="Appointment")
    appointment_state = fields.Selection([('draft', 'Draft'),
                                          ('scheduled', 'Scheduled'),
                                          ('request_reschedule', 'Request for Reschedule'),
                                          ('rescheduled', 'Re-Scheduled'),
                                          ('no_response', 'No Response'),
                                          ('re_assign', 'Re-Assign Requested'),
                                          ('re_assign_accepted', 'Re-Assign Accepted'),
                                          ('re_assign_rejected', 'Re-Assign Rejected'),
                                          ('completed', 'Completed'),
                                          ('cancelled', 'Cancelled')], string='Appointment Status')

    def _default_registration_batch(self):
        return self.env['registration.batch'].search([('status', '=', 'current')], limit=1)

    registration_batch = fields.Many2one('registration.batch', default=_default_registration_batch)
    registration_batch_status = fields.Selection(related='registration_batch.status')
    starmark_name = fields.Char(related="starmark_category_id.name", readonly=True)
    allow_starmark = fields.Selection([('Yes', 'Yes'), ('no', 'No')], default='no')
    is_in_pre_reg_stage = fields.Boolean(default=False)
    last_automated_email_sent_date = fields.Datetime(string='Last Automated Email Sent', readonly=True)
    last_bulk_email_sent_date = fields.Datetime(string='Last Bulk Email Sent', readonly=True)
    last_bulk_email_sent_by = fields.Many2one('res.users', string='Last Bulk Email Sent By', readonly=True)

    is_starmarked = fields.Selection([('yes', 'Yes'), ('no', 'No')], compute="check_star_mark_status", store=True,
                                     string="Is Starmarked?")
    x_css = fields.Html(string='CSS', sanitize=False, compute='_compute_css', store=False)
    # Auditing Fields
    user_agent = fields.Char()
    user_registered_ip = fields.Char()
    registration_source = fields.Char()
    utm_campaign = fields.Char()
    utm_source = fields.Char()
    utm_medium = fields.Char()
    utm_term = fields.Char()
    utm_content = fields.Char()

    is_from_import = fields.Boolean(default=False)
    sp_applied_date = fields.Datetime(string='Applied Date', default=lambda self: fields.Datetime.now(), readonly=True)

    message_ids = fields.One2many()
    sp_mail_id = fields.One2many('mail.mail', 'res_id', 'Mail', domain=lambda self: [('message_type', '=', 'email')])

    # Full profile form related fields
    sp_profile_id = fields.One2many('sp.registration.profile', 'sp_reg_id', string='SP registration full form')
    sp_profile_status = fields.Selection([('not_sent_email', 'Email not Sent'),
                                          ('email_sent', 'Email Sent'),
                                          ('partial', 'Partial Filled'),
                                          ('submitted', 'Submitted')], 'Profile form Status',
                                         default='not_sent_email', track_visibility='always', copy=False, readonly=True)
    height = fields.Integer(string='Height in Centimeters')
    weight = fields.Integer(string='Weight in kilograms')
    bmi = fields.Float(string='BMI', compute="_compute_bmi", store=True, readonly=True)
    bmi_indicator = fields.Selection([('not_applicable', 'Not applicable'),
                                      ('under_weight', 'Underweight'),
                                      ('normal', 'Normal'),
                                      ('over_weight', 'Overweight'),
                                      ('obese', 'Obese')],
                                     compute="_compute_bmi_indicator", string='BMI Indicator', store=True,
                                     readonly=True)

    @api.depends('height', 'weight')
    def _compute_bmi(self):
        for rec in self:
            if rec.height != 0:
                rec.bmi = rec.weight / ((rec.height / 100) * (rec.height / 100))
            else:
                rec.bmi = 0.00

    @api.depends('bmi')
    def _compute_bmi_indicator(self):
        for rec in self:
            if rec.bmi <= 0:
                rec.bmi_indicator = 'not_applicable'
            elif 0 < rec.bmi < 18.5:
                rec.bmi_indicator = 'under_weight'
            elif 18.5 <= rec.bmi <= 24.9:
                rec.bmi_indicator = 'normal'
            elif 25 <= rec.bmi <= 29.9:
                rec.bmi_indicator = 'over_weight'
            elif rec.bmi >= 30:
                rec.bmi_indicator = 'obese'

    # Profile: Health history
    physical_ailment = fields.Text(string='Do you have any physical ailments? If yes, provide details including '
                                          'diagnosis and treatment')
    psychological_ailment = fields.Text(string='Do you currently have, or do you have a history of any '
                                               'psychological/psychiatric ailment (i.e., depression, anxiety, '
                                               'bipolar disorder) or are under the care of a psychiatrist? If yes, '
                                               'provide details including diagnosis and treatment')
    mental_ailments_in_family = fields.Text(string='Do any family members have a history of any '
                                                   'psychological/psychiatric ailment? If yes, '
                                                   'please provide details')
    current_medication = fields.Text(
        string='Are you currently taking any medication? If yes, list medication & its purpose')
    past_medication = fields.Text(
        string='Have you taken any medication in the past? If yes, list medication and its purpose here')
    drug_usage = fields.Text(
        string='Do you currently use alcohol or drugs, or have you used these in the past? If yes, give details here, '
               'including the most recent date of use')
    drug_treatment = fields.Text(
        string='Have you taken treatment for drug or alcohol use? If yes, give details here')
    surgery_details = fields.Text(
        string='Did you undergo any surgery in the past? If yes, please provide details')
    physical_disabilities = fields.Text(
        string='Do you have any physical disabilities? If yes, please provide details')
    hospitalized = fields.Text(
        string='Have you ever been hospitalized (including psychiatric admission)? '
               'If yes, please provide details')
    # Profile: Criminal history
    convicted_crime_details = fields.Text(
        string='Have you been charged with or convicted of a crime? If yes, give details here:')
    incarceration_details = fields.Text(string='Have you ever been incarcerated? If yes, give details here:')
    existing_legal_cases = fields.Text(string='Do you have any existing legal cases? If yes, please explain')
    other_liabilities = fields.Text(string='Do you have any other liabilities?')

    # Trial period fields
    trial_period_status = fields.Selection([('email_not_sent', 'Email not sent'),
                                            ('email_sent', 'Email sent'),
                                            ('dates_received', 'Dates received'),
                                            ('vro_approved', 'VRO approved'),
                                            ('vro_rejected', 'VRO rejected'),
                                            ('vro_checked_in', 'VRO checked-in'),
                                            ('gp_meeting_scheduled', 'Group meeting scheduled'),
                                            ('gp_meeting_completed', 'Group meeting completed'),
                                            ('ind_meeting_scheduled', 'Individual meeting scheduled'),
                                            ('ind_meeting_completed', 'Individual meeting completed'),
                                            ('ready_for_review', 'Ready for review'),
                                            ('reviewed', 'Reviewed'),
                                            ('dropped_out', 'Dropped out')],
                                           string="Trial period status", default='email_not_sent')

    suvya_trial_period_status = fields.Selection([('approved_after_tp', 'Approved after TP'),
                                                  ('awaiting_feedback', 'Awaiting Feedback'),
                                                  ('cancelled', 'Cancelled'),
                                                  ('checked_in_for_tp', 'Checked-in for TP'),
                                                  ('dates_received', 'Dates Received'),
                                                  ('dropped_out_in_tp', 'Dropped Out in TP'),
                                                  ('later_date', 'Later Date'),
                                                  ('no_answer', 'No Answer'),
                                                  ('pending_other', 'Pending Other'),
                                                  ('rejected_after_tp', 'Rejected after TP'),
                                                  ('waiting_dates_from_ppt', 'Waiting dates from ppt')],
                                                 string="Suvya trial period status")
    # ,   track_visibility='always')

    trial_feedback_ids = fields.One2many(
        'registration.feedback', 'registration_id', readonly=True,
        domain=lambda self: ['|', ('feedback_type', '=',
                                   self.env.ref(
                                       'isha_sp_registration.feedback_type_tp_individual_meeting_comments').id),
                             ('feedback_type', '=',
                              self.env.ref('isha_sp_registration.feedback_type_tp_dropout_comments').id)])
    # Final Approval fields
    final_approval_email_send = fields.Selection([('sent', 'Sent'),
                                                  ('not_sent', 'Not Sent')])
    final_rejection_email_send = fields.Selection([('sent', 'Sent'),
                                                   ('not_sent', 'Not Sent')])

    # Low match pair
    sp_low_matches_count = fields.Integer(string='Low Match Count', store=True, compute="_compute_low_match_count")

    # Covid information for HA and Interview forms
    tested_covid_positive = fields.Selection(list(constants.BOOLEAN_VALUES.items()), string='Tested Covid Positive?')
    covid_positive_tested_date = fields.Date(string='Test Date')
    co_morbid_condition = fields.Selection(list(constants.BOOLEAN_VALUES.items()), string='Any Comorbid Conditions?')

    @api.constrains('appointment_state')
    def constrains_appointment_state(self):
        for record in self:
            interview_form_id = self.env['sp.interview.form'].search(
                [('sp_reg_id', '=', record.id)], limit=1, order="id desc")
            if record.appointment_state == 'completed' and interview_form_id and not interview_form_id.interviewer_opinion:
                record.sp_interview_status = 'appointment_completed'
                interview_form_id.interview_done_by = self.env.uid
                interview_form_id.interview_done_on = datetime.datetime.now()

    @api.depends('contact_low_match_pair_id_fkey')
    def _compute_low_match_count(self):
        """Count the number of low matches this partner has for Smart Button
        Don't count inactive matches.
        """
        for rec in self:
            rec.sp_low_matches_count = len(
                rec.contact_low_match_pair_id_fkey.filtered_domain(self.LOW_MATCH_PAIR_DOMAIN))

    @api.depends('interview_reviewer_decision')
    def _is_reviewer_decision_rejected(self):
        for record in self:
            if record.interview_reviewer_decision == 'not_selected':
                record.is_reviewer_decision_rejected = True
            else:
                record.is_reviewer_decision_rejected = False

    interviewer_feedback_ids = fields.One2many('sp.interviewer.feedback', 'registration_form_id')

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        return self.search(['|', ('sp_name', operator, name), '|', ('first_name', operator, name),
                            ('last_name', operator, name)] + (args or []), limit=limit).name_get()

    @api.depends('application_processing_priority')
    def _compute_priority_sequence(self):
        for record in self:
            if record.application_processing_priority:
                record.application_processing_priority_sequence = record.application_processing_priority.sequence
            else:
                record.application_processing_priority_sequence = 0

    @api.depends('partner_id', 'registration_batch_status')
    def _compute_previous_applications(self):
        for record in self:
            registration_id = self.env['sp.registration'].sudo().search([
                ('partner_id', '=', record.partner_id.id),
                ('registration_batch_status', '=', 'alumni')])
            record.previous_application_ids = registration_id
            if registration_id:
                record.is_previous_application = True
            else:
                record.is_previous_application = False

    @api.depends('sp_phone')
    def _compute_copy_phone_number(self):
        """ Prefixes the phone number with a 3 digit number"""
        for record in self:
            record.copy_phone = str(self.PHONE_COPY_PREFIX) + str(record.sp_phone) if str(
                record.sp_phone) is not None else ''

    @api.depends('sp_phone_country_code', 'sp_phone', 'sp_mobile_country_code', 'sp_mobile',
                 'sp_isha_phone_country_code', 'sp_isha_phone')
    def _compute_full_contact_number(self):
        for record in self:
            record.full_phone = str(record.sp_phone_country_code or '') + ' ' + str(record.sp_phone or '')
            record.full_mobile = str(record.sp_mobile_country_code or '') + ' ' + str(record.sp_mobile or '')
            record.full_isha_phone = str(record.sp_isha_phone_country_code or '') + ' ' + str(
                record.sp_isha_phone or '')

    @api.depends('first_name', 'last_name')
    def _compute_name(self):
        for record in self:
            if record.first_name and record.last_name:
                name = str(record.first_name) + ' ' + str(record.last_name)
            else:
                name = str(record.first_name)
            record.sp_name = name

    @api.constrains('sp_country_id', 'sp_state', 'sp_state_id')
    def _set_country_id(self):
        for record in self:
            if record.sp_state:
                # Needed for zip code changes from portal form
                record.state_id_show = False
            if record.sp_state_id:
                # Needed for zip code changes from portal form
                record.state_id_show = True
            if record.sp_country_id:
                # Needed for odoo backend changes. Country id won't be present in data from portal form
                record.state_id_show = False
                if record.sp_country_id.state_ids:
                    record.state_id_show = True
                if record.sp_state:
                    record.state_id_show = False

    @api.onchange('sp_country_id')
    def _onchange_country_id(self):
        res = {'domain': {'sp_state_id': []}}
        if self.sp_country_id:
            res['domain']['sp_state_id'] = [('country_id', '=', self.sp_country_id.id)]
            self.state_id_show = False
            if self.sp_country_id.state_ids:
                self.state_id_show = True
        return res

    @api.constrains('sp_nationality', 'sp_country_id')
    def _update_is_overseas(self):
        for record in self:
            if record.sp_nationality and record.sp_country_id:
                res_country = record.env['res.country'].sudo()
                if res_country.search([('id', '=', int(record.sp_nationality.id))]).name != 'India' or \
                    res_country.search([('id', '=', int(record.sp_country_id.id))]).name != 'India':
                    record.sp_is_overseas = True
                else:
                    record.sp_is_overseas = False
            else:
                record.sp_is_overseas = False

    @api.depends('approved_incident_by_partner_ids')
    def check_star_mark_status(self):
        for rec in self.sudo():
            if rec.approved_incident_by_partner_ids and len(rec.approved_incident_by_partner_ids) > 0:
                rec.is_starmarked = 'yes'
            else:
                rec.is_starmarked = 'no'

    @api.depends('ha_email_sent_date', 'awaiting_info_date', 'ha_submission_date', 'reminder_sent_date',
                 'review_doctor_decision_date', 'screening_doctor_decision_date')
    def _compute_pending_since(self):
        today = datetime.datetime.today().date()
        for record in self:
            record.ha_submission_pending_since = 0
            record.screening_doctor_pending_since = 0
            record.review_doctor_pending_since = 0
            record.awaiting_info_pending_since = 0
            record.reminder_sent_pending_since = 0
            record.compute_pending_since = 0

            if record.ha_email_sent_date:
                ha_email_sent_date = record.ha_email_sent_date.date()
                if record.ha_submission_date:
                    ha_submission_date = record.ha_submission_date.date()
                    record.ha_submission_pending_since = (ha_submission_date - ha_email_sent_date).days
                else:
                    record.ha_submission_pending_since = (today - ha_email_sent_date).days
            if record.ha_submission_date:
                ha_submission_date = record.ha_submission_date.date()
                if record.review_doctor_decision_date:
                    review_doctor_decision_date = record.review_doctor_decision_date.date()
                    record.review_doctor_pending_since = (review_doctor_decision_date - ha_submission_date).days
                else:
                    record.review_doctor_pending_since = (today - ha_submission_date).days
                if record.screening_doctor_decision_date:
                    screening_doctor_decision_date = record.screening_doctor_decision_date.date()
                    record.screening_doctor_pending_since = (screening_doctor_decision_date - ha_submission_date).days
                else:
                    record.screening_doctor_pending_since = (today - ha_submission_date).days
            if record.awaiting_info_date and record.reminder_sent_date:
                awaiting_info_date = record.awaiting_info_date.date()
                reminder_sent_date = record.reminder_sent_date.date()
                record.awaiting_info_pending_since = (reminder_sent_date - awaiting_info_date).days

    def _compute_css(self):
        for rec in self:
            rec.x_css = None
            if not self.env.user.has_group('isha_sp_registration.group_sp_registration_admin'):
                rec.x_css = '<style>.o_form_button_edit {display: none !important;}</style>' \
                    if rec.sp_pre_reg_status.id == self.env.ref(
                    'isha_sp_registration.sp_pre_reg_status_duplicate').id or \
                       rec.is_starmarked == 'yes' and rec.allow_starmark == 'no' else None

            if rec.application_stage.id == self.env.ref('isha_sp_registration.application_stage_cancelled').id:
                rec.x_css = '<style>.o_form_button_edit {display: none !important;}</style>'

    def _is_application_cancelled(self):
        for rec in self:
            if rec.application_stage.id == self.env.ref('isha_sp_registration.application_stage_cancelled').id and \
                (rec.feedback_ids.registration_id.id != rec.id and not self.env.ref(
                    'isha_sp_registration.feedback_type_reopen').id in rec.feedback_ids.feedback_type.ids):
                rec.is_application_cancelled = True
            else:
                rec.is_application_cancelled = False

    @api.depends('application_stage')
    def _is_application_stage_cancelled(self):
        for rec in self:
            if rec.application_stage.id == self.env.ref('isha_sp_registration.application_stage_cancelled').id:
                rec.is_application_stage_cancelled = True
            else:
                rec.is_application_stage_cancelled = False

    def write(self, values):
        for record in self:
            self._remove_underscores(values, 'sp_phone')
            self._remove_underscores(values, 'sp_isha_phone')
            self._remove_underscores(values, 'sp_mobile')
            if values.get('application_stage'):
                values.update({'previous_status': record.application_stage, 'is_in_pre_reg_stage': True})
            if values.get('cancellation_reason'):
                values.update({'cancel_date': fields.Datetime.today()})
            if self._context.get('active_model') == 'call_queue.call_queue' or values.get('sp_call_status') or \
                values.get('sp_pre_reg_status'):
                values.update({'person_called': self.env.user, 'date_called': fields.Datetime.now()})

            if record.sp_is_overseas and not record.is_overseas_mail_sent or not values.get('is_overseas_mail_sent'):
                if values.get("ie_status") == 'completed':
                    record.send_sp_mail(self.env.ref('isha_sp_registration.overseas_email_template_submit_complete'))
                    values["is_overseas_mail_sent"] = True
                if values.get("ie_status") == 'only_ie_online':
                    record.send_sp_mail(
                        self.env.ref('isha_sp_registration.overseas_email_template_submit_only_ie_online'))
                    values["is_overseas_mail_sent"] = True
                if values.get("ie_status") == 'not_completed':
                    record.send_sp_mail(
                        self.env.ref('isha_sp_registration.overseas_email_template_submit_not_complete'))
                    values["is_overseas_mail_sent"] = True

            if not record.sp_is_overseas and not record.ie_status_mail_sent or not values.get('ie_status_mail_sent'):
                if values.get("ie_status") == 'completed':
                    record.send_sp_mail(self.env.ref('isha_sp_registration.email_template_submit_complete'))
                    values["ie_status_mail_sent"] = True
                if values.get("ie_status") == 'only_ie_online':
                    record.send_sp_mail(self.env.ref('isha_sp_registration.email_template_submit_only_ie_online'))
                    values["ie_status_mail_sent"] = True
                if values.get("ie_status") == 'not_completed':
                    record.send_sp_mail(self.env.ref('isha_sp_registration.email_template_submit_not_complete'))
                    values["ie_status_mail_sent"] = True

            if values.get("sp_call_status") == self.env.ref("isha_sp_registration.sp_call_status_no_answer").id:
                if self.call_count < 3:
                    values["call_count"] = self.call_count + 1
                if self.call_count == 3:
                    values["sp_call_status"] = self.env.ref("isha_sp_registration.sp_call_status_unreachable")
                    values["sp_pre_reg_status"] = self.env.ref("isha_sp_registration.sp_pre_reg_status_no_response")

            # Pre-Registration Status
            # if values.get('sp_pre_reg_status') == self.env.ref('isha_sp_registration.sp_pre_reg_status_all_set_call_completed').id:
            if values.get('sp_pre_reg_status') == self.env.ref('isha_sp_registration.sp_pre_reg_status_all_set').id:
                # and values.get('application_stage') == self.env.ref('isha_sp_registration.application_stage_pre_registration_stage').id
                values["application_stage"] = self.env.ref(
                    'isha_sp_registration.application_stage_registration_stage').id
                self._create_profile_record(values)
                self._create_sso_user_for_profile(values)
                self._create_interview_record(values)

            elif values.get("sp_pre_reg_status") == self.env.ref('isha_sp_registration.sp_pre_reg_status_cancelled').id:
                values["application_stage"] = self.env.ref('isha_sp_registration.application_stage_cancelled').id

            if values.get("sp_call_status") == self.env.ref("isha_sp_registration.sp_call_status_wrong_number").id \
                and values.get("sp_pre_reg_status") == self.env.ref("isha_sp_registration.sp_pre_reg_status_new").id:
                # and values.get("sp_pre_reg_status") == self.env.ref("isha_sp_registration.sp_pre_reg_status_new_wrong_number").id:
                record.send_sp_mail(self.env.ref('isha_sp_registration.email_template_wrong_number_new'))

            # if values.get("sp_call_status") == self.env.ref("isha_sp_registration.sp_call_status_call_completed").id \
            #     and values.get("sp_pre_reg_status") == self.env.ref("isha_sp_registration.sp_pre_reg_status_all_set").id:
            #     record.send_sp_mail(self.env.ref('isha_sp_registration.email_template_call_completed_all_set'))

            if values.get("sp_call_status") == self.env.ref("isha_sp_registration.sp_call_status_unreachable").id and \
                values.get("sp_pre_reg_status") == self.env.ref(
                "isha_sp_registration.sp_pre_reg_status_no_response").id:
                if record.call_count < 6:
                    values["call_count"] = record.call_count + 1
                if values["call_count"] == 3 or record.call_count == 3 and not record.call_count_3_mail or \
                    values["call_count"] == 6 or record.call_count == 6 and not record.call_count_6_mail:
                    record.send_sp_mail(self.env.ref('isha_sp_registration.email_template_unreachable_no_response'))
                    if values["call_count"] == 3 or record.call_count == 3:
                        values["call_count_3_mail"] = True
                    if values["call_count"] == 6 or record.call_count == 6:
                        values["call_count_6_mail"] = True

            if values.get("sp_call_status") == self.env.ref("isha_sp_registration.sp_call_status_call_completed").id and \
                values.get("sp_pre_reg_status") == self.env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending").id \
                and not record.ie_pending_mail_sent:
                # values.get("sp_pre_reg_status") == self.env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending_call_completed").id \
                record.send_sp_mail(self.env.ref('isha_sp_registration.email_template_call_completed_ie_pending'))
                values["ie_pending_mail_sent"] = True
            else:
                values['ie_pending_mail_sent'] = False

            # Application stage Field
            if values.get("application_stage") and values.get("application_stage") != self.env.ref(
                'isha_sp_registration.application_stage_pre_registration_stage').id:
                values["is_in_pre_reg_stage"] = False
            if values.get("application_stage") == self.env.ref('isha_sp_registration.application_stage_cancelled').id \
                and not record.cancellation_mail_sent:
                record.send_sp_mail(self.env.ref('isha_sp_registration.email_template_cancelled'))
                values["cancellation_mail_sent"] = True
            elif record.cancellation_mail_sent:
                values["cancellation_mail_sent"] = False

            # update date_called and person_called
            if self._context.get('call_queue', False):
                values["date_called"] = fields.Datetime.now()
                values["person_called"] = self.env.user

        result = super(SpRegistration, self).write(values)
        # create sso user in separate thread
        # td = threading.Thread(target=self._thread_create_sso_user_for_profile(), args=())
        # td.start()

        return result

    def reopen(self):
        return {
            'name': 'Reopen Application',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'registration.feedback',
            'view_id': self.env.ref('isha_sp_registration.registration_feedback_form').id,
            'target': 'new',
            'domain': [('default_feedback_type', '=', self.env.ref('isha_sp_registration.feedback_type_reopen').id)],
            'context': {
                'default_registration_id': self.id,
                'default_previous_status': self.previous_status.id,
                'default_feedback_type': self.env.ref('isha_sp_registration.feedback_type_reopen').id,
            }
        }

    def send_sp_email(self):
        ctx = dict()
        ctx.update({
            'default_composition_mode': 'mass_mail',
            'default_partner_to': self.partner_id.ids or '',
            'default_use_template': True,
            'default_email_from': '"Sadhanapada - Registration" sadhanapada.office@ishafoundation.org',
            'default_email_to': [record.sp_email for record in self],
            'default_model': self._name,
            'active_ids': self.ids,
            'default_notify': False,
            # 'default_reply_to': True,
            # 'default_template_id': self.env.ref('isha_sp_registration.mail_template_final_approval').id,
        })
        return {
            'name': 'Send email',
            'res_model': 'mail.compose.message',
            'view_mode': 'form',
            'view_id': self.env.ref('isha_sp_registration.sp_email_compose_message_wizard_form').id,
            'type': 'ir.actions.act_window',
            'context': ctx,
            'target': 'new',
        }

    def create_appointment(self):
        userinput_id = self._context.get('userinput_id')

    def call_queue_skip(self):
        userinput_id = self._context.get('userinput_id')

    def call_queue_not_today(self):
        userinput_id = self._context.get('userinput_id')

    def update_registration(self):
        userinput_id = self._context.get('userinput_id')

    def applicant_need_time(self):
        userinput_id = self._context.get('userinput_id')

    def did_not_pick(self):
        userinput_id = self._context.get('userinput_id')
        # return {'type': 'ir.actions.act_window_close'}

    @api.onchange('call_back_date')
    def _onchange_call_back_date(self):
        for record in self:
            if record.call_back_date and record.call_back_date < fields.Datetime.now():
                # record.call_back_date = record.call_back_date.replace(hour=8, minute=30, second=0, microsecond=0)
                raise ValidationError("Please select a 'Call Back Date' equal/greater than the current date")

    def get_full_form_registration_url(self):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        url = base_url + "/sp-profile/"
        return url

    def get_health_assessment_url(self):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        url = base_url + "/ha-form/"
        return url

    @api.constrains('sp_interview_status')
    def _change_sp_interview_status(self):
        for record in self:
            if record.sp_interview_status == "participant_ready_for_interview":
                # Create Final Approval record
                sp_interview_form_id = self.env['sp.interview.form'].sudo().search(
                    [('registration_profile_id', '=', record.sp_profile_id.id,)])
                sp_final_approval_id = self.env['sp.final.approval'].sudo().search(
                    [('sp_interview_id', '=', sp_interview_form_id.id)])
                if not sp_final_approval_id:
                    self.env['sp.final.approval'].sudo().create({
                        'sp_interview_id': sp_interview_form_id.id
                    })

    @api.depends('application_stage')
    def _compute_show_cancel_button(self):
        for record in self:
            if record.application_stage == self.env.ref('isha_sp_registration.application_stage_cancelled'):
                record.show_cancel_button = False
            elif self.env.user.has_group('isha_sp_registration.sp_registration_vro_consultant') or \
                self.env.user.has_group('isha_sp_registration.sp_registration_oco_consultant'):
                record.show_cancel_button = False
            else:
                record.show_cancel_button = True

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(SpRegistration, self).fields_get(allfields, attributes=attributes)
        if res.get('low_matches_count'):
            res['low_matches_count']['searchable'] = False
        if res.get('sp_interview_status'):
            res['sp_interview_status']['searchable'] = False
        if res.get('interview_status'):
            res['interview_status']['searchable'] = False
        return res

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        result = super(SpRegistration, self).fields_view_get(view_id=view_id, view_type=view_type,
                                                             toolbar=toolbar, submenu=submenu)
        pre_reg_caller = self.user_has_groups('isha_sp_registration.group_sp_registration_pre_reg_caller')
        pre_reg_co_ordinator = self.user_has_groups('isha_sp_registration.group_sp_registration_pre_reg_co_ordinator')
        pre_reg_reviewer = self.user_has_groups('isha_sp_registration.group_sp_registration_pre_reg_reviewer')
        pre_reg_admin = self.user_has_groups('isha_sp_registration.group_sp_registration_pre_reg_admin')
        sp_admin = self.user_has_groups('isha_sp_registration.group_sp_registration_admin')

        READONLY_FIELDS = []
        MANDATORY_FIELDS = []
        if pre_reg_caller:
            READONLY_FIELDS = ['first_name', 'last_name', 'sp_email', 'sp_id', 'dob', 'gender', 'sp_marital_status',
                               'photo', 'iep_location', 'iep_teacher', 'ie_status', 'sp_nationality',
                               'interview_status', 'iep_month', 'iep_year', 'why_sadhanapada',
                               'sp_reference', 'doctor_screening_status', 'sdp_tagged_review_comments_ids',
                               'special_handling', 'sdp_tagged_status', 'last_automated_email_sent_date',
                               'last_bulk_email_sent_date', 'last_bulk_email_sent_by', 'sp_mobile_country_code',
                               'sp_mobile', 'sp_phone_country_code', 'sp_phone', 'sp_isha_phone_country_code',
                               'sp_isha_phone', 'zip', 'registration_batch',
                               'date_called', 'person_called', 'allow_starmark']
        if pre_reg_co_ordinator:
            READONLY_FIELDS = ['first_name', 'last_name', 'sp_email', 'sp_id', 'dob', 'gender', 'sp_marital_status',
                               'photo', 'iep_location', 'iep_teacher', 'ie_status', 'sp_nationality',
                               'interview_status', 'iep_month', 'iep_year', 'why_sadhanapada',
                               'sp_reference', 'doctor_screening_status', 'date_called', 'person_called',
                               'allow_starmark']
        if pre_reg_reviewer:
            READONLY_FIELDS = ['first_name', 'last_name', 'sp_email', 'sp_id', 'dob', 'gender',
                               'photo', 'sp_marital_status', 'sp_mobile_country_code', 'sp_mobile',
                               'sp_phone_country_code',
                               'sp_phone', 'special_handling', 'sp_isha_phone_country_code', 'sp_isha_phone',
                               'iep_location', 'iep_teacher', 'ie_status', 'sp_nationality',
                               'interview_status', 'iep_month', 'iep_year', 'why_sadhanapada',
                               'sp_reference', 'doctor_screening_status', 'sdp_tagged_review_comments_ids',
                               'date_called', 'person_called', 'allow_starmark']
        if pre_reg_admin:
            READONLY_FIELDS = ['sp_email', 'sp_id', 'dob', 'gender', 'sp_marital_status', 'iep_location',
                               'photo', 'iep_teacher',
                               'ie_status', 'interview_status', 'iep_month', 'iep_year', 'why_sadhanapada',
                               'sp_reference', 'doctor_screening_status', 'date_called', 'person_called',
                               'allow_starmark']

        if sp_admin:
            READONLY_FIELDS = ['sp_id', 'dob', 'gender', 'sp_marital_status', 'iep_location', 'iep_teacher',
                               'ie_status', 'interview_status', 'iep_month', 'iep_year', 'why_sadhanapada',
                               'sp_reference', 'doctor_screening_status', 'date_called', 'person_called']
            MANDATORY_FIELDS = ['allow_starmark']

        if view_type == 'kanban':
            for rfield in READONLY_FIELDS:
                if rfield in result['fields']:
                    result['fields'][rfield]['readonly'] = True
        if view_type == 'form':
            doc = etree.XML(result['arch'])
            # for node in doc.xpath("//field[@name='call_back_date']"):
            #     node.set('options', "{'datepicker': {'minDate': '%sT00:00:00'}}"
            #              % fields.Date.today().strftime(DEFAULT_SERVER_DATE_FORMAT))
            for node in doc.xpath("//field"):
                name = node.get('name')
                if name in READONLY_FIELDS:
                    attr = {
                        'readonly': 1
                    }
                    if node.attrib.get('modifiers'):
                        attr = json.loads(node.attrib.get('modifiers'))
                        attr['readonly'] = 1
                    node.set('modifiers', json.dumps(attr))
                if name in MANDATORY_FIELDS:
                    attr = {
                        'required': 1
                    }
                    if node.attrib.get('modifiers'):
                        attr = json.loads(node.attrib.get('modifiers'))
                        attr['required'] = 1
                    node.set('modifiers', json.dumps(attr))
            result['arch'] = etree.tostring(doc)
        return result

    # Import Module
    def convert_suvya_dc2type_arrays_to_list(suvya_refs, converting_func, field_string):
        vals = []
        if converting_func:
            dict = phpserialize.loads(converting_func.encode(), decode_strings=True)
            if dict:
                for key in dict:
                    val = dict[key]
                    if val:
                        vals.append(val)
        return vals

    @api.model
    def create(self, values):
        # if values.get('is_from_import'):
        #     if values['phone']:
        #         phones = self.convert_suvya_dc2type_arrays_to_list(values['phone'], 'Phones')
        #         if phones:
        #             for ph in phones:
        #                 if ph['type'] == 'Personal':
        #                     if ph['number']:
        #                         phone = ph['number'].split('-')
        #                         values['phone_country_code'] = phone[0] if len(phone) >= 2 else None
        #                         values['phone'] = phone[1] if len(phone) >= 2 else phone[0]
        #                 elif ph['type'] == 'Whats-App':
        #                     values['mobile'] = ph['number']
        #                 elif ph['type'] == 'CUG':
        #                     values['isha_phone'] = ph['number']

        if not values.get('is_from_import'):
            values['application_stage'] = self.env.ref(
                'isha_sp_registration.application_stage_pre_registration_stage').id
        else:
            contact_processor = IshaVolunteer()
            contact_flag = self.env['res.partner'].sudo().create_internal_flag(
                contact_processor.getOdooFormat(values, {}, {}))
            res_partner_id = contact_flag['rec'][0]
            values['is_new_contact'] = SpRegistration.check_new_contact(contact_flag['phase'])
            values['partner_id'] = res_partner_id.id
        if 'sp_id' not in values:
            values['sp_id'] = self.env['ir.sequence'].next_by_code('sp.registration') or '/'
        self._remove_underscores(values, 'phone')
        self._remove_underscores(values, 'sp_phone')
        self._remove_underscores(values, 'sp_isha_phone')
        self._remove_underscores(values, 'sp_mobile')
        values['is_in_pre_reg_stage'] = True
        if values.get('sp_call_status') is None:
            values['sp_call_status'] = self.env.ref('isha_sp_registration.sp_call_status_not_called').id
        if values.get('sp_pre_reg_status') is None:
            values['sp_pre_reg_status'] = self.env.ref('isha_sp_registration.sp_pre_reg_status_new').id
        # values['sp_pre_reg_status'] = self.env.ref('isha_sp_registration.sp_pre_reg_status_new_not_called').id
        res_country = self.env['res.country'].sudo()

        if res_country.search([('id', '=', int(values['sp_nationality']))]).name != 'India' or \
            res_country.search([('id', '=', int(values['sp_country_id']))]).name != 'India':
            if values['ie_status'] == 'completed':
                values['is_overseas_mail_sent'] = True
                template_id = self.env.ref('isha_sp_registration.overseas_email_template_submit_complete')
            if values['ie_status'] == 'only_ie_online':
                values['is_overseas_mail_sent'] = True
                template_id = self.env.ref('isha_sp_registration.overseas_email_template_submit_only_ie_online')
            if values['ie_status'] == 'not_completed':
                values['is_overseas_mail_sent'] = True
                template_id = self.env.ref('isha_sp_registration.overseas_email_template_submit_not_complete')
        else:
            if values['ie_status'] == 'completed':
                values['ie_status_mail_sent'] = True
                template_id = self.env.ref('isha_sp_registration.email_template_submit_complete')
            if values['ie_status'] == 'only_ie_online':
                values['ie_status_mail_sent'] = True
                template_id = self.env.ref('isha_sp_registration.email_template_submit_only_ie_online')
            if values['ie_status'] == 'not_completed':
                values['ie_status_mail_sent'] = True
                template_id = self.env.ref('isha_sp_registration.email_template_submit_not_complete')

        record = super(SpRegistration, self).create(values)
        record.send_sp_mail(template_id)
        # if values.get('is_from_import') and values.get('application_stage') != self.env.ref(
        #     'isha_sp_registration.application_stage_pre_registration_stage').id:
        #     record._create_sso_user_for_profile(values)
            # record._create_interview_record_import(values)
            # record._create_ha_form(values)
        return record

    def _remove_underscores(self, values, string):
        if values.get(string, False):
            values[string] = values[string].replace("_", "")

    def send_sms(self):
        ctx = dict()
        ctx.update({
            'default_sp_reg_ids': self.select_indian_num_for_sms()
        })
        return {
            'name': 'Send SMS',  # Label
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_id': self.env.ref('isha_sp_registration.sp_sms_recipients_form').id,
            'target': 'new',
            'res_model': 'sp.sms.recipients',
            'context': ctx
        }

    def select_indian_num_for_sms(self):
        indian_numbers_list = []
        for record in self:
            if record.sp_phone_country_code == '91':
                indian_numbers_list.append(record.id)
        return indian_numbers_list

    @api.onchange('application_stage')
    def _compute_next_stage_ids(self):
        for record in self:
            routes = self.env['application.stage.route'].search(record._get_next_stage_route_domain())
            record.next_stage_ids = (record.application_stage + routes.mapped('stage_to_id'))

    def _get_next_stage_route_domain(self):
        self.ensure_one()
        return [
            ('stage_from_id', '=', self.application_stage.id),
            # ('allowed_group_ids', 'in', self.env.user.groups_id.ids)
            # ('close', '=', False)
            # ('show_in_ui', '=', True)
        ]

    @api.constrains('ie_completed_post_registration', 'ie_status')
    def _status_set(self):
        for record in self:
            if record.ie_status == 'only_ie_online' and not record.ie_completed_post_registration and \
                record.sp_pre_reg_status != self.env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending"):
                # record.sp_pre_reg_status != self.env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending_call_completed"):
                record.sp_pre_reg_status = self.env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending")
                # record.sp_pre_reg_status = self.env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending_call_completed")

    @api.model
    def check_condition_show_dialog(self, record_id, data_changed):
        """
            :param:   self: current model
                      record_id: id of record if save on write function, False on create function
                      data_changed: data changed on form
            :returns: True: show dialog
                      False: ignore dialog
        """
        # Checking Validation error to avoid showing confirm dialog box before validation message.
        # if data_changed.get('sp_pre_reg_status') == self.env.ref('isha_sp_registration.sp_pre_reg_status_all_set_call_completed').id:
        if data_changed.get('sp_pre_reg_status') == self.env.ref('isha_sp_registration.sp_pre_reg_status_all_set').id:
            is_all_set_allowed = False
            if data_changed.get('ie_completed_post_registration') == 'yes' or \
                data_changed.get('ie_completed_post_registration') == 'incorrect' and \
                data_changed.get('ie_status') == 'not_completed' or \
                data_changed.get('ie_completed_post_registration') is False and \
                data_changed.get('ie_status') == 'completed':
                is_all_set_allowed = True
            if not is_all_set_allowed:
                raise exceptions.ValidationError('Pre-registration can not be set to All set')
        if data_changed.get('sp_pre_reg_status') == self.env.ref(
            'isha_sp_registration.sp_pre_reg_status_ie_pending').id:
            # 'isha_sp_registration.sp_pre_reg_status_ie_pending_call_completed').id:
            is_ie_pending_allowed = False
            if data_changed.get('ie_status') == 'completed' and \
                data_changed.get('ie_completed_post_registration') == 'incorrect' or \
                data_changed.get('ie_status') == 'only_ie_online' and \
                data_changed.get('ie_completed_post_registration') is False or \
                data_changed.get('ie_status') == 'not_completed' and \
                data_changed.get('ie_completed_post_registration') is False:
                is_ie_pending_allowed = True
            if not is_ie_pending_allowed:
                raise exceptions.ValidationError('Pre-registration can not be set to IE Pending')
        if self.browse(record_id).sp_call_status.id != data_changed.get('sp_call_status') or \
            self.browse(record_id).sp_pre_reg_status.id != data_changed.get('sp_pre_reg_status'):
            if data_changed.get('sp_pre_reg_status') == self.env.ref(
                'isha_sp_registration.sp_pre_reg_status_cancelled').id \
                and not data_changed.get('cancellation_mail_sent'):
                return True
            if data_changed.get('sp_call_status') == self.env.ref("isha_sp_registration.sp_call_status_wrong_number").id \
                and data_changed.get('sp_pre_reg_status') == \
                self.env.ref("isha_sp_registration.sp_pre_reg_status_new").id:
                # self.env.ref("isha_sp_registration.sp_pre_reg_status_new_wrong_number").id:
                return True
            if data_changed.get('sp_call_status') == self.env.ref("isha_sp_registration.sp_call_status_unreachable").id \
                and data_changed.get('sp_pre_reg_status') == \
                self.env.ref("isha_sp_registration.sp_pre_reg_status_no_response").id and \
                data_changed.get('call_count') + 1 == 3 and not data_changed.get('call_count_3_mail') or \
                data_changed.get('call_count') + 1 == 6 and not data_changed.get('call_count_6_mail'):
                return True
            if data_changed.get('sp_call_status') == self.env.ref(
                "isha_sp_registration.sp_call_status_call_completed").id \
                and data_changed.get('sp_pre_reg_status') == \
                self.env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending").id \
                and not data_changed.get('ie_pending_mail_sent'):
                # self.env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending_call_completed").id \
                return True
        else:
            return False

    def send_sp_mail(self, template_id):
        _logger.info("Send SP Mail: template id :- %s. record id :- %s", str(template_id), str(self))
        mail_values = template_id.generate_email(self.id, fields=None)
        self.env['mail.mail'].sudo().create(mail_values).send(False)
        self.last_automated_email_sent_date = fields.Datetime.now()

    @api.constrains('sp_pre_reg_status', 'ie_completed_post_registration', 'ie_status')
    def _constrains_all_status_set(self):
        for record in self:
            # if record.sp_pre_reg_status.id == self.env.ref('isha_sp_registration.sp_pre_reg_status_all_set_call_completed').id:
            if record.sp_pre_reg_status.id == self.env.ref('isha_sp_registration.sp_pre_reg_status_all_set').id:
                is_all_set_allowed = False
                if record.ie_completed_post_registration == 'yes' or \
                    record.ie_completed_post_registration == 'incorrect' and record.ie_status == 'not_completed' or \
                    record.ie_completed_post_registration is False and record.ie_status == 'completed':
                    is_all_set_allowed = True
                if not is_all_set_allowed:
                    raise exceptions.ValidationError('Pre-registration can not be set to All set')
            # if record.sp_pre_reg_status.id == self.env.ref('isha_sp_registration.sp_pre_reg_status_ie_pending_call_completed').id:
            if record.sp_pre_reg_status.id == self.env.ref('isha_sp_registration.sp_pre_reg_status_ie_pending').id:
                is_ie_pending_allowed = False
                if record.ie_status == 'completed' and record.ie_completed_post_registration == 'incorrect' or \
                    record.ie_status == 'only_ie_online' and record.ie_completed_post_registration is False or \
                    record.ie_status == 'not_completed' and record.ie_completed_post_registration is False:
                    is_ie_pending_allowed = True
                if not is_ie_pending_allowed:
                    raise exceptions.ValidationError('Pre-registration can not be set to IE Pending')

    @api.onchange('sp_call_status')
    def _onchange_call_status(self):
        for rec in self:
            rec.sp_pre_reg_status = None

    @api.onchange('sp_pre_reg_status')
    def _on_pre_reg_change(self):
        for rec in self:
            # if rec.sp_pre_reg_status.id == self.env.ref(
            #     'isha_sp_registration.sp_pre_reg_status_pending_other_call_completed').id or \
            #     rec.sp_pre_reg_status.id == self.env.ref(
            #     'isha_sp_registration.sp_pre_reg_status_pending_other_will_call_back').id:
            if rec.sp_pre_reg_status.id == self.env.ref('isha_sp_registration.sp_pre_reg_status_pending_other').id:
                rec.is_pre_reg_pending_other = True
            else:
                rec.is_pre_reg_pending_other = False
                rec.pending_other_reason = None
            if rec.sp_pre_reg_status.id == self.env.ref('isha_sp_registration.sp_pre_reg_status_cancelled').id:
                rec.is_pre_reg_cancelled = True
                rec.cancel_date = fields.Datetime.today()
                return {
                    'warning': {
                        'title': 'Warning!',
                        'message': 'Do you want to Cancel the Application?'
                    }
                }
            else:
                rec.is_pre_reg_cancelled = False
                rec.cancellation_reason = None
                rec.cancel_date = None

    @api.constrains('sp_health_assessment_form_status')
    def _constrains_sp_health_assessment_form_status(self):
        for record in self:
            if record.application_stage.id == self.env.ref('isha_sp_registration.application_stage_interview_stage').id \
                and record.sp_health_assessment_form_status and record.sp_health_assessment_form_status == 'ready_screening':
                record.application_stage = self.env.ref(
                    'isha_sp_registration.application_stage_health_assessment_stage').id
            if record.sp_health_assessment_form_status == 'ready_screening':
                record.ha_submission_date = datetime.datetime.today()
            if record.sp_health_assessment_form_status == 'awaiting_info':
                record.awaiting_info_date = datetime.datetime.today()
            if record.sp_health_assessment_form_status == 'reminder_sent':
                record.reminder_sent_date = datetime.datetime.today()

    @api.constrains('sp_profile_email_send_datetime', 'ha_email_sent_date')
    def _update_last_automated_email_sent_date(self):
        for record in self:
            record.last_automated_email_sent_date = fields.Datetime.now()

    def cancel_registration(self):
        return {
            'name': 'Cancel Application',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'cancellation.wizard',
            'view_id': False,
            'target': 'new',
        }

    def match_pair_list_action(self):
        local_context = dict(
            self.env.context,
            search_default_id2=self.partner_id.id,
            default_id2=self.partner_id.id,
            active_id=self.partner_id.id,
            active_ids=[self.partner_id.id]
        )
        return {
            'name': 'Low Match Pairs',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_id': False,
            'views': [(False, 'tree'), (False, 'form')],
            'res_model': 'low.match.pairs',
            'target': 'current',
            'context': local_context,
            'domain': self.LOW_MATCH_PAIR_DOMAIN
        }

    @staticmethod
    def check_new_contact(phase):
        return phase == 'phase4'

    def _create_profile_record(self, values):
        # Create profile record for user
        if not self.sp_profile_id.id:
            self.env['sp.registration.profile'].sudo().create({
                'sp_reg_id': self.id
            })

            values['sp_pre_reg_status_changed_datetime'] = datetime.datetime.now()

    def _create_sso_user_for_profile(self, values):
        # Create User
        try:
            user, is_new_sso_user = ensure_user(self.env, self.partner_id.id, self.sp_email, self.first_name,
                                                self.last_name, self.sp_country_id.code)
            values['is_new_sso_user'] = is_new_sso_user
            values['sso_api_called_success'] = True

            # Send email
            self.sudo().send_sp_mail(self.env.ref('isha_sp_registration.mail_template_registration_full_form_invite'))

            values['sp_profile_status'] = "email_sent"
            values['sp_profile_email_send_datetime'] = datetime.datetime.now()

        except Exception:
            _logger.error("SSO User creation failed.Moving to create SP Profile record.", exc_info=True)

    def _create_sso_user_when_not_there_cron(self):
        # check for which users sso needs to be created
        sso_user_needs_to_be_created_list = self.env['sp.registration'].sudo().search(
            [('sso_api_called_success', '=', False),
             ('sp_pre_reg_status', '=', self.env.ref('isha_sp_registration.sp_pre_reg_status_all_set').id)])
        # ('sp_pre_reg_status', '=', self.env.ref('isha_sp_registration.sp_pre_reg_status_all_set_call_completed').id)])
        # if not call sso_user_util func to create it
        for rec in sso_user_needs_to_be_created_list:
            try:
                user, is_new_sso_user = ensure_user(rec.env, rec.partner_id.id, rec.sp_email, rec.first_name,
                                                    rec.last_name, rec.sp_country_id.code)
                rec.is_new_sso_user = is_new_sso_user
                rec.sso_api_called_success = True

                # Send email
                rec.send_sp_mail(self.env.ref('isha_sp_registration.mail_template_registration_full_form_invite'))

                rec.sp_profile_status = "email_sent"
                rec.sp_profile_email_send_datetime = datetime.datetime.now()

            except Exception:
                _logger.error("SSo login creation failed from cron job", exc_info=True)

    def _create_interview_record(self, values):
        sp_interview_form_id = self.env['sp.interview.form'].sudo().search(
            [('registration_profile_id', '=', self.sp_profile_id.id)])
        if not sp_interview_form_id:
            self.env['sp.interview.form'].sudo().create({
                'registration_profile_id': self.sp_profile_id.id,
                'interview_form_state': 'new',
            })
            values['sp_interview_status'] = 'participant_ready_for_interview'

            # Create interviewer feedback records
            interview_feedbacks = []
            for question in self.env['sp.interviewer.question'].sudo().search([('active', '=', True)]):
                interview_feedbacks.append({
                    'question_id': question.id,
                    'registration_form_id': self.id,
                })
            self.env['sp.interviewer.feedback'].sudo().create(interview_feedbacks)

    def _create_interview_record_import(self, values):
        sp_interview_form_id = self.env['sp.interview.form'].sudo().search(
            [('registration_profile_id', '=', self.sp_profile_id.id)])
        if not sp_interview_form_id:
            self.env['sp.interview.form'].sudo().create({
                'registration_profile_id': self.sp_profile_id.id,
                'interview_form_state': values.get('interview_form_state'),
                'show_button_interviewer': False if values.get('interview_form_state') == 'reviewed' else True
            })

            # Create interviewer feedback records
            interview_feedbacks = []
            for question in self.env['sp.interviewer.question'].sudo().search([('active', '=', True)]):
                interview_feedbacks.append({
                    'question_id': question.id,
                    'registration_form_id': self.id,
                })
            self.env['sp.interviewer.feedback'].sudo().create(interview_feedbacks)

    # def _create_ha_form(self, values):
    #     sp_ha_form_id = self.env['sp.reg.health.assessment'].sudo().search([('sp_reg_id', '=', self.id)])
    #     if not sp_ha_form_id:
    #         sp_interview_form_id = None
    #         sp_interview_form = self.env['sp.interview.form'].sudo().search(
    #             [('registration_profile_id', '=', self.sp_profile_id.id)], limit=1)
    #         if len(sp_interview_form) > 0:
    #             sp_interview_form_id = sp_interview_form.id
    #         self.env['sp.reg.health.assessment'].sudo().create({
    #             'sp_reg_id': self.id,
    #             'interview_form_id': sp_interview_form_id,
    #             'sp_reg_profile_id': self.sp_profile_id.id,
    #             'screening_doctor_decision': values['review_doctor_decision'],
    #             'review_doctor_decision': values['review_doctor_decision']
    #         })

    def get_sso_password_reset_link(self):
        return get_sso_account_password_reset_link(self.sp_email, self.sp_country_id.code)

    def _create_interviewed(self):
        _logger.info("Create new user")
        crm_config = Configuration('CRM')
        with open(os.path.join(crm_config['CRM_ASSETS_PATH'], 'sp_users.csv'), mode='r') as csv_file:
            csv_reader = csv.DictReader(csv_file)
            _logger.info("Create partner in CSV User")
            for row in csv_reader:
                x = json.dumps(row)
                user_list = eval(x)
                contact_processor = IshaVolunteer()
                _logger.info("Partner Name %s", user_list['first_name'])
                contact_flag = self.env['res.partner'].sudo().create_internal_flag(
                    contact_processor.getOdooFormat({'first_name': user_list['first_name'], 'last_name': None, 'sp_email': user_list['email']}, {}, {}))
                res_partner_id = contact_flag['rec'][0].id
                _logger.info("Created partner in ishagam Partner Id %s", res_partner_id)
                sp_interviewed = self.env['res.users'].sudo().search(['|', ('partner_id', '=', res_partner_id), ('login', '=', user_list['email'])])
                if not sp_interviewed:
                    _logger.info("Start group mapping")
                    if user_list['user_type'] == 'interviewer':
                        user_group_id = self.env.ref('isha_sp_registration.group_sp_appointment_interviewer').id
                    else:
                        user_group_id = self.env.ref('isha_sp_registration.group_sp_ha_doctor_approval').id
                    try:
                        _logger.info("Group Id %s", user_group_id)
                        user_group_interviewer = [user_group_id, self.env.ref('base.group_user').id]

                        self.env['res.users'].sudo().create({
                            'partner_id': res_partner_id,
                            'login': user_list['email'],
                            'groups_id': [(6, 0, user_group_interviewer)]
                        })
                    except Exception as ex:
                        error_msg = "Create user"
                        _logger.error(error_msg, exc_info=True)
                        raise ex
                    _logger.info("created group mapping")


class ApplicationStage(models.Model):
    _name = 'application.stage'
    _description = 'Application Stages'
    _order = 'sequence'

    _sql_constraints = [('code_unique', 'UNIQUE (code)', 'You can not have status with the same code!')]

    name = fields.Char(required=True)
    code = fields.Char(required=True)
    sequence = fields.Integer(default=5, index=True)
    fold = fields.Boolean(default=False)
    active = fields.Boolean(required=True, default=True)


class ApplicationStageRoute(models.Model):
    _name = "application.stage.route"
    _description = "Application Stage Route"
    _sql_constraints = [
        ('stage_stage_from_to_type_uniq',
         'UNIQUE (stage_from_id, stage_to_id)',
         'Such route already present in this Application type')
    ]

    stage_from_id = fields.Many2one('application.stage', 'From', ondelete='restrict', required=True, index=True,
                                    track_visibility='onchange')
    stage_to_id = fields.Many2one('application.stage', 'To', ondelete='restrict', required=True, index=True,
                                  track_visibility='onchange')
    allowed_group_ids = fields.Many2many('res.groups', string='Allowed groups')
    allowed_user_ids = fields.Many2many('res.users', string='Allowed users')
    active = fields.Boolean(default=True, index=True)


class WhySadhanapada(models.Model):
    _name = 'why.sadhanapada'
    _description = 'Why Sadhanapada'
    _order = 'sequence'

    name = fields.Char()
    code = fields.Char()
    sequence = fields.Integer(default=1, index=True)

    @api.constrains('code')
    def _check_code_constraint(self):
        """ code must be unique """
        for program in self.filtered(lambda p: p.code):
            domain = [('id', '!=', program.id), ('code', '=', program.code)]
            if self.search(domain):
                raise ValidationError(_('The code must be unique!'))


class AboutSadhanapada(models.Model):
    _name = 'about.sadhanapada'
    _description = 'About Sadhanapada'
    _order = 'sequence'

    name = fields.Char()
    code = fields.Char()
    sequence = fields.Integer(default=1, index=True)

    @api.constrains('code')
    def _check_code_constraint(self):
        """ code must be unique """
        for program in self.filtered(lambda p: p.code):
            domain = [('id', '!=', program.id), ('code', '=', program.code)]
            if self.search(domain):
                raise ValidationError(_('The code must be unique!'))


class RegistrationStatus(models.Model):
    _name = 'registration.status'
    _description = 'Registration Status'

    _sql_constraints = [
        ('code_unique', 'UNIQUE (code,status_type)', 'You can not have status with the same code in this type!')]

    name = fields.Char(string='Status', required=True)
    code = fields.Char(required=True)
    active = fields.Boolean(string='Active', required=True, default=True)
    status_type = fields.Many2many('registration.status.type', string='Type', required=True)


class RegistrationStatusType(models.Model):
    _name = 'registration.status.type'
    _description = 'Registration Status Type'

    _sql_constraints = [('name', 'UNIQUE (name)', 'You can not have two statuses with the same name!')]

    name = fields.Char('Name', required=True)
    active = fields.Boolean(string='Is Active', default=True)


class FeedbackType(models.Model):
    _name = 'feedback.type'
    _description = 'Feedback Type'
    _order = 'id desc'

    name = fields.Char('Type', required=True)


class BspLanguage(models.Model):
    _name = 'bsp.language'
    _description = 'BSP Language'

    name = fields.Char('Language', required=True)
    code = fields.Char(string='Locale Code', required=True, help='This field is used to set/get locales for user')
    iso_code = fields.Char(string='ISO code', help='This ISO code is the name of po files to use for translations')
    active = fields.Boolean(string='Is Active', default=True)


class RegistrationFeedback(models.Model):
    _name = 'registration.feedback'
    _description = 'Registration Feedback'
    _rec_name = 'value'
    _order = 'id desc'

    user_id = fields.Many2one('res.users', string='Feedback By', default=lambda self: self.env.user.id, readonly=True)
    registration_id = fields.Many2one('sp.registration')
    health_assessment_id = fields.Many2one('sp.reg.health.assessment', string='Health Assessment', ondelete='cascade')
    previous_status = fields.Many2one('application.stage', string='Previous Status')

    # trial_period_id = fields.Many2one('sp.reg.trial.period', string='Trial Period', ondelete='cascade')

    def _default_feedback_type(self):
        if self._context.get('call_queue'):
            return self.env.ref('isha_sp_registration.feedback_type_call').id
        else:
            if self._context.get('HA_registration_feedback'):
                return self.env.ref('isha_sp_registration.feedback_type_screen_doc_seva_team_comments').id
            else:
                return None

    feedback_type = fields.Many2one('feedback.type', default=_default_feedback_type)
    value = fields.Text(string='Feedback')
    is_from_import = fields.Boolean(default=False)
    # allowed_users = fields.Many2one('res.partner', string='Allowed Users')
    interest_id = fields.Char(store=False)
    interviewed_by_suvya = fields.Char(string='Interviewed By')

    @api.model
    def create(self, values):
        if values.get('is_from_import'):
            registration = self.env['sp.registration'].search([('interest_id', '=', values['interest_id'])], limit=1)
            # feedback_type = self.env['feedback.type'].search([('name', '=', values['feedback_type'])], limit=1)
            if registration:
                values['registration_id'] = registration.id
                # values['feedback_type'] = feedback_type.id
            else:
                return
        return super(RegistrationFeedback, self).create(values)

    @api.constrains('feedback_type')
    def _feedback_type(self):
        for rec in self:
            if rec.feedback_type.id == self.env.ref('isha_sp_registration.feedback_type_reopen').id:
                rec.registration_id.update({'application_stage': rec.previous_status})
                rec.registration_id.cancellation_mail_sent = False
            if rec.registration_id.trial_period_status == 'ind_meeting_scheduled' and \
                rec.feedback_type.id == self.env.ref(
                'isha_sp_registration.feedback_type_tp_individual_meeting_comments').id:
                if not rec.value:
                    raise ValidationError('Please add feedback comments')
                rec.registration_id.trial_period_status = 'ind_meeting_completed'


class CancellationReason(models.Model):
    _name = 'cancellation.reason'
    _description = 'Cancellation Reason'

    _sql_constraints = [('name', 'UNIQUE (name)', 'You can not have two reasons with the same name!')]

    name = fields.Char('Reason', required=True)
    registration_id = fields.One2many('sp.registration', 'cancellation_reason')


class PendingOtherReason(models.Model):
    _name = 'pending.other.reason'
    _description = 'Pending Other Reasons'

    _sql_constraints = [('name', 'UNIQUE (name)', 'You can not have two reasons with the same name!')]

    name = fields.Char('Reason', required=True)


class CallStatus(models.Model):
    _name = 'sp.call.status'
    _description = 'SP Call Status'

    _sql_constraints = [
        ('name', 'UNIQUE (name)', 'You can not have two statuses with the same name!'),
        ('code', 'UNIQUE (code)', 'Code must be unique!')
    ]

    name = fields.Char('Name', required=True)
    code = fields.Char(required=True)
    active = fields.Boolean(string='Is Active', default=True)


class PreRegStatus(models.Model):
    _name = 'sp.pre.reg.status'
    _description = 'SP Pre-Reg Status'

    _sql_constraints = [
        ('name', 'UNIQUE (name, call_status)', 'You can not have two statuses with the same name!'),
        ('code', 'UNIQUE (code)', 'Code must be unique!')
    ]

    name = fields.Char('Name', required=True)
    code = fields.Char(required=True)
    call_status = fields.Many2many('sp.call.status', string='Call Status', required=True)
    active = fields.Boolean(string='Is Active', default=True)


class SpRegistrationSettings(models.Model):
    _name = 'sp.registration.settings'
    _description = 'SP Registration Settings'

    name = fields.Char('Title', required=True, default='Sadhanapada at Isha Yoga Center, 2021')
    thank_message_1 = fields.Text(required=True,
                                  default='Thank you for expressing your interest in Sadhanapada at Isha Yoga Center, 2021.')
    thank_message_2 = fields.Text(required=True, default='Your Sadhanapada application id is')
    thank_message_3 = fields.Text(required=True,
                                  default='. A volunteer will be in touch with you as soon as the application is available.')

    thank_message_already_1 = fields.Text(required=True,
                                          default='We have already received your application for Sadhanapada at Isha Yoga Center, 2021.')
    thank_message_already_end = fields.Text(required=True, default='. A volunteer will be in touch with you soon.')
    active = fields.Boolean(default=True)

    @api.constrains('active')
    def _change_active(self):
        for record in self:
            if record.active:
                record.search([('id', '!=', record.id), ('active', '=', True)]).active = False
                return {
                    'type': 'ir.actions.client',
                    'tag': 'reload'
                }
