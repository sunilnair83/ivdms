# -*- coding: utf-8 -*-
from odoo import models, fields


class BreakPracticeReasons(models.Model):
    _name = 'break.practice.reasons'
    _description = 'Break in Practice Reasons'

    name = fields.Char(string='Batch', required=True)
    active = fields.Boolean(string='Active', default=True)
