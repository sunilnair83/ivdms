# -*- coding: utf-8 -*-
NUMBER_OF_PROFILE_FORM_SECTIONS = 4
NUMBER_OF_HEALTH_FORM_SECTIONS = 5
GENDER_SELECTION = {
    'male': 'Male',
    'female': 'Female',
    'other': 'Others'
}

MARITAL_STATUS = {
    'unmarried': 'Unmarried',
    'married': 'Married',
    'divorced': 'Divorced',
    'widow': 'Widow',
    'widower': 'Widower',
    'separated': 'Separated'
}

# Health assessment constants

ALCOHOL_TYPES = {
    'beer': 'Beer',
    'whisky': 'Whisky',
    'rum': 'Rum',
    'vodka': 'Vodka',
    'wine': 'Wine',
    'others': 'Other'
}

SUBSTANCE_TYPES = {
    'marijuana': 'Marijuana',
    'lsd': 'LSD',
    'cocaine': 'Cocaine',
    'ecstasy': 'Ecstasy',
    'injectable': 'Injectable',
    'others': 'Others'
}

TIME_UNIT = {
    'day': 'Day(s)',
    'week': 'Week(s)',
    'month': 'Month(s)',
    'year': 'Year(s)'
}

BOOLEAN_VALUES = {
    'yes': 'Yes',
    'no': 'No'
}
