import logging
from requests_toolbelt.utils import formdata
import pytz
import requests

from odoo import fields, models, api, _
from odoo.exceptions import ValidationError


class SpSMSRecipients(models.Model):
    _name = 'sp.sms.recipients'
    _description = 'SMS Recipients'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'state'
    _order = 'send_date DESC'

    sms_title = fields.Many2one('sp.sms.text', string='SMS Title Text',
                                domain="['&',('active','=','True'),('state','=','ready')]")

    sp_reg_ids = fields.Many2many('sp.registration', string='Registration ID', required=True)
    send_date = fields.Datetime(string='Send Date', default=fields.Datetime.now(), readonly=True)

    title_body = fields.Text(related='sms_title.body', invisible="1")
    body = fields.Text('Message Body', required=True)
    state = fields.Selection([('new', 'New'),
                              ('sent', 'Sent')], 'SMS Status', readonly=True, copy=False, default='new', required=True)
    response = fields.Char('Response')
    sp_sms_details = fields.One2many('sp.sms.recipients.status', inverse_name='sms_recipient_id',
                                     string='SMS Recipient statuses', readonly=True)

    @api.onchange('sms_title')
    def onchange_sms_title(self):
        self.body = self.title_body

    @api.model
    def create(self, vals_list):
        logger = logging.getLogger('Isha-SMS-Logger')
        logger.info('::SMS Recipients record creation - started::')
        sp_sms_recipients_status_det = self.env['sp.sms.recipients.status'].sudo()

        try:
            sp_sms_recipients_record = super(SpSMSRecipients, self).create(vals_list)

            if len(sp_sms_recipients_record.sp_reg_ids) == 0:
                raise ValidationError('Recipients must be selected.')

            logger.info('::SMS Recipients record creation - completed::')
            detail_values = {
                'sms_recipient_id': int(sp_sms_recipients_record.id),
                'status_date': sp_sms_recipients_record.send_date,
                'status': '0',
                'response': '0'
            }

            try:
                logger.info('::SMS Recipients Status record creation - started::')
                number = ''
                for x in sp_sms_recipients_record.sp_reg_ids:
                    detail_values['mobile'] = x.full_phone.replace(' ', '')
                    detail_values['sp_reg_ids'] = x.id
                    number += detail_values['mobile'] + ","
                    sp_sms_recipients_status_det.create(detail_values)
            except Exception:
                logger.error("SP SMS recipients record creation exception", exc_info=True)

            logger.info('::SMS Recipients Status record creation - completed::')
            logger.info('::SMS message delivery - started::')

            response = self.send_sms_for_recipients(number[:-1], sp_sms_recipients_record.body)
            # sp_sms_recipients_record.response = response

            if 'ERROR' not in response:
                res = response.split(':')
                JobID = res[1] if len(res) == 2 else response

                sp_sms_recipients_record.state = 'sent'

                timeNow = fields.datetime.now()
                for rec in sp_sms_recipients_record.sp_sms_details:
                    rec.response = JobID
                    rec.status_date = timeNow

                logger.info(
                    'sms object status changed to sent for SMS Recipients ID :' + str(sp_sms_recipients_record.id))
            else:

                logger.info('error while sending the sms :' + response + ' with ID:' + str(sp_sms_recipients_record.id))

            logger.info('::SMS message delivery - completed::')

            return sp_sms_recipients_record
        except Exception:
            logger.error("SP SMS recipients record creation exception", exc_info=True)

    def send_sms_for_recipients(self, number, body):
        sms_config = self.env['sp.sms.configuration'].search([], limit=1, order="id desc")
        url = sms_config['provider_url']
        params = {'User': sms_config['provider_username'],
                  'passwd': sms_config['provider_password'],
                  'mobilenumber': number,
                  'message': body,
                  'sid': sms_config['sender_id'],
                  'mtype': 'N', 'DR': 'Y'}
        data = formdata.urlencode(params)
        try:
            result = requests.post(
                url,
                data=data.encode('utf-8'),
                headers={
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            )
            result = requests.get(url, params)
        except Exception as e:
            self.response = result.text
            self._raise_query_error(e)
        if 'Invalid Password' in result.text:
            self._raise_query_error('Unexpected issue. Please contact Administrator.')
        return result.text

    def _raise_query_error(self, error):
        raise ValueError('Error with sending sms: %s' % error)

    def sms_delivery(self):
        today = fields.datetime.today()
        fmt = "%Y-%m-%d %H:%M:%S"
        user_tz = pytz.timezone('Asia/Kolkata')
        today = user_tz.localize(today, fmt).astimezone(pytz.utc)
        sms_config = self.env['sp.sms.configuration'].search([], limit=1, order="id desc")
        url = sms_config['provider_url_for_status']

        params = {'User': sms_config['provider_username'],
                  'passwd': sms_config['provider_password'],
                  'fromdate': today.strftime("%d/%m/%Y 00:00:01"),  # today.strftime("%d/%m/%Y %H:%M:%S"),
                  'todate': today.strftime("%d/%m/%Y 23:59:59")
                  }
        try:
            result = requests.get(url, params)
            if 'Invalid Password' in result.text:
                self._raise_query_error('Unexpected issue. Please contact Administrator.')
            if result.text:
                response = result.text.split('#')
                for var in response:
                    oneset = var.split('~')
                    if len(oneset) > 4:
                        sms_details = self.env['sp.sms.recipients.status'].search(
                            [('response', '=', oneset[0]), ('mobile', '=', oneset[1]), '|', ('status', '=', '0'),
                             ('status', '=', '1')]
                        )

                        if sms_details:
                            sms_details.update({'status': oneset[2]})
        except Exception as e:
            self._raise_query_error(e)
