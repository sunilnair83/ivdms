from odoo import fields, models, api, _


class SpSMSText(models.Model):
    _name = 'sp.sms.text'
    _description = 'SMS Message Text'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = "title"
    _order = 'active DESC'

    _sql_constraints = [('title_unique', 'UNIQUE (title)', 'You can not have two similar titles!')]

    title = fields.Char("Title", required=True)
    body = fields.Text()
    state = fields.Selection([('draft', 'Draft'),
                              ('ready', 'Ready')], 'SMS Text State', default='draft', required=True)  # readonly=True,

    active = fields.Boolean(string='Is Active', default=True)
