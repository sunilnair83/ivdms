import datetime

from odoo import models, fields


class MailTemplate(models.Model):
    _inherit = "mail.template"

    mail_type = fields.Selection([('info', 'Info email'), ('reminder', 'Reminder email')])


class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    mail_type = fields.Selection(related='template_id.mail_type')

    def _filter_templates(self):
        domain = "[('model', '=', model)]"
        if self._context.get('default_model') and self._context.get('default_model') == 'sp.reg.health.assessment' and \
            len(self._context.get('active_ids')) == 1:
            record = self.env['sp.reg.health.assessment'].browse(self._context['active_ids'])
            if record[0].sp_health_assessment_form_status in ['awaiting_info', 'reminder_sent']:
                domain = [('model', '=', 'sp.reg.health.assessment'), ('mail_type', '=', 'reminder')]
        return domain

    template_id = fields.Many2one('mail.template', 'Use template', index=True, domain=_filter_templates)

    def action_send_mail(self):
        if self._context.get('default_model'):
            records = self.env[self._context.get('default_model')].browse(self._context.get('active_ids'))
            if self._context.get('default_model') == 'sp.reg.health.assessment':
                for record in records:
                    if record.sp_health_assessment_form_status != 'reviewed':
                        if self.template_id.mail_type == 'info':
                            record.write({'sp_health_assessment_form_status': 'awaiting_info'})
                        if self.template_id.mail_type == 'reminder':
                            record.write({'sp_health_assessment_form_status': 'reminder_sent'})

            if self._context.get('default_model') == 'sp.final.approval':
                for record in records:
                    if self._context.get('final_approval_mail'):
                        record.sudo().write({
                            'application_stage': self.env.ref('isha_sp_registration.application_stage_approved').id,
                            'final_approval_email_send': 'sent',
                        })
                    if self._context.get('final_reject_mail'):
                        record.sudo().write({
                            'application_stage': self.env.ref('isha_sp_registration.application_stage_rejected').id,
                            'final_rejection_email_send': 'sent',
                        })
            if self._context.get('default_model') == 'sp.registration':
                for record in records:
                    record.last_bulk_email_sent_date = datetime.datetime.now()
                    record.last_bulk_email_sent_by = self.env.user.id
        return super(MailComposer, self).action_send_mail()
