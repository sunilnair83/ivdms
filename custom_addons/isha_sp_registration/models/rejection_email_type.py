# -*- coding: utf-8 -*-

import logging

from odoo import fields, models

_logger = logging.getLogger(__name__)


class RejectionEmailType(models.Model):
    _name = 'rejection.email.type'
    _description = "Rejection Email Type"
    _order = 'id desc'

    _sql_constraints = [('name_unique', 'UNIQUE (name)', 'You can not have two types with the same name!')]

    name = fields.Char()
    active = fields.Boolean(string='Active', default=True)
