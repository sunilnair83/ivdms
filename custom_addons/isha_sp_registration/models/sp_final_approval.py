# -*- coding: utf-8 -*-
import datetime
import json

import phpserialize
from lxml import etree

from odoo import models, fields, api, _, exceptions
from odoo.exceptions import ValidationError
from . import constants


class SpFinalApproval(models.Model):
    _name = 'sp.final.approval'
    _description = 'SP Final Approval'
    _order = 'interview_opinion_on desc'

    sp_interview_id = fields.Many2one('sp.interview.form', string='Interview', delegate=True, required=True,
                                      readonly=True)

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(SpFinalApproval, self).fields_get(allfields, attributes=attributes)
        if res.get('low_matches_count'):
            res['low_matches_count']['searchable'] = False
        if res.get('sp_interview_status'):
            res['sp_interview_status']['searchable'] = False
        if res.get('interview_status'):
            res['interview_status']['searchable'] = False
        return res

    def cancel_registration(self):
        return {
            'name': 'Cancel Application',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'cancellation.wizard',
            'view_id': False,
            'target': 'new',
            'context': {
                'final_approval': True,
                'sp_interview_id': self.sp_interview_id.id,
            },
        }

    def button_approve_with_email(self):
        return self.final_approval_send_email()

    def final_approval_send_email(self):
        return {
            'name': 'Send email',
            'res_model': 'mail.compose.message',
            'view_mode': 'form',
            'view_id': self.env.ref('isha_sp_registration.sp_final_approval_email_compose_message_wizard_form').id,
            'type': 'ir.actions.act_window',
            'context': {
                'default_composition_mode': 'mass_mail',
                'default_partner_to': self.partner_id.ids or '',
                'default_use_template': True,
                'default_model': self._name,
                'active_ids': self.ids,
                'default_notify': False,
                # 'default_reply_to': 'sadhanapada.office@ishafoundation.org',
                'default_template_id': self.env.ref('isha_sp_registration.mail_template_sp_final_approval').id,
                'final_approval_mail': True,
            },
            'target': 'new',
        }

    def button_approve_without_email(self):
        self.sudo().write({
            'application_stage': self.env.ref('isha_sp_registration.application_stage_approved').id,
            'final_approval_email_send': 'not_sent',
        })

    def button_reject_with_email(self):
        return self.button_reject_send_email()

    def button_reject_send_email(self):
        return {
            'name': 'Send email',
            'res_model': 'mail.compose.message',
            'view_mode': 'form',
            'view_id': self.env.ref('isha_sp_registration.sp_final_approval_email_compose_message_wizard_form').id,
            'type': 'ir.actions.act_window',
            'context': {
                'default_composition_mode': 'mass_mail',
                'default_partner_to': self.partner_id.ids or '',
                'default_use_template': True,
                'default_model': self._name,
                'active_ids': self.ids,
                'default_notify': False,
                # 'default_reply_to': 'sadhanapada.office@ishafoundation.org',
                'default_template_id': self.env.ref('isha_sp_registration.mail_template_sp_final_reject').id,
                'final_reject_mail': True,
            },
            'target': 'new',
        }

    def button_reject_without_email(self):
        self.sudo().write({
            'application_stage': self.env.ref('isha_sp_registration.application_stage_rejected').id,
            'final_rejection_email_send': 'not_sent',
        })

    def button_approve_email(self):
        return self.final_approval_send_email()

    def button_reject_email(self):
        return self.button_reject_send_email()

    def button_add_comment(self):
        return {
            'name': 'Add Comment',
            'view_mode': 'form',
            'res_model': 'interview.comments.wizard',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': {
                'final_approval': True,
            },
            'target': 'new',
        }
