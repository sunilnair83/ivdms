# -*- coding: utf-8 -*-

from odoo import models, fields


class SpInterviewerFeedback(models.Model):
    _name = 'sp.interviewer.feedback'
    _description = 'SP Interviewer Feedback'
    _rec_name = 'question_id'

    _sql_constraints = [
        ('feedback_unique', 'UNIQUE (question_id, registration_form_id)',
         'Question already exists for the Interview form!')
    ]

    question_id = fields.Many2one('sp.interviewer.question', required=True)
    # todo remove after data migration
    interview_form_id = fields.Many2one('sp.interview.form')
    registration_form_id = fields.Many2one('sp.registration', required=True)
    answer = fields.Text(string='Answer')
    is_mandatory = fields.Boolean(related='question_id.is_mandatory')
