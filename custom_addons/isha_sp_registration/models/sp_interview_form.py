# -*- coding: utf-8 -*-

import datetime
import json
from lxml import etree

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class SpInterviewForm(models.Model):
    _name = 'sp.interview.form'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'generic.mixin.track.changes']
    _description = 'SP Interview Form'
    _order = 'interview_done_on desc'
    _rec_name = 'sp_name'

    # _sql_constraints = [('registration_profile_id_uniq', 'UNIQUE (registration_profile_id)',
    #                      'Registration Profile form stage already exists!')]

    registration_profile_id = fields.Many2one('sp.registration.profile', string='Applicant', delegate=True,
                                              required=True)

    health_assessment_id = fields.One2many('sp.reg.health.assessment', 'interview_form_id')
    review_doctor_decision = fields.Selection(related='health_assessment_id.review_doctor_decision', readonly=True)

    interview_follow_up_status = fields.Selection([('no_followup', 'No Follow-up'),
                                                   ('followup_required', 'Follow-up required'),
                                                   ('followup_completed', 'Follow-up Completed')],
                                                  string="Interview Follow-up Status", default='no_followup',
                                                  required=True)
    is_interviewer_editable = fields.Boolean("Is Editable By Interviewer", compute='_compute_interviewer_editable')

    def _compute_interviewer_editable(self):
        is_interviewer = self.env.user.has_group('isha_sp_registration.group_sp_appointment_interviewer')
        for record in self:
            record.is_interviewer_editable = is_interviewer

    interviewer_editable_css = fields.Html(string='CSS', sanitize=False, compute='_compute_editable_css', store=False)

    follow_by = fields.Many2one('res.users', 'Follow up By', readonly=True)
    show_button_interviewer = fields.Boolean(default=True)
    show_ready_for_review_button = fields.Boolean()
    show_button_reviewer = fields.Boolean()
    show_button_need_time = fields.Boolean(default=True)
    is_feedback_received = fields.Boolean()
    is_followup_required = fields.Boolean()

    # Trial form
    trial_period_id = fields.One2many('sp.reg.trial.period', 'sp_interview_id')

    def _compute_editable_css(self):
        for rec in self:
            if self.env.user.has_group('isha_sp_registration.group_sp_appointment_interviewer') and \
                rec.interview_form_state not in ['new', 'feedback_pending', 'feedback_received', 'awaiting_info'] \
                or self.is_application_stage_cancelled or self.env.context.get('is_form_readonly'):
                rec.interviewer_editable_css = '<style>.o_form_button_edit {display: none !important;}</style>'
            else:
                rec.interviewer_editable_css = None

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(SpInterviewForm, self).fields_get(allfields, attributes=attributes)
        if res.get('low_matches_count'):
            res['low_matches_count']['searchable'] = False
        if res.get('sp_interview_status'):
            res['sp_interview_status']['searchable'] = False
        if res.get('interview_status'):
            res['interview_status']['searchable'] = False
        return res

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        result = super(SpInterviewForm, self).fields_view_get(view_id, view_type, toolbar, submenu)
        sp_admin = self.user_has_groups('isha_sp_registration.group_sp_registration_admin')
        if view_type == 'form':
            doc = etree.XML(result['arch'])
            if not sp_admin:
                for node in doc.xpath("//field[@name='photo']"):
                    attr = {
                        'readonly': 1
                    }
                    if node.attrib.get('modifiers'):
                        attr = json.loads(node.attrib.get('modifiers'))
                        attr['readonly'] = 1
                    node.set('modifiers', json.dumps(attr))
            result['arch'] = etree.tostring(doc)
        # Display interview form in readonly mode in Previous Application view
        if self.env.context.get('is_form_readonly') and view_type == 'form':
            doc = etree.XML(result['arch'])
            for node in doc.xpath("//button"):
                attr = {
                    'invisible': 1
                }
                if node.attrib.get('modifiers'):
                    attr = json.loads(node.attrib.get('modifiers'))
                    attr['invisible'] = 1
                node.set('modifiers', json.dumps(attr))
            result['arch'] = etree.tostring(doc)
        return result

    @api.model
    def create(self, values):
        return super(SpInterviewForm, self).create(values)

    def write(self, values):
        res = super(SpInterviewForm, self).write(values)
        for record in self:
            if values.get('interview_concerns') or values.get('interviewer_opinion') or \
                values.get('interviewer_feedback_ids'):
                if record.interview_concerns and record.interviewer_opinion:
                    interviewer_feedback_given = True
                    for interviewer_feedback_id in record.interviewer_feedback_ids:
                        if interviewer_feedback_given and interviewer_feedback_id.question_id.is_mandatory and \
                            not interviewer_feedback_id.answer:
                            interviewer_feedback_given = False
                    if interviewer_feedback_given:
                        if record.interview_form_state != 'feedback_received':
                            record.interview_form_state = 'feedback_received'
                        # record.show_ready_for_review_button = True
                    else:
                        if record.interview_form_state != 'feedback_pending':
                            record.interview_form_state = 'feedback_pending'
                        # record.show_ready_for_review_button = False
                else:
                    if record.interview_form_state != 'feedback_pending':
                        record.interview_form_state = 'feedback_pending'
                    # record.show_ready_for_review_button = False
        return res

    @api.constrains('interview_form_state')
    def _change_interview_form_state(self):
        for record in self:
            if record.interview_form_state == "ready_review":
                record.show_button_reviewer = True
                record.sp_interview_status = "ready_for_review"
                if record.sp_health_assessment_form_status == 'form_submitted':
                    record.sp_health_assessment_form_status = 'ready_screening'
                    record.sp_health_assessment_state = 'ready_screening'

    @api.onchange('tested_covid_positive')
    def _onchange_tested_covid_positive(self):
        for record in self:
            if record.tested_covid_positive == 'no':
                record.covid_positive_tested_date = None

    @api.constrains('tested_covid_positive', 'covid_positive_tested_date')
    def _validate_tested_positive_date(self):
        for record in self:
            if record.tested_covid_positive == 'yes' and not record.covid_positive_tested_date:
                raise ValidationError('Please enter valid date for covid positive testing')
            if record.tested_covid_positive == 'no':
                record.covid_positive_tested_date = None
            if record.tested_covid_positive == 'yes' and record.covid_positive_tested_date and record.covid_positive_tested_date > datetime.date.today():
                raise ValidationError("'Covid Positive Tested Date' can not be future date")

    @api.constrains('interview_reviewer_decision')
    def _change_interview_reviewer_decision(self):
        for record in self:
            if record.interview_reviewer_decision:
                record.reviewed_on = datetime.datetime.now()
                record.reviewed_by = self.env.uid

    @api.constrains('interviewer_opinion')
    def _change_interviewer_opinion(self):
        for record in self:
            if record.interviewer_opinion:
                record.show_ready_for_review_button = True
                record.interview_opinion_on = datetime.datetime.now()
                record.interview_opinion_by = self.env.uid
                if not record.interview_done_by:
                    record.interview_done_by = record.interview_opinion_by
                if not record.interview_done_on:
                    record.interview_done_on = record.interview_opinion_on

            if record.env.context.get('active_model') and record.env.context.get('active_model') == 'sp.appointment' \
                and record.interviewer_opinion:
                self.env['sp.appointment'].browse(record.env.context.get('active_id')).sudo().write({
                    'state': 'completed',
                })
            if record.application_stage == self.env.ref('isha_sp_registration.application_stage_registration_stage'):
                record.application_stage = self.env.ref('isha_sp_registration.application_stage_interview_stage').id
            # record.interview_status = self.env.ref('isha_sp_registration.registration_status_selected').id

    # commenting Trial period form creation as we are expecting changes in this section
    #  @api.constrains('interviewer_opinion', 'sp_health_assessment_form_status')
    #   def _on_interviewer_opinion_change(self):
    #      for record in self:
    #        if record.interviewer_opinion == 'selected_with_tp' \
    #             or record.interviewer_opinion == 'selected_with_tp_ha' \
    #              or record.sp_health_assessment_form_status == 'evaluate_after_trial':
    #             if record.application_stage.id == self.env.ref(
    #                'isha_sp_registration.application_stage_interview_stage').id:
    #                 record.application_stage = self.env.ref(
    #                     'isha_sp_registration.application_stage_approval_after_trial').id
    #          # Create trial record
    #          if not record.trial_period_id.id:
    #            self.env['sp.reg.trial.period'].sudo().create({
    #               'sp_interview_id': self.id
    #           })
    #       # Send trial form email
    #       if record.trial_period_status == 'email_not_sent':
    #           template_id = self.env.ref('isha_sp_registration.mail_template_sp_trial_period')
    #           values = template_id.generate_email(record.id, fields=None)
    #           self.env['mail.mail'].sudo().create(values).send(False)
    #           record.trial_period_status = 'email_sent'

    def get_trial_period_url(self):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        url = base_url + "/trial-form/"
        return url

    def send_health_assessment_email(self):
        template_id = self.self.env.ref('isha_sp_registration.mail_template_sp_health_assessment').id
        message = "Are you sure you want to send Health assessment email?"
        if (self.sp_health_assessment_form_status != 'email_sent' or self._context.get('resend')) \
            and not self.is_application_stage_cancelled:
            return {
                'name': 'Confirm Mail Send',
                'view_mode': 'form',
                'res_model': 'confirm.mail.message',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'target': 'new',
                'context': {
                    'default_message': message,
                    'default_template_id': template_id,
                    'health_assessment': True
                },
            }

    def server_action_send_health_assessment_email(self):
        template_id = self.self.env.ref('isha_sp_registration.mail_template_sp_health_assessment').id
        message = "Are you sure you want to send Health assessment email?"
        self.env['confirm.mail.message'].with_context(default_message=message,
                                                      template_id=template_id,
                                                      default_template_id=template_id,
                                                      health_assessment=True).server_action_send_mail()

    # Interviewer Comments
    def capture_interviewer_comment(self, interview_form_state, interviewer_opinion, ha_email=False):
        ctx = {
            'interviewer_comment': True,
            'interview_form_state': interview_form_state,
            'interviewer_opinion': interviewer_opinion,
            'ha_email': ha_email,
            'default_registration_id': self.id,
            'sp_interview_id': self.id,
        }
        return {
            'name': 'Interviewer Comment',
            'view_mode': 'form',
            'res_model': 'interview.comments.wizard',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': ctx,
            'target': 'new',
        }

    # Interviewer Button
    def button_recommend(self):
        return self.capture_interviewer_comment("feedback_pending", "selected", True)

    def button_not_recommend(self):
        return self.capture_interviewer_comment("feedback_pending", "not_selected", False)

    def button_cannot_decide(self):
        return self.capture_interviewer_comment("feedback_pending", "pending", True)

    def button_recommended_with_tp(self):
        return self.capture_interviewer_comment("feedback_pending", "selected_with_tp", True)

    def button_recommended_with_ha(self):
        return self.capture_interviewer_comment("feedback_pending", "selected_with_ha", True)

    def button_recommended_with_tp_ha(self):
        return self.capture_interviewer_comment("feedback_pending", "selected_with_tp_ha", True)

    def button_recommended_with_vro(self):
        return self.capture_interviewer_comment("feedback_pending", "waiting_with_vro", True)

    def button_recommended_with_oco(self):
        return self.capture_interviewer_comment("feedback_pending", "waiting_with_oco", True)

    def button_interviewer_trial_is_must(self):
        return self.capture_interviewer_comment("feedback_pending", "trial_is_must", True)

    def button_ready_for_review(self):
        if not self.is_application_stage_cancelled:
            if not self.interview_concerns:
                raise ValidationError("'Concerns' is missing. In 'Interview' Tab!")
            if not self.application_processing_priority:
                raise ValidationError("'Application Processing Priority' is missing. In 'Interview' Tab!")
            for interviewer_feedback_id in self.interviewer_feedback_ids:
                if interviewer_feedback_id.question_id.is_mandatory and not interviewer_feedback_id.answer:
                    raise ValidationError("Please answer the mandatory questions in 'Interviewer Feedback' Section!")
            if len(self.skills) == 0:
                raise ValidationError("'Skills' is missing. In 'Education & Work Experience' Tab!")
            if not self.medical_comments:
                raise ValidationError("'Medical Comments' is missing. In 'Medical History' Tab!")
            if not self.tested_covid_positive:
                raise ValidationError("'Tested Covid Positive' is missing. In 'Medical History' Tab!")
            if not self.co_morbid_condition:
                raise ValidationError("'Any Co Morbid Condition' is missing. In 'Medical History' Tab!")
            if self.show_button_interviewer:
                self.show_button_interviewer = False
            self.interview_form_state = "ready_review"
            self.show_ready_for_review_button = False
            return self.send_health_assessment_email()

    def button_awaiting_info_from_ppt(self):
        if not self.is_application_stage_cancelled:
            self.show_ready_for_review_button = False
            self.previous_interview_form_state = self.interview_form_state
            self.interview_form_state = "awaiting_info"

    def button_update_info(self):
        if not self.is_application_stage_cancelled:
            return {
                'name': 'Update PPT Info',
                'view_mode': 'form',
                'res_model': 'interview.comments.wizard',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {
                    'awaiting_info': True,
                },
                'target': 'new',
            }

    def button_followup_response(self):
        if not self.is_application_stage_cancelled:
            if self.show_button_interviewer:
                self.show_button_interviewer = False
            return {
                'name': 'Follow-up Response',
                'view_mode': 'form',
                'res_model': 'interview.comments.wizard',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {
                    'followup_response': True,
                },
                'target': 'new',
            }

    # Reviewer Button
    def button_followup_required(self):
        if not self.is_application_stage_cancelled:
            return {
                'name': 'Follow-up Request Comment',
                'view_mode': 'form',
                'res_model': 'interview.comments.wizard',
                'view_id': False,
                'type': 'ir.actions.act_window',
                'context': {
                    'followup_required': True,
                },
                'target': 'new',
            }

    def button_need_time_to_decide(self):
        if not self.is_application_stage_cancelled:
            if self.show_button_need_time:
                self.sudo().write({
                    'show_button_need_time': False
                })
            self.sudo().write({
                'interview_form_state': "decision_pending",
                'sp_interview_status': "reviewer_decision_pending",
            })

    # Reviewer Comments
    def capture_reviewer_comment(self, interview_form_state, interview_reviewer_decision, sp_interview_status):
        ctx = {
            'reviewer_comment': True,
            'interview_form_state': interview_form_state,
            'interview_reviewer_decision': interview_reviewer_decision,
            'sp_interview_status': sp_interview_status,
            'default_registration_id': self.id,
            'sp_interview_id': self.id,
        }
        return {
            'name': 'Reviewer Comment',
            'view_mode': 'form',
            'res_model': 'interview.comments.wizard',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': ctx,
            'target': 'new',
        }

    # Reviewer Inputs
    def button_selected(self):
        return self.capture_reviewer_comment("reviewed", "selected", "selected")

    def button_not_selected(self):
        return self.capture_reviewer_comment("reviewed", "not_selected", "rejected")

    def button_approval_after_tp(self):
        return self.capture_reviewer_comment("reviewed", "approval_tp", "selected_with_tp")

    def button_approval_after_ha(self):
        return self.capture_reviewer_comment("reviewed", "approval_ha", "selected_with_ha")

    def button_approval_after_tp_ha(self):
        return self.capture_reviewer_comment("reviewed", "approval_tp_ha", "selected_with_tp_ha")

    def button_waiting_vro_feedback(self):
        return self.capture_reviewer_comment("awaiting_vro", "waiting_vro_feedback", "awaiting_vro")

    def button_waiting_oco_feedback(self):
        return self.capture_reviewer_comment("awaiting_oco", "waiting_oco_feedback", "awaiting_oco")

    def button_reviewer_trial_is_must(self):
        return self.capture_reviewer_comment("reviewed", "trial_is_must", False)

    def button_add_feedback(self):
        return {
            'name': 'Feedback Comment',
            'view_mode': 'form',
            'res_model': 'interview.comments.wizard',
            'view_id': False,
            'type': 'ir.actions.act_window',
            # 'context': {
            #     'default_registration_id': self.registration_id,
            # },
            'target': 'new',
        }

    # def button_add_oco_feedback(self):
    #     return {
    #         'name': 'OCO Feedback Comment',
    #         'view_mode': 'form',
    #         'res_model': 'interview.comments.wizard',
    #         'view_id': False,
    #         'type': 'ir.actions.act_window',
    #         # 'context': {
    #         #     'default_registration_id': self.registration_id,
    #         # },
    #         'target': 'new',
    #     }

    def button_add_comment(self):
        return {
            'name': 'Add Comment',
            'view_mode': 'form',
            'res_model': 'interview.comments.wizard',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': {
                'comment': True,
            },
            'target': 'new',
        }

    def cancel_registration(self):
        return {
            'name': 'Cancel Application',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'cancellation.wizard',
            'view_id': False,
            'target': 'new',
            'context': {
                'interview_form': True,
                'sp_reg_id': self.registration_profile_id.sp_reg_id.id,
            },
        }
