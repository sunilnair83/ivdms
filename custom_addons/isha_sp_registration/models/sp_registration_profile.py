import json
from lxml import etree

from odoo import models, fields, api
from . import constants


class SpRegistrationFullForm(models.Model):
    _name = 'sp.registration.profile'
    _description = "SP registration profile form"
    _rec_name = 'sp_reg_id'

    sp_reg_id = fields.Many2one('sp.registration', string='SP Name', delegate=True, required=True, ondelete='cascade')

    interview_form_ids = fields.One2many('sp.interview.form', 'registration_profile_id', string='Interview Form')
    # Full form fields
    section_submitted = fields.Integer(string='SP Profile form section submitted', default=0, readonly=True)
    current_name = fields.Char(string='Current name')
    has_name_changed = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    previous_name = fields.Char(string='Previous/Other name')
    is_gazette_updated = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    gazette_details = fields.One2many('sp.registration.profile.gazette.details', 'sp_profile_id',
                                      string='Gazette details')
    spouses_name = fields.Char(string='Spouse Name')
    fathers_name = fields.Char(string='Father Name')
    mothers_name = fields.Char(string='Mother Name')
    religion = fields.Char(string='Religion')
    address = fields.Text('Current address')
    primary_nationality_details = fields.One2many('sp.registration.profile.nationality.details',
                                                  'sp_profile_id', string='Nationality details',
                                                  domain=[('nationality_type', '=', 'primary')])

    # Dual nationality
    is_dual_nationality = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    second_nationality_country_id = fields.Many2one('res.country', string='Country of Second Nationality')
    dual_nationality_details = fields.One2many('sp.registration.profile.nationality.details',
                                               'sp_profile_id', string='Dual Nationality details',
                                               domain=[('nationality_type', '=', 'dual')])
    birth_country_id = fields.Many2one('res.country', string='Birth country')

    medical_comments = fields.Text(string='Medical Comments')

    is_profile_editable_css = fields.Html(string='CSS', sanitize=False, compute='_compute_profile_editable',
                                          store=False)

    def _compute_profile_editable(self):
        for rec in self:
            rec.is_profile_editable_css = '<style>.o_form_button_edit {display: none !important;}</style>' \
                if not self.env.user.has_group('isha_sp_registration.group_sp_registration_admin') else None

    @api.constrains('section_submitted')
    def _on_section_submitted_change(self):
        for record in self:
            if record.section_submitted == constants.NUMBER_OF_PROFILE_FORM_SECTIONS:
                record.sp_profile_status = 'submitted'
            if 0 < record.section_submitted < constants.NUMBER_OF_PROFILE_FORM_SECTIONS:
                record.sp_profile_status = 'partial'

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(SpRegistrationFullForm, self).fields_get(allfields, attributes=attributes)
        if res.get('low_matches_count'):
            res['low_matches_count']['searchable'] = False
        if res.get('sp_interview_status'):
            res['sp_interview_status']['searchable'] = False
        if res.get('interview_status'):
            res['interview_status']['searchable'] = False
        return res

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        result = super(SpRegistrationFullForm, self).fields_view_get(view_id=view_id, view_type=view_type,
                                                                     toolbar=toolbar, submenu=submenu)
        sp_admin = self.user_has_groups('isha_sp_registration.group_sp_registration_admin')

        if view_type == 'kanban':
            for rfield in result['fields']:
                if not sp_admin:
                    result['fields'][rfield]['readonly'] = True
        if view_type == 'form':
            doc = etree.XML(result['arch'])
            for node in doc.xpath("//field"):
                if not sp_admin:
                    attr = {
                        'readonly': 1
                    }
                    if node.attrib.get('modifiers'):
                        attr = json.loads(node.attrib.get('modifiers'))
                        attr['readonly'] = 1
                    node.set('modifiers', json.dumps(attr))
            result['arch'] = etree.tostring(doc)
        return result

    @api.model
    def create(self, values):
        return super(SpRegistrationFullForm, self).create(values)

    def write(self, values):
        return super(SpRegistrationFullForm, self).write(values)


class SpRegistrationFullFormGazetteDetails(models.Model):
    _name = 'sp.registration.profile.gazette.details'
    _description = "Gazette Details"
    # _sql_constraints = [('gazette_details_issue_number_unique', 'UNIQUE(issue_number, sp_profile_id)',
    #                      'Gazette details already added for the provided issue number')]

    sp_profile_id = fields.Many2one('sp.registration.profile', string='SP Profile Id', ondelete='cascade',
                                    required=True)
    issue_number = fields.Char(string='Issue Number')
    issue_date = fields.Date(string='Issue Date')
    page_number = fields.Integer(string='Page Number')


class SpRegistrationFullFormNationalityDetails(models.Model):
    _name = 'sp.registration.profile.nationality.details'
    _description = "Nationality Details"

    sp_profile_id = fields.Many2one('sp.registration.profile', string='SP Profile Id', ondelete='cascade',
                                    required=True)
    nationality_type = fields.Selection([('primary', 'Primary'), ('dual', 'Dual')],
                                        string='Nationality type')
    passport_no = fields.Char(string='Passport Number')
    passport_issue_date = fields.Date(string='Passport issue date')
    passport_expiry_date = fields.Date(string='Passport expiry date')
    passport_issue_city = fields.Char(string='City where Passport was issued')
    passport_issue_country = fields.Many2one('res.country', string='Country where passport was issued')
