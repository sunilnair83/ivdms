# -*- coding: utf-8 -*-

from odoo import models, fields


class SpReviewerSuggestion(models.Model):
    _name = 'sp.reviewer.suggestion'
    _description = 'SP Reviewer Suggestion'

    # _sql_constraints = [('question_unique', 'UNIQUE (name)', 'Question already exists!')]

    name = fields.Char(string='Suggestion', required=True)
    active = fields.Boolean(string='Active', default=True)
    color = fields.Integer('Color Index')
