# -*- coding: utf-8 -*-

import calendar
import datetime
from datetime import timedelta

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class SpAppointment(models.Model):
    _name = 'sp.appointment'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'generic.mixin.track.changes']
    _description = 'SP Appointment'
    _order = 'id desc'

    _sql_constraints = [
        ('registration_unique', 'UNIQUE (registration_id)', 'You can not have two Appointments for same Registration!'),
        ('time_slot_unique', 'UNIQUE (time_slot)', 'Slot already booked!')
    ]

    name = fields.Char(default='New', track_visibility='always')
    registration_id = fields.Many2one('sp.registration', string='SP Applicant', track_visibility='always',
                                      domain="[('registration_batch.status','=','current'),"
                                             "('application_stage.code','=','registration'),"
                                             "'|', ('appointment_state','=','draft'),"
                                             "('appointment_state','=',False),"
                                             "('appointment_ids','=',False)]")
    interviewer_id = fields.Many2one('sp.interviewer', string='Interviewer', track_visibility='always')
    assign_interviewer_id = fields.Many2one('sp.interviewer', string='Assigned Interviewer')
    language_id = fields.Many2one('res.lang')
    time_slot_id = fields.One2many(related='interviewer_id.time_slot_id', string="Interviewer's Time Slots")
    user_id = fields.Many2one(related='interviewer_id.user_id', string="Interviewer's User ID")
    time_slot = fields.Many2one('calendar.event', track_visibility='always')
    time_slot_tags = fields.Many2many(related='time_slot.categ_ids', string='Slot Tags')
    slot_state = fields.Selection(related='time_slot.slot_state', track_visibility='always')
    time_slot_day = fields.Char(readonly=True, track_visibility='always')
    appoint_date = fields.Date(string="Appointment Date", track_visibility='always')
    app_dt_start = fields.Datetime('Start Datetime', track_visibility='always')
    app_dt_stop = fields.Datetime('End Datetime', track_visibility='always')
    duration = fields.Float('Duration', compute='_compute_duration')
    appointment_state = fields.Selection(related='registration_id.appointment_state', string='Status', store=True,
                                         track_visibility='always')
    type = fields.Selection([('regular', 'Regular'),
                             ('marathon', 'Marathon')], required=True, default='regular')
    scheduler_comment_ids = fields.One2many('sp.appointment.comment', 'appointment_id', 'Scheduler Comment',
                                            domain=lambda self: [('type', '=', self.env.ref(
                                                'isha_sp_registration.appointment_comment_type_scheduler').id)])
    interviewer_comment_ids = fields.One2many('sp.appointment.comment', 'appointment_id', 'Interviewer Comment',
                                              domain=lambda self: [('type', '=', self.env.ref(
                                                  'isha_sp_registration.appointment_comment_type_interviewer').id)])
    is_scheduler = fields.Boolean(compute='_compute_user')
    is_interviewer = fields.Boolean(compute='_compute_user')
    is_interviewer_comment = fields.Boolean(compute='_compute_user')
    is_assigned = fields.Boolean(compute='_compute_appointment')
    is_re_assigned = fields.Boolean(compute='_compute_appointment')
    is_re_assigned_by_scheduler = fields.Boolean()
    is_expired_appointment = fields.Boolean(compute='_compute_expired_appointment')
    show_cancel_button = fields.Boolean(related='registration_id.show_cancel_button')
    is_application_stage_cancelled = fields.Boolean(related='registration_id.is_application_stage_cancelled')
    x_css = fields.Html(string='CSS', sanitize=False, compute='_compute_css', store=False)

    upcoming_time_slot_ids = fields.Many2many(
        'calendar.event', compute='compute_upcoming_time_slot',
        default=lambda self: self.env['calendar.event'].search(
            [('start', '>=', fields.Date.today().strftime(DEFAULT_SERVER_DATE_FORMAT)),
             ('slot_state', '!=', 'booked')], order='start asc'))

    def _compute_css(self):
        for rec in self:
            rec.x_css = None
            if self.env.user.has_group('isha_sp_registration.group_sp_appointment_interview_scheduler'):
                rec.x_css = '<style>.o_form_button_edit {display: none !important;}</style>'

    @api.depends('app_dt_start', 'app_dt_stop')
    def _compute_duration(self):
        for record in self:
            if record.app_dt_start and record.app_dt_stop:
                diff = fields.Datetime.from_string(record.app_dt_stop) - \
                       fields.Datetime.from_string(record.app_dt_start)
                record.duration = round(diff.total_seconds() / 60.0, 2)
            else:
                record.duration = 0.0

    def _compute_appointment(self):
        for record in self:
            record.is_assigned = False
            record.is_re_assigned = False
            if self.sudo().appointment_state == 're_assign':
                if record.interviewer_id.user_id == self.env.user or record.is_re_assigned_by_scheduler:
                    record.is_assigned = True
                if record.assign_interviewer_id.user_id == self.env.user or record.is_re_assigned_by_scheduler:
                    record.is_re_assigned = True

    def _compute_expired_appointment(self):
        today = fields.Datetime.today()
        for record in self:
            record.is_expired_appointment = False
            if record.app_dt_start and record.app_dt_start < today:
                record.is_expired_appointment = True

    def _compute_user(self):
        for record in self:
            record.is_interviewer = False
            record.is_interviewer_comment = False
            record.is_scheduler = False
            if self.env.user.has_group('isha_sp_registration.group_sp_appointment_interview_scheduler'):
                record.is_scheduler = True
                if record.interviewer_id.user_id == self.env.user:
                    record.is_interviewer_comment = True
            if self.env.user.has_group('isha_sp_registration.group_sp_appointment_interviewer'):
                record.is_interviewer = True

    def name_get(self):
        res = []
        for rec in self:
            interviewer_name = '[' + rec.interviewer_id.name + '] -' if rec.interviewer_id else ''
            registration_name = ''
            if rec.registration_id and rec.registration_id.name:
                registration_name = '[' + rec.registration_id.name + '] -'
            if rec.appoint_date:
                appoint_date = '[' + str(rec.appoint_date) + '] -'
            elif rec.app_dt_stop:
                appoint_date = '[' + str(rec.app_dt_stop) + '] -'
            else:
                appoint_date = ''
            res.append((rec.id, '%s %s %s %s' % (interviewer_name, registration_name, appoint_date, rec.name)))
        return res

    @api.onchange('appoint_date')
    def _onchange_appoint_date(self):
        for record in self:
            if record.appoint_date and record.appoint_date < fields.Datetime.today().date():
                raise ValidationError("Please select a 'Appointment Date' equal/greater than the current date")

    @api.onchange('app_dt_start', 'app_dt_stop')
    def _onchange_app_dt_start(self):
        for record in self:
            if record.app_dt_start:
                record.app_dt_stop = record.app_dt_start + timedelta(hours=1)
            if record.app_dt_start and record.app_dt_start < fields.Datetime.today():
                raise ValidationError("Please select a 'Appointment Start Date' equal/greater than the current date")
            if record.app_dt_start and record.app_dt_stop and record.app_dt_start >= record.app_dt_stop:
                raise ValidationError('The start date must be before the end date')

    @api.onchange('interviewer_id', 'time_slot', 'appoint_date', 'language_id')
    def onchange_domain(self):
        domain = {'time_slot': [('interviewer_id', '!=', False),
                                ('start', '>=', datetime.datetime.today()),
                                ('slot_state', 'not in', ['booked'])],
                  'interviewer_id': []}
        if self.language_id:
            domain['interviewer_id'] += [('language_ids', 'in', self.language_id.ids)]
            domain['time_slot'] += [('interviewer_id.language_ids', 'in', self.language_id.ids)]
        if self.interviewer_id:
            domain['time_slot'] += [('interviewer_id', '=', self.interviewer_id.id)]
        if self.time_slot:
            domain['interviewer_id'] += [('id', '=', self.time_slot.interviewer_id.id)]
        if self.appoint_date:
            domain['time_slot'] += [('start', '>', datetime.datetime.combine(self.appoint_date, datetime.time(0, 0))),
                                    ('start', '<', datetime.datetime.combine(self.appoint_date, datetime.time(23, 59)))]
        return {'domain': domain}

    @api.onchange('interviewer_id')
    def onchange_interviewer_id(self):
        for record in self:
            if not record.interviewer_id:
                record.time_slot = None

    @api.onchange('language_id')
    def onchange_language_id(self):
        for record in self:
            if not record.language_id:
                record.interviewer_id = None
            if record.language_id and record.language_id.id not in record.interviewer_id.language_ids.ids:
                record.interviewer_id = None

    @api.onchange('type')
    def onchange_type(self):
        for record in self:
            if record.type == 'marathon':
                record.interviewer_id = None

    @api.onchange('time_slot')
    def onchange_time_slot(self):
        for record in self:
            if record.time_slot:
                record.appoint_date = record.time_slot.start
                record.interviewer_id = record.time_slot.interviewer_id
            else:
                record.appoint_date = None
                record.interviewer_id = None
            if record.time_slot and record.time_slot.start_datetime and not isinstance(record.time_slot.id, str):
                record.time_slot_day = calendar.day_name[record.time_slot.start_datetime.weekday()]
            else:
                record.time_slot_day = ''

    @api.constrains('time_slot')
    def constrains_time_slot(self):
        for record in self:
            if record.time_slot and record.time_slot.start_datetime and not isinstance(record.time_slot.id, str) and \
                not record.app_dt_start == record.time_slot.start_datetime:
                record.app_dt_start = record.time_slot.start_datetime
                record.app_dt_stop = record.time_slot.stop_datetime
                record.time_slot_day = calendar.day_name[record.time_slot.start_datetime.weekday()]
            elif not record.app_dt_stop:
                record.time_slot_day = None
            if record.time_slot and record.appoint_date != record.time_slot.start:
                record.appoint_date = record.time_slot.start
            if not record.time_slot and record.type != 'marathon':
                if record.app_dt_start:
                    record.app_dt_start = None
                if record.app_dt_stop:
                    record.app_dt_stop = None
                if record.time_slot_day:
                    record.time_slot_day = ''

    @api.constrains('app_dt_start')
    def constrains_app_dt_start(self):
        for record in self:
            if record.app_dt_start:
                record.time_slot_day = calendar.day_name[record.app_dt_start.weekday()]

    @api.model
    def create(self, values):
        values['name'] = self.env['ir.sequence'].next_by_code('sp.appointment') or '/'
        if values.get('time_slot') and isinstance(values.get('time_slot'), str):
            recurring_virtual_event = self.env['calendar.event'].browse(values.get('time_slot'))
            recurring_actual_event = recurring_virtual_event.detach_recurring_event().id
            values.update({'time_slot': recurring_actual_event})
        res = super(SpAppointment, self).create(values)
        res.time_slot.slot_state = 'booked'
        if self._context.get('call_queue', False):
            call_record = self.env['call_queue.user_input'].search([('registration_id', '=', res.registration_id.id)])
            if call_record:
                call_record.state = 'done'
        if self.env.user.has_group('isha_sp_registration.group_sp_appointment_interview_scheduler'):
            res.is_re_assigned_by_scheduler = True
        res.registration_id.appointment_state = 'draft'
        res.registration_id.sp_interview_status = "appointment_scheduled"
        res.registration_id.appointment_state = 'scheduled'
        return res

    def write(self, values):
        if values.get('time_slot') and isinstance(values.get('time_slot'), str):
            recurring_virtual_event = self.env['calendar.event'].browse(values.get('time_slot'))
            recurring_actual_event = recurring_virtual_event.detach_recurring_event().id
            values.update({'time_slot': recurring_actual_event})
        old_time_slot = self.time_slot.id
        new_time_slot = values.get('time_slot')
        if new_time_slot and not isinstance(new_time_slot, int):
            new_time_slot = new_time_slot.id
        if new_time_slot and old_time_slot != new_time_slot:
            # if self.env.user.has_group('isha_sp_registration.group_sp_appointment_interview_scheduler'):
            if old_time_slot:
                self.env['calendar.event'].browse(old_time_slot).slot_state = 'available'
            self.env['calendar.event'].browse(new_time_slot).slot_state = 'booked'
        res = super(SpAppointment, self).write(values)
        return res

    def compute_upcoming_time_slot(self):
        for record in self:
            record.upcoming_time_slot_ids = self.env['calendar.event'].search(
                [('start', '>=', record.create_date.strftime(DEFAULT_SERVER_DATE_FORMAT)),
                 ('slot_state', '!=', 'booked')], order='start asc').ids
            # [('start', '>=', record.appoint_date.strftime(DEFAULT_SERVER_DATE_FORMAT))]).ids
            # [('start', '>=', fields.Date.today().strftime(DEFAULT_SERVER_DATE_FORMAT))]).ids

    @api.model
    def list_appointment(self):
        context = {"appointment": True}
        domain = []
        if self.env.user.has_group('isha_sp_registration.group_sp_appointment_interviewer'):
            context.update({'search_default_my_appointments': 1})
            domain = [('appointment_state', 'not in', ['cancelled']), ('is_application_stage_cancelled', '=', False)]
        if self.env.user.has_group('isha_sp_registration.group_sp_appointment_interview_scheduler'):
            context.update({'search_default_regular_appointments': 1})
        views = [(self.env.ref('isha_sp_registration.sp_appointment_list').id, 'tree'),
                 (self.env.ref('isha_sp_registration.sp_appointment_form').id, 'form'),
                 (self.env.ref('isha_sp_registration.sp_appointment_calendar').id, 'calendar')]
        search_view_id = self.env.ref('isha_sp_registration.sp_appointment_search').id
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'sp.appointment',
            'name': "Appointment",
            'view_mode': 'tree,form,calendar',
            'views': views,
            'search_view_id': search_view_id,
            'target': 'current',
            'context': context,
            'domain': domain,
        }

    def button_open_interview_form(self):
        interview_form_id = self.env['sp.interview.form'].search([('sp_reg_id', '=', self.registration_id.id)],
                                                                 limit=1, order="id desc")
        if not interview_form_id:
            raise ValidationError("No Interview form related to this SP Applicant")
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'sp.interview.form',
            'name': "Interview Form",
            'view_mode': 'form,tree',
            'target': 'current',
            'flags': {
                # 'mode': 'readonly',
            },
            'res_id': interview_form_id.id,
            'context': {
                'create': False,
            },
        }

    def button_cancel_appointment(self):
        self.registration_id.sudo().write({
            'appointment_state': 'cancelled',
        })
        if self.time_slot:
            self.time_slot.slot_state = 'available'

    def button_assign_to_me(self):
        self.sudo().write({
            'interviewer_id': self.env['sp.interviewer'].search([('user_id', '=', self.env.uid)], limit=1),
        })
        self.registration_id.sudo().write({
            'appointment_state': 'scheduled',
        })
        self.sudo().select_or_create_slot()

    def button_reassign_scheduler(self):
        return {
            'name': 'Re Assign',
            'view_mode': 'form',
            'res_model': 'appointment.reassign',
            'view_id': self.env.ref('isha_sp_registration.appointment_reassign_form_scheduler').id,
            'type': 'ir.actions.act_window',
            'context': {
                'default_appointment_id': self.id,
                'time_slot_id': self.time_slot.id,
                'appointment': True,
            },
            'target': 'new',
        }

    def button_reassign_interviewer(self):
        return {
            'name': 'Re Assign',
            'view_mode': 'form',
            'res_model': 'appointment.reassign',
            'view_id': self.env.ref('isha_sp_registration.appointment_reassign_form_interviewer').id,
            'type': 'ir.actions.act_window',
            'context': {
                'default_appointment_id': self.id,
            },
            'target': 'new',
        }

    def button_accept_reassign(self):
        if self.sudo().appointment_state != 're_assign':
            raise ValidationError("This re-assigned appointment was 'Canceled' by another Interviewer. \n"
                                  "So, You don't have rights to access this document. \n"
                                  "Please navigate back!")
        if self.time_slot:
            self.time_slot.slot_state = 'unavailed'
        self.sudo().write({
            'interviewer_id': self.assign_interviewer_id,
            'assign_interviewer_id': None,
            'is_re_assigned_by_scheduler': False,
        })
        self.registration_id.sudo().write({
            'appointment_state': 're_assign_accepted',
        })
        self.sudo().select_or_create_slot()

    def button_reject_reassign(self):
        self.sudo().write({
            'assign_interviewer_id': None,
            'is_re_assigned_by_scheduler': False,
        })
        self.registration_id.sudo().write({
            'appointment_state': 'scheduled',
        })
        return {
            'name': 'Appointment',
            'view_mode': 'tree',
            'view_id': self.env.ref('isha_sp_registration.sp_appointment_list').id,
            'res_model': 'sp.appointment',
            'type': 'ir.actions.act_window',
        }

    def button_cancel_reassign(self):
        if self.sudo().appointment_state == 're_assign_accepted':
            raise ValidationError("This re-assigned appointment was 'Accepted' by another Interviewer. \n"
                                  "So, You don't have rights to access this document. \n"
                                  "Please navigate back!")
        if self.sudo().appointment_state != 're_assign':
            raise ValidationError("This re-assigned appointment was Modified by another Interviewer. \n"
                                  "So, You don't have rights to access this document. \n"
                                  "Please navigate back!")
        self.sudo().write({
            'assign_interviewer_id': None,
            'is_re_assigned_by_scheduler': False,
        })
        self.registration_id.sudo().write({
            'appointment_state': 'scheduled',
        })

    def button_approve_completed(self):
        self.registration_id.sudo().write({
            'appointment_state': 'completed',
        })
        self.time_slot.slot_state = 'availed'

    def button_no_response(self):
        self.registration_id.sudo().write({
            'appointment_state': 'no_response',
            'sp_interview_status': 'no_response_from_participants',
        })
        self.time_slot.slot_state = 'unavailed'
        self.registration_id.appointment_no_response_counter += 1
        return {
            'name': 'No Response Notification',
            'view_mode': 'form',
            'res_model': 'sp.notification',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': {
                'no_response': True,
            },
            'target': 'new',
        }

    def button_request_reschedule(self):
        self.registration_id.sudo().write({
            'appointment_state': 'request_reschedule',
        })
        self.registration_id.sp_interview_status = "requested_reschedule"

    def button_reschedule_interviewer(self):
        return {
            'name': 'Re Schedule',
            'view_mode': 'form',
            'res_model': 'appointment.reschedule',
            'view_id': self.env.ref('isha_sp_registration.appointment_reschedule_interviewer_form').id,
            'type': 'ir.actions.act_window',
            'context': {
                'default_appointment_id': self.id,
                'default_interviewer_id': self.interviewer_id.id,
                'default_current_time_slot': self.time_slot.id,
            },
            'target': 'new',
        }

    def button_reschedule_scheduler(self):
        return {
            'name': 'Re-Schedule',
            'view_mode': 'form',
            'res_model': 'appointment.reschedule',
            'view_id': self.env.ref('isha_sp_registration.appointment_reschedule_scheduler_form').id,
            'type': 'ir.actions.act_window',
            'context': {
                'default_appointment_id': self.id,
                'default_current_interviewer_id': self.interviewer_id.id,
                'default_current_time_slot': self.time_slot.id,
            },
            'target': 'new',
        }

    def button_add_comment(self):
        return {
            'name': 'Comment',
            'view_mode': 'form',
            'res_model': 'appointment.comment',
            'view_id': False,
            'type': 'ir.actions.act_window',
            'context': {
                'default_appointment_id': self.id,
            },
            'target': 'new',
        }

    def cancel_registration(self):
        return {
            'name': 'Cancel Application',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'cancellation.wizard',
            'view_id': False,
            'target': 'new',
            'context': {
                'appointment': True,
                'registration_id': self.registration_id.id,
            },
        }

    def select_or_create_slot(self):
        for record in self:
            if record.app_dt_start and record.app_dt_stop:
                if isinstance(record.app_dt_start, datetime.datetime):
                    start = record.app_dt_start.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                else:
                    start = record.app_dt_start
                if isinstance(record.app_dt_stop, datetime.datetime):
                    stop = record.app_dt_stop.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
                else:
                    stop = record.app_dt_stop
                domain = [
                    ('interviewer_id', '=', record.interviewer_id.id),
                    '|',
                    '&',
                    ('start', '>=', start),
                    ('start', '<=', stop),
                    '&',
                    ('stop', '>=', start),
                    ('stop', '<=', stop),
                ]
                calendar_id = self.env['calendar.event'].search(domain, order='start asc', limit=1)
                if not calendar_id:
                    calendar_id = self.env['calendar.event'].create({
                        'name': self.env.user.name,
                        'partner_ids': self.env.user.partner_id,
                        'interviewer_id': self.env['sp.interviewer'].search([('user_id', '=', self.env.user.id)]).id,
                        'start': start,
                        'stop': stop,
                    })
                calendar_id.slot_state = 'booked'
                record.time_slot = calendar_id
