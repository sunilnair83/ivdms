# -*- coding: utf-8 -*-
from odoo import models

from odoo.exceptions import AccessError
from odoo.tools import consteq


class Http(models.AbstractModel):
    _inherit = 'ir.http'

    def _get_record_and_check(self, xmlid=None, model=None, id=None, field='datas', access_token=None):
        # get object and content
        record = None
        if xmlid:
            record = self._xmlid_to_obj(self.env, xmlid)
        elif id and model in self.env:
            record = self.env[model].browse(int(id))

        # obj exists
        if not record or not record.exists() or field not in record:
            return None, 404

        if model == 'ir.attachment':
            record_sudo = record.sudo()
            if access_token and not consteq(record_sudo.access_token or '', access_token):
                return None, 403
            elif (access_token and consteq(record_sudo.access_token or '', access_token)):
                record = record_sudo
            elif record_sudo.public:
                record = record_sudo
            # Specific backend users will be able to access all files
            elif self.env.user.has_group('isha_sp_registration.group_sp_registration_admin') or \
                self.env.user.has_group('isha_sp_registration.group_sp_ha_screening_doctor') or \
                self.env.user.has_group('isha_sp_registration.group_sp_ha_doctor_approval'):
                record = record_sudo
            # Access for portal users with SP/HA groups will be limited to their own files
            elif self.env.user.has_group('isha_sp_registration.group_sp_portal_profile') or \
                self.env.user.has_group('isha_sp_registration.group_sp_portal_health_assessment'):
                if record_sudo.create_uid.id == self.env.user.id:
                    record = record_sudo
                else:
                    raise AccessError('Sorry, you are not allowed to view documents of other applicants')
            elif self.env.user.has_group('base.group_portal'):
                # Check the read access on the record linked to the attachment
                # eg: Allow to download an attachment on a task from /my/task/task_id
                record.check('read')
                record = record_sudo

        # check read access
        try:
            # We have prefetched some fields of record, among which the field
            # 'write_date' used by '__last_update' below. In order to check
            # access on record, we have to invalidate its cache first.
            record._cache.clear()
            record['__last_update']
        except AccessError:
            return None, 403

        return record, 200

    def binary_content(self, xmlid=None, model='ir.attachment', id=None, field='datas',
                       unique=False, filename=None, filename_field='name', download=False,
                       mimetype=None, default_mimetype='application/octet-stream',
                       access_token=None):
        if model == 'sp.interview.form':
            sp_interview_form_id = self.env['sp.interview.form'].sudo().search([('id', '=', id)])
            if sp_interview_form_id.registration_profile_id:
                model = 'sp.registration.profile'
                id = sp_interview_form_id.registration_profile_id.id
        # if model == 'sp.reg.health.assessment':
        #     sp_reg_health_assessment_id = self.env['sp.reg.health.assessment'].sudo().search([('id', '=', id)])
        #     if sp_reg_health_assessment_id.sp_reg_profile_id:
        #         model = 'sp.registration.profile'
        #         id = sp_reg_health_assessment_id.sp_reg_profile_id.id
        return super(Http, self).binary_content(xmlid=xmlid, model=model, id=id, field=field, unique=unique,
                                                filename=filename, filename_field=filename_field, download=download,
                                                mimetype=mimetype, default_mimetype=default_mimetype,
                                                access_token=access_token)
