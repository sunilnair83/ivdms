# -*- coding: utf-8 -*-

from odoo import models, fields, api


class SpInterviewer(models.Model):
    _name = 'sp.interviewer'
    _description = 'SP Interviewer'
    _rec_name = "user_id"

    _sql_constraints = [
        ('unique_user_id', 'UNIQUE (user_id)', 'Interviewer be unique!'),
    ]
    sp_interviewer_active = fields.Boolean('Active', default=True)
    user_id = fields.Many2one('res.users', delegate=True, required=True)
    language_ids = fields.Many2many('res.lang')
    appointment_id = fields.One2many('sp.appointment', 'interviewer_id', required=True)
    time_slot_id = fields.One2many('calendar.event', 'interviewer_id', readonly=True)

    @api.model
    def _name_search(self, name='', args=None, operator='ilike', limit=100, name_get_uid=None):
        if not self.env.context.get('bin_size', False):
            if args is None:
                args = []
            domain = args + ['|', ('language_ids', operator, name), ('name', operator, name)]
            model_ids = self._search(domain, limit=limit, access_rights_uid=name_get_uid)
            return models.lazy_name_get(self.browse(model_ids).with_user(name_get_uid))
        args = args + ['|', ('language_ids', operator, name), ('user_id', operator, name)]
        return super(SpInterviewer, self)._name_search(name, args=args, operator=operator, limit=limit,
                                                       name_get_uid=name_get_uid)

    @api.model
    def action_form_interviewer(self):
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'sp.interviewer',
            'name': "Profile",
            'view_mode': 'form',
            'res_id': self.env['sp.interviewer'].sudo().search([('user_id', '=', self.env.user.id)], limit=1).id,
            'target': 'current',
            'context': {
                'appointment': True,
            },
        }
