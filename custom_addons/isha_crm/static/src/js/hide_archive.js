odoo.define('isha_crm.HideArchiveAction', function (require) {
"use strict";

var session = require('web.session');
var BasicView = require('web.BasicView');
BasicView.include({
        init: function(viewInfo, params) {
            var self = this;
            this._super.apply(this, arguments);
            var model = self.controllerParams.modelName == "res.partner"
                           || self.controllerParams.modelName == "program.lead"
                           || self.controllerParams.modelName == "program.attendance"
                           || self.controllerParams.modelName == "ieo.record"
                           || self.controllerParams.modelName == "sadhguru.exclusive"
                           || self.controllerParams.modelName == "low.match.pairs"
                           || self.controllerParams.modelName == "donation.isha.education"
                           || self.controllerParams.modelName == 'isha.mailing.subscriptions'
                           || self.controllerParams.modelName == 'mailing.mailing'
            if(model) {
                session.user_has_group('base.group_system').then(function(has_group) {
                    if(!has_group) {
                        self.controllerParams.archiveEnabled = false in viewInfo.fields;
                    }
                });
            }
        },
});
});
