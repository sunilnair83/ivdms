odoo.define('isha_crm.datetime_search_filters', function (require) {
"use strict";
var datetime = require('web.search_filters');
var core = require('web.core');
var datepicker = require('web.datepicker');
var field_utils = require('web.field_utils');
var search_filters_registry = require('web.search_filters_registry');
var Widget = require('web.Widget');

var _t = core._t;
var _lt = core._lt;

datetime.DateTime.include({
        operators: [
        {value: "between", text: _lt("is between")},
        {value: "not between", text: _lt("is not between")},
        {value: "=", text: _lt("is equal to")},
        {value: "!=", text: _lt("is not equal to")},
        {value: ">", text: _lt("is after")},
        {value: "<", text: _lt("is before")},
        {value: ">=", text: _lt("is after or equal to")},
        {value: "<=", text: _lt("is before or equal to")},
        {value: "∃", text: _lt("is set")},
        {value: "∄", text: _lt("is not set")}
    ],
    /**
     * Gets the value of the datepicker
     *
     * @public
     * @param {Integer} [index] The datepicker's index.
     *  0 for the lower boundary (default)
     *  1 for the higher boundary
     *
     * @return {Moment} The value in UTC
     */
    get_value: function (index) {
        // retrieve the datepicker value
        var value = this["datewidget_" + (index || 0)].getValue().clone();
        // convert to utc
        return value.add(-this.getSession().getTZOffset(value), 'minutes');
    },
    get_domain: function (field, operator) {
        switch (operator.value) {
        case '∃':
            return [[field.name, '!=', false]];
        case '∄':
            return [[field.name, '=', false]];
        case 'between':
            return [
                [field.name, '>=', this._formatMomentToServer(this.get_value(0))],
                [field.name, '<=', this._formatMomentToServer(this.get_value(1))]
            ];
            case 'not between':
                console.log([
                [field.name, '<', this._formatMomentToServer(this.get_value(0))],
                [field.name, '>', this._formatMomentToServer(this.get_value(1))]
            ]);
                // console.log(this.get_value(1));
            return ['|',
                [field.name, '<', this._formatMomentToServer(this.get_value(0))],
                [field.name, '>', this._formatMomentToServer(this.get_value(1))]
            ];
        default:
            return [[field.name, operator.value, this._formatMomentToServer(this.get_value())]];
        }
    },
    show_inputs: function ($operator) {
        this._super.apply(this, arguments);

        if ($operator.val() === "between" || $operator.val() === "not between") {
            this.datewidget_1.do_show();
        } else {
            this.datewidget_1.do_hide();
        }
    },
});

datetime.Date.include({
    operators: [
        {value: "=", text: _lt("is equal to")},
        {value: "!=", text: _lt("is not equal to")},
        {value: ">", text: _lt("is after")},
        {value: "<", text: _lt("is before")},
        {value: ">=", text: _lt("is after or equal to")},
        {value: "<=", text: _lt("is before or equal to")},
        {value: "between", text: _lt("is between")},
        {value: "not between", text: _lt("is not between")},
        {value: "∃", text: _lt("is set")},
        {value: "∄", text: _lt("is not set")}
    ],
});
});