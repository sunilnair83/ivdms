odoo.define('isha_crm.custom_copy_values', function(require) {
'use strict';
	var FormRenderer = require('web.FormRenderer');
	var ListRenderer = require('web.ListRenderer');
	const core = require('web.core');
	const _t = core._t;
	FormRenderer.include({
		events: _.extend({}, FormRenderer.prototype.events, {
			'mouseover .o_field_widget':'mouseOverFields'
		}),
		mouseOverFields: async function (ev) {
        	if (ev.ctrlKey === true){
        		console.log('mouseover handler o_field_widget');
        		const value = ev.target.innerText;
				await navigator.clipboard.writeText(value);
				this.do_notify(_t("Copied to clipboard"));
        	}
    	},

	});
	ListRenderer.include({
		events: _.extend({}, ListRenderer.prototype.events, {
			'mouseover .o_field_cell':'mouseOverFields'
		}),
		mouseOverFields: async function (ev) {
        	if (ev.ctrlKey === true){
        		console.log('mouseover handler o_field_cell');
        		const value = ev.target.innerText;
				await navigator.clipboard.writeText(value);
				this.do_notify(_t("Copied to clipboard"));
				return true;
        	}
    	},

	});
});