odoo.define('isha_crm.hamburger_fix', function (require) {
    'use strict';
    /**
     * Need to revisit this logic as menu burger icon on mobile is affected.
     * Menuitem is not collapsed  after we click a sub menu instead it is made as display none in css
     *
     */
    const Menu = require("web.Menu");

    Menu.include({
        events: _.extend({
            // Clicking a hamburger menu item should close the hamburger
            "click .o_menu_sections [role=menuitem]": "_hideMobileSubmenus1"
        }, Menu.prototype.events),
        _hideMobileSubmenus1: function () {
            this.$section_placeholder.collapse("hide");
            if (
                this.$menu_toggle.is(":visible") &&
                this.$section_placeholder.is(":visible") &&
                !isWaiting()
            ) {
                this.$section_placeholder.collapse("hide");
            }
        },
    });
});
