odoo.define('isha_crm.custom_pager_validation', function(require) {
'use strict';
	var Pager = require('web.Pager');
	var utils = require('web.utils');
	var Widget = require('web.Widget');
	const core = require('web.core');
	const _t = core._t;
	var Dialog = require('web.Dialog');

	Pager.include({
		_save: function ($input) {
			var self = this;
			this.options.validate().then(function() {
				var value = $input.val().split("-");
				var min = utils.confine(parseInt(value[0], 10), 1, self.state.size);
				var max = utils.confine(parseInt(value[1], 10), 1, self.state.size);
				if (max-min > 200){
					Dialog.alert(this, _t("Maximum range in 200.\nPlease select a value less than 200 !!!."));
					return
				}else{
					if (!isNaN(min)) {
						self.state.current_min = min;
						if (!isNaN(max)) {
							self.state.limit = utils.confine(max-min+1, 1, self.state.size);
						} else {
							// The state has been given as a single value -> set the limit to 1
							self.state.limit = 1;
						}
						self.trigger('pager_changed', _.clone(self.state));
					}
					// Render the pager's new state (removes the input)
					self._render();
				}
			});
		}
	});
});