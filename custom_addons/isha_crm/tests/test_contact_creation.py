import json
import logging
from datetime import datetime

from odoo.tests.common import TransactionCase, tagged
from ..kafka_processors.EreceiptsEducationProcessor import EreceiptsEducationProcessor
from ...isha_crm.GoldenContactAdv import GoldenContactAdv

_logger = logging.getLogger('TestCases')

null = None

# To run the tests, append this to the odoo-bin command: --test-enable --test-tags isha
@tagged('-standard', 'isha')
class IshangamContact(TransactionCase):

    def test_bounced_flag_should_be_cleared_on_email_update(self):
        contact = self.env['res.partner'].create({
            'name': 'Test contact',
            'country': 'India',
            'nationality': 'Canada',
            'email': 'bounced@example.com',
            'contact_type': 'INDIVIDUAL',
            'email_validity': 'bounced',
        })

        contact = self.env['res.partner'].search([('id', '=', contact.id)])[0]
        self.assertEqual(contact.email_validity, 'bounced')
        contact.write({'email': 'updated@example.com'})

        contact = self.env['res.partner'].search([('id', '=', contact.id)])[0]
        self.assertEqual(contact.email_validity, 'valid')

    def test_emails_should_be_orderd_as_per_validity_after_merge(self):
        rec1 = {
            'name': 'Test contact',
            'country': 'India',
            'nationality': 'Canada',
            'email': 'bounced@example.com',
            'contact_type': 'INDIVIDUAL',
            'email_validity': 'bounced',
        }

        rec2 = {
            'name': 'Test contact',
            'country': 'India',
            'nationality': 'Canada',
            'email': 'bounced@example.com',
            'email_validity': None,
            'email2': 'dontknow@example.com',
            'email2_validity': None,
            'email3': 'valid@example.com',
            'contact_type': 'INDIVIDUAL',
            'email3_validity': 'valid',
        }
        merged_rec = GoldenContactAdv.getEmailsDict(rec1, rec2)

        self.assertEqual(merged_rec.get('email'), 'valid@example.com')
        self.assertEqual(merged_rec.get('email_validity'), 'valid')
        self.assertEqual(merged_rec.get('email2'), 'dontknow@example.com')
        self.assertIsNone(merged_rec.get('email2_validity'))
        self.assertEqual(merged_rec.get('email3'), 'bounced@example.com')
        self.assertEqual(merged_rec.get('email3_validity'), 'bounced')

    def test_contact_merge_with_high_match(self):
        contact1 = self.env['res.partner'].create({
            'name': 'Test contact',
            'country': 'India',
            'nationality': 'Canada',
            'email': 'bounced@example.com',
            'email_validity': 'bounced',
            'email2': 'dontknow@example.com',
            'contact_type': 'INDIVIDUAL',
        })

        contact2 = self.env['res.partner'].create({
            'name': 'Test contact',
            'country': 'India',
            'nationality': 'Canada',
            'email': 'bounced@example.com',
            'email_validity': None,
            'email2': 'valid@example.com',
            'email2_validity': 'valid',
            'contact_type': 'INDIVIDUAL',
        })

        self.assertEqual(contact1, contact2)
        self.assertEqual(contact2.email, 'valid@example.com')
        self.assertEqual(contact2.email_validity, 'valid')
        self.assertEqual(contact2.email2, 'dontknow@example.com')
        self.assertFalse(contact2.email2_validity)
        self.assertEqual(contact2.email3, 'bounced@example.com')
        self.assertEqual(contact2.email3_validity, 'bounced')

    def test_contact_merge_with_low_auth_merge(self):
        contact1 = self.env['res.partner'].create({
            'name': 'Test contact',
            'country': 'India',
            'nationality': 'Canada',
            'email': 'bounced@example.com',
            'email_validity': 'bounced',
            'contact_type': 'INDIVIDUAL',
        })
        _logger.info('contact1: ' + str(contact1.id))

        # create another contact with same name and email1 so that they will get merged.
        # set an old local date so that low auth merge will be triggered.
        # Becuase the second contact is outdated, it shouldn't get merged with contact1 though it's a high match.
        contact2 = self.env['res.partner'].create({
            'name': 'Test contact',
            'country': 'India',
            'nationality': 'Canada',
            'email': 'dontknow1@example.com',
            'contact_type': 'INDIVIDUAL',
            'local_modified_date': '2020-09-01 13:00:00'
        })
        _logger.info('contact2: ' + str(contact2.id))

        self.assertNotEqual(contact1, contact2)
        self.assertEqual(contact2.email, 'dontknow1@example.com')
        self.assertFalse(contact2.email_validity)
        self.assertFalse(contact2.email2)
        self.assertFalse(contact2.email2_validity)
        self.assertFalse(contact2.email3)
        self.assertFalse(contact2.email3_validity)

    def test_create_kafka_contact(self):

        # Reuse the same name,phone,email in all the kafka test cases
        contact = self.env['res.partner'].create({
            'name': 'Kafka User',
            'country': 'India',
            'nationality': 'India',
            'email': 'kafka@example.com',
            'phone_country_code': '91',
            'phone': '9999911111',
            'contact_type': 'INDIVIDUAL',
            'sso_id': 'kafka_user_sso_id'
        })
        self.assertIsNotNone(contact)


    def test_ereceipts_flows(self):
        contact = self.env['res.partner'].search([('phone', '=', '9999911111'),('email','=','kafka@example.com')])
        contact.last_txn_date = '1900-01-01'
        # self.env.cr.commit()
        cur_date = datetime.now()
        local_trans_id = "EDU-2021-5ed3a85d29c58d69148b4feh" + str(cur_date)
        erc_txn = {"payload":{
                    "record_modified_at":1591229402,
                    "row_id":97269,
                    "id":local_trans_id,
                    "contact_id":local_trans_id,
                    "modified_at":"2020-06-03T00:00:00Z",
                    "modified_by":"emedia@ishafoundation.org",
                    "project":"Ishanga",
                    "mode_of_payment":"Online",
                    "amount":2001.0,
                    "receipt_number":"if21-EAU-001559",
                    "receipt_date":str(cur_date.date())+"T18:30:00Z",
                    "entity":"IshaEducation",
                    "status":"SUCCESS",
                    "active":1,
                    "purpose_type": 'Isha Vidhay FRCA',
                    "contact_type":"INDIVIDUAL",
                    "first_name":"Kafka User",
                    "nationality":"India",
                    "phones":"9999911111",
                    "emails":"kafka@example.com",
                    "address1":"22A, Suryadev Nagar ",
                    "town_city1":"Indore",
                    "state_province_region1":"Madhya Pradesh",
                    "postal_code1":"452009",
                    "country1":"India",
                    "pan_number": None,
                    "passport_number": None,
                    "center":"Online"
                }
            }
        msg = json.dumps(erc_txn)
        rec = {'src_msg': msg,  'topic': 'erc_local', 'processor': 'ereceipts','process_msg':True}
        don_txn = self.env['kafka.errors'].create(rec)
        # Last Txn Date should be the cur_date
        self.assertEquals(str(contact.last_txn_date),str(cur_date.date()))

        erc_txn['receipt_date'] = '1900-01-01T18:30:00Z'
        msg = json.dumps(erc_txn)
        rec = {'src_msg': msg, 'topic': 'erc_local', 'processor': 'ereceipts', 'process_msg': True}
        don_txn = self.env['kafka.errors'].create(rec)

        # Last Txn Date should not be overriden with older date
        self.assertNotEquals(str(contact.last_txn_date), '1900-01-01')
        self.assertEquals(str(contact.last_txn_date), str(cur_date.date()))


    def test_prs_flows(self):
        contact = self.env['res.partner'].search([('sso_id','=','kafka_user_sso_id')])
        contact.last_txn_date = '1900-01-01'
        if len(self.env['program.type.master'].search([('local_program_type_id','=','Online Monthly Satsang Registration')]))==0:
            pgm_master = self.env['program.type.master'].create({'local_program_type_id':'Online Monthly Satsang Registration',
                                                                'program_type':'Online Monthly Satsang Registration',
                                                                 'is_lead':True,'system_id':13})
        # self.env.cr.commit()
        cur_date = datetime.now()
        prs_txn = {
            "_id": "prs"+str(cur_date),
            "address": "B-2002, Hillcrest CHS, Sariput Nagar Road\nOpp. Seepz gate no.3, Off JVLR",
            "adhar_no": None,
            "arrival_date": None,
            "bay_name": "Yamuna(Student)",
            "checkin_status_day_2": None,
            "city": "Mumbai",
            "country": "India",
            "pincode": "400093",
            "created_by": None,
            "created_date": str(cur_date.date())+" 01:01:01",
            "departure_date": None,
            "dob": "07-08-1999",
            "email": "kafka@example.com",
            "gender": "F",
            "idcard_no": "",
            "idcard_type": None,
            "ie_hear_about": "Volunteer",
            "ieo_class_status": "1",
            "is_ie_done": "No",
            "marital_status": None,
            "mobile": "9999911111",
            "modified_by": "anandstg@gmail.com",
            "modified_date": "2020-06-10 18:16:41",
            "name": "Kafka User",
            "nationality": "India",
            "occupation": "Student",
            "organization": None,
            "passport_number": None,
            "phone_country_code": None,
            "program_id": "049",
            "program_location": None,
            "program_name": "Online Monthly Satsang Registration",
            "register_mobile": None,
            "row_id": 138326,
            "state": "Maharashtra",
            "utm_campaign": None,
            "utm_medium": None,
            "whatsapp_country_code": None,
            "whatsapp_number": None,
            "end_date": "29-02-2020",
            "start_date": "28-02-2020",
            "status": "Confirmed",
            "ishangam_id": None,
            "alt_email": None,
            "alt_phone": None,
            "sso_id": "kafka_user_sso_id",
            "event_attendance_date": None
        }
        msg = json.dumps(prs_txn)
        rec = {'src_msg': msg,  'topic': 'prs_local', 'processor': 'prs','process_msg':True}
        prs_rec = self.env['kafka.errors'].create(rec)

        # Last Txn Date should be the reg_date_time
        self.assertEquals(str(contact.last_txn_date),str(cur_date.date()))

        contact.last_txn_date = '1900-01-01'
        # # self.env.cr.commit()
        cur_date = datetime.now()
        prs_txn['event_attendance_date'] = datetime.strftime(cur_date,'%d-%m-%Y %H:%M:%S')
        prs_txn['_id'] = "prs"+str(cur_date)
        msg = json.dumps(prs_txn)
        rec = {'src_msg': msg,  'topic': 'prs_local', 'processor': 'prs','process_msg':True}
        prs_rec = self.env['kafka.errors'].create(rec)

        # Last Txn Date should be the event_attendance_date
        self.assertEquals(str(contact.last_txn_date),str(cur_date.date()))


    def test_sadhguru_exclusive_flows(self):
        contact = self.env['res.partner'].search([('sso_id', '=', 'kafka_user_sso_id')])
        contact.last_txn_date = '1900-01-01'
        cur_date = int(datetime.now().timestamp()*1000)

        sgx_txn = {"sso_id":"kafka_user_sso_id",
                   "first_name":"Kafka","last_name":"User",
                   "email":"kafka@example.com",
                   "country":"IN",
                   "mobile_phone":"+919999911111",
                   "address_line_1":"House No 1109A, Sector 46-B",
                   "address_line_2":"","city":"Chandigarh",
                   "state":"Chandigarh",
                   "post_code":"160047",
                   "id":'sg_ex'+str(cur_date),
                   "status":"ACTIVE","created_ts":cur_date,
                   "modified_ts":cur_date,"type":"ANNUAL","amount":900.0,
                   "currency":"INR","cancel_ts":null,"cancel_reason":null}
        msg = json.dumps(sgx_txn)
        rec = {'src_msg': msg,  'topic': 'sgex_local', 'processor': 'sgex','process_msg':True}
        sgx_rec = self.env['kafka.errors'].create(rec)

        # Last Txn Date should be the subscription date
        self.assertEquals(str(contact.last_txn_date), str(datetime.fromtimestamp(cur_date/1000).date()))

    def test_phone_validations_should_be_skipped_in_kafka_flow(self):

        contact = self.env['res.partner'].create({
            'name': 'Kafka User',
            'country': 'India',
            'nationality': 'India',
            'email': 'kafka@example.com',
            'phone_country_code': '91',
            'phone': '9999999999',
            'contact_type': 'INDIVIDUAL',
            'sso_id': 'kafka_user_sso_id'
        })

        contact.write({'kafka_flow': True, 'phone_country_code': '55'})


    def test_high_match_contacts_with_different_sso_id_should_be_added_as_match_pairs(self):


        contact = self.env['res.partner'].create({
            'name': 'Kafka User',
            'country': 'India',
            'nationality': 'India',
            'email': 'kafka@example.com',
            'phone_country_code': '91',
            'phone': '9999999999',
            'contact_type': 'INDIVIDUAL',
            'sso_id': 'kafka_user_sso_id'
        })

        print("#### ID1: " + str(contact.id))
        match_pairs = self.env['low.match.pairs'].search(['|', ('id1', '=', contact.id), ('id2', '=', contact.id)])
        self.assertFalse(match_pairs)

        contact2 = self.env['res.partner'].create({
            'name': 'Kafka User',
            'country': 'India',
            'nationality': 'India',
            'email': 'kafka@example.com',
            'phone_country_code': '91',
            'phone': '9999999999',
            'contact_type': 'INDIVIDUAL',
            'sso_id': 'kafka_user_sso_id_2'
        })

        match_pairs = self.env['low.match.pairs'].search(['|', '&', ('id1', '=', contact.id), ('id2', '=', contact2.id),
                                                          '&', ('id1', '=', contact2.id), ('id2', '=', contact.id)])
        self.assertTrue(match_pairs)

    def test_erc_edu_purpose_breakup(self):
        EreceiptsEducationProcessor().get_purpose_breakup({'center': 'IVMS Admin Office - CBE'})