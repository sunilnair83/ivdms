import GoldenContactAdv
import ContactProcessor
import sys
from odoo import modules, api, SUPERUSER_ID
import unittest, csv


class kafkaTest(unittest.TestCase):

    null = None
    DB_NAME = 'odoo'
    UID = SUPERUSER_ID
    registry = modules.registry.Registry.new(DB_NAME)
    with open('/mnt/extra-addons/isha_crm/data/country-nationality-iso2-map.csv') as f:
        iso2 = dict(filter(None, csv.reader(f)))
    with open('/mnt/extra-addons/isha_crm/data/eReceipts-country-nationality-corrections.csv') as f:
        csvReader = []
        for x in csv.reader(f):
            csvReader.append([x[0].upper(), x[1].upper()])
        correction = dict(filter(None, csvReader))

    contact_processors = {'svro': ContactProcessor.SVROContacts(), 'frcoms': ContactProcessor.FRCommsContact(),
                          'dms': ContactProcessor.DMSContacts(), 'hys':ContactProcessor.HYSContacts(),
                          'prs':ContactProcessor.PRSContacts(), 'al':ContactProcessor.ALContact(),
                          'erecipts':ContactProcessor.EReceipts()}
    match_engine = GoldenContactAdv.GoldenContactAdv()

    # Phase 4 new records from source systems
    src_message = { 'svro': {"id":32694,"first_name":"ADITI MUKHERJEE","last_name":null,"personal_email":"ditipiu@gmail.com","date_of_birth":"1957-10-28","gender":"Female","marital_status":null,"address":"10/71 BEJOYGARH,P.S. JADAVPORE,","city":"","state":null,"country_of_residence":"India","postal_code":"700032","phone":"a:1:{i:0;a:2:{s:4:\"type\";s:8:\"Personal\";s:6:\"number\";s:10:\"9830634606\";}}","cdiid":null,"sys_modified_at":0},
                    'frcoms': {"row_id":2229,"local_id":"file1-2229","title":null,"name":"Manju Kar","contact_no":"9821542116","email":"manjukar21@gmail.com","second_email":null,"gender":"Female","date_of_birth":"01-Jan-51", "nature_of_business":"exporter","designation":"director","company_name":"LONSIA SHOES","nationality":null,"address":"Swastik Chhadva, Solitair, 503, CTS, 1736/1, charges premises, CHS, VN purav marg, chembur, Mumbai 400071","city":"MUMBAI","state":"Maharashtra","country":"India","pincode":"400071","in_file_date":"1 Dec 16","record_modified_at":1531647894096},
                    'dms': {"row_id":15,"local_contact_id":"103de873-28cf-0e5d-ec6a-575aa8da3a34","modified_at":"2016-06-21T09:23:21Z","modified_by":"PremalathaJ","contact_type":"ORGANIZATION","title":"","first_name":"Shimpo Wood Sdn Bhd","dob":"","gender":"","occupation":"","company_ids":"","nationality":"malaysia","phones":"9999999999;8888888888","emails":"test@gmail.com","address1":"Lot 1680 Lorong Haji Musran Rantau Panjang","town_city1":"","state_province_region1":"","postal_code1":"421000","country1":"malaysia","pan_number":"","deceased":0,"do_not_call":0,"do_not_email":0,"do_not_postmail":0,"active":1},
                    'hys': {"row_id":10,"local_contact_id":"24ed004d-3803-901f-e36b-5bd2a678803a","date_modified":"2018-10-29T12:27:20Z","first_name":"SivaVivekanantha","last_name":"Vivekanantha","marital_status":"Married","nationality":"INDIA","mobile_phone":"91-9443141002","home_phone":"91-6383653291","primary_email":"sivavivekanantha@gmail.com","email":"","address_line_1":"27/28,Palanisamy Gounder Street, Sanganoorvillage","address_line_2":"Rathinapuri","town_city":"COIMBATORE","state_province_region":"Near Gandhipuram 7th street Railway Bridge","postal_code":"641027","country":"INDIA","date_of_birth":"1984-05-25","gender":"Male","modifed_by":"ITAdministrator","date_created":"2018-10-26T05:31:31Z","created_by":"ITAdministrator","status":"New","batch_applied":"HYTT2018_BATCH2","start_date":"2018-07-27","record_modified_at":"2019-02-19 12:41:00","active":1},
                    'prs': {"row_id":11,"record_id":"5cb5c0477a9e88bb1c8b4ed0","title":null,"name":"Nikhil Mahajan","firstname":"Nikhil","lastname":"Mahajan","mobile":"9881259547","whatsapp_number":null,"register_mobile":null,"email":"nikhil_mahajan1982@yahoo.co.in","gender":"M","nationality":"India","dob":"1982-04-21T18:30:00.000Z","age":null,"address":"Kalpataru Harmony F 607, Mankar Chowk, Wakad","city":"Wakad, Pune","district":null,"state":"Maharashtra","country":"India","pincode":"411057","occupation":"Business","organization":null,"idcard_type":"Aadhaar","idcard_no":null,"adhar_no":null,"passport_number":null,"passport_expirydate":null,"created_by":null,"created_date":"2019-04-16T11:45:11.196Z","modified_by":null,"modified_date":"2019-05-29T05:08:52.707Z","category":null,"program_id":"024","program_type":null,"program_name":"21 days Hatha Yoga Program 2019","program_location":null,"marital_status":null,"initiate_shambhavi":null,"program_attending":null,"prc_status":null,"isha_programs":"[\"Shambhavi\",\"Hatha Yoga\"]","status":"Confirmed"},
                    'al': {"Pam_Serial_Number":2,"Pam_Initial":" ","Pam_Participant_Name":"Sivakumar A","Pam_Address_Line1":"1","Pam_Address_Line2":"Sriram Nager","Pam_Address_Line3":"Hudco Coloney","Pam_City":"Coimbatore","Pam_District_County":"","Pam_State":"Tamil Nadu","Pam_Country":"India","Pam_Postal_Code":"641004","Pam_Home_Phone":"","Pam_Mobile_Phone":"9500572425","Pam_Mobile_Phone1":"","Pam_Email_ID":"siva_core@yahoo.co.in","Pam_Oemail_Id":"","Pam_Occupation":" ","Pam_Work_Address_Line1":"workaddtest","Pam_Work_Address_Line2":"workaddtest","Pam_Work_Address_Line3":"workaddtest","Pam_Work_City":"workaddtest","Pam_Work_District_County":"","Pam_Work_State":"workaddtest","Pam_Work_Country":"workaddtest","Pam_Work_PostalCode":"workaddtest","Pam_Work_Phone":" ","Pam_Gender":"M","Pam_Age":"39","Pam_Date_Of_Birth":"03-11-1970","Pam_Record_Created_By":"0","Pam_Record_Modified_By":"0","Pam_Record_Created_Date":1268839511000,"Pam_Record_Modified_Date":19801000},
                    'erecipts': {"row_id":848224,"record_modified_at":1566797219,"id":"FND-1920-5ca1a70129c58d0b1c68aab3","modified_at":"2019-04-04T00:00:00Z","modified_by":"emedia@ishafoundation.org","contact_type":"INDIVIDUAL","first_name":"Vanaja Penumatsa","nationality":"India","phones":"7989067864","emails":"vanajapenumatsa@gmail.com","address1":"Flat 203, Arca Sarovar,KFC StreetIndra Nagar, Gachibowli ","town_city1":"Hyderabad","state_province_region1":"Telangana","postal_code1":"500032","country1":"India","pan_number":"","passport_number":null}
                   }

    # Phase 1 phone exact and name match > 80%

    phase1_message = { 'svro': {"id":32694,"first_name":"ADITI MUKHERJEE","last_name":null,"personal_email":"ditipiu@yahoo.com","date_of_birth":"1957-10-28","gender":"Female","marital_status":null,"address":"10/71 BEJOYGARH,P.S. JADAVPORE, edit","city":"","state":null,"country_of_residence":"India","postal_code":"700032","phone":"a:1:{i:0;a:2:{s:4:\"type\";s:8:\"Personal\";s:6:\"number\";s:10:\"9830634606\";}}","cdiid":null,"sys_modified_at":0},
                    'frcoms': {"row_id":2229,"local_id":"file1-2229","title":null,"name":"Manju Kar","contact_no":"9821542116","email":"manjukar21@yahoo.com","second_email":null,"gender":"Female","date_of_birth":"01-Jan-51", "nature_of_business":"exporter","designation":"director","company_name":"LONSIA SHOES","nationality":null,"address":"Swastik Chhadva, Solitair, 503, CTS, 1736/1, charges premises, CHS, VN purav marg, chembur, Mumbai 400071 edit","city":"MUMBAI","state":"Maharashtra","country":"India","pincode":"400071","in_file_date":"1 Dec 16","record_modified_at":1531647894096},
                    'dms': {"row_id":15,"local_contact_id":"103de873-28cf-0e5d-ec6a-575aa8da3a34","modified_at":"2016-06-21T09:23:21Z","modified_by":"PremalathaJ","contact_type":"ORGANIZATION","title":"","first_name":"Shimpo Wood Sdn Bhd","dob":"","gender":"","occupation":"","company_ids":"","nationality":"malaysia","phones":"9999999999;8888888888","emails":"test@yahoo.com","address1":"Lot 1680 Lorong Haji Musran Rantau Panjang","town_city1":"","state_province_region1":"","postal_code1":"421000","country1":"malaysia","pan_number":"","deceased":0,"do_not_call":0,"do_not_email":0,"do_not_postmail":0,"active":1},
                    'hys': {"row_id":10,"local_contact_id":"24ed004d-3803-901f-e36b-5bd2a678803a","date_modified":"2018-10-29T12:27:20Z","first_name":"SivaVivekanantha","last_name":"Vivekanantha","marital_status":"Married","nationality":"INDIA","mobile_phone":"91-9443141002","home_phone":"91-6383653291","primary_email":"sivavivekanantha@yahoo.com","email":"","address_line_1":"27/28,Palanisamy Gounder Street, Sanganoorvillage edit","address_line_2":"Rathinapuri","town_city":"COIMBATORE","state_province_region":"Near Gandhipuram 7th street Railway Bridge","postal_code":"641027","country":"INDIA","date_of_birth":"1984-05-25","gender":"Male","modifed_by":"ITAdministrator","date_created":"2018-10-26T05:31:31Z","created_by":"ITAdministrator","status":"New","batch_applied":"HYTT2018_BATCH2","start_date":"2018-07-27","record_modified_at":"2019-02-19 12:41:00","active":1},
                    'prs': {"row_id":11,"record_id":"5cb5c0477a9e88bb1c8b4ed0","title":null,"name":"Nikhil Mahajan","firstname":"Nikhil","lastname":"Mahajan","mobile":"9881259547","whatsapp_number":null,"register_mobile":null,"email":"nikhil_mahajan@yahoo.com","gender":"M","nationality":"India","dob":"1982-04-21T18:30:00.000Z","age":null,"address":"Kalpataru Harmony F 607, Mankar Chowk, Wakad edit","city":"Wakad, Pune","district":null,"state":"Maharashtra","country":"India","pincode":"411057","occupation":"Business","organization":null,"idcard_type":"Aadhaar","idcard_no":null,"adhar_no":null,"passport_number":null,"passport_expirydate":null,"created_by":null,"created_date":"2019-04-16T11:45:11.196Z","modified_by":null,"modified_date":"2019-05-29T05:08:52.707Z","category":null,"program_id":"024","program_type":null,"program_name":"21 days Hatha Yoga Program 2019","program_location":null,"marital_status":null,"initiate_shambhavi":null,"program_attending":null,"prc_status":null,"isha_programs":"[\"Shambhavi\",\"Hatha Yoga\"]","status":"Confirmed"},
                    'al': {"Pam_Serial_Number":2,"Pam_Initial":" ","Pam_Participant_Name":"Sivakumar A","Pam_Address_Line1":"1","Pam_Address_Line2":"Sriram Nager","Pam_Address_Line3":"Hudco Coloney","Pam_City":"Coimbatore","Pam_District_County":"","Pam_State":"Tamil Nadu","Pam_Country":"India","Pam_Postal_Code":"641004","Pam_Home_Phone":"","Pam_Mobile_Phone":"9500572425","Pam_Mobile_Phone1":"","Pam_Email_ID":"siva_core@yahoo.com","Pam_Oemail_Id":"","Pam_Occupation":" ","Pam_Work_Address_Line1":" ","Pam_Work_Address_Line2":" ","Pam_Work_Address_Line3":" ","Pam_Work_City":" ","Pam_Work_District_County":"","Pam_Work_State":" ","Pam_Work_Country":" ","Pam_Work_PostalCode":" ","Pam_Work_Phone":" ","Pam_Gender":"M","Pam_Age":"39","Pam_Date_Of_Birth":"03-11-1970","Pam_Record_Created_By":"0","Pam_Record_Modified_By":"0","Pam_Record_Created_Date":1268839511000,"Pam_Record_Modified_Date":19801000},
                    'erecipts': {"row_id":848224,"record_modified_at":1566797219,"id":"FND-1920-5ca1a70129c58d0b1c68aab3","modified_at":"2019-04-04T00:00:00Z","modified_by":"emedia@ishafoundation.org","contact_type":"INDIVIDUAL","first_name":"Vanaja Penumatsa","nationality":"India","phones":"7989067864","emails":"vanajapenumatsa@yahoo.com","address1":"Flat 203, Arca Sarovar,KFC StreetIndra Nagar, Gachibowli edit","town_city1":"Hyderabad","state_province_region1":"Telangana","postal_code1":"500032","country1":"India","pan_number":"","passport_number":null}
                   }

    # Phase 2 email exact and name match > 90%
    phase2_messages = { 'svro': {"id":32694,"first_name":"ADITI MUKHERJEE","last_name":null,"personal_email":"ditipiu@yahoo.com","date_of_birth":"1957-10-28","gender":"Female","marital_status":null,"address":"10/71 BEJOYGARH,P.S. JADAVPORE, edit","city":"","state":null,"country_of_residence":"India","postal_code":"700032","phone":"a:1:{i:0;a:2:{s:4:\"type\";s:8:\"Personal\";s:6:\"number\";s:10:\"9830634607\";}}","cdiid":null,"sys_modified_at":0},
                    'frcoms': {"row_id":2229,"local_id":"file1-2229","title":null,"name":"Manju Kar","contact_no":"9821542110","email":"manjukar21@yahoo.com","second_email":null,"gender":"Female","date_of_birth":"01-Jan-51", "nature_of_business":"exporter","designation":"director","company_name":"LONSIA SHOES","nationality":null,"address":"Swastik Chhadva, Solitair, 503, CTS, 1736/1, charges premises, CHS, VN purav marg, chembur, Mumbai 400071 edit","city":"MUMBAI","state":"Maharashtra","country":"India","pincode":"400071","in_file_date":"1 Dec 16","record_modified_at":1531647894096},
                    'dms': {"row_id":15,"local_contact_id":"103de873-28cf-0e5d-ec6a-575aa8da3a34","modified_at":"2016-06-21T09:23:21Z","modified_by":"PremalathaJ","contact_type":"ORGANIZATION","title":"","first_name":"Shimpo Wood Sdn Bhd","dob":"","gender":"","occupation":"","company_ids":"","nationality":"malaysia","phones":"9999909099;8888888880","emails":"test@yahoo.com","address1":"Lot 1680 Lorong Haji Musran Rantau Panjang","town_city1":"","state_province_region1":"","postal_code1":"421000","country1":"malaysia","pan_number":"","deceased":0,"do_not_call":0,"do_not_email":0,"do_not_postmail":0,"active":1},
                    'hys': {"row_id":10,"local_contact_id":"24ed004d-3803-901f-e36b-5bd2a678803a","date_modified":"2018-10-29T12:27:20Z","first_name":"SivaVivekanantha","last_name":"Vivekanantha","marital_status":"Married","nationality":"INDIA","mobile_phone":"91-9443741002","home_phone":"91-6383653290","primary_email":"sivavivekanantha@yahoo.com","email":"","address_line_1":"27/28,Palanisamy Gounder Street, Sanganoorvillage edit","address_line_2":"Rathinapuri","town_city":"COIMBATORE","state_province_region":"Near Gandhipuram 7th street Railway Bridge","postal_code":"641027","country":"INDIA","date_of_birth":"1984-05-25","gender":"Male","modifed_by":"ITAdministrator","date_created":"2018-10-26T05:31:31Z","created_by":"ITAdministrator","status":"New","batch_applied":"HYTT2018_BATCH2","start_date":"2018-07-27","record_modified_at":"2019-02-19 12:41:00","active":1},
                    'prs': {"row_id":11,"record_id":"5cb5c0477a9e88bb1c8b4ed0","title":null,"name":"Nikhil Mahajan","firstname":"Nikhil","lastname":"Mahajan","mobile":"9881259647","whatsapp_number":null,"register_mobile":null,"email":"nikhil_mahajan@yahoo.com","gender":"M","nationality":"India","dob":"1982-04-21T18:30:00.000Z","age":null,"address":"Kalpataru Harmony F 607, Mankar Chowk, Wakad edit","city":"Wakad, Pune","district":null,"state":"Maharashtra","country":"India","pincode":"411057","occupation":"Business","organization":null,"idcard_type":"Aadhaar","idcard_no":null,"adhar_no":null,"passport_number":null,"passport_expirydate":null,"created_by":null,"created_date":"2019-04-16T11:45:11.196Z","modified_by":null,"modified_date":"2019-05-29T05:08:52.707Z","category":null,"program_id":"024","program_type":null,"program_name":"21 days Hatha Yoga Program 2019","program_location":null,"marital_status":null,"initiate_shambhavi":null,"program_attending":null,"prc_status":null,"isha_programs":"[\"Shambhavi\",\"Hatha Yoga\"]","status":"Confirmed"},
                    'al': {"Pam_Serial_Number":2,"Pam_Initial":" ","Pam_Participant_Name":"Sivakumar A","Pam_Address_Line1":"1","Pam_Address_Line2":"Sriram Nager","Pam_Address_Line3":"Hudco Coloney","Pam_City":"Coimbatore","Pam_District_County":"","Pam_State":"Tamil Nadu","Pam_Country":"India","Pam_Postal_Code":"641004","Pam_Home_Phone":"","Pam_Mobile_Phone":"9500572426","Pam_Mobile_Phone1":"","Pam_Email_ID":"siva_core@yahoo.com","Pam_Oemail_Id":"","Pam_Occupation":" ","Pam_Work_Address_Line1":" ","Pam_Work_Address_Line2":" ","Pam_Work_Address_Line3":" ","Pam_Work_City":" ","Pam_Work_District_County":"","Pam_Work_State":" ","Pam_Work_Country":" ","Pam_Work_PostalCode":" ","Pam_Work_Phone":" ","Pam_Gender":"M","Pam_Age":"39","Pam_Date_Of_Birth":"03-11-1970","Pam_Record_Created_By":"0","Pam_Record_Modified_By":"0","Pam_Record_Created_Date":1268839511000,"Pam_Record_Modified_Date":19801000},
                    'erecipts': {"row_id":848224,"record_modified_at":1566797219,"id":"FND-1920-5ca1a70129c58d0b1c68aab3","modified_at":"2019-04-04T00:00:00Z","modified_by":"emedia@ishafoundation.org","contact_type":"INDIVIDUAL","first_name":"Vanaja Penumatsa","nationality":"India","phones":"7989067868","emails":"vanajapenumatsa@yahoo.com","address1":"Flat 203, Arca Sarovar,KFC StreetIndra Nagar, Gachibowli edit","town_city1":"Hyderabad","state_province_region1":"Telangana","postal_code1":"500032","country1":"India","pan_number":"","passport_number":null}
                   }

    # Phase 3 name exact and phone edit dist 1
    phase3_messages = { 'svro': {"id":32694,"first_name":"ADITI MUKHERJEE","last_name":null,"personal_email":"1ditipiu@yahoo.com","date_of_birth":"1957-10-28","gender":"Female","marital_status":null,"address":"10/71 BEJOYGARH,P.S. JADAVPORE, edit","city":"","state":null,"country_of_residence":"India","postal_code":"700032","phone":"a:1:{i:0;a:2:{s:4:\"type\";s:8:\"Personal\";s:6:\"number\";s:10:\"9830634601\";}}","cdiid":null,"sys_modified_at":0},
                    'frcoms': {"row_id":2229,"local_id":"file1-2229","title":null,"name":"Manju Kar","contact_no":"9821542170","email":"manjukar121@yahoo.com","second_email":null,"gender":"Female","date_of_birth":"01-Jan-51", "nature_of_business":"exporter","designation":"director","company_name":"LONSIA SHOES","nationality":null,"address":"Swastik Chhadva, Solitair, 503, CTS, 1736/1, charges premises, CHS, VN purav marg, chembur, Mumbai 400071 edit","city":"MUMBAI","state":"Maharashtra","country":"India","pincode":"400071","in_file_date":"1 Dec 16","record_modified_at":1531647894096},
                    'dms': {"row_id":15,"local_contact_id":"103de873-28cf-0e5d-ec6a-575aa8da3a34","modified_at":"2016-06-21T09:23:21Z","modified_by":"PremalathaJ","contact_type":"ORGANIZATION","title":"","first_name":"Shimpo Wood Sdn Bhd","dob":"","gender":"","occupation":"","company_ids":"","nationality":"malaysia","phones":"9999909999;8888888980","emails":"test1@yahoo.com","address1":"Lot 1680 Lorong Haji Musran Rantau Panjang","town_city1":"","state_province_region1":"","postal_code1":"421000","country1":"malaysia","pan_number":"","deceased":0,"do_not_call":0,"do_not_email":0,"do_not_postmail":0,"active":1},
                    'hys': {"row_id":10,"local_contact_id":"24ed004d-3803-901f-e36b-5bd2a678803a","date_modified":"2018-10-29T12:27:20Z","first_name":"SivaVivekanantha","last_name":"Vivekanantha","marital_status":"Married","nationality":"INDIA","mobile_phone":"91-9443741012","home_phone":"91-6383653280","primary_email":"sivavivekananthh1a@yahoo.com","email":"","address_line_1":"27/28,Palanisamy Gounder Street, Sanganoorvillage edit","address_line_2":"Rathinapuri","town_city":"COIMBATORE","state_province_region":"Near Gandhipuram 7th street Railway Bridge","postal_code":"641027","country":"INDIA","date_of_birth":"1984-05-25","gender":"Male","modifed_by":"ITAdministrator","date_created":"2018-10-26T05:31:31Z","created_by":"ITAdministrator","status":"New","batch_applied":"HYTT2018_BATCH2","start_date":"2018-07-27","record_modified_at":"2019-02-19 12:41:00","active":1},
                    'prs': {"row_id":11,"record_id":"5cb5c0477a9e88bb1c8b4ed0","title":null,"name":"Nikhil Mahajan","firstname":"Nikhil","lastname":"Mahajan","mobile":"9081259647","whatsapp_number":null,"register_mobile":null,"email":"nikhil_mahaja1n@yahoo.com","gender":"M","nationality":"India","dob":"1982-04-21T18:30:00.000Z","age":null,"address":"Kalpataru Harmony F 607, Mankar Chowk, Wakad edit","city":"Wakad, Pune","district":null,"state":"Maharashtra","country":"India","pincode":"411057","occupation":"Business","organization":null,"idcard_type":"Aadhaar","idcard_no":null,"adhar_no":null,"passport_number":null,"passport_expirydate":null,"created_by":null,"created_date":"2019-04-16T11:45:11.196Z","modified_by":null,"modified_date":"2019-05-29T05:08:52.707Z","category":null,"program_id":"024","program_type":null,"program_name":"21 days Hatha Yoga Program 2019","program_location":null,"marital_status":null,"initiate_shambhavi":null,"program_attending":null,"prc_status":null,"isha_programs":"[\"Shambhavi\",\"Hatha Yoga\"]","status":"Confirmed"},
                    'al': {"Pam_Serial_Number":2,"Pam_Initial":" ","Pam_Participant_Name":"Sivakumar A","Pam_Address_Line1":"1","Pam_Address_Line2":"Sriram Nager","Pam_Address_Line3":"Hudco Coloney","Pam_City":"Coimbatore","Pam_District_County":"","Pam_State":"Tamil Nadu","Pam_Country":"India","Pam_Postal_Code":"641004","Pam_Home_Phone":"","Pam_Mobile_Phone":"9510572426","Pam_Mobile_Phone1":"","Pam_Email_ID":"siva1_212core@yahoo.com","Pam_Oemail_Id":"","Pam_Occupation":" ","Pam_Work_Address_Line1":" ","Pam_Work_Address_Line2":" ","Pam_Work_Address_Line3":" ","Pam_Work_City":" ","Pam_Work_District_County":"","Pam_Work_State":" ","Pam_Work_Country":" ","Pam_Work_PostalCode":" ","Pam_Work_Phone":" ","Pam_Gender":"M","Pam_Age":"39","Pam_Date_Of_Birth":"03-11-1970","Pam_Record_Created_By":"0","Pam_Record_Modified_By":"0","Pam_Record_Created_Date":1268839511000,"Pam_Record_Modified_Date":19801000},
                    'erecipts': {"row_id":848224,"record_modified_at":1566797219,"id":"FND-1920-5ca1a70129c58d0b1c68aab3","modified_at":"2019-04-04T00:00:00Z","modified_by":"emedia@ishafoundation.org","contact_type":"INDIVIDUAL","first_name":"Vanaja Penumatsa","nationality":"India","phones":"7980067868","emails":"van1ajapenumatsa@yahoo.com","address1":"Flat 203, Arca Sarovar,KFC StreetIndra Nagar, Gachibowli edit","town_city1":"Hyderabad","state_province_region1":"Telangana","postal_code1":"500032","country1":"India","pan_number":"","passport_number":null}
                   }



    def test_1_phase4(self):
        UID = SUPERUSER_ID
        cr = self.registry.cursor()
        with api.Environment.manage():
            env = api.Environment(cr, UID, {})

            for key in self.contact_processors:
                msg = self.src_message.get(key)
                processor = self.contact_processors.get(key)
                env['res.partner']
                odoo_format = processor.getOdooFormat(msg,self.iso2,self.correction)
                flag_dict = self.match_engine.createOrUpdate(env, odoo_format)
                self.assertTrue(flag_dict['flag'])
                self.assertEqual(flag_dict['phase'], 'phase4')
        cr.commit()
        cr.close()

    def test_2_phase1(self):
        UID = SUPERUSER_ID
        cr = self.registry.cursor()
        with api.Environment.manage():
            env = api.Environment(cr, UID, {})

            for key in self.contact_processors:
                msg = self.phase1_message.get(key)
                processor = self.contact_processors.get(key)
                flag_dict = self.match_engine.findMatch(env, processor.getOdooFormat(msg,self.iso2,self.correction))

                self.assertTrue(flag_dict['flag'])
                self.assertEqual(flag_dict['phase'], 'phase1')

    def test_3_pahse2(self):
        UID = SUPERUSER_ID
        cr = self.registry.cursor()
        with api.Environment.manage():
            env = api.Environment(cr, UID, {})

            for key in self.contact_processors:
                msg = self.phase2_messages.get(key)
                processor = self.contact_processors.get(key)
                flag_dict = self.match_engine.findMatch(env, processor.getOdooFormat(msg,self.iso2,self.correction))

                self.assertTrue(flag_dict['flag'])
                self.assertEqual(flag_dict['phase'], 'phase2')

    def test_4_phase3(self):
        UID = SUPERUSER_ID
        cr = self.registry.cursor()
        with api.Environment.manage():
            env = api.Environment(cr, UID, {})

            for key in self.contact_processors:
                msg = self.phase3_messages.get(key)
                processor = self.contact_processors.get(key)
                flag_dict = self.match_engine.findMatch(env, processor.getOdooFormat(msg,self.iso2,self.correction))

                self.assertTrue(flag_dict['flag'])
                self.assertEqual(flag_dict['phase'], 'phase3')


    def test_5_phase0(self):
        UID = SUPERUSER_ID
        cr = self.registry.cursor()
        with api.Environment.manage():
            env = api.Environment(cr, UID, {})

            msg = {"id":32694,"first_name":"ADITI MUKHERJEE","last_name":None,"personal_email":"ditipiu@yahoo.com","date_of_birth":"1957-10-28","gender":"Female","marital_status":None,"address":"10/71 BEJOYGARH,P.S. JADAVPORE, edit","city":"","state":None,"country_of_residence":"India","postal_code":"700032","phone":"a:1:{i:0;a:2:{s:4:\"type\";s:8:\"Personal\";s:6:\"number\";s:10:\"9830634607\";}}","cdiid":None,"sys_modified_at":0}
            flag_dict = self.match_engine.findMatch(env,self.contact_processors.get('svro').getOdooFormat(msg,self.iso2,self.correction))
            msg['cdiid'] = flag_dict['rec']['guid']
            flag_dict = self.match_engine.findMatch(env,self.contact_processors.get('svro').getOdooFormat(msg,self.iso2,self.correction))
            self.assertTrue(flag_dict['flag'])
            self.assertEqual(flag_dict['phase'], 'phase0')

    def test_6_lmp(self):
        UID = SUPERUSER_ID
        cr = self.registry.cursor()
        with api.Environment.manage():
            env = api.Environment(cr, UID, {})

            msg = {"id":32695,"first_name":"Siva Anandan","last_name":None,"personal_email":"siva@yahoo.com","date_of_birth":"1957-10-28","gender":"Male","marital_status":None,"address":"221B baker street,","city":"","state":None,"country_of_residence":"India","postal_code":"625312","phone":"a:1:{i:0;a:2:{s:4:\"type\";s:8:\"Personal\";s:6:\"number\";s:10:\"9125454321\";}}","cdiid":None,"sys_modified_at":0}
            flag_dict = self.match_engine.findMatch(env,self.contact_processors.get('svro').getOdooFormat(msg,self.iso2,self.correction))
            msg = {"id":32696,"first_name":"Siva Anandan","last_name":None,"personal_email":"siva112@yahoo.com","date_of_birth":"1957-10-28","gender":"Male","marital_status":None,"address":"221B baker street,","city":"","state":None,"country_of_residence":"India","postal_code":"625302","phone":"a:1:{i:0;a:2:{s:4:\"type\";s:8:\"Personal\";s:6:\"number\";s:10:\"9126454327\";}}","cdiid":None,"sys_modified_at":0}
            flag_dict = self.match_engine.findMatch(env,self.contact_processors.get('svro').getOdooFormat(msg,self.iso2,self.correction))
            msg = {"id":32697,"first_name":"Anaandaan Siva","last_name":None,"personal_email":"siva1@yahoo.com","date_of_birth":"1957-10-28","gender":"Male","marital_status":None,"address":"221B baker street,","city":"","state":None,"country_of_residence":"India","postal_code":"625312","phone":"a:1:{i:0;a:2:{s:4:\"type\";s:8:\"Personal\";s:6:\"number\";s:10:\"9126450327\";}}","cdiid":None,"sys_modified_at":0}
            flag_dict = self.match_engine.findMatch(env,self.contact_processors.get('svro').getOdooFormat(msg,self.iso2,self.correction))
            msg = {"id":32697,"first_name":"Anaandaan Sivaa","last_name":None,"personal_email":"siva@yahoo.com","date_of_birth":"1957-10-28","gender":"Male","marital_status":None,"address":"221B baker street,","city":"","state":None,"country_of_residence":"India","postal_code":"625362","phone":"a:1:{i:0;a:2:{s:4:\"type\";s:8:\"Personal\";s:6:\"number\";s:10:\"9876543210\";}}","cdiid":None,"sys_modified_at":0}
            flag_dict = self.match_engine.findMatch(env,self.contact_processors.get('svro').getOdooFormat(msg,self.iso2,self.correction))

    def test_7_al_name_title(self):
        null = None
        msg = {"Pam_Serial_Number":945336,"Pam_Initial":null,"Pam_Participant_Name":"Dr. santosh nagaraj","Pam_Address_Line1":"B\u0000u\u0000i\u0000l\u0000d\u0000i\u0000n\u0000g\u0000 \u00001\u00004\u0000,\u0000 \u0000T\u0000o\u0000w\u0000e\u0000r\u0000 \u0000\u0013  \u0000C\u0000,\u0000 \u00001\u00007\u0000T\u0000h\u0000 \u0000F\u0000l\u0000o\u0000o\u0000r\u0000,\u0000 \u0000S\u0000e\u0000c\u0000t\u0000o\u0000r\u0000 \u00002\u00005\u0000-\u0000A\u0000,\u0000 \u0000D\u0000l\u0000f\u0000 \u0000C\u0000y\u0000b\u0000e\u0000r\u0000 \u0000C\u0000i\u0000t\u0000y\u0000,\u0000 \u0000D\u0000l\u0000f\u0000 \u0000P\u0000h\u0000a\u0000s\u0000e\u0000-\u00002\u0000","Pam_Address_Line2":"","Pam_Address_Line3":"","Pam_City":"Gurugram","Pam_District_County":"","Pam_State":"Haryana","Pam_Country":"India","Pam_Postal_Code":"122002","Pam_Home_Phone":"","Pam_Mobile_Phone":"8722044007","Pam_Mobile_Phone1":null,"Pam_Email_ID":"","Pam_Oemail_Id":null,"Pam_Occupation":"Salaried","Pam_Work_Address_Line1":"","Pam_Work_Address_Line2":"","Pam_Work_Address_Line3":"","Pam_Work_City":"","Pam_Work_District_County":"","Pam_Work_State":"","Pam_Work_Country":"India","Pam_Work_PostalCode":"","Pam_Work_Phone":"","Pam_Gender":"M","Pam_Age":"","Pam_Date_Of_Birth":"1975-10-07","Pam_Record_Created_By":"ORS","Pam_Record_Modified_By":null,"Pam_Record_Created_Date":1571498534000,"Pam_Record_Modified_Date":1571498534000}
        odoo_rec = self.contact_processors.get('al').getOdooFormat(msg,self.iso2,self.correction)
        self.assertEqual('Dr',odoo_rec['prof_dr'])
        self.assertEqual('santosh nagaraj', odoo_rec['name'])
        self.assertEqual('IN',odoo_rec['country'])
        self.assertEqual('IN', odoo_rec['work_country'])
        msg = {"Pam_Serial_Number":945336,"Pam_Initial":null,"Pam_Participant_Name":"M. santosh nagaraj Dr","Pam_Address_Line1":"B\u0000u\u0000i\u0000l\u0000d\u0000i\u0000n\u0000g\u0000 \u00001\u00004\u0000,\u0000 \u0000T\u0000o\u0000w\u0000e\u0000r\u0000 \u0000\u0013  \u0000C\u0000,\u0000 \u00001\u00007\u0000T\u0000h\u0000 \u0000F\u0000l\u0000o\u0000o\u0000r\u0000,\u0000 \u0000S\u0000e\u0000c\u0000t\u0000o\u0000r\u0000 \u00002\u00005\u0000-\u0000A\u0000,\u0000 \u0000D\u0000l\u0000f\u0000 \u0000C\u0000y\u0000b\u0000e\u0000r\u0000 \u0000C\u0000i\u0000t\u0000y\u0000,\u0000 \u0000D\u0000l\u0000f\u0000 \u0000P\u0000h\u0000a\u0000s\u0000e\u0000-\u00002\u0000","Pam_Address_Line2":"","Pam_Address_Line3":"","Pam_City":"Gurugram","Pam_District_County":"","Pam_State":"Haryana","Pam_Country":"Mylapore","Pam_Postal_Code":"122002","Pam_Home_Phone":"","Pam_Mobile_Phone":"8722044007","Pam_Mobile_Phone1":null,"Pam_Email_ID":"","Pam_Oemail_Id":null,"Pam_Occupation":"Salaried","Pam_Work_Address_Line1":"","Pam_Work_Address_Line2":"","Pam_Work_Address_Line3":"","Pam_Work_City":"","Pam_Work_District_County":"","Pam_Work_State":"","Pam_Work_Country":"","Pam_Work_PostalCode":"","Pam_Work_Phone":"","Pam_Gender":"M","Pam_Age":"","Pam_Date_Of_Birth":"1975-10-07","Pam_Record_Created_By":"ORS","Pam_Record_Modified_By":null,"Pam_Record_Created_Date":1571498534000,"Pam_Record_Modified_Date":1571498534000}
        odoo_rec = self.contact_processors.get('al').getOdooFormat(msg,self.iso2,self.correction)
        self.assertEqual('M santosh nagaraj', odoo_rec['name'])
        self.assertEqual('Dr', odoo_rec['prof_dr'])
        self.assertEqual('IN', odoo_rec['country'])
        msg = {"Pam_Serial_Number":945336,"Pam_Initial":null,"Pam_Participant_Name":"santosh nagaraj .M","Pam_Address_Line1":"B\u0000u\u0000i\u0000l\u0000d\u0000i\u0000n\u0000g\u0000 \u00001\u00004\u0000,\u0000 \u0000T\u0000o\u0000w\u0000e\u0000r\u0000 \u0000\u0013  \u0000C\u0000,\u0000 \u00001\u00007\u0000T\u0000h\u0000 \u0000F\u0000l\u0000o\u0000o\u0000r\u0000,\u0000 \u0000S\u0000e\u0000c\u0000t\u0000o\u0000r\u0000 \u00002\u00005\u0000-\u0000A\u0000,\u0000 \u0000D\u0000l\u0000f\u0000 \u0000C\u0000y\u0000b\u0000e\u0000r\u0000 \u0000C\u0000i\u0000t\u0000y\u0000,\u0000 \u0000D\u0000l\u0000f\u0000 \u0000P\u0000h\u0000a\u0000s\u0000e\u0000-\u00002\u0000","Pam_Address_Line2":"","Pam_Address_Line3":"","Pam_City":"Gurugram","Pam_District_County":"","Pam_State":"Haryana","Pam_Country":"Syrian Arab Republic","Pam_Postal_Code":"122002","Pam_Home_Phone":"","Pam_Mobile_Phone":"8722044007","Pam_Mobile_Phone1":null,"Pam_Email_ID":"","Pam_Oemail_Id":null,"Pam_Occupation":"Salaried","Pam_Work_Address_Line1":"","Pam_Work_Address_Line2":"","Pam_Work_Address_Line3":"","Pam_Work_City":"","Pam_Work_District_County":"","Pam_Work_State":"","Pam_Work_Country":"","Pam_Work_PostalCode":"","Pam_Work_Phone":"","Pam_Gender":"M","Pam_Age":"","Pam_Date_Of_Birth":"1975-10-07","Pam_Record_Created_By":"ORS","Pam_Record_Modified_By":null,"Pam_Record_Created_Date":1571498534000,"Pam_Record_Modified_Date":1571498534000}
        odoo_rec = self.contact_processors.get('al').getOdooFormat(msg,self.iso2,self.correction)
        self.assertEqual('santosh nagaraj M', odoo_rec['name'])
        self.assertEqual('SY', odoo_rec['country'])
        msg = {"Pam_Serial_Number":945336,"Pam_Initial":null,"Pam_Participant_Name":"Dr M.santosh nagaraj","Pam_Address_Line1":"B\u0000u\u0000i\u0000l\u0000d\u0000i\u0000n\u0000g\u0000 \u00001\u00004\u0000,\u0000 \u0000T\u0000o\u0000w\u0000e\u0000r\u0000 \u0000\u0013  \u0000C\u0000,\u0000 \u00001\u00007\u0000T\u0000h\u0000 \u0000F\u0000l\u0000o\u0000o\u0000r\u0000,\u0000 \u0000S\u0000e\u0000c\u0000t\u0000o\u0000r\u0000 \u00002\u00005\u0000-\u0000A\u0000,\u0000 \u0000D\u0000l\u0000f\u0000 \u0000C\u0000y\u0000b\u0000e\u0000r\u0000 \u0000C\u0000i\u0000t\u0000y\u0000,\u0000 \u0000D\u0000l\u0000f\u0000 \u0000P\u0000h\u0000a\u0000s\u0000e\u0000-\u00002\u0000","Pam_Address_Line2":"","Pam_Address_Line3":"","Pam_City":"Gurugram","Pam_District_County":"","Pam_State":"Haryana","Pam_Country":"India","Pam_Postal_Code":"122002","Pam_Home_Phone":"","Pam_Mobile_Phone":"8722044007","Pam_Mobile_Phone1":null,"Pam_Email_ID":"","Pam_Oemail_Id":null,"Pam_Occupation":"Salaried","Pam_Work_Address_Line1":"","Pam_Work_Address_Line2":"","Pam_Work_Address_Line3":"","Pam_Work_City":"","Pam_Work_District_County":"","Pam_Work_State":"","Pam_Work_Country":"","Pam_Work_PostalCode":"","Pam_Work_Phone":"","Pam_Gender":"M","Pam_Age":"","Pam_Date_Of_Birth":"1975-10-07","Pam_Record_Created_By":"ORS","Pam_Record_Modified_By":null,"Pam_Record_Created_Date":1571498534000,"Pam_Record_Modified_Date":1571498534000}
        odoo_rec = self.contact_processors.get('al').getOdooFormat(msg,self.iso2,self.correction)
        self.assertEqual('M santosh nagaraj', odoo_rec['name'])
        self.assertEqual('Dr', odoo_rec['prof_dr'])


    def test_8_al(self):
        msg1 =  {"Pam_Serial_Number": 2, "Pam_Initial": " ", "Pam_Participant_Name": "VINOD R R",
               "Pam_Address_Line1": "1", "Pam_Address_Line2": "Sriram Nager", "Pam_Address_Line3": "Hudco Coloney",
               "Pam_City": "Coimbatore", "Pam_District_County": "", "Pam_State": "Tamil Nadu", "Pam_Country": "India",
               "Pam_Postal_Code": "560097", "Pam_Home_Phone": "", "Pam_Mobile_Phone": "7057802204",
               "Pam_Mobile_Phone1": "7760749054", "Pam_Email_ID": "vinod0511@gmail.com", "Pam_Oemail_Id": "vinodkumarrajputh@gmail.com",
               "Pam_Occupation": " ", "Pam_Work_Address_Line1": " ", "Pam_Work_Address_Line2": " ",
               "Pam_Work_Address_Line3": " ", "Pam_Work_City": " ", "Pam_Work_District_County": "",
               "Pam_Work_State": " ", "Pam_Work_Country": " ", "Pam_Work_PostalCode": " ", "Pam_Work_Phone": " ",
               "Pam_Gender": "M", "Pam_Age": "39", "Pam_Date_Of_Birth": "03-11-1970", "Pam_Record_Created_By": "0",
               "Pam_Record_Modified_By": "0", "Pam_Record_Created_Date": 1268839511000,
               "Pam_Record_Modified_Date": 19801000}
        msg2 =  {"Pam_Serial_Number": 2, "Pam_Initial": " ", "Pam_Participant_Name": "Sabari P R",
               "Pam_Address_Line1": "1", "Pam_Address_Line2": "Sriram Nager", "Pam_Address_Line3": "Hudco Coloney",
               "Pam_City": "Coimbatore", "Pam_District_County": "", "Pam_State": "Tamil Nadu", "Pam_Country": "India",
               "Pam_Postal_Code": "620001", "Pam_Home_Phone": "", "Pam_Mobile_Phone": "9686802990",
               "Pam_Mobile_Phone1": "", "Pam_Email_ID": "", "Pam_Oemail_Id": "",
               "Pam_Occupation": " ", "Pam_Work_Address_Line1": " ", "Pam_Work_Address_Line2": " ",
               "Pam_Work_Address_Line3": " ", "Pam_Work_City": " ", "Pam_Work_District_County": "",
               "Pam_Work_State": " ", "Pam_Work_Country": " ", "Pam_Work_PostalCode": " ", "Pam_Work_Phone": " ",
               "Pam_Gender": "M", "Pam_Age": "39", "Pam_Date_Of_Birth": "03-11-1970", "Pam_Record_Created_By": "0",
               "Pam_Record_Modified_By": "0", "Pam_Record_Created_Date": 1268839511000,
               "Pam_Record_Modified_Date": 19801000}
        UID = SUPERUSER_ID
        cr = self.registry.cursor()
        with api.Environment.manage():
            env = api.Environment(cr, UID, {})
            flag_dict = self.match_engine.createOrUpdate(env, self.contact_processors.get('al').getOdooFormat(msg2,self.iso2,self.correction))
            flag_dict = self.match_engine.createOrUpdate(env, self.contact_processors.get('al').getOdooFormat(msg1,self.iso2,self.correction))

    def test_9_al_bool_error(self):
        null = None
        msg = {"row_id":2473094,"record_modified_at":1570752044,"id":"OTR-1920-5d9c94ba29c58d491215a830","modified_at":"2019-10-10T00:00:00Z","modified_by":"emedia@ishafoundation.org","contact_type":"INDIVIDUAL","first_name":"Tushar Petigara","nationality":"India","phones":"8141335246433","emails":"Lala_thk@yahoo.com","address1":"B-1/303,sai Milan Residency ","town_city1":"SURAT","state_province_region1":"Gujarat","postal_code1":"395009","country1":"India","pan_number":"","passport_number":null}
        flag_dict = self.match_engine.createOrUpdate(self.registry,  self.contact_processors.get('erecipts').getOdooFormat(msg,self.iso2,self.correction))
        self.assertTrue(flag_dict['flag'])
        print(flag_dict['phase'])

    def test_10_unlink(self):
        map_list=[27951,27953,27954]
        dst_id = 3
        UID = SUPERUSER_ID
        cr = self.registry.cursor()
        with api.Environment.manage():
            env = api.Environment(cr, UID, {})
            contactModel = env['contact.map'].update_foreign_keys(map_list,dst_id)
            env['donation.isha.foundation'].browse([20,22,23]).write({'active':False})
        cr.commit()

    def test_11_kafka_error(self):
        UID = SUPERUSER_ID
        cr = self.registry.cursor()
        with api.Environment.manage():
            env = api.Environment(cr, UID, {})
            error = env['kafka.errors'].reprocess_errors()
    def test_12_check_phones(self):
        dms= {"row_id": 15, "local_contact_id": "103de873-28cf-0e5d-ec6a-575aa8da3a34",
                "modified_at": "2016-06-21T09:23:21Z", "modified_by": "PremalathaJ", "contact_type": "ORGANIZATION",
                "title": "", "first_name": "Shimpo Wood Sdn Bhd", "dob": "", "gender": "", "occupation": "",
                "company_ids": "", "nationality": "malaysia", "phones": "+919998889999;+918015431114",
                "emails": "test1@yahoo.com", "address1": "Lot 1680 Lorong Haji Musran Rantau Panjang", "town_city1": "",
                "state_province_region1": "", "postal_code1": "421000", "country1": "India", "pan_number": "",
                "deceased": 0, "do_not_call": 0, "do_not_email": 0, "do_not_postmail": 0, "active": 1}
        rec = self.contact_processors.get('dms').getOdooFormat(dms,self.iso2,self.correction)
        x=1
        UID = SUPERUSER_ID
        cr = self.registry.cursor()
        with api.Environment.manage():
            env = api.Environment(cr, UID, {})
            contactModel = env['res.partner']
            rec = contactModel.sudo().create_internal(rec)
            x = 1
            cr.commit()
    if __name__ == '__main__':
        unittest.main()
