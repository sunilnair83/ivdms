import csv
import json
from datetime import datetime
from xmlrpc import client

#odoo server config
input_file_name = "al_dec15.csv"
username = "admin"
password = "pwd"
url = "https://ishangam.isha.in"
client.ServerProxy('{}/xmlrpc/2/common'.format(url))
db = "ishangam"
common = client.ServerProxy('{}/xmlrpc/2/common'.format(url))
uid = common.authenticate(db, username, password, {})
odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url))


def create_pgm(pgm):
    odoo_models.execute_kw(db, uid, password,
                           'program.attendance', 'create',
                           [pgm])


def txn_exists(local_trans_id):
    ids = odoo_models.execute_kw(db, uid, password,
                                 'program.attendance', 'search',
                                 [[['local_trans_id', 'ilike', local_trans_id]]])
    if ids:
        id = max(ids)
        last_tnxn_id = odoo_models.execute_kw(db, uid, password,
                                              'program.attendance', 'read',
                                              [id], {'fields': ['local_trans_id']})
        return last_tnxn_id[0]['local_trans_id']
    else:
        False


################## transformations ##########

# get isha center
center_dict = {}
centers = odoo_models.execute_kw(db, uid, password,
                                 'isha.center', 'search_read',
                                 [[['active', '=', True]]],
                                 {'fields': ['id', 'name']})
for x in centers:
    center_dict[x['name']] = x['id']

# gender transform
gender_dict = {
    'Male': 'M', 'Female': 'F', 'male': 'M', 'female': 'F'
}


# date transform
def get_date_formatted(str_date, from_format):
    date_parsed = datetime.strptime(str_date, from_format) if str_date else None
    return date_parsed.strftime("%Y-%m-%d") if date_parsed else None


# source db config
with open(input_file_name, mode='r') as csv_file:
    csv_reader = csv.DictReader(csv_file)
    line_count = 0
    skip_till = txn_exists('AL-2020-Dec15-')
    skip = True if skip_till else False
    for row in csv_reader:

        x = json.dumps(row)
        x = eval(x)
        if skip:
            if x['local_trans_id'] == skip_till:
                skip = False
            print('skippped ' + x['local_trans_id'])
            continue

        x['gender'] = gender_dict[str.lower(x['gender'])] if x['gender'] else ''
        x['dob'] = get_date_formatted(x['dob'], '%Y-%m-%d')
        x['center_id'] = center_dict[x['contact_center_name']] if x['contact_center_name'] else ''
        x['start_date'] = get_date_formatted(x['start_date'], '%m/%d/%Y')
        x['end_date'] = get_date_formatted(x['end_date'], '%m/%d/%Y')
        x['reg_date_time'] = get_date_formatted(x['reg_date_time'], '%Y') + ' 00:00:00' if x['reg_date_time'] else x['start_date'] + ' 00:00:00'
        # for non char fields, ensure that None is not passed as xml rpc is not accepting it.
        # remove such fields from the input dict
        if not x['dob']:
            x.pop('dob')
        if not x['start_date']:
            x.pop('start_date')
        if not x['end_date']:
            x.pop('end_date')
        if not x['center_id']:
            x.pop('center_id')
        if 'age' in x:
            x.pop('age')
        x.pop('contact_center_name')
	
#        create_pgm(x)
        print('created '+ x['local_trans_id'])




