--- db archives are present in /home/sutharson/gdrive/FMF-DB-ARCHIVES/JULY/

----- temp_fmf_platform,temp_fmf_backup,temp_fmf_sso,temp_fmf_email

-- create temp tables as per the csv format with additional columns for source and sso_id if it is not present
-- source will be one of the vales
--1-> platform
--2-> backup prs with sso
--3-> backup prs without sso
--4 -> backup netlify / dynamo


--- ecg should be a text field
-- delete whereever the lastloggedintime is null


--- ceanup the dynamo db using the custom script
-- load it to a temp table

-- load the sadhana page data to 2 seperate tables




----------- commands
-- platform file
create table temp_fmf_platform (city character varying, country character varying, createdAt character varying, ecg text, id character varying, isComplete character varying, lastLoggedInTime character varying, name character varying, programType character varying, rollNumber character varying, sessionId character varying, sessionName character varying, ssoId character varying, state character varying, streamLanguage character varying, timeZone character varying, type character varying, updatedAt character varying);

\copy temp_fmf_platform (city,country,createdAt,ecg,id,isComplete,lastLoggedInTime,name,programType,rollNumber,sessionId,sessionName,ssoId,state,streamLanguage,timeZone,type,updatedAt) from  './studentsessions-july-full-export.csv' CSV HEADER;


delete from temp_fmf_platform where lastLoggedInTime is null;

delete from temp_fmf_email where sso_id in (select sso_id  from temp_fmf_dynamo);

delete from temp_fmf_sso where sso_id in (select sso_id  from temp_fmf_dynamo);


create table temp_fmf_dynamo (email character varying, language character varying, updatedAt character varying, timestamp character varying, streamLanguage character varying);

\copy temp_fmf_dynamo (email,language,updatedAt,timestamp,streamLanguage) from './backup.csv' CSV HEADER;

create table temp_fmf_email(program_name character varying, email character varying, trans_lang character varying, event_attendance_date character varying);
\copy temp_fmf_email (program_name,email,trans_lang,event_attendance_date) from './fmf_backup_withoutsso_july.csv' CSV HEADER;

create table temp_fmf_sso(program_name character varying, trans_lang character varying, event_attendance_date character varying, email character varying, name character varying, sso_id character varying) ;

\copy temp_fmf_sso (program_name,trans_lang,event_attendance_date,email,name,sso_id) from './fmf_backup_withsso_july.csv' CSV HEADER;


-- delete the dupes
delete from temp_fmf_email where email in (select email from temp_fmf_sso);
delete from temp_fmf_sso source using  (select sso_id, max(event_attendance_date) as max_date from temp_fmf_sso group by sso_id) sub where sub.sso_id = source.sso_id and event_attendance_date != sub.max_date;


alter table temp_fmf_email add column sso_id varchar, add column source varchar;

update temp_fmf_email temp set sso_id = pl.sso_id from program_lead pl where lower(temp.email) = lower(pl.record_email ) and pl.program_name = 'Online Pournami Satsang' and pl.pgm_remark = 'First timer' and pl.active = true and pl.start_date <= '2021-07-23';

delete from temp_fmf_email where sso_id is null;


delete from temp_fmf_email source using  (select sso_id, max(event_attendance_date) as max_date from temp_fmf_email group by sso_id) sub where sub.sso_id = source.sso_id and event_attendance_date != sub.max_date;


alter table temp_fmf_dynamo add column sso_id varchar, add column source varchar;
-- cleanup for dynamo
update temp_fmf_dynamo temp set sso_id = pl.sso_id from program_lead pl where lower(temp.email) = lower(pl.record_email ) and pl.program_name = 'Online Pournami Satsang' and pl.pgm_remark = 'First timer' and pl.active = true and pl.start_date <= '2021-07-23';

delete from temp_fmf_dynamo where sso_id is null;


delete from temp_fmf_dynamo source using  (select sso_id, max(updatedat) as max_date from temp_fmf_dynamo group by sso_id) sub where sub.sso_id = source.sso_id and updatedat != sub.max_date;

--- ensure that dupes are cleared else use partiotion to clean up


alter table temp_fmf_sso add column source varchar;
alter table temp_fmf_platform add column source varchar;

--- update source
update temp_fmf_email set source = '3';
update temp_fmf_sso set source = '2';
update temp_fmf_platform set source = '1';
update temp_fmf_dynamo set source = '4';
--


update temp_fmf_email set event_attendance_date = TO_TIMESTAMP(event_attendance_date, 'DD-MM-YYYY HH24:MI:SS');
update temp_fmf_sso set event_attendance_date = TO_TIMESTAMP(event_attendance_date, 'DD-MM-YYYY HH24:MI:SS');

update temp_fmf_email set event_attendance_date = to_char(event_attendance_date::timestamp - interval '330 minutes','YYYY-MM-DD HH24:MI:SS');
update temp_fmf_sso set event_attendance_date = to_char(event_attendance_date::timestamp - interval '330 minutes','YYYY-MM-DD HH24:MI:SS');



---- delete the sadhana page recs if it is in dynamo
delete from temp_fmf_email where sso_id in (select sso_id  from temp_fmf_dynamo);
delete from temp_fmf_sso where sso_id in (select sso_id  from temp_fmf_dynamo);

--- delete from platform if the rec is in any of the backup pages
delete from temp_fmf_platform where ssoid in (select sso_id  from temp_fmf_dynamo union select sso_id  from temp_fmf_email union select sso_id  from temp_fmf_sso);




---- migrating data to platform table
insert into temp_fmf_platform (ssoid,lastloggedintime,streamlanguage,source) select sso_id,event_attendance_date,
trans_lang,source from temp_fmf_sso ;
insert into temp_fmf_platform (ssoid,lastloggedintime,streamlanguage,source) select sso_id,event_attendance_date,
trans_lang,source from temp_fmf_email ;
insert into temp_fmf_platform (ssoid,lastloggedintime,streamlanguage,source) select sso_id,updatedat,language,source from temp_fmf_dynamo ;



----------------ENSURE AFTER MIGRATION THAT NO DUPES ARE PRESENT IN THE PLATFORM TABLE
select count(*) from temp_fmf_platform group by ssoid having count(*)>1;



---- clean up the platform table
update temp_fmf_platform set streamlanguage = initcap(streamlanguage);
update temp_fmf_platform set timeZone = 'Asia/Kolkata' where timeZone = 'IST';
update temp_fmf_platform set timeZone = 'America/New_York' where timeZone = 'EST';
update temp_fmf_platform set timeZone = 'America/Los_Angeles' where timeZone = 'PST';




--- Direct check for that month
select count(*) from temp_fmf_platform temp,program_lead pl where pl.sso_id = temp.ssoid and pl.program_name = 'Online Pournami Satsang' and pl.active = true and pl.pgm_remark = 'First timer' and pl.start_date >= <month>

select temp.source,temp.ecg,temp.lastLoggedInTime::timestamp,temp.streamLanguage,temp.timeZone from temp_fmf_platform temp,program_lead pl where pl.sso_id = temp.ssoid and pl.program_name = 'Online Pournami Satsang' and pl.active = true and pl.pgm_remark = 'First timer' and pl.start_date >= <month>

-- update the status,start_date,write_date for the direct month participants


update program_lead pl set start_date = <month>,reg_status='ATTENDED',write_date=now(),ie_hear_about = temp.source,ecg_heart_beat = temp.ecg,last_login_date = temp.lastLoggedInTime::timestamp,stream_lang = temp.streamLanguage,stream_tz = temp.timeZone from temp_fmf_platform temp where pl.sso_id = temp.ssoid and pl.program_name = 'Online Pournami Satsang'
and pl.active = true and pl.pgm_remark = 'First timer' and pl.start_date >= <month>



--- FOR RSVP people

insert into program_lead (ie_hear_about,ecg_heart_beat,last_login_date,stream_lang,stream_tz,record_name,record_phone,
	record_email,record_country,bay_name,trans_lang,program_name,start_date,reg_status,active,pgm_type_master_id,
	contact_id_fkey,pgm_remark,local_program_type_id,sso_id,create_date,create_uid,write_date,write_uid)
(select temp.source,temp.ecg,temp.lastLoggedInTime::timestamp,temp.streamLanguage,temp.timeZone, pl.record_name,
	pl.record_phone,pl.record_email,pl.record_country,pl.bay_name,pl.trans_lang,pl.program_name,<month>,'ATTENDED',
	true,pl.pgm_type_master_id,pl.contact_id_fkey,'RSVP',pl.local_program_type_id,pl.sso_id,now(),1,now(),1 from
	program_lead pl, temp_fmf_platform temp where pl.sso_id = temp.ssoid and pl.start_date < <month> and
	pl.active=true and pl.pgm_remark = 'First timer' and pl.program_name = 'Online Pournami Satsang'
);

update program_lead set ie_hear_about= '4', write_date=now()
where start_date = '2021-07-23' and program_name = 'Online Pournami Satsang' and reg_status = 'ATTENDED' and ie_hear_about is null and active = true

