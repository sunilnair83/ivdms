from datetime import datetime, date, timedelta

import ContactProcessor


class PgmLeadProcessor:
    system_id = 13

    def removePlus(self, code):
        if code and type(code) == str and len(code)>1 and code[0]=='+':
            return code[1:]
        else:
            return code

    def getDateParsed(self, str_date):
        try:
            date_parsed = datetime.strptime(str_date, '%Y-%m-%d %H:%M:%S') if str_date else None
            date_parsed = str(date_parsed) if date_parsed else None
        except:
            date_parsed = None
        return date_parsed

    def getContactFormat(self, src_msg, iso2, correction):
        src_msg = ContactProcessor.replaceEmptyString(src_msg)
        name = src_msg['name']
        email = ContactProcessor.getTokenizedVersion(src_msg['email'], ',')
        occuption = src_msg['occupation'] if src_msg['occupation'] else None
        organization = src_msg['organization'] if src_msg['organization'] else None
        try:
            dob = datetime.strptime(src_msg['dob'], '%d-%m-%Y') if src_msg['dob'] else None
            dob = dob.strftime("%Y-%m-%d") if dob else None
        except:
            dob = None
        whatsapp_number = src_msg['whatsapp_number']
        whatsapp_country_code = self.removePlus(src_msg['whatsapp_country_code'])
        phone = src_msg['phone_country_code'] if src_msg['phone_country_code'] else ''
        phone += src_msg['mobile'] if src_msg['mobile'] else ''
        if phone == '':
            phone = None

        phone3 = None
        if src_msg['register_mobile']:
            phone2 = src_msg['register_mobile']
            phone3 = src_msg['alt_phone'] if 'alt_phone' in src_msg else None
        else:
            phone2 = src_msg['alt_phone'] if 'alt_phone' in src_msg else None

        id_card_type = src_msg['idcard_type'].upper() if src_msg['idcard_type'] else None
        id_card_no = src_msg['idcard_no'] if src_msg['idcard_no'] else None
        sso_id = src_msg['sso_id'] if 'sso_id' in src_msg and src_msg['sso_id'] else None
        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': src_msg['_id'],
            'active': True,
            'name': name, 'display_name': name,'prof_dr': None,
            'phone': phone, 'phone2': phone2,'phone3': phone3, 'phone4': None, 'whatsapp_number':whatsapp_number, 'whatsapp_country_code':whatsapp_country_code,
            'email': email['1'] if '1' in email else None, 'email2': email['2'] if '2' in email else None,'email3': email['3'] if '3' in email else None,
            'street': src_msg['address'],  'street2':None,'city': src_msg['city'], 'state': src_msg['state'], 'country': src_msg['country'], 'zip': src_msg['pincode'],
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': ContactProcessor.getgender(src_msg['gender']), 'occupation': occuption,
            'marital_status': ContactProcessor.getMaritalStatus(src_msg['marital_status']), 'dob': dob,
            'aadhaar_no': src_msg['adhar_no'], 'pan_no': None, 'passport_no': src_msg['passport_number'],
            'nationality': src_msg['nationality'], 'deceased': None, 'contact_type': None, 'companies': organization,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': sso_id

        }
        if not odooRec['email']:
            odooRec['email'] = src_msg['alt_email'] if 'alt_email' in src_msg else None
        elif not odooRec['email2']:
            odooRec['email2'] = src_msg['alt_email'] if 'alt_email' in src_msg else None
        else:
            odooRec['email3'] = src_msg['alt_email'] if 'alt_email' in src_msg else None
        if(id_card_type == 'AADHAAR'):
            odooRec['id_proofs'] = str({'AADHAAR':id_card_no})
        elif(id_card_type == 'PANCARD'):
            odooRec['id_proofs'] = str({'PANCARD':id_card_no})
        elif(id_card_type == 'PASSPORT'):
            odooRec['id_proofs'] = str({'PASSPORT':id_card_no})

        local_modified_at = self.getDateParsed(src_msg['modified_date'])
        if local_modified_at:
            odooRec['local_modified_date'] = local_modified_at
        else:
            odooRec['local_modified_date'] = self.getDateParsed(src_msg['created_date'])

        if 'ishangam_id' in src_msg:
            odooRec['contact_id_fkey'] = src_msg['ishangam_id']

        return ContactProcessor.validateValues(odooRec, iso2, correction)

    def parseDateTime(self, date_str):
        try:
            date_parsed = datetime.strptime(date_str, '%Y-%m-%d %H:%M') if date_str else None
            date_parsed = str(date_parsed) if date_parsed else None
        except:
            try:
                date_parsed = datetime.strptime(date_str, '%Y-%m-%d') if date_str else None
                date_parsed = str(date_parsed) if date_parsed else None
            except:
                date_parsed = None

        return date_parsed

    def getDateTime(self, date_str):
        try:
            date_parsed = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S') - timedelta(hours=5, minutes=30) if date_str else None
            date_parsed = str(date_parsed) if date_parsed else None
        except:
            try:
                date_parsed = datetime.strptime(date_str, '%Y-%m-%d %H:%M') - timedelta(hours=5, minutes=30) if date_str else None
                date_parsed = str(date_parsed) if date_parsed else None
            except:
                date_parsed = None

        return date_parsed

    def getEventAttDatetime(self, src_dt):
        try:
            return str(datetime.strptime(src_dt, '%d-%m-%Y %H:%M:%S') - timedelta(hours=5, minutes=30))
        except:
            return None
    def getDateFromDateTime(self, date_time):
        try:
            return datetime.strptime(date_time, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
        except:
            return None

    def getLastTxnDate(self, datetime):
        last_txn_date = None
        try:
            last_txn_date =  datetime.split(' ')[0]
        except:
            pass
        return last_txn_date

    def getFirstSunday(self, rsvp_month_year):
        if rsvp_month_year and type(rsvp_month_year)==str:
            # rsvp_month_year comes as mm-yy we need to return the first sunday for the corresponding month year
            try:
                dt_split = rsvp_month_year.split('-')
                dt = date(int('20' + dt_split[1]), int(dt_split[0]), 1)
                dt += timedelta(days=6 - dt.weekday())
                return str(dt)
            except Exception as ex:
                return False

    def getTxnFormat(self,src_msg):
        src_msg = ContactProcessor.replaceEmptyString(src_msg)
        try:
            sd = self.getFirstSunday(src_msg['rsvp_month_year']) if 'rsvp_month_year' in src_msg and src_msg['rsvp_month_year'] else None
            if not sd:
                sd = datetime.strptime(src_msg['start_date'], '%d-%m-%Y') if src_msg['start_date'] else None
                sd = sd.strftime('%Y-%m-%d') if sd else None
        except:
            sd = None

        try:
            ar = self.parseDateTime(src_msg['arrival_date'])
        except:
            ar = None

        try:
            dp = self.parseDateTime(src_msg['departure_date'])
        except:
            dp = None

        pgm_schedule_info={
            'teacher_id' : None,
            'teacher_name' : None,
            'center_id': None,
            'center_name': None,
            'location':src_msg['program_location'],
            'country':'IN',
            'remarks':None,
            'program_id':src_msg['program_id'],
            'schedule_name':src_msg['program_name'],
            'start_date':src_msg['start_date'],
            'end_date':src_msg['end_date'],
            'arrival_date': src_msg['arrival_date'],
            'departure_date': src_msg['departure_date'],
            'program_schedule_guid':None,
            'localScheduleId':None
        }
        odooRec = {
            'system_id':self.system_id,
            'active':True,
            'local_trans_id': src_msg['_id'],
            'local_contact_id': src_msg['_id'],
            'reg_status' : src_msg['status'] if 'status' in src_msg else None,
            'reg_date_time':src_msg['created_date'],
            'program_schedule_info':pgm_schedule_info,
            'local_program_type_id': src_msg['program_name'],
            'program_name': src_msg['program_name'],
            'start_date': sd,
            'event_attendance_date':self.getEventAttDatetime(src_msg['event_attendance_date']) if 'event_attendance_date' in src_msg else None,
            'center_name': None,
            'bay_name':src_msg['bay_name'],
            'arrival_date':ar,
            'departure_date':dp,
            'ie_hear_about':src_msg['ie_hear_about'] if 'ie_hear_about' in src_msg else None,
            'utm_medium':src_msg['utm_medium'] if 'utm_medium' in src_msg else None,
            'utm_campaign':src_msg['utm_campaign'] if 'utm_campaign' in src_msg else None,
            'ieo_class_status':src_msg['ieo_class_status'] if 'ieo_class_status' in src_msg else None,
            'checkin_status_day_2':src_msg['checkin_status_day_2'] if 'checkin_status_day_2' in src_msg else None,
            'trans_lang':src_msg['trans_lang'] if 'trans_lang' in src_msg else None,
            'webinar_event_id':src_msg['webinar_event_id'] if 'webinar_event_id' in src_msg and src_msg['webinar_event_id'] else None,
            'webinar_event_name':src_msg['webinar_event_name'] if 'webinar_event_name' in src_msg and src_msg['webinar_event_name'] else None,
            'webinar_event_date':self.getDateTime(src_msg['webinar_event_date']) if 'webinar_event_date' in src_msg and src_msg['webinar_event_date'] else None
        }

        if src_msg['program_name'] == 'Online Offerings Webinar Registration' or src_msg['program_name'] == 'Surya Shakthi Yogaveera Training':
            odooRec['reg_status'] = 'ATTENDED' if src_msg['event_attendance_date'] else odooRec['reg_status']

        if src_msg['program_name'] == 'Online Monthly Satsang Confirmation':
            odooRec['pgm_remark'] = 'RSVP'
            odooRec['program_name'] = 'Online Satsang'
            odooRec['local_program_type_id'] = 'Online Satsang'
            odooRec['start_date'] = self.getDateFromDateTime(odooRec['event_attendance_date'])

        if src_msg['program_id'] == '061' or src_msg['program_name'] == 'Sadhana Intensive':
            odooRec['program_name'] = 'Sadhana Support COVID-19'
            odooRec['local_program_type_id'] = 'Sadhana Support COVID-19'
            odooRec['program_schedule_info']['schedule_name'] = 'Sadhana Support COVID-19'
            odooRec['reg_date_time'] = src_msg['created_date']
            odooRec['start_date'] = src_msg['created_date'].split()[0]
            odooRec['program_schedule_info']['start_date'] = src_msg['created_date'].split()[0]
            odooRec['is_ie_done'] = True if src_msg['is_ie_done'] == 'Yes' else False


        return odooRec

    def process_message(self, src_msg_dict, env, metadata):
        iso2 = metadata['iso2']
        correction = metadata['correction']

        try:
            # logger.debug(msg)
            contactMsgDict = self.getContactFormat(src_msg_dict, iso2, correction)
            txn_dict = self.getTxnFormat(src_msg_dict)

            last_txn_date = self.getLastTxnDate( txn_dict['event_attendance_date'])
            last_txn_date = self.getLastTxnDate( txn_dict['reg_date_time']) if not last_txn_date else last_txn_date
            if last_txn_date:
                contactMsgDict['last_txn_date'] = last_txn_date

            gc = env['gc']
            contact_flag = gc.createOrUpdate({}, contactMsgDict)
            contact = contact_flag['rec']
            txn_dict['contact_id_fkey'] = contact['id']

            txn_dict['guid'] = contact['guid']

            self.createPgmInternal(env, txn_dict)
            return True
        finally:
            pass

    def createPgmInternal(self, ENV, src_rec):
        gc = ENV['gc']
        pgm_mst = gc.ishaPgmTypeMaster.get(src_rec['local_program_type_id'] + str(src_rec['system_id']), None)
        # pgm_mst = self.get_mapping_pgm_master(ENV,src_rec)
        if pgm_mst:
            src_rec.update({'pgm_type_master_id': pgm_mst})
        else:
            dummy = gc.ishaPgmTypeMaster['Placeholder']
            src_rec.update({'pgm_type_master_id': dummy})
        if src_rec['reg_status']:
            status = gc.regStatusMap.get(src_rec['reg_status'].lower() + str(src_rec['system_id']), None)
            if not status:
                status = src_rec['reg_status']
            src_rec.update({'reg_status': status})

        self.createOrUpdate(src_rec, ENV)

    def createOrUpdate(self, src_rec, env_model):
        gc = env_model['gc']
        existing_rec = None
        query = 'select * from program_lead where local_trans_id = \'' + src_rec[
            'local_trans_id'] + '\' and system_id = \'' + str(src_rec['system_id']) + '\';'
        gc.db_cursor.execute(query)
        temp = gc.db_cursor.fetchall()
        for row in temp:
            temp_dict = dict()
            for x in row:
                temp_dict[x] = row[x]
            existing_rec = temp_dict
        if existing_rec:
            existing_rec.update(src_rec)
            env_model['txn_updater_class'].write(existing_rec)
            return True

        record = env_model['txn_updater_class'].create(src_rec)
        return record
