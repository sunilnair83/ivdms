import json
from datetime import datetime

import psycopg2

import ContactProcessor


class AthithiContactsProcessor:
    system_id = 45

    def getContactFormat(self, src_msg, env, iso2, correction):
        gc = env['gc']

        company_id = None
        if src_msg['company']:
            if src_msg['company'] in gc.ishaCompanyMaster:
                company_id = gc.ishaCompanyMaster[src_msg['company']]
            else:
                db_cursor = gc.db_connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

                query = 'insert into isha_company(name, industry, sector) values(%s, %s, %s) RETURNING id;'
                db_cursor.execute(query, (src_msg['company'], src_msg['Industry'], src_msg['Sector']))
                company_id = db_cursor.fetchone()
                gc.db_connection.commit()
                db_cursor.close()

        primary_poc_id = None
        if src_msg['Primary POC ID'] and src_msg['Primary POC ID'] in gc.users:
            primary_poc_id = gc.users[src_msg['Primary POC ID']]
        secondary_poc_id = None
        if src_msg['Secondary POC ID'] and src_msg['Secondary POC ID'] in gc.users:
            secondary_poc_id = gc.users[src_msg['Secondary POC ID']]

        if src_msg['influencer_type'] == 'Influencer':
            src_msg['influencer_type'] = 'influencer'
        elif src_msg['influencer_type'] == 'Pre-Influencer':
            src_msg['influencer_type'] = 'pre_influencer'
        elif src_msg['influencer_type'] == 'Relative':
            src_msg['influencer_type'] = 'relative'
        elif src_msg['influencer_type'] == 'Unassigned':
            src_msg['influencer_type'] = 'unassigned'

        if src_msg['dob']:
            src_msg['dob'] = datetime.strptime(src_msg['dob'], '%d-%b-%Y').strftime('%Y-%m-%d')
        src_msg['system_id'] = self.system_id
        src_msg['active'] = True
        src_msg['prof_dr'] = src_msg['title']
        src_msg['display_name'] = src_msg['name']
        src_msg['occupation'] = src_msg['Occupation']
        src_msg['designation1'] = src_msg['designation']
        src_msg['company1'] = company_id
        src_msg['primary_poc'] = primary_poc_id
        src_msg['secondary_poc'] = secondary_poc_id
        src_msg['category'] = src_msg['relationship_status'].lower() if src_msg['relationship_status'] else None
        src_msg['deceased'] = False
        src_msg['contact_type'] = 'INDIVIDUAL'
        src_msg['companies'] = None
        src_msg['aadhaar_no'] = None
        src_msg['pan_no'] = None
        src_msg['passport_no'] = None
        src_msg['dnd_phone'] = None
        src_msg['dnd_email'] = None
        src_msg['dnd_postmail'] = None
        src_msg['sso_id'] = None
        src_msg['wikipedia_link'] = src_msg['Wikipedia']
        src_msg['facebook_id'] = src_msg['Facebook Link']
        insta_followers = None
        try:
            insta_followers = int(src_msg['instagram_followers'])
        except:
            pass

        twitter_followers = None
        try:
            twitter_followers = int(src_msg['twitter_followers'])
        except:
            pass

        fb_followers = None
        try:
            fb_followers = int(src_msg['Facebook Followers'])
        except:
            pass

        fb_likes = None
        try:
            fb_likes = int(src_msg['Facebook Likes'])
        except:
            pass

        linkedin_followers = None
        try:
            linkedin_followers = int(src_msg['linkedin_followers'])
        except:
            pass

        src_msg['insta_followers'] = insta_followers
        src_msg['facebook_followers'] = fb_followers
        src_msg['facebook_likes'] = fb_likes
        src_msg['twitter_followers'] = twitter_followers
        src_msg['linkedin_followers'] = linkedin_followers

        src_msg.pop('other POC', None)
        src_msg.pop('secondary POC', None)
        src_msg.pop('relationship_status', None)
        src_msg.pop('preferred_moc\n(preferred mode of contact)', None)
        src_msg.pop('Secondary POC ID', None)
        src_msg.pop('Primary POC ID', None)
        src_msg.pop('Occupation', None)
        src_msg.pop('primary POC', None)
        src_msg.pop('Sector', None)
        src_msg.pop('designation', None)
        src_msg.pop('company', None)
        src_msg.pop('Industry', None)
        src_msg.pop('Wikipedia', None)
        src_msg.pop('Facebook Link', None)
        src_msg.pop('instagram_followers', None)
        src_msg.pop('Facebook Followers', None)
        src_msg.pop('Facebook Likes', None)
        src_msg.pop('center_name', None)
        src_msg.pop('title', None)

        return ContactProcessor.validateValues(src_msg, iso2, correction)


    def process_message(self, src_msg_dict, env, metadata):
        iso2 = metadata['iso2']
        correction = metadata['correction']

        contactMsgDict = self.getContactFormat(src_msg_dict, env, iso2, correction)

        gc = env['gc']
        print('src rec: ' + json.dumps(contactMsgDict))

        contact_flag = gc.createOrUpdate({}, contactMsgDict)


