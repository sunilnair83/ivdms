#!/bin/sh

#export PGPASSWORD='odoo'

# cache for pincode center mapping
psql -h ishangam-app.isha.internal.in -p 6432 -U ishangam_role -d ishangam -c "\COPY (select ipc.name,center_id,ir.name as region_name,ipc.countryiso_2 from isha_pincode_center ipc,isha_center ic,isha_region ir where ipc.center_id = ic.id and ic.region_id = ir.id) TO './ipc.csv' CSV HEADER;"

# cache for res_partner name match engine
psql -h ishangam-app.isha.internal.in -p 6432 -U ishangam_role -d ishangam -c "\COPY (select id,name,phone,phone2,phone3,phone4,email,email2,email3,sso_id,write_date,influencer_type,guid from res_partner where active=true) TO './res_partner_nm.csv' CSV HEADER;"

# cache for program_type_master
psql -h ishangam-app.isha.internal.in -p 6432 -U ishangam_role -d ishangam -c "\COPY (select id,local_program_type_id,system_id from program_type_master) TO './pgm_master.csv' CSV HEADER;"

# cache for status mapping
psql -h ishangam-app.isha.internal.in -p 6432 -U ishangam_role -d ishangam -c "\COPY (select id,schedule_status,system_id from isha_program_status) TO './pgm_reg_status.csv' CSV HEADER;"

psql -h ishangam-app.isha.internal.in -p 6432 -U ishangam_role -d ishangam -c "\COPY (select id, purpose, entity from isha_purpose_project_mapping) TO './purpose_project_mapping.csv' CSV HEADER;"

psql -h ishangam-app.isha.internal.in -p 6432 -U ishangam_role -d ishangam -c "\COPY (select id, name, sector, industry from isha_company) TO './isha_company.csv' CSV HEADER;"

psql -h ishangam-app.isha.internal.in -p 6432 -U ishangam_role -d ishangam -c "\COPY (select id, partner_id from res_users) TO './res_users.csv' CSV HEADER;"
