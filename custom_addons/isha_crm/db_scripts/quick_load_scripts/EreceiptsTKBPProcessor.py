from datetime import datetime

import ContactProcessor


class EreceiptsTKBPProcessor:
    system_id = 8

    def process_message(self, src_msg_dict, env, metadata):
        gc = env['gc']
        iso2 = metadata['iso2']
        correction = metadata['correction']

        try:
            contactMsgDict = self.getGoldenFormatForContact(src_msg_dict['payload'], iso2, correction)
            txn_dict = self.getGoldenFormatForTxn(src_msg_dict)

            # If the project is Cottage then ignore creating the contact and create the txn without mapping to a contact
            contact_flag = self.contactCreateFlag(env, txn_dict, contactMsgDict)
            if contact_flag:
                contact_flag = gc.createOrUpdate({}, contactMsgDict)
                contact = contact_flag['rec']
                txn_dict['contact_id_fkey'] = contact['id']
                txn_dict['guid'] = contact['guid']

                self.createDonationInternal(env, txn_dict)
        finally:
            pass

    def createDonationInternal(self, ENV, src_rec):
        # entity = src_rec['entity'].lower()
        # self.donationModel = ENV[self.model_mapper.get(entity)]
        # self.contactMap = ENV['contact.map']

        # update_rec = self.donationModel.sudo().search(
        #     [('local_trans_id', '=', src_rec['local_trans_id']), ('system_id', '=', str(src_rec['system_id']))], limit=1)
        txn_updater = ENV['txn_updater_class']
        query = 'select * from donation_tkbp where local_trans_id = \'' + src_rec[
            'local_trans_id'] + '\' and system_id = \'' + str(src_rec['system_id']) + '\' limit 1;'
        gc = ENV['gc']
        gc.db_cursor.execute(query)
        temp = gc.db_cursor.fetchall()
        existing_rec = None
        for row in temp:
            temp_dict = dict()
            for x in row:
                temp_dict[x] = row[x]
            existing_rec = temp_dict
        if existing_rec:
            src_rec.pop('entity', None)
            existing_rec.update(src_rec)
            print('trans_id: ' + src_rec['local_trans_id'])
            txn_updater.write(existing_rec)
            return {'flag': True, 'rec': existing_rec}
        else:
            src_rec.update({'purpose_id': self.getMappingProject(ENV, src_rec)})
            src_rec.pop('entity', None)
            # Requirement as per FR
            # If a record from ERECEIPTS(system_id = 8) has receipt_number with 'a' suffix then it is modified.
            # So if a record from DMS(system_id = 7) comes with same receipt_number without suffix set DMS to active false
            # else set the ERECEIPTS to active false
            # self.activeFieldCheck(src_rec)
            if src_rec['status'] == 'SUCCESS':
                src_rec['active'] = 1
            else:
                src_rec['active'] = 0
            rec = txn_updater.create(src_rec)
            return {'flag': True, 'rec': rec}

    def getPurposeId(self, env, rec):
        if rec['project'] is not None:
            purpose_id = self.getMappingProject(env, rec)
            if len(purpose_id) != 0:
                return purpose_id
        return env.ref('isha_crm.isha_purpose_project_mapping_dummy').id

    def getMappingProject(self, env, txn_dict):
        gc = env['gc']
        if txn_dict['project'] + txn_dict['entity'] in gc.purposeProjMap:
            project_id = gc.purposeProjMap[txn_dict['project'] + txn_dict['entity']]
        else:
            project_id = gc.purposeProjMap['PlaceholderPlaceholder']
        return project_id

    def contactCreateFlag(self, env, txn_dict, contactMsgDict):

        project_id = self.getMappingProject(env, txn_dict)

        # project_rec = self.getMappingProject(env, txn_dict)
        # contactMsgDict['last_txn_date'] = txn_dict['don_receipt_date']
        # contactMsgDict.update(self.get_don_flags(self.getMappingProject(env, txn_dict)))
        txn_dict['purpose_id'] = project_id
        return True

    def getGoldenFormatForContact(self, msg, iso2, correction):
        msg = ContactProcessor.replaceEmptyString(msg)
        phone = ContactProcessor.getTokenizedVersion(msg['phones'],',')
        email = {'1':msg['emails']}
        dob = None

        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': msg['id'],
            'active': True,
            'name': msg['first_name'], 'display_name': msg['first_name'], 'prof_dr': None,
            'phone': phone['1'] if '1' in phone else None, 'phone2': phone['2'] if '2' in phone else None,
            'phone3': phone['3'] if '3' in phone else None,'phone4': phone['4'] if '4' in phone else None,
            'whatsapp_number': None, 'whatsapp_country_code': None,
            'email': email['1'] if '1' in email else None, 'email2': email['2'] if '2' in email else None,'email3': email['3'] if '3' in email else None,
            'street': msg['address1'],  'street2':None,'city': msg['town_city1'], 'state': msg['state_province_region1'], 'country': msg['country1'],
            'zip': msg['postal_code1'],
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': None, 'occupation': None, 'marital_status': None, 'dob': dob,
            'nationality': msg['nationality'], 'deceased': None, 'contact_type': msg['contact_type'], 'companies': None,
            'aadhaar_no': None, 'pan_no': msg['pan_number'], 'passport_no': msg['passport_number'],
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': msg['sso_id'] if 'sso_id' in msg else None

        }
        if 'modified_at' in msg and msg['modified_at']:
            odooRec['local_modified_date'] = ContactProcessor.getDateParsed(msg['modified_at'])
        return ContactProcessor.validateValues(odooRec, iso2, correction)

    def getDateTimeField(self, src_date):
        return datetime.strptime(src_date, '%Y-%m-%dT%H:%M:%SZ').strftime('%Y-%m-%d')

    def getGoldenFormatForTxn(self, src_msg):
        src_msg = ContactProcessor.replaceEmptyString(src_msg['payload'])
        if 'local_patron_id' in src_msg and src_msg['local_patron_id']:
            pat_id = src_msg['local_patron_id']
            pat_poc_id = src_msg['patron_poc_id'] if 'patron_poc_id' in src_msg else None
        else:
            pat_id = src_msg['contact_id'] if 'contact_id' in src_msg else None
            pat_poc_id = src_msg['donor_poc_id'] if 'donor_poc_id' in src_msg else None
        if 'donation_type' not in src_msg or src_msg['donation_type'] is None:
            donation = 'DONATION'
        rec_date = None
        if src_msg['receipt_date']:
            try:
                date_time = self.getDateTimeField(src_msg['receipt_date'])
            except:
                pass
        odooRec = {
            'local_donor_id': src_msg['contact_id'] if 'contact_id' in src_msg else None,
            'donor_poc_id': src_msg['donor_poc_id'] if 'donor_poc_id' in src_msg else None,
            'local_patron_id': pat_id,
            'patron_poc_id': pat_poc_id,
            'project': src_msg['project'] if 'project' in src_msg else None,
            'mode_of_payment': src_msg['mode_of_payment'] if 'mode_of_payment' in src_msg else None,
            'inkind': src_msg['inkind'] if 'inkind' in src_msg else None,
            'amount': src_msg['amount'] if 'amount' in src_msg else None,
            'receipt_number': src_msg['receipt_number'] if 'receipt_number' in src_msg else None,
            'don_receipt_date': date_time,
            'active': src_msg['active'] if 'active' in src_msg else None,
            'local_trans_id': src_msg['id'] if 'id' in src_msg else None,
            'status': src_msg['status'] if 'status' in src_msg else None,
            'donation_type': donation,
            'entity': src_msg['entity'] if 'entity' in src_msg else None,
            'system_id': self.system_id,
            'center': src_msg['center'] if 'center' in src_msg else None,
            'ebook_number': src_msg['ebook_number']

        }
        return odooRec
