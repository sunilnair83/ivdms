import csv

import psycopg2.extras

import GoldenContactAdv
import PgmLeadProcessor

"""
\COPY (select ipc.name,center_id,ir.name as region_name,ipc.countryiso_2 from isha_pincode_center ipc,isha_center ic,isha_region ir where ipc.center_id = ic.id and ic.region_id = ir.id) TO './ipc.csv' CSV HEADER;

\COPY (select id,name,phone,phone2,phone3,phone4,email,email2,email3,sso_id,write_date,is_flagged,guid from res_partner where active=true) TO './res_partner_nm.csv' CSV HEADER;


"""

with open('country-nationality-iso2-map.csv') as f:
    iso2 = dict(filter(None, csv.reader(f)))
with open('eReceipts-country-nationality-corrections.csv') as f:
    csvReader = []
    for x in csv.reader(f):
        csvReader.append([x[0].upper(), x[1].upper()])
    correction = dict(filter(None, csvReader))
print('loaded csv')
print(iso2['UK'])

conn = psycopg2.connect(database = "ishangam", user = "ishangam_role", host = "ishangam-app.isha.internal.in", port = "6432")
db_cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

db_cur.execute("""SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='res_partner';""")
res_partner_fields = []
for x in db_cur.fetchall():
    res_partner_fields.append(x['column_name'])
res_partner_fields.remove('id')
res_partner_fields.sort()

db_cur.execute("""SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='contact_map';""")
contact_map_fields = []
for x in db_cur.fetchall():
    contact_map_fields.append(x['column_name'])
contact_map_fields.remove('id')
contact_map_fields.sort()

db_cur.execute("""SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='program_attendance';""")
pgm_att_fields = []
for x in db_cur.fetchall():
    pgm_att_fields.append(x['column_name'])
pgm_att_fields.remove('id')
pgm_att_fields.sort()

db_cur.close()
conn.close()

pgm_att_file = GoldenContactAdv.OdooBase('pgm_att.csv', pgm_att_fields)
gc = GoldenContactAdv.GoldenContactAdv({'res_partner_fields':res_partner_fields,'contact_map_fields':contact_map_fields,
                                        'iso2':iso2,'correction':correction})

# gc.setEnvValues({})

pgm_att_processor = PgmLeadProcessor.PgmLeadProcessor()
index=0
input_data_file_name = 'vanashree.csv'

for i in range(1,2):
    with open(input_data_file_name, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            pgm_att_processor.process_message(row,{'txn_updater_class':pgm_att_file,'gc':gc,'iso2':iso2,'correction':correction},
                                          {'iso2':iso2,'correction':correction})
            index+=1
            print(index)

# with open('ieo_file2.csv', newline='') as csvfile:
#     reader = csv.DictReader(csvfile)
#     for row in reader:
#         sge_processor.process_message(row,{'sadhguru.exclusive':sge_txn_file,'gc':gc,'iso2':iso2,'correction':correction},{'iso2':iso2,'correction':correction})
#         index+=1
#         print(index)


res_partner_fields_update = 'id=excluded.id,'+','.join([x+'=excluded.'+x for x in res_partner_fields])
contact_map_fields_update = 'id=excluded.id,'+','.join([x+'=excluded.'+x for x in contact_map_fields])
pgm_att_fields_update = 'id=excluded.id,'+','.join([x+'=excluded.'+x for x in pgm_att_fields])


write_obj = open('sql.sql', 'w', newline='')
# Create a writer object from csv module
write_obj = csv.writer(write_obj)
write_obj.writerow(["""drop table res_partner_temp;"""])
write_obj.writerow(["""drop table contact_map_temp;"""])
write_obj.writerow(["""drop table pgm_att_temp;"""])

write_obj.writerow(["""create table res_partner_temp as select *  from res_partner limit 0;"""])
write_obj.writerow(["""create table contact_map_temp as select *  from contact_map limit 0;"""])
write_obj.writerow(["""create table pgm_att_temp as select *  from program_attendance limit 0;"""])


write_obj.writerow(["""\\COPY res_partner("""+','.join(res_partner_fields)+""") FROM './create_res_partner.csv' DELIMITER ',' CSV HEADER;"""])
write_obj.writerow(["""\\COPY res_partner_temp("""+','.join(['id']+res_partner_fields)+""") FROM './update_res_partner.csv' DELIMITER ',' CSV HEADER;"""])

write_obj.writerow(["""insert into res_partner("""+','.join(['id']+res_partner_fields)+""") select """+','.join(['id']+res_partner_fields)+
                    """ from res_partner_temp where id not in (select id from res_partner_temp group by id having count(*)>1) on conflict(id) do update set """+
                    res_partner_fields_update+""";"""
                    ])
write_obj.writerow(["""
do
$$
declare
    f record;
begin
    for f in select id from res_partner_temp  group by id having count(*)>1
    loop 
	insert into res_partner("""+','.join(['id']+res_partner_fields)+""")
	select """+','.join(['id']+res_partner_fields)+""" from res_partner_temp 
	where id = f.id limit 1 on conflict(id) do update set """
    +res_partner_fields_update+""";    
    end loop;
end;
$$;
    """])


write_obj.writerow(["""\\COPY contact_map("""+','.join(contact_map_fields)+""") FROM './create_contact_map.csv' DELIMITER ',' CSV HEADER;"""])
write_obj.writerow(["""\\COPY contact_map_temp("""+','.join(['id']+contact_map_fields)+""") FROM './update_contact_map.csv' DELIMITER ',' CSV HEADER;"""])
write_obj.writerow(["""insert into contact_map("""+','.join(['id']+contact_map_fields)+""") select """+','.join(['id']+contact_map_fields)+
                    """ from contact_map_temp where id not in (select id from contact_map_temp group by id having count(*)>1) on conflict(id) do update set """+
                    contact_map_fields_update+""";"""
                    ])
write_obj.writerow(["""
do
$$
declare
    f record;
begin
    for f in select id from contact_map_temp  group by id having count(*)>1
    loop 
	insert into contact_map("""+','.join(['id']+contact_map_fields)+""")
	select """+','.join(['id']+contact_map_fields)+""" from contact_map_temp 
	where id = f.id limit 1 on conflict(id) do update set """
    +contact_map_fields_update+""";    
    end loop;
end;
$$;
    """])



write_obj.writerow(["""\\COPY program_attendance("""+','.join(pgm_att_fields)+""") FROM './create_pgm_att.csv' DELIMITER ',' CSV HEADER;"""])
write_obj.writerow(["""\\COPY pgm_att_temp("""+','.join(['id']+pgm_att_fields)+""") FROM './update_pgm_att.csv' DELIMITER ',' CSV HEADER;"""])
write_obj.writerow(["""insert into program_attendance("""+','.join(['id']+pgm_att_fields)+""") select """+','.join(['id']+pgm_att_fields)+
                    """ from pgm_att_temp where id not in (select id from pgm_att_temp group by id having count(*)>1) on conflict(id) do update set """+
                    pgm_att_fields_update+""";"""])
write_obj.writerow(["""
do
$$
declare
    f record;
begin
    for f in select id from pgm_att_temp  group by id having count(*)>1
    loop 
	insert into program_attendance("""+','.join(['id']+pgm_att_fields)+""")
	select """+','.join(['id']+pgm_att_fields)+""" from pgm_att_temp 
	where id = f.id limit 1 on conflict(id) do update set """
    +pgm_att_fields_update+""";
    end loop;
end;
$$;
    """])


write_obj.writerow(["""\\COPY low_match_pairs(id1,id2,guid1,guid2,active,match_level) from './create_low_match_pairs.csv' DELIMITER ','  CSV HEADER;"""])
write_obj.writerow(["""
update contact_map cm set contact_id_fkey = rp.id from res_partner rp where cm.guid=rp.guid and cm.contact_id_fkey=2;
update program_attendance pgm set contact_id_fkey = cm.contact_id_fkey from contact_map cm where pgm.local_trans_id=cm.local_contact_id and cm.system_id=13 and pgm.contact_id_fkey=2;
---insert into isha_program_tag_res_partner_rel (pgm_tag_ids,partner_id) select (select id from isha_program_tag where name = 'IEO-R') as tag_id,rp.id from res_partner rp, program_attendance pgm where rp.id=pgm.contact_id_fkey and rp.ieo_date is null on conflict do nothing;
update low_match_pairs set id1 = rp.id from res_partner rp where rp.guid=guid1;
update low_match_pairs set id2 = rp.id from res_partner rp where rp.guid=guid2; 
"""])
