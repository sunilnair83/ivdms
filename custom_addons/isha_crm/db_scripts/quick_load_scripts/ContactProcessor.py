from datetime import date
from datetime import datetime

import phonenumbers

null = None

def getAdditionalFields():
    return ['dna_stayarea_comments', 'dna_program_comments', 'dna_volunteering_comments', 'dna_general_comments',
            'starmark_category_id', 'partner_severity', 'incident_date_closed', 'incident_date_assigned',
            'donotallow_stayarea_ids', 'donotallow_program_ids', 'donotallow_volunteering_ids',
            'donotallow_general_ids', 'approx_dob']


def getDateParsed(str_date):
    try:
        date_parsed = datetime.strptime(str_date, '%Y-%m-%dT%H:%M:%SZ') if str_date else None
        date_parsed = str(date_parsed) if date_parsed else None
    except:
        date_parsed = None
    return date_parsed

def getgender(gender):
    if type(gender) == str and len(gender)>0:
        gender = gender.lower()
        if gender == 'male' or gender == 'm' or gender == 'Мужской':
            return 'M'
        elif gender == 'female' or gender == 'f' or gender == 'Женский':
            return 'F'
        elif gender == 'other' or gender == 'o':
            return 'O'

    return None


def getMaritalStatus(marital_status):
    if marital_status and len(marital_status)>0:
        marital_status = marital_status.lower()
        ms = None
        if (marital_status and len(marital_status) > 0) and (
                marital_status == 'single' or marital_status == 'divorced' or marital_status == 'unmarried' or marital_status == 'widow'
                or marital_status == 'partner' or marital_status == 'widower'
                or marital_status == 'widowed' or marital_status == 'seperated'
                or marital_status == 'separated'):
            ms = "single"
        elif (marital_status and len(marital_status) > 0) and (marital_status == 'married'):
            ms = 'married'
        return ms

def getTokenizedVersion(string, delimiter):
    tokenized = {}
    if string and len(string)>0:
        i=1
        for x in string.split(delimiter):
            tokenized[str(i)] = x.lower().strip()
            i += 1

    return tokenized

def phoneStrip(phone):
    return phone[-10:] if phone else None

def replaceEmptyString(src_msg):
    for x in src_msg:
        if src_msg[x] == '':
            src_msg[x] = None
        elif type(src_msg[x]) == str:
            src_msg[x] = src_msg[x].replace('\x00','') # Handle Byte String
            src_msg[x] = src_msg[x].strip()
    return src_msg

def getTitlesRemoved(name):
    titles = ['Dr','DR','Prof', 'PROF','Advocate', 'ADVOCATE','Adv', 'Mr', 'MR','Mrs','MRS','MS','Ms','CA','smt','Smt','SMT']
    name = name.replace('. ',' ')
    name = name.replace(' .', ' ')
    name = name.replace('.', ' ')
    title=''
    for x in titles:
        if name.startswith(x+' '):
            name = name.replace(x+' ', '').strip()
            title += x if len(title) == 0 else ' '+x
        if name.endswith(' '+x):
            name = name.replace(' '+x, '').strip()
            title += x if len(title) == 0 else ' '+x
        if name.startswith(x.lower()+' '):
            name = name.replace(x.lower()+' ', '').strip()
            title += x if len(title) == 0 else ' '+x.lower()
        if name.endswith(' '+x.lower()):
            name = name.replace(' '+x.lower(), '').strip()
            title += x if len(title) == 0 else ' '+x.lower()
    return {'name':name, 'title':title}

def validatePhones(phone):
    invalid_phones = ['1111111111','1234567890','0123456789','none','15101127157','-','None','9999999999','18641964166',
                      '0','91','1111111111','Nil','null','Na','na','NA','123','1234567','0000000000','(000) 000-0000',
                      '123123123','No','91211','00','000','0000','000000','0000000','00000000','000000000',
                      '00000000000','0000000001','0000000012','0000000033','0000111111','0000222222','+91',
                      '9876543210','987654321', '123456789', '9000000000', '8888888888', '1234567891', '7777777777',
                      '12345678', '9123456789', '123456', '13333333333']
    return phone if type(phone) == str and phone.isdigit() and phone not in invalid_phones else None

def validateValues(odoo_rec, iso2, correction):

    name_dict = getTitlesRemoved(odoo_rec['name'])
    odoo_rec['name'] = name_dict['name']
    odoo_rec['prof_dr'] = name_dict['title'] if odoo_rec['prof_dr'] is None else odoo_rec['prof_dr']

    country_keys = ['country','country2','work_country','nationality']
    for x in country_keys:
        if odoo_rec[x]:
            country = odoo_rec[x].upper()
            if country in iso2:
                odoo_rec[x] = iso2[country]
            elif country in correction and correction[country] in iso2:
                odoo_rec[x] = iso2[correction[country]]

    phone_keys = ['phone','phone2','phone3','phone4']
    for x in phone_keys:
        odoo_rec[x+'_src'] = odoo_rec[x]
        try:
            phn_number = phonenumbers.parse(number=odoo_rec[x],region=odoo_rec['country'],keep_raw_input=True)
            odoo_rec[x] = str(phn_number.national_number)
            odoo_rec[x+'_country_code'] = str(phn_number.country_code)
            odoo_rec[x+'_is_valid'] = phonenumbers.is_valid_number(phn_number)
            odoo_rec[x+'_is_verified'] = False
        except:
            odoo_rec[x] = phoneStrip(odoo_rec[x])
            odoo_rec[x+'_country_code'] = odoo_rec[x+'_src'][0:-10] if odoo_rec[x] else None
            odoo_rec[x+'_is_valid'] = False
            odoo_rec[x+'_is_verified'] = False
        odoo_rec[x] = validatePhones(odoo_rec[x])

    return odoo_rec

class crmContact:
    system_id = 0
    def getOdooFormat(self, src_msg, iso2, correction):
        src_msg = replaceEmptyString(src_msg)


        odooRec = {
            'local_contact_id': 'UI',
            'active': True,
            'guid':None,
            'name': '', 'display_name': None,'prof_dr': None,
            'phone': None, 'phone2': None,
            'phone3': None,'phone4': None, 'whatsapp_number':None, 'whatsapp_country_code':None,
            'email': None, 'email2': None, 'email3': None,
            'street':  None, 'street2':None,'city': None, 'state': None, 'country': None, 'zip':  None,
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': None, 'occupation': None, 'marital_status': None, 'dob': None,
            'nationality': None, 'deceased': None, 'contact_type': None, 'companies': None,
            'aadhaar_no': None, 'pan_no': None, 'passport_no': None,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': None
            }
        if 'system_id' in src_msg and src_msg['system_id'] == 20 : # Yantra data
            odooRec['low_auth_data'] = True
        elif 'system_id' not in src_msg:
            odooRec['system_id'] = self.system_id

        for x in src_msg:
            odooRec[x] = src_msg[x]

        odooRec['email'] = odooRec['email'].lower().strip() if type(odooRec['email']) == str else None
        odooRec['email2'] = odooRec['email2'].lower().strip() if type(odooRec['email2']) == str else None
        odooRec['email3'] = odooRec['email3'].lower().strip() if type(odooRec['email3']) == str else None
        return validateValues(odooRec, iso2, correction)


class SSOContacts:
    system_id = 31

    def getOdooFormat(self,msg,iso2,correction):
        sso_id = msg['profileId']
        basic_profile = replaceEmptyString(msg['basicProfile'])
        phone_dict = replaceEmptyString(msg['basicProfile']['phone'])
        extended_profile = replaceEmptyString(msg['extendedProfile'])

        name = basic_profile['firstName']
        if basic_profile['lastName'] and len(basic_profile['lastName']) > 0:
            name = name + " " + basic_profile['lastName']
        phone = None
        if phone_dict['number']:
            phone = phone_dict['countryCode'] if phone_dict['countryCode'] else ''
            phone += phone_dict['number']
        email = basic_profile['email']
        gender = getgender(basic_profile['gender'])
        dob = basic_profile['dob'] if basic_profile['dob'] else None
        nationality = extended_profile['nationality']
        passport = extended_profile['passportNum'] if extended_profile['passportNum'] else None
        pan = extended_profile['pan'] if extended_profile['pan'] else None
        street = None
        street2 = None
        state = None
        city = None
        country = None
        zip = None
        if len(msg['addresses']) >0:
            address_dict = replaceEmptyString(msg['addresses'][0])
            street = address_dict['addressLine1']
            street2 = address_dict['addressLine2']
            if address_dict['townVillageDistrict']:
                if street2 and type(street2) == str:
                    street2 = street2 + ', ' + address_dict['townVillageDistrict']
                else:
                    street2 = address_dict['townVillageDistrict']
            city = address_dict['city']
            state = address_dict['state']
            country = address_dict['country']
            zip = address_dict['pincode']
        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': sso_id,
            'active': True,
            'name': name, 'display_name': name, 'prof_dr': None,
            'phone': phone, 'phone2': None,
            'phone3': None,'phone4': None,
            'whatsapp_number': None, 'whatsapp_country_code': None,
            'email': email, 'email2': None,'email3': None,
            'street': street,'street2': street2,'city': city, 'state': state, 'country': country,
            'zip': zip,
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': gender, 'occupation': None, 'marital_status': None, 'dob': dob,
            'nationality': nationality, 'deceased': None, 'contact_type': None, 'companies': None,
            'aadhaar_no': None, 'pan_no': pan, 'passport_no': passport,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': sso_id

        }

        return validateValues(odooRec, iso2, correction)

class SVROContacts:
    system_id = 17

    def extractPhone(self, phone):
        ext_phone = {}
        if phone and "number" in phone:
            phone_array = phone.split(';')
            j=1
            for i in range(len(phone_array)):
                if "number" in phone_array[i]:
                    i = i + 1
                    if 'N' not in phone_array[i]:
                        extracted_phone = phone_array[i].split(":")[2]
                        ext_phone[str(j)] = extracted_phone[1:len(extracted_phone)-1:1]
                        j+=1
        return ext_phone

    def getOdooFormat(self, msg, iso2, correction):

        msg = replaceEmptyString(msg)
        name = msg['first_name']
        if msg['last_name'] and len(msg['last_name']) > 0:
            name = name + " " + msg['last_name']

        marital_status = getMaritalStatus(msg['marital_status'])

        email = getTokenizedVersion(msg['personal_email'],',')

        phone = self.extractPhone(msg['phone'])

        odooRec = {
            'system_id': self.system_id,
            'local_contact_id':msg['id'],
            'active': True,
            'guid':msg['cdiid'],
            'name': name, 'display_name': name,'prof_dr': None,
            'phone': phone['1'] if '1' in phone else None, 'phone2': phone['2'] if '2' in phone else None,
            'phone3': phone['3'] if '3' in phone else None, 'phone4': phone['4'] if '4' in phone else None,
            'whatsapp_number': None, 'whatsapp_country_code': None,
            'email': email['1'] if '1' in email else None, 'email2': email['2'] if '2' in email else None, 'email3': email['3'] if '3' in email else None,
            'street':  msg['address'], 'street2':None,'city': msg['city'], 'state': msg['state'], 'country': msg['country_of_residence'], 'zip':  msg['postal_code'],
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': getgender(msg['gender']), 'occupation': None, 'marital_status': marital_status, 'dob': msg['date_of_birth'],
            'nationality': None, 'deceased': None, 'contact_type': 'INDIVIDUAL', 'companies': None,
            'aadhaar_no': None, 'pan_no': None, 'passport_no': None,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': None
            }

        return validateValues(odooRec, iso2, correction)


class FRCommsContact:
    system_id = 9

    def getOdooFormat(self, msg, iso2, correction):
        typecast = lambda x: x.lower().strip() if type(x) == str else None
        msg = replaceEmptyString(msg)
        phone = {'1': msg['contact_no']}
        email = {'1':typecast(msg['email']),'2':typecast(msg['second_email'])}
        occupation = ""
        if msg['nature_of_business'] and len(msg['nature_of_business']) > 0:
            occupation = msg['nature_of_business']
        if msg['designation'] and len(msg['designation']) > 0:
            if occupation:
                occupation += "; " + msg['designation']
            else:
                occupation = msg['designation']
        try:
            dob = datetime.strptime(msg['date_of_birth'], '%d-%b-%y') if msg['date_of_birth'] else None
            dob = dob.strftime("%Y-%m-%d") if dob else None
        except:
            dob = None

        odooRec = {
            'system_id': self.system_id,
            'local_contact_id':msg['local_id'],
            'active': True,
            'name': msg['name'], 'display_name': msg['name'],'prof_dr': msg['title'],
            'phone': phone['1'] if '1' in phone else None, 'phone2': phone['2'] if '2' in phone else None,
            'phone3': phone['3'] if '3' in phone else None, 'phone4': phone['4'] if '4' in phone else None,
            'whatsapp_number': None, 'whatsapp_country_code': None,
            'email': email['1'] if '1' in email else None, 'email2': email['2'] if '2' in email else None,'email3': email['3'] if '3' in email else None,
            'street':  msg['address'],  'street2':None,'city': msg['city'], 'state': msg['state'], 'country': msg['country'], 'zip':  msg['pincode'],
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': getgender(msg['gender']), 'occupation':occupation, 'marital_status': None, 'dob': dob,
            'nationality': None, 'deceased': None, 'contact_type': 'INDIVIDUAL', 'companies': msg['company_name'],
            'aadhaar_no': None, 'pan_no': None, 'passport_no': None,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': None
            }

        return validateValues(odooRec, iso2, correction)


class DMSContacts:
    system_id = 7

    def getOdooFormat(self, src_msg, iso2, correction):
        src_msg = replaceEmptyString(src_msg)
        title = "DR" if src_msg['title'] and (
                    src_msg['title'].upper() == 'DR' or src_msg['title'].upper() == 'DR.') else None
        name = src_msg['first_name'] if 'first_name' in src_msg else None

        phone = getTokenizedVersion(src_msg['phones'],';') if 'phones' in src_msg and src_msg['phones'] else {}

        email = getTokenizedVersion(src_msg['emails'],';') if 'emails' in src_msg and src_msg['emails'] else {}
        dob = src_msg['dob'] if src_msg['dob'] is not None and src_msg['dob'] != '' else None

        company_ids = src_msg['company_ids']
        companies = {}
        if company_ids:
            company_arr = company_ids.split(';')
            i = 1
            for x in company_arr:
                key = 'company' + str(i)
                i += 1
                companies[key] = x.strip()

        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': src_msg['local_contact_id'],
            'active': True if src_msg['active'] == 1 else False,
            'name': name, 'display_name': name,'prof_dr': title,
            'phone': phone['1'] if '1' in phone else None, 'phone2': phone['2'] if '2' in phone else None,
            'phone3': phone['3'] if '3' in phone else None, 'phone4': phone['4'] if '4' in phone else None,
            'whatsapp_number': None, 'whatsapp_country_code': None,
            'email': email['1'] if '1' in email else None, 'email2': email['2'] if '2' in email else None,'email3': email['3'] if '3' in email else None,
            'street': src_msg['address1'],  'street2':None,'city': src_msg['town_city1'], 'state': src_msg['state_province_region1'],
            'country': src_msg['country1'], 'zip': src_msg['postal_code1'],
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': getgender(src_msg['gender']), 'occupation': src_msg['occupation'], 'marital_status': None,
            'dob': dob,
            'nationality': src_msg['nationality'], 'deceased': True if src_msg['deceased'] == 1 else False,
            'contact_type': src_msg['contact_type'], 'companies': str(companies),
            'aadhaar_no': None, 'pan_no': src_msg['pan_number'], 'passport_no': None,
            'dnd_phone': src_msg['do_not_call'], 'dnd_email': src_msg['do_not_email'],
            'dnd_postmail': src_msg['do_not_postmail'], 'sso_id': None
        }
        if 'modified_at' in src_msg and src_msg['modified_at']:
            odooRec['local_modified_date'] = getDateParsed(src_msg['modified_at'])
        return validateValues(odooRec, iso2, correction)


class HYSContacts:
    system_id = 16

    def getOdooFormat(self, src_msg, iso2, correction):
        src_msg = replaceEmptyString(src_msg)
        name = src_msg['first_name']
        name += " "+src_msg['last_name'] if src_msg['last_name'] else ""

        email = getTokenizedVersion(src_msg['emails'],',') if src_msg['email'] else {}

        street = src_msg['address_line_1']
        street += " "+src_msg['address_line_2'] if src_msg['address_line_1'] else ''
        dob = src_msg['date_of_birth'] if src_msg['date_of_birth'] is not None else None
        typecast = lambda x: x.lower().strip() if type(x) == str else None

        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': src_msg['local_contact_id'],
            'active': True,
            'name': name, 'prof_dr': None, 'display_name':name,
            'phone': src_msg['mobile_phone'], 'phone2': src_msg['home_phone'],
            'phone3': None, 'phone4': None, 'whatsapp_number':None, 'whatsapp_country_code':None,
            'email': typecast(src_msg['primary_email']), 'email2': email['1'] if '1' in email else None, 'email3': email['2'] if '2' in email else None,
            'street': street,  'street2':None,'city': src_msg['town_city'], 'state': src_msg['state_province_region'], 'country': src_msg['country'], 'zip': src_msg['postal_code'],
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': getgender(src_msg['gender']), 'occupation': None, 'marital_status': getMaritalStatus(src_msg['marital_status']), 'dob': dob,
            'nationality': None, 'deceased': None, 'contact_type': 'INDIVIDUAL', 'companies': None,
            'aadhaar_no': None, 'pan_no': None, 'passport_no': None,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': None

        }
        if 'date_modified' in src_msg and src_msg['date_modified']:
            odooRec['local_modified_date'] = getDateParsed(src_msg['date_modified'])

        return validateValues(odooRec, iso2, correction)


class PRSContacts:
    system_id = 13

    def removePlus(self, code):
        if code and type(code) == str and len(code)>1 and code[0]=='+':
            return code[1:]
        else:
            return code

    def getDateParsed(self, str_date):
        try:
            date_parsed = datetime.strptime(str_date, '%Y-%m-%d %H:%M:%S') if str_date else None
            date_parsed = str(date_parsed) if date_parsed else None
        except:
            date_parsed = None
        return date_parsed

    def getOdooFormat(self, src_msg, iso2, correction):
        src_msg = replaceEmptyString(src_msg)
        name = src_msg['name']
        email = getTokenizedVersion(src_msg['email'], ',')
        occuption = src_msg['occupation'] if src_msg['occupation'] else None
        organization = src_msg['organization'] if src_msg['organization'] else None
        try:
            dob = datetime.strptime(src_msg['dob'], '%d-%m-%Y') if src_msg['dob'] else None
            dob = dob.strftime("%Y-%m-%d") if dob else None
        except:
            dob = None
        whatsapp_number = src_msg['whatsapp_number']
        whatsapp_country_code = self.removePlus(src_msg['whatsapp_country_code'])
        phone = src_msg['phone_country_code'] if src_msg['phone_country_code'] else ''
        phone += src_msg['mobile'] if src_msg['mobile'] else ''
        if phone == '':
            phone = None

        phone3 = None
        if src_msg['register_mobile']:
            phone2 = src_msg['register_mobile']
            phone3 = src_msg['alt_phone'] if 'alt_phone' in src_msg else None
        else:
            phone2 = src_msg['alt_phone'] if 'alt_phone' in src_msg else None

        id_card_type = src_msg['idcard_type'].upper() if src_msg['idcard_type'] else None
        id_card_no = src_msg['idcard_no'] if src_msg['idcard_no'] else None
        sso_id = src_msg['sso_id'] if 'sso_id' in src_msg and src_msg['sso_id'] else None
        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': src_msg['_id'],
            'active': True,
            'name': name, 'display_name': name,'prof_dr': None,
            'phone': phone, 'phone2': phone2,'phone3': phone3, 'phone4': None, 'whatsapp_number':whatsapp_number, 'whatsapp_country_code':whatsapp_country_code,
            'email': email['1'] if '1' in email else None, 'email2': email['2'] if '2' in email else None,'email3': email['3'] if '3' in email else None,
            'street': src_msg['address'],  'street2':None,'city': src_msg['city'], 'state': src_msg['state'], 'country': src_msg['country'], 'zip': src_msg['pincode'],
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': getgender(src_msg['gender']), 'occupation': occuption, 'marital_status': getMaritalStatus(src_msg['marital_status']), 'dob': dob,
            'aadhaar_no': src_msg['adhar_no'], 'pan_no': None, 'passport_no': src_msg['passport_number'],
            'nationality': src_msg['nationality'], 'deceased': None, 'contact_type': None, 'companies': organization,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': sso_id

        }
        if not odooRec['email']:
            odooRec['email'] = src_msg['alt_email'] if 'alt_email' in src_msg else None
        elif not odooRec['email2']:
            odooRec['email2'] = src_msg['alt_email'] if 'alt_email' in src_msg else None
        else:
            odooRec['email3'] = src_msg['alt_email'] if 'alt_email' in src_msg else None
        if(id_card_type == 'AADHAAR'):
            odooRec['id_proofs'] = str({'AADHAAR':id_card_no})
        elif(id_card_type == 'PANCARD'):
            odooRec['id_proofs'] = str({'PANCARD':id_card_no})
        elif(id_card_type == 'PASSPORT'):
            odooRec['id_proofs'] = str({'PASSPORT':id_card_no})

        local_modified_at = self.getDateParsed(src_msg['modified_date'])
        if local_modified_at:
            odooRec['local_modified_date'] = local_modified_at
        else:
            odooRec['local_modified_date'] = self.getDateParsed(src_msg['created_date'])

        if 'ishangam_id' in src_msg:
            odooRec['contact_id_fkey'] = src_msg['ishangam_id']

        return validateValues(odooRec, iso2, correction)


class ALContact:
    system_id = 6

    def getOdooFormat(self, src_msg, iso2, correction):
        src_msg = replaceEmptyString(src_msg)
        name = src_msg['Pam_Participant_Name'].strip() if src_msg['Pam_Participant_Name'] else ''
        name += ' '+src_msg['Pam_Initial'].strip() if src_msg['Pam_Initial'] != None else ''

        street = src_msg['Pam_Address_Line1'].strip() if src_msg['Pam_Address_Line1'] else ''
        street += src_msg['Pam_Address_Line2'].strip() if src_msg['Pam_Address_Line2'] else ''
        street2 = src_msg['Pam_Address_Line3'].strip() if src_msg['Pam_Address_Line3'] else None

        work_street = src_msg['Pam_Work_Address_Line1'].strip() if src_msg['Pam_Work_Address_Line1'] else ''
        work_street2 = src_msg['Pam_Work_Address_Line2'].strip() if src_msg['Pam_Work_Address_Line2'] else ''
        work_street2 += src_msg['Pam_Work_Address_Line3'].strip() if src_msg['Pam_Work_Address_Line3'] else ''
        work_city = src_msg['Pam_Work_City'] if src_msg['Pam_Work_City'] else None
        work_street = work_street if work_street else None
        work_street2 = work_street2 if work_street2 else None
        work_state = src_msg['Pam_Work_State'] if src_msg['Pam_Work_State'] else None
        work_zip = src_msg['Pam_Work_PostalCode'] if src_msg['Pam_Work_PostalCode'] else None
        work_country = src_msg['Pam_Work_Country'] if src_msg['Pam_Work_Country'] else None
        try:
            dob = datetime.strptime(src_msg['Pam_Date_Of_Birth'], '%d-%m-%Y') if src_msg['Pam_Date_Of_Birth'] else None
            dob = dob.strftime("%Y-%m-%d") if dob else None
        except:
            dob = None
        email = src_msg['Pam_Email_ID'].lower().strip() if src_msg['Pam_Email_ID'] else None
        email2 = src_msg['Pam_Oemail_Id'].lower().strip() if src_msg['Pam_Oemail_Id'] else None
        #dob = src_msg['Pam_Date_Of_Birth'] if src_msg['Pam_Date_Of_Birth'] and src_msg['Pam_Date_Of_Birth'] != ' ' and src_msg['Pam_Date_Of_Birth'] != ''  else None

        approx_dob = None
        if src_msg['Pam_Age'] and src_msg['Pam_Record_Created_Date']:
            age = int(src_msg['Pam_Age'])
            al_created_date = date.fromtimestamp(int(src_msg['Pam_Record_Created_Date']) / 1000)
            birth_year = al_created_date.year - age
            approx_dob = date(birth_year, al_created_date.month, al_created_date.day)

        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': src_msg['Pam_Serial_Number'],
            'active': True,
            'name': name, 'display_name': name,'prof_dr': None,
            'phone': src_msg['Pam_Mobile_Phone'], 'phone2': src_msg['Pam_Mobile_Phone1'],
            'phone3': src_msg['Pam_Home_Phone'], 'phone4': None, 'whatsapp_number':None, 'whatsapp_country_code':None,
            'email': email, 'email2': email2,'email3': None,
            'street': street,  'street2':street2,'city': src_msg['Pam_City'], 'state': src_msg['Pam_State'], 'country': src_msg['Pam_Country'], 'zip': src_msg['Pam_Postal_Code'],
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': work_street, 'work_street2':work_street2, 'work_city': work_city,'work_state': work_state,'work_country': work_country,'work_zip': work_zip,
            'gender': getgender(src_msg['Pam_Gender']), 'occupation': src_msg['Pam_Occupation'], 'marital_status': None, 'dob': dob,
            'nationality': None, 'deceased': None, 'contact_type': 'INDIVIDUAL', 'companies': None,
            'aadhaar_no': None, 'pan_no': None, 'passport_no': None,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': None,
            'approx_dob': approx_dob
        }
        if 'modified_at' in src_msg and src_msg['modified_at']:
            odooRec['local_modified_date'] = getDateParsed(src_msg['modified_at'])
        return validateValues(odooRec, iso2, correction)

class IMSContact:
    system_id = 11

    def getOdooFormat(self, src_msg, iso2, correction):
        src_msg = replaceEmptyString(src_msg)
        name = src_msg['name']['firstName']
        name +=  src_msg['name']['lastName'] if src_msg['name']['lastName'] else ''

        add1 = src_msg['addresses'][0] if len(src_msg['addresses'])>0 else None
        add2 = src_msg['addresses'][0] if len(src_msg['addresses'])>1 else None
        dob = src_msg['dob'] if src_msg['dob'] is not None else None

        phones=''
        for x in src_msg['phones']:
            phones+=';'+x['number']

        phone = getTokenizedVersion(phones,';')
        emails =''
        for x in src_msg['emails']:
            phones+=';'+x['address']
        email = getTokenizedVersion(emails,';')
        companies=''
        for x in src_msg['companies']:
            companies+=","+x['name']

        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': src_msg['localContactId'],
            'active': src_msg['active'],
            'name': name, 'display_name': name,'prof_dr': src_msg['title'],
            'phone': phone['1'] if '1' in phone else None, 'phone2': phone['2'] if '2' in phone else None,
            'phone3': phone['3'] if '3' in phone else None,'phone4': phone['4'] if '4' in phone else None,
            'whatsapp_number': None, 'whatsapp_country_code': None,
            'email': email['1'] if '1' in email else None, 'email2': email['2'] if '2' in email else None,'email3': email['3'] if '3' in email else None,
            'gender': getgender(src_msg['gender']), 'occupation': src_msg['occupation'], 'marital_status': src_msg['maritalStatus'], 'dob': dob,
            'nationality': src_msg['nationality'], 'deceased': src_msg['deceased'], 'contact_type': 'INDIVIDUAL', 'companies': companies,
            'dnd_phone': src_msg['doNotDisturb']['doNotCall'] or src_msg['doNotDisturb']['doNotSms'], 'dnd_email': src_msg['doNotDisturb']['doNotEmail'], 'dnd_postmail': src_msg['doNotDisturb']['doNotPostmail'], 'sso_id': None
        }
        if add1:
            odooRec.update({'street':add1['addressline1'],'street2':add1['addressline2'],'city':add1['townCity'],
                            'state':add1['stateProvinceRegion'],'zip':add1['postalCode'],'country':add1['country']})
        if add2:
            odooRec.update({'street2_1':add2['addressline1'],'street2_2':add2['addressline2'],'city2':add2['townCity'],
                            'state2':add2['stateProvinceRegion'],'zip2':add2['postalCode'],'country2':add2['country']})
        for x in src_msg['idProof']:
            if x['title'] == 'AADHAAR':
                odooRec['aadhaar_no'] = odooRec['number']
            elif x['title'] == 'PAN':
                odooRec['pan_no'] = odooRec['number']

        return validateValues(odooRec, iso2, correction)


class Sangam:
    system_id = 12

    def getOdooFormat(self, msg, iso2, correction):
        msg = replaceEmptyString(msg)
        phone={}
        email={}
        dob = None
        add1 = msg['addresses'][0] if len(msg['addresses'])>0 else None
        add2 = msg['addresses'][0] if len(msg['addresses'])>1 else None


        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': msg['con_id'],
            'active': True if msg['con_deleted'] == 0 else False,
            'name': msg['name'], 'display_name': msg['name'], 'prof_dr': None,
            'phone': phone['1'] if '1' in phone else None, 'phone2': phone['2'] if '2' in phone else None,
            'phone3': phone['3'] if '3' in phone else None,
            'whatsapp_number': None, 'whatsapp_country_code': None,
            'email': email['1'] if '1' in email else None, 'email2': email['2'] if '2' in email else None,'email3': email['3'] if '3' in email else None,
            'street': None,  'street2':None,'city': None, 'state': None, 'country': None, 'zip': None,
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': None, 'occupation': None, 'marital_status': None, 'dob': dob,
            'nationality': None, 'deceased': None, 'contact_type': None,
            'companies': None,
            'aadhaar_no': None, 'pan_no': None, 'passport_no': None,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': None
        }
        if add1:
            odooRec.update({'street':add1['address'],'street2':None,'city':add1['city'],
                            'state':add1['state'],'zip':add1['pincode'],'country':add1['country']})
        if add2:
            odooRec.update({'street2_1':add2['address'],'street2_2':None,'city2':add2['city'],
                            'state2':add2['state'],'zip2':add2['pincode'],'country2':add2['country']})

        return validateValues(odooRec, iso2, correction)
