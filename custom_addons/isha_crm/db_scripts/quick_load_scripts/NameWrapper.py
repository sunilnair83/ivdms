import csv

import phonenumbers

import ContactProcessor
import NameMatchEngine

null = None
NO_MATCH = 0
LOW_MATCH = 1
HIGH_MATCH = 2
EXACT_MATCH = 4
NA = -1


class OdooBase:
    file_name = None
    create_file = None
    write_file = None


    def __init__(self,filename,field_names):
        # Open file in append mode
        write_obj = open('create_'+filename, 'w', newline='')
        # Create a writer object from csv module
        self.create_file = csv.DictWriter(write_obj, fieldnames=field_names)
        self.create_file.writeheader()
        write_obj = open('update_'+filename, 'w', newline='')
        # Create a writer object from csv module
        field_names = ['id']+field_names
        self.write_file = csv.DictWriter(write_obj, fieldnames=field_names)
        self.write_file.writeheader()
    def create(self, val):
        # print(val)
        self.create_file.writerow(val)
        return 2

    def write(self, val):
        # print(val)
        self.write_file.writerow(val)


class ResPartner(OdooBase):
    def phase4Create(self, val):
        return super(ResPartner, self).create(val)



class GoldenContactAdv:
    db_connection = None
    db_cursor = None
    phone_to_ids={}
    email_to_ids={}
    sso_to_id={}
    id_to_name={}
    mapModel = None
    contactModel = None
    countryModel = None
    stateModel = None
    lowMatchModel = None
    pincodeModel = None
    ishaCenterModel = None
    low_match = []
    CR = None
    env = None
    iso2 = None
    correction = None

    def __init__(self, ENV):
        self.mapModel = OdooBase('name_match.csv',['flag','name','phone','email','starmark_flag','phase','low','relatives'])

        self.low_match = []
        self.rel_match = []
        self.CR = None #ENV.cr
        self.env = None #ENV
        with open('res_partner_nm.csv', newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                if row['email'] and row['email'] in self.email_to_ids:
                    self.email_to_ids[row['email']].add(row['id'])
                elif row['email']:
                    self.email_to_ids[row['email']] = set()
                    self.email_to_ids[row['email']].add(row['id'])
                if row['email2'] and row['email2'] in self.email_to_ids:
                    self.email_to_ids[row['email2']].add(row['id'])
                elif row['email2']:
                    self.email_to_ids[row['email2']] = set()
                    self.email_to_ids[row['email2']].add(row['id'])
                if row['email3'] and row['email3'] in self.email_to_ids:
                    self.email_to_ids[row['email3']].add(row['id'])
                elif row['email3']:
                    self.email_to_ids[row['email3']] = set()
                    self.email_to_ids[row['email3']].add(row['id'])
                if row['phone'] and row['phone'] in self.phone_to_ids:
                    self.phone_to_ids[row['phone']].add(row['id'])
                elif row['phone']:
                    self.phone_to_ids[row['phone']] = set()
                    self.phone_to_ids[row['phone']].add(row['id'])
                if row['phone2'] and row['phone2'] in self.phone_to_ids:
                    self.phone_to_ids[row['phone2']].add(row['id'])
                elif row['phone2']:
                    self.phone_to_ids[row['phone2']] = set()
                    self.phone_to_ids[row['phone2']].add(row['id'])
                if row['phone3'] and row['phone3'] in self.phone_to_ids:
                    self.phone_to_ids[row['phone3']].add(row['id'])
                elif row['phone3']:
                    self.phone_to_ids[row['phone3']] = set()
                    self.phone_to_ids[row['phone3']].add(row['id'])
                if row['phone4'] and row['phone4'] in self.phone_to_ids:
                    self.phone_to_ids[row['phone4']].add(row['id'])
                elif row['phone4']:
                    self.phone_to_ids[row['phone4']] = set()
                    self.phone_to_ids[row['phone4']].add(row['id'])
                if row['sso_id']:
                    self.sso_to_id[row['sso_id']] = row['id']
                self.id_to_name[row['id']] = {'guid':row['guid'],'id':row['id'],'name':row['name'],'write_date':row['write_date'],'is_flagged':row['is_flagged'],'starmark_category_id':row['starmark_category_id']}



    def findMatches(self, ENV, src_msg):
        # self.setEnvValues(ENV)
        flag_dict = {'flag':False, 'name':src_msg['name'],'phone':src_msg['phone'],'email':src_msg['email'],'starmark_flag':False,'phase':{}}

        '''
            Phase 0 -> If the record already contains guid or local_contact_id exists in contact_map
                        then find the Matching record and update the values
                        else follow match and merge
        '''

        '''
            Phase 1 -> Find Records with Exact Phone match
                        1.1 ->  CDI's Name match to populate high_match_list and global_low_match_list
                    -> Merge the tail of high_match_list with src_msg else go to Phase 2.
        '''
        if not flag_dict['flag']:
            flag_dict = self.firstPhaseSearch(src_msg,flag_dict)

        '''
            Phase 2 -> Find Records with Exact email Match
                        2.1 ->  CDI's Name match to populate high_match_list and global_low_match_list
                    -> Merge the tail of high_match_list with src_msg if exists else go to Phase 3.
        '''

        flag_dict = self.secondPhaseSearch(src_msg,flag_dict)

        '''
            Phase 3 -> Find Records with Exact Name Match
                    3.1 ->  In those records find records upto 2 edit distance variation on phone.
                        ->  Add to high_match_list if distance is <= 1
                        ->  Add to global_low_match_list if distance = 2
                    3.2 ->  Find record with 1 edit distance on email and add to global_low_match_list
                    3.3 - > Find records with exact zip match
                        - > And 70 street match and to global_low_match_list
                        - > If 90 street match and phone, email both are None then add to high_match_list

                    -> Merge the tail of high_match_list with src_msg if exists else go to Phase 4
        '''
        '''
            Phase 4 -> Create new Record.
        '''
        flag_dict.update({
        	'low':self.low_match,
        	'relatives':self.rel_match
        })
        self.mapModel.create(flag_dict)

        self.low_match = []
        self.rel_match = []
        # self.CR.commit()
        return flag_dict


    def firstPhaseSearch(self, src_msg,flag_dict):

        # Phase 1
        # search for Exact Phone match
        rec_list = self.getPhoneMatch(src_msg['phone']) if src_msg['phone'] is not None and len(src_msg['phone']) > 9 else set()
        rec_list2 = set()#self.getPhoneMatch(src_msg['phone2']) if src_msg['phone2'] is not None and len(src_msg['phone2']) > 9  else set()

        rec_list |= rec_list2
        rec_list = [self.id_to_name[x] for x in rec_list]

        # if len(rec_list) == 0:
        #     rec_list = self.getEditDistancePhone(src_msg['phone'])

        if len(rec_list) > 0:
            # Phase 1.1
            # CDI's Name Match
            matchEngine = NameMatchEngine.NameMatchEngine()
            high_match = []
            for rec in rec_list:
                match_status = matchEngine.compareNamesWithVariations(src_msg['name'], rec['name'])
                if match_status == EXACT_MATCH or match_status == HIGH_MATCH:
                    high_match.append(rec)
                elif match_status == LOW_MATCH:
                    self.low_match.append(rec)
                else:
                    self.rel_match.append(rec)
                if rec['is_flagged']:
                    src_msg.update({'is_flagged': True})
                if len(high_match)>0:
                    flag_dict['flag'] = True
                    for x in high_match:
                        if x['starmark_category_id']:
                            flag_dict['starmark_flag'] = True

                flag_dict['phase']['phase1_high'] = high_match

        return flag_dict


    def secondPhaseSearch(self, src_msg,flag_dict):

        # Phase 2
        # search for Exact email match
        rec_list = self.getEmailMatch(src_msg['email']) if src_msg['email'] != null else set()
        rec_list2 = set()#self.getEmailMatch(src_msg['email2']) if src_msg['email2'] != null else set()

        rec_list = rec_list | rec_list2
        rec_list = [self.id_to_name[x] for x in rec_list]
        if len(rec_list) > 0:
            # Phase 2.1
            # CDI's Name Match
            matchEngine = NameMatchEngine.NameMatchEngine()
            high_match = []
            for rec in rec_list:
                match_status = matchEngine.compareNamesWithVariations(src_msg['name'],rec['name'])
                if match_status == EXACT_MATCH or match_status == HIGH_MATCH:
                    high_match.append(rec)
                elif match_status == LOW_MATCH:
                    self.low_match.append(rec)
                else:
                    self.rel_match.append(rec)
                if rec['is_flagged']:
                    src_msg.update({'is_flagged': True})
                if len(high_match)>0:
                    flag_dict['flag'] = True
                    for x in high_match:
                        if x['starmark_category_id']:
                            flag_dict['starmark_flag'] = True
                    
            flag_dict['phase']['phase2_high'] = high_match

        return flag_dict


    def closeCursorWhenException(self):
        if self.CR is not None:
            self.CR.close()

    def getPhoneMatch(self, phone):
        rec = self.phone_to_ids[phone] if phone in self.phone_to_ids else set()
        # rec = self.getMatchRecordList('phone', '=', phone)
        # if len(rec) == 0:  # secondary search
        #     rec = self.getMatchRecordList('phone2', '=', phone)
        # if len(rec) == 0:  # final search
        #     rec = self.getMatchRecordList('phone3','=',phone)
        # if len(rec) == 0:  # final search
        #     rec = self.getMatchRecordList('phone4','=',phone)

        return rec

    def getEditDistancePhone(self, phone):
        self.env.cr.execute("SELECT set_limit(0.57);")
        return self.getMatchRecordList('phone','%',phone)

    def getEmailMatch(self, email):
        rec = self.email_to_ids[email] if email in self.email_to_ids else set()
        # rec = self.getMatchRecordList('email', '=ilike', email)
        # if len(rec) == 0:  # secondary search
        #     rec = self.getMatchRecordList('email2', '=ilike', email)
        # if len(rec) == 0:  # secondary search
        #     rec = self.getMatchRecordList('email3', '=ilike', email)
        return rec


    def getNameMatch(self, name):
        rec = self.getMatchRecordList('name', '=ilike', name)
        return rec

    def getLongerString(self, str1, str2):
        if type(str1) == str and type(str2) == str:
            if len(str1.split()) == len(str2.split()):
                return str1 if len(str1) > len(str2) else str2
            else:
                return str1 if len(str1.split()) > len(str2.split()) else str2
        if type(str1) == str and type(str2) != str:
            return str1
        if type(str2) == str and type(str1) != str:
            return str2
        return None

    def is_two_way_substring(self, str1, str2):
       return str1.startswith(str2) or str2.startswith(str1)

    def getCountryCode(self, country):
        if country is not None:
            country_id = self.countryModel.get(country,None)
            if country_id:
                return country_id['id']
        return None

    def getStateCode(self, state):
        if state is not None:
            state_id = self.stateModel.get(state,None)
            if state_id:
                return state_id
        return None

    def get_first_name(self, name):
        cp = ContactProcessor
        name_dict = cp.getTitlesRemoved(name)
        name_token = name_dict['name'].split(' ')
        first_name = None
        for x in name_token:
            if len(x) >= 3:
                first_name = x
                break

        return first_name



def phoneStrip(phone):
    return phone[-10:] if phone else None
    
gc = GoldenContactAdv({})
i=0
with open('./input_file.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        phone_keys = ['phone']
        for x in phone_keys:
            row[x+'_src'] = row[x]
            try:
                phn_number = phonenumbers.parse(number=row[x],region=row['country'],keep_raw_input=True)
                row[x] = str(phn_number.national_number)
                row[x+'_country_code'] = str(phn_number.country_code)
            except:
                row[x] = phoneStrip(row[x])
                row[x+'_country_code'] = row[x+'_src'][0:-10] if row[x] else None
        # print(row['name'],i)
        i+=1
        gc.findMatches({},row)

