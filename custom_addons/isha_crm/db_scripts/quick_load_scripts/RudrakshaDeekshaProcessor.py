from datetime import datetime

import ContactProcessor


class RudrakshaDeekshaProcessor:
    system_id = 41

    def getOdooFormatContact(self, src_msg, iso2, correction):

        name = src_msg['first_name'] if src_msg['first_name'] else ''
        name += ' ' + src_msg['last_name'] if src_msg['last_name'] else ''

        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': src_msg['registration_id'],
            'active': True,
            'name': name, 'display_name': name,'prof_dr': None,
            'phone': src_msg['phone'], 'phone2': None,'phone3': None, 'phone4': None, 'whatsapp_number': src_msg['whatsapp_number'],
            'whatsapp_country_code': src_msg['whatsapp_country_code'],
            'email': src_msg['email'], 'email2': None,'email3': None,
            'street': src_msg['street'],  'street2':src_msg['street2'],
            'city': src_msg['city'], 'state': src_msg['state'], 'country': src_msg['country'], 'zip': src_msg['zip'],
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': None, 'occupation': None, 'marital_status': None, 'dob': None,
            'aadhaar_no': None, 'pan_no': None, 'passport_no': None,
            'nationality': None, 'deceased': None, 'contact_type': None, 'companies': None,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None,
            'sso_id': src_msg['sso_id']

        }
        return ContactProcessor.validateValues(odooRec, iso2, correction)

    def getOdooFormatTxn(self, src_msg):
        name = src_msg['first_name'] if src_msg['first_name'] else ''
        name += ' ' + src_msg['last_name'] if src_msg['last_name'] else ''
        try:
            reg_date = datetime.fromtimestamp(int(src_msg['registration_date'])/1000)
            reg_date = reg_date.strftime("%Y-%m-%d %H:%M:%S")
        except:
            reg_date =False

        if src_msg['courier_supported'] == '1':
            courier_supported = True
        else:
            courier_supported = False

        odoo_rec = {
            'name': name,
            'phone_country_code': src_msg['phone_countr_code'],
            'phone':src_msg['phone'],
            'email':src_msg['email'],
            'whatsapp_country_code':src_msg['whatsapp_country_code'],
            'whatsapp_number':src_msg['whatsapp_number'],
            'courier_supported':courier_supported,
            'street':src_msg['street'],
            'street2':src_msg['street2'],
            'landmark':src_msg['landmark'],
            'city':src_msg['city'],
            'state':src_msg['state'],
            'country':src_msg['country'],
            'zip':src_msg['zip'],
            'no_of_rudraksha':src_msg['total_items'],
            'local_txn_id':src_msg['registration_id'],
            'sso_id':src_msg['sso_id'],
            'registration_date':reg_date,
            'reg_status':'COMPLETED',
            'utm_campaign':src_msg.get('utm_campaign', None),
            'utm_source':src_msg.get('utm_source', None),
            'utm_medium':src_msg.get('utm_medium', None),
            'utm_content':src_msg.get('utm_content', None),
            'utm_term':src_msg.get('utm_term', None)
        }

        if not odoo_rec['whatsapp_number']:
            odoo_rec['whatsapp_country_code'] = None
        return odoo_rec

    def process_message(self, src_msg_dict, env, metadata):
        txn_dict = self.getOdooFormatTxn(src_msg_dict)
        rec = self.createOrUpdateTxn(env, txn_dict)
        return {'flag': True, 'rec': rec}

    def createOrUpdateTxn(self,env,txn_dict):
        txn_model = env['rudraksha.deeksha']
        txn_rec = txn_model.search([('local_txn_id', '=', txn_dict['local_txn_id'])])
        if txn_rec:
            txn_rec.write(txn_dict)
        else:
            txn_rec = txn_model.create(txn_dict)

        return txn_rec



