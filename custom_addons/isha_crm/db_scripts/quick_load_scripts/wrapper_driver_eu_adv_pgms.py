import csv

import psycopg2.extras

import EUAdvProg2ProgAttProcessor
import GoldenContactAdv

txn_table_name = 'program_attendance'

with open('country-nationality-iso2-map.csv') as f:
    iso2 = dict(filter(None, csv.reader(f)))
with open('eReceipts-country-nationality-corrections.csv') as f:
    csvReader = []
    for x in csv.reader(f):
        csvReader.append([x[0].upper(), x[1].upper()])
    correction = dict(filter(None, csvReader))
print('loaded csv')
print(iso2['UK'])

conn = psycopg2.connect(database = "odoo", user = "odoo", host = "localhost", port = "5432",password='odoo')
# conn = psycopg2.connect(database = "ishangam", user = "ishangam_role", host = "ishangam-app.isha.internal.in", port = "6432")
db_cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

db_cur.execute("""SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='res_partner';""")
res_partner_fields = []
for x in db_cur.fetchall():
    res_partner_fields.append(x['column_name'])
res_partner_fields.remove('id')
res_partner_fields.sort()

db_cur.execute("""SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='contact_map';""")
contact_map_fields = []
for x in db_cur.fetchall():
    contact_map_fields.append(x['column_name'])
contact_map_fields.remove('id')
contact_map_fields.sort()

db_cur.execute("""SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME ='%s';""" % txn_table_name)
txn_fields = []
for x in db_cur.fetchall():
    txn_fields.append(x['column_name'])
txn_fields.remove('id')
txn_fields.sort()

db_cur.close()
conn.close()

txn_writer = GoldenContactAdv.OdooBase('pgm_attendance.csv', txn_fields)
gc = GoldenContactAdv.GoldenContactAdv({'res_partner_fields':res_partner_fields,'contact_map_fields':contact_map_fields,
                                        'iso2':iso2,'correction':correction})

# gc.setEnvValues({})

processor = EUAdvProg2ProgAttProcessor.EUAdvProg2ProgAttProcessor()
index=0

input_data_file_name = 'test_pgm.csv'
with open(input_data_file_name, newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        processor.process_message(row, {'txn_updater_class':txn_writer, 'gc':gc, 'iso2':iso2, 'correction':correction},
                                  {'iso2':iso2,'correction':correction})
        index+=1
        print(index)

res_partner_fields_update = 'id=excluded.id,'+','.join([x+'=excluded.'+x for x in res_partner_fields])
contact_map_fields_update = 'id=excluded.id,'+','.join([x+'=excluded.'+x for x in contact_map_fields])
txn_fields_update = 'id=excluded.id,' + ','.join([x + '=excluded.' + x for x in txn_fields])


write_obj = open('sql.sql', 'w', newline='')
# Create a writer object from csv module
write_obj = csv.writer(write_obj)
write_obj.writerow(["""drop table res_partner_temp;"""])
write_obj.writerow(["""drop table contact_map_temp;"""])
write_obj.writerow(["""drop table program_attendance_temp;"""])

write_obj.writerow(["""create table res_partner_temp as select *  from res_partner limit 0;"""])
write_obj.writerow(["""create table contact_map_temp as select *  from contact_map limit 0;"""])
write_obj.writerow(["""create table program_attendance_temp as select *  from program_attendance limit 0;"""])


write_obj.writerow(["""\\COPY res_partner("""+','.join(res_partner_fields)+""") FROM './create_res_partner.csv' DELIMITER ',' CSV HEADER;"""])
write_obj.writerow(["""\\COPY res_partner_temp("""+','.join(['id']+res_partner_fields)+""") FROM './update_res_partner.csv' DELIMITER ',' CSV HEADER;"""])

write_obj.writerow(["""insert into res_partner("""+','.join(['id']+res_partner_fields)+""") select """+','.join(['id']+res_partner_fields)+
                    """ from res_partner_temp where id not in (select id from res_partner_temp group by id having count(*)>1) on conflict(id) do update set """+
                    res_partner_fields_update+""";"""
                    ])


write_obj.writerow(["""\\COPY contact_map("""+','.join(contact_map_fields)+""") FROM './create_contact_map.csv' DELIMITER ',' CSV HEADER;"""])
write_obj.writerow(["""\\COPY contact_map_temp("""+','.join(['id']+contact_map_fields)+""") FROM './update_contact_map.csv' DELIMITER ',' CSV HEADER;"""])
write_obj.writerow(["""insert into contact_map("""+','.join(['id']+contact_map_fields)+""") select """+','.join(['id']+contact_map_fields)+
                    """ from contact_map_temp where id not in (select id from contact_map_temp group by id having count(*)>1) on conflict(id) do update set """+
                    contact_map_fields_update+""";"""
                    ])

write_obj.writerow(["""\\COPY program_attendance(""" +','.join(txn_fields) + """) FROM './create_donation_yogini.csv' DELIMITER ',' CSV HEADER;"""])
write_obj.writerow(["""\\COPY program_attendance_temp(""" +','.join(['id'] + txn_fields) + """) FROM './update_donation_yogini.csv' DELIMITER ',' CSV HEADER;"""])
write_obj.writerow(["""insert into program_attendance(""" +','.join(['id'] + txn_fields) + """) select """ + ','.join(['id'] + txn_fields) +
                    """ from program_attendance_temp where id not in (select id from program_attendance_temp group by id having count(*)>1) on conflict(id) do update set """ +
                    txn_fields_update + """;"""])

write_obj.writerow(["""\\COPY low_match_pairs(id1,id2,guid1,guid2,active,match_level) from './create_low_match_pairs.csv' DELIMITER ','  CSV HEADER;"""])
write_obj.writerow(["""
update contact_map cm set contact_id_fkey = rp.id from res_partner rp where cm.guid=rp.guid and cm.contact_id_fkey=2;
update program_attendance pgm set contact_id_fkey = cm.contact_id_fkey from contact_map cm where pgm.local_trans_id=cm.local_contact_id and cm.system_id=31 and pgm.contact_id_fkey=2;
update low_match_pairs set id1 = rp.id from res_partner rp where rp.guid=guid1;
update low_match_pairs set id2 = rp.id from res_partner rp where rp.guid=guid2; 
"""])
