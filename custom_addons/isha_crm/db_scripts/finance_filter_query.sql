--------------Please Execute the following sql in pgadmin if you create a new db...---------------------

----------- Trigram index creation----------
CREATE EXTENSION pg_trgm;

------------Donation view---------------
CREATE OR REPLACE VIEW public.donations_view
 AS
 SELECT donation_isha_foundation.id,
    donation_isha_foundation.local_trans_id,
    donation_isha_foundation.local_donor_id,
    donation_isha_foundation.donor_poc_id,
    donation_isha_foundation.local_patron_id,
    donation_isha_foundation.status,
    donation_isha_foundation.patron_poc_id,
    donation_isha_foundation.donation_type,
    donation_isha_foundation.project,
    donation_isha_foundation.mode_of_payment,
    donation_isha_foundation.inkind,
    donation_isha_foundation.amount,
    donation_isha_foundation.receipt_number,
    donation_isha_foundation.don_receipt_date,
    donation_isha_foundation.active,
    donation_isha_foundation.system_id,
    donation_isha_foundation.contact_id_fkey,
    donation_isha_foundation.purpose_id,
    donation_isha_foundation.create_date,
    'IshaFoundation'::text AS entity_name
   FROM donation_isha_foundation
UNION
 SELECT donation_isha_education.id,
    donation_isha_education.local_trans_id,
    donation_isha_education.local_donor_id,
    donation_isha_education.donor_poc_id,
    donation_isha_education.local_patron_id,
    donation_isha_education.status,
    donation_isha_education.patron_poc_id,
    donation_isha_education.donation_type,
    donation_isha_education.project,
    donation_isha_education.mode_of_payment,
    donation_isha_education.inkind,
    donation_isha_education.amount,
    donation_isha_education.receipt_number,
    donation_isha_education.don_receipt_date,
    donation_isha_education.active,
    donation_isha_education.system_id,
    donation_isha_education.contact_id_fkey,
    donation_isha_education.purpose_id,
    donation_isha_education.create_date,
    'IshaEducation'::text AS entity_name
   FROM donation_isha_education
UNION
 SELECT donation_isha_arogya.id,
    donation_isha_arogya.local_trans_id,
    donation_isha_arogya.local_donor_id,
    donation_isha_arogya.donor_poc_id,
    donation_isha_arogya.local_patron_id,
    donation_isha_arogya.status,
    donation_isha_arogya.patron_poc_id,
    donation_isha_arogya.donation_type,
    donation_isha_arogya.project,
    donation_isha_arogya.mode_of_payment,
    donation_isha_arogya.inkind,
    donation_isha_arogya.amount,
    donation_isha_arogya.receipt_number,
    donation_isha_arogya.don_receipt_date,
    donation_isha_arogya.active,
    donation_isha_arogya.system_id,
    donation_isha_arogya.contact_id_fkey,
    donation_isha_arogya.purpose_id,
    donation_isha_arogya.create_date,
    'IshaArogya'::text AS entity_name
   FROM donation_isha_arogya
UNION
 SELECT donation_isha_iii.id,
    donation_isha_iii.local_trans_id,
    donation_isha_iii.local_donor_id,
    donation_isha_iii.donor_poc_id,
    donation_isha_iii.local_patron_id,
    donation_isha_iii.status,
    donation_isha_iii.patron_poc_id,
    donation_isha_iii.donation_type,
    donation_isha_iii.project,
    donation_isha_iii.mode_of_payment,
    donation_isha_iii.inkind,
    donation_isha_iii.amount,
    donation_isha_iii.receipt_number,
    donation_isha_iii.don_receipt_date,
    donation_isha_iii.active,
    donation_isha_iii.system_id,
    donation_isha_iii.contact_id_fkey,
    donation_isha_iii.purpose_id,
    donation_isha_iii.create_date,
    'IIIS'::text AS entity_name
   FROM donation_isha_iii
UNION
 SELECT donation_isha_outreach.id,
    donation_isha_outreach.local_trans_id,
    donation_isha_outreach.local_donor_id,
    donation_isha_outreach.donor_poc_id,
    donation_isha_outreach.local_patron_id,
    donation_isha_outreach.status,
    donation_isha_outreach.patron_poc_id,
    donation_isha_outreach.donation_type,
    donation_isha_outreach.project,
    donation_isha_outreach.mode_of_payment,
    donation_isha_outreach.inkind,
    donation_isha_outreach.amount,
    donation_isha_outreach.receipt_number,
    donation_isha_outreach.don_receipt_date,
    donation_isha_outreach.active,
    donation_isha_outreach.system_id,
    donation_isha_outreach.contact_id_fkey,
    donation_isha_outreach.purpose_id,
    donation_isha_outreach.create_date,
    'IshaOutreach'::text AS entity_name
   FROM donation_isha_outreach;

ALTER TABLE public.donations_view
    OWNER TO odoo;




----------- Finaance filter---------------

CREATE or REPLACE FUNCTION finance_filter(contact_id integer)
RETURNS boolean AS $BODY$
DECLARE
	max_amt float;
	ishanga_tag integer;
BEGIN

	select count(*) into ishanga_tag from res_partner_res_partner_category_rel
	where partner_id = contact_id and category_id = 17;

	select max(sq.sum_amt) into max_amt from 
	( 
	select sum(amount) as sum_amt, extract(year from don_receipt_date) as don_year
	from donations_view  where contact_id_fkey = contact_id 
	group by extract(year from don_receipt_date) having extract(year from don_receipt_date) > '2015'
	) sq;
	
	IF ishanga_tag = 0 THEN
		IF max_amt >= 100000 THEN
			RETURN true;
		ELSE 
			RETURN false;
		END IF;
	ELSIF ishanga_tag = 1 THEN
		IF max_amt >= 50000 THEN
			RETURN true;
		ELSE 
			RETURN false;
		END IF;
	END IF;
			
END; $BODY$
LANGUAGE PLPGSQL;


