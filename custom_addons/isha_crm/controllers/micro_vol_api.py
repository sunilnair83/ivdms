from odoo.http import request, content_disposition, Response
from odoo import fields, http, _
import json

class MicroVolController(http.Controller):
	@http.route('/isha_crm/get_tags', type='http', auth="public", methods=['GET'], website=True, sitemap=False)
	def tag_read(self, query='', model='',limit=25, hierarchical=False, **post):
		if hierarchical:
			data = request.env[model].search_read(domain=[('name', 'ilike', (query or ''))],
				fields=['id', 'name', 'parent_id'],
				limit=int(limit),)
			result = {}
			for x in data:
				if x['parent_id'] and x['parent_id'][1] in result:
					result[x['parent_id'][1]].append({'id':x['id'],'text':x['name']})
				elif x['parent_id'] and x['parent_id'][1] not in result:
					result[x['parent_id'][1]] = [{'id': x['id'], 'text': x['name']}]
			params = []
			for key in result:
				params.append({'id': '',
								'text': key,
								'children': result[key],
								'disabled': True})
			return json.dumps(params)
		else:
			data = request.env[model].search_read(
				domain=[('name', 'ilike', (query or ''))],
				fields=['id', 'name'],
				limit=int(limit),
			)
			return json.dumps(data)
