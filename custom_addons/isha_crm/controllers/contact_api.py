import datetime
import json
import logging
import traceback

import requests
import werkzeug
import werkzeug.wrappers
from odoo.addons.muk_rest import tools
from .. import NameMatchEngine
from ..GoldenContactAdv import EXACT_MATCH,HIGH_MATCH,LOW_MATCH,NO_MATCH
import odoo
from odoo.http import request, Response

_logger = logging.getLogger(__name__)
null = None


def converter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()


class ContactApiController(odoo.http.Controller):

    @odoo.http.route([
        '/api/golden-contacts/search'
    ], auth="none", type='json', methods=['POST'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def search_contacts(self, **kw):
        _logger.info("Contact Search api [POST]")
        name = request.params.get('name')
        phone = request.params.get('phone')
        email = request.params.get('email')

        result = []
        high_match = request.env['res.partner'].apiSearchContact(name, phone, email)

        for rec in high_match:
            if len(rec.read()) == 1:
                result.append(self._get_contact_fields(rec))

        return result

    def construct_api_response(self, params):
        res = {"jsonrpc": "2.0", "id": null, "result": params}
        _logger.info(str(res))
        return Response(json.dumps(res, default=converter), content_type='application/json;charset=utf-8',
                        status=200)

    @odoo.http.route([
        '/api/golden-contacts/verifyContact'
    ], auth="none", type='http', methods=['GET'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def get_is_meditator_or_ieo(self, name=None, phone=None, email=None, sso_id = None,check_meditator=False, check_ieo=False, check_shoonya=False, check_samyama=False,lenient_search=False, **kw):
        try:
            if check_meditator:
                check_meditator = eval(check_meditator)
            if check_ieo:
                check_ieo = eval(check_ieo)
            if check_shoonya:
                check_shoonya = eval(check_shoonya)
            if check_samyama:
                check_samyama = eval(check_samyama)
        except:
            pass

        if sso_id:
            matchEngine = NameMatchEngine.NameMatchEngine()
            matches = request.env['contact.map'].sudo().search([('sso_id', '=', sso_id)]).mapped('contact_id_fkey')
            if len(matches) > 0 and check_meditator:
                for rec in matches:
                    match_status = matchEngine.compareNamesWithVariations(name, rec['name'])
                    if (match_status == EXACT_MATCH or match_status == HIGH_MATCH) and rec.is_meditator:
                        return self.construct_api_response(
                            {
                                'ishangam_id': rec.id,
                                'is_meditator': True,
                                'ieo_completed': None,
                                'matched_id': rec.id
                            }
                        )


        high_match = request.env['res.partner'].getHighMatch(name, phone, email,lenient_search)
        ishangam_id = None
        is_meditator = False
        shoonya_completed = False
        samyama_completed = False
        ieo_completed = False
        rec_id = None

        if check_shoonya:
            _logger.info('check Shoonya '+str(high_match))
            for rec in high_match:
                if rec.shoonya_date:
                    shoonya_completed = True
                    rec_id = rec.id
                    break
            return self.construct_api_response({"ishangam_id":rec_id,"shoonya_completed":shoonya_completed})

        if check_samyama:
            _logger.info('check Samyama '+str(high_match))
            for rec in high_match:
                if rec.samyama_date:
                    samyama_completed = True
                    rec_id = rec.id
                    break
            return self.construct_api_response({"ishangam_id": rec_id, "samyama_completed": samyama_completed})

        if check_meditator:
            _logger.info('check meditator '+str(high_match))
            for rec in high_match:
                if rec.is_meditator:
                    is_meditator = True
                    ieo_completed = 'NA'
                    rec_id = rec.id
                    break
        if check_ieo and not is_meditator:
            _logger.info('check ieo '+str(high_match))
            for rec in high_match:
                if rec.ieo_date:
                    ieo_completed = True
                    is_meditator = rec.is_meditator
                    rec_id = rec.id
                    break
        if len(high_match)>0 and not rec_id:
            recent_contact = high_match[0]
            for rec in high_match:
                if rec.write_date > recent_contact.write_date:
                    recent_contact = rec
            rec_id = recent_contact.id

        res = {
            'ishangam_id': rec_id,
            'is_meditator': is_meditator,
            'ieo_completed': ieo_completed,
            'matched_id' : rec_id
        }
        return self.construct_api_response(res)

    @odoo.http.route([
        '/api/ieco/getdetails'
    ], auth="none", type='http', methods=['GET'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def get_sso_details_from_cm1(self, ssoid='',ipctry='', ipadd='', name='', phone='', email='', check_meditator=False,
                              check_ieo=False, check_shoonya=False, check_samyama=False, lenient_search=False, **kw):

        try:
            matchEngine = NameMatchEngine.NameMatchEngine()
            ipctry = ipctry.upper()
            is_meditator = False
            rec_id = None
            isMumbaiIEC = False
            PartialPaid = None
            currencyVar = 'INR'
            res = {
                'ishangam_id': 0,
                'is_meditator': is_meditator,
                'isMumbaiIEC': isMumbaiIEC,
                'amount_to_be_paid': PartialPaid,
                'currency': currencyVar
            }

            if ipctry == "IN":
                _logger.info("inside india" + ipadd + ssoid + name + phone + email)
                sso_match = request.env['contact.map'].sudo().search([('sso_id', '=', ssoid)], limit=1)
                _logger.info("inside india" + ipadd + ssoid + name + phone + email)
                if email:
                    email = email.lower()
                    reccollvar = request.env['isha.iecousercheckismumbaiiec'].sudo().search([('email', '=', email)])
                    for recvar in reccollvar:
                        isMumbaiIEC = recvar['ismumbaiiec']
                        if recvar['partialpaid'] != -1:
                            PartialPaid = recvar['partialpaid']
                        #PartialPaid = recvar['partialpaid']
                        if recvar['currency']:
                            currencyVar = recvar['currency']
                        break
                # if ssoid:
                #     reccollvar2 = request.env['contact.map'].sudo().search([('sso_id', '=', ssoid)])
                #     for recvar2 in reccollvar2:
                #         if recvar2.contact_id_fkey.is_meditator:
                #             is_meditator = True
                #             break

                sso_match = request.env['contact.map'].sudo().search([('sso_id', '=', ssoid)]).mapped('contact_id_fkey')
                if sso_match:
                    for rec in sso_match:
                        match_status = matchEngine.compareNamesWithVariations(name, rec['name'])
                        if (match_status == EXACT_MATCH or match_status == HIGH_MATCH) and rec.is_meditator:
                            rec_id = rec.id
                            is_meditator = True
                            break
                # reccollvar2 = request.env['res.partner'].sudo().search([('sso_id', '=', ssoid)])
                # for match in reccollvar2:
                #     rec_id = match.id
                #     if match.is_meditator:
                #         print("ssoid inside2")
                #         is_meditator = True
                #         break
                existing_rec = request.env['iec.mega.pgm'].sudo().search([('sso_id', '=', ssoid),('status', '=', 'inq')])
                if len(existing_rec) == 0:
                    request.env['iec.mega.pgm'].sudo().create({
                        'sso_id': ssoid,
                        'email': email,
                        'name': name,
                        'phone': phone,
                        'partner_id': rec_id,
                        'payment_completed': False,
                        'status':'inq'
                    })
                return self.construct_api_response({
                    'ishangam_id': rec_id,
                    'is_meditator': is_meditator,
                    'isMumbaiIEC': isMumbaiIEC,
                    'amount_to_be_paid': PartialPaid,
                    'currency': currencyVar
                })


            if ipctry == "AU" or ipctry == "SG" or ipctry == "MY" or ipctry == "NZ" or ipctry == "HK" or ipctry == 'ID' or ipctry == 'TH' or ipctry == 'PH':
                _logger.info("inside moksha1 apac" + ipadd + ssoid + name + phone + email)

                try:
                    self.s2_api_transfer()
                except Exception as ex:
                    tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                    _logger.error("IECSO: api transfer to s2 has failed "+tb_ex)

                headersvar = {key: value for (key, value) in request.httprequest.headers if key != 'Host'}
                headersvar['x-api-key'] = request.env['ir.config_parameter'].sudo().get_param('isha_crm.ieco_apac_api_token')
                # url=request.httprequest.url.replace(request.httprequest.host_url, 'https://crmt.ishayoga.live/_api/moksha/checkMeditator'),
                urlvar = request.httprequest.url.replace(request.httprequest.base_url,request.env['ir.config_parameter'].sudo().get_param('isha_crm.ieco_apac_api_url'))
                resp = requests.request(
                    method=request.httprequest.method,
                    url=urlvar,
                    headers=headersvar,
                    data=request.httprequest.get_data(),
                    cookies=request.httprequest.cookies,
                    verify=False,
                    allow_redirects=False)

                excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
                headers = [(name, value) for (name, value) in resp.raw.headers.items()
                           if name.lower() not in excluded_headers]

                # response = werkzeug.wrappers.Response()
                responsev = werkzeug.wrappers.Response(resp.content, resp.status_code, headers)
                return responsev

            else:  # if ipctry == "US":
                _logger.info("inside moksha1 other countries" + ipadd + ssoid + name + phone + email)
                return self.s2_api_transfer()

        except Exception as e:
            tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
            _logger.info(tb_ex)
        return self.construct_api_response(res)


    def s2_api_transfer(self):
        headersvar = {key: value for (key, value) in request.httprequest.headers if key != 'Host'}
        # del headersvar['Upgrade-Insecure-Requests']
        # urlvar = request.httprequest.url.replace(request.httprequest.host_url,'https://mokshatwo.sushumna.isha.in')
        urlvar = request.httprequest.url.replace(request.httprequest.base_url,
                                                 request.env['ir.config_parameter'].sudo().get_param(
                                                     'isha_crm.ieco_api_s2'))
        # urlvar = 'https://registrations.sushumna.isha.in/api/ieco/getdetailstwomoksha?name=Mitchell&phone=9876543210&email=harishkumar.nakshatram%40ishafoundation.org&check_meditator=True&check_ieo=False&check_shoonya=False&check_samyama=False&lenient_search=true&ssoid=H2llKFJS09X0MrJqYn1Jfeq3puG2&ipadd=138.132.9.102'
        _logger.info("inside moksha1 non india non apac")
        _logger.info(urlvar)
        # print('urlvar')
        resp = requests.request(
            method=request.httprequest.method,
            url=urlvar,
            headers=headersvar,
            data=request.httprequest.get_data(),
            cookies=request.httprequest.cookies,
            verify=False,
            allow_redirects=False)

        excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
        headers = [(name, value) for (name, value) in resp.raw.headers.items()
                   if name.lower() not in excluded_headers]

        # response = werkzeug.wrappers.Response()
        responsev = werkzeug.wrappers.Response(resp.content, resp.status_code, headers)
        return responsev

    @odoo.http.route([
        '/api/ieco/getdetailstwomoksha'
    ], auth="none", type='http', methods=['GET'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def get_sso_details_from_cm2(self, ssoid='', ipadd='', name='', phone='', email='', check_meditator=False,
                                 check_ieo=False, check_shoonya=False, check_samyama=False, lenient_search=False, **kw):

        try:
            _logger.info("inside getdetailsmoksha2" + request.httprequest.url)
            _logger.info("inside getdetailsmoksha2 ssoid " + ssoid)
            matchEngine = NameMatchEngine.NameMatchEngine()
            is_meditator = False
            rec_id = None
            isMumbaiIEC = False
            PartialPaid = None
            currencyVar = 'INR'
            res = {
                'ishangam_id': 0,
                'is_meditator': is_meditator,
                'isMumbaiIEC': isMumbaiIEC,
                'amount_to_be_paid': PartialPaid,
                'currency': currencyVar
            }

            if email:
                email = email.lower()
                reccollvar = request.env['isha.iecousercheckismumbaiiec'].sudo().search([('email', '=', email)])
                for recvar in reccollvar:
                    isMumbaiIEC = recvar['ismumbaiiec']
                    if recvar['partialpaid'] != -1:
                        PartialPaid = recvar['partialpaid']
                    # PartialPaid = recvar['partialpaid']
                    if recvar['currency']:
                       currencyVar = recvar['currency']
                    break
            #print("ssoid inside2")
            sso_match = request.env['contact.map'].sudo().search([('sso_id', '=', ssoid)]).mapped('contact_id_fkey')
            if sso_match:
                for rec in sso_match:
                    match_status = matchEngine.compareNamesWithVariations(name, rec['name'])
                    if (match_status == EXACT_MATCH or match_status == HIGH_MATCH) and rec.is_meditator:
                        rec_id = rec.id
                        is_meditator = True
                        break
            # reccollvar2 = request.env['res.partner'].sudo().search([('sso_id', '=', ssoid)])
            # for match in reccollvar2:
            #     rec_id = match.id
            #     if match.is_meditator:
            #         print("ssoid inside2")
            #         is_meditator = True
            #         break

            #_logger.info("outside ssoid match not if block is_meditator")
            existing_rec = request.env['iec.mega.pgm'].sudo().search([('sso_id', '=', ssoid),('status','=','inq')])
            if len(existing_rec) == 0:
                request.env['iec.mega.pgm'].sudo().create({
                    'sso_id': ssoid,
                    'email': email,
                    'name': name,
                    'phone': phone,
                    'partner_id': rec_id,
                    'payment_completed': False,
                    'status':'inq'
                })
            return self.construct_api_response({
                'ishangam_id': rec_id,
                'is_meditator': is_meditator,
                'isMumbaiIEC': isMumbaiIEC,
                'amount_to_be_paid': PartialPaid,
                'currency': currencyVar
            })
        except Exception as e:
            tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
            _logger.info(tb_ex)
        return self.construct_api_response(res)



    @odoo.http.route([
        '/api/golden-contacts/search'
    ], auth="none", type='http', methods=['GET'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def search_contacts_get(self, name=None, phone=None, email=None, **kw):
        _logger.info("Contact Search api [GET]")

        result = []
        high_match = request.env['res.partner'].apiSearchContact(name, phone, email)
        for rec in high_match:
            if len(rec.read()) == 1:
                result.append(self._get_contact_fields(rec))
        res = {
            "jsonrpc": "2.0",
            "id": null,
            "result": result
        }
        return Response(json.dumps(res, default=converter), content_type='application/json;charset=utf-8',
                        status=200)

    @odoo.http.route([
        '/api/golden-contacts/transactions/program'
    ], auth="none", type='json', methods=['POST'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def get_program_txn(self, **kw):
        _logger.info("Program Txn api [POST]")
        guid = request.params.get('guid')

        result = []
        txns = request.env['program.attendance'].search([('guid','=',guid)]) if guid else request.env['program.attendance']

        for rec in txns.read():
            result.append(self._get_program_fields(rec))

        return result

    @odoo.http.route([
        '/api/golden-contacts/transactions/program'
    ], auth="none", type='http', methods=['GET'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def get_program_txn_get(self, guid=None,**kw):
        _logger.info("Program Txn api [GET]")

        result = []
        txns = request.env['program.attendance'].search([('guid','=',guid)]) if guid else request.env['program.attendance']
        for rec in txns.read():
            result.append(self._get_program_fields(rec))

        res = {
            "jsonrpc": "2.0",
            "id": null,
            "result": result
        }
        return Response(json.dumps(res, default=converter), content_type='application/json;charset=utf-8',
                        status=200)

    @odoo.http.route([
        '/api/golden-contacts/get'
    ], auth="none", type='json', methods=['POST'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def get_contact_by_guid(self, **kw):
        _logger.info("Contact GUID api [POST]")
        guid = request.params.get('guid')
        rec = request.env['res.partner'].search([('guid','=',guid)]) if guid else request.env['res.partner']
        if len(rec.read()) == 1:
            return self._get_contact_fields(rec)
        return {}

    @odoo.http.route([
        '/api/golden-contacts/get'
    ], auth="none", type='http', methods=['GET'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def get_contact_by_guid_get(self, guid=None,**kw):
        _logger.info("Contact GUID api [GET]")
        rec = request.env['res.partner'].search([('guid','=',guid)]) if guid else request.env['res.partner']
        result = {}
        if len(rec.read()) == 1:
            result =  self._get_contact_fields(rec)
        res = {
            "jsonrpc": "2.0",
            "id": null,
            "result": result
        }
        return Response(json.dumps(res, default=converter), content_type='application/json;charset=utf-8',
                        status=200)

    def _get_program_fields(self, rec):
        return {
            "localTransId": rec['local_trans_id'],
            "localContactId": rec['local_contact_id'],
            "programTypeName": rec['program_name'],
            "regDateTime": rec['reg_date_time'],
            "regCreatedBy": None,
            "regStatus": rec['reg_status'],
            "localProgramTypeId": rec['local_program_type_id'],
            "active": rec['active'],
            "systemId": rec['system_id'],
            "modifiedAt": rec['write_date'],
            "modifiedBy": None,
            "recordModifiedAt": rec['write_date'],
            "programScheduleInfo": eval(rec['program_schedule_info'])
        }

    def _get_contact_fields(self, rec):
        return {
            "contactGuid": rec['guid'],
            "modifiedAt": rec['write_date'],
            "title": rec['prof_dr'],
            "name": {
                "firstName": rec['name'],
                "lastName": None
            },
            "preferredName": None,
            "occupation": rec['occupation'],
            "gender": rec['gender'],
            "dob": rec['dob'],
            "approxDob": None,
            "maritalStatus": rec['marital_status'],
            "nationality": rec['nationality'],
            "doNotDisturb": {
                'DND Phone' : rec['dnd_phone'],
                'DND EMail' : rec['dnd_email'],
                'DND Post Mail' : rec['dnd_postmail']
            },
            "nationality2": None,
            "deceased": rec['deceased'],
            "photoImage": None,
            "countryOfResidence": None,
            "phones": {
                "phone1": {
                    "number": rec['phone_country_code'] + rec['phone'] if rec['phone_country_code'] else rec['phone'],
                    "inferredMobile": None,
                    "nationalSignificantNumber": rec['phone'],
                    "socialPlatform": [],
                    "verifiedInfo": []
                },
                "phone2": {
                    "number": rec['phone2_country_code'] + rec['phone2'] if rec['phone2_country_code'] else rec['phone2'],
                    "inferredMobile": None,
                    "nationalSignificantNumber": rec['phone2'],
                    "socialPlatform": [],
                    "verifiedInfo": []
                },
                "phone3": {
                    "number": rec['phone3_country_code'] + rec['phone3'] if rec['phone3_country_code'] else rec['phone3'],
                    "inferredMobile": None,
                    "nationalSignificantNumber": rec['phone3'],
                    "socialPlatform": [],
                    "verifiedInfo": []
                },
                "phone4": {
                    "number": rec['phone4_country_code'] + rec['phone4'] if rec['phone4_country_code'] else rec['phone4'],
                    "inferredMobile": None,
                    "nationalSignificantNumber": rec['phone4'],
                    "socialPlatform": [],
                    "verifiedInfo": []
                },
            },
            "emails": {
                "email1": {
                    "address": rec['email'],
                    "socialPlatform": [],
                    "verifiedInfo": []
                },
                "email2": {
                    "address": rec['email2'],
                    "socialPlatform": [],
                    "verifiedInfo": []
                },
                "email3": {
                    "address": rec['email3'],
                    "socialPlatform": [],
                    "verifiedInfo": []
                }
            },
            "companies": rec['companies'],
            "addresses": {
                "address1": {
                    "addressline1": rec['street'],
                    "addressline2": rec['street2'],
                    "townCity": rec['city'],
                    "stateProvinceRegion": rec['state'],
                    "postalCode": rec['zip'],
                    "country": rec['country']
                },
                "address2": {
                    "addressline1": rec['street2_1'],
                    "addressline2": rec['street2_2'],
                    "townCity": rec['city2'],
                    "stateProvinceRegion": rec['state2'],
                    "postalCode": rec['zip2'],
                    "country": rec['country2']
                },

            },
            "workAddresses": {
                "address1": {
                    "addressline1": rec['work_street1'],
                    "addressline2": rec['work_street2'],
                    "townCity": rec['work_city'],
                    "stateProvinceRegion": rec['work_state'],
                    "postalCode": rec['work_zip'],
                    "country": rec['work_country']
                }

            },
            "idProof": {
                "AADHAAR": {
                    "number": rec['aadhaar_no'],
                    "expiryDate": None,
                    "image": None
                },
                "PAN": {
                    "number": rec['pan_no'],
                    "expiryDate": None,
                    "image": None
                },
                "PASSPORT": {
                    "number": rec['passport_no'],
                    "expiryDate": None,
                    "image": None
                }
            },
            "nonStandardIdProof": rec['id_proofs'],
            "contactType": rec['contact_type'],
            "active": rec['active']

        }
