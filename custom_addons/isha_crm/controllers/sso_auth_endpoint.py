import hashlib
import hmac
import json
import logging
import requests
import traceback

import werkzeug.urls
import werkzeug.utils
from odoo.addons.auth_oauth.controllers.main import OAuthLogin
from odoo.addons.auth_oauth.controllers.main import fragment_to_query_string
from odoo.addons.web.controllers.main import ensure_db, set_cookie_and_redirect, login_and_redirect
from odoo.addons.website.controllers.main import Website

from odoo import models, api, http, SUPERUSER_ID, _
from odoo import registry as registry_get
from odoo.exceptions import AccessDenied
from odoo.http import request
from .. import ContactProcessor
from .. import ErrorUpdater
from .. import GoldenContactAdv
from .. import NameInitialsComp
from .. import NameMatchEngine
from ..isha_base_importer import Configuration

_logger = logging.getLogger(__name__)
null = None
false = False
true = True
# Method to compute the Hash Value
def hash_hmac(data,secret):
    data_enc = json.dumps(data['session_id'], sort_keys=True, separators=(',', ':'))
    signature = hmac.new(bytes(secret, 'utf-8'), bytes(data_enc, 'utf-8'), digestmod=hashlib.sha256).hexdigest()
    return signature

# Api to get the profile data
def sso_get_user_profile(url, payload):
    dp = {"data": json.dumps(payload)}
    return requests.post(url, data=dp)

# Api to get full profile data
def sso_get_full_data(sso_config,sso_id):
    url = sso_config['SSO_PMS_ENDPOINT']+'/api/full-profiles/'+sso_id

    headers = {
        'authorization': 'Bearer '+sso_config['SSO_API_SYSTEM_TOKEN'],
        'X-legal-entity': sso_config['SSO_LEGEL_ENTITY'],
        'x-user': sso_id
    }
    profile = requests.get(url, headers=headers)
    if profile.status_code == 200:
        return eval(profile.text)
    else:
        return {'no_consent':1}

# Get high matches for the user_profile
def get_partners(env,user_profile):
    # Primary Check on sso_id
    partners = env['res.partner'].sudo().search([('sso_id','=',user_profile['autologin_profile_id'])]).sorted(
        lambda rec: rec.write_date, reverse=True)
    if len(partners) > 0:
        return partners[0]

    # Secondary check on email and name match engine
    partners = env['res.partner'].sudo().search(['|', ('email', '=ilike', user_profile['autologin_email']),
                                              ('email2', '=ilike', user_profile['autologin_email'])]).sorted(
        lambda rec: rec.write_date, reverse=True)
    matchEngine = NameMatchEngine.NameMatchEngine()
    high_match = []
    for rec in partners:
        match_status = matchEngine.compareNamesWithVariations(user_profile['autologin_name'], rec['name'])
        if match_status == NameInitialsComp.EXACT_MATCH or match_status == NameInitialsComp.HIGH_MATCH:
            high_match.append(rec)

    return high_match

# Get the valid user for the user_profile
def get_sso_user(env, user_profile):
    # Direct search for sso_id
    users = env['res.users'].sudo().search([('login','=',user_profile['autologin_profile_id'])])
    if len(users) > 0:
        return users

    # Search for Res Partner
    high_match = get_partners(env,user_profile)

    # Search for existing Users for the high matches
    users = env['res.users'].sudo().search([('partner_id','in',[x.id for x in high_match])])
    if len(users) > 0:
        return users

    # Select a valid partner else most recent
    if len(high_match) > 0:
        valid_partner = high_match[0]
        for partner in high_match:
            if filter_res_partner(partner):
                valid_partner = partner
                break

        # Create a user for the selected res_partner
        user = create_user_from_template(env, {'login': user_profile['autologin_profile_id'],
                                               'partner_id': valid_partner.id,
                                               'company_id': 1,
                                               'website_published':True,
                                               'company_ids': [(6, 0, [1])]}
                                         )
    else:
        # Creat a user for the user_profile from sso
        user = create_user_from_template(env, {'login': user_profile['autologin_profile_id'],
                                               'name': user_profile['autologin_name'],
                                               'email': user_profile['autologin_email'],
                                               'website_published':True,
                                               'company_id': 1,
                                               'company_ids': [(6, 0, [1])]}
                                         )

    return user

# Check if sso user is internal user
def check_internal_user(env, user_profile):
    # Search for email
    users = env['res.users'].sudo().search([('login','=',user_profile['autologin_email'])])
    return True if len(users) > 0 else False


# Create res_user for the dict
def create_user_from_template(env, user_params):
    return env['res.users'].sudo()._signup_create_user(user_params)


# Filter for res_partner
def filter_res_partner(partner):
    if partner.is_meditator is True and len(partner.starmark_category_id) == 0:  # \
        # and (not partner.country or partner.country == 'IN'):
        return True
    else:
        return False

# Get the user if the matching partner is valid
def get_valid_users(users):
    for x in users:
        if x.special_portal_access or filter_res_partner(x):
            return x

    return False

# update Res Partner with sso_profile data
def update_res_partner_profile_data(env,user,profile_data):
    try:
        goldenContact = GoldenContactAdv.GoldenContactAdv()
        goldenContact.setEnvValues(env)
        sso_contact = ContactProcessor.SSOContacts()
        odoo_rec = sso_contact.getOdooFormat(profile_data,{},{})
        flag_dict = goldenContact.mergeRecord([user.partner_id],odoo_rec)
    except Exception as ex:
        tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        rec = {'src_msg':profile_data,'error':tb_ex,'topic':'SSO'}
        errorUpdater = ErrorUpdater.ErrorUpdater()
        errorUpdater.logErrorEnv(env, rec)


class SSOAuthController(http.Controller):

    @http.route('/sso_auth/signin', type='http',methods=['POST'], auth='none',csrf=False)
    @fragment_to_query_string
    def sso_auth_signin(self, **kw):
        registry = registry_get(request.env.cr.dbname)
        with registry.cursor() as cr:
            try:
                sso_config = Configuration('SSO')
                env = api.Environment(cr, SUPERUSER_ID, {})
                secret = sso_config['SSO_API_SECRET']
                api_key = sso_config['SSO_API_KEY']
                session_id = request.httprequest.form['session_id']
                url = request.httprequest.form['request_url']

                hash_val = hash_hmac({'session_id':session_id} ,secret)
                data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
                sso_user_profile = request.env.context.get('sso_user_profile')
                if not sso_user_profile:
                    sso_user_profile = eval(sso_get_user_profile(sso_config['SSO_LOGIN_INFO_ENDPOINT'],data).text)

                # Redirect internal user
                if check_internal_user(env,sso_user_profile):
                    return set_cookie_and_redirect('/web/login?oauth_error=4')
                res_users = get_sso_user(env, sso_user_profile)
                valid_user = get_valid_users(res_users)

                if not valid_user:
                    valid_user = res_users[0]
                    if 'gate_open' not in url:
                        url = '/my/verify?login=' + sso_user_profile['autologin_profile_id']

                valid_user.sudo().write({'login':sso_user_profile['autologin_profile_id'],'oauth_access_token':session_id})
                env.cr.commit()

                if 'verify' in url:
                    return set_cookie_and_redirect(url)
                # Due to low trust value of sso data we are not updating res_partner for now.
                # full_profile = sso_get_full_data(sso_config,sso_user_profile['autologin_profile_id'])
                # update_res_partner_profile_data(env,valid_user,full_profile)
                # env.cr.commit()
                credentials = (request.env.cr.dbname, valid_user.login, valid_user.oauth_access_token)

                resp = login_and_redirect(*credentials, redirect_url=url)
                # Since /web is hardcoded, verify user has right to land on it
                if werkzeug.urls.url_parse(resp.location).path == '/web' and not request.env.user.has_group('base.group_user'):
                    resp.location = '/my/home'
                return resp
            except AttributeError:
                # auth_signup is not installed
                _logger.error("auth_signup not installed on database oauth sign up cancelled.")
                url = "/web/login?oauth_error=1"
            except AccessDenied:
                # oauth credentials not valid, user could be on a temporary session
                _logger.info('OAuth2: access denied, redirect to main page in case a valid session exists, without setting cookies')
                url = "/web/login?oauth_error=3"
                redirect = werkzeug.utils.redirect(url, 303)
                redirect.autocorrect_location_header = False
                return redirect
            except Exception as e:
                # signup error
                _logger.exception("OAuth2: %s" % str(e))
                url = "/web/login?oauth_error=2"

        return set_cookie_and_redirect(url)

    @http.route(['/my/verify'], type='http',methods=['GET'], auth="public", website=True)
    def sso_verify_user_get(self, login=None,upd=None):
        valid_user = request.env['res.users'].sudo().search([('login', '=', login)]) if login else None
        countries = request.env['res.country'].sudo().search([])
        if login and len(valid_user)>0:
            values = {'partner': valid_user.partner_id,'error':True,'user':valid_user,'countries':countries}
            if upd:
                values['error']=False
            return request.render("isha_crm.mv_unverified_user", values)
        else:
            url = "/web/login?oauth_error=2"
            return set_cookie_and_redirect(url)

    @http.route(['/my/verify'], type='http',methods=['POST'], auth="public", website=True)
    def sso_verify_user_post(self, **post):
        if post:
            partner_id = post.pop('partner_id',None)
            user_id = post.pop('user_id',None)
            if partner_id:
                registry = registry_get(request.env.cr.dbname)
                with registry.cursor() as cr:
                    try:
                        env = api.Environment(cr, SUPERUSER_ID, {})
                        partner = env['res.partner'].sudo().browse(int(partner_id))
                        valid_user = request.env['res.users'].sudo().browse(int(user_id))
                        post['country_id'] = int(post['country_id'])
                        goldenContact = GoldenContactAdv.GoldenContactAdv()
                        post = ContactProcessor.replaceEmptyString(post)
                        # Phone
                        post.update({'phone_country_code':post['phone_country_code'], 'phone_is_valid': True, 'phone_is_verified': True})
                        if 'phone2' in post and post['phone2']:
                            post.update({'phone2_country_code': post['phone2_country_code'], 'phone2_is_valid': True, 'phone2_is_verified': True})
                        phone_dict = goldenContact.getPhonesDict(partner, post)
                        post.update(phone_dict)
                        # Email
                        email_dict = goldenContact.getEmailsDict(partner, post)
                        post.update(email_dict)
                        partner.sudo().write(post)
                        env.cr.commit()
                        partner = request.env['res.partner'].sudo().browse(int(partner_id))
                        countries = request.env['res.country'].sudo().search([])
                        values = {'partner': partner,'error': False,'user':valid_user,'countries':countries}
                        return werkzeug.utils.redirect('/my/verify?upd=1&login='+valid_user.login, 303)
                    except Exception as ex:
                        print(ex)
            else:
                url = "/web/login?oauth_error=2"
                return set_cookie_and_redirect(url)

    @http.route(['/home'], type='http',methods=['GET'], auth="public", website=True)
    def load_home_page(self):
        params = OAuthLogin.list_providers(OAuthLogin())
        return request.render('isha_crm.home_page_layout',{'params':params})

class OatutOverride(OAuthLogin):
    @http.route()
    def web_login(self, *args, **kw):
        ensure_db()
        if request.httprequest.method == 'GET' and request.session.uid and request.params.get('redirect'):
            # Redirect if already logged in and redirect param is present
            return http.redirect_with_hash(request.params.get('redirect'))
        providers = self.list_providers()

        response = super(OAuthLogin, self).web_login(*args, **kw)
        if response.is_qweb:
            error = request.params.get('oauth_error')
            if error == '1':
                error = _("Sign up is not allowed on this database.")
            elif error == '2':
                error = _("Access Denied")
            elif error == '3':
                error = _("You do not have access to this database or your invitation has expired. Please ask for an invitation and be sure to follow the link in your invitation email.")
            elif error == '4':
                error = _("Please login as an internal user")
            else:
                error = None

            response.qcontext['providers'] = providers
            if error:
                response.qcontext['error'] = error

        return response


class SSOConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    def getOauthLinks(self):
        return OAuthLogin.list_providers(OAuthLogin())

    def get_sso_config(self):
        sso_config = Configuration('SSO')
        secret = sso_config['SSO_API_SECRET']
        api_key = sso_config['SSO_API_KEY']
        call_back_url = sso_config['SSO_LOGIN_CALLBACK_URL']
        ret_list = {'request_url': str(request.httprequest.url),
                    "callback_url": call_back_url,
                    "api_key": api_key
                    }
        data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
        data_enc = data_enc.replace("/", "\\/")
        signature = hmac.new(bytes(secret, 'utf-8'), bytes(data_enc, 'utf-8'),
                             digestmod=hashlib.sha256).hexdigest()

        ret_list = [sso_config['SSO_LOGIN_URL'],
                    str(request.httprequest.url),
                    call_back_url,
                    api_key,
                    signature,
                    sso_config['SSO_LOGIN_ACTION'],
                    sso_config['SSO_LEGEL_ENTITY'],
                    sso_config['SSO_LOGIN_FORCE_CONSENT'],
                    ]

        return ret_list

class WebsiteOverride(Website):

    @http.route(website=True, auth="public", sitemap=False)
    def web_login(self, redirect=None, *args, **kw):
        if 'admin_login' not in kw and 'login' not in kw and 'oauth_error' not in kw and request.env.user.id == 4:
            params = OAuthLogin.list_providers(OAuthLogin())
            return request.render('isha_crm.home_page_layout', {'params': params})

        else:
            return super(WebsiteOverride, self).web_login(redirect,*args,**kw)
