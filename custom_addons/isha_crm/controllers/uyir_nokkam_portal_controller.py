import urllib.parse

from odoo.addons.portal.controllers.portal import CustomerPortal

import odoo.http as http
from odoo import _
from odoo.http import request
from odoo.osv.expression import OR


class UyirNokkamPortalController(CustomerPortal):

    def _prepare_portal_layout_values(self):
        values = super(UyirNokkamPortalController, self)._prepare_portal_layout_values()
        return values

    @http.route(['/onlineuyirnokkam'], type='http', auth="user", website=True)
    def query_uyirnokkam_data(self, page=1, date_begin=None, date_end=None, sortby=None, search_in='record_phone', search='', **kw):
        if not request.env.user.has_group('isha_crm.group_portal_reg_search') and not request.env.user.has_group('base.group_user'):
            return request.redirect('/my/home')
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        searchbar_sortings = {
            'create_date_asc': {'label': _('Created Date (Asc)'), 'order': 'create_date'},
            'create_date_desc': {'label': _('Created Date (Desc)'), 'order': 'create_date desc'},
            'write_date_asc': {'label': _('Updated On (Asc)'), 'order': 'write_date'},
            'write_date_desc': {'label': _('Updated On (Desc)'), 'order': 'write_date desc'},
        }
        searchbar_inputs = {
            'record_phone': {'input': 'record_phone', 'label': _('Search in Phone')},
            'record_name': {'input': 'record_name', 'label': _('Search in Contact Name')},
            'record_email': {'input': 'record_email', 'label': _('Search in Email')},
            'reg_number': {'input': 'reg_number', 'label': _('Search in Registration Number')},
            # 'all': {'input': 'all', 'label': _('Search in All')},
        }

        domain = [('program_name', '=', 'Uyir Nokkam Online'), ('start_date', '=', '2021-09-24')] if search else []
        # search
        if search and search_in:
            search_domain = []
            if search_in in ('record_email', 'all'):
                search_domain = OR([search_domain, [('record_email', 'ilike', search)]])
            if search_in in ('record_phone', 'all'):
                search_domain = OR([search_domain, [('record_phone', 'ilike', search)]])
            if search_in in ('record_name', 'all'):
                search_domain = OR([search_domain, [('record_name', 'ilike', search)]])
            if search_in in ('reg_number', 'all'):
                search_domain = OR([search_domain, [('reg_number', 'ilike', search)]])
            domain += search_domain

        # default sort by value
        if not sortby:
            sortby = 'create_date_desc'
        order = searchbar_sortings[sortby]['order']

        program_lead_sudo = request.env['program.lead'].sudo()

        uyir_nokkam_recs = program_lead_sudo.search(domain, order=order, limit=5)

        uyir_nokkam_list = []
        for rec in uyir_nokkam_recs:
            uyir_nokkam_list.append({
                    'id': rec.id,
                    'record_name': rec.record_name,
                    'reg_status': rec.reg_status,
                    'record_email': rec.record_email,
                    'record_phone': rec.record_phone,
                    'record_center': rec.contact_id_fkey.center_id.name,
                    'record_region': rec.contact_id_fkey.center_id.region_id.name,
                    'reg_num': rec.reg_number,
                    'zoom_url': rec.meeting_url,
                    'day_wise_attendance': rec.day_1_attendance,
                    'duration_missed': rec.day_1_duration_missed
                        })

        values.update({
            'uyir_nokkam_recs': uyir_nokkam_list,
            'page_name': 'Uyir Nokkam',
            # 'pager': pager,
            'searchbar_sortings': searchbar_sortings,
            'searchbar_inputs': searchbar_inputs,
            'search_in': search_in,
            'sortby': sortby,
            # 'archive_groups': archive_groups,
            'default_url': '/onlineuyirnokkam',
        })
        return request.render("isha_crm.portal_uyirnokkam_view", values)

    @http.route(['/onlineuyirnokkam/participant/details_view/<int:reg_num>'], type='http', auth="user", website=True)
    def uyirnokkam_participant_details_view(self, reg_num, category='', search='', **post):
        if not request.env.user.has_group('isha_crm.group_portal_reg_search') and not request.env.user.has_group('base.group_user'):
            return request.redirect('/my/home')
        context = dict(request.env.context or {})
        program_lead_obj = request.env['program.lead']
        context.update(active_id=reg_num)

        uyirnokkam_data_list = []
        uyirnokkam_data = program_lead_obj.sudo().browse(int(reg_num))
        base_url = ''
        zoom_url = uyirnokkam_data.meeting_url
        # if send Email button is clicked
        if post.get('send_email'):
            template = request.env.ref('isha_crm.uyirnokkam_send_email_template')
            values = template.sudo().generate_email(uyirnokkam_data.id, fields=None)
            values['body_html'] = values['body_html'].replace('$$record_name', uyirnokkam_data.record_name)
            values['body_html'] = values['body_html'].replace('$$base_url', zoom_url)
            mail_mail_obj = request.env['mail.mail']
            msg_id = mail_mail_obj.sudo().create(values)
            msg_id.send(False)
        whatsapp_msg = 'நமஸ்காரம் '+ str(uyirnokkam_data.record_name) + \
            '\nஉயிர்நோக்கம் வகுப்பில் அடுத்து வருகின்ற நாட்களில் கீழ்க்கண்ட இணைப்பு மூலமாக இணையவும்.' +\
            '\n\nதங்களுக்கான பிரத்யேக அடையாள குறியீடு:' +\
            '\n'+str(uyirnokkam_data.reg_number) +\
            '\n\nஇணைப்பு: ' +\
            '\n'+uyirnokkam_data.meeting_url +\
            '\n\nதேதி: ' +\
            uyirnokkam_data.pgm_remark +\
            '\nநீங்கள் மொபைல் மூலமாக இணையும் பட்சத்தில்  zoomஐ நீங்கள் டவுன்லோட் செய்து இருக்க வேண்டும்.' +\
             '\n\nநன்றி' +\
             '\nஈஷா தன்னார்வத் தொண்டர்'
        if uyirnokkam_data.contact_id_fkey.whatsapp_number:
            code = uyirnokkam_data.contact_id_fkey.whatsapp_country_code
            if not code:
                code = ''
            params = {'phone': code + uyirnokkam_data.contact_id_fkey.whatsapp_number, 'text': whatsapp_msg}
            url_params = urllib.parse.urlencode(params)
            whatsapp_url = "https://api.whatsapp.com/send?" + url_params
        else:
            whatsapp_url = '/onlineuyirnokkam/participant/details_view/' + str(uyirnokkam_data.id)

        if uyirnokkam_data.contact_id_fkey.phone:
            code = uyirnokkam_data.contact_id_fkey.phone_country_code
            if not code:
                code = ''
            sms_url = 'sms://' + code + uyirnokkam_data.contact_id_fkey.phone
        else:
            sms_url = '/onlineuyirnokkam/participant/details_view/' + str(uyirnokkam_data.id)

        if uyirnokkam_data.record_email:
            email_url = '/onlineuyirnokkam/participant/details_view/' + str(uyirnokkam_data.id) + '?send_email=true'
        else:
            email_url = '/onlineuyirnokkam/participant/details_view/' + str(uyirnokkam_data.id)

        sms_message = 'Namaskaram,' + \
                      '\nUyirnokkam Program Invitation. Registration ID is ' + str(uyirnokkam_data.reg_number) + \
                      '\nTo join, click: ' + uyirnokkam_data.meeting_url + \
                      '\nPranam'

        uyirnokkam_data_list.append({
            'id': uyirnokkam_data.id,
            'record_name': uyirnokkam_data.record_name,
            'record_email': uyirnokkam_data.record_email,
            'record_phone': uyirnokkam_data.record_phone,
            'whatsapp_number': uyirnokkam_data.contact_id_fkey.whatsapp_number,
            'record_center': uyirnokkam_data.contact_id_fkey.center_id.name,
            'record_region': uyirnokkam_data.contact_id_fkey.center_id.region_id.name,
            'meeting_number': uyirnokkam_data.meeting_number,
            'bay_name': uyirnokkam_data.bay_name,
            'batch': uyirnokkam_data.batch,
            'check_in_status': uyirnokkam_data.check_in_status,
            'reg_status': uyirnokkam_data.reg_status,
            'reg_num': uyirnokkam_data.reg_number,
            'zoom_url': uyirnokkam_data.meeting_url,
            'day_1_attendance': uyirnokkam_data.day_1_attendance,
            'day_2_attendance': uyirnokkam_data.day_2_attendance,
            'day_3_attendance': uyirnokkam_data.day_3_attendance,
            'day_4_attendance': uyirnokkam_data.day_4_attendance,
            'day_5_attendance': uyirnokkam_data.day_5_attendance,
            'day_1_duration_missed': uyirnokkam_data.day_1_duration_missed,
            'day_2_duration_missed': uyirnokkam_data.day_2_duration_missed,
            'day_3_duration_missed': uyirnokkam_data.day_3_duration_missed,
            'day_4_duration_missed': uyirnokkam_data.day_4_duration_missed,
            'day_5_duration_missed': uyirnokkam_data.day_5_duration_missed,
            'whatsapp_url': whatsapp_url,
            'sms_url': sms_url,
            'email_url': email_url,
            'whatsapp_message': whatsapp_msg,
            'sms_message': sms_message,
            'duration_missed': uyirnokkam_data.day_1_duration_missed
                        })

        return http.request.render('isha_crm.participant_details_view', {
            'uyir_nokkam_recs': uyirnokkam_data_list
        })

