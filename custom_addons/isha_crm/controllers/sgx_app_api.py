import datetime
import json
import logging
import traceback

import requests
import werkzeug
import werkzeug.wrappers
from odoo.addons.muk_rest import tools

import odoo
from odoo.http import request, Response

_logger = logging.getLogger(__name__)
null = None


def converter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()


class SgXApiController(odoo.http.Controller):
    def construct_api_response(self, params):
        res = {"jsonrpc": "2.0", "id": null, "result": params}
        _logger.info(str(res))
        return Response(json.dumps(res, default=converter), content_type='application/json;charset=utf-8',
                        status=200)

    @odoo.http.route([
        '/api/sgx/get_contact_info'
    ], auth="none", type='http', methods=['GET'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def get_contact_info(self, name='', phone=None, email=None, sso_id=None, ssoId=None, global_api=None,**kw):
        if not global_api:
            return self.construct_api_response([])
        sso_id = sso_id or ssoId  # Global api is using ssoId but keeping the backward compatibility for sso_id
        _logger.info('Get Contact Info '+str({'name':name,'phone':phone,'email':email,'sso_id':sso_id}))
        matches = []
        if sso_id:
            sso_matches = request.env['contact.map'].sudo().search([('sso_id', '=', sso_id)]).mapped('contact_id_fkey')
            for rec in sso_matches:
                matches.append(rec)

        high_match = request.env['res.partner'].sudo().getHighMatch(name, phone, email)
        for rec in high_match:
            matches.append(rec)

        if len(matches) > 0:
            recent_contact = matches[0]
            for rec in matches:
                if rec.write_date > recent_contact.write_date:
                    recent_contact = rec
            return self.construct_api_response(self.get_contact_dict(matches, recent_contact))

        return self.construct_api_response(matches)

    def get_contact_dict(self, matches, rec):
        consolidated_fields = self.get_consolidated_fields(matches)
        return [
            {
                'contact_info':{
                    "id": rec.id,
                    'meditator': consolidated_fields.get('is_meditator'),
                    'gender': rec.gender,
                    'segment': rec.segment,
                    'age': rec.age,
                    'center': rec.center_id.name,
                    'region': rec.region_name
                },
                'pgm_info': consolidated_fields.get('pgm_info'),
                'tag_info': consolidated_fields.get('tag_info')
            }
        ]

    def get_consolidated_fields(self, recs):
        pgm_tags = set()
        tags = set()
        is_meditator = False

        for rec in recs:
            is_meditator = is_meditator or rec.is_meditator
            if rec.rudraksha_reg:
                pgm_tags.add('RD')
            if rec.goy_reg:
                pgm_tags.add('GOY')
            for x in rec.pgm_tag_ids.mapped('name'):
                pgm_tags.add(x)
            for x in rec.category_id.mapped('name'):
                tags.add(x)

        return {
                    'is_meditator':is_meditator,
                    'pgm_info': list(pgm_tags),
                    'tag_info': list(tags)
                }
