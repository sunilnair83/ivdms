# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import json
import lxml
import requests
import logging
import werkzeug.exceptions
import werkzeug.urls
import werkzeug.wrappers
import base64
from datetime import datetime

from odoo import http, tools, _
from odoo.addons.http_routing.models.ir_http import slug
from odoo.addons.website.models.ir_http import sitemap_qs2dom
from odoo.http import request

_logger = logging.getLogger(__name__)


def tags(self, forum, tag_char=None, **post):
    # build the list of tag first char, with their value as tag_char param Ex : [('All', 'all'), ('C', 'c'), ('G', 'g'), ('Z', z)]
    first_char_tag = forum.get_tags_first_char()
    first_char_list = [(t, t.lower()) for t in first_char_tag if t.isalnum()]
    first_char_list.insert(0, (_('All'), 'all'))

    active_char_tag = tag_char and tag_char.lower() or 'all'

    # generate domain for searched tags
    domain = [('forum_id', '=', forum.id), ('posts_count', '>', 0)]
    order_by = 'name'
    if active_char_tag and active_char_tag != 'all':
        domain.append(('name', '=ilike', tools.escape_psql(active_char_tag) + '%'))
        order_by = 'posts_count DESC'
    tags = request.env['forum.tag'].search(domain, limit=None, order=order_by)
    # prepare values and render template
    values = self._prepare_user_values(forum=forum, searches={'tags': True}, **post)

    values.update({
        'tags': tags,
        'pager_tag_chars': first_char_list,
        'active_char_tag': active_char_tag,
    })
    return request.render("isha_crm.tag", values)

def questions(self, forum, tag=None, page=1, filters='all', my=None, sorting=None, search='', **post):
    if not forum.can_access_from_current_website():
        raise werkzeug.exceptions.NotFound()

    Post = request.env['forum.post']

    domain = [('forum_id', '=', forum.id), ('parent_id', '=', False), ('state', '=', 'active')]
    if search:
        domain += ['|', ('name', 'ilike', search), ('content', 'ilike', search)]
    if tag:
        domain += [('tag_ids', 'in', tag.id)]
    if filters == 'unanswered':
        domain += [('child_ids', '=', False)]
    elif filters == 'solved':
        domain += [('has_validated_answer', '=', True)]
    elif filters == 'unsolved':
        domain += [('has_validated_answer', '=', False)]

    user = request.env.user

    if my == 'mine':
        application_ids = request.env['forum.task.applications'].sudo().search([('create_uid','=',user.id)])
        domain += [('id', 'in', application_ids.mapped('forum_post_id').ids)]
    elif my == 'followed':
        domain += [('message_partner_ids', '=', user.partner_id.id)]
    elif my == 'tagged':
        domain += [('tag_ids.message_partner_ids', '=', user.partner_id.id)]
    elif my == 'favourites':
        domain += [('favourite_ids', '=', user.id)]

    if sorting:
        # check that sorting is valid
        # retro-compatibily for V8 and google links
        try:
            Post._generate_order_by(sorting, None)
        except ValueError:
            sorting = False

    if not sorting:
        sorting = forum.default_order

    question_count = Post.search_count(domain)

    if tag:
        url = "/forum/%s/tag/%s/questions" % (slug(forum), slug(tag))
    else:
        url = "/forum/%s" % slug(forum)

    url_args = {
        'sorting': sorting
    }
    if search:
        url_args['search'] = search
    if filters:
        url_args['filters'] = filters
    pager = request.website.pager(url=url, total=question_count, page=page,
                                  step=self._post_per_page, scope=self._post_per_page,
                                  url_args=url_args)

    question_ids = Post.search(domain, limit=self._post_per_page, offset=pager['offset'], order=sorting)

    values = self._prepare_user_values(forum=forum, searches=post, header={'ask_hide': not forum.active})
    values.update({
        'main_object': tag or forum,
        'edit_in_backend': not tag,
        'question_ids': question_ids,
        'question_count': question_count,
        'pager': pager,
        'tag': tag,
        'filters': filters,
        'my': my,
        'sorting': sorting,
        'search': search,
    })
    return request.render("isha_crm.forum_index", values)

def post_create(self, forum, post_parent=None, **post):
    if post.get('content', '') == '<p><br></p>':
        return request.render('http_routing.http_error', {
            'status_code': _('Bad Request'),
            'status_message': post_parent and _('Reply should not be empty.') or _('Question should not be empty.')
        })

    post_tag_ids = forum._tag_to_write_vals(post.get('post_tags', ''))

    if request.env.user.forum_waiting_posts_count:
        return werkzeug.utils.redirect("/forum/%s/ask" % slug(forum))
    task_dict = {}
    if 'image_1920' in post:
        image_1920 = post.get('image_1920')
        if image_1920:
            image_1920 = image_1920.read()
            image_1920 = base64.b64encode(image_1920)
            task_dict.update({
                'image_1920': image_1920
            })
    task_dict.update({
        'forum_id': forum.id,
        'name': post.get('post_name') or (post_parent and 'Re: %s' % (post_parent.name or '')) or '',
        'content': post.get('content', False),
        'parent_id': post_parent and post_parent.id or False,
        'tag_ids': post_tag_ids,
        'task_eta': post.get('task_eta')
    })
    new_question = request.env['forum.post'].create(task_dict)

    return werkzeug.utils.redirect("/forum/%s/question/%s" % (slug(forum), post_parent and slug(post_parent) or new_question.id))

def question(self, forum, question, **post):
    if not forum.active:
        return request.render("isha_crm.header", {'forum': forum})

    # Hide posts from abusers (negative karma), except for moderators
    if not question.can_view:
        raise werkzeug.exceptions.NotFound()

    # Hide pending posts from non-moderators and non-creator
    user = request.env.user
    if question.state == 'pending' and user.karma < forum.karma_post and question.create_uid != user:
        raise werkzeug.exceptions.NotFound()

    # increment view counter
    question.sudo().set_viewed()
    if question.parent_id:
        redirect_url = "/forum/%s/question/%s" % (slug(forum), slug(question.parent_id))
        return werkzeug.utils.redirect(redirect_url, 301)
    filters = 'question'
    values = self._prepare_user_values(forum=forum, searches=post)
    values.update({
        'main_object': question,
        'question': question,
        'can_bump': (question.forum_id.allow_bump and not question.child_ids and (datetime.today() - question.write_date).days > 9),
        'header': {'question_data': True},
        'filters': filters,
        'reversed': reversed,
    })
    return request.render("isha_crm.post_description_full", values)

def question_ask_for_close(self, forum, question, **post):
    reasons = request.env['forum.post.reason'].search([('reason_type', '=', 'basic')])

    values = self._prepare_user_values(**post)
    values.update({
        'question': question,
        'forum': forum,
        'reasons': reasons,
    })
    return request.render("isha_crm.close_post", values)

def forum_post(self, forum, **post):
    user = request.env.user
    if not user.email or not tools.single_email_re.match(user.email):
        return werkzeug.utils.redirect("/forum/%s/user/%s/edit?email_required=1" % (slug(forum), request.session.uid))
    values = self._prepare_user_values(forum=forum, searches={}, header={'ask_hide': True}, new_question=True)
    return request.render("isha_crm.new_question", values)

def post_edit(self, forum, post, **kwargs):
    tags = [dict(id=tag.id, name=tag.name) for tag in post.tag_ids]
    tags = json.dumps(tags)
    values = self._prepare_user_values(forum=forum)
    values.update({
        'tags': tags,
        'post': post,
        'is_edit': True,
        'is_answer': bool(post.parent_id),
        'searches': kwargs,
        'content': post.name,
    })
    return request.render("isha_crm.edit_post", values)


def flagged_queue(self, forum, **kwargs):
    user = request.env.user
    if user.karma < forum.karma_moderate:
        raise werkzeug.exceptions.NotFound()

    Post = request.env['forum.post']
    domain = [('forum_id', '=', forum.id), ('state', '=', 'flagged')]
    if kwargs.get('spam_post'):
        domain += [('name', 'ilike', kwargs.get('spam_post'))]
    flagged_posts_ids = Post.search(domain, order='write_date DESC')

    values = self._prepare_user_values(forum=forum)
    values.update({
        'posts_ids': flagged_posts_ids.sudo(),
        'queue_type': 'flagged',
        'flagged_queue_active': 1,
    })

    return request.render("isha_crm.moderation_queue", values)

def offensive_posts(self, forum, **kwargs):
    user = request.env.user
    if user.karma < forum.karma_moderate:
        raise werkzeug.exceptions.NotFound()

    Post = request.env['forum.post']
    domain = [('forum_id', '=', forum.id), ('state', '=', 'offensive'), ('active', '=', False)]
    offensive_posts_ids = Post.search(domain, order='write_date DESC')

    values = self._prepare_user_values(forum=forum)
    values.update({
        'posts_ids': offensive_posts_ids.sudo(),
        'queue_type': 'offensive',
    })

    return request.render("isha_crm.moderation_queue", values)

def post_ask_for_mark_as_offensive(self, forum, post, **kwargs):
    offensive_reasons = request.env['forum.post.reason'].search([('reason_type', '=', 'offensive')])

    values = self._prepare_user_values(forum=forum)
    values.update({
        'question': post,
        'forum': forum,
        'reasons': offensive_reasons,
        'offensive': True,
    })
    return request.render("isha_crm.close_post", values)

