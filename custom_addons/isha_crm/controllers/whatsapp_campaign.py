
import json
import logging

import werkzeug

from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)

import base64

class CallCampaign(http.Controller):

	def getResponseDict(self, data):
		response = werkzeug.wrappers.Response()
		response.data = json.dumps(data)
		response.headers['Cache-Control'] = 'public, max-age=604800'
		response.headers['Access-Control-Allow-Origin'] = '*'
		response.headers['Access-Control-Allow-Methods'] = 'GET, POST'
		return response

	def getAnalyseRecList(self, campaign_sudo, **post):
		true = True
		false = False
		null = None
		errors = set()
		analyse_recs = []
		# print(post)
		_logger.info('Whatsapp Analyser Update ' + post['analyseResult'])
		for x in eval(post['analyseResult']):
			if x.get('error', False):
				errors.add('\n\n'.join(x['replies']))
			analyse_recs.append({
				'campaign_id': campaign_sudo.id,
				'wa_number': x['number'],
				'dnd_prediction': x['dnd_prediction'],
				'replies': '\n\n'.join(x['replies'])
			})
		if len(errors) > 0:
			_logger.error("WhatsApp Extension Analyser error \n ------- \n"+','.join(list(errors)))
		return analyse_recs

	@http.route('/whatsapp_campaign', type='http', auth='public',csrf=False,
				website=True,cors='*')
	def get_campaign_details(self, caller_token,update=False,analyse = False, version=False, **post):
		if request.httprequest.method == 'GET':
			return request.redirect('/home')
		campaign_sudo = request.env['whatsapp.campaign'].sudo().search([('access_token', '=', caller_token)])
		if analyse and campaign_sudo.exists():
			analyse_recs = self.getAnalyseRecList(campaign_sudo, **post)
			request.env['whatsapp.campaign.analyser'].sudo().create(analyse_recs)
			campaign_sudo.write({
				'state': 'analysed'
			})
			return self.getResponseDict({'update':True,'status':'SUCCESS'})

		if update and campaign_sudo.exists():
			campaign_sudo.write({
				'messages_sent':int(post['msgCompleted']),
				'messages_errored':int(post['msgFailed']),
				'error_phone_list':post['errorList'],
				'state':'closed'
			})
			return self.getResponseDict({'update':True,'status':'SUCCESS'})

		if not campaign_sudo.exists() or not campaign_sudo.state == 'open':
			error = {"error":True,"status":"Unauthorized Access."}
			return self.getResponseDict(error)

		if not version or version != request.env['ir.config_parameter'].sudo().get_param('isha_crm.wa_extension_version'):
			error = {"error": True, "status": "version_error"}
			return self.getResponseDict(error)

		if campaign_sudo.campaign_outdated:
			error = {"error": True, "status": "campaign_outdated"}
			return self.getResponseDict(error)

		msg_dict = {
			'error':False,
			'numbers':campaign_sudo.res_partner_phones,
			'message':'`'+campaign_sudo.whatsapp_message+'`',  # backticks to enable Template literals in JS
			'totalMessages':campaign_sudo.valid_phones,
			'imgData': base64.b64encode(campaign_sudo.image).decode('ascii') if campaign_sudo.image else None,
		}
		return self.getResponseDict(msg_dict)


	@http.route('/whatsapp_campaign/analyse', type='http', auth='public',csrf=False,
				website=True,cors='*')
	def get_campaign_analyse_handle(self, caller_token,update=False,analyse = False, request_token=False,version=False,**post):
		if request.httprequest.method == 'GET':
			return request.redirect('/home')
		if update or not request_token:
			error = {"error":True,"status":"Unauthorized Access."}
			return self.getResponseDict(error)

		caller_token_parsed = caller_token.split(request_token+'-')[1]
		campaign_sudo = request.env['whatsapp.campaign'].sudo().search([('access_token', '=', caller_token_parsed)])

		if not campaign_sudo.exists():
			error = {"error":True,"status":"Unauthorized Access."}
			return self.getResponseDict(error)

		if analyse and campaign_sudo.exists():
			analyse_recs = self.getAnalyseRecList(campaign_sudo, **post)
			request.env['whatsapp.campaign.analyser'].sudo().create(analyse_recs)
			campaign_sudo.write({
				'state': 'analysed'
			})
			return self.getResponseDict({'update':True,'status':'SUCCESS'})

		if not version or version != request.env['ir.config_parameter'].sudo().get_param('isha_crm.wa_extension_version'):
			error = {"error": True, "status": "version_error"}
			return self.getResponseDict(error)

		if campaign_sudo.campaign_outdated:
			error = {"error": True, "status": "campaign_outdated"}
			return self.getResponseDict(error)

		msg_dict = {
			'error':False,
			'numbers':campaign_sudo.res_partner_phones,
			'message':'`'+campaign_sudo.whatsapp_message+'`',  # backticks to enable Template literals in JS
			'totalMessages':campaign_sudo.valid_phones
		}
		return self.getResponseDict(msg_dict)