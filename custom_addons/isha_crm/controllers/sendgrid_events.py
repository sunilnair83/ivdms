import datetime
import json
import logging
import sys

from confluent_kafka import Producer

from odoo import http, fields
from odoo.http import request
from ...isha_base.configuration import Configuration

_logger = logging.getLogger(__name__)

kafka_config = Configuration('KAFKA')
crm_config = Configuration('CRM')
p = Producer({
	'bootstrap.servers': kafka_config['KAFKA_BOOTSTRAP_SERVERS'],
	'security.protocol': kafka_config['KAFKA_SECURITY_PROTOCOL'],
	'ssl.ca.location': kafka_config['KAFKA_SSL_CA_CERT_PATH'],
	'ssl.certificate.location': kafka_config['KAFKA_SSL_CERT_PATH'],
	#    'ssl.key.location': 'key.pem'
})


class SendgridEvents(http.Controller):
	@http.route([
		'/api/custom/isha_crm/sendgrid/events'
	], auth="none", type='http', methods=['POST'], csrf=False)
	def sendgrid_event(self, **kw):
		_logger.info("Sendgrid event data: " + request.httprequest.data.decode("utf-8") )

	@http.route([
		'/api/custom/isha_crm/whatsapp/yellow_messenger/callback'
	], auth="none", type='json', methods=['POST'], csrf=False)
	def whatsapp_response(self, **kw):
		_logger.info("Whatsapp response data: " + request.httprequest.data.decode("utf-8"))
		stats = json.loads(request.httprequest.data.decode("utf-8"))
		trace = request.env['mailing.trace'].sudo().search([('message_id', '=', stats['msgId'])])
		if stats['event']['status'] == 'read':
			trace.write({
				'opened': fields.Datetime.now(),
				'state': 'opened'
			})
		elif stats['event']['status'] == 'delivered' and trace.state != 'opened':
			trace.write({
				'sent': fields.Datetime.now(),
				'state': 'sent'
			})
		elif stats['event']['status'] == 'failed':
			trace.write({
				'exception': fields.Datetime.now(),
				'state': 'exception'
			})

		# p.produce(crm_config['CRM_KAFKA_WHATSAPP_YELLOW_DELIVERY_STATS_TOPIC'], request.httprequest.data)
		# p.flush(3)


