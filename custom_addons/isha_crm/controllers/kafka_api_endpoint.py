import datetime
import json
import logging

from odoo.addons.muk_rest import tools

import odoo
from odoo.http import request

_logger = logging.getLogger(__name__)
null = None

def converter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()


class KafkaApiEndpoint(odoo.http.Controller):

    @odoo.http.route([
        '/api/kafka/iecmegapgm'
    ], auth="none", type='json', methods=['POST'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def ieco_processor(self, **kw):
        src_msg = json.dumps(request.params.get('result'))
        _logger.info(src_msg)
        ieoc_rec = request.env['kafka.errors'].sudo().create({'src_msg': src_msg, 'topic': 'iecomegapgm', 'processor': 'iecmegapgm',
                   'process_msg': True})
        res = {
            "timestamp":request.params.get('meta').get('timestamp',None),
            "prsId":request.params.get('result').get('prsId',None)
        }
        return res

    @odoo.http.route([
        '/api/kafka/outreach_ishanga_reg'
    ], auth="none", type='json', methods=['POST'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def outreach_ishanga_reg_processor(self, **kw):
        src_msg = json.dumps(request.params.get('result'))
        ieoc_rec = request.env['kafka.errors'].sudo().create({'src_msg': src_msg, 'topic': 'uat_outreach_ishanga_reg', 'processor': 'outreach_ishanga_reg',
                   'process_msg': True})
        res = {
            "timestamp":request.params.get('meta').get('timestamp',None),
            "prsId":request.params.get('result').get('prsId',None)
        }
        return res
