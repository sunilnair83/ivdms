import base64
import json
import traceback

from odoo.tools import config
import werkzeug
import datetime
import logging
from odoo import _, exceptions, http
from odoo.http import request
from odoo.tools import consteq
from odoo.tools import ustr
from ..models.email_marketing import dynamic_mail_res_partner_mapper
from ..models.mail_activity_override import _unsubscribe_token, transform_mail_body_links
from odoo.addons.mail.models import mail_template
from odoo.addons.muk_rest import tools

_logger = logging.getLogger(__name__)

from treelib import Tree, Node

true=True
false=False

def len_child_nodes(node):
	return len(node['children']) if 'children' in node else 0

def to_dict_custom(self,check, nid=None, key=None, sort=True, reverse=False, with_data=False):
	"""Transform the whole tree into a dict.
	[{ "id": "0", "text": "node-0", "children": [{ "id": "0-0", "text": "node-0-0", "children": [{ "id": "0-0-0", "text": "node-0-0-0" }, { "id": "0-0-1", "text": "node-0-0-1" }, { "id": "0-0-2", "text": "node-0-0-2" }] }, { "id": "0-1", "text": "node-0-1", "children": [{ "id": "0-1-0", "text": "node-0-1-0" }, { "id": "0-1-1", "text": "node-0-1-1" }, { "id": "0-1-2", "text": "node-0-1-2" }] }, { "id": "0-2", "text": "node-0-2", "children": [{ "id": "0-2-0", "text": "node-0-2-0" }, { "id": "0-2-1", "text": "node-0-2-1" }, { "id": "0-2-2", "text": "node-0-2-2" }] }] }, { "id": "1", "text": "node-1", "children": [{ "id": "1-0", "text": "node-1-0", "children": [{ "id": "1-0-0", "text": "node-1-0-0" }, { "id": "1-0-1", "text": "node-1-0-1" }, { "id": "1-0-2", "text": "node-1-0-2" }] }, { "id": "1-1", "text": "node-1-1", "children": [{ "id": "1-1-0", "text": "node-1-1-0" }, { "id": "1-1-1", "text": "node-1-1-1" }, { "id": "1-1-2", "text": "node-1-1-2" }] }, { "id": "1-2", "text": "node-1-2", "children": [{ "id": "1-2-0", "text": "node-1-2-0" }, { "id": "1-2-1", "text": "node-1-2-1" }, { "id": "1-2-2", "text": "node-1-2-2" }] }] }, { "id": "2", "text": "node-2", "children": [{ "id": "2-0", "text": "node-2-0", "children": [{ "id": "2-0-0", "text": "node-2-0-0" }, { "id": "2-0-1", "text": "node-2-0-1" }, { "id": "2-0-2", "text": "node-2-0-2" }] }, { "id": "2-1", "text": "node-2-1", "children": [{ "id": "2-1-0", "text": "node-2-1-0" }, { "id": "2-1-1", "text": "node-2-1-1" }, { "id": "2-1-2", "text": "node-2-1-2" }] }, { "id": "2-2", "text": "node-2-2", "children": [{ "id": "2-2-0", "text": "node-2-2-0" }, { "id": "2-2-1", "text": "node-2-2-1" }, { "id": "2-2-2", "text": "node-2-2-2" }] }] }]

	"""
	nid = self.root if (nid is None) else nid
	ntag = self[nid].tag
	if ntag.id == False:
		tree_dict = {"id":0, "text":"Subscriptions", "children": []}
	else:
		tree_dict = {"id": ntag.id, "text": ntag.name, "children": []}
	if with_data:
		tree_dict[ntag]["data"] = self[nid].data

	if self[nid].expanded:
		queue = [self[i] for i in self[nid].fpointer]
		key = (lambda x: x) if (key is None) else key
		if sort:
			queue.sort(key=key, reverse=reverse)

		for elem in queue:
			tree_dict["children"].append(
				to_dict_custom(self,check,elem.identifier, with_data=with_data, sort=sort, reverse=reverse))
		if len(tree_dict["children"]) == 0:
			tree_dict = {"id":self[nid].tag.id,"text":self[nid].tag.name}
		else:
			tree_dict['children'].sort(key=len_child_nodes,reverse=True)
		return tree_dict

class MassMailControllerCustom(http.Controller):

	@http.route('/mail/track/<int:mass_mailing_id>/<int:res_id>/blank.gif', type='http', auth='public')
	def track_mail_open_custom(self, mass_mailing_id,res_id, **post):
		""" Email tracking. """
		request.env['mailing.trace'].sudo().set_opened_custom(mass_mailing_id,res_id)
		response = werkzeug.wrappers.Response()
		response.mimetype = 'image/gif'
		response.data = base64.b64decode(b'R0lGODlhAQABAIAAANvf7wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==')

		return response

	@http.route('/r/<string:code>/ccm/<int:mass_mailing_id>/<int:res_id>', type='http', auth="public")
	def link_tracer_custom(self, code, mass_mailing_id,res_id, **post):
		# don't assume geoip is set, it is part of the website module
		# which mass_mailing doesn't depend on
		country_code = request.session.get('geoip', False) and request.session.geoip.get('country_code', False)
		mail_trace = request.env['mailing.trace'].sudo().set_clicked_custom(mass_mailing_id,res_id)
		request.env['link.tracker.click'].sudo().add_click_custom(
			code,
			ip=request.httprequest.remote_addr,
			country_code=country_code,
			mailing_trace_id=mail_trace.id
		)
		return werkzeug.utils.redirect(request.env['link.tracker'].get_url_from_code(code), 301)

	def _valid_unsubscribe_token(self, mailing_id, res_id, email, token):
		# _unsubscribe_token(secret, dbname, mailing_id, res_id, email)
		if not (mailing_id and res_id and email and token):
			return False
		mailing = request.env['mailing.mailing'].sudo().browse(mailing_id)
		return consteq(mailing._unsubscribe_token(res_id, email), token)


	def check_authentication(self, mailing_id, res_id, email, token):
		mailing_id = mailing_id and int(mailing_id)
		mailing = request.env['mailing.mailing'].sudo().browse(mailing_id)
		if mailing.exists():
			res_id = res_id and int(res_id)
			if not self._valid_unsubscribe_token(mailing_id, res_id, email, str(token)):
				return {'status':'unauthorized'}

			return {'status':'authorized','mailing':mailing}

		return {'status':'unauthorized'}


	def get_contact_from_res_id(self,mailing,res_id):
		res_id = res_id and int(res_id)
		if mailing.mailing_model_real == 'res.partner':
			contact = request.env['res.partner'].sudo().browse(res_id)
		else:
			txn_rec = request.env[mailing.mailing_model_real].sudo().browse(res_id)
			partner_id = txn_rec[dynamic_mail_res_partner_mapper.get(mailing.mailing_model_real)].id
			contact = request.env['res.partner'].sudo().browse(partner_id)
		return contact


	@http.route(['/mail/isha_mailing/<int:mailing_id>/unsubscribe'], type='http', website=True, auth='public')
	def mailing(self, mailing_id, email=None, res_id=None, token="", **post):
		auth_status = self.check_authentication(mailing_id,res_id,email,token)
		if auth_status['status'] == 'authorized':
			mailing = auth_status['mailing']

			return request.render('isha_crm.page_unsubscribe', {
				'contacts': True,
				'token':token,
				'email': email,
				'mailing_id': mailing_id,
				'is_pgm_upd':mailing.is_pgm_upd,
				'res_id': res_id,
				'category':mailing.dynamic_mailing_list.subscription_id,
				'show_blacklist_button': request.env['ir.config_parameter'].sudo().get_param(
					'mass_mailing.show_blacklist_buttons'),
			})

		return request.redirect('/web')

	@http.route(['/mail/isha_mailing/<int:mailing_id>/manage'], type='http', website=True, auth='public')
	def mailing_manage(self, mailing_id, email=None, res_id=None, token="", **post):
		auth_status = self.check_authentication(mailing_id,res_id,email,token)
		if auth_status['status'] == 'authorized':
			mailing = auth_status['mailing']
			res_id = res_id and int(res_id)
			contact = self.get_contact_from_res_id(mailing,res_id)
			unsubs = request.env['isha.mailing.unsubscriptions'].sudo().search([('unsubscription_email','=',email)]).mapped('subscription_id')
			subs_list = list(set(contact.ml_subscription_ids.ids) - set(unsubs.ids))

			subscription_category = request.env['isha.mailing.subscriptions'].sudo()\
				.search([('is_pgm_update', '=', False), '|', ('is_public', '=', True), ('id', 'in', contact.ml_subscription_ids.ids)])

			subs_tree = Tree()
			subs_tree.create_node(request.env['isha.mailing.subscriptions'], 0)
			for category in subscription_category:
				categories = []
				current = category
				while current:
					categories.append(current)
					current = current.parent_id
				categories = list(reversed(categories))
				if not subs_tree.contains(categories[0].id):
					subs_tree.create_node(categories[0], categories[0].id, 0)
				for i in range(1,len(categories)):
					if not subs_tree.contains(categories[i].id):
						subs_tree.create_node(categories[i], categories[i].id, categories[i-1].id)

			js_tree = json.dumps(to_dict_custom(subs_tree,contact.ml_subscription_ids.ids))
			print(js_tree)
			print(subs_list)
			return request.render('isha_crm.page_manage_subscribe', {
				'contact': contact,
				'token':token,
				'email': email,
				'mailing_id': mailing_id,
				'subscription_category':subscription_category,
				'subslist':subs_list,
				'res_id': res_id,
				'js_tree':js_tree,
				'show_blacklist_button': request.env['ir.config_parameter'].sudo().get_param(
					'mass_mailing.show_blacklist_buttons'),
			})

		return request.redirect('/web')

	@http.route('/mail/isha_mailing/update_subscription', type='http', auth='public', website=True)
	def update_subscription(self, mailing_id,res_id, token,**post):
		email = post['email']
		auth_status = self.check_authentication(mailing_id,res_id,email,token)
		if auth_status['status'] == 'authorized':
			mailing = auth_status['mailing']
			res_id = res_id and int(res_id)
			contact = self.get_contact_from_res_id(mailing,res_id)
			unsubs = request.env['isha.mailing.unsubscriptions'].sudo().with_context(mail_create_nosubscribe=True).search([('unsubscription_email','=',email)]).mapped('subscription_id')
			subs_list = list(set(contact.ml_subscription_ids.ids) - set(unsubs.ids))

			subscription_category = request.env['isha.mailing.subscriptions'].sudo().search(['|',('is_public','=',True),('id','in',contact.ml_subscription_ids.ids)])

			subscriptions = []
			for x in eval(post['active_subs']):
				if x['id'] in subscription_category.ids:
					subscriptions.append(x['id'])
			print(post['active_subs'])
			# for x in post:
			# 	if 'subs_ids_' in x:
			# 		subscriptions.append(int(post[x]))

			# Handle opt-in
			if post.get('user_action', '') != 'blacklist':
				contact.sudo().with_context(mail_create_nosubscribe=True).write({
					'ml_subscription_ids': [(4,x) for x in subscriptions]
				})
				unsubs = request.env['isha.mailing.unsubscriptions'].sudo().search([('subscription_id','in',subscriptions),
																		   ('unsubscription_email','=',email)])
				unsubs.write({'active':False,'reason':'Manual Opt-In by '+str(contact.name)+ ' <' +str(contact.id) + '>','mailing_id':mailing.id})
				unsubs.filtered(lambda x: x.partner_id.id == contact.id).write({'opt_in_date':datetime.datetime.now()})


			# Handle opt-out
			if post.get('user_action','') == 'blacklist': # Add to blocklist emails
				request.env['mail.blacklist'].sudo()._add_rec(email, 'unsubs_all')
				opt_outs = subscription_category.ids
				reason = 'OPT-OUT FROM ALL LISTS by ' + str(contact.name)+ ' <' +str(contact.id) + '>'
			else:
				opt_outs = list(set(subscription_category.ids) - set(subscriptions))
				reason = 'OPT-OUT by ' + str(contact.name) + ' <' +str(contact.id) + '>'
			opt_out_rec_list = []
			for x in opt_outs:
				existing_rec = request.env['isha.mailing.unsubscriptions'].sudo().with_context(active_test=False,mail_create_nosubscribe=True).search(
					[('subscription_id','=',int(x)),('unsubscription_email','=',email),('partner_id','=',contact.id)]
				)
				if existing_rec:
					existing_rec.write({'unsubscription_date':datetime.datetime.now(),'active':True, 'reason':reason,'mailing_id':mailing.id})
				else:
					opt_out_rec_list.append({
						'partner_id': contact.id,
						'model': mailing.mailing_model_real,
						'res_id': res_id,
						'subscription_id':x,
						'mailing_id':mailing.id,
						'unsubscription_date':datetime.datetime.now(),
						'unsubscription_email':email,
						'reason':reason
					})
			if opt_out_rec_list:
				request.env['isha.mailing.unsubscriptions'].sudo().with_context(mail_create_nosubscribe=True).create(opt_out_rec_list)

			return request.render('isha_crm.page_unsubscribed',{
				'contact': True,
				'token':token,
				'email': email,
				'mode': 'upd',
				'mailing_id': mailing_id,
				'res_id': res_id,
				'show_blacklist_button': request.env['ir.config_parameter'].sudo().get_param(
					'mass_mailing.show_blacklist_buttons'),
			})

		return request.render('isha_crm.page_display_updates')


	@http.route('/mail/isha_mailing/unsubscribe', type='http', auth='public', website=True)
	def unsubscribe(self, mailing_id, email, res_id, token,**kwargs):
		auth_status = self.check_authentication(mailing_id,res_id,email,token)
		if auth_status['status'] == 'authorized':
			mailing = auth_status['mailing']
			res_id = res_id and int(res_id)
			mode = 'unsubs'
			contact = self.get_contact_from_res_id(mailing,res_id)
			# Handle opt-out
			if kwargs.get('user_action','') == 'blacklist': # Need to unsubscribe from all the mailing lists
				request.env['mail.blacklist'].sudo()._add_rec(email,'unsubs_all')
				mode = 'upd'
				subscription_category = request.env['isha.mailing.subscriptions'].sudo().search(
					['|', ('is_public', '=', True), ('id', 'in', contact.ml_subscription_ids.ids)])
				opt_outs = subscription_category.ids
				reason = 'OPT-OUT FROM ALL LISTS by ' + str(contact.name) + ' <' + str(contact.id) + '>'
				opt_out_rec_list = []
				for x in opt_outs:
					existing_rec = request.env['isha.mailing.unsubscriptions'].sudo().with_context(active_test=False,
					   mail_create_nosubscribe=True).search([('subscription_id', '=', int(x)), ('unsubscription_email', '=', email),('partner_id', '=', contact.id)])
					if existing_rec:
						existing_rec.write(
							{'unsubscription_date': datetime.datetime.now(), 'active': True, 'reason': reason,'mailing_id': mailing.id})
					else:
						opt_out_rec_list.append({
							'partner_id': contact.id,
							'model': mailing.mailing_model_real,
							'res_id': res_id,
							'subscription_id': x,
							'mailing_id': mailing.id,
							'unsubscription_date': datetime.datetime.now(),
							'unsubscription_email': email,
							'reason': reason
						})
						if opt_out_rec_list:
							request.env['isha.mailing.unsubscriptions'].sudo().with_context(mail_create_nosubscribe=True).create(
								opt_out_rec_list)
			else:
				existing_rec = request.env['isha.mailing.unsubscriptions'].sudo().with_context(active_test=False).search(
						[('subscription_id','=',mailing.dynamic_mailing_list.subscription_id.id),('unsubscription_email','=',email),('partner_id','=',contact.id)]
					)
				if existing_rec:
					existing_rec.write({'unsubscription_date':datetime.datetime.now(),
										'active':True,'mailing_id': mailing.id,
										'reason':kwargs.get('reason','')})
				else:
					rec_dict = {
						'partner_id':contact.id,
						'model': mailing.mailing_model_real,
						'res_id': res_id,
						'mailing_id':mailing.id,
						'subscription_id': mailing.dynamic_mailing_list.subscription_id.id,
						'unsubscription_date': datetime.datetime.now(),
						'unsubscription_email': email,
						'active':True,
						'reason':kwargs.get('reason','')
					}
					request.env['isha.mailing.unsubscriptions'].sudo().with_context(mail_create_nosubscribe=True).create(rec_dict)
			return request.render('isha_crm.page_unsubscribed',{
				'contact': True,
				'token':token,
				'mode':mode,
				'category':mailing.dynamic_mailing_list.subscription_id,
				'email': email,
				'mailing_id': mailing_id,
				'res_id': res_id,
				'show_blacklist_button': request.env['ir.config_parameter'].sudo().get_param(
					'mass_mailing.show_blacklist_buttons'),
			})

		return request.render('isha_crm.page_display_updates')


	@http.route(['/mail/emailer/<int:mailing_id>/view'], type='http', auth='public')
	def render_email_template(self, mailing_id, email=None, res_id=None, token="", **post):
		auth_status = self.check_authentication(mailing_id, res_id, email, token)
		if auth_status['status'] == 'authorized':
			mailing = auth_status['mailing']
			res_id = res_id and int(res_id)
			mako_env = mail_template.mako_template_env


			# body_html = update_body_with_trackable_urls(request,mailing.template_id.body_html,mailing.id)
			body_html = mailing.template_id.body_html
			template = mako_env.from_string(ustr(body_html))
			email_rec = request.env[mailing.mailing_model_real].sudo().browse(res_id)
			temp_dict = {}
			temp_dict['object'] = email_rec
			body_html_rendered = template.render(temp_dict)
			base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
			base_url = base_url.rstrip('/')
			secret = request.env["ir.config_parameter"].sudo().get_param("database.secret")
			return transform_mail_body_links(base_url, config['db_name'], secret, mailing.id, email_rec, body_html_rendered, email)
		return request.render('/web')

	@http.route([
		'/api/isha_mailing/add_subscription'
	], auth="none", type='json', methods=['POST'], csrf=False)
	@tools.common.parse_exception
	@tools.common.ensure_database
	@tools.common.ensure_module()
	@tools.security.protected()
	def add_subscription(self, **kw):
		mandatory_fields = ["name", "email", "country", "language", "subs_id", "system_id"]
		for x in mandatory_fields:
			if not request.jsonrequest.get(x, False):
				return {
					'status': 'Error',
					'reason': '%s is a mandatory field' % x
				}
		country = request.env['res.country'].sudo().search([('code', '=', request.jsonrequest.get('country'))], limit=1)
		if not country:
			return {
				'status': 'Error',
				'reason': 'Country Code is invalid'
			}
		# Search if the mailing contact already exists with the same email and the list identifier
		# If so then just update all the info and append the new subs category
		existing_rec = request.env['isha.mailing.contact'].sudo().search([('email','=',request.jsonrequest.get('email').lower()),
																		('list_identifier','=', 'Manual Opt In list')])
		if existing_rec:
			if int(request.jsonrequest.get('subs_id')) in existing_rec.ml_subscription_ids.ids:
				return {
					'status': 'Info',
					'txn_status': 'Already Verified',
					'transaction_id': existing_rec.id,
					'create_date': str(existing_rec.create_date),
					'update_date': str(existing_rec.write_date)
				}
			else:
				existing_rec.sudo().write({
					'name': request.jsonrequest.get('name'),
					'country': request.jsonrequest.get('country'),
					'language': request.jsonrequest.get('language'),
					'country_id': country.id,
					'center_id': country.center_id.id,
					'language_id': request.env['res.lang'].sudo().search(
						[('iso_code', '=', request.jsonrequest.get('language'))], limit=1).id,
					'un_verified_subs_ids': [(4, int(request.jsonrequest.get('subs_id')))],
				})
				response = {
					'status': 'Success',
					'txn_status': 'Existing Record',
					'transaction_id': existing_rec.id,
					'create_date': str(existing_rec.create_date),
					'update_date': str(existing_rec.write_date)
				}
				existing_rec.send_verificaion_email(int(request.jsonrequest.get('subs_id')))
		else:
			rec = request.env['isha.mailing.contact'].sudo().create({
				'name': request.jsonrequest.get('name'),
				'email': request.jsonrequest.get('email'),
				'country': request.jsonrequest.get('country'),
				'language': request.jsonrequest.get('language'),
				'country_id': country.id,
				'center_id': country.center_id.id,
				'language_id': request.env['res.lang'].sudo().search(
					[('iso_code', '=', request.jsonrequest.get('language'))], limit=1).id,

				'un_verified_subs_ids': [(4, int(request.jsonrequest.get('subs_id')))],
				'system_id': int(request.jsonrequest.get('system_id')),
				'list_identifier': 'Manual Opt In list'
			})
			response = {
				'status': 'Success',
				'txn_status': 'New Record',
				'transaction_id': rec.id,
				'create_date': str(rec.create_date),
				'update_date': str(rec.write_date)
			}
			rec.send_verificaion_email(int(request.jsonrequest.get('subs_id')))
		return response


	@http.route(['/mail/isha_mailing/<int:mailing_contact_id>/verify_subscription'], type='http', website=True, auth='public')
	def verify_subscription(self, mailing_contact_id, email=None, res_id=None, token="",db='', **post):
		if not (email and res_id and mailing_contact_id and token and db):
			return request.render('isha_crm.page_display_verification_updates',{'status':'danger','message':'Sorry the URL is broken. Please use a valid token URL!!!'})
		try:
			mailing_contact = request.env['isha.mailing.contact'].sudo().browse(int(mailing_contact_id))
			auth_status = consteq(mailing_contact._verification_token(res_id), token)
			if auth_status:
				# Check if the user has already verified the subscription
				if int(res_id) in mailing_contact.ml_subscription_ids.ids:
					return request.render('isha_crm.page_display_verification_updates', {'status': 'info',
																						 'message': 'Your subscription is already verified.'})

				# mark the subscription as verified and remove it from unverified
				mailing_contact.sudo().write(
					{
						'ml_subscription_ids': [(4, int(res_id))],
						'un_verified_subs_ids': [(3, int(res_id))]
					}
				)
				# Do match n merge if contact_id_fkey is not present
				if not mailing_contact.contact_id_fkey:
					mailing_contact.match_n_merge()

				return request.render('isha_crm.page_display_verification_updates',{'status':'success','message':'Congratulations, your subscription is now complete.'})

			else:  # Token validity has failed. Send a error page as response
				return request.render('isha_crm.page_display_verification_updates', {'status': 'danger',
																					 'message': 'Sorry the URL is broken. Please use a valid token URL!!!'})

		except Exception as ex:
			tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
			_logger.error(tb_ex)
			return request.render('isha_crm.page_display_updates')
