from odoo.http import request, content_disposition, Response
from odoo import fields, http, _, tools
import json
import logging
import json
from ..GoldenContactAdv import GoldenContactAdv
import datetime
import traceback
_logger = logging.getLogger(__name__)


class AmazonSESEvents(http.Controller):
	@http.route([
		'/api/custom/isha_crm/amazonses/mark_events'
	], auth="none", type='http', methods=['POST'], csrf=False)
	def ses_mark_event(self, **kw):
		data = json.loads(request.httprequest.data.decode("utf-8"))
		_logger.info("AmazonSES mark event data: " + json.dumps(data))
		try:
			messageType = data["Type"]
			if messageType == 'SubscriptionConfirmation':
				subscribeURL = data["SubscribeURL"]
				_logger.info("MessageType :" + messageType + "subscribeURL:" + subscribeURL)
			else:
				message = data["Message"]
				jsonData = json.loads(message)
				messageId = jsonData["mail"]["messageId"]
				notificationType = jsonData["eventType"]
				if notificationType == "Bounce":
					bounceType = jsonData["bounce"]["bounceType"]
					bounceSubType = jsonData["bounce"]["bounceSubType"]
					bouncedReceipients = jsonData["bounce"]["bouncedRecipients"]
					email_ids = []
					for rec in bouncedReceipients:
						mail_Id = tools.email_normalize(rec["emailAddress"])
						email_ids.append(mail_Id)
						diagnosticCode = rec.get("diagnosticCode")
						mailingTrace = request.env['mailing.trace'].sudo().search([('message_id', '=', messageId)])
						if mailingTrace:
							mailingTrace.with_context(mail_create_nosubscribe=True).write({'bounced': fields.Datetime.now(),'bounce_type':bounceType,'bounce_sub_type':bounceSubType,'diagnostic_code':diagnosticCode})
							for marketing_trace in mailingTrace.mapped('marketing_trace_id'):
								marketing_trace.process_event('mail_bounce')
						else:
							_logger.info("AmazonSES Trace Out of order: " + json.dumps(data))
							if '[TEST MAIL]' not in message:
								request.env['kafka.errors'].sudo().create({
									'src_msg': json.dumps(data),
									'error': 'AmazonSES Trace Out of order',
									'active': True,
									'process_msg': False,
									'processor': 'email_marketing_sqs',
									'topic': 'email_marketing_sqs'
								})
						if mail_Id:
							self.set_bounced(mail_Id,mailingTrace,'soft_bounce' if bounceType != 'Permanent' else 'bounced')
					if bounceType == 'Permanent':
						for email in email_ids:
							request.env["mail.blacklist"].sudo().with_context(mail_create_nosubscribe=True)._add_rec(email,'hard_bounce')
				if notificationType == "Complaint":
					complaintReceipients = jsonData["complaint"]["complainedRecipients"]
					complaintFeedbackType = jsonData["complaint"].get("complaintFeedbackType")
					email_ids = []
					for rec in complaintReceipients:
						mail_Id = tools.email_normalize(rec["emailAddress"])
						email_ids.append(mail_Id)
						mailingTrace = request.env['mailing.trace'].sudo().search([('message_id', '=', messageId)])
						if mailingTrace:
							mailingTrace.with_context(mail_create_nosubscribe=True).write({'complaint': fields.Datetime.now(),'compliant_feedback_type':complaintFeedbackType})
						else:
							_logger.info("AmazonSES Trace Out of order: " + json.dumps(data))
							if '[TEST MAIL]' not in message:
								request.env['kafka.errors'].sudo().create({
									'src_msg': json.dumps(data),
									'error': 'AmazonSES Trace Out of order',
									'active': True,
									'process_msg': False,
									'processor': 'email_marketing_sqs',
									'topic': 'email_marketing_sqs'
								})

						if mail_Id:
							self.set_complaints(mail_Id,mailingTrace)
					for email in email_ids:
						request.env["mail.blacklist"].sudo().with_context(mail_create_nosubscribe=True)._add_rec(email,'complaint')
				if notificationType == "Click":
					message_node = jsonData
					ip_address = message_node["click"]["ipAddress"]
					url_clicked = message_node["click"]["link"]
					click_ts = message_node["click"]["timestamp"]
					click_ts_without_tz = click_ts.split(".")[0].replace("T", " ")
					if '/unsubscribe?' not in url_clicked:  # do not consider unsubs link as click
						search_result = request.env['mailing.trace'].sudo().search([('message_id', '=', messageId)])
						if len(search_result) > 0:
							search_result.sudo().with_context(mail_create_nosubscribe=True).write({'opened': click_ts_without_tz,'clicked': click_ts_without_tz})
							for marketing_trace in search_result.mapped('marketing_trace_id'):
								marketing_trace.process_event('mail_click')
							search_result[0].sudo().with_context(mail_create_nosubscribe=True).mark_click(url_clicked, ip_address)
						else:
							_logger.info("AmazonSES Trace Out of order: " + json.dumps(data))
							if '[TEST MAIL]' not in message:
								request.env['kafka.errors'].sudo().create({
									'src_msg': json.dumps(data),
									'error': 'AmazonSES Trace Out of order',
									'active': True,
									'process_msg': False,
									'processor': 'email_marketing_sqs',
									'topic': 'email_marketing_sqs'
								})

				if notificationType == "Open":
					message_node = jsonData
					open_ts = message_node["open"]["timestamp"]
					open_ts_without_tz = open_ts.split(".")[0].replace("T", " ")
					search_result = request.env['mailing.trace'].sudo().search([('message_id', '=', messageId)])
					if len(search_result) > 0:
						search_result[0].sudo().with_context(mail_create_nosubscribe=True).write({'opened': open_ts_without_tz})
						for marketing_trace in search_result.mapped('marketing_trace_id'):
							marketing_trace.process_event('mail_open')
					else:
						_logger.info("AmazonSES Trace Out of order: " + json.dumps(data))
						if '[TEST MAIL]' not in message:
							request.env['kafka.errors'].sudo().create({
								'src_msg': json.dumps(data),
								'error': 'AmazonSES Trace Out of order',
								'active': True,
								'process_msg': False,
								'processor': 'email_marketing_sqs',
								'topic': 'email_marketing_sqs'
							})
			return json.dumps({'status':'success'})
		except Exception as ex:
			tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
			_logger.error("SNS Mark events Error\n----------\n"+json.dumps(data) + '\n----------\n' + tb_ex)


	@http.route([
		'/api/custom/isha_crm/amazonses/update_bounce'
	], auth="none", type='http', methods=['POST'], csrf=False)
	def ses_event(self, **kw):
		data = json.loads(request.httprequest.data.decode("utf-8"))
		_logger.info("AmazonSES event data: " + json.dumps(data))
		try:
			messageType = data["Type"]
			if messageType == 'SubscriptionConfirmation':
				subscribeURL = data["SubscribeURL"]
				_logger.info("MessageType :" + messageType + "subscribeURL:" + subscribeURL)
			else:
				message = data["Message"]
				jsonData = json.loads(message)
				messageId = jsonData["mail"]["messageId"]
				notificationType = jsonData["notificationType"]
				if notificationType == "Bounce":
					bounceType = jsonData["bounce"]["bounceType"]
					bounceSubType = jsonData["bounce"]["bounceSubType"]
					bouncedReceipients = jsonData["bounce"]["bouncedRecipients"]
					email_ids = []
					for rec in bouncedReceipients:
						mail_Id = tools.email_normalize(rec["emailAddress"])
						email_ids.append(mail_Id)
						diagnosticCode = rec.get("diagnosticCode")
						mailingTrace = request.env['mailing.trace'].sudo().search([('message_id', '=', messageId)])
						if mailingTrace:
							mailingTrace.with_context(mail_create_nosubscribe=True).write({'bounced': fields.Datetime.now(),'bounce_type':bounceType,'bounce_sub_type':bounceSubType,'diagnostic_code':diagnosticCode})
						if mail_Id:
							self.set_bounced(mail_Id,mailingTrace,'soft_bounce' if bounceType != 'Permanent' else 'bounced')
					if bounceType == 'Permanent':
						for email in email_ids:
							request.env["mail.blacklist"].sudo().with_context(mail_create_nosubscribe=True)._add_rec(email,'hard_bounce')

				if notificationType == "Complaint":
					complaintReceipients = jsonData["complaint"]["complainedRecipients"]
					complaintFeedbackType = jsonData["complaint"].get("complaintFeedbackType")
					email_ids = []
					for rec in complaintReceipients:
						mail_Id = tools.email_normalize(rec["emailAddress"])
						email_ids.append(mail_Id)
						mailingTrace = request.env['mailing.trace'].sudo().search([('message_id', '=', messageId)])
						if mailingTrace:
							mailingTrace.with_context(mail_create_nosubscribe=True).write({'complaint': fields.Datetime.now(),'compliant_feedback_type':complaintFeedbackType})
						if mail_Id:
							self.set_complaints(mail_Id,mailingTrace)
					for email in email_ids:
						request.env["mail.blacklist"].sudo().with_context(mail_create_nosubscribe=True)._add_rec(email,'complaint')
			return json.dumps({'status':'success'})
		except Exception as ex:
			tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
			_logger.error("SNS ERROR\n----------\n"+json.dumps(data) + '\n----------\n' + tb_ex)

	def set_bounced(self, mail_id, trace, bounce_type):
		if mail_id:
			start = datetime.datetime.now()
			if trace.model == 'res.partner':
				contacts = request.env['res.partner'].sudo().search([('email', '=', mail_id)])
				if contacts:
					for contact in contacts:
						contact.with_context(mail_create_nosubscribe=True).write({'email_validity': bounce_type})
						updated_emails = GoldenContactAdv.getEmailsDict(contact)
						contact.sudo().with_context(mail_create_nosubscribe=True).write(updated_emails)
			# TODO: Handle updates for txn tables
			# else:
			# 	rec = request.env[trace.model].sudo().search([('id','=',trace.res_id)])
			# 	rec.write({'email_validity': 'bounced'})
			end = datetime.datetime.now()
			_logger.info('######## SES Setting bounce for ' + str(mail_id)+' '+str(end-start))

	def set_complaints(self, mail_id, trace):
		if mail_id:
			start = datetime.datetime.now()
			contacts = request.env['res.partner'].sudo().search([('email', '=', mail_id)])
			if contacts:
				for contact in contacts:
					contact.with_context(mail_create_nosubscribe=True).write({'email_validity': 'complaint'})
					updated_emails = GoldenContactAdv.getEmailsDict(contact)
					contact.sudo().with_context(mail_create_nosubscribe=True).write(updated_emails)
			# TODO: Handle updates for txn tables
			# else:
			# 	rec = request.env[trace.model].sudo().search([('id','=',trace.res_id)])
			# 	rec.write({'email_validity': 'complaint'})
			end = datetime.datetime.now()
			_logger.info('######## SES Setting Compliant for ' + str(mail_id) + ' ' + str(end - start))