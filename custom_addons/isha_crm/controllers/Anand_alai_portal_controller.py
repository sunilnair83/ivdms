import urllib.parse

from odoo.addons.portal.controllers.portal import CustomerPortal

import odoo.http as http
from odoo import _
from odoo.http import request
from odoo.osv.expression import OR


class AnandAlaiPortalController(CustomerPortal):

    def _prepare_portal_layout_values(self):
        values = super(AnandAlaiPortalController, self)._prepare_portal_layout_values()
        return values

    @http.route(['/onlineanandalai'], type='http', auth="user", website=True)
    def query_anandalai_data(self, page=1, date_begin=None, date_end=None, sortby=None, search_in='record_phone', search='', **kw):
        if not request.env.user.has_group('isha_crm.group_portal_reg_search') and not request.env.user.has_group('base.group_user'):
            return request.redirect('/my/home')
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        searchbar_sortings = {
            'create_date_asc': {'label': _('Created Date (Asc)'), 'order': 'create_date'},
            'create_date_desc': {'label': _('Created Date (Desc)'), 'order': 'create_date desc'},
            'write_date_asc': {'label': _('Updated On (Asc)'), 'order': 'write_date'},
            'write_date_desc': {'label': _('Updated On (Desc)'), 'order': 'write_date desc'},
        }
        searchbar_inputs = {
            'record_phone': {'input': 'record_phone', 'label': _('Search in Phone')},
            'record_name': {'input': 'record_name', 'label': _('Search in Contact Name')},
            'record_email': {'input': 'record_email', 'label': _('Search in Email')},
            'reg_number': {'input': 'reg_number', 'label': _('Search in Registration Number')},
            # 'all': {'input': 'all', 'label': _('Search in All')},
        }

        domain = [('program_name','=','Online Ananda Alai')] if search else []
        # search
        if search and search_in:
            search_domain = []
            if search_in in ('record_email', 'all'):
                search_domain = OR([search_domain, [('record_email', 'ilike', search)]])
            if search_in in ('record_phone', 'all'):
                search_domain = OR([search_domain, [('record_phone', 'ilike', search)]])
            if search_in in ('record_name', 'all'):
                search_domain = OR([search_domain, [('record_name', 'ilike', search)]])
            if search_in in ('reg_number', 'all'):
                search_domain = OR([search_domain, [('program_schedule_info', 'ilike', '%'+search+'%')]])
            domain += search_domain

        # default sort by value
        if not sortby:
            sortby = 'create_date_desc'
        order = searchbar_sortings[sortby]['order']

        program_lead_sudo = request.env['program.lead'].sudo()

        anand_alai_recs = program_lead_sudo.search(domain, order=order, limit=5)

        anand_alai_list = []
        for rec in anand_alai_recs:
            anand_alai_list.append({
                    'id': rec.id,
                    'record_name': rec.record_name,
                    'reg_status': rec.reg_status,
                    'record_email': rec.record_email,
                    'record_phone': rec.record_phone,
                    'record_center': rec.contact_id_fkey.center_id.name,
                    'record_region': rec.contact_id_fkey.center_id.region_id.name,
                    'reg_num': rec.reg_number,
                    'zoom_url': rec.meeting_url,
                    'day_wise_attendance': rec.day_1_attendance,
                    'duration_missed': rec.day_1_duration_missed
                        })

        values.update({
            'anand_alai_recs': anand_alai_list,
            'page_name': 'Anand Alai',
            # 'pager': pager,
            'searchbar_sortings': searchbar_sortings,
            'searchbar_inputs': searchbar_inputs,
            'search_in': search_in,
            'sortby': sortby,
            # 'archive_groups': archive_groups,
            'default_url': '/onlineanandalai',
        })
        return request.render("isha_crm.portal_anandalai_view", values)

    @http.route(['/onlineanandalai/participant/details_view/<int:reg_num>'], type='http', auth="user", website=True)
    def participant_details_view(self, reg_num, category='', search='', **post):
        if not request.env.user.has_group('isha_crm.group_portal_reg_search') and not request.env.user.has_group('base.group_user'):
            return request.redirect('/my/home')
        context = dict(request.env.context or {})
        program_lead_obj = request.env['program.lead']
        context.update(active_id=reg_num)

        anandalai_data_list = []
        anandalai_data = program_lead_obj.sudo().browse(int(reg_num))
        base_url = ''
        zoom_url = anandalai_data.meeting_url
        # if send Email button is clicked
        if post.get('send_email'):
            template = request.env.ref('isha_crm.anandalai_send_email_template')
            values = template.sudo().generate_email(anandalai_data.id, fields=None)
            values['body_html'] = values['body_html'].replace('$$record_name', anandalai_data.record_name)
            values['body_html'] = values['body_html'].replace('$$base_url', zoom_url)
            mail_mail_obj = request.env['mail.mail']
            msg_id = mail_mail_obj.sudo().create(values)
            msg_id.send(False)

        whatsapp_msg = 'Namaskaram ' + str(anandalai_data.record_name) + \
                       ',\n\nPlease use the below link to join the session on all days for the Anand Alai Program\n\n' + \
                       str(zoom_url) + '\n\nYour Session Date and Time: ' + str(anandalai_data.bay_name) + \
                       '\n\nIf you are joining from phone, please make sure to download the latest version of Zoom App from the App Store' + \
                       '\n\nFor any assistance please click https://isha.co/anandabot' + \
                       '\n\nPranam\nIsha Volunteer'
        if anandalai_data.contact_id_fkey.whatsapp_number:
            code = anandalai_data.contact_id_fkey.whatsapp_country_code
            if not code:
                code = ''
            params = {'phone': code + anandalai_data.contact_id_fkey.whatsapp_number, 'text': whatsapp_msg}
            url_params = urllib.parse.urlencode(params)
            whatsapp_url = "https://api.whatsapp.com/send?" + url_params
        else:
            whatsapp_url = '/onlineanandalai/participant/details_view/' + str(anandalai_data.id)

        if anandalai_data.contact_id_fkey.phone:
            code = anandalai_data.contact_id_fkey.phone_country_code
            if not code:
                code = ''
            sms_message = 'Namaskaram,'+\
                        '\nAnand Alai program invitation.'+\
                        '\nPlease use this link to join :' + anandalai_data.meeting_url +\
                        '\nPranam'
            sms_url = 'sms://' + code + anandalai_data.contact_id_fkey.phone
        else:
            sms_url = '/onlineanandalai/participant/details_view/' + str(anandalai_data.id)

        if anandalai_data.record_email:
            email_url = '/onlineanandalai/participant/details_view/' + str(anandalai_data.id) + '?send_email=true'
        else:
            email_url = '/onlineanandalai/participant/details_view/' + str(anandalai_data.id)

        anandalai_data_list.append({
            'id': anandalai_data.id,
            'record_name': anandalai_data.record_name,
            'record_email': anandalai_data.record_email,
            'record_phone': anandalai_data.record_phone,
            'whatsapp_number': anandalai_data.contact_id_fkey.whatsapp_number,
            'record_center': anandalai_data.contact_id_fkey.center_id.name,
            'record_region': anandalai_data.contact_id_fkey.center_id.region_id.name,
            'meeting_number': anandalai_data.meeting_number,
            'bay_name': anandalai_data.bay_name,
            'check_in_status': anandalai_data.check_in_status,
            'reg_status': anandalai_data.reg_status,
            'reg_num': anandalai_data.reg_number,
            'zoom_url': anandalai_data.meeting_url,
            'day_1_attendance': anandalai_data.day_1_attendance,
            'day_2_attendance': anandalai_data.day_2_attendance,
            'day_3_attendance': anandalai_data.day_3_attendance,
            'day_4_attendance': anandalai_data.day_4_attendance,
            'day_5_attendance': anandalai_data.day_5_attendance,
            'day_1_duration_missed': anandalai_data.day_1_duration_missed,
            'day_2_duration_missed': anandalai_data.day_2_duration_missed,
            'day_3_duration_missed': anandalai_data.day_3_duration_missed,
            'day_4_duration_missed': anandalai_data.day_4_duration_missed,
            'day_5_duration_missed': anandalai_data.day_5_duration_missed,
            'whatsapp_url': whatsapp_url,
            'sms_url': sms_url,
            'email_url': email_url,
            'whatsapp_message': whatsapp_msg,
            'duration_missed': anandalai_data.day_1_duration_missed
                        })

        return http.request.render('isha_crm.participant_details_view', {
            'anand_alai_recs': anandalai_data_list
        })

