from datetime import datetime

from .. import ContactProcessor


class AttendeesProcessor:
    system_id = 53

    def getGoldenFormat(self, src_msg, iso2, correction):
        src_msg = ContactProcessor.replaceEmptyString(src_msg)

        influencer_type = None

        if 'Influencer Type' in src_msg:
            influencer_type = src_msg.get('Influencer Type').lower()

        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': src_msg.get('local_contact_id'),
            'active': True,
            'name': src_msg.get('Name'), 'display_name':  src_msg.get('Name'), 'prof_dr': None, 'phone_country_code': src_msg.get('phone_country_code'),
            'phone': src_msg.get('Phone Number'), 'phone2': None,'phone3': None, 'phone4': None, 'whatsapp_number': src_msg.get('WhatsApp Number'),
            'whatsapp_country_code': src_msg.get('whatsapp_country_code'),
            'email': src_msg.get('Email'), 'email2': None,'email3': None,
            'street': None,  'street2': None, 'work_company_name': None,
            'city': None, 'state': None, 'country': src_msg.get('Country'), 'zip': None,
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None, 'influencer_type': influencer_type,
            'gender': None, 'occupation': src_msg.get('Occupation'), 'marital_status': None, 'dob': None,
            'aadhaar_no': None, 'pan_no': None, 'passport_no': None, 'education': None,
            'nationality': None, 'deceased': None, 'contact_type': 'INDIVIDUAL', 'companies': None,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'work_designation': None,
            'sso_id': src_msg.get('sso_id')

        }

        return ContactProcessor.validateValues(odooRec, iso2, correction)

    def getGoldenFormatForTxn(self, ENV, attendees_txn):

        event_id_rec = ENV['pr.events'].search([('event_name','=',attendees_txn.get('event_name'))])
        event_id = event_id_rec.id
        bay_id_rec = ENV['bay.name'].search([('bay_name', '=', attendees_txn.get('Bay Name'))]).id
        bay_ids = event_id_rec.bay_names.ids
        if event_id is False:
            raise ValueError("Please mention an existing event Name")
        if attendees_txn.get('Bay Name') and bay_id_rec not in bay_ids:
            raise ValueError("Please enter a proper bay name for the event")

        return {
            'active': True,
            'event_id_fkey': event_id,
            'event_name': attendees_txn.get('event_name'),
            'local_trans_id': attendees_txn.get('local_contact_id'),
            'attendees_name': attendees_txn.get('Name'),
            'bay_name': attendees_txn.get('Bay Name'),
            'attendance_notes': attendees_txn.get('Attendance notes')
        }

    def process_message(self, src_msg, env, metadata):
        # logger.debug(msg)

        iso2 = metadata['iso2']
        correction = metadata['correction']

        msg_dict = self.getGoldenFormat(src_msg, iso2, correction)
        txn_dict = self.getGoldenFormatForTxn(env, src_msg)

        try:
            contact = env['res.partner'].sudo().create_internal(msg_dict)
            txn_dict['contact_id_fkey'] = contact[0].id
            flag_dict = self.create_attendees_internal(env, txn_dict)
            env.cr.commit()
            return flag_dict
        finally:
            pass

    def create_attendees_internal(self, ENV, src_rec):
        AttendeesModel = ENV['event.attendees']

        rec = AttendeesModel.search([('event_id_fkey','=',src_rec['event_id_fkey']),('contact_id_fkey','=',src_rec['contact_id_fkey'])])
        if len(rec) != 0:
            src_rec['active'] = False
            rec = AttendeesModel.create(src_rec)
        elif len(rec) == 0:
            rec = AttendeesModel.create(src_rec)

        flag_dict = {'flag': True, 'rec': rec}
        return flag_dict
