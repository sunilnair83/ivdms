from datetime import datetime

from .. import ContactProcessor


class AthithiContactsProcessor:
    system_id = 45

    def process_message(self, src_msg_dict, env, metadata):
        # env['res.partner'].create(src_msg_dict)
        iso2 = metadata['iso2']
        correction = metadata['correction']

        contactMsgDict = self.getContactFormat(src_msg_dict, env, iso2, correction)
        if src_msg_dict['id']:
            contactMsgDict.pop('local_contact_id', None)
            contactMsgDict.pop('system_id', None)
            contactMsgDict.pop('phone_src', None)
            contactMsgDict.pop('phone2_src', None)
            contactMsgDict.pop('phone3_src', None)
            contactMsgDict.pop('phone4_src', None)

            contact = env['res.partner'].browse(int(contactMsgDict['id']))
            contact.write(contactMsgDict)

        else:
            env['res.partner'].sudo().create_internal(contactMsgDict)

    def getContactFormat(self, src_msg, env, iso2, correction):

        company_id = None
        if src_msg['company']:
            companies = env['isha.company'].search([('name', '=', src_msg['company'])])
            if companies:
                company_id = companies[0].id
            else:
                company_id = env['isha.company'].create({
                    'name': src_msg['company'],
                    'industry': src_msg['industry'],
                    'sector': src_msg['sector']
                }).id

        company_id2 = None
        if src_msg['company2_name']:
            companies = env['isha.company'].search([('name', '=', src_msg['company2_name'])])
            if companies:
                company_id2 = companies[0].id
            else:
                company_id2 = env['isha.company'].create({
                    'name': src_msg['company2_name'],
                    'industry': src_msg['industry2'],
                    'sector': src_msg['sector2']
                }).id

        company_id3 = None
        if src_msg['company3_name']:
            companies = env['isha.company'].search([('name', '=', src_msg['company3_name'])])
            if companies:
                company_id3 = companies[0].id
            else:
                company_id3 = env['isha.company'].create({
                    'name': src_msg['company3_name'],
                    'industry': src_msg['industry3'],
                    'sector': src_msg['sector3']
                }).id

        primary_poc_user_id = None
        if src_msg['primary_poc']:
            users = env['res.users'].search([('id', '=', int(src_msg['primary_poc']))])
            if users:
                if not users[0].has_group('isha_crm.group_athithi_poc'):
                    raise Exception('Primary POC user id ' + str(users[0].id) + ' does not have Athithi POC group assigned')
                else:
                    primary_poc_user_id = users[0].id
            else:
                raise Exception('Primary POC user with user id ' + str(src_msg['primary_poc']) + ' not found')

        secondary_poc_user_id = None
        if src_msg['secondary_poc']:
            users = env['res.users'].search([('id', '=', int(src_msg['secondary_poc']))])
            if users:
                if not users[0].has_group('isha_crm.group_athithi_poc'):
                    raise Exception('Secondary POC user id ' + str(users[0].id) + ' does not have Athithi POC group assigned')
                else:
                    secondary_poc_user_id = users[0].id
            else:
                raise Exception('Secondary POC user with user id ' + str(src_msg['secondary_poc']) + ' not found')

        center_id = None
        if src_msg['center_name']:
            center = env['isha.center'].search([('name', '=', src_msg['center_name'])])
            if center:
                center_id = center[0].id
            else:
                center_id = None

        if src_msg['influencer_type'] == 'Influencer':
            src_msg['influencer_type'] = 'influencer'
        elif src_msg['influencer_type'] == 'Pre-Influencer':
            src_msg['influencer_type'] = 'pre_influencer'
        elif src_msg['influencer_type'] == 'Relative':
            src_msg['influencer_type'] = 'relative'
        elif src_msg['influencer_type'] == 'Unassigned':
            src_msg['influencer_type'] = 'unassigned'
        elif src_msg['influencer_type'] == 'Do-Not-Know':
            src_msg['influencer_type'] = 'do_not_know'

        if src_msg['dob']:
            if src_msg['dob'].__contains__('-'):
                src_msg['dob'] = datetime.strptime(src_msg['dob'], '%d-%b-%Y').strftime('%Y-%m-%d')
            else:
                src_msg['dob'] = datetime.strptime(src_msg['dob'], '%m-%d-%Y').strftime('%Y-%m-%d')
        else:
            src_msg['dob'] = None
        src_msg['gender'] = ContactProcessor.getgender(src_msg['gender'])
        src_msg['zonal_category'] = src_msg['zonal_category'].lower() if src_msg['zonal_category'] else None
        src_msg['id'] = src_msg['ishangam_id']
        src_msg['system_id'] = self.system_id
        src_msg['phone_type'] = src_msg['phone_type'].lower() if src_msg['phone_type'] else None
        src_msg['phone2_type'] = src_msg['phone2_type'].lower() if src_msg['phone2_type'] else None
        src_msg['phone3_type'] = src_msg['phone3_type'].lower() if src_msg['phone3_type'] else None
        src_msg['phone4_type'] = src_msg['phone4_type'].lower() if src_msg['phone4_type'] else None
        src_msg['active'] = True
        # src_msg['prof_dr'] = src_msg['title']
        src_msg['display_name'] = src_msg['name']
        # src_msg['designation1'] = src_msg['designation1']
        src_msg['company1'] = company_id
        src_msg['company2'] = company_id2
        src_msg['company3'] = company_id3
        src_msg['center_id'] = center_id
        src_msg['primary_poc'] = primary_poc_user_id
        src_msg['secondary_poc'] = secondary_poc_user_id
        src_msg['category'] = src_msg['relationship_status'].lower() if src_msg['relationship_status'] else None
        src_msg['deceased'] = False
        src_msg['contact_type'] = 'INDIVIDUAL'
        src_msg['companies'] = None
        src_msg['aadhaar_no'] = None
        src_msg['pan_no'] = None
        src_msg['passport_no'] = None
        src_msg['dnd_phone'] = None
        src_msg['dnd_email'] = None
        src_msg['dnd_postmail'] = None
        src_msg['sso_id'] = None
        # src_msg['wikipedia_link'] = src_msg['Wikipedia']
        # src_msg['facebook_id'] = src_msg['Facebook Link']
        insta_followers = int(src_msg['insta_followers']) if src_msg['insta_followers'] else None
        twitter_followers = int(src_msg['twitter_followers']) if src_msg['twitter_followers'] else None
        fb_followers = int(src_msg['facebook_followers']) if src_msg['facebook_followers'] else None
        fb_likes = int(src_msg['facebook_likes']) if src_msg['facebook_likes'] else None
        linkedin_followers = int(src_msg['linkedin_followers']) if src_msg['linkedin_followers'] else None

        src_msg['insta_followers'] = insta_followers
        src_msg['facebook_followers'] = fb_followers
        src_msg['facebook_likes'] = fb_likes
        src_msg['twitter_followers'] = twitter_followers
        src_msg['linkedin_followers'] = linkedin_followers

        # src_msg.pop('other POC', None)
        src_msg.pop('SPOC Name', None)
        src_msg.pop('relationship_status', None)
        src_msg.pop('ishangam_id', None)
        # src_msg.pop('preferred_moc\n(preferred mode of contact)', None)
        src_msg.pop('secondary_poc_user_id', None)
        src_msg.pop('primary_poc_user_id', None)
        # src_msg.pop('Occupation', None)
        src_msg.pop('POC Name', None)
        src_msg.pop('sector', None)
        # src_msg.pop('designation', None)
        src_msg.pop('company', None)
        src_msg.pop('industry', None)
        src_msg.pop('company2_name', None)
        src_msg.pop('industry2', None)
        src_msg.pop('sector2', None)
        src_msg.pop('company3_name', None)
        src_msg.pop('industry3', None)
        src_msg.pop('sector3', None)
        # src_msg.pop('Wikipedia', None)
        # src_msg.pop('Facebook Link', None)
        # src_msg.pop('instagram_followers', None)
        # src_msg.pop('Facebook Followers', None)
        # src_msg.pop('Facebook Likes', None)
        src_msg.pop('center_name', None)
        # src_msg.pop('title', None)

        return ContactProcessor.validateValues(src_msg, iso2, correction)
