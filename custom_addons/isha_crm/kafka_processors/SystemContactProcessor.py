
class SystemContactProcessor:

    def process_message(self, msg, env, metadata):
        iso2 = metadata['iso2']
        correction = metadata['correction']
        formatConverter = metadata['contactFormatConverter']

        try:
            msgDict = formatConverter.getOdooFormat(msg, iso2, correction)
            contactModel = env['res.partner']
            contactModel.sudo().create_internal(msgDict)
            env.cr.commit()
        finally:
            pass

