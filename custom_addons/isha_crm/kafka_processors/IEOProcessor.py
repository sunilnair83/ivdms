from datetime import datetime

from .. import ContactProcessor


class IEOProcessor:
    system_id = 30

    def removePlus(self, code):
        if code and type(code) == str and len(code)>1 and code[0]=='+':
            return code[1:]
        else:
            return code

    def getDateParsed(self, str_date):
        try:
            date_parsed = datetime.strptime(str_date, '%Y-%m-%d %H:%M:%S') if str_date else None
            date_parsed = str(date_parsed) if date_parsed else None
        except:
            date_parsed = None
        return date_parsed

    def langTransform(self, code):
        if code:
            code = str.lower(code)
            if code == 'mr':
                return 'Marathi'
            elif code == 'te':
                return 'Telugu'
            elif code == 'hi':
                return 'Hindi'
            elif code == 'kn':
                return 'Kanada'
            elif code == 'ta':
                return 'Tamil'
            elif code == 'en':
                return 'English'
            elif code == 'ru':
                return 'Russian'
            elif code == 'es':
                return 'Spanish'
            elif code == 'ml':
                return 'Malayalam'
            elif code == 'zh':
                return 'Simplified Chinese'
            elif code == 'fr':
                return 'French'
            elif code == 'de':
                return 'German'
            else:
                return code
        return None

    def validateCoupon(self, coupon):
        if coupon == '0' or coupon == '' or coupon == ' ':
            return None
        else:
            return coupon


    def getGoldenFormat(self, src_msg, iso2, correction):
        src_msg = ContactProcessor.replaceEmptyString(src_msg)
        name = src_msg['first_name'] if src_msg['first_name'] else ''
        name += ' ' + src_msg['last_name'] if src_msg['last_name'] else ''
        email = src_msg['email']
        phone = src_msg['mobilePhone'] if 'mobilePhone' in src_msg else ''
        phone2 = src_msg['homePhone'] if 'homePhone' in src_msg else ''
        if phone == '':
            phone = None

        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': src_msg['user_id'],
            'active': True,
            'name': name, 'display_name': name,'prof_dr': None,
            'phone': phone, 'phone2': phone2,'phone3': None, 'phone4': None, 'whatsapp_number': None, 'whatsapp_country_code': None,
            'email': email if email else None, 'email2': None,'email3': None,
            'street': src_msg['resident_address1'] if 'resident_address1' in src_msg else None,  'street2':src_msg['resident_address2'] if 'resident_address2' in src_msg else None,
            'city': src_msg['resident_city'], 'state': src_msg['resident_state'], 'country': src_msg['resident_country'], 'zip': src_msg['resident_ZIP'],
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': ContactProcessor.getgender(src_msg['sex']), 'occupation': None, 'marital_status': None, 'dob': None,
            'aadhaar_no': None, 'pan_no': None, 'passport_no': None,
            'nationality': None, 'deceased': None, 'contact_type': None, 'companies': None,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None,
            'sso_id': src_msg['ssoId'] if 'ssoId' in src_msg else None,
            'has_charitable_txn': True
        }

        local_modified_at = self.getDateParsed(src_msg['lastUpdTime'])
        if local_modified_at:
            odooRec['local_modified_date'] = local_modified_at
        else:
            odooRec['local_modified_date'] = self.getDateParsed(src_msg['start_date'])

        return ContactProcessor.validateValues(odooRec, iso2, correction)

    def getGoldenFormatForTxn(self, ieo_txn):
        # {
        #     "user_id": "790480",
        #     "first_name": "Supreet",
        #     "last_name": "Kaur",
        #     "email": "supreet10000@gmail.com",
        #     "sex": "female",
        #     "resident_address1": "3012 nSector 35D",
        #     "resident_address2": "Null",
        #     "resident_city": "CHANDIGARH",
        #     "resident_state": "Chandigarh",
        #     "resident_ZIP": "160022",
        #     "resident_country": "IN",
        #     "custType": "PAID",
        #     "coupon_code_value": "",
        #     "lastUpdTime": "2020-04-25 6:40:38",
        #     "class_id": "1",
        #     "course_completion_date": "0000-00-00",
        #     "start_date": "2020-03-31 13:15:51",
        #     "end_date": "2020-04-30 13:15:51",
        #     "last_login_date": "2020-04-25"
        # }
        cust_type = ieo_txn.get('custType')
        if cust_type == 'PAYASUCAN':
            cust_type = 'FREE'
        return {
            'local_user_id': ieo_txn.get('user_id'),
            'cust_type': cust_type,
            'class_id': int(ieo_txn.get('class_id')),
            'course_completion_date': ieo_txn.get('course_completion_date') if ieo_txn.get('course_completion_date') != '0000-00-00' else None,
            'start_date': ieo_txn.get('start_date'),
            'end_date': ieo_txn.get('end_date'),
            'last_login_date': ieo_txn.get('last_login_date') if ieo_txn.get('last_login_date') != '0000-00-00' else None,
            'coupon': self.validateCoupon(ieo_txn.get('coupon_code_value')),
            'lang_pref': self.langTransform(ieo_txn['langPref']) if 'langPref' in ieo_txn else None,
            'last_upd_time':ieo_txn.get('lastUpdTime'),
            'create_date': ieo_txn.get('start_date'),
            'write_date': ieo_txn.get('lastUpdTime'),
            'email':ieo_txn.get('email'),
            'sso_id': ieo_txn['ssoId'] if 'ssoId' in ieo_txn else None
        }

    def process_message(self, src_msg, env, metadata):
        # logger.debug(msg)
        src_msg_dict = src_msg['payload']

        iso2 = metadata['iso2']
        correction = metadata['correction']

        msg_dict = self.getGoldenFormat(src_msg_dict, iso2, correction)
        txn_dict = self.getGoldenFormatForTxn(src_msg_dict)

        try:
            contact = env['res.partner'].sudo().create_internal(msg_dict)
            txn_dict['contact_id_fkey'] = contact[0].id
            flag_dict = self.createPgmInternal(env, txn_dict)
            txn = flag_dict['rec']
            # to check
            if txn.course_completion_date != contact[0].ieo_date or\
                    txn.last_login_date != contact[0].last_txn_date or\
                    txn.progress != contact[0].ieo_progress:
                contact._compute_ieo_date()
            env.cr.commit()
            return flag_dict
        finally:
            pass

    def createPgmInternal(self, ENV, src_rec):
        ieoModel = ENV['ieo.record']

        rec = ieoModel.search([('local_user_id', '=', src_rec['local_user_id'])])
        if len(rec) != 0:
            if not src_rec['course_completion_date']:
                src_rec.pop('course_completion_date',None)
            rec.write(src_rec)
        elif len(rec) == 0:
            rec = ieoModel.create(src_rec)

        flag_dict = {'flag': True, 'rec': rec}
        return flag_dict
