import logging

from .. import ContactProcessor, TxnProcessor
from .. import TxnUpdater

logger = logging.getLogger("Ereceipts Processor")


class EreceiptsProcessor:
    system_id = 8

    def process_message(self, src_msg_dict, env, metadata):
        src_msg_dict = src_msg_dict['payload']
        metadata = self.get_common_transformations(metadata, src_msg_dict)
        ercUpdater = TxnUpdater.DonationsUpdater()
        contactMsgDict = self.getGoldenFormatForContact(src_msg_dict, metadata)
        txn_dict = self.getGoldenFormatForTxn(src_msg_dict, env, metadata)

        # If the project is Cottage then ignore creating the contact and create the txn without mapping to a contact
        contact_flag = ercUpdater.contactCreateFlag(env, txn_dict, contactMsgDict)
        if contact_flag:
            contactModel = env['res.partner']
            contact = contactModel.sudo().create_internal(contactMsgDict)
            txn_dict['contact_id_fkey'] = contact[0].id
            txn_dict['guid'] = contact[0].guid
        ercUpdater.createDonationInternal(env, txn_dict)
        env.cr.commit()

    # to be overridden
    def get_common_transformations(self, metadata, src_msg):
        rec_date = None
        if src_msg['receipt_date']:
            try:
                rec_date = TxnProcessor.getDateTimeField(src_msg['receipt_date'])
            except:
                pass

        metadata.update({
            'don_receipt_date': rec_date,
        })

        return metadata

    def getGoldenFormatForContact(self, msg, metadata):
        iso2 = metadata['iso2']
        correction = metadata['correction']

        msg = ContactProcessor.replaceEmptyString(msg)
        phone = ContactProcessor.getTokenizedVersion(msg['phones'],',')
        email = {'1':msg['emails']}
        dob = None

        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': msg['id'],
            'active': True,
            'name': msg['first_name'], 'display_name': msg['first_name'], 'prof_dr': None,
            'phone': phone['1'] if '1' in phone else None, 'phone2': phone['2'] if '2' in phone else None,
            'phone3': phone['3'] if '3' in phone else None,'phone4': phone['4'] if '4' in phone else None,
            'whatsapp_number': None, 'whatsapp_country_code': None,
            'email': email['1'] if '1' in email else None, 'email2': email['2'] if '2' in email else None,'email3': email['3'] if '3' in email else None,
            'street': msg['address1'],  'street2':None,'city': msg['town_city1'], 'state': msg['state_province_region1'], 'country': msg['country1'],
            'zip': msg['postal_code1'],
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': None, 'occupation': None, 'marital_status': None, 'dob': dob,
            'nationality': msg['nationality'], 'deceased': None, 'contact_type': msg['contact_type'], 'companies': None,
            'aadhaar_no': None, 'pan_no': msg['pan_number'], 'passport_no': msg['passport_number'],
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': None,
            'has_charitable_txn': True
        }
        if 'modified_at' in msg and msg['modified_at']:
            odooRec['local_modified_date'] = ContactProcessor.getDateParsed(msg['modified_at'])
        return ContactProcessor.validateValues(odooRec, iso2, correction)

    def getGoldenFormatForTxn(self, src_msg, env, metadata):
        iso2 = metadata['iso2']
        correction = metadata['correction']

        src_msg = ContactProcessor.replaceEmptyString(src_msg)
        if 'local_patron_id' in src_msg and src_msg['local_patron_id']:
            pat_id = src_msg['local_patron_id']
            pat_poc_id = src_msg['patron_poc_id'] if 'patron_poc_id' in src_msg else None
        else:
            pat_id = src_msg['contact_id'] if 'contact_id' in src_msg else None
            pat_poc_id = src_msg['donor_poc_id'] if 'donor_poc_id' in src_msg else None
        if 'donation_type' not in src_msg or src_msg['donation_type'] is None:
            donation = 'DONATION'
        else:
            donation = src_msg['donation_type']
        odooRec = {
            'local_donor_id': src_msg['contact_id'] if 'contact_id' in src_msg else None,
            'donor_poc_id': src_msg['donor_poc_id'] if 'donor_poc_id' in src_msg else None,
            'local_patron_id': pat_id,
            'patron_poc_id': pat_poc_id,
            'project': src_msg['project'] if 'project' in src_msg else None,
            'mode_of_payment': src_msg['mode_of_payment'] if 'mode_of_payment' in src_msg else None,
            'inkind': src_msg['inkind'] if 'inkind' in src_msg else None,
            'amount': src_msg['amount'] if 'amount' in src_msg else None,
            'receipt_number': src_msg['receipt_number'] if 'receipt_number' in src_msg else None,
            'don_receipt_date': metadata['don_receipt_date'],
            'active': src_msg['active'] if 'active' in src_msg else None,
            'local_trans_id': src_msg['id'] if 'id' in src_msg else None,
            'status': src_msg['status'] if 'status' in src_msg else None,
            'donation_type': donation,
            'entity': src_msg['entity'] if 'entity' in src_msg else None,
            'system_id': self.system_id,
            'center': src_msg['center'] if 'center' in src_msg else None
        }
        return odooRec
