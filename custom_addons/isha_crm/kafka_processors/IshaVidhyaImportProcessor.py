from datetime import datetime

from . import IshaVidhyaUtil
from .. import ContactProcessor, TxnUpdater


class IshaVidhyaImportProcessor:
    system_id = 48

    def process_message(self, src_msg_dict, env, metadata):
        env['donation.isha.education'].with_context(kafka_import=True).create(src_msg_dict)
