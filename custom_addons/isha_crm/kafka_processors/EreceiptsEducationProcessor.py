from datetime import datetime

from . import IshaVidhyaUtil
from .EreceiptsProcessor import EreceiptsProcessor
from .. import ContactProcessor, TxnProcessor, TxnUpdater


class EreceiptsEducationProcessor(EreceiptsProcessor):

    def process_message(self, src_msg, env, metadata):
        src_msg = src_msg['payload']
        existing_don = None
        if src_msg['center'] != 'Online':
            existing_don = env['donation.isha.education'].search(
                [('receipt_number', '=', src_msg['receipt_number']), ('system_id', '!=', 47)], limit=1)
        elif src_msg['transfer_reference']:
            existing_don = env['donation.isha.education'].search(
                [('transfer_reference', '=', src_msg['transfer_reference']), ('system_id', '!=', 47)], limit=1)
            if not existing_don and src_msg['transfer_reference'] and src_msg['transfer_reference'].startswith('IVD'):
                raise Exception('Waiting for the corresponding record from Outreach to come first.')
        if existing_don:
            metadata = self.get_common_transformations(metadata, src_msg)
            txn_dict = self.getGoldenFormatForTxn(src_msg, env, metadata, existing_don.contact_id_fkey.id)
            # if breakup is already present, don't update it.
            if (existing_don.general_donation_amount or existing_don.govt_school_adoption_prgm_amount or
                    existing_don.infrastructure_requirements_amount or existing_don.full_educational_support_qty or
                    existing_don.full_educational_support_amount or existing_don.scholarship_qty or
                    existing_don.scholarship_amount or existing_don.transport_subsidy_qty or
                    existing_don.transport_subsidy_amount or existing_don.noon_meal_subsidy_qty or
                    existing_don.noon_meal_subsidy_amount):
                txn_dict.pop('general_donation_amount', None)
                txn_dict.pop('govt_school_adoption_prgm_amount', None)
                txn_dict.pop('infrastructure_requirements_amount', None)
                txn_dict.pop('full_educational_support_qty', None)
                txn_dict.pop('full_educational_support_amount', None)
                txn_dict.pop('scholarship_qty', None)
                txn_dict.pop('scholarship_amount', None)
                txn_dict.pop('transport_subsidy_qty', None)
                txn_dict.pop('transport_subsidy_amount', None)
                txn_dict.pop('noon_meal_subsidy_qty', None)
                txn_dict.pop('noon_meal_subsidy_amount', None)
            update_vals = {}
            update_vals['local_trans_id'] = txn_dict['local_trans_id']
            if txn_dict['status'] == 'CANCELLED':
                update_vals['status'] = txn_dict['status']
                update_vals['amount'] = None
                update_vals['inr_amount'] = None
                update_vals['fes_new_qty'] = None
                update_vals['fes_renewal_qty'] = None
                update_vals['scho_new_qty'] = None
                update_vals['scho_renewal_qty'] = None
                update_vals['general_donation_amount'] = None
                update_vals['govt_school_adoption_prgm_amount'] = None
                update_vals['infrastructure_requirements_amount'] = None
                update_vals['full_educational_support_qty'] = None
                update_vals['full_educational_support_amount'] = None
                update_vals['scholarship_qty'] = None
                update_vals['scholarship_amount'] = None
                update_vals['transport_subsidy_qty'] = None
                update_vals['transport_subsidy_amount'] = None
                update_vals['noon_meal_subsidy_qty'] = None
                update_vals['noon_meal_subsidy_amount'] = None
                last_donation_date = None
                for donation in existing_don.contact_id_fkey.contact_donations_education_id_fkey:
                    if donation.id != existing_don.id:
                        if not last_donation_date or last_donation_date < donation.donation_date:
                            last_donation_date = donation.donation_date
                update_vals['iv_last_donated_date'] = last_donation_date
            else:
                for key in txn_dict:
                    if not existing_don[key]:
                        update_vals[key] = txn_dict[key]
                update_vals.update(self.get_txn_fields_in_contact(src_msg, metadata))
            if existing_don.iv_email_manually_edited and 'iv_email' in update_vals:
                update_vals.pop('iv_email', None)
            existing_don.update(update_vals)

        else:
            metadata = self.get_common_transformations(metadata, src_msg)
            ercUpdater = TxnUpdater.DonationsUpdater()
            contactMsgDict = self.getGoldenFormatForContact(src_msg, metadata)

            if not contactMsgDict['phone'] and not contactMsgDict['email']:
                anonymous_contact = env.ref('isha_crm.res_partner_anonymous')
                txn_dict = self.getGoldenFormatForTxn(src_msg, env, metadata, None)
                txn_dict['contact_id_fkey'] = anonymous_contact.id
                txn_dict['guid'] = anonymous_contact.guid
            else:
                contactModel = env['res.partner']
                contact = contactModel.sudo().create_internal(contactMsgDict)
                txn_dict = self.getGoldenFormatForTxn(src_msg, env, metadata, contact[0].id)
                txn_dict['contact_id_fkey'] = contact[0].id
                txn_dict['guid'] = contact[0].guid
            ercUpdater.createDonationInternal(env, txn_dict)
        env.cr.commit()

    def getGoldenFormatForContact(self, msg, metadata):
        odooRec = super(EreceiptsEducationProcessor, self).getGoldenFormatForContact(msg, metadata)
        odooRec.update(self.get_txn_fields_in_contact(msg, metadata))
        return odooRec

    def get_txn_fields_in_contact(self, msg, metadata):
        msg = ContactProcessor.replaceEmptyString(msg)
        return {
            'iv_last_donated_date': metadata['donation_date'].date(),
            'last_txn_date': metadata['donation_date'].date(),
            'charitable_last_txn_date': metadata['donation_date'].date(),
            'iv_email': msg['emails']
         }

    def get_common_transformations(self, metadata, src_msg):
        metadata = super(EreceiptsEducationProcessor, self).get_common_transformations(metadata, src_msg)
        chq_date_str = src_msg['dd_chq_date']
        chq_date = None
        try:
            chq_date = datetime.strptime(chq_date_str, '%d-%m-%Y') if chq_date_str else None
        except ValueError:
            try:
                chq_date = datetime.strptime(chq_date_str, '%d %b %Y') if chq_date_str else None
            except ValueError:
                chq_date = datetime.strptime(chq_date_str, '%d/%m/%Y') if chq_date_str else None

        metadata.update({
            'chq_date': chq_date,
            'donation_date': datetime.strptime(metadata['don_receipt_date'], '%Y-%m-%d') if src_msg['mode_of_payment'] == 'Cash' else chq_date
        })
        return metadata

    def getGoldenFormatForTxn(self, src_msg, env, metadata, contact_id):
        odooRec = super(EreceiptsEducationProcessor, self).getGoldenFormatForTxn(src_msg, env, metadata)
        src_msg = ContactProcessor.replaceEmptyString(src_msg)

        odooRec['ereceipt_record_id'] = src_msg['id'] if 'id' in src_msg else None
        odooRec['ebook_number'] = src_msg['ebook_number']
        if src_msg['dd_chq_number'] and src_msg['dd_chq_number'] != '0':
            odooRec['transfer_reference'] = src_msg['dd_chq_number']
            odooRec['dd_chq_number'] = src_msg['dd_chq_number']
        else:
            odooRec['transfer_reference'] = src_msg['transfer_reference']
        odooRec['bank_name'] = src_msg['bank_name']
        odooRec['bank_branch'] = src_msg['bank_branch']
        odooRec['pan_card'] = src_msg['pan_number']
        odooRec['dd_chq_date'] = metadata['chq_date']
        odooRec['payment_gateway'] = src_msg['payment_gateway']
        odooRec['short_year'] = src_msg['short_year']
        odooRec['center'] = src_msg['center']
        odooRec['contact_email'] = src_msg['emails']
        odooRec['contact_phone'] = src_msg['phone']
        odooRec['contact_mobile'] = src_msg['mobile']
        odooRec['contact_address'] = src_msg['address1']
        odooRec['contact_city'] = src_msg['town_city1']
        odooRec['contact_state'] = src_msg['state_province_region1']
        odooRec['contact_pincode'] = src_msg['postal_code1']
        odooRec['contact_country'] = src_msg['country1']
        currency = env.ref('base.INR').id
        odooRec['currency'] = currency
        odooRec['exchange_rate'] = 1
        odooRec['inr_amount'] = src_msg['amount']
        odooRec['donation_date'] = metadata['donation_date']
        odooRec['books_of_account'] = src_msg['books_of_account']
        odooRec['comments'] = src_msg['comments']
        # 'Isha Education IVMS', 'Isha Education AP GSAP'
        if src_msg['books_of_account'] in ['Isha Education HO IVMS', 'Isha Education FCRA', 'Isha Education TN GSAP']:
            odooRec['iv_donation'] = True
        if not IshaVidhyaUtil.is_consolidated_parent_receipt(src_msg['first_name'], src_msg['comments']):
            odooRec.update(IshaVidhyaUtil.get_purpose_breakup(src_msg, env, odooRec['donation_date'], currency,
                                                              src_msg['project'], contact_id, src_msg['short_year'],
                                                              None))
            odooRec['iv_donor_category'] = IshaVidhyaUtil.get_donor_category(env, None, src_msg['first_name'],
                                                                             contact_id, odooRec['donation_date'],
                                                                             odooRec['inr_amount'],
                                                                             src_msg['transfer_reference'],
                                                                             src_msg['receipt_number'])
            fes_qty = odooRec['full_educational_support_qty'] if odooRec['full_educational_support_qty'] else 0
            scho_qty = odooRec['scholarship_qty'] if odooRec['scholarship_qty'] else 0
            odooRec.update(IshaVidhyaUtil.get_new_renewal_info(env, odooRec['donation_date'], contact_id,
                                                               fes_qty, scho_qty, src_msg['transfer_reference'],
                                                               src_msg['receipt_number']))
        else:
            odooRec['consolidated_receipt_type'] = 'parent'

        return odooRec
