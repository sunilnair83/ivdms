from datetime import datetime

from . import IshaVidhyaUtil
from .. import ContactProcessor, TxnUpdater


def get_currency(src_msg, env):
    currency = None
    gateway = src_msg['payment_gateway']
    if gateway:
        if gateway == 'paypal':
            currency = env.ref('base.USD').id
        else:
            currency = env.ref('base.INR').id
    return currency


class IshaVidhyaOutreachProcessor:
    system_id = 46

    def process_message(self, src_msg_dict, env, metadata):
        iso2 = metadata['iso2']
        correction = metadata['correction']

        ercUpdater = TxnUpdater.DonationsUpdater()
        contactMsgDict = self.getGoldenFormatForContact(env, src_msg_dict, iso2, correction)

        existing_don = None
        if src_msg_dict['isha_transaction_id']:
            existing_don = env['donation.isha.education'].search(
                [('transfer_reference', '=', src_msg_dict['isha_transaction_id'])], limit=1)
        if existing_don:
            txn_dict = self.getGoldenFormatForTxn(src_msg_dict, env, iso2, correction, existing_don.contact_id_fkey.id)
            update_vals = {}
            for key in txn_dict:
                if not existing_don[key]:
                    update_vals[key] = txn_dict[key]
            if not existing_don.iv_email_manually_edited and txn_dict['contact_email']:
                update_vals['iv_email'] = txn_dict['contact_email']
            existing_don.update(update_vals)
        else:
            contactModel = env['res.partner']
            contact = contactModel.sudo().create_internal(contactMsgDict)
            txn_dict = self.getGoldenFormatForTxn(src_msg_dict, env, iso2, correction, contact[0].id)
            txn_dict['contact_id_fkey'] = contact[0].id
            txn_dict['guid'] = contact[0].guid
            ercUpdater.createDonationInternal(env, txn_dict)
        env.cr.commit()

    def getGoldenFormatForContact(self, env, msg, iso2, correction):
        crmCon = self.get_empty_contact_object()
        if not msg['isha_transaction_id']:
            raise Exception('Isha Txn Id is missing.')
        donation_date = datetime.fromtimestamp(msg['transaction_date'] / 1000)
        crmCon.update({
            'system_id': self.system_id,
            'local_contact_id': msg['isha_transaction_id'],
            'active': True,
            'name': msg['donor_name'], 'display_name': msg['donor_name'], 'prof_dr': None,
            'street': msg['address'], 'street2': None, 'city': msg['city'], 'state': msg['state'],
            'zip': msg['pincode'],
            'country': msg['country'],
            'nationality': msg['nationality'], 'deceased': None, 'companies': None,
            'phone': msg['mobile'],
            'email': msg['email'], 'email2': None, 'email3': None,
            'iv_email': msg['email'],
            'aadhaar_no': None, 'pan_no': msg['pan_number'],
            'iv_last_donated_date': donation_date.date(),
            'last_txn_date': donation_date.date(),
            'charitable_last_txn_date': donation_date.date(),
            'has_charitable_txn': True,
            'sso_id': msg['sso_id']
        })
        crmCon = ContactProcessor.validateValues(crmCon, iso2, correction)
        if crmCon['nationality']:
            country = env['res.country'].search([('code', '=', crmCon['nationality'])], limit=1)
            if country:
                crmCon['nationality'] = country.name
        return crmCon

    def getGoldenFormatForTxn(self, src_msg, env, iso2, correction, contact_id):
        crmTxn = {
        'system_id': self.system_id,
        }
        crmTxn['entity'] = 'IshaEducation'
        crmTxn['contact_email'] = src_msg['email']
        crmTxn['contact_mobile'] = src_msg['mobile']
        crmTxn['contact_address'] = src_msg['address']
        crmTxn['contact_city'] = src_msg['city']
        crmTxn['contact_state'] = src_msg['state']
        crmTxn['contact_pincode'] = src_msg['pincode']
        if src_msg['country']:
            country = env['res.country'].search([('code', '=', src_msg['country'])], limit=1)
            if country:
                crmTxn['contact_country'] = country.name
            else:
                crmTxn['contact_country'] = src_msg['country']
        if not src_msg['payment_gateway']:
            raise Exception('Payment Gateway is missing.')
        currency = get_currency(src_msg, env)
        crmTxn['currency'] = currency
        crmTxn['amount'] = src_msg['amount']
        if not src_msg['transaction_date']:
            raise Exception('Transaction date is missing.')
        donation_date = datetime.fromtimestamp(src_msg['transaction_date'] / 1000)
        donation_date_str = donation_date.strftime('%Y-%m-%d')
        short_year = IshaVidhyaUtil.get_short_financial_year(donation_date)
        crmTxn['short_year'] = short_year
        exchange_rate = self.get_exchange_rate(short_year, currency, env)
        crmTxn['exchange_rate'] = exchange_rate
        inr_amount = src_msg['amount'] * exchange_rate
        crmTxn['inr_amount'] = inr_amount
        crmTxn['donation_date'] = donation_date_str
        gateway = src_msg['payment_gateway']
        if gateway and gateway == 'paypal':
            crmTxn['don_receipt_date'] = donation_date_str
        crmTxn['mode_of_payment'] = 'Online'
        crmTxn['transfer_reference'] = src_msg['isha_transaction_id']
        crmTxn['payment_gateway'] = src_msg['payment_gateway']
        crmTxn['center'] = 'Online'
        crmTxn['project'] = src_msg['purpose']
        crmTxn['campaign_id'] = src_msg['isha_campaign_id']
        crmTxn['fundraiser_name'] = src_msg['owner_name'] if 'owner_name' in src_msg else None
        crmTxn['campaign_name'] = src_msg['campaign_name'] if 'campaign_name' in src_msg else None
        crmTxn['local_trans_id'] = src_msg['isha_transaction_id']
        crmTxn['status'] = 'SUCCESS'
        crmTxn['iv_donation'] = True

        if (src_msg['general_donation'] or src_msg['govt_school_adoption_program']
                or src_msg['infrastructure_requirements'] or src_msg['full_educational_support_qty']
                or src_msg['full_educational_support_total_amount'] or src_msg['scholarship_qty']
                or src_msg['scholarship_total_amount'] or src_msg['transport_subsidy_qty']
                or src_msg['transport_subsidy_total_amount'] or src_msg['noon_meal_subsidy_qty']
                or src_msg['noon_meal_subsidy_total_amount']):
            if crmTxn['campaign_name'] and 'isha vidhya valasaravakkam' in crmTxn['campaign_name'].lower():
                crmTxn['scholarship_amount'] = src_msg['amount']
                crmTxn['subsidy_breakup_modified_by'] = 'System'
            else:
                crmTxn['general_donation_amount'] = src_msg['general_donation']
                crmTxn['govt_school_adoption_prgm_amount'] = src_msg['govt_school_adoption_program']
                crmTxn['infrastructure_requirements_amount'] = src_msg['infrastructure_requirements']
                crmTxn['full_educational_support_qty'] = src_msg['full_educational_support_qty']
                crmTxn['full_educational_support_amount'] = src_msg['full_educational_support_total_amount']
                crmTxn['scholarship_qty'] = src_msg['scholarship_qty']
                crmTxn['scholarship_amount'] = src_msg['scholarship_total_amount']
                crmTxn['transport_subsidy_qty'] = src_msg['transport_subsidy_qty']
                crmTxn['transport_subsidy_amount'] = src_msg['transport_subsidy_total_amount']
                crmTxn['noon_meal_subsidy_qty'] = src_msg['noon_meal_subsidy_qty']
                crmTxn['noon_meal_subsidy_amount'] = src_msg['noon_meal_subsidy_total_amount']
                crmTxn['subsidy_breakup_modified_by'] = 'Source'
        else:
            # no breakup field has a value, so calculate the breakup.
            crmTxn.update(IshaVidhyaUtil.get_purpose_breakup(src_msg, env, donation_date, currency, src_msg['purpose'],
                                                             contact_id, short_year,
                                                             crmTxn['campaign_name']))
        crmTxn['iv_donor_category'] = IshaVidhyaUtil.get_donor_category(env, src_msg['isha_campaign_id'], src_msg['donor_name'],
                                                                        contact_id, donation_date, inr_amount,
                                                                        src_msg['isha_transaction_id'], None)
        fes_qty = int(src_msg['full_educational_support_qty']) if src_msg['full_educational_support_qty'] else 0
        scho_qty = int(src_msg['scholarship_qty']) if src_msg['scholarship_qty'] else 0
        crmTxn.update(IshaVidhyaUtil.get_new_renewal_info(env, donation_date, contact_id, fes_qty, scho_qty,
                                                          src_msg['isha_transaction_id'], None))
        # else:
        #     crmTxn['consolidated_receipt'] = True
        return crmTxn

    def get_exchange_rate(self, short_year, currency, env):
        exchange_rate = None
        if currency:
            if currency == env.ref('base.INR').id:
                exchange_rate = 1
            else:
                result = env['iv.exchange.rate'].search([('financial_year', '=', short_year), ('currency', '=', currency)])
                if result:
                    exchange_rate = result[0].rate
                else:
                    raise Exception('No Exchange rate found for the given donation date and currency.')
        return exchange_rate

    def get_empty_contact_object(self):
        return {
            'system_id': None,
            'local_contact_id': None,
            'active': True,
            'guid': None,
            'name': None, 'display_name': None, 'prof_dr': None,
            'phone': None, 'phone2': None,
            'phone3': None, 'phone4': None,
            'whatsapp_number': None, 'whatsapp_country_code': None,
            'email': None, 'email2': None,
            'email3': None,
            'street': None, 'street2': None, 'city': None, 'state': None,
            'country': None, 'zip': None,
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': None, 'occupation': None, 'marital_status': None,
            'dob': None,
            'nationality': None, 'deceased': None, 'contact_type': None, 'companies': None,
            'aadhaar_no': None, 'pan_no': None, 'passport_no': None,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': None
        }
