from ...isha_crm.ContactProcessor import IshaVolunteer
import phpserialize
from datetime import date
from datetime import datetime
from ..isha_base_importer import Configuration
import base64
from odoo.exceptions import _logger
import requests
import json


class SPProcessor:

    def process_message(self, msg, env, metadata):
        res_partner = env['res.partner'].sudo()
        sp_registration = env['sp.registration'].sudo()
        contact_processor = IshaVolunteer()
        values = self._get_pre_reg_values(msg, env, metadata)
        contact_flag = res_partner.create_internal_flag(contact_processor.getOdooFormat(values, {}, {}))
        res_partner_id = contact_flag['rec'][0]
        values['partner_id'] = res_partner_id.id
        values['is_new_contact'] = sp_registration.check_new_contact(contact_flag['phase'])
        values['is_from_import'] = True
        # self.createVolunteer(env, values)
        rec = self.createOrUpdate(env, values)

        return {'flag': True, 'rec': rec}

    def createOrUpdate(self, env, sp_dict):
        sp_model = env['sp.registration']
        sp_rec = sp_model.search([('interest_id', '=', sp_dict['interest_id'])])
        if not sp_rec:
            sp_rec = sp_model.create(sp_dict)
            # sp_rec.write(sp_dict)
        # else:
        #     sp_rec = sp_model.create(sp_dict)

        return sp_rec

    def createVolunteer(self, env, sp_dict):
        sp_model = env['isha.volunteer']
        sp_rec = sp_model.search([('partner_id', '=', sp_dict['partner_id'])])
        if sp_rec:
            sp_rec.write(sp_dict)
        else:
            sp_rec = sp_model.create(sp_dict)

        return sp_rec

    def _get_pre_reg_values(self, src_msg, env, metadata):
        country = getOdooCountry(env, src_msg['country'], metadata)
        state = getOdooState(env, country, src_msg['state'])
        values = {}
        if src_msg['first_name'] and src_msg['last_name']:
            values.update({
                'sp_name': src_msg['first_name'] + " " + src_msg['last_name']
            })

        phones = convert_suvya_dc2type_arrays_to_list(src_msg['phone'], env, lambda x, e: x, 'Phones')
        phone = mobile = isha_phone = None
        if phones:
            for ph in phones:
                if ph['type'] == 'Personal':
                    phone = ph['number']
                elif ph['type'] == 'Whats-App':
                    mobile = ph['number']
                elif ph['type'] == 'CUG':
                    isha_phone = ph['number']

        dob = datetime.strptime(src_msg['date_of_birth'], '%Y-%m-%d') if src_msg['date_of_birth'] else None
        education = None
        if src_msg.get('education'):
            education = convert_suvya_dc2type_arrays_to_odoo_one2many(src_msg['education'], env, convert_education, 'Education')

        isha_connect = None
        if src_msg.get('ishaConnect'):
            isha_connect = convert_suvya_dc2type_arrays_to_odoo_many2many(src_msg['ishaConnect'], env, convert_isha_connect, 'Isha Connect')

        volunteering_iyc = None
        if src_msg.get('volunteering_iyc'):
            volunteering_iyc = convert_suvya_dc2type_arrays_to_odoo_one2many(src_msg.get('volunteering_iyc'), env, convert_volunteering_iyc, 'Volunteering at IYC')

        local_volunteering = None
        if src_msg.get('volunteering_local'):
            local_volunteering = convert_suvya_dc2type_arrays_to_odoo_one2many(src_msg.get('volunteering_local'), env, convert_local_volunteering, 'Local Volunteering')

        program_history = None
        if src_msg.get('program_history'):
            program_history = convert_suvya_dc2type_arrays_to_odoo_one2many(src_msg.get('program_history'), env, convert_program_history, 'Program History')

        work_exp = None
        if src_msg.get('work_experience'):
            work_exp = convert_suvya_dc2type_arrays_to_odoo_one2many(src_msg.get('work_experience'), env, convert_work_experience, 'Work experience')

        language = None
        if src_msg.get('languages'):
            language = convert_suvya_dc2type_arrays_to_odoo_one2many(src_msg['languages'], env, convert_lang_proficiency, 'Languages')

        health_assessment_id = None
        if src_msg.get('ha_form_sent'):
            health_assessment_id = [(0, 0, getOdooHACreate(env, src_msg))]

        id_proof_image = None
        add_proof_image = None
        profile_image = None
        if src_msg['person_id']:
            sp_config = Configuration('CRM')
            profile_image_url = sp_config['SUVYA_API_BASE_URL'] + '/sadhanapada/person/' + src_msg['person_uid']
            _logger.info("Profile image url: " + profile_image_url)
            profile_image_response = requests.get(profile_image_url)
            if not profile_image_response or profile_image_response.status_code != 200:
                raise Exception('Profile image not received for url: ' + profile_image_url)

            profile_image = encode_image(profile_image_response.content)

            doc_list_url = sp_config['SUVYA_API_BASE_URL'] + '/api/get-people-document/' + str(
                src_msg['person_id']) + '?_format=json'
            doc_list_res = requests.get(doc_list_url)
            docs_res = json.loads(doc_list_res.content)

            if docs_res['documents']:
                for doc in docs_res['documents']:
                    if doc['type'] == 'ID Proof' and not id_proof_image:
                        id_proof_image = get_image_document(doc['id'], sp_config)
                    elif doc['type'] == 'Address Proof' and not add_proof_image:
                        add_proof_image = get_image_document(doc['id'], sp_config)
            first_name = src_msg['first_name']
            last_name = src_msg['last_name']
            email = src_msg['personal_email'].lower() if src_msg['personal_email'] else None
        else:
            first_name = src_msg['i_first_name']
            last_name = src_msg['i_last_name']
            email = src_msg['email'].lower() if src_msg['email'] else None

        values.update({
            'interest_id': src_msg['id'],
            'person_id': src_msg['person_id'],
            'first_name': first_name,
            'last_name': last_name,
            'sp_email': email,
            'dob': dob,
            'gender': getOdooGender(src_msg['gender']),
            'sp_marital_status': getOdooMaritalStatus(src_msg['marital_status']),
            'sp_suvya_person_phone': phone,
            'sp_phone': src_msg.get('mobile'),
            'sp_mobile': mobile,
            'sp_isha_phone': isha_phone,
            'sp_phone_country_code': src_msg.get('countryCode'),
            'sp_city': src_msg['city'],
            'sp_state': src_msg.get('state', None),
            'sp_state_id': state,
            'sp_country_id': country,
            'sp_zip': src_msg['zip_code'],
            'sp_nationality': getOdooCountry(env, src_msg['nationality'], metadata),
            'ie_status': getOdooIEStatus(src_msg['ie_status']),
            'iep_month': getOdooIEYear(src_msg['ie_month_year'], 'Month'),
            'iep_year': getOdooIEYear(src_msg['ie_month_year'], 'Year'),
            'iep_location': src_msg.get('ie_location', None),
            'iep_teacher': src_msg.get('ie_teacher_name', None),
            'ie_online_status': getOdooIEOnlineStatus(src_msg['ie_online_status']),
            'ie_completed_post_registration': getOdooIEPostRegistration(src_msg['ie_program_check']),
            'user_agent': src_msg.get('user_agent', None),
            'user_registered_ip': src_msg.get('user_registered_ip', None),
            'registration_source': src_msg.get('registration_source', None),
            'utm_campaign': src_msg.get('utmCampaign', None),
            'utm_source': src_msg.get('utmSource', None),
            'utm_medium': src_msg.get('utmMedium', None),
            'utm_term': src_msg.get('utmTerm', None),
            'utm_content': src_msg.get('utmContent', None),
            'sp_applied_date': src_msg.get('createdAt', None),
            'sp_call_status': getOdooCallStatus(env, src_msg),
            'sp_pre_reg_status': getOdooPreRegStatus(env, src_msg),
            'cancel_date': src_msg.get('pre_reg_cancellation_date'),
            'cancellation_reason': getOdooCancellationReason(env, src_msg['pre_reg_cancellation_reason']),
            'sp_profile_id': [(0, 0, getOdooProfile(env, src_msg))],
            'interview_general_comment_ids': getOdooComment(env, src_msg, metadata),
            'education_ids': education,
            'sdp_tagged_status': get_sdp_tagged_status(src_msg.get('sdp_tagged_status', None), env),
            'sdp_tagged_review_comments_ids': getOdooSDPTagged(env, src_msg, metadata),
            'sp_profile_status': getOdooProfileStatus(src_msg),
            'sp_health_assessment_form_status': getOdooHAFormStatus(src_msg),
            'sp_health_assessment_state': getOdooHAFormState(src_msg),
            'isha_connect': isha_connect,
            'emergency_contact_ids': getOdooEmergency(src_msg),
            'vol_dept_interview_feedback_ids': getOdooVROFeedback(env, src_msg, metadata),
            'center_volunteering_ids': volunteering_iyc,
            'local_volunteering_ids': local_volunteering,
            'pending_other_reason': getOdooPendingOtherReason(env, src_msg['application_stage_pending_reason']),
            'program_history_ids': program_history,
            'why_sadhanapada': getOdooWhySadhanapada(env, src_msg['why_sadhanapada_id']),
            'work_experience_ids': work_exp,
            'additional_skills': src_msg.get('areasContribute', None),
            'interview_done_on': src_msg.get('interview_date_time', None),
            'height': int(float(src_msg.get('height'))) if src_msg.get('height') else None,
            'weight': src_msg.get('weight'),
            'interview_concerns': getOdooConcerns(src_msg.get('interviewer_assessment', None)),
            'interview_concerns_comment': src_msg.get('action_item', None),
            'language_ids': language,
            'appointment_no_response_counter': src_msg.get('noResponseCounter', None),
            'sp_pre_reg_email_send_datetime': src_msg.get('sp_form_sent_date', None),
            'drug_usage': src_msg.get('drug_usage', None),
            'drug_treatment': src_msg.get('drugTreatment', None),
            # 'suvya_trial_period_status': getOdooTrialPeriodStatus(src_msg.get('trail_period_status')),
            'trial_feedback_ids': getOdooTPFeedback(env, src_msg, metadata),
            'interview_reviewer_decision': getOdooInterviewStatus(src_msg, 'decision'),
            'sp_interview_status': getOdooInterviewStatus(src_msg, 'status'),
            'interview_form_state': getOdooInterviewStatus(src_msg, 'form_state'),
            'application_stage': getOdooApplicationStage(src_msg, env),
            'is_in_pre_reg_stage': True,
            'interviewer_opinion': getOdooInterviewerOpinion(src_msg),
            'previous_status': getOdooPreviousStatus(src_msg, env),
            'registration_batch': getOdooRegYear(env, src_msg['sadhanapada_year']),
            'interview_opinion_by': getOdooPerson(env, src_msg['person_interviewed'], metadata),
            'health_assessment_ids': health_assessment_id,
            'ha_doctor_approval': getOdooPerson(env, src_msg['doctor_assigned'], metadata),
            'photo': profile_image,
            'id_proof': id_proof_image,
            'address_proof': add_proof_image,
            'other_liabilities': src_msg['otherLiabilities']
        })

        return values


def getOdooHACreate(env, src_msg):
    values = {}
    screening_doctor = src_msg['doctor_screening'] if src_msg['doctor_screening'] else src_msg['doctor_approval']
    values.update({
        'review_doctor_decision': getOdooDoctorDecision(src_msg['doctor_approval']),
        'screening_doctor_decision': getOdooDoctorDecision(screening_doctor)
    })
    return values


def getOdooDoctorDecision(doctor_decision):
    ret_value = None
    if doctor_decision == 'Approved':
        ret_value = 'approved'
    if doctor_decision == 'Rejected':
        ret_value = 'rejected'
    if doctor_decision == 'VCD to Decide' or doctor_decision == 'Revaluation after trial':
        ret_value = 'sp_to_decide'
    return ret_value


def getOdooHAFormState(src_msg):
    return_value = None
    if src_msg.get('not_sent_email') == 'Yes' and not src_msg.get('doctor_approval') and not src_msg.get('doctor_screening'):
        return 'partial'

    if src_msg.get('ha_form_sent') != 'Yes' and not src_msg.get('doctor_approval') and not src_msg.get('doctor_screening'):
        return 'not_sent_email'

    if src_msg.get('ha_form_sent') == 'Yes' and src_msg['doctor_approval']:
        if src_msg.get('doctor_approval') == 'Approved':
            return 'review_approved'
        if src_msg.get('doctor_approval') == 'Rejected':
            return 'review_rejected'
        if src_msg.get('doctor_approval') == 'VCD to Decide':
            return 'review_sp_to_decide'
        if src_msg.get('doctor_approval') == 'Revaluation after trial':
            return 'review_sp_to_decide'
        if src_msg.get('doctor_approval') == 'Awaiting info from participants':
            return 'awaiting_info'
        if src_msg.get('doctor_approval') == 'On Hold':
            return 'review_on_hold'
        if src_msg.get('doctor_approval') == 'Ongoing':
            return 'review_in_progress'

    if src_msg.get('ha_form_sent') == 'Yes' and src_msg.get('doctor_screening'):
        if src_msg.get('doctor_screening') == 'Approved':
            return 'screening_approved'
        if src_msg.get('doctor_screening') == 'Rejected':
            return 'screening_rejected'
        if src_msg.get('doctor_screening') == 'VCD to Decide':
            return 'screening_sp_to_decide'
        if src_msg.get('doctor_screening') == 'Revaluation after trial':
            return 'screening_sp_to_decide'
        if src_msg.get('doctor_screening') == 'Awaiting info from participants':
            return 'awaiting_info'
        if src_msg.get('doctor_screening') == 'On Hold':
            return 'screening_on_hold'
        if src_msg.get('doctor_screening') == 'Ongoing':
            return 'screening_in_progress'
        if src_msg.get('doctor_screening') == 'No Response':
            return 'reminder_sent'
    if src_msg.get('full_form_status'):
        if src_msg.get('ha_form_submitted') == 'Yes-Submitted' and not src_msg.get('doctor_approval') and not src_msg.get('doctor_screening'):
            return 'ready_screening'

    return return_value


def getOdooProfileStatus(src_msg):
    return_value = None
    if src_msg.get('reg_email_status'):
        if src_msg.get('reg_email_status') == 1:
            return_value = 'email_sent'
        else:
            return_value = 'not_sent_email'

    if src_msg.get('full_form_status'):
        if src_msg.get('full_form_status') == 1:
            return_value = 'submitted'
        else:
            return_value = 'partial'

    return return_value


def getOdooHAFormStatus(src_msg):
    return_value = None
    if src_msg.get('not_sent_email') == 'Yes' and not src_msg.get('doctor_approval') and not src_msg.get('doctor_screening'):
        return 'partial'

    if src_msg.get('ha_form_sent') != 'Yes' and not src_msg.get('doctor_approval') and not src_msg.get('doctor_screening'):
        return 'not_sent_email'

    if src_msg.get('ha_form_sent') == 'Yes' and src_msg.get('doctor_approval'):
        if src_msg.get('doctor_approval') == 'Approved':
            return 'reviewed'
        if src_msg.get('doctor_approval') == 'Rejected':
            return 'reviewed'
        if src_msg.get('doctor_approval') == 'VCD to Decide':
            return 'reviewed'
        if src_msg.get('doctor_approval') == 'Revaluation after trial':
            return 'reviewed'
        if src_msg.get('doctor_approval') == 'Awaiting info from participants':
            return 'awaiting_info'
        if src_msg.get('doctor_approval') == 'On Hold':
            return 'review_on_hold'
        if src_msg.get('doctor_approval') == 'Ongoing':
            return 'review_in_progress'

    if src_msg.get('ha_form_sent') == 'Yes' and src_msg.get('doctor_screening'):
        if src_msg.get('doctor_screening') == 'Approved':
            return 'ready_review'
        if src_msg.get('doctor_screening') == 'Rejected':
            return 'ready_review'
        if src_msg.get('doctor_screening') == 'VCD to Decide':
            return 'ready_review'
        if src_msg.get('doctor_screening') == 'Revaluation after trial':
            return 'ready_review'
        if src_msg.get('doctor_screening') == 'Awaiting info from participants':
            return 'awaiting_info'
        if src_msg.get('doctor_screening') == 'On Hold':
            return 'screening_on_hold'
        if src_msg.get('doctor_screening') == 'Ongoing':
            return 'screening_in_progress'
        if src_msg.get('') == 'No Response':
            return 'reminder_sent'
    if src_msg.get('full_form_status'):
        if src_msg.get('ha_form_submitted') == 'Yes-Submitted' and not src_msg.get('doctor_approval') and not src_msg.get('doctor_screening'):
            return 'ready_screening'

    return return_value


def getOdooPreviousStatus(src_msg, env):
    ret_value = None
    interview_status = src_msg.get('interview_status', None)
    interview_opinion = src_msg.get('interviewerOpinion', None)

    if interview_status == 'Cancelled':
        ret_value = env.ref("isha_sp_registration.application_stage_interview_stage").id
    if interview_opinion == 'Cancelled':
        ret_value = env.ref("isha_sp_registration.application_stage_registration_stage").id

    return ret_value


def getOdooApplicationStage(src_msg, env):
    ret_value = None
    sp_stage = src_msg.get('sadhanapada_status')
    interview_status = src_msg.get('interview_status', None)
    interview_opinion = src_msg.get('interviewerOpinion', None)
    if sp_stage == 'Approval after Trial':
        ret_value = env.ref("isha_sp_registration.application_stage_approval_after_trial").id
    elif sp_stage == 'Approved':
        ret_value = env.ref("isha_sp_registration.application_stage_approved").id
    elif sp_stage == 'Cancelled':
        ret_value = env.ref("isha_sp_registration.application_stage_cancelled").id
    elif sp_stage == 'Checked-In':
        ret_value = env.ref("isha_sp_registration.application_stage_checked_in").id
    elif sp_stage == 'Completed':
        ret_value = env.ref("isha_sp_registration.application_stage_completed").id
    elif sp_stage == 'DroppedOut':
        ret_value = env.ref("isha_sp_registration.application_stage_dropped_out").id
    elif sp_stage == 'Health Assessment Stage':
        ret_value = env.ref("isha_sp_registration.application_stage_health_assessment_stage").id
    elif sp_stage == 'Interview Stage':
        ret_value = env.ref("isha_sp_registration.application_stage_interview_stage").id
    elif sp_stage == 'No-Show':
        ret_value = env.ref("isha_sp_registration.application_stage_no_show").id
    elif sp_stage == 'Pre-Registration Stage':
        ret_value = env.ref("isha_sp_registration.application_stage_pre_registration_stage").id
    elif sp_stage == 'Registration Stage':
        ret_value = env.ref("isha_sp_registration.application_stage_registration_stage").id
    elif sp_stage == 'Rejected':
        ret_value = env.ref("isha_sp_registration.application_stage_rejected").id

    if interview_status == 'Cancelled' or interview_opinion == 'Cancelled':
        ret_value = env.ref("isha_sp_registration.application_stage_cancelled").id
    return ret_value


def getOdooInterviewerOpinion(src_msg):
    ret_value = None
    interview_opinion = src_msg.get('interviewerOpinion', None)
    country_of_residence = src_msg.get('country_of_residence', None)
    passport_country = src_msg.get('passport_country', None)
    if interview_opinion == 'Awaiting VRO Feedback':
        value = None
        if country_of_residence == 'India' and passport_country == 'India':
            value = 'vro'
        ret_value = 'waiting_with_vro' if value == 'vro' else 'waiting_with_oco'

    if interview_opinion == 'Pending':
        ret_value = 'pending'
    if interview_opinion == 'Rejected':
        ret_value = 'not_selected'
    if interview_opinion == 'Selected':
        ret_value = 'selected'
    if interview_opinion == 'Selected with HA':
        ret_value = 'selected_with_ha'
    if interview_opinion == 'Selected with HA & TP':
        ret_value = 'selected_with_tp_ha'
    if interview_opinion == 'Selected with Trial':
        ret_value = 'selected_with_tp'
    return ret_value


def getOdooInterviewStatus(src_msg, field):
    ret_value = None
    interview_status = src_msg.get('interview_status', None)
    country_of_residence = src_msg.get('country_of_residence', None)
    passport_country = src_msg.get('passport_country', None)
    if interview_status == 'Approval after HA':
        if field == 'decision':
            ret_value = 'approval_ha'
        elif field == 'status':
            ret_value = 'selected_with_ha'
        elif field == 'form_state':
            ret_value = 'reviewed'
    elif interview_status == 'Approval after HA & TP':
        if field == 'decision':
            ret_value = 'approval_tp_ha'
        elif field == 'status':
            ret_value = 'selected_with_tp_ha'
        elif field == 'form_state':
            ret_value = 'reviewed'
    elif interview_status == 'Approval after Trial':
        if field == 'decision':
            ret_value = 'approval_tp'
        elif field == 'status':
            ret_value = 'selected_with_tp'
        elif field == 'form_state':
            ret_value = 'reviewed'
    elif interview_status == 'Pending':
        if field == 'status':
            ret_value = 'reviewer_decision_pending'
        elif field == 'form_state':
            ret_value = 'decision_pending'
    elif interview_status == 'Rejected':
        if field == 'decision':
            ret_value = 'not_selected'
        elif field == 'status':
            ret_value = 'rejected'
        elif field == 'form_state':
            ret_value = 'reviewed'
    elif interview_status == 'Selected':
        if field == 'decision':
            ret_value = 'selected'
        elif field == 'status':
            ret_value = 'selected'
        elif field == 'form_state':
            ret_value = 'reviewed'
    elif interview_status == 'Waiting VRO Feedback':
        value = None
        if country_of_residence == 'India' and passport_country == 'India':
            value = 'vro'
        if field == 'decision':
            ret_value = 'waiting_vro_feedback' if value == 'vro' else 'waiting_oco_feedback'
        elif field == 'status':
            ret_value = 'awaiting_vro' if value == 'vro' else 'awaiting_oco'
        elif field == 'form_state':
            ret_value = 'awaiting_vro' if value == 'vro' else 'awaiting_oco'
    elif interview_status is None or interview_status == 'Cancelled' or interview_status == 'Interview Pending' or interview_status == 'New' or interview_status == 'Pending':
        if field == 'form_state':
            ret_value = 'feedback_pending' if src_msg.get('interviewerInput') is None else 'feedback_received'
    return ret_value


def getOdooTrialPeriodStatus(trail_period_status):
    return_value = None
    if trail_period_status == 'Approved after TP':
        return_value = 'approved_after_tp'
    elif trail_period_status == 'Awaiting Feedback':
        return_value = 'awaiting_feedback'
    elif trail_period_status == 'Cancelled':
        return_value = 'cancelled'
    elif trail_period_status == 'Checked-in for TP':
        return_value = 'checked_in_for_tp'
    elif trail_period_status == 'Dates Received':
        return_value = 'dates_received'
    elif trail_period_status == 'Dropped Out in TP':
        return_value = 'dropped_out_in_tp'
    elif trail_period_status == 'Later Date':
        return_value = 'later_date'
    elif trail_period_status == 'No Answer':
        return_value = 'no_answer'
    elif trail_period_status == 'Pending Other':
        return_value = 'pending_other'
    elif trail_period_status == 'Rejected after TP':
        return_value = 'rejected_after_tp'
    elif trail_period_status == 'Waiting dates from ppt':
        return_value = 'waiting_dates_from_ppt'
    return return_value


def convert_lang_proficiency(lang, env):
    if 'name' not in lang or not lang['name']:
        return None
    name = lang['name']
    speaking = lang['speaking'] if 'speaking' in lang else None
    reading = lang['reading'] if 'reading' in lang else None
    writing = lang['writing'] if 'writing' in lang else None
    typing = lang['typing'] if 'typing' in lang else None
    lang_search = env['res.lang'].search([('name', 'ilike', '%'+name+'%')])
    participant_language = None
    if lang_search:
        participant_language = {
            'language': lang_search[0].id,
            'can_read': get_lang_selection(reading),
            'can_write': get_lang_selection(writing),
            'can_speak': get_lang_selection(speaking),
            'can_type': get_lang_typing_selection(typing),
        }
    # else:
    #     raise Exception('Got no results for the given language: ' + name)

    return participant_language


def get_lang_typing_selection(str):
    if not str:
        return None
    elif str == 'Slow':
        return 'slow'
    elif str == 'Medium':
        return 'medium'
    elif str == 'Fast':
        return 'fast'
    elif str == "Don't Know":
        return 'dont_know'


def get_lang_selection(str):
    # ('beginner', 'Beginner'), ('intermediate', 'Intermediate'), ('medium', 'Medium'),
    # ('fluent', 'Fluent'), ('dont_know', "Don't Know")], string = "Can read")
    # can_write = fields.Selection(
    if not str:
        return None
    elif str == 'Beginner':
        return 'beginner'
    elif str == 'Intermediate':
        return 'intermediate'
    elif str == 'Medium':
        return 'medium'
    elif str == 'Fluent':
        return 'fluent'
    elif str == "Don't Know":
        return 'dont_know'


def convert_work_experience(exp, env):
    odoo_from_date = convert_multiple_date_formats(exp['from']) if 'from' in exp else None
    odoo_to_date = convert_multiple_date_formats(exp['to']) if 'to' in exp else None
    industry = exp['area'] if 'area' in exp else None
    industry_id = None
    if industry:
        industry = env['work.industry'].search([('name', '=', industry)])
        if len(industry) > 0:
            industry_id = industry.id
    work_exp = {
        'company': exp['company'] if 'company' in exp else None,
        'designation': exp['position'] if 'position' in exp else None,
        'tasks': exp['tasks'] if 'tasks' in exp else None,
        'industry': industry_id,
        'from_date': odoo_from_date,
        'to_date': odoo_to_date,
    }
    return work_exp


def getOdooConcerns(msg):
    concerns = None
    if msg:
        if msg == "Major concerns":
            concerns = "major"
        elif msg == "Some concerns":
            concerns = "minor"
        else:
            concerns = "none"
    return concerns


def getOdooWhySadhanapada(env, msg):
    why_sadhanapada = None
    if msg:
        if msg == "2":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_more_stable").id
        if msg == "3":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_my_health").id
        if msg == "4":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_improve_clarity").id
        if msg == "1":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_deepen_sadhana").id
        if msg == "12":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_establish_sadhana").id
        if msg == "13":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_spiritual_growth").id
        if msg == "14":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_existential_questions").id
        if msg == "15":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_improve_relationships").id
        if msg == "16":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_career_development").id
        if msg == "5":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_consecrated_space").id
        if msg == "8":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_just_be_with_sadhguru").id
        if msg == "6":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_inspired_by_sadhguru").id
        if msg == "7":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_inspired_by_participant").id
        if msg == "9":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_free_time_try").id
        if msg == "11":
            why_sadhanapada = env.ref("isha_sp_registration.why_sadhanapada_other").id
    return why_sadhanapada


def get_sdp_tagged_status(sdp_tagged_status, env):
    odoo_sdp_tagged_status = None
    if sdp_tagged_status:
        search_results = env['registration.status'].search([('name', '=', sdp_tagged_status)])
        if search_results:
            if len(search_results) == 1:
                odoo_sdp_tagged_status = search_results[0].id
        else:
            raise Exception('Got no results for the given SDP Tagged Status value: ' + sdp_tagged_status)
    return odoo_sdp_tagged_status


def convert_program_history(suvya_prog, env):
    odoo_prog_id = search_master_data(suvya_prog['name'], env, 'isha.programs', '=') if\
        'name' in suvya_prog else None
    odoo_vol = {
        'isha_program': odoo_prog_id,
        'location': suvya_prog['location'] if 'location' in suvya_prog else None,
        'teacher': suvya_prog['teacher'] if 'teacher' in suvya_prog else None,
        'program_date': convert_multiple_date_formats(suvya_prog['date']) if 'date' in suvya_prog else None,
    }
    return odoo_vol


def convert_multiple_date_formats(suvya_date):
    odoo_date = None
    if suvya_date:
        if suvya_date[0].isalpha():
            suvya_date.replace(' ', '-')
            split_date = suvya_date.split('-')
            if len(split_date) != 2:
                raise Exception('Date format not valid: ' + suvya_date)
            month = get_month_number_from_name(split_date[0])
            if not month:
                raise Exception('Date format not valid: ' + suvya_date)

            odoo_date = date(int(split_date[1]), month, 1)
        elif '-' in suvya_date:
            date_split = suvya_date.split('-')
            if len(date_split) == 2:
                odoo_date = date(int(date_split[1]), int(date_split[0]), 1)
            elif len(date_split) == 3:
                odoo_date = date.fromisoformat(suvya_date)
            else:
                raise Exception('Date format not valid: ' + suvya_date)
        else:
            try:
                suvya_year = int(suvya_date)
            except ValueError:
                raise Exception('Date format not valid: ' + suvya_date)
            odoo_date = date(suvya_year, 8, 1)
    return odoo_date


def get_month_number_from_name(month_name):
    month_name = month_name.capitalize()
    month_number = None
    if month_name:
        if month_name == 'Jan':
            month_number = 1
        elif month_name == 'Feb':
            month_number = 2
        elif month_name == 'Mar':
            month_number = 3
        elif month_name == 'Apr':
            month_number = 4
        elif month_name == 'May':
            month_number = 5
        elif month_name == 'Jun':
            month_number = 6
        elif month_name == 'Jul':
            month_number = 7
        elif month_name == 'Aug':
            month_number = 8
        elif month_name == 'Sep':
            month_number = 9
        elif month_name == 'Oct':
            month_number = 10
        elif month_name == 'Nov':
            month_number = 11
        elif month_name == 'Dec':
            month_number = 12
    return month_number


def search_master_data(suvya_val, env, model, comparator):
    odoo_val = None
    if suvya_val:
        val = suvya_val
        if comparator == 'ilike':
            val = '%' + suvya_val + '%'
        search_results = env[model].search([('name', comparator, val)])
        if search_results:
            if len(search_results) == 1 or len(search_results) > 1 and comparator == 'ilike':
                odoo_val = search_results[0].id
            elif len(search_results) > 1:
                raise Exception('Got more than one results for the given value: ' + suvya_val)
        else:
            raise Exception('Got no results for the given value: ' + suvya_val)
    return odoo_val


def getOdooPendingOtherReason(env, msg):
    pending_other_reason = None
    if msg:
        if msg == "COVID":
            pending_other_reason = env.ref("isha_sp_registration.pending_other_reason_covid").id
        if msg == "Education":
            pending_other_reason = env.ref("isha_sp_registration.pending_other_reason_education").id
        if msg == "Family not accepting":
            pending_other_reason = env.ref("isha_sp_registration.pending_other_reason_family_not_accepting").id
        if msg == "Financial problems":
            pending_other_reason = env.ref("isha_sp_registration.pending_other_reason_financial_problems").id
        if msg == "Not interested anymore":
            pending_other_reason = env.ref("isha_sp_registration.pending_other_reason_not_interested_anymore").id
        if msg == "Others":
            pending_other_reason = env.ref("isha_sp_registration.pending_other_reason_other").id
        if msg == "Personal commitments":
            pending_other_reason = env.ref("isha_sp_registration.pending_other_reason_personal_commitments").id
        if msg == "Work commitments":
            pending_other_reason = env.ref("isha_sp_registration.pending_other_reason_work_commitments").id
    return pending_other_reason


def convert_suvya_dc2type_arrays_to_odoo_many2many(suvya_refs, env, converting_func, field_string):
    vals = convert_suvya_dc2type_arrays_to_list(suvya_refs, env, converting_func, field_string)
    return [(6, 0, vals)]


def convert_volunteering_iyc(suvya_iyc, env):
    odoo_iyc = {
        # 'activity': suvya_iyc['activity'] if 'activity' in suvya_iyc else None,
        'activity_description': suvya_iyc['description'] if 'description' in suvya_iyc else None,
    }
    return odoo_iyc


def convert_local_volunteering(suvya_vol, env):
    odoo_vol = {
        # 'activity': suvya_vol['activity'] if 'activity' in suvya_vol else None,
        'activity_description': suvya_vol['description'] if 'description' in suvya_vol else None,
        'location': suvya_vol['location'] if 'location' in suvya_vol else None,
    }
    return odoo_vol


def convert_isha_connect(suvya_connect, env):
    odoo_connect = None
    if suvya_connect:
        if suvya_connect == "Only IE, no Isha volunteering":
            odoo_connect = env.ref("isha_volunteer.isha_connect_only_ie").id
        if suvya_connect == "IE and some Isha volunteering":
            odoo_connect = env.ref("isha_volunteer.isha_connect_ie_some_isha_volunteering").id
        if suvya_connect == "IE ++":
            odoo_connect = env.ref("isha_volunteer.isha_connect_ie_plus_plus").id
        if suvya_connect == "Any Ishanga training":
            odoo_connect = env.ref("isha_volunteer.isha_connect_any_ishanga").id
        if suvya_connect == "Isha Staff":
            odoo_connect = env.ref("isha_volunteer.isha_connect_isha_staff").id
        if suvya_connect == "Hatha Yoga Teacher":
            odoo_connect = env.ref("isha_volunteer.isha_connect_hatha_yoga").id
        if suvya_connect == "Drop out":
            odoo_connect = env.ref("isha_volunteer.isha_connect_drop_out").id
        if suvya_connect == "Fresher":
            odoo_connect = env.ref("isha_volunteer.isha_connect_fresher").id
        if suvya_connect == "Unemployed for over 6 months":
            odoo_connect = env.ref("isha_volunteer.isha_connect_unemployed").id
        if suvya_connect == "Government employee":
            odoo_connect = env.ref("isha_volunteer.isha_connect_government_employee").id
        if suvya_connect == "NGO":
            odoo_connect = env.ref("isha_volunteer.isha_connect_ngo").id
        if suvya_connect == "Coming as a Couple":
            odoo_connect = env.ref("isha_volunteer.isha_connect_couple").id
        if suvya_connect == "Sevadhar":
            odoo_connect = env.ref("isha_volunteer.isha_connect_sevadhar").id
        if suvya_connect == "Isha Vidhya / Home School / Samskriti Alumnus":
            odoo_connect = env.ref("isha_volunteer.isha_connect_alumnus").id
    return odoo_connect


def convert_suvya_dc2type_arrays_to_odoo_one2many(suvya_refs, env, converting_func, field_string):
    converted_list = []

    if suvya_refs:
        dict = phpserialize.loads(suvya_refs.encode(), decode_strings=True)
        if dict:
            for key in dict:
                val = dict[key]
                if val:
                    val = converting_func(val, env)
                    if val:
                        converted_list.append((0, 0, val))
                    else:
                        if field_string != 'Languages':
                            raise Exception('Failed to convert: ' + field_string)

    return converted_list


def convert_education(suvya_edu, env):
    suvya_category = suvya_edu['category'] if 'category' in suvya_edu else None

    odoo_category = None
    if suvya_category:
        # fix spelling mistakes.
        if suvya_category == 'Bachelor of Occupatoinal Therapy (B.O.T.)':
            suvya_category = 'Bachelor of Occupational Therapy (B.O.T.)'
        elif suvya_category == 'Bachelor of Homeopathic Medicine & Surgery (B.A.M.S)':
            suvya_category = 'Bachelor of Ayurvedic Medicine & Surgery (B.A.M.S)'
        elif suvya_category == 'None':
            suvya_category = 'Other'

        odoo_category = convert_edu_category(suvya_category, env)
        # odoo_category = get_selection_item_value(IshaMeditatorEduQualification.education_qualifications, suvya_category)
        if not odoo_category:
            raise Exception('Failed to find Education category.')

    # odoo_qual_group = None
    # if 'qualification_group' in suvya_edu and suvya_edu['qualification_group']:
    #     odoo_qual_group = get_selection_item_value(IshaMeditatorEduQualification.qual_groups, suvya_edu['qualification_group'])
    #     if not odoo_qual_group:
    #         raise Exception('Failed to find Education qualification group.')

    education = {
        'education_qualifications': odoo_category,
        'institution_name': suvya_edu['institution'] if 'institution' in suvya_edu else None,
        'city': suvya_edu['city'] if 'city' in suvya_edu else None,
        'specialization': suvya_edu['specialization'] if 'specialization' in suvya_edu else None,
        'passing_year': suvya_edu['passing_year'] if 'passing_year' in suvya_edu else None,
        'marks_percentage': suvya_edu['marks'] if 'marks' in suvya_edu else None,
    }
    return education


def get_selection_item_value(selection_list, search_item):
    selection_key = None
    if search_item:
        for cat in selection_list:
            if cat[1] == search_item:
                selection_key = cat[0]
                break
    return selection_key


def convert_edu_category(suvya_category, env):
    odoo_edu_category = None
    if suvya_category:
        search_results = env['educational.qualification'].search([('name', '=', suvya_category)])
        if search_results:
            if len(search_results) == 1:
                odoo_edu_category = search_results[0].id
            elif len(search_results) > 1:
                raise Exception('Got more than one results for the given Education Category value: ' + suvya_category)
        else:
            raise Exception('Got no results for the given Education Category value: ' + suvya_category)
    return odoo_edu_category


def getOdooProfile(env, src_msg):
    interview_id = None
    if src_msg['sadhanapada_status'] != 'Pre - Registration Stage':
        interview_id = [(0, 0, getOdooInterviewForm(env, src_msg))]
    values = {}
    values.update({
        'convicted_crime_details': src_msg['criminal_history'],
        'current_medication': src_msg.get('current_medication'),
        'incarceration_details': src_msg.get('incarceration_details'),
        'psychological_ailment': src_msg.get('mental_ailments'),
        'mental_ailments_in_family': src_msg.get('mental_ailments_in_family'),
        'past_medication': src_msg.get('past_medication'),
        'physical_ailment': src_msg.get('physical_ailments'),
        'physical_disabilities': src_msg.get('physical_disabilities'),
        'surgery_details': src_msg.get('surgery_details'),
        'hospitalized': src_msg.get('hospitalized'),
        'primary_nationality_details': [(0, 0, getOdooPassport(src_msg))],
        'interview_form_ids': interview_id,
        'existing_legal_cases': src_msg.get('legalCaseExist')
    })
    return values


def getOdooInterviewForm(env, src_msg):
    values = {}
    # values.update({
    #     'health_assessment_id': [(0, 0, getOdooHACreate(env, src_msg))]
    # })
    return values


def getOdooPassport(src_msg):
    values = {}
    if src_msg.get('passport_no'):
        values.update({
            'nationality_type': 'primary',
            'passport_no': src_msg.get('passport_no'),
            'passport_issue_date': src_msg.get('passport_issue_date'),
            'passport_expiry_date': src_msg.get('passport_expiry_date'),
            'passport_issue_city': src_msg.get('passport_city_of_issue'),
            'passport_issue_country': 2
        })
    return values


def getOdooEmergency(src_msg):
    values = None
    if src_msg.get('primary_emergency_contact_name'):
        values = {
            'name': src_msg.get('primary_emergency_contact_name'),
            'address': src_msg.get('primary_emergency_contact_address'),
            'phone': src_msg.get('primary_emergency_contact_phone'),
            'email': src_msg.get('primary_emergency_contact_email'),
            'relation': src_msg.get('primary_emergency_contact_relation'),
            'emergency_type': 'primary'
        }
        values = [(0, 0, values)]
    return values


def getOdooComment(env, src_msg, metadata):
    values = None
    # if src_msg['spComments']:
    #     values = [{
    #         'feedback_type': env.ref('isha_sp_registration.feedback_type_interviewer_comments').id,
    #         'is_from_import': True,
    #         'interest_id': src_msg['id'],
    #         'interviewed_by_suvya': src_msg['person_interviewed'],
    #         'value': src_msg['spComments']
    #     }]

    if src_msg['difficult_situation']:
        values = [{
            'feedback_type': env.ref('isha_sp_registration.feedback_type_interviewer_comments').id,
            'is_from_import': True,
            'interest_id': src_msg['id'],
            'interviewed_by_suvya': src_msg['person_interviewed'],
            'value': src_msg['difficult_situation'],
            'user_id': getOdooPerson(env, src_msg['person_interviewed'], metadata)
        }]

    if src_msg.get('interviewerInput'):
        values = {
            'feedback_type': env.ref('isha_sp_registration.feedback_type_interviewer_follow').id,
            'is_from_import': True,
            'interest_id': src_msg['id'],
            'interviewed_by_suvya': src_msg['person_interviewed'],
            'value': src_msg.get('interviewerInput'),
            'user_id': getOdooPerson(env, src_msg['person_interviewed'], metadata)
        }
    if values:
        return [(0, 0, values)]
    return values


def getOdooSDPTagged(env, src_msg, metadata):
    values = None
    if src_msg.get('sdp_tagged_comments'):
        values = {
            'feedback_type': env.ref('isha_sp_registration.feedback_type_sdp_tagged_comments').id,
            'is_from_import': True,
            'interest_id': src_msg.get('id'),
            'interviewed_by_suvya': src_msg.get('person_interviewed'),
            'value': src_msg.get('sdp_tagged_comments'),
            'user_id': getOdooPerson(env, src_msg['person_interviewed'], metadata)
        }
        values = [(0, 0, values)]
    return values


def getOdooVROFeedback(env, src_msg, metadata):
    values = None
    if src_msg.get('vroFeedback'):
        values = {
            'feedback_type': env.ref('isha_sp_registration.feedback_type_vro').id,
            'is_from_import': True,
            'interest_id': src_msg.get('id'),
            'interviewed_by_suvya': src_msg.get('person_interviewed'),
            'value': src_msg.get('vroFeedback'),
            'user_id': getOdooPerson(env, src_msg['person_interviewed'], metadata)
        }
        values = [(0, 0, values)]
    return values


def getOdooTPFeedback(env, src_msg, metadata):
    values = None
    if src_msg.get('trial_period_feedback'):
        values = {
            'feedback_type': env.ref('isha_sp_registration.feedback_type_tp_individual_meeting_comments').id,
            'is_from_import': True,
            'interest_id': src_msg.get('id'),
            'interviewed_by_suvya': src_msg.get('person_interviewed'),
            'value': src_msg.get('trial_period_feedback'),
            'user_id': getOdooPerson(env, src_msg['person_interviewed'], metadata)
        }
        values = [(0, 0, values)]
    return values


def getOdooGender(gender):
    if type(gender) == str and len(gender) > 0:
        gender = gender.lower()
        if gender == 'male' or gender == 'm':
            return 'M'
        elif gender == 'female' or gender == 'f':
            return 'F'
        elif gender == 'other' or gender == 'o':
            return 'O'
    return None


def getOdooCallStatus(env, src_msg):
    call_status = src_msg['call_status']
    pre_reg_status = src_msg['pre_registration_status']
    if call_status:
        if call_status == 'busy' and (pre_reg_status == 'BSP_pending' or pre_reg_status == 'cancelled' or
                                      pre_reg_status == 'duplicate' or pre_reg_status == 'new' or
                                      pre_reg_status == 'not_in_db'):
            return env.ref("isha_sp_registration.sp_call_status_not_called").id

        if call_status == 'call_back' and (pre_reg_status == 'IE_pending' or
                                           pre_reg_status == 'new' or pre_reg_status == 'pending_other'):
            return env.ref("isha_sp_registration.sp_call_status_participant_will_call_back").id

        if call_status == 'call_completed' and (pre_reg_status == 'all_set' or pre_reg_status == 'cancelled' or
                                                pre_reg_status == 'IE_pending' or pre_reg_status == 'pending_other' or
                                                pre_reg_status == 'SDP Tagged' or pre_reg_status == 'to_be_called'):
            return env.ref("isha_sp_registration.sp_call_status_call_completed").id
        elif call_status == 'call_completed' and (pre_reg_status == 'duplicate' or
                                                  pre_reg_status == 'new' or pre_reg_status == 'star_marked'):
            return env.ref("isha_sp_registration.sp_call_status_not_called").id

        if call_status == 'no_answer' and (pre_reg_status == 'all_set' or pre_reg_status == 'cancelled' or
                                           pre_reg_status == 'IE_pending' or pre_reg_status == 'pending_other'):
            return env.ref("isha_sp_registration.sp_call_status_call_completed").id
        elif call_status == 'no_answer' and (pre_reg_status == 'new' or pre_reg_status == 'not_in_db' or
                                             pre_reg_status == 'SDP Tagged' or pre_reg_status == 'to_be_called'):
            return env.ref("isha_sp_registration.sp_call_status_no_answer").id
        elif call_status == 'no_answer' and (pre_reg_status == 'duplicate' or pre_reg_status == 'star_marked'):
            return env.ref("isha_sp_registration.sp_call_status_not_called").id
        elif call_status == 'no_answer' and pre_reg_status == 'No response':
            return env.ref("isha_sp_registration.sp_call_status_unreachable").id

        if call_status == 'not_called' and (pre_reg_status == 'cancelled' or pre_reg_status == 'duplicate' or
                                            pre_reg_status == 'new' or pre_reg_status == 'SDP Tagged' or
                                            pre_reg_status == 'star_marked'):
            return env.ref("isha_sp_registration.sp_call_status_not_called").id
        elif call_status == 'not_called' and (pre_reg_status == 'all_set' or pre_reg_status == 'IE_pending'):
            return env.ref("isha_sp_registration.sp_call_status_call_completed").id
        elif call_status == 'not_called' and pre_reg_status == 'No response':
            return env.ref("isha_sp_registration.sp_call_status_unreachable").id

        if call_status == 'participant_call_back' and (pre_reg_status == 'all_set' or pre_reg_status == 'new' or
                                                       pre_reg_status == 'pending_other' or
                                                       pre_reg_status == 'IE_pending'):
            return env.ref("isha_sp_registration.sp_call_status_participant_will_call_back").id
        elif call_status == 'participant_call_back' and pre_reg_status == 'SDP Tagged':
            return env.ref("isha_sp_registration.sp_call_status_call_completed").id

        if call_status == 'unreachable' and (pre_reg_status == 'cancelled' or pre_reg_status == 'No response' or
                                             pre_reg_status == 'not_in_db'):
            return env.ref("isha_sp_registration.sp_call_status_unreachable").id
        elif call_status == 'unreachable' and (pre_reg_status == 'all_set' or pre_reg_status == 'IE_pending' or
                                               pre_reg_status == 'pending_other'):
            return env.ref("isha_sp_registration.sp_call_status_call_completed").id
        elif call_status == 'unreachable' and (pre_reg_status == 'duplicate' or pre_reg_status == 'new'):
            return env.ref("isha_sp_registration.sp_call_status_not_called").id

        if call_status == 'wrong_number' and pre_reg_status == 'IE_pending':
            return env.ref("isha_sp_registration.sp_call_status_call_completed").id
        elif call_status == 'wrong_number' and (pre_reg_status == 'IE_pending' or pre_reg_status == 'new' or
                                                pre_reg_status == 'No response' or pre_reg_status == 'not_in_db'):
            return env.ref("isha_sp_registration.sp_call_status_wrong_number").id
    return None


def getOdooPreRegStatus(env, src_msg):
    call_status = src_msg['call_status']
    pre_reg_status = src_msg['pre_registration_status']
    if len(call_status) > 0:
        if call_status == 'busy' and (pre_reg_status == 'BSP_pending' or pre_reg_status == 'duplicate' or
                                      pre_reg_status == 'new' or pre_reg_status == 'not_in_db'):
            return env.ref("isha_sp_registration.sp_pre_reg_status_duplicate").id
        elif call_status == 'busy' and pre_reg_status == 'cancelled':
            return env.ref("isha_sp_registration.sp_pre_reg_status_new").id

        if call_status == 'call_back' and pre_reg_status == 'IE_pending':
            return env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending").id
        elif call_status == 'call_back' and pre_reg_status == 'new':
            return env.ref("isha_sp_registration.sp_pre_reg_status_new").id
        elif call_status == 'call_back' and pre_reg_status == 'pending_other':
            return env.ref("isha_sp_registration.sp_pre_reg_status_pending_other").id

        if call_status == 'call_completed' and (pre_reg_status == 'all_set' or pre_reg_status == 'SDP Tagged'):
            return env.ref("isha_sp_registration.sp_pre_reg_status_all_set").id
        elif call_status == 'call_completed' and (pre_reg_status == 'cancelled' or pre_reg_status == 'pending_other' or
                                                  pre_reg_status == 'to_be_called'):
            return env.ref("isha_sp_registration.sp_pre_reg_status_pending_other").id
        elif call_status == 'call_completed' and pre_reg_status == 'duplicate':
            return env.ref("isha_sp_registration.sp_pre_reg_status_duplicate").id
        elif call_status == 'call_completed' and pre_reg_status == 'IE_pending':
            return env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending").id
        elif call_status == 'call_completed' and (pre_reg_status == 'new' or pre_reg_status == 'star_marked'):
            return env.ref("isha_sp_registration.sp_pre_reg_status_new").id

        if call_status == 'no_answer' and pre_reg_status == 'all_set':
            return env.ref("isha_sp_registration.sp_pre_reg_status_all_set").id
        elif call_status == 'no_answer' and (pre_reg_status == 'cancelled' or pre_reg_status == 'pending_other'):
            return env.ref("isha_sp_registration.sp_pre_reg_status_pending_other").id
        elif call_status == 'no_answer' and pre_reg_status == 'duplicate':
            return env.ref("isha_sp_registration.sp_pre_reg_status_duplicate").id
        elif call_status == 'no_answer' and pre_reg_status == 'IE_pending':
            return env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending").id
        elif call_status == 'no_answer' and pre_reg_status == 'No response':
            return env.ref("isha_sp_registration.sp_pre_reg_status_no_response").id
        elif call_status == 'no_answer' and (pre_reg_status == 'new' or pre_reg_status == 'not_in_db' or
                                             pre_reg_status == 'SDP Tagged' or pre_reg_status == 'star_marked' or
                                             pre_reg_status == 'to_be_called'):
            return env.ref("isha_sp_registration.sp_pre_reg_status_new").id

        if call_status == 'not_called' and pre_reg_status == 'all_set':
            return env.ref("isha_sp_registration.sp_pre_reg_status_all_set").id
        elif call_status == 'not_called' and (pre_reg_status == 'cancelled' or pre_reg_status == 'new' or
                                              pre_reg_status == 'SDP Tagged' or pre_reg_status == 'star_marked'):
            return env.ref("isha_sp_registration.sp_pre_reg_status_new").id
        elif call_status == 'not_called' and pre_reg_status == 'duplicate':
            return env.ref("isha_sp_registration.sp_pre_reg_status_duplicate").id
        elif call_status == 'not_called' and pre_reg_status == 'IE_pending':
            return env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending").id
        elif call_status == 'not_called' and pre_reg_status == 'No response':
            return env.ref("isha_sp_registration.sp_pre_reg_status_no_response").id

        if call_status == 'participant_call_back' and (pre_reg_status == 'all_set' or pre_reg_status == 'SDP Tagged'):
            return env.ref("isha_sp_registration.sp_pre_reg_status_all_set").id
        elif call_status == 'participant_call_back' and pre_reg_status == 'IE_pending':
            return env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending").id
        elif call_status == 'participant_call_back' and pre_reg_status == 'new':
            return env.ref("isha_sp_registration.sp_pre_reg_status_new").id
        elif call_status == 'participant_call_back' and pre_reg_status == 'pending_other':
            return env.ref("isha_sp_registration.sp_pre_reg_status_pending_other").id

        if call_status == 'unreachable' and (pre_reg_status == 'cancelled' or pre_reg_status == 'No response' or
                                             pre_reg_status == 'not_in_db'):
            return env.ref("isha_sp_registration.sp_pre_reg_status_no_response").id
        elif call_status == 'unreachable' and pre_reg_status == 'all_set':
            return env.ref("isha_sp_registration.sp_pre_reg_status_all_set").id
        elif call_status == 'unreachable' and pre_reg_status == 'duplicate':
            return env.ref("isha_sp_registration.sp_pre_reg_status_duplicate").id
        elif call_status == 'unreachable' and pre_reg_status == 'IE_pending':
            return env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending").id
        elif call_status == 'unreachable' and pre_reg_status == 'new':
            return env.ref("isha_sp_registration.sp_pre_reg_status_new").id
        elif call_status == 'unreachable' and pre_reg_status == 'pending_other':
            return env.ref("isha_sp_registration.sp_pre_reg_status_pending_other").id

        if call_status == 'wrong_number' and pre_reg_status == 'IE_pending':
            return env.ref("isha_sp_registration.sp_pre_reg_status_ie_pending").id
        elif call_status == 'wrong_number' and (pre_reg_status == 'IE_pending' or pre_reg_status == 'new' or
                                                pre_reg_status == 'No response' or pre_reg_status == 'not_in_db'):
            return env.ref("isha_sp_registration.sp_pre_reg_status_new").id


def getOdooCancellationReason(env, cancellation_reason):
    if cancellation_reason:
        if cancellation_reason == 'Education':
            return env.ref("isha_sp_registration.cancellation_reason_education").id
        elif cancellation_reason == 'Family not accepting':
            return env.ref("isha_sp_registration.cancellation_reason_family_not_accepting").id
        elif cancellation_reason == 'Financial problems':
            return env.ref("isha_sp_registration.cancellation_reason_financial_problems").id
        elif cancellation_reason == 'Not interested anymore':
            return env.ref("isha_sp_registration.cancellation_reason_not_interested_anymore").id
        elif cancellation_reason == 'Others':
            return env.ref("isha_sp_registration.cancellation_reason_other").id
        elif cancellation_reason == 'Personal commitments':
            return env.ref("isha_sp_registration.cancellation_reason_personal_commitments").id
        elif cancellation_reason == 'Work commitments':
            return env.ref("isha_sp_registration.cancellation_reason_work_commitments").id
    return None


def getOdooIEStatus(ie_status):
    if type(ie_status) == str and len(ie_status) > 0:
        if ie_status == 'COMPLETED':
            return 'completed'
        elif ie_status == 'IE_ONLINE_COMPLETED_WAITING':
            return 'only_ie_online'
        elif ie_status == 'NOT COMPLETED' or ie_status == 'NOT_COMPLETED' or ie_status == 'REGISTERED':
            return 'not_completed'
    return None


def getOdooIEOnlineStatus(ie_online_status):
    if type(ie_online_status) == str and len(ie_online_status) > 0:
        if ie_online_status == 'Completed':
            return 'yes'
        elif ie_online_status == 'Pending':
            return 'no'
    return None


def getOdooIEPostRegistration(ie_online_status):
    if str(ie_online_status) == '1':
        return 'yes'
    elif str(ie_online_status) == '0':
        return 'incorrect'
    return None


def getOdooMaritalStatus(marital_status):
    if type(marital_status) == str and len(marital_status) > 0:
        if marital_status == 'Unmarried':
            return 'unmarried'
        elif marital_status == 'Married':
            return 'married'
        elif marital_status == 'Divorced':
            return 'divorced'
        elif marital_status == 'Single':
            return 'separated'
        elif marital_status == 'Widower':
            return 'widower'
        elif marital_status == 'Widow' or marital_status == 'Widowed':
            return 'widow'
    return None


def getOdooIEYear(ie_date, date_format):
    if ie_date:
        split_date = ie_date.split('-')
        if date_format == 'Year':
            return str(int(split_date[0]))
        if date_format == 'Month':
            return str(int(split_date[1]))
    return False


def getOdooCountry(env, suvya_country, metadata):
    odoo_country = None
    iso2 = None
    if not suvya_country:
        return None
    suvya_country = suvya_country.upper()
    if suvya_country in metadata['iso2']:
        iso2 = metadata['iso2'][suvya_country]
    elif suvya_country in metadata['correction'] and metadata['correction'][suvya_country] in metadata['iso2']:
        iso2 = metadata['iso2'][metadata['correction'][suvya_country]]

    if iso2:
        country_id = env['res.country'].search([('code', '=', iso2)], limit=1)
        if len(country_id) != 0:
            odoo_country = country_id[0].id
    if not odoo_country:
        raise Exception('Got no results for the given Country value: ' + suvya_country)
    return odoo_country


def getOdooState(env, country, state):
    rec_state = None
    if country and state:
        search = env['res.country.state'].search([('name', 'ilike', '%' + state + '%'), ('country_id', '=', country)])
        rec_state = search[0].id if search else None
    return rec_state


def getOdooPerson(env, person, metadata):
    odoo_person = None
    sp_user = None
    if not person:
        return None
    if person in metadata['sp_users']:
        sp_user = metadata['sp_users'][person]

    if sp_user:
        user_id = env['res.users'].search([('login', '=', sp_user)], limit=1)
        if len(user_id) != 0:
            odoo_person = user_id[0].id
    return odoo_person


def getOdooRegYear(env, sadhanapada_year):
    rec_sp_year = None
    if sadhanapada_year:
        search = env['registration.batch'].search([('name', 'ilike', '%' + sadhanapada_year + '%')])
        rec_sp_year = search[0].id if search else None
    return rec_sp_year


def convert_suvya_dc2type_arrays_to_list(suvya_refs, env, converting_func, field_string):
    vals = []
    if suvya_refs:
        dict = phpserialize.loads(suvya_refs.encode(), decode_strings=True)
        if dict:
            for key in dict:
                val = dict[key]
                if val:
                    val = converting_func(val, env)
                    if val:
                        vals.append(val)
                    else:
                        raise Exception('Failed to convert: ' + field_string)
    return vals


# Image Import
def encode_image(image_1920):
    image_1920 = base64.b64encode(image_1920)
    return image_1920

def get_image_document(doc_id, sp_config):
    image_url = sp_config['SUVYA_API_BASE_URL'] + '/documents_sp/download-my-document/' + str(doc_id)
    _logger.info("Document image url: " + image_url)
    image_res = requests.get(image_url)
    #if not image_res or image_res.status_code != 200:
    #    raise Exception('Document image not received for url: ' + image_url)

    image = encode_image(image_res.content)
    return image
