from .. import ContactProcessor
from ..controllers import sso_auth_endpoint
from ..isha_base_importer import Configuration

null = None
false = False
true = True
class IECMegaPgmProcessor:

    system_id = 40

    def getGoldenFormat(self, msg, iso2, correction):
        sso_id = msg['profileId']
        basic_profile = ContactProcessor.replaceEmptyString(msg['basicProfile'])
        phone_dict = ContactProcessor.replaceEmptyString(msg['basicProfile']['phone'])
        extended_profile = ContactProcessor.replaceEmptyString(msg['extendedProfile'])

        name = basic_profile['firstName']
        if basic_profile['lastName'] and len(basic_profile['lastName']) > 0:
            name = name + " " + basic_profile['lastName']
        phone = None
        if phone_dict['number']:
            phone = '+' + phone_dict['countryCode'] if phone_dict['countryCode'] else ''
            phone += phone_dict['number']
        whatsapp_cc = None
        whatsapp_number = None
        if basic_profile.get('whatsapp', False):
            whatsapp_cc = basic_profile.get('whatsapp').get('countryCode', None)
            whatsapp_number = basic_profile.get('whatsapp').get('number', None)

        email = basic_profile['email']
        gender = ContactProcessor.getgender(basic_profile['gender'])
        dob = basic_profile['dob'] if basic_profile['dob'] else None
        nationality = extended_profile['nationality']
        passport = extended_profile['passportNum'] if extended_profile['passportNum'] else None
        pan = extended_profile['pan'] if extended_profile['pan'] else None
        street = None
        street2 = None
        state = None
        city = None
        country = None
        zip = None
        if len(msg['addresses']) > 0:
            address_dict = ContactProcessor.replaceEmptyString(msg['addresses'][0])
            street = address_dict['addressLine1']
            street2 = address_dict['addressLine2']
            if address_dict['townVillageDistrict']:
                if street2 and type(street2) == str:
                    street2 = street2 + ', ' + address_dict['townVillageDistrict']
                else:
                    street2 = address_dict['townVillageDistrict']
            city = address_dict['city']
            state = address_dict['state']
            country = address_dict['country']
            zip = address_dict['pincode']
        odooRec = {
            'system_id': self.system_id,
            'local_contact_id': sso_id,
            'active': True,
            'name': name, 'display_name': name, 'prof_dr': None,
            'phone': phone, 'phone2': None,
            'phone3': None, 'phone4': None,
            'whatsapp_number': whatsapp_number, 'whatsapp_country_code': whatsapp_cc,
            'email': email, 'email2': None, 'email3': None,
            'street': street, 'street2': street2, 'city': city, 'state': state, 'country': country,
            'zip': zip,
            'street2_1': None, 'street2_2': None, 'city2': None, 'state2': None, 'country2': None, 'zip2': None,
            'work_street1': None, 'work_street2': None, 'work_city': None,
            'work_state': None, 'work_country': None, 'work_zip': None,
            'gender': gender, 'occupation': None, 'marital_status': None, 'dob': dob,
            'nationality': nationality, 'deceased': None, 'contact_type': None, 'companies': None,
            'aadhaar_no': None, 'pan_no': pan, 'passport_no': passport,
            'dnd_phone': None, 'dnd_email': None, 'dnd_postmail': None, 'sso_id': sso_id,
            'has_charitable_txn': True
        }

        return ContactProcessor.validateValues(odooRec, iso2, correction)

    def getGoldenFormatTxn(self, msg):
        msg_dict = {
            'sso_id': msg.get('ssoId', None),
            'prs_id': msg.get('prsId', None),
            'schedule_id': msg.get('scheduleId', None),
            'session_id':msg.get('batchId',None),
            'reg_date': msg.get('regDate', None),
            'pgm_amt': msg.get('pgmAmt', None),
            'pgm_curr': msg.get('pgmCurr', None),
            'txn_id': msg.get('txnId', None),
            'quota_cat': msg.get('userType', None),
            'ip_country': msg.get('ctry', None),
            'status': msg.get('cStatus', None),
        }
        if msg.get('fromScheduleId', None) and msg.get('fromScheduleId', None) != 0:
            msg_dict['from_schedule_id'] = msg.get('fromScheduleId', None)
        if msg.get('fromBatchId', None) and msg.get('fromBatchId', None) != 0:
            msg_dict['from_session_id'] = msg.get('fromBatchId', None)
        if msg_dict['session_id'] == 0:
            msg_dict.pop('session_id',None)
        if msg_dict['schedule_id'] == 0:
            msg_dict.pop('schedule_id',None)
        return msg_dict

    def update_no_marketing_flag(self, odoo_rec, txn_dict, profile_data, env):
        url = env['ir.config_parameter'].sudo().get_param('web.base.url')
        # check if it is s1
        if 'ishangam.isha.in' in url and profile_data['basicProfile']['countryOfResidence'] != 'IN':
            odoo_rec['no_marketing'] = True
        # check if it is s2
        elif 'ishangam.isha.us' in url and profile_data['basicProfile']['countryOfResidence'] == 'IN':
            odoo_rec['no_marketing'] = True

    def process_message(self, src_msg, env, metadata):
        src_msg_dict = src_msg

        txn_dict = self.getGoldenFormatTxn(src_msg_dict)
        try:
            sso_config = Configuration('SSO')
            pms_dict ={
                'SSO_PMS_ENDPOINT':sso_config['SSO_PMS_ENDPOINT'],
                'SSO_API_SYSTEM_TOKEN':sso_config['SSO_API_SYSTEM_TOKEN'],
                'SSO_LEGEL_ENTITY': env['ir.config_parameter'].sudo().get_param('isha_crm.ieco_pms_entity')
            }
            if not pms_dict['SSO_LEGEL_ENTITY']:
                pms_dict['SSO_LEGEL_ENTITY'] = 'IFINC'

            profile_data = sso_auth_endpoint.sso_get_full_data(pms_dict,txn_dict['sso_id'])

            # If consent is not set we don't have the PII
            if 'no_consent' not in profile_data:
                msg_dict = self.getGoldenFormat(profile_data, metadata['iso2'],metadata['correction'])
                self.update_no_marketing_flag(msg_dict,txn_dict,profile_data,env)
                contact = env['res.partner'].sudo().create_internal(msg_dict)
                txn_dict['partner_id'] = contact[0].id
                txn_dict['name'] = msg_dict['name']
                txn_dict['phone'] = msg_dict['phone']
                txn_dict['phone_country_code'] = msg_dict.get('phone_country_code',None)
                txn_dict['email'] = msg_dict['email']

            flag_dict = self.createPgmInternal(env, txn_dict)
            env.cr.commit()
            return flag_dict
        finally:
            pass

    def createPgmInternal(self, ENV, src_rec):
        txnModel = ENV['iec.mega.pgm']

        recs = txnModel.search([('sso_id', '=', src_rec['sso_id'])])

        prs_reg_rec = recs.filtered(lambda x: x.prs_id == src_rec['prs_id'])
        inq_rec = None
        if prs_reg_rec:
            prs_reg_rec.write(src_rec)
        else:
            inq_rec = recs.filtered(lambda x: x.status == 'inq')
            if inq_rec:
                inq_rec.write(src_rec)

        if not prs_reg_rec and not inq_rec:
            rec = txnModel.create(src_rec)

        active_recs = recs.filtered(lambda x: x.status in ['A', 'X'])
        if active_recs and ENV.ref('isha_crm.res_partner_pgm_tag_20').id not in active_recs[0].partner_id.pgm_tag_ids.ids:
            active_recs[0].partner_id.pgm_tag_ids = [(4,ENV.ref('isha_crm.res_partner_pgm_tag_20').id)]
            active_recs[0].partner_id.iecso_r_date = active_recs[0].create_date.strftime("%Y-%m-%d")
            try:
                if active_recs[0].reg_date.strftime("%Y-%m-%d") > active_recs[0].partner_id.last_txn_date.strftime("%Y-%m-%d"):
                    active_recs[0].partner_id.last_txn_date = active_recs[0].reg_date.strftime("%Y-%m-%d")
            except:
                pass
        else:  # There is no active records
            cancel_recs = recs.filtered(lambda x: x.status in ['C'])
            if cancel_recs and ENV.ref('isha_crm.res_partner_pgm_tag_20').id in cancel_recs[0].partner_id.pgm_tag_ids.ids:
                cancel_recs[0].partner_id.pgm_tag_ids = [(3,ENV.ref('isha_crm.res_partner_pgm_tag_20').id)]
            try:
                if cancel_recs and cancel_recs[0].reg_date.strftime("%Y-%m-%d") > cancel_recs[0].partner_id.last_txn_date.strftime("%Y-%m-%d"):
                    cancel_recs[0].partner_id.last_txn_date = cancel_recs[0].reg_date.strftime("%Y-%m-%d")
            except:
                pass
        flag_dict = {'flag': True, 'rec': recs}
        return flag_dict
