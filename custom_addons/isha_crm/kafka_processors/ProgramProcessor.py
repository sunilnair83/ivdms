from .. import TxnUpdater


class ProgramProcessor:

    def process_message(self, msg, env, metadata):
        formatConverter = metadata['programFormatConverter']

        try:
            pgmUpdater = TxnUpdater.ProgramAttUpdater()
            #logger.debug(msg)
            flag_dict = pgmUpdater.createPgmInternal(env, formatConverter.getOdooFormat(msg))
            env.cr.commit()
            return flag_dict
        finally:
            pass

