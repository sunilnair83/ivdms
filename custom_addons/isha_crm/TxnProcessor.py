from datetime import datetime, date, timedelta
from . import ContactProcessor
from dateutil.tz import *

null = None
false = False
true = True

def getDateFromTimestamp(time):
    time = float(time)
    return datetime.utcfromtimestamp(time).strftime('%Y-%m-%dT%H:%M:%SZ')

def getDateTimeField(src_date):
    return datetime.strptime(src_date, '%Y-%m-%dT%H:%M:%SZ').strftime('%Y-%m-%d')
def getDateTimeFromDateField(src_date):
    return datetime.strptime(src_date, '%Y-%m-%d').strftime('%Y-%m-%d')

def convertToUtc(src_date):
    local = gettz('Asia/Kolkata')
    ist_time = src_date.replace(tzinfo = local)
    utc = tzutc()
    utc_time = ist_time.astimezone(utc)
    return getFormatedDate(utc_time)

def getFormatedDate(src_date):
    return src_date.strftime('%Y-%m-%dT%H:%M:%SZ')

def replaceEmptyString(src_msg):
    for x in src_msg:
        if src_msg[x] == '':
            src_msg[x] = None
    return src_msg


class Program:
    system_id = 6
    def getOdooFormat(self, msg):
        msg = replaceEmptyString(msg['payload'])
        try:
            sd = datetime.strptime(msg['Pam_Start_Date'], '%d/%m/%Y') if msg['Pam_Start_Date'] else None
            sd = sd.strftime("%Y-%m-%d") if sd else None
        except:
            sd = None

        pgm_schedule_info={
            'teacher_id' : msg['Pam_Teacher_Id'],
            'teacher_name' : msg['teacher_name'],
            'center_id':msg['Pam_Center_Id'],
            'center_name':msg['center_name'],
            'location':None,
            'country':msg['country_name'],
            'remarks':None,
            'schedule_name':msg['schedule_name'],
            'start_date':msg['Pam_Start_Date'],
            'end_date':msg['end_date'],
            'program_schedule_guid':None,
            'localScheduleId':None,
        }
        odooRec = {
            'system_id':self.system_id,
            'local_trans_id': 'AL-FRST-'+str(msg['Pam_Serial_Number']),
            'local_contact_id':msg['Pam_Serial_Number'],
            'reg_status':'COMPLETED',
            'reg_date_time':None,
            'program_schedule_info':pgm_schedule_info,
            'local_program_type_id':msg['Pam_Program_Id'],
            'program_name': msg['schedule_name'], #' '.join(filter(None, [msg['schedule_name'],msg['center_name'],str(sd)])),
            'start_date':sd,
            'teacher_name':msg['teacher_name'],
            'center_name':msg['center_name']
        }
        return odooRec

class ProgramAdv:
    system_id = 6
    def getOdooFormat(self, msg):
        msg = replaceEmptyString(msg['payload'])
        try:
            sd = datetime.strptime(msg['Pap_Start_Date'], '%d/%m/%Y') if msg['Pap_Start_Date'] else None
            sd = sd.strftime("%Y-%m-%d") if sd else None
        except:
            sd = None

        pgm_schedule_info={
            'teacher_id' : msg['Pap_Teacher_Id'],
            'teacher_name' : msg['teacher_name'],
            'center_id':msg['Pap_Center_Id'],
            'center_name':msg['center_name'],
            'location':None,
            'country':msg['country_name'],
            'remarks':None,
            'schedule_name':msg['schedule_name'],
            'start_date':msg['Pap_Start_Date'],
            'end_date':msg['end_date'],
            'program_schedule_guid':None,
            'localScheduleId':None,
        }
        odooRec = {
            'system_id':self.system_id,
            'local_trans_id': 'AL-ADV-'+str(msg['Pap_Serial_Number']),
            'local_contact_id':msg['Pap_Pam_Serial_Number'],
            'reg_status':'COMPLETED',
            'reg_date_time':None,
            'program_schedule_info':pgm_schedule_info,
            'local_program_type_id':msg['Pap_Program_Id'],
            'program_name': msg['schedule_name'], #' '.join(filter(None, [msg['schedule_name'],msg['center_name'],str(sd)])),
            'start_date':sd,
            'teacher_name':msg['teacher_name'],
            'center_name':msg['center_name']
        }
        return odooRec


class SVROTxn:
    system_id = 17

    def getOdooForamt(self, src_msg):

        odooRec = {
            'local_trans_id' : src_msg['id'],
            'local_contact_id' : src_msg['person_id'],
            'category' : src_msg['visit_category'],
            'purpose_of_visit' : src_msg['purpose_of_visit'],
            'location' : 'IYC',
            'type' : 'Ashram Volunteering',
            'actual_check_in': src_msg['check_in_date'],
            'actual_check_out' : src_msg['check_out_date'],
            'scheduled_check_out' : src_msg['departure_date'],
            'system_id' : self.system_id,
            'active' : 'true',
            'modified_at' : getDateFromTimestamp(src_msg['sys_modified_at'])
        }

class SVROInfoTxn:
    system_id = 17

    def getOdooFormat(self, src_msg):

        odooRec = {
            'local_trans_id' : src_msg['id'],
            'department' : src_msg['department'],
            'from_date' : src_msg['from_date'],
            'to_date' : src_msg['to_date'],
            'system_id' : self.system_id,
            'active' : 'true'
        }
        return odooRec

class CICOTxn:
    system_id = 15

    def getOdooFormat(self, src_msg):
        odooRec = {
            'system_id':self.system_id,
            'local_trans_id': src_msg['record_id'],
            'local_contact_id': src_msg['record_id'],
            'program_name': src_msg['programname'],
            'program_type': src_msg['programtype'],
            'visit_purpose': src_msg['purposevisit'],
            'visit_category': src_msg['category'],
            'stay_area': src_msg['stayarea'],
            'check_in_date': src_msg['checkindate'],
            'check_out_date': src_msg['checkoutdate'],
            'schedule_departure_date': src_msg['departuredate'],
            'created_at': src_msg['created_date'],
            'created_by': src_msg['created_by'],
            'modified_at': src_msg['modified_date'],
            'modified_by': src_msg['modified_by']

        }

        return odooRec

class IlaProgramTxn:
    system_id = 16

    def getOdooFormat(self,src_msg):
        pgm_schedule_info = {
            'teacher_id': None,
            'teacher_name': None,
            'center_id': None,
            'center_name': None,
            'location':'ASHRAM',
            'country':'IN',
            'remarks': None,
            'schedule_name': src_msg['batch_applied'],
            'start_date': src_msg['start_date'],
            'end_date': src_msg['start_date'],
            'program_schedule_guid': None,
            'localScheduleId': None,
        }
        odooRec = {
            'system_id': self.system_id,
            'local_trans_id': src_msg['local_id'],
            'local_contact_id': src_msg['local_id'],
            'reg_status':src_msg['registration_status'],
            'reg_date_time': None,
            'program_schedule_info': pgm_schedule_info,
            'local_programtype_id':src_msg['insight_program'],
            'program_name':src_msg['batch_applied'],
            'start_date':src_msg['start_date'],
            'teacher_name': None,
            'center_name': None
        }


        return odooRec

class HYSProgramTxn:
    system_id = 16

    def getOdooFormat(self, src_msg):

        pgm_schedule_info={
            'teacher_id' : None,
            'teacher_name' : None,
            'center_id':None,
            'center_name':None,
            'location':'ASHRAM-ADIYOGI ALAYAM',
            'country':'IN',
            'remarks':None,
            'schedule_name':src_msg['batch_applied'],
            'start_date':src_msg['start_date'],
            'end_date':None,
            'program_schedule_guid':None,
            'localScheduleId':None,
        }
        odooRec = {
            'system_id':self.system_id,
            'local_trans_id': src_msg['local_contact_id'],
            'local_contact_id': src_msg['local_contact_id'],
            'reg_status':src_msg['status'],
            'reg_date_time':None,
            'program_schedule_info':pgm_schedule_info,
            'local_program_type_id' : "HYS Training",
            'program_name':src_msg['batch_applied'],
            'start_date': src_msg['start_date'],

        }


        return odooRec

class PRSProgramTxn:
    system_id = 13

    def parseDateTime(self, date_str):
        try:
            date_parsed = datetime.strptime(date_str, '%Y-%m-%d %H:%M') if date_str else None
            date_parsed = str(date_parsed) if date_parsed else None
        except:
            try:
                date_parsed = datetime.strptime(date_str, '%Y-%m-%d') if date_str else None
                date_parsed = str(date_parsed) if date_parsed else None
            except:
                date_parsed = None

        return date_parsed

    def getDateTime(self, date_str):
        try:
            date_parsed = datetime.strptime(date_str, '%Y-%m-%d %H:%M:%S') - timedelta(hours=5, minutes=30) if date_str else None
            date_parsed = str(date_parsed) if date_parsed else None
        except:
            try:
                date_parsed = datetime.strptime(date_str, '%Y-%m-%d %H:%M') - timedelta(hours=5, minutes=30) if date_str else None
                date_parsed = str(date_parsed) if date_parsed else None
            except:
                date_parsed = None

        return date_parsed

    def getEventAttDatetime(self, src_dt):
        try:
            return str(datetime.strptime(src_dt, '%d-%m-%Y %H:%M:%S') - timedelta(hours=5, minutes=30))
        except:
            return None
    def getDateFromDateTime(self, date_time):
        try:
            return datetime.strptime(date_time, '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
        except:
            return None

    def getLastTxnDate(self, datetime):
        last_txn_date = None
        try:
            last_txn_date =  datetime.split(' ')[0]
        except:
            pass
        return last_txn_date

    def getFirstSunday(self, rsvp_month_year):
        if rsvp_month_year and type(rsvp_month_year)==str:
            # rsvp_month_year comes as mm-yy we need to return the first sunday for the corresponding month year
            try:
                dt_split = rsvp_month_year.split('-')
                dt = date(int('20' + dt_split[1]), int(dt_split[0]), 1)
                dt += timedelta(days=6 - dt.weekday())
                return str(dt)
            except Exception as ex:
                return False

    def getOdooFormat(self,src_msg):
        src_msg = replaceEmptyString(src_msg)
        try:
            sd = self.getFirstSunday(src_msg['rsvp_month_year']) if 'rsvp_month_year' in src_msg and src_msg['rsvp_month_year'] else None
            if not sd:
                sd = datetime.strptime(src_msg['start_date'], '%d-%m-%Y') if src_msg['start_date'] else None
                sd = sd.strftime('%Y-%m-%d') if sd else None
        except:
            sd = None

        try:
            ar = self.parseDateTime(src_msg['arrival_date'])
        except:
            ar = None

        try:
            dp = self.parseDateTime(src_msg['departure_date'])
        except:
            dp = None
        email = ContactProcessor.getTokenizedVersion(src_msg['email'], ',')
        pgm_schedule_info={
            'teacher_id' : None,
            'teacher_name' : None,
            'center_id': None,
            'center_name': None,
            'location':src_msg['program_location'],
            'country':'IN',
            'remarks':None,
            'program_id':src_msg['program_id'],
            'schedule_name':src_msg['program_name'],
            'start_date':src_msg['start_date'],
            'end_date':src_msg['end_date'],
            'arrival_date': src_msg['arrival_date'],
            'departure_date': src_msg['departure_date'],
            'program_schedule_guid':None,
            'localScheduleId':None
        }
        phone = src_msg['phone_country_code'] if src_msg['phone_country_code'] else ''
        phone += src_msg['mobile'] if src_msg['mobile'] else ''
        if phone == '':
            phone = None

        odooRec = {
            'system_id':self.system_id,
            'local_trans_id': src_msg['_id'],
            'local_contact_id': src_msg['_id'],
            'reg_status' : src_msg['status'] if 'status' in src_msg else None,
            'reg_date_time':src_msg['created_date'],
            'program_schedule_info':pgm_schedule_info,
            'local_program_type_id': src_msg['program_name'],
            'program_name': src_msg['program_name'],
            'start_date': sd,
            'event_attendance_date':self.getEventAttDatetime(src_msg['event_attendance_date']) if 'event_attendance_date' in src_msg else None,
            'center_name': None,
            'bay_name':src_msg['bay_name'],
            'qualification': src_msg['education'] if 'education' in src_msg else None,
            'support_needed': src_msg['zoom_support'] == 'Yes' if 'zoom_support' in src_msg else None,
            'agreed_to_terms': src_msg['pre_program_info'] == 'Yes' if 'pre_program_info' in src_msg else None,
            'arrival_date':ar,
            'departure_date':dp,
            'ie_hear_about':src_msg['ie_hear_about'] if 'ie_hear_about' in src_msg else None,
            'utm_medium':src_msg['utm_medium'] if 'utm_medium' in src_msg else None,
            'utm_campaign':src_msg['utm_campaign'] if 'utm_campaign' in src_msg else None,
            'utm_source':src_msg['utm_source'] if 'utm_source' in src_msg else None,
            'utm_content':src_msg['utm_content'] if 'utm_content' in src_msg else None,
            'utm_term':src_msg['utm_term'] if 'utm_term' in src_msg else None,
            'utm_referer':src_msg['utm_referer'] if 'utm_referer' in src_msg else None,
            'ieo_class_status':src_msg['ieo_class_status'] if 'ieo_class_status' in src_msg else None,
            'checkin_status_day_2':src_msg['checkin_status_day_2'] if 'checkin_status_day_2' in src_msg else None,
            'trans_lang':src_msg['trans_lang'] if 'trans_lang' in src_msg else None,
            'webinar_event_id':src_msg['webinar_event_id'] if 'webinar_event_id' in src_msg and src_msg['webinar_event_id'] else None,
            'webinar_event_name':src_msg['webinar_event_name'] if 'webinar_event_name' in src_msg and src_msg['webinar_event_name'] else None,
            'webinar_event_date':self.getDateTime(src_msg['webinar_event_date']) if 'webinar_event_date' in src_msg and src_msg['webinar_event_date'] else None,
            'record_name': src_msg['name'],
            'record_phone': phone,
            'record_email': email['1'] if '1' in email else None
        }
        if src_msg['program_name'] and src_msg['program_name'] == 'Uyirnokkam Online':
            odooRec['program_name'] = 'Uyir Nokkam Online'
            odooRec['local_program_type_id'] = 'Uyir Nokkam Online'
            odooRec['batch'] = src_msg['batch']

        if src_msg['program_name'] and 'Shivanga' in src_msg['program_name']:
            # odooRec['program_name'] = src_msg['program_name'].replace('Shivanga','Vanashree')
            sd = datetime.strptime(src_msg['shivanga_initiation_date'], '%d-%m-%Y') if src_msg.get('shivanga_initiation_date') else None
            sd = sd.strftime('%Y-%m-%d') if sd else None
            odooRec['start_date'] = sd
            ed = datetime.strptime(src_msg['shivanga_culmination_date'], '%d-%m-%Y') if src_msg.get('shivanga_culmination_date', None)  else None
            ed = ed.strftime('%Y-%m-%d') if ed else None
            odooRec['end_date'] = ed

        if src_msg['program_name'] == 'In the Grace of Yoga with Sadhguru during Mahashivratri - Registration':
            odooRec['program_name'] = 'Grace of Yoga with Sadhguru 2021'
            odooRec['local_program_type_id'] = 'Grace of Yoga with Sadhguru'
            odooRec['start_date'] = '2021-03-08'
            odooRec['end_date'] = '2021-03-12'

        if src_msg['program_name'] == 'Online Offerings Webinar Registration' or src_msg['program_name'] == 'Surya Shakthi Yogaveera Training Registrations'\
                or src_msg['program_name'] == 'IDY 2021':
            odooRec['reg_status'] = 'ATTENDED' if src_msg['event_attendance_date'] else odooRec['reg_status']

        if src_msg['program_name'] == 'Online Monthly Satsang Confirmation':
            odooRec['pgm_remark'] = 'RSVP'
            odooRec['program_name'] = 'Online Satsang'
            odooRec['local_program_type_id'] = 'Online Satsang'
            odooRec['start_date'] = self.getDateFromDateTime(odooRec['event_attendance_date'])

        if src_msg['program_id'] == '061' or src_msg['program_name'] == 'Sadhana Intensive':
            odooRec['program_name'] = 'Sadhana Support COVID-19'
            odooRec['local_program_type_id'] = 'Sadhana Support COVID-19'
            odooRec['program_schedule_info']['schedule_name'] = 'Sadhana Support COVID-19'
            odooRec['reg_date_time'] = src_msg['created_date']
            odooRec['start_date'] = src_msg['created_date'].split()[0]
            odooRec['program_schedule_info']['start_date'] = src_msg['created_date'].split()[0]
            odooRec['is_ie_done'] = True if src_msg['is_ie_done'] == 'Yes' else False

        return odooRec

class ComunicationTxn:
    system_id = 9

    def getOdooFormat(self, src_msg):

        odooRec = {
            'system_id' : self.system_id,
            'local_trans_id': src_msg['local_id'],
            'local_contact_id': src_msg['local_id'],
            'communication':src_msg['communication_tag'],
            'response':src_msg['response_tag'],
            'file_date':src_msg['in_file_date'],
            'modified_at':src_msg['in_file_date']
        }
        return odooRec

class DMSTxn:
    system_id = 7

    def getOdooFormat(self, src_msg):
        src_msg = replaceEmptyString(src_msg['payload'])
        if 'local_patron_id' in src_msg and src_msg['local_patron_id']:
            pat_id = src_msg['local_patron_id']
            pat_poc_id = src_msg['patron_poc_id']  if 'patron_poc_id' in src_msg else None
        else:
            pat_id = src_msg['local_donor_id'] if 'local_donor_id' in src_msg else None
            pat_poc_id = src_msg['donor_poc_id']  if 'donor_poc_id' in src_msg else None
        rec_date = None
        if src_msg['receipt_date']:
            try:
                rec_date = getDateTimeFromDateField(src_msg['receipt_date'])
            except:
                pass


        odooRec={
            'local_trans_id': src_msg['local_trans_id'] if 'local_trans_id' in src_msg else None,
            'local_donor_id': src_msg['local_donor_id'] if 'local_donor_id' in src_msg else None,
            'donor_poc_id': src_msg['donor_poc_id'] if 'donor_poc_id' in src_msg else None,
            'local_patron_id': pat_id,
            'patron_poc_id': pat_poc_id,
            'project': src_msg['project'] if 'project' in src_msg else None,
            'mode_of_payment': src_msg['mode_of_payment'] if 'mode_of_payment' in src_msg else None,
            'inkind': src_msg['inkind'] if 'inkind' in src_msg else None,
            'amount': src_msg['amount'] if 'amount' in src_msg else None,
            'receipt_number': src_msg['receipt_number'] if 'receipt_number' in src_msg else None,
            'don_receipt_date': rec_date,
            'active': src_msg['active'] if 'active' in src_msg else None,
            'status': src_msg['donation_status'] if 'donation_status' in src_msg else None,
            'donation_type':'DONATION',
            'entity': src_msg['entity'] if 'entity' in src_msg else None,
            'system_id':self.system_id
        }
        return odooRec
