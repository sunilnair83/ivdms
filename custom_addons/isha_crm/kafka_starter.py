import csv
import datetime
import json
import logging
import multiprocessing
import os
import sys
import time
import traceback
from xmlrpc import client

from confluent_kafka import Consumer
from kafka import KafkaAdminClient
from kafka import TopicPartition
from kafka.consumer import KafkaConsumer

from odoo.tools import config
from . import ContactProcessor
from . import TxnProcessor
from .kafka_processors import IEOProcessor, EreceiptsProcessor, SystemContactProcessor, ProgramProcessor, PRSProcessor, \
    DMSDonationProcessor, SgExProcessor, SgExUTMProcessor, OnlineSatsangProcesssor, EreceiptsEducationProcessor, \
    RudrakshaDeekshaProcessor, outreach_ishanga_regProcessor,\
    EreceiptsReligiousProcessor, IshaVidhyaOutreachProcessor, AthithiContactsProcessor, SPProcessor, IshaVidhyaImportProcessor,\
    IshaVidhyaImportProcessor, FeedbackProcessor, SulabaContactsProcessor
from ..isha_base.configuration import Configuration

logger = logging.getLogger(__name__)

def check_consumer_lag_threshold(admin_client, consumer, topic, group, thread_count):
    if int(thread_count) < 1:
        logger.info(group+' not active')
        return ''
    new_offsets_data = admin_client.list_consumer_group_offsets(group)
    partitions_to_end_offsets_map = get_end_offset(topic, consumer)
    lag = 0
    lag_info = ''
    if new_offsets_data:
        for partition in new_offsets_data:
            end_offset = 0
            new_offset = 0
            if getattr(partition, 'topic') == topic:
                if partitions_to_end_offsets_map:
                    end_offset = partitions_to_end_offsets_map.get(partition)
                if partition in new_offsets_data:
                    new_offset = getattr(new_offsets_data.get(partition), 'offset')
                lag += (end_offset-new_offset)
                if end_offset - new_offset > 5000:
                    lag_info += str('  -> lag for ' + str(partition) + ' ' + group + ' ' + str(end_offset-new_offset)) + '\n'

    if lag_info or lag > 5000:
        return topic + ' topic has exceeded the lag threshold ' + str(lag) + '\n' + lag_info
    else:
        return ''


def monitor_consumer_lag(ENV):
    kafka_config = Configuration('KAFKA')
    admin_client = KafkaAdminClient(bootstrap_servers=kafka_config['KAFKA_BOOTSTRAP_SERVERS'],
                                    security_protocol=kafka_config['KAFKA_SECURITY_PROTOCOL'],
                                    ssl_cafile=kafka_config['KAFKA_SSL_CA_CERT_PATH'],
                                    ssl_certfile=kafka_config['KAFKA_SSL_CERT_PATH'])
    consumer = KafkaConsumer(bootstrap_servers=kafka_config['KAFKA_BOOTSTRAP_SERVERS'],
                             security_protocol=kafka_config['KAFKA_SECURITY_PROTOCOL'],
                             ssl_cafile=kafka_config['KAFKA_SSL_CA_CERT_PATH'],
                             ssl_certfile=kafka_config['KAFKA_SSL_CERT_PATH'])

    crm_config = Configuration('CRM')

    report = ''

    logger.info("Checking IEO")
    ieo = check_consumer_lag_threshold(admin_client, consumer,crm_config['CRM_KAFKA_IEO_TOPIC'], crm_config['CRM_KAFKA_IEO_GROUP'], crm_config['CRM_KAFKA_IEO_THREAD_COUNT'])

    logger.info("Checking erc")
    ereceipts = check_consumer_lag_threshold(admin_client,consumer, crm_config['CRM_KAFKA_ER_TOPIC'],crm_config['CRM_KAFKA_ER_GROUP'], crm_config['CRM_KAFKA_ER_THREAD_COUNT'])

    logger.info("Checking erc relegious")
    ereceipts_relegious = check_consumer_lag_threshold(admin_client,consumer, crm_config['CRM_KAFKA_ER_RELIGIOUS_TOPIC'],crm_config['CRM_KAFKA_ER_RELIGIOUS_GROUP'], crm_config['CRM_KAFKA_ER_RELIGIOUS_THREAD_COUNT'])

    logger.info("Checking erc Education")
    ereceipts_edu = check_consumer_lag_threshold(admin_client, consumer,crm_config['CRM_KAFKA_ER_EDU_TOPIC'],crm_config['CRM_KAFKA_ER_EDU_GROUP'], crm_config['CRM_KAFKA_ER_EDU_THREAD_COUNT'])

    logger.info("Checking prs")
    prs = check_consumer_lag_threshold(admin_client, consumer, crm_config['CRM_KAFKA_PRS_TXN_TOPIC'],crm_config['CRM_KAFKA_PRS_TXN_GROUP'], crm_config['CRM_KAFKA_PRS_TXN_THREAD_COUNT'])

    logger.info("Checking al adv")
    al_adv = check_consumer_lag_threshold(admin_client, consumer, crm_config['CRM_KAFKA_PGMADV_TOPIC'],crm_config['CRM_KAFKA_PGMADV_GROUP'], crm_config['CRM_KAFKA_PGMADV_THREAD_COUNT'])

    logger.info("Checking al first")
    al_first = check_consumer_lag_threshold(admin_client, consumer, crm_config['CRM_KAFKA_PGM_TOPIC'],crm_config['CRM_KAFKA_PGM_GROUP'], crm_config['CRM_KAFKA_PGM_THREAD_COUNT'])

    logger.info("Checking al contacts")
    al = check_consumer_lag_threshold(admin_client, consumer, crm_config['CRM_KAFKA_AL_TOPIC'],crm_config['CRM_KAFKA_AL_GROUP'], crm_config['CRM_KAFKA_AL_THREAD_COUNT'])

    logger.info("Checking Isha Vidhya Joomla records")
    iv_joomla = check_consumer_lag_threshold(admin_client, consumer, crm_config['CRM_KAFKA_ISHA_VIDHYA_JOOMLA_TOPIC'],crm_config['CRM_KAFKA_ISHA_VIDHYA_JOOMLA_GROUP'], crm_config['CRM_KAFKA_ISHA_VIDHYA_JOOMLA_THREAD_COUNT'])

    logger.info("Checking sadhguru Exclusive")
    sg_ex = check_consumer_lag_threshold(admin_client, consumer, crm_config['CRM_KAFKA_SGEX_TOPIC'],crm_config['CRM_KAFKA_SGEX_GROUP'], crm_config['CRM_KAFKA_SGEX_THREAD_COUNT'])

    logger.info("Checking sadhguru Exclusive UTM")
    sg_ex_utm = check_consumer_lag_threshold(admin_client, consumer, crm_config['CRM_KAFKA_SGEX_UTM_TOPIC'],crm_config['CRM_KAFKA_SGEX_UTM_GROUP'], crm_config['CRM_KAFKA_SGEX_UTM_THREAD_COUNT'])

    report = ieo + ereceipts + ereceipts_relegious + ereceipts_edu + prs + al_adv + al_first + al + iv_joomla + sg_ex + sg_ex_utm
    if len(report) > 0:
        template = ENV.ref('isha_crm.kafka_lag_limit')
        if template:
            # get random partner
            partner = ENV['res.partner'].search([], limit=1)
            values = template.generate_email(partner.id, fields=None)
            base_url = ENV['ir.config_parameter'].sudo().get_param('web.base.url')
            values['body_html'] = values['body_html'].replace('$$base_url', base_url)
            values['body_html'] = values['body_html'].replace('$$', report)

            mail_mail_obj = ENV['mail.mail']
            msg_id = mail_mail_obj.sudo().create(values)
            msg_id.send(False)
        ENV.cr.commit()

def monitor_kafka_errors(ENV):
    kafka_errors_count = ENV['kafka.errors'].search_count([('topic', 'not like', 'utm_prod')])
    if kafka_errors_count > 0:
        template = ENV.ref('isha_crm.kafka_errors')
        if template:
            # get random partner
            partner = ENV['res.partner'].search([], limit=1)
            values = template.generate_email(partner.id, fields=None)
            base_url = ENV['ir.config_parameter'].sudo().get_param('web.base.url')
            values['body_html'] = values['body_html'].replace('$$base_url', base_url)
            values['body_html'] = values['body_html'].replace('$$', str(kafka_errors_count))

            mail_mail_obj = ENV['mail.mail']
            msg_id = mail_mail_obj.sudo().create(values)
            msg_id.send(False)


def monitor_kafka(ENV):
    kafka_config = Configuration('KAFKA')
    admin_client = KafkaAdminClient(bootstrap_servers=kafka_config['KAFKA_BOOTSTRAP_SERVERS'],
                                    security_protocol=kafka_config['KAFKA_SECURITY_PROTOCOL'],
                                    ssl_cafile=kafka_config['KAFKA_SSL_CA_CERT_PATH'],
                                    ssl_certfile=kafka_config['KAFKA_SSL_CERT_PATH'])
    consumer = KafkaConsumer(bootstrap_servers=kafka_config['KAFKA_BOOTSTRAP_SERVERS'],
                             security_protocol=kafka_config['KAFKA_SECURITY_PROTOCOL'],
                             ssl_cafile=kafka_config['KAFKA_SSL_CA_CERT_PATH'],
                             ssl_certfile=kafka_config['KAFKA_SSL_CERT_PATH'])

    crm_config = Configuration('CRM')

    try:
        ieo_old_offsets_data = admin_client.list_consumer_group_offsets(crm_config['CRM_KAFKA_IEO_GROUP'])
        logger.info("IEO offset: " + str(ieo_old_offsets_data))
        erc_old_offsets_data = admin_client.list_consumer_group_offsets(crm_config['CRM_KAFKA_ER_GROUP'])
        logger.info("ERC offset: " + str(erc_old_offsets_data))
        erc_edu_old_offsets_data = admin_client.list_consumer_group_offsets(crm_config['CRM_KAFKA_ER_EDU_GROUP'])
        logger.info("ERC EDU offset: " + str(erc_edu_old_offsets_data))
        prs_old_offsets_data = admin_client.list_consumer_group_offsets(crm_config['CRM_KAFKA_PRS_TXN_GROUP'])
        logger.info("PRS offset: " + str(prs_old_offsets_data))
        al_adv_old_offsets_data = admin_client.list_consumer_group_offsets(crm_config['CRM_KAFKA_PGMADV_GROUP'])
        logger.info("al adv offset: " + str(al_adv_old_offsets_data))
        al_first_old_offsets_data = admin_client.list_consumer_group_offsets(crm_config['CRM_KAFKA_PGM_GROUP'])
        logger.info("al first offset: " + str(al_first_old_offsets_data))
        al_con_old_offsets_data = admin_client.list_consumer_group_offsets(crm_config['CRM_KAFKA_AL_GROUP'])
        logger.info("al con offset: " + str(al_con_old_offsets_data))
        iv_joomla_old_offsets_data = admin_client.list_consumer_group_offsets(crm_config['CRM_KAFKA_ISHA_VIDHYA_JOOMLA_GROUP'])
        logger.info("iv joomla record offset: " + str(al_con_old_offsets_data))

        logger.info("Waiting for half-a-minute to let it progress")
        time.sleep(30)

        logger.info("Checking IEO")
        ieo = get_non_processing_info(admin_client, crm_config['CRM_KAFKA_IEO_TOPIC'],
                                          crm_config['CRM_KAFKA_IEO_GROUP'],
                                          kafka_config, consumer, ieo_old_offsets_data,
                                          int(crm_config['CRM_KAFKA_IEO_THREAD_COUNT']))

        logger.info("Checking erc")
        ereceipts = get_non_processing_info(admin_client, crm_config['CRM_KAFKA_ER_TOPIC'],
                                                crm_config['CRM_KAFKA_ER_GROUP'],
                                                kafka_config, consumer, erc_old_offsets_data,
                                                int(crm_config['CRM_KAFKA_ER_THREAD_COUNT']))

        logger.info("Checking erc Education")
        ereceipts_edu = get_non_processing_info(admin_client, crm_config['CRM_KAFKA_ER_EDU_TOPIC'],
                                                    crm_config['CRM_KAFKA_ER_EDU_GROUP'],
                                                    kafka_config, consumer, erc_edu_old_offsets_data,
                                                    int(crm_config['CRM_KAFKA_ER_EDU_THREAD_COUNT']))

        logger.info("Checking prs")
        prs = get_non_processing_info(admin_client, crm_config['CRM_KAFKA_PRS_TXN_TOPIC'],
                                          crm_config['CRM_KAFKA_PRS_TXN_GROUP'],
                                          kafka_config, consumer, prs_old_offsets_data,
                                          int(crm_config['CRM_KAFKA_PRS_TXN_THREAD_COUNT']))

        logger.info("Checking al adv")
        al_adv = get_non_processing_info(admin_client, crm_config['CRM_KAFKA_PGMADV_TOPIC'],
                                             crm_config['CRM_KAFKA_PGMADV_GROUP'],
                                             kafka_config, consumer, al_adv_old_offsets_data,
                                             int(crm_config['CRM_KAFKA_PGMADV_THREAD_COUNT']))

        logger.info("Checking al first")
        al_first = get_non_processing_info(admin_client, crm_config['CRM_KAFKA_PGM_TOPIC'],
                                               crm_config['CRM_KAFKA_PGM_GROUP'],
                                               kafka_config, consumer, al_first_old_offsets_data,
                                               int(crm_config['CRM_KAFKA_PGM_THREAD_COUNT']))

        logger.info("Checking al contacts")
        al = get_non_processing_info(admin_client, crm_config['CRM_KAFKA_AL_TOPIC'],
                                         crm_config['CRM_KAFKA_AL_GROUP'],
                                         kafka_config, consumer, al_con_old_offsets_data,
                                         int(crm_config['CRM_KAFKA_AL_THREAD_COUNT']))

        logger.info("Checking Isha Vidhya Joomla records")
        iv_joomla = get_non_processing_info(admin_client, crm_config['CRM_KAFKA_ISHA_VIDHYA_JOOMLA_TOPIC'],
                                         crm_config['CRM_KAFKA_ISHA_VIDHYA_JOOMLA_GROUP'],
                                         kafka_config, consumer, iv_joomla_old_offsets_data,
                                         int(crm_config['CRM_KAFKA_ISHA_VIDHYA_JOOMLA_THREAD_COUNT']))
    finally:
        if admin_client:
            admin_client.close()
        if consumer:
            consumer.close()
    body = ieo + ereceipts + ereceipts_edu + prs + al_adv + al_first + al + iv_joomla
    if body != '':
        template = ENV.ref('isha_crm.kafka_messages_not_processing')
        if template:
            # get random partner
            partner = ENV['res.partner'].search([], limit=1)
            values = template.generate_email(partner.id, fields=None)
            base_url = ENV['ir.config_parameter'].sudo().get_param('web.base.url')
            values['body_html'] = values['body_html'].replace('$$base_url', base_url)
            values['body_html'] = values['body_html'].replace('$$', body)

            mail_mail_obj = ENV['mail.mail']
            msg_id = mail_mail_obj.sudo().create(values)
            msg_id.send(False)
        ENV.cr.commit()


def get_end_offset(topic, consumer):
    partitions = consumer.partitions_for_topic(topic)
    if partitions:
        tpartitions = [TopicPartition(topic, p) for p in partitions]
        return consumer.end_offsets(tpartitions)
    return False


def get_non_processing_info(admin_client, topic, group, kafka_config, consumer, old_offsets_data, thread_count):
    response = ''
    processing = None
    if thread_count == 0:
        processing = True
    # get new offsets
    new_offsets_data = admin_client.list_consumer_group_offsets(group)
    partitions_to_end_offsets_map = get_end_offset(topic, consumer)

    if len(old_offsets_data) == 0:
        processing = True
    for partition in old_offsets_data:
        if getattr(partition, 'topic') == topic:
            if partitions_to_end_offsets_map:
                end_offset = partitions_to_end_offsets_map.get(partition)
            if partition in old_offsets_data:
                old_offset = getattr(old_offsets_data.get(partition), 'offset')
            if partition in new_offsets_data:
                new_offset = getattr(new_offsets_data.get(partition), 'offset')
            if not end_offset or not old_offset or not new_offset:
                processing = True
            # if there is nothing to process or if new offset is greater than the old offset, then we're good.
            if old_offset == end_offset or old_offset < new_offset <= end_offset:
                # if at least one partition is progressing, then we are good.
                processing = True
    # no partition is progressing
    if not processing:
        response += 'topic: ' + topic + ' group: ' + group + '<br/>'
    return response


def kafkaLoader():
    # Using a custom logger
    logger = logging.getLogger('Kafka-Consumer-Logger')
    logger.setLevel(logging.DEBUG)
    ch = logging.StreamHandler(sys.stdout)
    ch.setFormatter(logging.Formatter("%(asctime)s — %(name)s — %(levelname)s — %(threadName)s:%(message)s"))
    logger.addHandler(ch)

    # External config
    # config = configparser.RawConfigParser()
    # config.read('/mnt/extra-addons/ext-config/ConfigFile.properties')

    logger.debug("Starting Kafka loader Base Thread")
    td = multiprocessing.Process(target=startThreads, args=(logger,))
    td.start()
    logger.debug("Started Kafka loader From Base Thread")
    return


def getConsumerGroupStatus(kafka_config, group):
    # Retry until we get response from kafka Broker
    while True:
        try:
            admin_client = KafkaAdminClient(bootstrap_servers=kafka_config['KAFKA_BOOTSTRAP_SERVERS'],
                                            security_protocol=kafka_config['KAFKA_SECURITY_PROTOCOL'],
                                            ssl_cafile=kafka_config['KAFKA_SSL_CA_CERT_PATH'],
                                            ssl_certfile=kafka_config['KAFKA_SSL_CERT_PATH'])
            meta_data = admin_client.describe_consumer_groups([group])
            break
        except Exception as ex:
            pass

    if len(meta_data[0][5]) > 0:
        # Consumer group is already active
        return True
    else:
        # No active consumers for the group
        return False


def startThreads(logger):
    # Load Config
    kafka_config = Configuration('KAFKA')
    crm_config = Configuration('CRM')

    logger.debug("Loading Kafka Consumers......")
    with open(os.path.join(crm_config['CRM_ASSETS_PATH'], 'country-nationality-iso2-map.csv')) as f:
        iso2 = dict(filter(None, csv.reader(f)))
    with open(os.path.join(crm_config['CRM_ASSETS_PATH'], 'eReceipts-country-nationality-corrections.csv')) as f:
        csvReader = []
        for x in csv.reader(f):
            csvReader.append([x[0].upper(), x[1].upper()])
        correction = dict(filter(None, csvReader))
    with open(os.path.join(crm_config['CRM_ASSETS_PATH'], 'sp_interviewer.csv')) as f:
        sp_users = dict(filter(None, csv.reader(f)))
    dms_contacts_metadata = {
        'iso2': iso2,
        'correction': correction,
        'contactFormatConverter': ContactProcessor.DMSContacts()
    }
    start_consumers(int(crm_config['CRM_KAFKA_DMS_THREAD_COUNT']), crm_config['CRM_KAFKA_DMS_TOPIC'],
                    crm_config['CRM_KAFKA_DMS_GROUP'], SystemContactProcessor.SystemContactProcessor(),
                    dms_contacts_metadata, logger, kafka_config, 'dms-contacts')

    hys_contacts_metadata = {
        'iso2': iso2,
        'correction': correction,
        'contactFormatConverter': ContactProcessor.HYSContacts()
    }
    start_consumers(int(crm_config['CRM_KAFKA_HYS_THREAD_COUNT']), crm_config['CRM_KAFKA_HYS_TOPIC'],
                    crm_config['CRM_KAFKA_HYS_GROUP'], SystemContactProcessor.SystemContactProcessor(),
                    hys_contacts_metadata, logger, kafka_config, 'hys-contacts')

    al_contacts_metadata = {
        'iso2': iso2,
        'correction': correction,
        'contactFormatConverter': ContactProcessor.ALContact()
    }
    start_consumers(int(crm_config['CRM_KAFKA_AL_THREAD_COUNT']), crm_config['CRM_KAFKA_AL_TOPIC'],
                    crm_config['CRM_KAFKA_AL_GROUP'], SystemContactProcessor.SystemContactProcessor(),
                    al_contacts_metadata, logger, kafka_config, 'al-contacts')

    al_pgm_metadata = {
        'programFormatConverter': TxnProcessor.Program()
    }
    start_consumers(int(crm_config['CRM_KAFKA_PGM_THREAD_COUNT']), crm_config['CRM_KAFKA_PGM_TOPIC'],
                    crm_config['CRM_KAFKA_PGM_GROUP'], ProgramProcessor.ProgramProcessor(),
                    al_pgm_metadata, logger, kafka_config, 'al-prog')

    al_adv_pgm_metadata = {
        'programFormatConverter': TxnProcessor.ProgramAdv()
    }
    start_consumers(int(crm_config['CRM_KAFKA_PGMADV_THREAD_COUNT']), crm_config['CRM_KAFKA_PGMADV_TOPIC'],
                    crm_config['CRM_KAFKA_PGMADV_GROUP'], ProgramProcessor.ProgramProcessor(),
                    al_adv_pgm_metadata, logger, kafka_config, 'al-adv-prog')

    prs_metadata = {
        'iso2': iso2,
        'correction': correction
    }
    start_consumers(int(crm_config['CRM_KAFKA_PRS_TXN_THREAD_COUNT']), crm_config['CRM_KAFKA_PRS_TXN_TOPIC'],
                    crm_config['CRM_KAFKA_PRS_TXN_GROUP'], PRSProcessor.PRSProcessor(),
                    prs_metadata, logger, kafka_config, 'prs')

    start_consumers(int(crm_config['CRM_KAFKA_DMS_TXN_THREAD_COUNT']), crm_config['CRM_KAFKA_DMS_TXN_TOPIC'],
                    crm_config['CRM_KAFKA_DMS_TXN_GROUP'], DMSDonationProcessor.DMSDonationProcessor(),
                    None, logger, kafka_config, 'dms-donation')

    metadata = {
        'iso2': iso2,
        'correction': correction
    }
    start_consumers(int(crm_config['CRM_KAFKA_ER_THREAD_COUNT']), crm_config['CRM_KAFKA_ER_TOPIC'],
                    crm_config['CRM_KAFKA_ER_GROUP'],
                    EreceiptsProcessor.EreceiptsProcessor(), metadata, logger, kafka_config, 'ereceipts')

    metadata = {
        'iso2': iso2,
        'correction': correction
    }
    start_consumers(int(crm_config['CRM_KAFKA_ER_EDU_THREAD_COUNT']), crm_config['CRM_KAFKA_ER_EDU_TOPIC'],
                    crm_config['CRM_KAFKA_ER_EDU_GROUP'],
                    EreceiptsEducationProcessor.EreceiptsEducationProcessor(), metadata, logger, kafka_config, 'ereceipts-edu')

    start_consumers(int(crm_config['CRM_KAFKA_ER_RELIGIOUS_THREAD_COUNT']),
                    crm_config['CRM_KAFKA_ER_RELIGIOUS_TOPIC'], crm_config['CRM_KAFKA_ER_RELIGIOUS_GROUP'],
                    EreceiptsReligiousProcessor.EreceiptsReligiousProcessor(), metadata,
                    logger, kafka_config, 'ereceipts-religious')

    start_consumers(int(crm_config['CRM_KAFKA_IEO_THREAD_COUNT']), crm_config['CRM_KAFKA_IEO_TOPIC'],
                    crm_config['CRM_KAFKA_IEO_GROUP'],
                    IEOProcessor.IEOProcessor(), metadata, logger, kafka_config, 'ieo')

    start_consumers(int(crm_config['CRM_KAFKA_SGEX_THREAD_COUNT']), crm_config['CRM_KAFKA_SGEX_TOPIC'],
                    crm_config['CRM_KAFKA_SGEX_GROUP'],
                    SgExProcessor.SgExProcessor(), metadata, logger, kafka_config, 'sgex')

    start_consumers(int(crm_config['CRM_KAFKA_SGEX_UTM_THREAD_COUNT']), crm_config['CRM_KAFKA_SGEX_UTM_TOPIC'],
                    crm_config['CRM_KAFKA_SGEX_UTM_GROUP'],
                    SgExUTMProcessor.SgExUTMProcessor(), metadata, logger, kafka_config, 'sgex_utm')

    start_consumers(int(crm_config['CRM_KAFKA_SATSANG_THREAD_COUNT']), crm_config['CRM_KAFKA_SATSANG_TOPIC'],
                    crm_config['CRM_KAFKA_SATSANG_GROUP'],
                    OnlineSatsangProcesssor.OnlineSatsangProcessor(), metadata, logger, kafka_config, 'satsang')

    start_consumers(int(crm_config['CRM_KAFKA_RUDRAHSHA_THREAD_COUNT']), crm_config['CRM_KAFKA_RUDRAHSHA_TOPIC'],
                    crm_config['CRM_KAFKA_RUDRAHSHA_GROUP'],
                    RudrakshaDeekshaProcessor.RudrakshaDeekshaProcessor(), metadata, logger, kafka_config, 'rudraksha')
    start_consumers(int(crm_config['CRM_KAFKA_OUTREACHISHANGAREG_THREAD_COUNT']), crm_config['CRM_KAFKA_OUTREACHISHANGAREG_TOPIC'],
                    crm_config['CRM_KAFKA_OUTREACHISHANGAREG_GROUP'],
                    outreach_ishanga_regProcessor.outreach_ishanga_regProcessor(), metadata, logger, kafka_config, 'outreach_ishanga_reg')

    sp_metadata = {
        'iso2': iso2,
        'correction': correction,
        'sp_users': sp_users
    }
    start_consumers(int(crm_config['CRM_KAFKA_SUVYA_SP_REGISTRATION_THREAD_COUNT']), crm_config['CRM_KAFKA_SUVYA_SP_REGISTRATION_TOPIC'],
                    crm_config['CRM_KAFKA_SUVYA_SP_REGISTRATION_GROUP'],
                    SPProcessor.SPProcessor(), sp_metadata, logger, kafka_config, 'sp-registration')

    start_consumers(int(crm_config['CRM_KAFKA_ISHA_VIDHYA_JOOMLA_THREAD_COUNT']), crm_config['CRM_KAFKA_ISHA_VIDHYA_JOOMLA_TOPIC'],
                    crm_config['CRM_KAFKA_ISHA_VIDHYA_JOOMLA_GROUP'],
                    IshaVidhyaOutreachProcessor.IshaVidhyaOutreachProcessor(), metadata, logger, kafka_config, 'isha-vidhya')

    start_consumers(int(crm_config['CRM_KAFKA_ATHITHI_CONTACTS_THREAD_COUNT']), crm_config['CRM_KAFKA_ATHITHI_CONTACTS_TOPIC'],
                    crm_config['CRM_KAFKA_ATHITHI_CONTACTS_GROUP'],
                    AthithiContactsProcessor.AthithiContactsProcessor(), metadata, logger, kafka_config, 'athithi-contacts')

    start_consumers(int(crm_config['CRM_KAFKA_FEEDBACK_THREAD_COUNT']),
                    crm_config['CRM_KAFKA_FEEDBACK_TOPIC'],
                    crm_config['CRM_KAFKA_FEEDBACK_GROUP'],
                    None, metadata, logger, kafka_config,
                    'feedback-contacts')

    # start_consumers(int(crm_config['CRM_KAFKA_EVENT_ATTENDEES_THREAD_COUNT']), crm_config['CRM_KAFKA_EVENT_ATTENDEES_TOPIC'],
    #                 crm_config['CRM_KAFKA_EVENT_ATTENDEES_GROUP'], None, metadata, logger, kafka_config,
    #                 'event-attendees')

    start_consumers(int(crm_config['CRM_KAFKA_ISHA_VIDHYA_IMPORT_THREAD_COUNT']), crm_config['CRM_KAFKA_ISHA_VIDHYA_IMPORT_TOPIC'],
                    crm_config['CRM_KAFKA_ISHA_VIDHYA_IMPORT_GROUP'],
                    None, metadata, logger, kafka_config, 'isha-vidhya-import')
    start_consumers(int(crm_config['CRM_KAFKA_RD_DELIVERY_THREAD_COUNT']), crm_config['CRM_KAFKA_RD_DELIVERY_TOPIC'],
                    crm_config['CRM_KAFKA_RD_DELIVERY_GROUP'],
                    None, metadata, logger, kafka_config, 'rd-delivery-updates')

    start_consumers(int(crm_config['CRM_CONTACT_SEARCH_SULABA_THREAD_COUNT']),
                    crm_config['CRM_CONTACT_SEARCH_SULABA_TOPIC'],
                    crm_config['CRM_CONTACT_SEARCH_SULABA_GROUP'],
                    SulabaContactsProcessor.SulabaContactsProcessor(), sp_metadata, logger, kafka_config,
                    'sulaba-contact-search')

    start_consumers_silent('CRM_KAFKA_ISHA_VIDHYA_IMPORT_THREAD_COUNT', 'CRM_KAFKA_ISHA_VIDHYA_IMPORT_TOPIC',
                    'CRM_KAFKA_ISHA_VIDHYA_IMPORT_GROUP',
                    None, metadata, logger, kafka_config, 'isha-vidhya-import')

    start_consumers_silent('CRM_KAFKA_WHATSAPP_YELLOW_DELIVERY_STATS_THREAD_COUNT',
                           'CRM_KAFKA_WHATSAPP_YELLOW_DELIVERY_STATS_TOPIC',
                           'CRM_KAFKA_WHATSAPP_YELLOW_DELIVERY_STATS_GROUP', None,
                           metadata, logger, kafka_config, 'wa_yellow_stats')

    # test_sp_consumer()
    # start_consumers(int(crm_config['CRM_KAFKA_IEC_MEGAPPGM_THREAD_COUNT']), crm_config['CRM_KAFKA_IEC_MEGAPPGM_TOPIC'],
    #                 crm_config['CRM_KAFKA_IEC_MEGAPPGM_GROUP'],
    #                 IECMegaPgmProcessor.IECMegaPgmProcessor(), metadata, logger, kafka_config, 'iecmegapgm')

    # test_isha_vidhya_import_consumer()
    # test_athithi_contact_consumer()
    # test_isha_vidhya_joomla_consumer()
    # test_ereceipts_edu_consumer()
    # test_feedback_consumer()
    # test_event_attendees_consumer()
    # test_prs_consumer(PRSProcessor.PRSProcessor(), prs_metadata, logger)
    # start_test_consumer(test_ieo_consumer,  IEOProcessor.IEOProcessor(), metadata, logger)
    # test_dms_contact_consumer( SystemContactProcessor.SystemContactProcessor(), dms_contacts_metadata, logger)
    # test_dms_donation_consumer( DMSDonationProcessor.DMSDonationProcessor(), None, logger)


def start_test_consumer(test_method, processor, metadata, logger):
    multiprocessing.Process(target=test_method, args=(processor, metadata, logger)).start()


def start_consumers_silent(thread_count_key, topic_key, consumer_group_key, processor, metadata, logger, kafka_config,
                    processor_key):
    crm_config = Configuration('CRM')
    if crm_config.get(thread_count_key) and crm_config.get(topic_key) and crm_config.get(consumer_group_key):
        return start_consumers(int(crm_config[thread_count_key]), crm_config[topic_key], crm_config[consumer_group_key],
                               processor, metadata, logger, kafka_config, processor_key)
    else:
        logger.info('Kafka configuration is missing for processor: ' + processor_key )


def start_consumers(thread_count, topic, consumer_group, processor, metadata, logger, kafka_config,
                    processor_key):
    if thread_count > 0 and getConsumerGroupStatus(kafka_config, consumer_group) is False:
        for i in range(thread_count):
            txn = multiprocessing.Process(target=kafka_consumer,
                                          args=(topic, consumer_group, processor, metadata, logger, kafka_config,
                                                processor_key))

            txn.start()
            time.sleep(5)
        logger.debug("Successfully Initiated Kafka Consumers for topic: " + topic + " Pid: " + str(txn.pid))


def kafka_consumer(topic, group, processor, metadata, logger, kafka_config, processor_key):
    try:
        start_consumer(topic, group, processor, metadata, logger, kafka_config, processor_key)
    except Exception as ex:
        tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        print(tb_ex)
        # Todo: send email notif as a xml rpc function
        # UID = odoo.SUPERUSER_ID
        # CR = registry.cursor()
        # with odoo.api.Environment.manage():
        #     ENV = odoo.api.Environment(CR, UID, {})
        #
        #     body = '''
        #             Exception: %s
        #     		''' % (tb_ex)
        #     template = ENV.ref('isha_crm.kafka_consumer_stopped')
        #     if template:
        #         partner = ENV['res.partner'].search([], limit=1)
        #         values = template.generate_email(partner.id, fields=None)
        #         values['body_html'] = values['body_html'].replace('$$', body)
        #         mail_mail_obj = ENV['mail.mail']
        #         msg_id = mail_mail_obj.sudo().create(values)
        #         msg_id.send(False)
        # values = template.generate_email(3, fields=None)
        # values['body_html'] = values['body_html'].replace('$$', body)
        # template.send_mail(self.dst_partner_id.id, force_send=True)


def start_consumer(topic, group, processor, metadata, logger, kafka_config, processor_key):
    time.sleep(10)
    rpc_config = Configuration('RPC')
    # odoo server config
    username = rpc_config['RPC_USERNAME']
    password = rpc_config['RPC_PASSWORD']
    url = rpc_config['RPC_URL']
    client.ServerProxy('{}/xmlrpc/2/common'.format(url))
    db = config['db_name']
    common = client.ServerProxy('{}/xmlrpc/2/common'.format(url))
    uid = common.authenticate(db, username, password, {})
    odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url))

    null = None
    NULL = None
    true = True
    false = False
    # Kafka Consumer Config
    c = Consumer({
        'bootstrap.servers': kafka_config['KAFKA_BOOTSTRAP_SERVERS'],
        'security.protocol': kafka_config['KAFKA_SECURITY_PROTOCOL'],
        'ssl.ca.location': kafka_config['KAFKA_SSL_CA_CERT_PATH'],
        'ssl.certificate.location': kafka_config['KAFKA_SSL_CERT_PATH'],
        'group.id': group,
        'auto.offset.reset': 'earliest',
        'enable.auto.commit': False
    })
    # subscribing to a topic
    c.subscribe([topic])

    logger.debug("Loaded " + topic)
    while True:
        msg = c.poll(5.0)
        # msg = {"interest_id":442,
        #         "person_id":4420,
        #         "first_name":"Kumar",
        #         "last_name":"R",
        #         "personal_email":"kumar@gmail.com",
        #         "marital_status":"R",
        #         "city":"Chennai",
        #         "state":"Tamil Nadu",
        #         "country":"India",
        #         "zip_code":"600113",
        #         "nationality":"India",
        #         "phone":"9434234587",
        #         "gender":"Male",
        #         "iep_month":"3",
        #         "iep_year":"2019",
        #         "iep_location":"Adyar",
        #         "iep_teacher":"Suresh",
        #         "date_of_birth":"1571498532000",
        #        "sadhanapada_year": "2019 - 2020",
        #         "mobile":"9843248757"}
        if msg is None:
            if datetime.datetime.now().minute % 10 == 0:
                logger.debug('------- ' + topic + ' alive -------')
            continue
        if msg.error():
            logger.debug("Consumer error: {}".format(msg.error()))
            continue

        try:
            msg = msg.value().decode('utf-8')
            # logger.debug(msg)
            rec = {'src_msg': msg, 'topic': topic, 'processor': processor_key,
                   'process_msg': True}
            odoo_models.execute_kw(db, uid, password,
                                   'kafka.errors', 'create',
                                   [rec])
        except Exception as ex:
            tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
            rec = {'src_msg': msg, 'error': tb_ex, 'topic': topic, 'processor': processor_key, 'process_msg': False}
            odoo_models.execute_kw(db, uid, password,
                                   'kafka.errors', 'create',
                                   [rec])
        finally:
            c.commit()


def test_ereceipts_edu_consumer():
    rpc_config = Configuration('RPC')
    # odoo server config
    username = rpc_config['RPC_USERNAME']
    password = rpc_config['RPC_PASSWORD']
    url = rpc_config['RPC_URL']
    db = config['db_name']
    common = client.ServerProxy('{}/xmlrpc/2/common'.format(url), allow_none=True)
    uid = common.authenticate(db, username, password, {})
    odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url), allow_none=True)

    msg = {
        "payload": {
            "record_modified_at": 1591229402,
            "row_id": 97269,
            "id": "EDU-2021-5ed3a85d29c58d69148b4feh",
            "contact_id": "EDU-2021-5ed3a85d29c58d69148b4feh",
            "modified_at": "2020-06-03T00:00:00Z",
            "modified_by": "emedia@ishafoundation.org",
            "project": "Ishanga",
            "mode_of_payment": "Online",
            "amount": 2001.0,
            "receipt_number": "if21-EAU-001559",
            # "receipt_number":None,
            "receipt_date": "2020-05-31T18:30:00Z",
            "entity": "IshaEducation",
            "status": "SUCCESS",
            "active": 1,
            "purpose_type": 'Isha Vidhay FRCA',
            "contact_type": "INDIVIDUAL",
            "first_name": "Test name",
            "nationality": "India",
            "phones": "7507081471",
            "emails": "test-erc@example.com",
            "address1": "22A, Suryadev Nagar ",
            "town_city1": "Indore",
            "state_province_region1": "Madhya Pradesh",
            "postal_code1": "452009",
            "country1": "India",
            "pan_number": None,
            "passport_number": None,
            "center": "Online",
            "ebook_number": "ebn-123",
            "transfer_reference": "trref-123",
            "bank_name": "some bank",
            "bank_branch": "some branch",
            "dd_chq_number": "some chq num",
            "dd_chq_date": "03-12-2020",
            "payment_gateway": "some gateway",
            "short_year": "2021"
        }
    }
    rec = {'src_msg': json.dumps(msg), 'topic': 'ereceipts', 'processor': 'ereceipts-edu',
           'process_msg': True}
    flag_dict = odoo_models.execute_kw(db, uid, password,
                                       'kafka.errors', 'create',
                                       [rec])


def test_isha_vidhya_joomla_consumer():
    rpc_config = Configuration('RPC')
    # odoo server config
    username = rpc_config['RPC_USERNAME']
    password = rpc_config['RPC_PASSWORD']
    url = rpc_config['RPC_URL']
    db = config['db_name']
    common = client.ServerProxy('{}/xmlrpc/2/common'.format(url), allow_none=True)
    uid = common.authenticate(db, username, password, {})
    odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url), allow_none=True)

    msg = {
        "id":68,
        "sso_id":"wNHnKmIRgBf4mH5qEqL0qBLVy072",
        "isha_transaction_id":"IVD-1565697652-5458",
        "transaction_date":1565672932000,
        "amount":240.0,
        "isha_campaign_id":0,
        "campaign_id":0,
        "isha_campaign_name": None,
        "transaction_status":"success",
        "currency":"usd",
        "udate":1566501814000,
        "payment_gateway":"paypal",
        "purpose":"General Donation",
        "payment_gateway_type":"",
        "donor_name":"Mohammed Ansari",
        "address":"fvhfthfjhkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk",
        "city":"jgjghkhg",
        "state":"Alabama",
        "country":"US",
        "nationality": None,
        "mobile":"123456789012345",
        "pincode":"1234",
        "email":"mohammeda@fermion.in",
        "pan_number": None,
        "general_donation": None,
        "govt_school_adoption_program": None,
        "infrastructure_requirements": None,
        "full_educational_support_qty": None,
        "full_educational_support_amount": None,
        "scholarship_qty": None,
        "scholarship_amount": None,
        "noon_meal_subsidy_qty": None,
        "noon_meal_subsidy_amount": None,
        "transport_subsidy_qty": None,
        "transport_subsidy_amount": None
    }
    rec = {'src_msg': json.dumps(msg), 'processor': 'isha-vidhya',
           'process_msg': True}
    flag_dict = odoo_models.execute_kw(db, uid, password,
                                       'kafka.errors', 'create',
                                       [rec])

def test_isha_vidhya_import_consumer():
    rpc_config = Configuration('RPC')
    # odoo server config
    username = rpc_config['RPC_USERNAME']
    password = rpc_config['RPC_PASSWORD']
    url = rpc_config['RPC_URL']
    db = config['db_name']
    common = client.ServerProxy('{}/xmlrpc/2/common'.format(url), allow_none=True)
    uid = common.authenticate(db, username, password, {})
    odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url), allow_none=True)

    msg = {"books_of_account":"Isha Education HO IVM","center":"IVMS Admin Office – CB","donation_date":"15/04/2020","receipt_number":"ie20-EB4-00002","source_transaction_id":"Src-101","don_receipt_date":"16/04/2020","amount":"250002","exchange_rate":"2","inr_amount":"5000202","currency":"USD","fr_converted_currency":"INR","fr_converted_amount":"520","fr_retention_amount":"420","fr_payable_amount":"320","mode_of_payment":"Online","dd_chq_number":"109","dd_chq_date":"15/04/2020","bank_name":"KARUR VYSYA BAN","bank_branch":"ANNA NAGA","contact_name":"Anchale","contact_address":"No:32,Srinivasaperumal Sannidhi,2nd Street,Royapettah","contact_city":"Chenn","contact_state":"Tamil","contact_country":"Ind","contact_pincode":"600020","contact_mobile":"9444208200","contact_email":"ndsurendra@gmail.in","iv_donor_category":"Individual","nationality":"In","project":"Infras","general_donation_amount":"120","govt_school_adoption_prgm_amount":"220","infrastructure_requirements_amount":"320","full_educational_support_qty":"420","full_educational_support_amount":"520","scholarship_qty":"620","scholarship_amount":"720","transport_subsidy_qty":"820","transport_subsidy_amount":"920","noon_meal_subsidy_qty":"1020","noon_meal_subsidy_amount":"1120","pan_card":"AVAPS8019H","platform":"plat","fr_project_id":"prjid","fr_project_name":"prjname","fr_project_link":"prjlink","campaign_id":"1","campaign_name":"campname","campaign_link":"camplink","fundraiser_name":"fndname","comments":"some comment","consolidated_receipt":"TRUE"}
    rec = {'src_msg': json.dumps(msg), 'processor': 'isha-vidhya-import',
           'process_msg': True}
    flag_dict = odoo_models.execute_kw(db, uid, password,
                                       'kafka.errors', 'create',
                                       [rec])


def test_athithi_contact_consumer():
    rpc_config = Configuration('RPC')
    # odoo server config
    username = rpc_config['RPC_USERNAME']
    password = rpc_config['RPC_PASSWORD']
    url = rpc_config['RPC_URL']
    db = config['db_name']
    common = client.ServerProxy('{}/xmlrpc/2/common'.format(url), allow_none=True)
    uid = common.authenticate(db, username, password, {})
    odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url), allow_none=True)

    msg = {

        'prof_dr': 'Ms.',
        'ishangam_id': 25,
        'name': 'Bandana Tewari',
        'alias': None,
        'gender': 'Female',
        'designation1': 'Director',
        'company': 'Vogue India (Fashion features)',
        'industry': 'Media',
        'sector': 'Media',
        'designation2': None,
        'company2': 'Apollo',
        'industry2': None,
        'sector2': None,
        'occupation': 'Media Executive',
        'additional_info': None,
        'primary_poc': 6,
        'secondary_poc': None,
        'influencer_type': 'Influencer',
        'relationship_status': 'Rajata',
        'phone_type': None,
        'phone_country_code': '91',
        'phone': '7899908423',
        'phone2_type': 'Home',
        'phone2_country_code': '91',
        'phone2': '7988880423',
        'phone3_type': None,
        'phone3_country_code': None,
        'phone3': None,
        'phone4_type': None,
        'phone4_country_code': None,
        'phone4': None,
        'whatsapp_country_code': None,
        'whatsapp_number': None,
        'email': 'arpita.shaleen@ishafoundation.org',
        'email2': None,
        'email3': None,
        'street': 'Champagne House,69, Worli sea face,Worli',
        'street2': None,
        'city': 'Mumbai',
        'state': 'Maharashtra',
        'country': 'India',
        'zip': '400025',
        'street2_1': 'Director, Vogue India (Fashion features), Darabshaw House, SV Marg, Ballard Estate, Fort,',
        'street2_2': None,
        'city2': 'Mumbai',
        'state2': 'Maharastra',
        'country2': 'India',
        'zip2': '400001',
        'work_street1': None,
        'work_street2': None,
        'work_city': None,
        'work_state': None,
        'work_country': None,
        'work_zip': None,
        # 'street': 'Maharashtra 400025 India',
        # 'street2': None,
        'zonal_category': 'C',
        'center_name': 'Mumbai - Others',
        'region_name': 'West India',
        'dob': None,
        'wikipedia_link': None,
        'twitter_handle': 'https://twitter.com/behavebandana',
        'twitter_followers': 40100,
        'instagram_id': 'https://www.instagram.com/behavebandana/',
        'insta_followers': 32500,
        'facebook_id': 'https://www.facebook.com/bandanatewari/',
        'facebook_likes': 11827,
        'facebook_followers': 12080,
        'linkedin_id': None,
        'linkedin_followers': None,
        'SPOC Name': None,
        'POC Name': None,
        # to add in the file
        'nationality': None,
        'local_contact_id': None,
        'marital_status': None,

    }
    rec = {'src_msg': json.dumps(msg), 'processor': 'athithi-contacts',
           'process_msg': True}
    flag_dict = odoo_models.execute_kw(db, uid, password,
                                       'kafka.errors', 'create',
                                       [rec])

def test_feedback_consumer():
    rpc_config = Configuration('RPC')
    # odoo server config
    username = rpc_config['RPC_USERNAME']
    password = rpc_config['RPC_PASSWORD']
    url = rpc_config['RPC_URL']
    db = config['db_name']
    common = client.ServerProxy('{}/xmlrpc/2/common'.format(url), allow_none=True)
    uid = common.authenticate(db, username, password, {})
    odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url), allow_none=True)

    msg = {
        'system_id': 52,
        'SSO id': '94b52cea23e64580a4614f931678f5',
        'Submission Date': '2021-09-04 12:42:38',
        'local_contact_id': 'feedback_Sept10_5',
        'First Name': 'Komal',
        'Last Name': 'Ademu',
        'Email': 'komalnabdepu@gmail.com',
        'phone_country_code': '+91',
        'Phone Number': '1023456789',
        'whatsapp_country_code': '+91',
        'WhatsApp Number': '1023456789',
        'Country': 'IN',
        'Feedback Occupation': 'Self-Employed',
        'Designation': 'Other',
        'event_name': 'Online Ananda Alai',
        'event_date': '2021-09-04',
        'Professional/ Other skills': 'Education',
        'Wish to say about pgm': 'It was amazing',
        'Interested in advanced pgms': 'Yes',
        'Volunteering Interest': 'Yes',
        'Programs Interested In': 'Sadhanapada',
        'Activities to Volunteer': 'IT Support (Programming, Testing, Web Development and Design)',
        'Volunteering Availability': 'Less than 30 min daily',
        'Marketing Skills': 'Brand Management',
        'IT Skills': None,
        'Multimedia Skills': 'Social Media',
        'Language Skills': 'Dubbing/ Voiceover',
        'Advertising Skills': 'Client Services',
        'Languages Known': 'Telugu',
        'FTV Interest': 'Yes'
        # 'How did you hear about Sadhguru?': 'Sadhguru’s YouTube Channel',
        # 'How did you hear about Inner Engineering': 'Sadhguru’s YouTube Channel',
        # 'What motivated you to sign up for Inner Engineering?': 'Spirituality',
        # 'What did you wish to derive from this program?': 'I found this as a wonderful start for my spiritual path so I want to build my body and mind in such a way that I am ready to enter into the advanced spirituality and without knowing inner me there is no spirituality.',
        # 'What specific benefits have you noticed since the program started?': 'I have started to my self and the world in a different perspective as of now.',
        # 'What do you feel is the most vital aspect of the program?': 'Sadhguru; Shambhavi Kriya; Shambhavi Kriya Initiation; The wisdom; I have been',
        # 'What would you like to say about the program?': 'the program was very well designed for online purpose.',
        # 'Would you recommend the program to your friends and family?': 'Yes',
        # 'Would you like to take advanced programs to experience higher levels of consciousness and wellbeing?': 'Yes',
        # "Would you like to support Isha's Initiatives?": 'Volunteering at Isha Yoga Center, India or Isha Institute Inner-Sciences, USA; Volunteering from home',
        # "Is there any other way you would like to support Isha Foundation's activities?": 'I would like to become a full time volunteer at isha serving my entire life selflessly and get into spiritual path more and more and try to get free from the cycle of life and death',
        # '(For participants within North America): How would you like to stay connected with local Isha meditators, programs and events? (Check all that apply)': None,
        # '(For participants outside the Americas): Would you like to be added to a Mandala WhatsApp group to support your 40 days mandala of Shambhavi Mahamudra? Trained Ishangas will be part of this group to clarify your queries and to support your practices.': 'Yes',
        # 'How easy was the login process to the system?': "5",
        # 'How was the video stream? Were you able to go through session smoothly?': "4",
        # 'If and when you had technical difficulties, did you find the necessary support to resolve them?': 'No',
        # 'Were the technical requirements to go through the program clear enough?': 'Yes',
        # 'Any other feedback on the technical aspects': 'I was not able to ask my question to sadhguru the chat box did not appear. and there was a lot of technical issue during initiation so I have missed out few parts which made me feel like my initiation was not complete'
    }
    rec = {'src_msg': json.dumps(msg), 'processor': 'feedback-contacts',
           'process_msg': True}
    flag_dict = odoo_models.execute_kw(db, uid, password,
                                       'kafka.errors', 'create',
                                       [rec])

def test_event_attendees_consumer():
    rpc_config = Configuration('RPC')
    # odoo server config
    username = rpc_config['RPC_USERNAME']
    password = rpc_config['RPC_PASSWORD']
    url = rpc_config['RPC_URL']
    db = config['db_name']
    common = client.ServerProxy('{}/xmlrpc/2/common'.format(url), allow_none=True)
    uid = common.authenticate(db, username, password, {})
    odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url), allow_none=True)

    msg = {
        'local_contact_id': 'Attendees_Jul_9',
        'Name': 'Mukul jain',
        'Email': 'mu@kl.com',
        'Phone Number': '6789567345',
        'WhatsApp Number': '6789567345',
        'Influencer Type': 'Influencer',
        'event_name': 'Golf',
        'Bay Name': 'Godavari',
        'Attendance notes': 'Something'
    }
    rec = {'src_msg': json.dumps(msg), 'processor': 'event-attendees',
           'process_msg': True}
    flag_dict = odoo_models.execute_kw(db, uid, password,
                                       'kafka.errors', 'create',
                                       [rec])

def test_prs_consumer(processor, metadata, logger):
    rpc_config = Configuration('RPC')
    # odoo server config
    username = rpc_config['RPC_USERNAME']
    password = rpc_config['RPC_PASSWORD']
    url = rpc_config['RPC_URL']
    db = config['db_name']
    common = client.ServerProxy('{}/xmlrpc/2/common'.format(url), allow_none=True)
    uid = common.authenticate(db, username, password, {})
    odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url), allow_none=True)

    msg = {
        "_id": "id2",
        "address": "B-2002, Hillcrest CHS, Sariput Nagar Road\nOpp. Seepz gate no.3, Off JVLR",
        "adhar_no": None,
        "arrival_date": None,
        "bay_name": "Yamuna(Student)",
        "checkin_status_day_2": None,
        "city": "Mumbai",
        "country": "India",
        "pincode": "400093",
        "created_by": None,
        "created_date": "2020-02-15 12:19:11",
        "departure_date": None,
        "dob": "07-08-1999",
        "email": "test-prs@gmail.com",
        "gender": "F",
        "idcard_no": "",
        "idcard_type": None,
        "ie_hear_about": "Volunteer",
        "ieo_class_status": "1",
        "is_ie_done": "No",
        "marital_status": None,
        "mobile": "9757064856",
        "modified_by": "anandstg@gmail.com",
        "modified_date": "2020-06-10 18:16:41",
        "name": "test one",
        "nationality": "India",
        "occupation": "Student",
        "organization": None,
        "passport_number": None,
        "phone_country_code": None,
        "program_id": "049",
        "program_location": None,
        "program_name": "Inner Engineering Completion with Sadhguru Mumbai March 2020",
        "register_mobile": None,
        "row_id": 138326,
        "state": "Maharashtra",
        "utm_campaign": None,
        "utm_medium": None,
        "whatsapp_country_code": None,
        "whatsapp_number": None,
        "end_date": "29-02-2020",
        "start_date": "28-02-2020",
        "status": "Confirmed",
        "ishangam_id": None,
        "sso_id": None,
        # "trans_lang":None,
        "alt_email": None,
        "alt_phone": None,
        "sso_id": "sso1"
    }

    rec = {'src_msg': json.dumps(msg), 'topic': 'prs', 'processor': 'prs',
           'process_msg': True}
    flag_dict = odoo_models.execute_kw(db, uid, password,
                                       'kafka.errors', 'create',
                                       [rec])


def test_sp_consumer():
    rpc_config = Configuration('RPC')
    # odoo server config
    username = rpc_config['RPC_USERNAME']
    password = rpc_config['RPC_PASSWORD']
    url = rpc_config['RPC_URL']
    db = config['db_name']
    common = client.ServerProxy('{}/xmlrpc/2/common'.format(url), allow_none=True)
    uid = common.authenticate(db, username, password, {})
    odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url), allow_none=True)

    msg = {
        "id": 5347872,
        "first_name": "RajKannan2",
        "last_name": "D",
        "email": "sdf-eew-dsdsd2-qa@ishafoundation.org",
        "mobile": "140xxxxx",
        "city": "Omaha",
        "state": "Nebraska",
        "country": "India",
        "postal_code": "86305",
        "sadhanapada_year": "2019 - 2020",
        "nationality": "India",
        "gender": "Male",
        "personal_email": "asdasd2@ymail.com",
        "education": "a:9:{s:18:\"educationundefined\";a:8:{s:8:\"category\";s:28:\"Bachelor of Commerce (B.Com)\";s:11:\"institution\";s:31:\"University of the Witwatersrand\";s:4:\"city\";s:12:\"Johannesburg\";s:13:\"qualification\";s:21:\"Batchelor of Commerce\";s:19:\"qualification_group\";s:8:\"Commerce\";s:14:\"specialization\";s:42:\"Business Information System and Management\";s:12:\"passing_year\";s:4:\"1993\";s:5:\"marks\";s:2:\"75\";}s:10:\"education8\";a:8:{s:8:\"category\";s:7:\"Diploma\";s:11:\"institution\";s:27:\"University of Witwatersrand\";s:4:\"city\";s:12:\"Johannesburg\";s:13:\"qualification\";s:32:\"Management Development Programme\";s:19:\"qualification_group\";s:8:\"Commerce\";s:14:\"specialization\";s:19:\"Business Management\";s:12:\"passing_year\";s:4:\"2006\";s:5:\"marks\";s:2:\"75\";}s:10:\"education9\";a:8:{s:8:\"category\";s:5:\"Other\";s:11:\"institution\";s:22:\"Robin Madanes Training\";s:4:\"city\";s:6:\"Online\";s:13:\"qualification\";s:10:\"Life Coach\";s:19:\"qualification_group\";s:5:\"Other\";s:14:\"specialization\";s:13:\"Life Coaching\";s:12:\"passing_year\";s:4:\"2018\";s:5:\"marks\";s:3:\"100\";}s:11:\"education26\";a:8:{s:8:\"category\";s:5:\"Other\";s:11:\"institution\";s:7:\"Nedbank\";s:4:\"city\";s:12:\"Johannesburg\";s:13:\"qualification\";s:18:\"Project Management\";s:19:\"qualification_group\";s:8:\"Commerce\";s:14:\"specialization\";s:32:\"IT Project Management in Banking\";s:12:\"passing_year\";s:4:\"2004\";s:5:\"marks\";s:2:\"75\";}s:11:\"education27\";a:8:{s:8:\"category\";s:5:\"Other\";s:11:\"institution\";s:5:\"Reiki\";s:4:\"city\";s:12:\"Johannesburg\";s:13:\"qualification\";s:18:\"Reiki Practitioner\";s:19:\"qualification_group\";s:5:\"Other\";s:14:\"specialization\";s:31:\"Auto immune diseases and trauma\";s:12:\"passing_year\";s:4:\"2006\";s:5:\"marks\";s:3:\"100\";}s:11:\"education28\";a:8:{s:8:\"category\";s:5:\"Other\";s:11:\"institution\";s:11:\"Reflexology\";s:4:\"city\";s:6:\"London\";s:13:\"qualification\";s:13:\"Reflexologist\";s:19:\"qualification_group\";s:5:\"Other\";s:14:\"specialization\";s:17:\"Cancer and Trauma\";s:12:\"passing_year\";s:4:\"2010\";s:5:\"marks\";s:2:\"80\";}s:11:\"education29\";a:8:{s:8:\"category\";s:5:\"Other\";s:11:\"institution\";s:7:\"Massage\";s:4:\"city\";s:6:\"London\";s:13:\"qualification\";s:26:\"Holistic Massage Therapist\";s:19:\"qualification_group\";s:5:\"Other\";s:14:\"specialization\";s:17:\"Cancer and trauma\";s:12:\"passing_year\";s:4:\"2011\";s:5:\"marks\";s:2:\"90\";}s:11:\"education30\";a:8:{s:8:\"category\";s:5:\"Other\";s:11:\"institution\";s:4:\"EFTi\";s:4:\"city\";s:6:\"London\";s:13:\"qualification\";s:16:\"EFT Practitioner\";s:19:\"qualification_group\";s:5:\"Other\";s:14:\"specialization\";s:17:\"Cancer and trauma\";s:12:\"passing_year\";s:4:\"2015\";s:5:\"marks\";s:2:\"90\";}s:11:\"education31\";a:8:{s:8:\"category\";s:5:\"Other\";s:11:\"institution\";s:18:\"Pilates Instructor\";s:4:\"city\";s:12:\"Johannesburg\";s:13:\"qualification\";s:18:\"Pilates Instructor\";s:19:\"qualification_group\";s:5:\"Other\";s:14:\"specialization\";s:21:\"Fitness and wellbeing\";s:12:\"passing_year\";s:4:\"2004\";s:5:\"marks\";s:2:\"90\";}}",
        "phone": "a:1:{s:14:\"phoneundefined\";a:3:{s:4:\"type\";s:8:\"Personal\";s:6:\"number\";s:10:\"4027140011\";s:11:\"countryCode\";d:1;}}",
        "address": "1901 Promontory",
        "date_of_birth": "1978-04-01",
        "passport_country": "India",
        "country_of_residence": "India",
        "program_history": "a:2:{s:16:\"program-history0\";a:4:{s:4:\"name\";s:39:\"Shambhavi Mahamudra Kriya Initiation/IE\";s:8:\"location\";s:12:\"Addison, TX.\";s:7:\"teacher\";s:6:\"Sharad\";s:4:\"date\";s:7:\"12-2018\";}s:16:\"program-history1\";a:4:{s:4:\"name\";s:17:\"Inner Engineering\";s:8:\"location\";s:12:\"Addison, TX.\";s:7:\"teacher\";s:6:\"Sharad\";s:4:\"date\";s:8:\"Dec-2018\";}}",
        "primary_emergency_contact_name": "Karen Danko",
        "primary_emergency_contact_address": "1901 Promontory\r\nPrescott, AZ. 86305\r\nUSA",
        "primary_emergency_contact_phone": "214-288-4112",
        "primary_emergency_contact_email": "jmdsakdanko@gmail.com",
        "primary_emergency_contact_relation": "Mother",
        "secondary_emergency_contact_name": "Melissa Schumacher",
        "secondary_emergency_contact_address": "17507 Douglas St\r\nOmaha, NE. 68118\r\nUSA",
        "secondary_emergency_contact_phone": "314-323-0070",
        "secondary_emergency_contact_email": "Melissaschumacher@gmail.com",
        "secondary_emergency_contact_relation": "sister",
        "tags": "SSO",
        "work_experience": "a:1:{s:5:\"work0\";a:6:{s:4:\"from\";N;s:2:\"to\";N;s:4:\"area\";s:16:\"Pharma & Biotech\";s:7:\"company\";s:30:\"Precision Compounding Pharmacy\";s:8:\"position\";s:19:\"Pharmacy Technician\";s:5:\"tasks\";s:37:\"I am in charge of the compounding lab\";}}",
        "languages": "a:1:{s:18:\"languagesundefined\";a:5:{s:4:\"name\";s:7:\"English\";s:8:\"speaking\";s:6:\"Fluent\";s:7:\"reading\";s:6:\"Fluent\";s:7:\"writing\";s:6:\"Fluent\";s:6:\"typing\";s:4:\"Fast\";}}",
        "person_id": 3453245,
        "ie_status": "COMPLETED",
        "ie_month_year": "2018-12",
        "arrival_date": "2019-07-07 11:00:00",
        "departure_date": "2020-02-21 06:00:00",
        "sadhanapada_status": "Approved",
        "mother_tongue": "English",
        "professional_details": "Indian Public Health Association, South East Asian Regional bureau of Health education, Rotary Club",
        "references_professional": "If i Studied 10th Std. My left arm was broken.",
        "skills_hobbies_awards": "a:9:{s:22:\"skills-hobbies-awards0\";a:2:{s:4:\"type\";s:6:\"Awards\";s:11:\"description\";s:104:\"Award of excellence for the efforts and dedicated service to in-house magazine called \u201CDDU Connect\u201D.\";}s:22:\"skills-hobbies-awards1\";a:2:{s:4:\"type\";s:6:\"Awards\";s:11:\"description\";s:82:\"Runner-up in Inter Class badminton Tournament \u2013 Singles, for the year 2011-2012.\";}s:22:\"skills-hobbies-awards2\";a:2:{s:4:\"type\";s:6:\"Awards\";s:11:\"description\";s:108:\"Runner-up in Inter Class badminton Tournament \u2013 Singles, Doubles & Mixed Doubles, for the year\r\n2012-2013.\";}s:22:\"skills-hobbies-awards3\";a:2:{s:4:\"type\";s:27:\"Extra Curricular Activities\";s:11:\"description\";s:82:\"Completed The Landmark Forum - personal development program by Landmark Worldwide.\";}s:22:\"skills-hobbies-awards4\";a:2:{s:4:\"type\";s:27:\"Extra Curricular Activities\";s:11:\"description\";s:108:\"Completed Coursera courses in Morality of Everyday Life by Paul Bloom and Social Psychology by\r\nScott Plous.\";}s:22:\"skills-hobbies-awards5\";a:2:{s:4:\"type\";s:27:\"Extra Curricular Activities\";s:11:\"description\";s:135:\"Founding Member & Technical Editor of the team which started an in-house magazine called \u201CDDU\r\nConnect\u201D. (January 2012 \u2013 May2013)\";}s:22:\"skills-hobbies-awards6\";a:2:{s:4:\"type\";s:27:\"Extra Curricular Activities\";s:11:\"description\";s:156:\"During my tenure at \u201CDDU Connect\u201D, we got to interact with the then Chief Minister of Gujarat Mr.\r\nNarendra Modi for an interview for \u201CDDU Connect\u201D.\";}s:22:\"skills-hobbies-awards7\";a:2:{s:4:\"type\";s:27:\"Extra Curricular Activities\";s:11:\"description\";s:109:\"Climbed Bhrigu Lake \u2013 a lake located at an elevation of around 4300 metres on Himalaya in Himachal Pradesh.\";}s:22:\"skills-hobbies-awards8\";a:2:{s:4:\"type\";s:27:\"Extra Curricular Activities\";s:11:\"description\";s:119:\"Have played All India Inter Engineering College Badminton Championship (National Event) held in the year 2013 at Surat.\";}}",
        "employed_in_india": "Yes",
        "personal_skills": "Leadership, Good Communication, People coordination, observing and transmitting certain aspect to others",
        "knowledge_based_skills": "Typing, Computer handling, Mathamatics Teaching, Karate (Maritial arts) Teaching, Silambam, Project coordination and Management, Project implementation and Monitoring, Handling trainings.",
        "transferable_skills": "Project Coordination and Management, Project implementation and Monitoring,",
        "computer_skills": "MS Office-word, Excel, Power-point, etc",
        "references_volunteering": "a:4:{s:7:\"refvol0\";a:4:{s:4:\"name\";s:13:\"Bhargavi akka\";s:6:\"center\";s:4:\"Pune\";s:5:\"phone\";s:10:\"9860073950\";s:5:\"email\";N;}s:7:\"refvol1\";a:4:{s:4:\"name\";s:12:\"Maa Hruditya\";s:6:\"center\";s:3:\"IYC\";s:5:\"phone\";s:10:\"9489045212\";s:5:\"email\";N;}s:7:\"refvol2\";a:4:{s:4:\"name\";s:11:\"Maa Jayitri\";s:6:\"center\";s:3:\"IYC\";s:5:\"phone\";s:10:\"8903816404\";s:5:\"email\";N;}s:7:\"refvol3\";a:4:{s:4:\"name\";s:11:\"Sheela akka\";s:6:\"center\";s:3:\"IYC\";s:5:\"phone\";s:10:\"8903816403\";s:5:\"email\";N;}}",
        "teacher_training": "a:1:{s:17:\"teacher-training0\";a:5:{s:5:\"class\";s:4:\"HYTT\";s:8:\"language\";s:7:\"English\";s:17:\"trainingCompleted\";s:3:\"Yes\";s:14:\"handledSession\";s:3:\"Yes\";s:13:\"completedDate\";s:8:\"Dec-2015\";}}",
        "volunteeringAtIYCOption": "No",
        "volunteering_iyc": "a:2:{s:17:\"volunteering-iyc0\";a:5:{s:8:\"activity\";s:43:\"Volunteering with Isha School of Hatha Yoga\";s:11:\"description\";s:122:\"Hall activities, recitations, prelims, software development, translations, admissions support, conducting class for guests\";s:4:\"date\";s:8:\"Jul-2016\";s:10:\"department\";s:10:\"hatha_yoga\";s:15:\"coordinatorname\";s:11:\"Maa Jayitri\";}s:17:\"volunteering-iyc1\";a:5:{s:8:\"activity\";s:35:\"Volunteering at Rejuvenation Centre\";s:11:\"description\";s:122:\"Hall activities, recitations, prelims, software development, translations, admissions support, conducting class for guests\";s:4:\"date\";s:8:\"Oct-2018\";s:10:\"department\";s:10:\"hatha_yoga\";s:15:\"coordinatorname\";s:11:\"Maa Jayitri\";}}",
        "stay_duration_during_volunteering": "Mar 2016 to Mar 2017, Nov 2017 to Dec 2017, Oct 2018 till date.(17 months) AYA stay area.",
        "volunteeringLocalOption": "No",
        "volunteering_local": "a:1:{s:19:\"volunteering-local0\";a:6:{s:8:\"activity\";s:47:\"Volunteering for IE, GuruPooja, IDY, Mystic Eye\";s:11:\"description\";s:99:\"Volunteered for IE, IDY, Guru pooja programs. Conducted IDY sessions, registrations for Mystic eye.\";s:8:\"location\";s:4:\"Pune\";s:4:\"date\";s:8:\"Jun-2014\";s:10:\"department\";s:17:\"inner_engineering\";s:15:\"coordinatorname\";s:8:\"Bhargavi\";}}",
        "skillyouHave": "A lot~anything~interact with people~understand them~learns new skills quickly~efficient~computer savvy~managing people~understand o peopleÃ¢â‚¬â„¢s needs and weaknesses~",
        "ishaConnect": "a:4:{i:0;s:29:\"Only IE, no Isha volunteering\";i:1;s:8:\"Drop out\";i:2;s:7:\"Fresher\";i:3;s:28:\"Unemployed for over 6 months\";}",
        "sp_remarks": "Couldn't come due to flight cancellations",
        "person_uid": "5d820a852f02b.104b957a4e34e02e7b67b65aa136b00c-f94991d877",
        "marital_status": "Unmarried",
        "spouses_name": "laxmi",
        "fathers_name": "Nataraja",
        "mothers_name": "Latha",
        "mental_ailments": "No",
        "mental_ailments_in_family": "No",
        "current_medication": "Yes, I take Estradiol as part of my hormone replacement therapy initiated during my gender transition.",
        "past_medication": "Yes, I used to take Spironolactone as part of my hormone therapy, but it is no longer needed.",
        "drug_usage": "No",
        "drugTreatment": "No",
        "surgery_details": "Yes, I had cosmetic surgery on my face in 2012 to feminize my features and genital reassignment surgery in 2015 to finalize my gender transition.",
        "physical_disabilities": "No",
        "hospitalized": "No",
        # "family_details": "a:3:{s:19:\"family-sadhanapada1\";a:12:{s:4:\"name\";s:15:\"KVD Prasada Rao\";s:12:\"relationship\";s:6:\"Father\";s:11:\"dateOfBirth\";O:8:\"DateTime\":3:{s:4:\"date\";s:26:\"1962-12-30 00:00:00.000000\";s:13:\"timezone_type\";i:3;s:8:\"timezone\";s:13:\"Asia/Calcutta\";}s:6:\"gender\";s:4:\"Male\";s:19:\"occupationalAddress\";N;s:10:\"occupation\";s:20:\"Chartered Accountant\";s:14:\"contactAddress\";s:53:\"Flat no. 103, tirtha four seasons, attapur, Hyderabad\";s:9:\"contactNo\";s:10:\"7337097378\";s:5:\"email\";s:21:\"Kvdprasad12@gmail.com\";s:8:\"resident\";N;s:10:\"department\";N;s:12:\"is_meditator\";s:2:\"No\";}}",
        "family_details": "a:1:{s:19:\"family-sadhanapada1\";a:12:{s:4:\"name\";s:15:\"KVD Prasada Rao\";s:12:\"relationship\";s:6:\"Father\";s:6:\"gender\";s:4:\"Male\";s:19:\"occupationalAddress\";N;s:10:\"occupation\";s:20:\"Chartered Accountant\";s:14:\"contactAddress\";s:53:\"Flat no. 103, tirtha four seasons, attapur, Hyderabad\";s:9:\"contactNo\";s:10:\"7337097378\";s:5:\"email\";s:21:\"Kvdprasad12@gmail.com\";s:8:\"resident\";N;s:10:\"department\";N;s:12:\"is_meditator\";s:2:\"No\";}}",
        "yatra_history": "a:2:{s:16:\"program-history0\";a:2:{s:4:\"name\";s:8:\"Varanasi\";s:4:\"date\";s:8:\"Sep-2018\";}s:16:\"program-history1\";a:2:{s:4:\"name\";s:9:\"Himalayas\";s:4:\"date\";s:8:\"Sep-2019\";}}",
        "event_history": "a:5:{s:16:\"program-history0\";a:3:{s:4:\"name\";s:18:\"Mahashivratri 2016\";s:8:\"location\";s:3:\"IYC\";s:4:\"date\";s:8:\"Feb-2016\";}s:16:\"program-history1\";a:3:{s:4:\"name\";s:57:\"Consecration of Yogeshwar Linga | Consecration of Adiyogi\";s:8:\"location\";s:3:\"IYC\";s:4:\"date\";s:8:\"Feb-2016\";}s:16:\"program-history2\";a:3:{s:4:\"name\";s:24:\"In the Lap of the Master\";s:8:\"location\";s:3:\"IYC\";s:4:\"date\";s:8:\"Jul-2016\";}s:16:\"program-history3\";a:3:{s:4:\"name\";s:56:\"Ishanga 7% - \u2018 Nanmai Uruvam\u2019 Ceremony with Sadhguru\";s:8:\"location\";s:3:\"IYC\";s:4:\"date\";s:8:\"May-2015\";}s:16:\"program-history4\";a:3:{s:4:\"name\";s:20:\"Mahashivarathri 2015\";s:8:\"location\";s:3:\"IYC\";s:4:\"date\";s:8:\"Feb-2015\";}}",
        "interviewer_assessment": "Some concerns",
        "action_item": "Very little exposure so far~encouraged her to come ahead and volunteer",
        "redFlags": "a:6:{i:0;s:24:\"dropout from engineering\";i:1;s:40:\"parents not supportive. IE in March 2019\";i:2;s:27:\"no volunteering experience \";i:3;s:5:\"Other\";i:4;s:5:\"Other\";i:5;s:5:\"Other\";}",
        "redFlagsOther": "dropout from engineeringparents not supportive. IE in March 2019no volunteering experience",
        "difficult_situation": "Yes. He says he has no money and does not want to work. Family is expecting him to support them~but he wants to come here full time",
        "additionalObservation": "a:3:{i:0;s:20:\"Good in articulation\";i:1;s:12:\"Enthusiastic\";i:2;s:5:\"Other\";}",
        "additionalObservationother": "Can help with oco perhaps ",
        "person_interviewed": "Tina",
        "interview_date_time": "2019-12-04 18:33:00",
        "interviewerInput": "Young and enthusiastic|No|No|Thyroid issues~hypo |Chicken pox as a child|No|No|Thyroid meds|Thyroid |Alcohol occasionally~nothing last 3-4 months",
        "vroFeedback": "Not so regular for gurupuja. No specific complaints from sannidhi or radham or food shifting coordinators. Follows VRO instructions regularly",
        "trial_period_feedback": "VRO: Regular for GP, meeting and activities. Good feedback from VRO. Always appears smiling",
        "greatest_modified_date": "2020-09-05 16:59:59",
        "spComments": "Positive feedback after trial",
        "doctor_approval": "Approved",
        "doctor_assigned": "Thenmozhi P",
        "doctor_screening": "Approved",
        "height": "185",
        "sp_weight": "74",
        "areasContribute": "Not sure yet",
        "call_status": "call_completed",
        "pre_reg_cancellation_date": "2021-02-21 21:49:21",
        "pre_reg_cancellation_reason": "Personal commitments",
        "criminal_history": "Yes. Accused of 20(b),ndps act",
        "createdAt": "2019-01-24 10:03:35",
        "reg_email_status": 1,
        "expertiseAreas": "Pharmacy for 9 years",
        "full_form_status": 1,
        "ha_form_sent": "Yes",
        "ha_form_submitted": "Yes",
        "submitted_on": "2019-04-12",
        "incarceration_details": "Yes... for one day i have been jail....becoz of a fight",
        "ie_program_check": 1,
        "ie_location": "Yadein Marriage Hall,Seikhpura,Patna.",
        "ie_online_status": "Pending",
        "interview_status": "Approval after Trial",
        "interviewerOpinion": "Selected",
        "legalCaseExist": "No",
        "is_matched": 1,
        "countryCode": "91",
        "noResponseCounter": "3",
        "otherLiabilities": "No",
        "p.past_medication": "Yes, I used to take Spironolactone as part of my hormone therapy, but it is no longer needed.",
        "application_stage_pending_reason": "Work commitments",
        "physical_ailments": "No",
        "pre_registration_status": "all_set",
        "reviewStatus": "Review Completed",
        "sp_form_sent_date": "2019-05-23",
        "sdp_tagged_comments": "SP2020 Approved",
        "sdp_tagged_status": "Yet to Review",
        "p_extn.expertiseAreas": "Pharmacy for 9 years",
        "p_extn.skillyouHave": "A lot~anything~interact with people~understand them~learns new skills quickly~efficient~computer savvy~managing people~understand o peopleÃ¢â‚¬â„¢s needs and weaknesses~",
        "sp_reference_id": "a:9:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:2:\"15\";i:3;s:2:\"13\";i:4;s:1:\"4\";i:5;s:2:\"10\";i:6;s:1:\"5\";i:7;s:2:\"19\";i:8;s:2:\"21\";}",
        "trail_period_status": "Cancelled",
        "utmCampaign": "weeklynlfm",
        "utmSource": "mkto",
        "utmMedium": "email",
        "ie_teacher_name": "Sadhguru",
        "zip_code": "641114",
        "passport_no": "590283872",
        "passport_city_of_issue": "Omaha",
        "passport_country_of_issue": "United States",
        "passport_issue_date": "2016-05-02",
        "passport_expiry_date": "2026-05-02",
        "other_practices": "SadhnaPaada",
        "user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:64.0) Gecko/20100101 Firefox/64.0",
        "user_registered_ip": "172.21.1.6",
        "registration_source": "isha.sadhguru.org",
        "utmTerm": "video-ad",
        "utmContent": "transformation-video"
    }

    rec = {'src_msg': json.dumps(msg), 'topic': 'crm-sp-qa-suvya-odoo-apr02-11am', 'processor': 'sp-registration',
           'process_msg': True}
    flag_dict = odoo_models.execute_kw(db, uid, password,
                                       'kafka.errors', 'create',
                                       [rec])

def test_sp_contact_consumer():
    rpc_config = Configuration('RPC')
    # odoo server config
    username = rpc_config['RPC_USERNAME']
    password = rpc_config['RPC_PASSWORD']
    url = rpc_config['RPC_URL']
    db = config['db_name']
    common = client.ServerProxy('{}/xmlrpc/2/common'.format(url), allow_none=True)
    uid = common.authenticate(db, username, password, {})
    odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url), allow_none=True)

    msg = {
        "local_id": 1002,
        "name": "Anand",
        "phone_country_code": "91",
        "phone": "9878671232",
        "email": "anand@maildrop.cc",
        "system_id": "61",
        "gender":"M",
        "dob":"1984-08-25"
    }

    rec = {'src_msg': json.dumps(msg), 'topic': 'SP_CONTACT_SEARCH_REQUEST', 'processor': 'sulaba-contact-search',
           'process_msg': True}
    flag_dict = odoo_models.execute_kw(db, uid, password,
                                       'kafka.errors', 'create',
                                       [rec])

#
#
# def test_ieo_consumer( processor, metadata, logger):
#     msg = {
#         "payload":{
#             "user_id":"801987",
#             "first_name":"Rec",
#             "last_name":"ieo",
#             "email":"test-ieo@example.com",
#             "sex":"Female",
#             "resident_city":"Hyderabad",
#             "resident_state":"null",
#             "resident_ZIP":"500092",
#             "resident_country":"IN",
#             "custType":"PAYASUCAN",
#             "coupon_code_value":"Health care professi",
#             "lastUpdTime":"2020-07-14 02:33:56",
#             "class_id":"6",
#             "course_completion_date":"",
#             "start_date":"2020-04-14 00:00:00",
#             "end_date":"2020-07-28 11:38:04",
#             "last_login_date":"2020-07-13",
#             "homePhone":"9177621670",
#             "mobilePhone":"8142881120",
#             "langPref":"EN"
#         }
#     }
#     processor.process_message(msg,  metadata)
#
#
# def test_dms_contact_consumer( processor, metadata, logger):
#     msg = {
#         'local_contact_id': 'dms-con',
#         'modified_at': 121,
#         'modified_by': 'dms one',
#         'contact_type': 'INDIVIDUAL',
#         'title': 'MR',
#         'first_name': 'dms one',
#         'dob': None,
#         'gender': None,
#         'occupation': None,
#         'company_ids': None,
#         'nationality': None,
#         'phones': None,
#         'emails': None,
#         'address1': None,
#         'town_city1': None,
#         'state_province_region1': None,
#         'postal_code1': None,
#         'country1': None,
#         'pan_number': None,
#         'deceased': None,
#         'do_not_call': None,
#         'do_not_email': None,
#         'do_not_postmail': None,
#         'active': None,
#     }
#     processor.process_message(msg,  metadata)
#
#
# def test_dms_donation_consumer( processor, metadata, logger):
#     msg = {
#         'payload': {
#             'local_donor_id': 'dms-con',
#             'receipt_date': None,
#         }
#     }
#     processor.process_message(msg,  metadata)
