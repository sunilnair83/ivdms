import uuid
from datetime import datetime, date

import Levenshtein as lev
from fuzzywuzzy import fuzz

from . import ContactProcessor
from . import NameMatchEngine

null = None
NO_MATCH = 0
LOW_MATCH = 1
HIGH_MATCH = 2
EXACT_MATCH = 4
NA = -1

class GoldenContactAdv:

    mapModel = None
    contactModel = None
    countryModel = None
    stateModel = None
    lowMatchModel = None
    pincodeModel = None
    ishaCenterModel = None
    low_match = []
    CR = None
    env = None

    def setEnvValues(self, ENV):
        self.contactModel = ENV['res.partner']
        self.mapModel = ENV['contact.map']
        self.lowMatchModel = ENV['low.match.pairs']
        self.pincodeModel = ENV['isha.pincode.center']
        self.ishaCenterModel = ENV['isha.center']
        self.countryModel = ENV['res.country']
        self.stateModel = ENV['res.country.state']
        self.low_match = []
        self.rel_match = []
        self.CR = ENV.cr
        self.env = ENV

    def createOrUpdate(self, ENV, src_msg):
        self.setEnvValues(ENV)
        flag_dict = {'flag':False, 'rec':None, 'phase':''}

        '''
            Phase 0 -> If the record already contains guid or local_contact_id exists in contact_map
                        then find the Matching record and update the values
                        else follow match and merge
        '''
        flag_dict = self.initialPhase(src_msg)

        '''
            Phase 1 -> Find Records with Exact Phone match
                        1.1 ->  CDI's Name match to populate high_match_list and global_low_match_list
                    -> Merge the tail of high_match_list with src_msg else go to Phase 2.
        '''
        if not flag_dict['flag']:
            flag_dict = self.firstPhaseSearch(src_msg)

        '''
            Phase 2 -> Find Records with Exact email Match
                        2.1 ->  CDI's Name match to populate high_match_list and global_low_match_list
                    -> Merge the tail of high_match_list with src_msg if exists else go to Phase 3.
        '''
        if not flag_dict['flag']:
            flag_dict = self.secondPhaseSearch(src_msg)

        '''
            Phase 3 -> Find Records with Exact Name Match
                    3.1 ->  In those records find records upto 2 edit distance variation on phone.
                        ->  Add to high_match_list if distance is <= 1
                        ->  Add to global_low_match_list if distance = 2
                    3.2 ->  Find record with 1 edit distance on email and add to global_low_match_list
                    3.3 - > Find records with exact zip match
                        - > And 70 street match and to global_low_match_list
                        - > If 90 street match and phone, email both are None then add to high_match_list

                    -> Merge the tail of high_match_list with src_msg if exists else go to Phase 4
        '''
        if not flag_dict['flag']:
            flag_dict = self.thirdPhaseSearch(src_msg)

        '''
            Phase 4 -> Create new Record.
        '''
        if not flag_dict['flag']:
            created_rec = self.create_new_contact(src_msg)
            flag_dict = {'flag': True, 'rec': created_rec}
            flag_dict['phase'] = 'phase4'

        # self.CR.commit()
        return flag_dict

    def initialPhase(self, src_msg):
        # Phase 0
        high_match = []
        # check if the local contact id already exists
        map_rec = None
        if src_msg['local_contact_id']:
            map_rec = self.getContactMapRec(src_msg['local_contact_id'], src_msg['system_id'])
        if map_rec and len(map_rec) > 0:
            res_rec = map_rec[0]['contact_id_fkey']
            # Ignoring PRS, Ereceipts, SgX, IEO Updates as these systems don't have any workflows to update contacts
            if src_msg['system_id'] == 13 or src_msg['system_id'] == 8 or src_msg['system_id'] == 33 or src_msg['system_id'] == 30:
                return {'phase':'phase0', 'flag':True, 'rec':res_rec}
            for rec in res_rec:
                high_match.append(rec)
            flag_dict = self.mergeRecord(high_match, src_msg)
            if 'phase' not in flag_dict:
                flag_dict['phase'] = 'phase0'
            return flag_dict
        # check if contact_id_fkey is directly given
        if 'contact_id_fkey' in src_msg:
            contact_id_fkey = src_msg.pop('contact_id_fkey',None)
            contact_rec = self.contactModel.search([('id','=',contact_id_fkey)])
            if len(contact_rec) > 0:
                high_match.append(contact_rec[0])
                flag_dict = self.mergeRecord(high_match, src_msg)
                if 'phase' not in flag_dict:
                    flag_dict['phase'] = 'phase0'
                return flag_dict
        # check if guid has values
        if 'guid' in src_msg and src_msg['guid']:
            rec_list = self.getMatchRecordList('guid', '=', src_msg['guid'])
            if len(rec_list) > 0:
                high_match = []
                for x in rec_list:
                    high_match.append(x)
                flag_dict = self.mergeRecord(high_match, src_msg)
                if 'phase' not in flag_dict:
                    flag_dict['phase'] = 'phase0'
                return flag_dict
        # check if sso_id has values
        if 'sso_id' in src_msg and src_msg['sso_id']:
            rec_list = self.getMatchRecordList('sso_id', '=', src_msg['sso_id'])
            if len(rec_list) > 0:
                high_match = []
                for x in rec_list:
                    high_match.append(x)
                flag_dict = self.mergeRecord(high_match, src_msg)
                if 'phase' not in flag_dict:
                    flag_dict['phase'] = 'phase0'
                return flag_dict
        # else follow match and merge
        return {'flag':False, 'rec':None, 'phase':''}

    def firstPhaseSearch(self, src_msg):
        flag_dict = {'flag':False, 'rec':None, 'phase':''}

        # Phase 1
        # search for Exact Phone match
        rec_list = self.getPhoneMatch(src_msg['phone']) if src_msg['phone'] and len(src_msg['phone']) > 9 else self.contactModel
        rec_list2 = self.getPhoneMatch(src_msg['phone2']) if src_msg['phone2'] and len(src_msg['phone2']) > 9 else self.contactModel

        rec_list |= rec_list2
        # if len(rec_list) == 0:
        #     rec_list = self.getEditDistancePhone(src_msg['phone'])

        if len(rec_list) > 0:
            # Phase 1.1
            # CDI's Name Match
            matchEngine = NameMatchEngine.NameMatchEngine()
            high_match = []
            for rec in rec_list:
                match_status = matchEngine.compareNamesWithVariations(src_msg['name'], rec['name'])
                if match_status == EXACT_MATCH or match_status == HIGH_MATCH:
                    high_match.append(rec)
                elif match_status == LOW_MATCH:
                    self.low_match.append(rec)
                else:
                    if rec.influencer_type:
                        self.rel_match.append(rec)

                # Removing as part of ISH-999
                # if rec.influencer_type:
                #     src_msg.update({'influencer_type': 'unassigned'})

            if len(high_match) > 0:
                flag_dict = self.mergeRecord(high_match, src_msg)
                flag_dict['phase'] = 'phase1'

        return flag_dict


    def secondPhaseSearch(self, src_msg):
        flag_dict = {'flag':False, 'rec':None, 'phase':''}

        # Phase 2
        # search for Exact email match
        rec_list = self.getEmailMatch(src_msg['email']) if src_msg['email'] else self.contactModel
        rec_list2 = self.getEmailMatch(src_msg['email2']) if src_msg['email2'] else self.contactModel

        rec_list = rec_list | rec_list2
        if len(rec_list) > 0:
            # Phase 2.1
            # CDI's Name Match
            matchEngine = NameMatchEngine.NameMatchEngine()
            high_match = []
            for rec in rec_list:
                match_status = matchEngine.compareNamesWithVariations(src_msg['name'],rec['name'])
                if match_status == EXACT_MATCH or match_status == HIGH_MATCH:
                    high_match.append(rec)
                elif match_status == LOW_MATCH:
                    self.low_match.append(rec)
                else:
                    if rec.influencer_type:
                        self.rel_match.append(rec)

                # Removing as part of ISH-999
                # if rec.influencer_type:
                #     # Email matched, so mark as influencer
                #     src_msg.update({'influencer_type': 'unassigned'})

            if len(high_match) > 0:
                flag_dict = self.mergeRecord(high_match, src_msg)
                flag_dict['phase'] = 'phase2'

        return flag_dict


    def thirdPhaseSearch(self, src_msg):
        flag_dict = {'flag':False, 'rec':None, 'phase':''}
        # Phase 3
        # search for exact name match
        rec_list = self.getNameMatch(src_msg['name']) if src_msg['name'] != null and len(src_msg['name']) > 4 else self.contactModel

        if len(rec_list) > 0:
            high_match = []
            typecast = lambda x: x.lower().strip() if type(x) == str else None
            for rec in rec_list:
                # Phase 3.1
                # 1-Edit-distance match for phone, phone2, phone3 -> HIGH MATCH
                # 2-Edit-distance match for phone, phone2, phone3 -> LOW MATCH
                if rec['phone'] and src_msg['phone']:
                    dist = lev.distance(src_msg['phone'], rec['phone']) if src_msg['phone'] else 999
                    if dist <= 1 and rec not in high_match:
                        high_match.append(rec)
                        continue
                    elif dist == 2 and rec not in self.low_match:
                        self.low_match.append(rec)
                        continue
                if rec['phone2'] and src_msg['phone']:
                    dist = lev.distance(src_msg['phone'], rec['phone2']) if src_msg['phone'] else 999
                    if dist <= 1 and rec not in high_match:
                        high_match.append(rec)
                        continue
                    elif dist == 2 and rec not in self.low_match:
                        self.low_match.append(rec)
                        continue
                if rec['phone3'] and src_msg['phone']:
                    dist = lev.distance(src_msg['phone'], rec['phone3']) if src_msg['phone'] else 999
                    if dist <= 1 and rec not in high_match:
                        high_match.append(rec)
                        continue
                    elif dist == 2 and rec not in self.low_match:
                        self.low_match.append(rec)
                        continue

                # Email 1-Edit-Distance -> Low Match
                if rec['email'] and src_msg['email']:
                    dist = lev.distance(typecast(src_msg['email']),typecast(rec['email']))
                    if dist<=1 and rec not in self.low_match:
                        self.low_match.append(rec)

                # Address check only if zip matches
                # if street match ratio > 70 -> low match
                # if street match ratio > 90 and phone,email both are none -> High match
                if rec['zip'] == src_msg['zip'] and rec not in self.low_match:
                    street_match = fuzz.token_set_ratio(typecast(rec['street']), typecast(src_msg['street']))
                    if src_msg['street'] and rec['street'] and street_match > 70:
                        self.low_match.append(rec)
                        if street_match > 90 and src_msg['phone'] is None and src_msg['email'] is None:
                            high_match.append(rec)

            if len(high_match) > 0:
                flag_dict = self.mergeRecord(high_match, src_msg)
                flag_dict['phase'] = 'phase3'

        return flag_dict


    def closeCursorWhenException(self):
        if self.CR is not None:
            self.CR.close()

    def getMatchRecordList(self, field, condition, value):
        match = self.contactModel.sudo().search([(field, condition, value)])
        return match

    def getDomainMatches(self, args):
        match = self.contactModel.sudo().search(args)
        return match

    def getPhoneMatch(self, phone):
        rec = self.getDomainMatches(['|', '|', '|', ('phone', '=', phone), ('phone2', '=', phone), ('phone3','=',phone), ('phone4','=',phone)])
        # rec = self.getMatchRecordList('phone', '=', phone)
        # if len(rec) == 0:  # secondary search
        #     rec = self.getMatchRecordList('phone2', '=', phone)
        # if len(rec) == 0:  # final search
        #     rec = self.getMatchRecordList('phone3','=',phone)
        # if len(rec) == 0:  # final search
        #     rec = self.getMatchRecordList('phone4','=',phone)

        return rec

    def getEditDistancePhone(self, phone):
        self.env.cr.execute("SELECT set_limit(0.57);")
        return self.getMatchRecordList('phone','%',phone)

    def getEmailMatch(self, email):
        if type(email) == str:
            email = email.lower()
        rec = self.getDomainMatches(['|', '|', ('email', '=', email), ('email2', '=', email), ('email3', '=', email)])
        # rec = self.getMatchRecordList('email', '=ilike', email)
        # if len(rec) == 0:  # secondary search
        #     rec = self.getMatchRecordList('email2', '=ilike', email)
        # if len(rec) == 0:  # secondary search
        #     rec = self.getMatchRecordList('email3', '=ilike', email)
        return rec


    def getNameMatch(self, name):
        rec = self.getMatchRecordList('name', '=ilike', name)
        return rec

    def getLongerString(self, str1, str2):
        if type(str1) == str and type(str2) == str:
            if len(str1.split()) == len(str2.split()):
                return str1 if len(str1) > len(str2) else str2
            else:
                return str1 if len(str1.split()) > len(str2.split()) else str2
        if type(str1) == str and type(str2) != str:
            return str1
        if type(str2) == str and type(str1) != str:
            return str2
        return None

    def is_two_way_substring(self, str1, str2):
       return str1.startswith(str2) or str2.startswith(str1)

    def getCountryCode(self, country):
        if country is not None:
            country_id = self.countryModel.search([('code', '=', country)], limit=1)
            if len(country_id) != 0:
                return country_id[0].id
        return None

    def getStateCode(self, state):
        if state is not None:
            state_id = self.stateModel.search([('name', '=', state)], limit=1)
            if len(state_id) != 0:
                return state_id[0].id
        return None

    def get_first_name(self, name):
        cp = ContactProcessor
        name_dict = cp.getTitlesRemoved(name)
        name_token = name_dict['name'].split(' ')
        first_name = None
        for x in name_token:
            if len(x) >= 3:
                first_name = x
                break

        return first_name

    def is_lenient_name_match(self, rec, name):
        first_name = self.get_first_name(name)
        if first_name and first_name.lower() in rec.name.lower():
            return True
        else:
            return False


    def nameMatchList(self, rec_list, name, high_match, low_match, lenient_search=False):
        if len(rec_list) > 0:
            matchEngine = NameMatchEngine.NameMatchEngine()
            for rec in rec_list:
                match_status = matchEngine.compareNamesWithVariations(name, rec['name'])
                if match_status == EXACT_MATCH or match_status == HIGH_MATCH or (lenient_search and self.is_lenient_name_match(rec,name)):
                    high_match.append(rec)
                elif match_status == LOW_MATCH:
                    low_match.append(rec)
        return

    def getMatchList(self,env, name, phone, email,lenient_search=False, exact_email_search=False):
        self.setEnvValues(env)
        high_match = []
        low_match = []
        phone_rec_list = self.contactModel
        email_rec_list = self.contactModel
        if phone:
            phone_rec_list = self.getPhoneMatch(phone)
            if len(phone_rec_list) == 0:
                phone_rec_list = self.getEditDistancePhone(phone)
        if email:
            if exact_email_search:
                email_rec_list = self.getDomainMatches(
                    ['|', '|', ('email', '=', email), ('email2', '=', email), ('email3', '=', email)])
            else:
                email_rec_list = self.getEmailMatch(email)

        resultant = phone_rec_list | email_rec_list
        self.nameMatchList(resultant, name, high_match, low_match, lenient_search)

        if len(high_match) == 0:
            return {'is_high_match':False,'matches':low_match}
        else:
            return {'is_high_match': True, 'matches': high_match}

    def getApiSearchList(self, env, name, phone, email,lenient_search=False):
        self.setEnvValues(env)
        domain_list = []
        if phone:
            for x in ['|','|','|',('phone','=',phone),('phone2','=',phone),('phone3','=',phone),('phone4','=',phone)]:
                domain_list.append(x)
        if email:
            for x in ['|','|',('email','=ilike',email),('email2','=ilike',email),('email3','=ilike',email)]:
                domain_list.append(x)
        if (phone or email):
            rec_list = self.contactModel.search(domain_list)
            if name:
                high_match = []
                low_match = []
                self.nameMatchList(rec_list,name,high_match,low_match,lenient_search)
                return high_match
            elif not name:
                return rec_list
        if name:
            rec_list = self.getNameMatch(name)
            return rec_list
        else:
            return []

    def getRecentConact(self, rec_list):
        if len(rec_list) > 0:
            recent_contact = rec_list[0]
            for rec in rec_list:
                if rec.write_date > recent_contact.write_date:
                    recent_contact = rec
            return recent_contact

    def is_valid_insertion_sort(self, phone_list, phone_country_code, phone_valid, phone_verified):
        for i in range(1, len(phone_list)):
            phone_key = phone_list[i]
            phone_country_code_key = phone_country_code[i]
            phone_valid_key = phone_valid[i]
            phone_verified_key = phone_verified[i]

            j = i - 1
            while j >= 0 and ((phone_valid_key == True and phone_valid[j] == False) or
                              (phone_verified_key == True and phone_verified[j] == False)):
                phone_list[j + 1] = phone_list[j]
                phone_country_code[j + 1] = phone_country_code[j]
                phone_valid[j + 1] = phone_valid[j]
                phone_verified[j + 1] = phone_verified[j]
                j -= 1
            phone_list[j + 1] = phone_key
            phone_country_code[j + 1] = phone_country_code_key
            phone_valid[j + 1] = phone_valid_key
            phone_verified[j + 1] = phone_verified_key


    def getPhonesDict(self, update_rec, src_rec):
        # Phone
        phone_key = ['phone','phone2','phone3','phone4']
        phone_dict = {}
        phone_list = []
        phone_country_code = []
        phone_valid = []
        phone_verified = []
        for x in phone_key:
            if x in src_rec and src_rec[x] and (src_rec[x] not in phone_list):
                phone_list.append(src_rec[x])
                phone_country_code.append(src_rec[x+'_country_code'] if (x + '_country_code') in src_rec else None)
                phone_valid.append(src_rec[x + '_is_valid'] if (x + '_is_valid') in src_rec else None)
                phone_verified.append(src_rec[x + '_is_verified'] if (x + '_is_verified') in src_rec else None)
        for x in phone_key:
            if x in update_rec and update_rec[x]:
                if update_rec[x] not in phone_list:
                    phone_list.append(update_rec[x])
                    phone_country_code.append(update_rec[x+'_country_code'] if (x+'_country_code') in update_rec else None)
                    phone_valid.append(update_rec[x + '_is_valid'] if (x+'_is_valid') in update_rec else None)
                    phone_verified.append(update_rec[x + '_is_verified'] if (x+'_is_verified') in update_rec else None)
                elif update_rec[x] in phone_list:
                    index = phone_list.index(update_rec[x])
                    phone_valid[index] = phone_valid[index] or \
                                         (update_rec[x+'_is_valid'] if (x+'_is_valid') in update_rec else None)
                    phone_verified[index] = phone_verified[index] or \
                                            (update_rec[x+'_is_verified'] if (x+'_is_verified') in update_rec else None)

        while len(phone_list) < 4:
            phone_list.append(None)
            phone_country_code.append(None)
            phone_valid.append(None)
            phone_verified.append(None)
        self.is_valid_insertion_sort(phone_list,phone_country_code,phone_valid,phone_verified)
        for index in range(len(phone_key)):
            phone_dict[phone_key[index]] = phone_list[index]
            phone_dict[phone_key[index] + '_country_code'] = phone_country_code[index]
            phone_dict[phone_key[index] + '_is_valid'] = phone_valid[index]
            phone_dict[phone_key[index] + '_is_verified'] = phone_verified[index]

        return phone_dict

    @staticmethod
    def getEmailsDict(rec1, rec2=None):
        email_map = [
            ('email', 'email_validity'),
            ('email2', 'email2_validity'),
            ('email3', 'email3_validity'),
         ]
        valid_email_list = []
        bounced_email_list = []
        complaint_email_list = []
        dont_know_list = []
        emailDict = {}
        for x in email_map:
            email_field = x[0]
            email_validity_field = x[1]
            if rec2:
                if email_field in rec2 and type(rec2[email_field]) == str and rec2[email_field] != '':
                    if email_validity_field in rec2 and rec2[email_validity_field] == 'bounced':
                        if rec2[email_field].lower() not in bounced_email_list:
                            bounced_email_list.append(rec2[email_field].lower())
                    elif email_validity_field in rec2 and rec2[email_validity_field] == 'valid' and \
                            rec2[email_field].lower() not in valid_email_list:
                        valid_email_list.append(rec2[email_field].lower())
                    elif email_validity_field in rec2 and rec2[email_validity_field] == 'complaint' and \
                            rec2[email_field].lower() not in complaint_email_list:
                        complaint_email_list.append(rec2[email_field].lower())
                    elif rec2[email_field].lower() not in dont_know_list:
                        dont_know_list.append(rec2[email_field].lower())

        for x in email_map:
            email_field = x[0]
            email_validity_field = x[1]
            if rec1:
                if email_field in rec1 and type(rec1[email_field]) == str and rec1[email_field] != '':
                    if email_validity_field in rec1 and rec1[email_validity_field] == 'bounced':
                        if rec1[email_field].lower() not in bounced_email_list:
                            bounced_email_list.append(rec1[email_field].lower())
                    elif email_validity_field in rec1 and rec1[email_validity_field] == 'valid' and \
                            rec1[email_field].lower() not in valid_email_list:
                        valid_email_list.append(rec1[email_field].lower())
                    elif email_validity_field in rec1 and rec1[email_validity_field] == 'complaint' and \
                            rec1[email_field].lower() not in complaint_email_list:
                        complaint_email_list.append(rec1[email_field].lower())
                    elif rec1[email_field].lower() not in dont_know_list:
                        dont_know_list.append(rec1[email_field].lower())

        # removing emails from valid list, if they are present in bounced list or complaint list.
        valid_email_list = [x for x in valid_email_list if x not in bounced_email_list and x not in complaint_email_list ]
        # removing emails from don't know list if it is present in valid list or complaint list or bounced list
        dont_know_list = [x for x in dont_know_list if x not in bounced_email_list and x not in complaint_email_list and x not in valid_email_list]


        ordered_email_list = []
        # add tuples (email_id, is_bounced)
        for email in valid_email_list:
            ordered_email_list.append((email, 'valid'))
        for email in dont_know_list:
            ordered_email_list.append((email, None))
        for email in bounced_email_list:
            ordered_email_list.append((email, 'bounced'))
        for email in complaint_email_list:
            ordered_email_list.append((email, 'complaint'))
        while len(ordered_email_list) < len(email_map):
            ordered_email_list.append((None, None))

        for index in range(len(email_map)):
            email_field = email_map[index][0]
            email_validity_field = email_map[index][1]
            emailDict[email_field] = ordered_email_list[index][0]
            emailDict[email_validity_field] = ordered_email_list[index][1]
        return emailDict

    def getCountryDict(self, vals):
        country_dit = {}
        country_keys = ['country', 'country2', 'work_country', 'nationality']
        for x in country_keys:
            country_id = self.getCountryCode(vals[x])
            if country_id:
                country_dit[x+'_id'] = country_id
        return country_dit

    def getStateDict(self, vals):
        state_dit = {}
        state_keys = ['state', 'state2', 'work_state']
        for x in state_keys:
            state_id = self.getStateCode(vals[x])
            if state_id:
                state_dit[x+'_id'] = state_id
        return state_dit

    def getCenterInfo(self, center_id):
        center_dict = {}
        isha_center_id = self.ishaCenterModel.search([('id', '=', center_id)]) if center_id else self.ishaCenterModel
        if len(isha_center_id) > 0:
            center_dict['center_id'] = center_id
            center_dict['region_name'] = isha_center_id.region_id.name
            center_dict['is_valid_zip'] = True
            # commented center_manually_edited per issue 1036
            # center_dict['center_manually_edited'] = True
        return center_dict

    def getCenterId(self, city, zip, country, manually_edited):
        prefixed_search_country_dict = {'GB':2, 'FR':5, 'IE':3, 'NL':4, 'GR':3}
        center_dict = {}
        pincode_center_id = self.pincodeModel
        if zip and (' ' in zip or '-' in zip):
            zip = zip.replace(' ','')
            zip = zip.replace('-','')
            center_dict['zip'] = zip
        if country == 'IN' or not country:
            pincode_center_id = self.pincodeModel.search([('name', '=', zip),('countryiso_2','=','IN')]) if zip else self.pincodeModel
            if len(pincode_center_id) == 0:
                pincode_center_id = self.countryModel.search([('code', '=', 'IN')])
        else:
            if zip and country in prefixed_search_country_dict:
                substring_search_length = prefixed_search_country_dict[country]
                pincode_substring = zip[0:substring_search_length]
                pincode_center_id = self.pincodeModel.search([('name', '=ilike', pincode_substring),('countryiso_2','=',country)]) if pincode_substring else self.pincodeModel
            if len(pincode_center_id) == 0:
                pincode_center_id = self.pincodeModel.search([('name', '=ilike', zip),('countryiso_2','=',country)]) if zip else self.pincodeModel
            if len(pincode_center_id) == 0:
                pincode_center_id = self.pincodeModel.search([('name', '=ilike', city),('countryiso_2','=',country)]) if city else self.pincodeModel
            if len(pincode_center_id) == 0:
                pincode_center_id = self.countryModel.search([('code', '=', country)]) if country else self.countryModel

        center_dict['is_valid_zip'] = None
        if not manually_edited and len(pincode_center_id) > 0 and len(pincode_center_id.center_id)>0:
            pincode_center_id = pincode_center_id[0]
            center_dict['center_id'] = pincode_center_id.center_id.id
            center_dict['region_name'] = pincode_center_id.center_id.region_id.name
            center_dict['is_valid_zip'] = True
        return center_dict

    def generteLowMatchPairs(self, high_match_list, update_rec):
        high_set = set(high_match_list)
        low_set = set(self.low_match) - high_set
        rel_set = set(self.rel_match) - high_set
        rel_set = rel_set - low_set

        if update_rec in low_set:
            low_set.remove(update_rec)
        if update_rec in high_set:
            high_set.remove(update_rec)
        if update_rec in rel_set:
            rel_set.remove(update_rec)

        for rec in high_set:
            rec_ex = self.lowMatchModel.sudo().search([('id1','=',rec['id']),('id2','=',update_rec['id'])])
            if len(rec_ex) == 0:
                match_level = 'high'
                # [ISH-1323] - No need high_sso logic
                # if rec['sso_id'] and update_rec['sso_id'] and rec['sso_id'] != update_rec['sso_id']:
                #     match_level = 'high_sso'
                self.lowMatchModel.sudo().create(
                    {'id1': rec['id'], 'guid1': rec['guid'], 'id2': update_rec['id'], 'guid2': update_rec['guid'],
                     'active': True, 'match_level': match_level})

        for rec in low_set:
            rec_ex = self.lowMatchModel.sudo().search([('id1','=',rec['id']),('id2','=',update_rec['id'])])
            if len(rec_ex) == 0:
                self.lowMatchModel.sudo().create({'id1':rec['id'],'guid1':rec['guid'],'id2':update_rec['id'],'guid2':update_rec['guid'], 'active':True, 'match_level': 'low'})

        for rec in rel_set:
            rec_ex = self.lowMatchModel.sudo().search([('id1','=',rec['id']),('id2','=',update_rec['id'])])
            if len(rec_ex) == 0:
                self.lowMatchModel.sudo().create({'id1':rec['id'],'guid1':rec['guid'],'id2':update_rec['id'],'guid2':update_rec['guid'], 'active':True, 'match_level': 'relatives'})


    def getContactMapRec(self, local_contact_id, system_id):
        rec_ex = self.mapModel.sudo().search(
            [('local_contact_id', '=', local_contact_id), ('system_id', '=', system_id)], limit=1)
        return rec_ex

    def generateContactMap(self, map_rec):
        if map_rec['local_contact_id'] == 'UI':
            map_rec['local_contact_id'] = str(map_rec['contact_id_fkey'])
        rec_ex = None
        if map_rec['local_contact_id']:
            rec_ex = self.getContactMapRec(map_rec['local_contact_id'], map_rec['system_id'])
        if not rec_ex or len(rec_ex) == 0:
            self.mapModel.sudo().create(map_rec)
        else:
            rec_ex.sudo().write(map_rec)

    def create_new_contact(self, src_rec):
        if 'low_auth_data' in src_rec:
            src_rec.pop('low_auth_data',None)

        if ('guid' not in src_rec) or ('guid' in src_rec and (src_rec['guid'] == null or not src_rec['guid'])):
            src_rec['guid'] = str(uuid.uuid4())

        src_rec.update(self.getPhonesDict({}, src_rec))

        # ISH-1161 give precedence to sso's email and mark it as valid
        if src_rec.get('sso_id', False):
            src_rec['email_validity'] = 'valid'

        # Whatsapp number being copied from phone if src_rec doesn't have the value
        if not src_rec['whatsapp_number']:
            src_rec['whatsapp_number'] = src_rec['phone']
            src_rec['whatsapp_country_code'] = src_rec['phone_country_code']

        map_dict = {'local_contact_id': src_rec['local_contact_id'], 'system_id': src_rec['system_id'], 'guid':src_rec['guid'],
                    'name':src_rec['name'],'prof_dr':src_rec['prof_dr'],'phone':src_rec['phone_src'],'phone2':src_rec['phone2_src'],
                    'whatsapp_number': src_rec['whatsapp_number'], 'whatsapp_country_code': src_rec['whatsapp_country_code'],
                    'email':src_rec['email'],'email2':src_rec['email2'],'street1':src_rec['street'],'street2':src_rec['street2'],
                    'city':src_rec['city'],'state':src_rec['state'],'country':src_rec['country'],'zip':src_rec['zip'],
                    'work_street1': src_rec['work_street1'], 'work_street2': src_rec['work_street2'],
                    'work_city': src_rec['work_city'], 'work_state': src_rec['work_state'],
                    'work_country': src_rec['work_country'], 'work_zip': src_rec['work_zip'],
                    'gender':src_rec['gender'],'occupation':src_rec['occupation'],
                    'dob':src_rec['dob'],'marital_status':src_rec['marital_status'],'nationality':src_rec['nationality'],
                    'deceased':src_rec['deceased'],'contact_type':src_rec['contact_type'],'companies':src_rec['companies'],
                    'aadhaar_no':src_rec['aadhaar_no'],'pan_no':src_rec['pan_no'],'passport_no':src_rec['passport_no'],
                    'dnd_phone':src_rec['dnd_phone'],'dnd_email':src_rec['dnd_email'],'dnd_postmail':src_rec['dnd_postmail'], 'sso_id': src_rec['sso_id']}

        if map_dict['local_contact_id'] is None:
            map_dict['local_contact_id'] = str(uuid.uuid4())
        if 'local_modified_date' in src_rec:
            map_dict['local_modified_date'] = src_rec['local_modified_date']
            src_rec.pop('local_modified_date', None)
        if 'id_proofs' in src_rec and src_rec['id_proofs']:
            map_dict['id_proofs'] = src_rec['id_proofs']
        src_rec.pop('local_contact_id', None)
        src_rec.pop('system_id',None)
        src_rec.pop('phone_src', None)
        src_rec.pop('phone2_src', None)
        src_rec.pop('phone3_src', None)
        src_rec.pop('phone4_src', None)
        src_rec.update(self.getCountryDict(src_rec))
        src_rec.update(self.getStateDict(src_rec))
        if 'center_id' not in src_rec or not src_rec['center_id']:
            src_rec.update(self.getCenterId(src_rec['city'],src_rec['zip'],src_rec['country'],False))
        elif 'center_id' in src_rec:
            src_rec.update(self.getCenterInfo(src_rec['center_id']))
        update_rec = self.contactModel.sudo().phase4Create(src_rec)
        # update_rec.is_zip_valid()
        # self.CR.commit()
        map_dict['contact_id_fkey'] = update_rec['id']

        self.generateContactMap(map_dict)
        return update_rec

    def mergeRecord(self, high_match_list, src_rec):
        update_rec = self.getRecentConact(high_match_list)

        # [ISH-1323] - No need high_sso logic
        #
        # # [ISH-581] Don't merge 2 recs with different sso_id's
        # if src_rec['sso_id'] and update_rec['sso_id'] and src_rec['sso_id'] != update_rec['sso_id']:
        #     created_rec = self.create_new_contact(src_rec)
        #     flag_dict = {'flag': True, 'rec': created_rec}
        #     self.generteLowMatchPairs(high_match_list, created_rec)
        #     # the term 'phase4' is used to indicate that a new record is created instead of merging into an existing one.
        #     # todo: refactor to change the term for better understanding.
        #     flag_dict['phase'] = 'phase4'
        #     return flag_dict

        if ('low_auth_data' in src_rec and src_rec['low_auth_data']) \
                or ('local_modified_date' in src_rec and src_rec['local_modified_date']
                    and str(update_rec.write_date) > src_rec['local_modified_date']) \
                or (src_rec['system_id'] == 8) or (src_rec['system_id'] == 46) or (src_rec['system_id'] == 61) \
                or (update_rec.influencer_type and src_rec['system_id'] != 45) \
                or ('submitted_date' in src_rec and src_rec['submitted_date']
                    and str(update_rec.write_date) > src_rec['submitted_date'] and (src_rec['system_id'] == 52 or src_rec['system_id'] == 13)):
            return self.lowAuthMerge(high_match_list, src_rec)
        update_rec = high_match_list.pop()
        local_modified_at = src_rec.pop('local_modified_date', None) if 'local_modified_date' in src_rec else False

        # Get the longest of the names
        if type(src_rec['name']) == str and len(src_rec['name'].split()) >= 2:
            name = src_rec['name']
        else:
            name = self.getLongerString(update_rec['name'], src_rec['name'])

        # Phone
        phone_dict = self.getPhonesDict(update_rec,src_rec)
        # Email
        # ISH-1161 give precedence to sso's email and mark it as valid
        if src_rec.get('sso_id', False):
            src_rec['email_validity'] = 'valid'
        email_dict = self.getEmailsDict(update_rec,src_rec)

        #address
        address_key = ['street','street2','city','state','zip','country']
        address_key_2 = ['street2_1','street2_2','city2','state2','zip2','country2']
        address1 = []
        address2 = []
        is_new_zip_valid = True if (len( self.pincodeModel.sudo().search([('name', '=', src_rec['zip'])]) ) == 1) \
                                   or (src_rec['country'] and src_rec['country'] != 'IN') else False
        if is_new_zip_valid:
            # If the new zip is valid and new address1 and old address1 match take the longest of the two
            # Set address2 as new address2 if the zip2 is not None
            if src_rec['zip'] == update_rec['zip'] and src_rec['street'] and update_rec['street'] and fuzz.token_set_ratio(src_rec['street'].lower(), update_rec['street'].lower()) >= 60:
                for x in address_key:
                    address1.append(self.getLongerString(src_rec[x], update_rec[x]))
                if src_rec['zip2']:
                    for x in address_key_2:
                        address2.append(src_rec[x])
                else:
                    for x in address_key_2:
                        address2.append(update_rec[x])
            # Else if both zip matches take new address1 as address1 if street info is present else take old address1
            # If new address2 has zip take that as address2 else take old address2
            elif src_rec['zip'] == update_rec['zip']:
                if src_rec['street']:
                    for x in address_key:
                        address1.append(src_rec[x])
                else:
                    for x in address_key:
                        address1.append(update_rec[x])
                if src_rec['zip2']:
                    for x in address_key_2:
                        address2.append(src_rec[x])
                else:
                    for x in address_key_2:
                        address2.append(update_rec[x])
            else:
                # If the new zip is valid copy address1 to address2 and copy new address1 as address1
                for x in address_key:
                    address1.append(src_rec[x])
                    address2.append(update_rec[x])
        else:
            # If the new zip is invalid just copy new address1 as address2
            for x in address_key:
                address2.append(src_rec[x])
                address1.append(update_rec[x])

        if src_rec['system_id'] == 7: # DMS low trust value for gender
            gender = src_rec['gender'] if (not update_rec['gender'] and src_rec['gender'] is not None) else update_rec['gender']
        else:
            gender = src_rec['gender'] if src_rec['gender'] is not None else update_rec['gender']

        occupation = src_rec['occupation'] if src_rec['occupation'] is not None else update_rec['occupation']
        dob = src_rec['dob'] if src_rec['dob'] is not None else update_rec['dob']
        marital_status = src_rec['marital_status'] if src_rec['marital_status'] is not None else update_rec['marital_status']
        nationality = src_rec['nationality'] if src_rec['nationality'] is not None else update_rec['nationality']
        deceased = src_rec['deceased'] if src_rec['deceased'] is not None else update_rec['deceased']
        contact_type = src_rec['contact_type'] if src_rec['contact_type'] is not None else update_rec['contact_type']
        companies = src_rec['companies'] if src_rec['companies'] is not None else update_rec['companies']
        aadhaar_no = src_rec['aadhaar_no'] if src_rec['aadhaar_no'] is not None else update_rec['aadhaar_no']
        pan_no = src_rec['pan_no'] if src_rec['pan_no'] is not None else update_rec['pan_no']
        passport_no = src_rec['passport_no'] if src_rec['passport_no'] is not None else update_rec['passport_no']
        dnd_phone = src_rec['dnd_phone'] if src_rec['dnd_email'] is not None else update_rec['dnd_phone']
        dnd_email = src_rec['dnd_email'] if src_rec['dnd_email'] is not None else update_rec['dnd_email']
        dnd_postmail = src_rec['dnd_postmail'] if src_rec['dnd_postmail'] is not None else update_rec['dnd_postmail']
        whatsapp_number = src_rec['whatsapp_number'] if src_rec['whatsapp_number'] is not None else update_rec['whatsapp_number']
        whatsapp_country_code = src_rec['whatsapp_country_code'] if src_rec['whatsapp_country_code'] is not None else update_rec['whatsapp_country_code']

        # Whatsapp number being copied from phone if both src_rec and update_rec dont have the value
        if not whatsapp_number:
            whatsapp_number = phone_dict['phone']
            whatsapp_country_code = phone_dict['phone_country_code']

        sso_id = src_rec['sso_id'] if 'sso_id' in src_rec else None
        update_dict = {'name': name,'display_name':name,
                       'whatsapp_number': whatsapp_number,
                       'whatsapp_country_code': whatsapp_country_code,
                       'street': address1[0],'street2':address1[1],'city':address1[2],'state':address1[3], 'zip':address1[4],'country':address1[5],
                       'street2_1': address2[0], 'street2_2': address2[1], 'city2': address2[2], 'state2': address2[3], 'zip2': address2[4], 'country2': address2[5],
                       'work_street1': src_rec['work_street1'],'work_street2': src_rec['work_street2'],'work_city': src_rec['work_city'],'work_state': src_rec['work_state'],'work_country': src_rec['work_country'],'work_zip': src_rec['work_zip'],
                       'gender': gender, 'occupation': occupation,
                       'dob': dob, 'marital_status': marital_status,'nationality': nationality,
                       'deceased': deceased, 'contact_type': contact_type, 'companies':companies,
                       'aadhaar_no': aadhaar_no, 'pan_no': pan_no, 'passport_no': passport_no,
                       'dnd_phone': dnd_phone, 'dnd_email': dnd_email,
                       'dnd_postmail': dnd_postmail, 'sso_id': sso_id
                       }

        # Computed Fields Override
        # Similar flow for Merge in override_merge.custom_update_value
        if 'last_txn_date' in src_rec:
            last_txn_date = self.set_last_txn_date(update_rec.last_txn_date, src_rec['last_txn_date'])
            if last_txn_date:
                update_dict['last_txn_date'] = last_txn_date
        if 'religious_last_txn_date' in src_rec:
            religious_last_txn_date = self.set_last_txn_date(update_rec.religious_last_txn_date, src_rec['religious_last_txn_date'])
            if religious_last_txn_date:
                update_dict['religious_last_txn_date'] = religious_last_txn_date
        if 'charitable_last_txn_date' in src_rec:
            charitable_last_txn_date = self.set_last_txn_date(update_rec.charitable_last_txn_date, src_rec['charitable_last_txn_date'])
            if charitable_last_txn_date:
                update_dict['charitable_last_txn_date'] = charitable_last_txn_date
        if 'iv_last_donated_date' in src_rec:
            iv_last_donated_date = self.set_last_txn_date(update_rec.iv_last_donated_date, src_rec['iv_last_donated_date'])
            if iv_last_donated_date:
                update_dict['iv_last_donated_date'] = iv_last_donated_date
        if 'is_annadhanam' in src_rec:
            update_dict.update({'is_annadhanam': update_rec['is_annadhanam'] or src_rec['is_annadhanam']})
        if 'is_caca_donor' in src_rec:
            update_dict.update({'is_caca_donor': update_rec['is_caca_donor'] or src_rec['is_caca_donor']})
        if 'has_religious_txn' in src_rec:
            update_dict.update({'has_religious_txn': update_rec['has_religious_txn'] or src_rec['has_religious_txn']})
        if 'has_charitable_txn' in src_rec:
            update_dict.update({'has_charitable_txn': update_rec['has_charitable_txn'] or src_rec['has_charitable_txn']})
        if 'influencer_type' in src_rec:
            update_dict.update({'influencer_type': src_rec['influencer_type'] or update_rec['influencer_type']})
        # Honor Additional fields override
        for x in ContactProcessor.getAdditionalFields():
            if x in src_rec and src_rec[x]:
                update_dict[x] = src_rec[x]

        update_dict.update(phone_dict)
        update_dict.update(email_dict)
        update_dict.update(self.getCountryDict(update_dict))
        update_dict.update(self.getStateDict(update_dict))

        # below will make sure not to update the center_id if its manually updated from UI.
        if src_rec['system_id'] != 31:
            update_dict.update(self.getCenterId(update_dict['city'], update_dict['zip'], update_dict['country'], update_rec['center_manually_edited']))

        # center should be updated for Athithi load
        if src_rec['system_id'] == 45:
            if 'center_id' in src_rec and src_rec['center_id']:
                update_dict.update(self.getCenterInfo(src_rec['center_id']))

        update_dict.update({'kafka_flow': True})
        flag = update_rec.sudo().write(update_dict)
        # self.CR.commit()
        map_dict = {'local_contact_id': src_rec['local_contact_id'], 'system_id': src_rec['system_id'],'contact_id_fkey':update_rec['id'],'guid':str(update_rec['guid']),
                    'name':src_rec['name'],'prof_dr':src_rec['prof_dr'],'phone':src_rec['phone_src'],'phone2':src_rec['phone2_src'],
                    'whatsapp_number': src_rec['whatsapp_number'],
                    'whatsapp_country_code': src_rec['whatsapp_country_code'],
                    'email':src_rec['email'],'email2':src_rec['email2'],'street1':src_rec['street'],'street2':src_rec['street2'],
                    'city':src_rec['city'],'state':src_rec['state'],'country':src_rec['country'],'zip':src_rec['zip'],
                    'work_street1': src_rec['work_street1'], 'work_street2': src_rec['work_street2'],
                    'work_city': src_rec['work_city'], 'work_state': src_rec['work_state'],
                    'work_country': src_rec['work_country'], 'work_zip': src_rec['work_zip'],
                    'gender':src_rec['gender'],'occupation':src_rec['occupation'],
                    'dob':src_rec['dob'],'marital_status':src_rec['marital_status'],'nationality':src_rec['nationality'],
                    'deceased':src_rec['deceased'],'contact_type':src_rec['contact_type'],'companies':src_rec['companies'],
                    'aadhaar_no':src_rec['aadhaar_no'],'pan_no':src_rec['pan_no'],'passport_no':src_rec['passport_no'],
                    'dnd_phone':src_rec['dnd_phone'],'dnd_email':src_rec['dnd_email'],'dnd_postmail':src_rec['dnd_postmail'], 'sso_id': sso_id}
        if local_modified_at:
            map_dict['local_modified_date'] = local_modified_at
        if 'id_proofs' in src_rec and src_rec['id_proofs']:
            map_dict['id_proofs'] = src_rec['id_proofs']
        self.generateContactMap(map_dict)
        # self.CR.commit()
        self.generteLowMatchPairs(high_match_list, update_rec)
        # self.CR.commit()
        return {'flag':flag, 'rec':update_rec}

    def set_last_txn_date(self, src_last_date, update_last_date):
        last_txn_date = None
        if src_last_date and update_last_date:
            if type(update_last_date) == str:
                last_txn_date = max(src_last_date, datetime.strptime(update_last_date, '%Y-%m-%d').date())
            elif type(update_last_date) == datetime:
                last_txn_date = max(src_last_date, update_last_date.date())
            elif type(update_last_date) == date:
                last_txn_date = max(src_last_date, update_last_date)
        elif update_last_date:
            last_txn_date = update_last_date
        return last_txn_date



    def lowAuthMerge(self, high_match_list, src_rec):
        src_rec.pop('low_auth_data', None)
        local_modified_date = False
        if 'local_modified_date' in src_rec:
            local_modified_date = src_rec['local_modified_date']
            src_rec.pop('local_modified_date', None)

        update_rec = self.getRecentConact(high_match_list)
        set_move_fields=['street','street2','city','state','zip','country',
                         'street2_1','street2_2','city2','state2','zip2','country2',
                         'work_street1', 'work_street2', 'work_city', 'work_state', 'work_zip', 'work_country'
                         ]
        update_dict={}
        for x in src_rec:
            try:
                if x not in set_move_fields and not update_rec[x] and src_rec[x] is not None:
                    update_dict[x] = src_rec[x]
            except:
                pass

        # Phone
        phone_dict = self.getPhonesDict(src_rec,update_rec)
        update_dict.update(phone_dict)
        # Whatsapp Number
        if not update_rec['whatsapp_number'] and not src_rec['whatsapp_number']:
            update_dict.update({'whatsapp_number':update_dict['phone'],'whatsapp_country_code':update_dict['phone_country_code']})

        # Email
        # ISH-1161 give precedence to sso's email and mark it as valid
        if src_rec.get('sso_id', False):
            src_rec['email_validity'] = 'valid'
        email_dict = self.getEmailsDict(src_rec,update_rec)
        update_dict.update(email_dict)

        # Last Txn Date override
        if 'last_txn_date' in src_rec:
            last_txn_date = self.set_last_txn_date(update_rec.last_txn_date, src_rec['last_txn_date'])
            if last_txn_date:
                update_dict['last_txn_date'] = last_txn_date
        if 'religious_last_txn_date' in src_rec:
            religious_last_txn_date = self.set_last_txn_date(update_rec.religious_last_txn_date, src_rec['religious_last_txn_date'])
            if religious_last_txn_date:
                update_dict['religious_last_txn_date'] = religious_last_txn_date
        if 'charitable_last_txn_date' in src_rec:
            charitable_last_txn_date = self.set_last_txn_date(update_rec.charitable_last_txn_date, src_rec['charitable_last_txn_date'])
            if charitable_last_txn_date:
                update_dict['charitable_last_txn_date'] = charitable_last_txn_date
        if 'iv_last_donated_date' in src_rec:
            iv_last_donated_date = self.set_last_txn_date(update_rec.iv_last_donated_date, src_rec['iv_last_donated_date'])
            if iv_last_donated_date:
                update_dict['iv_last_donated_date'] = iv_last_donated_date
        if 'has_religious_txn' in src_rec:
            update_dict.update({'has_religious_txn': update_rec['has_religious_txn'] or src_rec['has_religious_txn']})
        if 'has_charitable_txn' in src_rec:
            update_dict.update({'has_charitable_txn': update_rec['has_charitable_txn'] or src_rec['has_charitable_txn']})

        # Address
        if not update_rec['zip'] and src_rec['zip']:
            update_dict['street']=src_rec['street']
            update_dict['street2']=src_rec['street2']
            update_dict['city']=src_rec['city']
            update_dict['state']=src_rec['state']
            update_dict['state_id']=src_rec['state_id'] if 'state_id' in src_rec else self.getStateCode(src_rec['state'])
            update_dict['zip']=src_rec['zip']
            update_dict['country']=src_rec['country']
            update_dict['country_id']=src_rec['country_id'] if 'country_id' in src_rec else self.getCountryCode(src_rec['country'])
        elif not update_rec['zip2'] and update_rec['zip'] and src_rec['zip'] and (src_rec['zip'] != update_rec['zip']) and src_rec['street'] and update_rec['street'] and fuzz.token_set_ratio(src_rec['street'].lower(), update_rec['street'].lower()) < 60:
            update_dict['street2_1'] = src_rec['street']
            update_dict['street2_2'] = src_rec['street2']
            update_dict['city2'] = src_rec['city']
            update_dict['state2'] = src_rec['state']
            update_dict['state2_id'] = self.getStateCode(src_rec['state'])
            update_dict['zip2'] = src_rec['zip']
            update_dict['country2'] = src_rec['country']
            update_dict['country2_id'] = self.getCountryCode(src_rec['country'])
        if not update_rec['work_zip'] and src_rec['work_zip']:
            update_dict['work_street1'] = src_rec['work_street1']
            update_dict['work_street2'] = src_rec['work_street2']
            update_dict['work_city'] = src_rec['work_city']
            update_dict['work_state'] = src_rec['work_state']
            update_dict['work_state_id'] = self.getStateCode(src_rec['work_state_id'])
            update_dict['work_zip'] = src_rec['work_zip']
            update_dict['work_country'] = src_rec['work_country']
            update_dict['work_country_id'] = self.getCountryCode(src_rec['country'])
        if not update_rec['zip']:
            update_dict.update(self.getCenterId(src_rec['city'], src_rec['zip'], src_rec['country'], update_rec['center_manually_edited']))
        else:
            update_dict.update(self.getCenterId(update_rec['city'],update_rec['zip'], update_rec['country'], update_rec['center_manually_edited']))
        update_dict.pop('system_id',None)
        update_dict.update({'kafka_flow': True})
        if not update_rec.iv_email_manually_edited and 'iv_email' in src_rec and src_rec['iv_email']:
            update_dict['iv_email'] = src_rec['iv_email']
        flag = update_rec.sudo().write(update_dict)
        # self.CR.commit()
        map_dict = {'local_contact_id': src_rec['local_contact_id'], 'system_id': src_rec['system_id'],'contact_id_fkey':update_rec['id'],'guid':str(update_rec['guid']),
                    'name':src_rec['name'],'prof_dr':src_rec['prof_dr'],'phone':src_rec['phone_src'],'phone2':src_rec['phone2_src'],
                    'whatsapp_number': src_rec['whatsapp_number'],
                    'whatsapp_country_code': src_rec['whatsapp_country_code'],
                    'email':src_rec['email'],'email2':src_rec['email2'],'street1':src_rec['street'],'street2':src_rec['street2'],
                    'city':src_rec['city'],'state':src_rec['state'],'country':src_rec['country'],'zip':src_rec['zip'],
                    'work_street1': src_rec['work_street1'], 'work_street2': src_rec['work_street2'],
                    'work_city': src_rec['work_city'], 'work_state': src_rec['work_state'],
                    'work_country': src_rec['work_country'], 'work_zip': src_rec['work_zip'],
                    'gender':src_rec['gender'],'occupation':src_rec['occupation'],
                    'dob':src_rec['dob'],'marital_status':src_rec['marital_status'],'nationality':src_rec['nationality'],
                    'deceased':src_rec['deceased'],'contact_type':src_rec['contact_type'],'companies':src_rec['companies'],
                    'aadhaar_no':src_rec['aadhaar_no'],'pan_no':src_rec['pan_no'],'passport_no':src_rec['passport_no'],
                    'dnd_phone':src_rec['dnd_phone'],'dnd_email':src_rec['dnd_email'],'dnd_postmail':src_rec['dnd_postmail'],'sso_id':src_rec['sso_id']}
        if map_dict['local_contact_id'] is None:
            map_dict['local_contact_id'] = str(uuid.uuid4())
        if local_modified_date:
            map_dict['local_modified_date'] = local_modified_date
        if 'id_proofs' in src_rec and src_rec['id_proofs']:
            map_dict['id_proofs'] = src_rec['id_proofs']
        self.generateContactMap(map_dict)
        # self.CR.commit()
        self.generteLowMatchPairs(high_match_list, update_rec)
        # self.CR.commit()
        return {'flag':flag, 'rec':update_rec}
