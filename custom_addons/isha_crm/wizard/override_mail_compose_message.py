from odoo import _, api, fields, models


class MailComposeMessageInherit(models.TransientModel):
	_inherit = 'mail.compose.message'
	body = fields.Html('Contents', default='', sanitize_attributes=False)