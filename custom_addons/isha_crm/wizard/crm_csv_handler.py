import base64
import csv
import logging
import io
from odoo import fields, models, _
from odoo.tools import pycompat
_logger = logging.getLogger(__name__)

class CSVHandlerWizard(models.TransientModel):
    _name = 'crm.csv.handler'

    file = fields.Binary(string="File")

    data_output = fields.Text(string='Data output')

    def from_data(self, fields, rows):
        fp = io.BytesIO()
        writer = pycompat.csv_writer(fp, quoting=1)

        writer.writerow(fields)

        for data in rows:
            row = []
            for d in data:
                # Spreadsheet apps tend to detect formulas on leading =, + and -
                if isinstance(d, str) and d.startswith(('=', '-', '+')):
                    d = "'" + d

                row.append(pycompat.to_text(d))
            writer.writerow(row)
        return fp.getvalue()

    def _get_data_dump(self):
        export_data = [x for x in eval(self.data_output)]
        self.data_output_dump = base64.b64encode(self.from_data(['email','reason'], export_data))

    data_output_dump = fields.Binary(compute=_get_data_dump,store=False)


    def update_rd(self):
        sql = ''
        if self.file:
            ieo_reader = csv.DictReader(base64.b64decode(self.file).decode('utf-8').split('\n'))
            for row in ieo_reader:
                delivery_status = 'Delivered' if row['status'] == 'delivered' else row['status']
                sql += "update rudraksha_deeksha set consignment_no = \'%s\', delivery_status = \'%s\' where id = %s;" % (
                    row['consignmentno'],delivery_status,row['crefno']
                )

            _logger.info('Update rudraksha deeksha start')
            self.env.cr.execute(sql)
            self.env.cr.commit()
            _logger.info('Update rudraksha deeksha end')
            return self.env['partner.import.temp.wizard'].get_popup('Successfully updated the data.')

    def verified_dedup_handler(self):
        import io, datetime
        a = base64.b64decode(self.file).decode('utf-8')
        self.env.cr.execute("CREATE TEMP TABLE temp_dedup (email character varying)")
        self.env.cr.copy_from(io.StringIO(a),'temp_dedup',sep=',')
        self.env.cr.execute("""
            select email,'unverified' as reason from (
                select lower(trim(email)) as email from temp_dedup 
                except( 
                  select lower(email) from isha_email_verified_master where active = true
                  union
                  select lower(email) from mail_blacklist
                )
            ) as unverified
            UNION
            select email,'blacklisted' as reason from (
                 select lower(trim(email)) as email from temp_dedup 
                 intersect
                 select lower(email) from mail_blacklist where active = true
            ) as blacklisted
            """)
        emails=[]
        for x in self.env.cr.dictfetchall():
            emails.append([x['email'],x['reason']])
        self.data_output = emails
        link = """
                <h4>Deduped File output:</h4>
                <p> No of Unverified Records: %s<br/><br/>
                <a id="download_data_output" class="btn-lg btn-info" style="border-radius: 20px; border:0px; font-weight:bold;"
                    href="/web/content/?model=crm.csv.handler&id=%s&filename=%s&field=%s&download=true&name_file=%s" target="_blank">
                    <span class="fa fa-download"/>
                </a></p>
            """ % (len(emails),self.id,'dedup_'+str(self.id)+'_'+str(int(datetime.datetime.now().timestamp()))+'.csv','data_output_dump','dedup_'+str(self.id)+'_'+str(int(datetime.datetime.now().timestamp()))+'.csv')
        self.env.cr.commit()
        view = self.env.ref('isha_crm.sh_message_wizard_html')
        view_id = view and view.id or False
        context = dict(self._context or {})
        context['message'] = link
        return {
            'name': 'Success',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sh.message.wizard',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': context
        }