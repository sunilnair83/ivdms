from odoo import api, fields, models
from .. import GoldenContactAdv



class IshaMicroVolVerifyUsersWiazrd(models.TransientModel):
	_name = 'isha.microvol.verify.user.wizard'
	_description = 'Micro Vol Verify Users'

	high_match = fields.Many2many('res.partner', 'mv_hm_rel',column1='partner_id', column2='high_match',string='High Matches')
	low_match = fields.Many2many('res.partner', 'mv_lm_rel',column1='partner_id', column2='low_match',string='Low Matches')
	dst_partner_id = fields.Many2one('res.partner', string='Destination Contact')
	src_partner_id = fields.Many2one('res.partner', string='Source Contact')
	center_id = fields.Many2one('isha.center',string="Finalized Center")
	region_name = fields.Char(related='center_id.region_id.name',string="Finalized Region")
	template_id = fields.Many2one('mail.template', 'Send Email notification')
	body_html = fields.Html(related="template_id.body_html")
	@api.model
	def default_get(self, fields):
		res = super(IshaMicroVolVerifyUsersWiazrd, self).default_get(fields)
		active_ids = self.env.context.get('active_ids')
		golden_contact = GoldenContactAdv.GoldenContactAdv()
		golden_contact.setEnvValues(self.env)
		if self.env.context.get('active_model') == 'res.users' and active_ids:
			user = self.env['res.users'].browse(active_ids)
			match = golden_contact.getPhoneMatch(user.phone) if user.phone else self.env['res.partner']
			match1 = golden_contact.getPhoneMatch(user.phone2) if user.phone2 else self.env['res.partner']
			match2 = golden_contact.getEmailMatch(user.email) if user.email else self.env['res.partner']
			match3 = golden_contact.getEmailMatch(user.email2) if user.email2 else self.env['res.partner']
			resultant = match | match1 | match2 | match3
			high_match_recs = []
			low_match_recs = []
			golden_contact.nameMatchList(resultant,user.name,high_match_recs,low_match_recs)

			res['high_match'] = [(6,0,[x.id for x in high_match_recs])]
			res['low_match'] = [(6,0,[x.id for x in low_match_recs])]
			res['dst_partner_id']=user[0].partner_id.id
		return res

	def action_merge(self):
		merge = self.env['base.partner.merge.automatic.wizard'].sudo()
		merge._merge([self.src_partner_id.id,self.dst_partner_id.id],self.dst_partner_id)
		update_dict = {
			'center_id': self.center_id,
			'region_name': self.region_name
		}
		if self.dst_partner_id.is_meditator and self.dst_partner_id.portal_verified:
			update_dict.update({'forum_status': 'verified_manually'})
		if not self.dst_partner_id.is_meditator and self.dst_partner_id.portal_verified:
			update_dict.update({'forum_status': 'unverified'})
		self.dst_partner_id.sudo().write(update_dict)
		if self.template_id:
			self.template_id.send_mail(self.dst_partner_id.id, force_send=True)
