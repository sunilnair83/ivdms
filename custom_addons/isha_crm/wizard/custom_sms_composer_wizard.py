import logging
import traceback

from odoo import api, fields, models

_logger = logging.getLogger(__name__)


class IshaSendSMSWiazrd(models.TransientModel):
	_name = 'isha.sms.composer.wizard'
	_description = 'Wizard to send SMS using SMS Country API'

	follow_up_sms_template_id = fields.Many2one('sms.template', string='SMS Template')
	follow_up_text = fields.Text(string='SMS Text')
	selected_count = fields.Integer(string='Valid Contacts')
	invalid_count = fields.Integer(string='Invalid Contacts')
	dnd_contacts = fields.Many2many('res.partner',string='DND contacts')


	def get_valid_sms_set(self):
		partner_recs = []
		if self._context.get('active_model') == 'res.partner':
			partner_recs = self.env['res.partner'].browse(self._context.get('active_ids'))
		elif self._context.get('active_model'):
			partner_recs = self.env[self._context.get('active_model')].sudo().browse(self._context.get('active_ids'))
			if self._context.get('res_partner_mapping'):
				partner_recs = partner_recs.mapped(self._context.get('res_partner_mapping'))
		# validity Check
		#suri: changes
		#
		# if self.env.user.has_group('isha_crm.group_sangam'):
		valid_partners = [x for x in partner_recs if x.phone and x.deceased is False and x.dnd_sms is False and not x.no_marketing]

		return set(valid_partners)

	def get_dnd_contacts(self):
		if self._context.get('active_model') == 'res.partner':
			partner_recs = self.env['res.partner'].browse(self._context.get('active_ids'))
		elif self._context.get('active_model'):
			partner_recs = self.env[self._context.get('active_model')].sudo().browse(self._context.get('active_ids'))
			if self._context.get('res_partner_mapping'):
				partner_recs = partner_recs.mapped(self._context.get('res_partner_mapping'))
		return partner_recs.filtered(lambda x: x.dnd_sms is True or x.phone is False).ids

	@api.model
	def default_get(self, fields):
		res = super(IshaSendSMSWiazrd, self).default_get(fields)

		res['selected_count'] = len(self.get_valid_sms_set())
		res['dnd_contacts'] = [(6,0,self.get_dnd_contacts())]
		return res

	def custom_send_sms_mass_now(self):
		for rec in self.get_valid_sms_set():
			phone = rec['phone']
			msg = self.follow_up_sms_template_id.body
			try:
				flag = self.env['sms.api']._send_smcountry_sms({
					'number':phone,
					'message':msg
				})
			except Exception as ex:
				tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
				_logger.info('SMS send Failed For '+ phone+' \n'+tb_ex)

		_logger.info('SMS sent successfully')
		return


	@api.onchange('follow_up_sms_template_id')
	def _onchange_template_id(self):
		if self.follow_up_sms_template_id:
			self['follow_up_text'] = self.follow_up_sms_template_id.body
		else:
			self['follow_up_text'] = None
		return



