from odoo import _, api, fields, models


class ContactMerge(models.TransientModel):
	_name = 'isha_crm.partner.merge.automatic.wizard'
	_inherit = 'base.partner.merge.automatic.wizard'

	# redirect_action_ext_id = fields.Char(string='External ID of the Redirect action')

	def action_merge(self):
		super(ContactMerge, self).action_merge()

		# action = self.env.ref(self.redirect_action_ext_id).read()[0]
		# return action
		# return {
		# 	'type': 'ir.actions.act_window',
		# 	'res_model': self._name,
		# 	'res_id': self.id,
		# 	'view_mode': 'tree',
		# 	'target': 'new',
		# }
