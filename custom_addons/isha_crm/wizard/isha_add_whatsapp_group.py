from ast import literal_eval

from odoo import api, fields, models
import logging, sys, traceback


_logger = logging.getLogger('ResPartner')
_logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
_logger.addHandler(ch)


class IshaSendSMSWiazrd(models.TransientModel):
	_name = 'isha.whats.app.group.wizard'
	_description = 'Wizard to add WhatsApp Group'


	group_name = fields.Char(string='Group Name')
	selected_count = fields.Integer(string='Selected Contacts')

	@api.model
	def default_get(self, fields):
		res = super(IshaSendSMSWiazrd, self).default_get(fields)

		res['selected_count'] = len(self._context.get('active_ids'))
		return res

	def assign_group(self):
		recs = self.env[self._context.get('active_model')].browse(self._context.get('active_ids'))
		recs.write({ 'whats_app_group' : self.group_name })