from odoo import api, fields, models


class IshaFlagInfluencersWiazrd(models.TransientModel):
	_name = 'isha.flag.influencer.wizard'
	_description = 'Flagging Influencers'

	selected_count = fields.Integer(string='Selected count')
	partner_ids = fields.Many2many('res.partner', string='Contacts')

	@api.model
	def default_get(self, fields):
		res = super(IshaFlagInfluencersWiazrd, self).default_get(fields)
		active_ids = self.env.context.get('active_ids')
		res['selected_count'] = len(active_ids)
		res_partner_ids = []
		if self.env.context.get('active_model') == 'low.match.pairs' and active_ids:
			recs = self.env['low.match.pairs'].browse(active_ids)
			for rec in recs:
				if rec.id1.id not in res_partner_ids:
					res_partner_ids.append(rec.id1.id)
				if rec.id2.id not in res_partner_ids:
					res_partner_ids.append(rec.id2.id)
			res['selected_count'] = len(res_partner_ids)

		res['partner_ids'] = [(6, 0, res_partner_ids)]
		return res

	def action_flag_influencers(self):
		active_ids = self.env.context.get('active_ids')
		flag=False
		if self.env.context.get('active_model') == 'low.match.pairs' and active_ids:
			recs = self.env['low.match.pairs'].browse(active_ids)
			res_partner_ids = []
			for rec in recs:
				if rec.id1.id not in res_partner_ids:
					res_partner_ids.append(rec.id1.id)
				if rec.id2.id not in res_partner_ids:
					res_partner_ids.append(rec.id2.id)
			flag = self.env['res.partner'].browse(res_partner_ids).write({'influencer_type': 'influencer'})
		return flag