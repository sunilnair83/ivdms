from odoo import models, fields, api
from odoo.exceptions import UserError
import datetime,logging

_logger = logging.getLogger(__name__)


class MultiEditWizard(models.TransientModel):
    _name = 'isha.crm.multi.edit.wizard'
    _description = 'Custom Multi Edit Wizard'

    sub_center = fields.Char(string='Sub-center')
    micro_center = fields.Char(string='Micro center')
    nurturing_volunteer = fields.Many2one(string='Nurturing Volunteer', comodel_name='res.partner')
    nurturing_remarks = fields.Text(string='Remarks')
    selected_count = fields.Integer(string='Selected count')
    selection_mode = fields.Selection([('selected_contact','Only Selected Contacts'),('selected_filter', 'All Filtered Contacts')],
                                      string='Selection Mode')


    @api.onchange('selection_mode')
    def get_ids_for_selection(self):
        for x in self:
            if self.selection_mode:
                x.selected_count = len(x.get_valid_partners_set())

    def get_valid_partners_set(self):
        partner_recs = []
        if not self.selection_mode or self.selection_mode == 'selected_contact':
            base_ids = ','.join([str(x) for x in self._context.get('active_ids')])
            where_clause_params = []
        else:
            table_name = self.env[self.env.context['active_model']]._table
            from_clause, where_clause, where_clause_params = self.env[self.env.context['active_model']].get_query_params(self.env.context['active_domain'])
            where_str = where_clause and (" WHERE %s" % where_clause) or ''
            base_query = 'SELECT "%s".id FROM %s %s'
            base_ids = base_query % (table_name,from_clause, where_str)

        center_ids = ','.join([str(x) for x in self.env.user.centers.ids])
        charitable = self.user_has_groups('isha_crm.group_all_contacts_grp,isha_crm.group_ieo_grp,isha_crm.group_online_satsang_grp,isha_crm.group_online_pournami_grp,isha_crm.group_mandala_app_grp,isha_crm.group_sadhguru_exclusive_grp,isha_crm.group_webinars_grp,isha_crm.group_sannidhi_grp,isha_crm.group_reports_grp,isha_crm.group_forum_grp,isha_crm.group_santosha_regional_admin,isha_crm.group_graceofyoga,isha_crm.group_covid_support,isha_crm.group_rfr_youth,isha_crm.group_global_search')
        religious = self.user_has_groups('isha_crm.group_religious_santosha,isha_crm.group_all_contacts_religious,isha_crm.group_vanashree_grp,isha_crm.group_rudraksha_deeksha,isha_crm.group_global_search_religious')
        charitable_clause = ' (rp.has_charitable_txn = true) '
        religious_clause = ' (rp.has_religious_txn = true) '

        suffix_array = []
        if charitable:
            suffix_array.append(charitable_clause)
        if religious:
            suffix_array.append(religious_clause)

        suffix_clause = ''
        if suffix_array:
            suffix_clause = ' and (' + ' or '.join(suffix_array) + ')'

        if self._context.get('active_model') == 'res.partner':
            base_query = '''SELECT rp.id,rp.deceased,rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,
                            rp.starmark_category_id FROM res_partner rp where rp.id in (%s) 
                            and (rp.no_marketing is false or rp.no_marketing is null) 
                            and rp.center_id in (%s) %s ''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        if self._context.get('active_model') == 'program.lead':
            base_query = '''SELECT rp.id,rp.deceased,rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,
                            rp.starmark_category_id FROM res_partner rp, program_lead pl where rp.id = pl.contact_id_fkey and pl.id in (%s) 
                            and (rp.no_marketing is false or rp.no_marketing is null) 
                            and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()

        valid_partners = set()
        for x in partner_recs:
            if x['deceased'] is not True and x['starmark_category_id'] is None:
                if (not x['influencer_type']) and (x['is_caca_donor'] is not True or (
                        x['is_caca_donor'] is True and str(x['last_txn_date']) != '1900-01-01')):
                    valid_partners.add(x['id'])

        return list(valid_partners)

    def assign_values(self):
        start = datetime.datetime.now()
        update_params = []
        if self.sub_center:
            update_params.append('sub_center = \'%s\'' % self.sub_center)
        if self.micro_center:
            update_params.append('micro_center = \'%s\'' % self.micro_center)
        if self.nurturing_volunteer:
            update_params.append('nurturing_volunteer = %s' % self.nurturing_volunteer.id)
        if self.nurturing_remarks:
            update_params.append('nurturing_remarks = \'%s\'' % self.nurturing_remarks)
        if update_params:
            self.env.cr.execute("""
                update res_partner set %s where id in (%s)
            """ % (', '.join(update_params),','.join([str(x) for x in self.get_valid_partners_set()])))
            self.env.cr.commit()
            _logger.info("PERF: Time Taken for "+str(self.selected_count)+" update "+str(datetime.datetime.now()-start))
            return self.env['sh.message.wizard'].get_popup("Successfully Updated "+str(self.selected_count)+" contacts. "
                                                           "Time taken "+str(datetime.datetime.now()-start))
        else:
            raise UserError('Atleast one value is expected to update the contacts')
