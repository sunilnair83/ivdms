import NameMatchEngine

NO_MATCH = 0
LOW_MATCH = 1
HIGH_MATCH = 2
EXACT_MATCH = 4
NA = -1



rec_list = [['R S Ranith','R S Kumar',NO_MATCH],
            ['S.sekar','S.SANTHA KUMARI',NO_MATCH],
            ["Babitha P B","BABITHA P",HIGH_MATCH],
            ["Rashika C P","Rashika C",HIGH_MATCH],
            ["P Rashika", "C P Rashika",HIGH_MATCH],
            ["C Rashika", "C P Rashika",HIGH_MATCH],
            ["C P Rashika","P Rashika",HIGH_MATCH],
            ["C P Rashika","C Rashika",HIGH_MATCH],
            ["Chandragandhi Selvaraj", "Chandra Selvaraj",HIGH_MATCH],
            ["Muthu S S", "S.S. MUTHU",HIGH_MATCH],
            ["S.S. MUTHU","Muthu S S",HIGH_MATCH],
            ["Bharath sai reddy Nagappagari","BHARATH SAI REDDY", HIGH_MATCH],
            ["Darshan Bhagwat", "DARSHAN S B",HIGH_MATCH],
            ["Latha N", "LATHA NAGARAJAN N",HIGH_MATCH],
            ["S Sarathi", "Sarathi Sundharam",HIGH_MATCH],
            ["Nishi N", "NISHI NAVALAN", HIGH_MATCH],
            ['Rajiv Menon','CA. Rajiv menon',EXACT_MATCH],
            ["PRUTHVIKUMAR CHOUHAN","Pruthvikumar",HIGH_MATCH],
            ["PRUTHVIKUMAR CHOUHAN","Pruthvikumar Chauhan",HIGH_MATCH],
            ["Pruthvikumar Pruthvikumar","PRUTHVIKUMAR CHOUHAN",HIGH_MATCH],
            ["PRUTHVIKUMAR CHOUHAN","Pruthvikumar Pruthvikumar",HIGH_MATCH],
            ["premkumar a", "prem kumar a", EXACT_MATCH],
            ["gopala krishnan", "gopalakrishnan", EXACT_MATCH],
            ["v k chaitanya", "v krishna chaitanya", HIGH_MATCH],
            ["A Balaji", "Balaji", HIGH_MATCH],
            ["K Lakshmipathi Balaji", "K L Balaji", HIGH_MATCH],
            ["K Lakshmipathi Balaji", "K Laxmipathi balaji", HIGH_MATCH],
            ["K Lakshmipathi Balaji", "Laxmipathi balaji", HIGH_MATCH],
            ["K Lakshmipathi Balaji", "Laxmipathi balaji", HIGH_MATCH],
            ["Amutha T", "Amudha T", HIGH_MATCH],
            ["K Lakshmipathi Balaji", "balaji", LOW_MATCH],
            ["gopala krishnan", "gopalakrisnan", HIGH_MATCH],
            ["A Balaji", "Balajee", HIGH_MATCH],
            ["k gopala krishnan", "gopalkrishnan", HIGH_MATCH],
            ["k gopala krishnan", "K L gopalkrishnan", LOW_MATCH],
            ["devika mathivanan", "devika", HIGH_MATCH],
            ["K Lakshmipathi Balaji", "K balaji", LOW_MATCH],
            ["K Lakshmipathi Balaji", "P balaji", NO_MATCH],
            ["prem kumar", "Preamkumar", HIGH_MATCH],
            ["K R Kannan", "K Kannan", LOW_MATCH],
            ["K Vinitha", "B Anitha", NO_MATCH],
            ["prem a kumar", "pream kumara", HIGH_MATCH],
            ["prem a kumar", "pream kumar b", NO_MATCH],
            ["yu chen", "chen yu", HIGH_MATCH],
            ["Prof Lokesh Jindal", "Lokesh Jindal", EXACT_MATCH],
            ["Lokesh Jindal", "Lokesh", HIGH_MATCH],
            ["prof Lokesh Jindal", "Lokesh jindall", HIGH_MATCH],
            ["K Lakshmipathi Balaji", "Lakshmipathi", HIGH_MATCH],
            ["K Lakshmipathi Balaji", "M Lakshmipathi", NO_MATCH],
            ["K Lakshmipathi Balaji", "L Balaji", LOW_MATCH],
            ["Mahesh Kumar Singh", "Mahesh Singh", HIGH_MATCH],
            ["Bhadrakumari Veera", "Veera Bhadra Kumari", HIGH_MATCH],
            ["Uma Balakrishnan", "Shikha Balakrishnan", NO_MATCH],
            ["Raja S", "Soundararajan R", NO_MATCH],
            ["ravi krishnan", "raghu krishnan", LOW_MATCH],
            ["Bipin Panday", "bipin kumar panday", HIGH_MATCH],
            ["UmaBalakrishnan", "ShikhaBalakrishnan", NO_MATCH],
            ["Raja S", "Ramarajan R", NO_MATCH],
            ["Raja S", "Ramarajan S", NO_MATCH],
            ["gopala", "gopalakrishnan", NO_MATCH],
            ["raja vadivelu", "ramarajan vadivelu", NO_MATCH],
            ["R raja vadivelu", "R ramarajan vadivelu", LOW_MATCH],
            ["ramchandra kumar", "ram chandarkumar", HIGH_MATCH],
            ["ramchandra kumar", "ram chandar kumar", HIGH_MATCH],
            ["S raja vadivelu", "R samar vadivelu", NO_MATCH],
            ["K Lakshmipathi Balaji", "K Laxmipathi Balajee", LOW_MATCH],
            ["g jabbar singh", "j gabbar singh", NO_MATCH],
            ["van veen pearson", "pearson vanveen", LOW_MATCH],
            ["J E Smith", "John Earl Smith", HIGH_MATCH],
            ["SOUMITA JHOSH", "SOUMITA GHOSH", HIGH_MATCH],
            ["SUBRAMANIAN BALACHANDAR", "Bala Chander S" , HIGH_MATCH],
            ["Raj S", "Ramarajan Sharma", NO_MATCH],
            ["Raja S", "Ramarajan Sharma", NO_MATCH],
            ["Raju S", "Ramarajan Sharma", NO_MATCH],
            ["ramchandra kumar", "chandrakumar", NO_MATCH],
            ["ramchandra kumar", "chandra kumar", NO_MATCH],
            ["SUBRAMANIAN M BALACHANDAR ", "Mahesh BalaChander S", HIGH_MATCH],
            ["M BALACHANDAR ", "Mahesh Bala Chander", HIGH_MATCH],
            ["SUBRAMANIAN M BALACHANDAR ", "Mahesh Bala Chander S", HIGH_MATCH],
            ["shridharan", "shruthi shridharan", NO_MATCH],
            ["santosh kumar", "saravana kumar", LOW_MATCH],
            ["raghuram munikrishnan", "shreya raghuram", NO_MATCH],
            ["pankaj kumar sundar", "sundar k ", LOW_MATCH],
            ["vaddi krishna chaitanya", "krishna c", LOW_MATCH],
            ["vaddi krishna chaitanya", "vaddi k chaitanya", HIGH_MATCH],
            ["K L Balaji", "Lakshmipathi balaji", LOW_MATCH],
            ["Rajendra S Kudchi", "Pratik R Kudchi", LOW_MATCH],
            ["Nandhagopal", "nandagopalan M", LOW_MATCH],
            ["Jograj Hansraj Parekh", "Chandra Parekh H", LOW_MATCH],
            ["Mohan Das S", "Dasa Sivarama Prasad Rao", LOW_MATCH],
            ["Selvizhi Subramanian", "Manimegalai Subramanian", LOW_MATCH],
            ["Nandlal G Sharma ", "Dinesh Sharma N", LOW_MATCH],
            ["Mayur Mahale ", "Deepika M Mahale ", LOW_MATCH],
            ["Manva Mayur Mahale", "Deepika M Mahale ", LOW_MATCH],
            ["Manva Mayur Mahale", "Mayur Mahale ", NO_MATCH],
            ["Naidu Chiriki", "Nari Chiriki ", NO_MATCH],
            ["Lakshmi Narayanan R", "Mala L Narayanan", LOW_MATCH],
            ["utsav doshi", "NAMEETA U DOSHI", LOW_MATCH],
            ["Arati Phadke", "Arya Phadke", NO_MATCH],
            ["krishna c vaddi", "v krishna chaitanya", HIGH_MATCH]]


low_match = []
high_match = []
no_match = []
exact_match = []
na = []


matchEngine = NameMatchEngine.NameMatchEngine()
for record in rec_list:
    rec = matchEngine.compareNamesWithVariations(record[0], record[1])
    if rec == NO_MATCH:
        no_match.append(record)
    elif rec == LOW_MATCH:
        low_match.append(record)
    elif rec == HIGH_MATCH:
        high_match.append(record)
    elif rec == EXACT_MATCH:
        exact_match.append(record)
    elif rec == NA:
        na.append(record)

print("___________EXACT__________")
for x in exact_match:
    if x[-1] != EXACT_MATCH:
        print(x,' <<<<<<<<<<<')
    else:
        print(x)
print("___________HIGH__________")
for x in high_match:
    if x[-1] != HIGH_MATCH:
        print(x, ' <<<<<<<<<<<')
    else:
        print(x)
print("___________LOW__________")
for x in low_match:
    if x[-1] != LOW_MATCH:
        print(x,' <<<<<<<<<<<')
    else:
        print(x)
print("___________NO__________")
for x in no_match:
    if x[-1] != NO_MATCH:
        print(x,' <<<<<<<<<<<')
    else:
        print(x)
print("___________NA__________")
for x in na:
    print(x)
