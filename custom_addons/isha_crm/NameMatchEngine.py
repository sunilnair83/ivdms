import math

import Levenshtein as lev
from fuzzywuzzy import fuzz

from . import ContactProcessor
from . import NameInitialsComp

null = None
NO_MATCH = 0
LOW_MATCH = 1
HIGH_MATCH = 2
EXACT_MATCH = 4
NA = -1

TITLES = ["prof", "mr", "mrs", "smt", "dr", "ms", "esq"]

class NameMatchEngine:
    def compareNamesWithVariations(self, s, t):
        vs = self.getVariations(s)
        vt = self.getVariations(t)

        res = NO_MATCH
        for si in vs:
            for ti in vt:
                res = max(res, self.compareNames1(si, ti))
        return res

    def getVariations(self, str1):

        str1 = str1.replace('. ', ' ')
        str1 = str1.replace(' .', ' ')
        str1 = str1.replace('.', ' ')

        # str1 = self.removeRepeatedTokensWithOrder(str1)
        str_tokens = str1.lower().split()
        initials = []
        words = []
        for x in str_tokens:
            if len(x) == 1:
                initials.append(x)
            else:
                words.append(x)
        variation = []
        for i in range(len(words) - 1):
            variation.append(self.getVariation(words, initials, i))
        variation.append(str1)

        return variation

    def getVariation(self, tokens, initials, i):
        if i > len(tokens) - 1:
            return None
        temp = tokens[i]
        sb = ''
        for n in range(len(tokens)):
            if n == i:
                continue
            if n == i + 1:
                sb += temp + tokens[n] + " "
            else:
                sb += tokens[n] + " "
        for i in initials:
            sb += i + " "

        return sb.strip()

    def compareNames1(self, name1, name2):
        name1 = self.cleanName(name1).strip()
        name2 = self.cleanName(name2).strip()

        name1 = self.removeTitle(name1)
        name2 = self.removeTitle(name2)

        if name1 == None or name2 == None:
            return NO_MATCH
        if self.isNameExactMatch(name1, name2):
            return EXACT_MATCH
        if self.isNameTokenWiseExact(name1, name2):
            return HIGH_MATCH
        if self.isMissingTokenCase(name1,name2):
            return HIGH_MATCH
        n1 = self.removeInitials(name1)
        n2 = self.removeInitials(name2)

        if self.isNameOneTokenOnly(name1, name2) and self.isFirstTokenMatch(n1, n2):
            return HIGH_MATCH

        nc = NameInitialsComp.NameInitialsComp(name1.lower(), name2.lower())
        if len(n1) == 0 or len(n2) == 0:
            return NA
        if self.isMissingTokenCase(n1, n2):
            return nc.initialMatchStatus

        tc1 = self.getTokenCount(n1)
        tc2 = self.getTokenCount(n2)

        if tc1 == tc2:
            if self.isLevenshteinMatch(n1, n2):
                return nc.initialMatchStatus
            if self.lengthDiff(n1, n2) <= 6 and self.getFuzzyWeightedRatio(n1, n2) >= 80:
                if self.isTokenWiseMatch(n1, n2) and (tc1 != 1 and tc2 != 1):
                    return nc.initialMatchStatus
                else:
                    return self.matchWholeString(n1, n2, nc)
            else:
                return self.matchPartially(n1, n2, nc)
        else:
            res = self.matchWholeString(n1, n2, nc)
            return self.matchPartially(n1, n2, nc) if res == NO_MATCH else res

    def matchPartially(self, n1, n2, nc):
        if self.isAtleastSingleTokenMatch(n1, n2) and nc.hasCommonWords:
            if nc.initialsExpanded:
                if nc.initialMatchStatus == HIGH_MATCH and nc.allWordsExhausted:
                    return HIGH_MATCH
                elif nc.initialMatchStatus == HIGH_MATCH and (not nc.allWordsExhausted):
                    return LOW_MATCH
                elif nc.initialMatchStatus == NO_MATCH:
                    return NO_MATCH
                else:
                    return NO_MATCH
            elif nc.initialMatchStatus == HIGH_MATCH and nc.containsInitials:
                return LOW_MATCH
            elif nc.initialMatchStatus == HIGH_MATCH and (not nc.containsInitials):
                return NO_MATCH
            else:
                return NO_MATCH
        else:
            return NO_MATCH

    def isAtleastSingleTokenMatch(self, s1, s2):
        s1t = s1.split()
        s2t = s2.split()
        for s in s1t:
            if len(s) > 1:
                for t in s2t:
                    if len(t) > 1:
                        if self.getCosineSimilarity(s, t, 2) >= 80 or self.isLevenshteinMatch(s, t):
                            return True

        return False

    def isMissingTokenCase(self, n1, n2):
        ns1 = n1.split()
        ns2 = n2.split()
        if len(ns1) == 2 and len(ns2) == 3:
            # matched token should not be initial
            if len(ns1[0]) > 1:
                # Handles ("Rashika P","Rashika C P"), ("Rashika C","Rashika C P")
                return ns1[0] == ns2[0] and (ns1[1] == ns2[2] or ns1[1][0] == ns2[2][0] or ns1[1] == ns2[1] or ns1[1][0] == ns2[1][0])
            else:
                #  Handles ("P Rashika","C P Rashika"), ("C Rashika","C P Rashika")
                return ns1[1] == ns2[2] and (ns1[0] == ns2[0] or ns1[0][0] == ns2[0][0] or ns1[0] == ns2[1] or ns1[0][0] == ns2[1][0])
        elif len(ns2) == 2 and len(ns1) == 3:
            # matched token should not be initial
            if len(ns1[0]) > 1:
                # Handles  ("Rashika C P","Rashika P"), ("Rashika C P","Rashika C")
                return ns2[0] == ns1[0] and (ns2[1] == ns1[2] or ns2[1][0] == ns1[2][0] or ns2[1] == ns1[1] or ns2[1][0] == ns1[1][0])
            else:
                # Handles  ("C P Rashika","P Rashika"), ("C P Rashika","C Rashika")
                return ns2[1] == ns1[2] and (ns2[0] == ns1[0] or ns2[0][0] == ns1[0][0] or ns2[0] == ns1[1] or ns2[0][0] == ns1[1][0])

        elif len(ns1) >= 3 and len(ns2) >= 3:
            return ns1[0] == ns2[0] and ns1[1] == ns2[1] and (len(ns1[0]) > 1 or len(ns1[1]) > 1)
        else:
            return False

    def matchWholeString(self, n1, n2, nc):
        n1_ns = self.removeSpaces(n1)
        n2_ns = self.removeSpaces(n2)
        if self.lengthDiff(n1_ns, n2_ns) > 4:
            return NO_MATCH

        a = self.getCosineSimilarity(n1_ns, n2_ns, 2)
        b = fuzz.WRatio(n1_ns, n2_ns)
        c = self.getCosineSimilarity(n1_ns, n2_ns, 1)

        if (((a >= 70 and b >= 85) or (a >= 80 and b >= 70)) and (n1_ns[0] == n2_ns[0]) and c >= 90):
            return nc.initialMatchStatus
        return NO_MATCH

    # Custom cosine similarity. Need to replace with a standard Library
    def getCosineSimilarity(self, n1, n2, k):
        ns1 = n1.replace(" ", '')
        ns2 = n2.replace(" ", '')

        tf_ns1 = self.getTermFrequency(ns1, k)
        tf_ns2 = self.getTermFrequency(ns2, k)

        intersection = set(tf_ns1.keys()).intersection(set(tf_ns2.keys()))

        dot_prod = 0
        for x in intersection:
            dot_prod += tf_ns1[x] * tf_ns2[x]

        mag_ns1 = 0
        mag_ns2 = 0

        for x in tf_ns1.keys():
            mag_ns1 += tf_ns1[x] ** 2

        for x in tf_ns2.keys():
            mag_ns2 += tf_ns2[x] ** 2

        return (dot_prod / (math.sqrt(mag_ns1 * mag_ns2))) * 100

    def cleanName(self, name):
        if name == None:
            return name
        sb = ''
        for c in name:
            if c.isalpha():
                sb += c.lower()
            else:
                sb += ' '
        return sb

    def removeTitle(self, name):
        cp = ContactProcessor
        name_dict = cp.getTitlesRemoved(name)

        # for t in TITLES:
        #     if name.startswith(t + ' '):
        #         return name[len(t):len(name):1].strip()

        return name_dict['name']

    def getTermFrequency(self, text, k):
        tm_freq = {}

        for i in range(len(text) - k + 1):
            shingle = text[i:i + k:1]
            count = tm_freq[shingle] if shingle in tm_freq.keys() else 0
            tm_freq[shingle] = count + 1
        return tm_freq

    def removeSpaces(self, n1):
        return n1.replace(' ', '')

    def isTokenWiseMatch(self, n1, n2):
        ns1 = n1.split()
        ns2 = n2.split()

        for s in ns1:
            flag = False
            for t in ns2:
                if (t[0] == s[0]) and (self.getFuzzyWeightedRatio(s, t) >= 80 or self.isLevenshteinMatch(s, t)):
                    flag = True
                    break
            if not flag:
                return False

        return True

    def getFuzzyWeightedRatio(self, n1, n2):
        return fuzz.WRatio(n1, n2)

    def lengthDiff(self, n1, n2):
        return abs(len(n1.replace(' ', '')) - len(n2.replace(' ', '')))

    def isLevenshteinMatch(self, n1, n2):
        if n1[0] != n2[0]:
            return False
        ed = lev.distance(n1, n2)
        minLen = min(len(n1), len(n2))
        if minLen <= 3:
            return ed == 0
        if minLen <= 5:
            return ed <= 1
        else:
            return ed <= 2

    def getTokenCount(self, n2):
        return len(n2.split())

    def isFirstTokenMatch(self, name1, name2):
        ns1 = name1.split()
        ns2 = name2.split()

        if not (len(ns1) == 1 or len(ns2) == 1):
            return False
        return ns1[0].lower() == ns2[0].lower() if len(ns1) > 0 and len(ns2) > 0 else False

    def isNameOneTokenOnly(self, name1, name2):
        ns1 = name1.split()
        ns2 = name2.split()

        return len(ns1) == 1 or len(ns2) == 1

    def isNameTokenWiseExact(self, name1, name2):
        ns1 = name1.split()
        ns2 = name2.split()

        if len(ns1) != len(ns2):
            return False
        string_set = set(x for x in ns1)

        for s in ns2:
            if s not in string_set:
                return False

        return True

    def isNameExactMatch(self, name1, name2):
        name1 = self.discardSpaces(name1)
        name2 = self.discardSpaces(name2)

        return name1 == name2

    def discardSpaces(self, name):
        if name == None:
            return name
        sb = ''
        for c in name:
            if c.isalpha():
                sb += c.lower()
        return sb

    def removeInitials(self, name1):
        token = name1.split()
        ret_name = ''
        for t in token:
            if len(t) == 1:
                continue
            ret_name += t + ' '
        return ret_name.strip()

    def removeRepeatedTokensWithOrder(self, name):
        if len(name) > 0:
            name_tkn = name.split()
            fin_name = []
            for x in name_tkn:
                if x not in fin_name:
                    fin_name.append(x)

            return ' '.join(fin_name)

        else:
            return name
