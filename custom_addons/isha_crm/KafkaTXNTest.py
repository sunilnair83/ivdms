import traceback

import odoo
import TxnUpdater
import TxnProcessor
import ContactProcessor
import GoldenContactAdv
import ErrorUpdater
import unittest, csv
import configparser
null = None
false = False
true = True

class kafkaTest(unittest.TestCase):

    null = None
    DB_NAME = 'odoo'
    UID = odoo.SUPERUSER_ID
    registry = odoo.modules.registry.Registry(DB_NAME)
    config = configparser.RawConfigParser()
    with open('/mnt/extra-addons/isha_crm/data/country-nationality-iso2-map.csv') as f:
        iso2 = dict(filter(None, csv.reader(f)))
    with open('/mnt/extra-addons/isha_crm/data/eReceipts-country-nationality-corrections.csv') as f:
        csvReader = []
        for x in csv.reader(f):
            csvReader.append([x[0].upper(), x[1].upper()])
        correction = dict(filter(None, csvReader))




    def test_1_al_pgm(self):
        msg = {"schema":{"type":"struct","fields":[{"type":"int64","optional":false,"field":"Pam_Serial_Number"},{"type":"string","optional":false,"field":"Pam_Program_Id"},{"type":"int16","optional":false,"field":"Pam_Teacher_Id"},{"type":"string","optional":false,"field":"Pam_Center_Id"},{"type":"string","optional":false,"field":"Pam_Start_Date"},{"type":"string","optional":true,"field":"end_date"},{"type":"string","optional":true,"field":"created_at"},{"type":"string","optional":true,"field":"modified_at"},{"type":"int64","optional":true,"name":"org.apache.kafka.connect.data.Timestamp","version":1,"field":"Pam_Record_Created_Date"},{"type":"int64","optional":true,"name":"org.apache.kafka.connect.data.Timestamp","version":1,"field":"Pam_Record_Modified_Date"},{"type":"string","optional":true,"field":"teacher_name"},{"type":"string","optional":true,"field":"center_name"},{"type":"string","optional":true,"field":"schedule_name"},{"type":"string","optional":true,"field":"country_name"}],"optional":false},"payload":{"Pam_Serial_Number":32843,"Pam_Program_Id":"327","Pam_Teacher_Id":1200,"Pam_Center_Id":"0359","Pam_Start_Date":"05/10/2019","end_date":"2019-10-06","created_at":"2019-10-19T15:22:12Z","modified_at":"2019-10-19T15:22:12Z","Pam_Record_Created_Date":1571498532000,"Pam_Record_Modified_Date":1571498532000,"teacher_name":"Sadhguru","center_name":"DELHI","schedule_name":"Inner Engineering with Sadhguru","country_name":"IN"}}
        msg_processor = TxnProcessor.Program()
        odoo_rec = msg_processor.getOdooFormat(msg)
        pgm = TxnUpdater.ProgramAttUpdater()
        flag_dict = pgm.findMatch(self.registry, odoo_rec)
        self.assertTrue(flag_dict['flag'])

    def test_2_al_adv_pgm(self):
        msg = {"schema":{"type":"struct","fields":[{"type":"int64","optional":false,"field":"Pap_Serial_Number"},{"type":"string","optional":false,"field":"Pap_Program_Id"},{"type":"int16","optional":false,"field":"Pap_Teacher_Id"},{"type":"string","optional":false,"field":"Pap_Center_Id"},{"type":"string","optional":false,"field":"Pap_Start_Date"},{"type":"string","optional":true,"field":"end_date"},{"type":"string","optional":true,"field":"created_at"},{"type":"string","optional":true,"field":"modified_at"},{"type":"int64","optional":false,"name":"org.apache.kafka.connect.data.Timestamp","version":1,"field":"Pap_Record_Created_Date"},{"type":"int64","optional":false,"name":"org.apache.kafka.connect.data.Timestamp","version":1,"field":"Pap_Record_Modified_Date"},{"type":"int64","optional":true,"field":"Pap_Pam_Serial_Number"},{"type":"string","optional":true,"field":"teacher_name"},{"type":"string","optional":true,"field":"center_name"},{"type":"string","optional":true,"field":"schedule_name"},{"type":"string","optional":true,"field":"country_name"}],"optional":false},"payload":{"Pap_Serial_Number":32843,"Pap_Program_Id":"20","Pap_Teacher_Id":2422,"Pap_Center_Id":"0721","Pap_Start_Date":"13/09/2019","end_date":"0000-00-00","created_at":"2019-10-17T12:53:19Z","modified_at":"2019-10-17T12:53:19Z","Pap_Record_Created_Date":1571316799000,"Pap_Record_Modified_Date":1571316799000,"Pap_Pam_Serial_Number":32843,"teacher_name":"Sumit Mathur","center_name":"ASHRAM - SPANDA HALL","schedule_name":"BSP - GENTS","country_name":"IN"}}
        msg_processor = TxnProcessor.ProgramAdv()
        odoo_rec = msg_processor.getOdooFormat(msg)
        pgm = TxnUpdater.ProgramAttUpdater()
        flag_dict = pgm.findMatch(self.registry, odoo_rec)
        self.assertTrue(flag_dict['flag'])

    def test_3_ereceipts(self):
        msg = {"schema":{"type":"struct","fields":[{"type":"int64","optional":true,"field":"record_modified_at"},{"type":"int64","optional":false,"field":"row_id"},{"type":"string","optional":false,"field":"id"},{"type":"string","optional":false,"field":"contact_id"},{"type":"string","optional":true,"field":"modified_at"},{"type":"string","optional":true,"field":"modified_by"},{"type":"string","optional":true,"field":"project"},{"type":"string","optional":true,"field":"mode_of_payment"},{"type":"float","optional":true,"field":"amount"},{"type":"string","optional":true,"field":"receipt_number"},{"type":"string","optional":true,"field":"receipt_date"},{"type":"string","optional":true,"field":"entity"},{"type":"string","optional":true,"field":"status"},{"type":"int16","optional":true,"field":"active"},{"type":"string","optional":true,"field":"purpose_type"}],"optional":false},"payload":{"record_modified_at":1569888060,"row_id":57240346,"id":"FND-1819-5b5958e229c58d603bf574424c6b","contact_id":"OTR-1920-5d944e5429c58de30815a07f","modified_at":"2019-09-30T00:00:00Z","modified_by":"emedia@ishafoundation.org","project":"Donation","mode_of_payment":"Online","amount":500.0,"receipt_number":"if19-G6S-07279","receipt_date":"2018-07-25T18:30:00Z","entity":"IshaFoundation","status":"SUCCESS","active":1,"purpose_type":null}}
        msg_processor = TxnProcessor.EreciptsTxn()
        odoo_rec = msg_processor.getOdooFormat(msg)
        donation = TxnUpdater.DonationsUpdater()
        flag_dict = donation.createDonation(self.registry, odoo_rec)
        self.assertTrue(flag_dict['flag'])

    def test_4_dms(self):
        msg = {"schema":{"type":"struct","fields":[{"type":"int64","optional":false,"field":"row_id"},{"type":"string","optional":false,"field":"local_trans_id"},{"type":"string","optional":false,"field":"modified_at"},{"type":"string","optional":false,"field":"modified_by"},{"type":"string","optional":false,"field":"local_donor_id"},{"type":"string","optional":false,"field":"donor_type"},{"type":"string","optional":false,"field":"donor_poc_id"},{"type":"string","optional":false,"field":"local_patron_id"},{"type":"string","optional":false,"field":"patron_type"},{"type":"string","optional":false,"field":"patron_poc_id"},{"type":"string","optional":false,"field":"project"},{"type":"string","optional":false,"field":"mode_of_payment"},{"type":"string","optional":false,"field":"inkind"},{"type":"int64","optional":false,"field":"amount"},{"type":"string","optional":false,"field":"receipt_number"},{"type":"string","optional":false,"field":"receipt_date"},{"type":"int32","optional":false,"field":"active"},{"type":"string","optional":false,"field":"entity"},{"type":"string","optional":false,"field":"donation_status"},{"type":"string","optional":false,"field":"record_created_at"}],"optional":false},"payload":{"row_id":114406,"local_trans_id":"622cabdd-7920-36da-7d73-5da42c3aaed9","modified_at":"2019-10-14T08:43:26Z","modified_by":"PapaRao","local_donor_id":"6037b239-b41f-cd49-3827-5da42ce511a1","donor_type":"Individual","donor_poc_id":"","local_patron_id":"","patron_type":"No","patron_poc_id":"","project":"Donations - Tree Saplings","mode_of_payment":"Cash","inkind":"NotInkind","amount":126,"receipt_number":"io20-ECC-001122","receipt_date":"2019-10-14","active":1,"entity":"IshaOutreach","donation_status":"SUCCESS","record_created_at":"2019-10-14 09:30:00"}}
        msg_processor = TxnProcessor.DMSTxn()

        odoo_rec = msg_processor.getOdooFormat(msg)
        donation = TxnUpdater.DonationsUpdater()
        flag_dict = donation.createDonation(self.registry, odoo_rec)
        self.assertTrue(flag_dict['flag'])


    def test_5_modified_ereceipt(self):
        msg = {"schema":{"type":"struct","fields":[{"type":"int64","optional":true,"field":"record_modified_at"},{"type":"int64","optional":false,"field":"row_id"},{"type":"string","optional":false,"field":"id"},{"type":"string","optional":false,"field":"contact_id"},{"type":"string","optional":true,"field":"modified_at"},{"type":"string","optional":true,"field":"modified_by"},{"type":"string","optional":true,"field":"project"},{"type":"string","optional":true,"field":"mode_of_payment"},{"type":"float","optional":true,"field":"amount"},{"type":"string","optional":true,"field":"receipt_number"},{"type":"string","optional":true,"field":"receipt_date"},{"type":"string","optional":true,"field":"entity"},{"type":"string","optional":true,"field":"status"},{"type":"int16","optional":true,"field":"active"},{"type":"string","optional":true,"field":"purpose_type"}],"optional":false},"payload":{"record_modified_at":1569888060,"row_id":570346,"id":"FND-1819-5b5958e229c58d603bf57c6b","contact_id":"FND-1819-5b5958e229c58d603bf57c6b","modified_at":"2019-09-30T00:00:00Z","modified_by":"emedia@ishafoundation.org","project":"Donation","mode_of_payment":"Online","amount":500.0,"receipt_number":"if19-G6S-07000a","receipt_date":"2018-07-25T18:30:00Z","entity":"IshaOutreach","status":"SUCCESS","active":1,"purpose_type":null}}
        msg_processor = TxnProcessor.EreciptsTxn()
        odoo_rec = msg_processor.getOdooFormat(msg)
        donation = TxnUpdater.DonationsUpdater()
        flag_dict = donation.findMatch(self.registry, odoo_rec)
        rec = flag_dict['rec']
        self.assertTrue(flag_dict['active'])
        msg = {"schema":{"type":"struct","fields":[{"type":"int64","optional":false,"field":"row_id"},{"type":"string","optional":false,"field":"local_trans_id"},{"type":"string","optional":false,"field":"modified_at"},{"type":"string","optional":false,"field":"modified_by"},{"type":"string","optional":false,"field":"local_donor_id"},{"type":"string","optional":false,"field":"donor_type"},{"type":"string","optional":false,"field":"donor_poc_id"},{"type":"string","optional":false,"field":"local_patron_id"},{"type":"string","optional":false,"field":"patron_type"},{"type":"string","optional":false,"field":"patron_poc_id"},{"type":"string","optional":false,"field":"project"},{"type":"string","optional":false,"field":"mode_of_payment"},{"type":"string","optional":false,"field":"inkind"},{"type":"int64","optional":false,"field":"amount"},{"type":"string","optional":false,"field":"receipt_number"},{"type":"string","optional":false,"field":"receipt_date"},{"type":"int32","optional":false,"field":"active"},{"type":"string","optional":false,"field":"entity"},{"type":"string","optional":false,"field":"donation_status"},{"type":"string","optional":false,"field":"record_created_at"}],"optional":false},"payload":{"row_id":114406,"local_trans_id":"622cabdd-7920-36da-7d73-5da42c3aaed9","modified_at":"2019-10-14T08:43:26Z","modified_by":"PapaRao","local_donor_id":"6037b239-b41f-cd49-3827-5da42ce511a1","donor_type":"Individual","donor_poc_id":"","local_patron_id":"","patron_type":"No","patron_poc_id":"","project":"Donations - Tree Saplings","mode_of_payment":"Cash","inkind":"NotInkind","amount":126,"receipt_number":"if19-G6S-07000","receipt_date":"2019-10-14","active":1,"entity":"IshaOutreach","donation_status":"SUCCESS","record_created_at":"2019-10-14 09:30:00"}}
        msg_processor = TxnProcessor.DMSTxn()
        odoo_rec = msg_processor.getOdooFormat(msg)
        donation = TxnUpdater.DonationsUpdater()
        flag_dict = donation.findMatch(self.registry, odoo_rec)
        self.assertFalse(flag_dict['active'])


    def test_6_tags_computed_fields(self):
        local_contact_id = 'OTR-1920-5d9c94ba29c58d491215a830'
        al = ContactProcessor.ALContact()
        msg =  {"Pam_Serial_Number": local_contact_id, "Pam_Initial": " ", "Pam_Participant_Name": "test tag creation",
               "Pam_Address_Line1": "1", "Pam_Address_Line2": "Sriram Nager", "Pam_Address_Line3": "Hudco Coloney",
               "Pam_City": "Coimbatore", "Pam_District_County": "", "Pam_State": "Tamil Nadu", "Pam_Country": "India",
               "Pam_Postal_Code": "560097", "Pam_Home_Phone": "", "Pam_Mobile_Phone": "7057802204",
               "Pam_Mobile_Phone1": "7760749054", "Pam_Email_ID": "vinod0511@gmail.com", "Pam_Oemail_Id": "vinodkumarrajputh@gmail.com",
               "Pam_Occupation": " ", "Pam_Work_Address_Line1": " ", "Pam_Work_Address_Line2": " ",
               "Pam_Work_Address_Line3": " ", "Pam_Work_City": " ", "Pam_Work_District_County": "",
               "Pam_Work_State": " ", "Pam_Work_Country": " ", "Pam_Work_PostalCode": " ", "Pam_Work_Phone": " ",
               "Pam_Gender": "M", "Pam_Age": "39", "Pam_Date_Of_Birth": "03-11-1970", "Pam_Record_Created_By": "0",
               "Pam_Record_Modified_By": "0", "Pam_Record_Created_Date": 1268839511000,
               "Pam_Record_Modified_Date": 19801000}
        UID = odoo.SUPERUSER_ID
        cr = self.registry.cursor()
        with odoo.api.Environment.manage():
            env = odoo.api.Environment(cr, UID, {})
            flag_dict = GoldenContactAdv.GoldenContactAdv().createOrUpdate(env,  al.getOdooFormat(msg, self.iso2, self.correction))
        self.assertTrue(flag_dict['flag'])

        msg = {"schema":{"type":"struct","fields":[{"type":"int64","optional":false,"field":"Pam_Serial_Number"},{"type":"string","optional":false,"field":"Pam_Program_Id"},{"type":"int16","optional":false,"field":"Pam_Teacher_Id"},{"type":"string","optional":false,"field":"Pam_Center_Id"},{"type":"string","optional":false,"field":"Pam_Start_Date"},{"type":"string","optional":true,"field":"end_date"},{"type":"string","optional":true,"field":"created_at"},{"type":"string","optional":true,"field":"modified_at"},{"type":"int64","optional":true,"name":"org.apache.kafka.connect.data.Timestamp","version":1,"field":"Pam_Record_Created_Date"},{"type":"int64","optional":true,"name":"org.apache.kafka.connect.data.Timestamp","version":1,"field":"Pam_Record_Modified_Date"},{"type":"string","optional":true,"field":"teacher_name"},{"type":"string","optional":true,"field":"center_name"},{"type":"string","optional":true,"field":"schedule_name"},{"type":"string","optional":true,"field":"country_name"}],"optional":false},"payload":{"Pam_Serial_Number":local_contact_id,"Pam_Program_Id":"327","Pam_Teacher_Id":1200,"Pam_Center_Id":"0359","Pam_Start_Date":"05/10/2019","end_date":"2019-10-06","created_at":"2019-10-19T15:22:12Z","modified_at":"2019-10-19T15:22:12Z","Pam_Record_Created_Date":1571498532000,"Pam_Record_Modified_Date":1571498532000,"teacher_name":"Sadhguru","center_name":"DELHI","schedule_name":"Inner Engineering with Sadhguru","country_name":"IN"}}
        msg_processor = TxnProcessor.Program()
        odoo_rec = msg_processor.getOdooFormat(msg)
        pgm = TxnUpdater.ProgramAttUpdater()
        flag_dict = pgm.findMatch(self.registry, odoo_rec)
        self.assertTrue(flag_dict['flag'])
        msg = {"schema":{"type":"struct","fields":[{"type":"int64","optional":false,"field":"Pap_Serial_Number"},{"type":"string","optional":false,"field":"Pap_Program_Id"},{"type":"int16","optional":false,"field":"Pap_Teacher_Id"},{"type":"string","optional":false,"field":"Pap_Center_Id"},{"type":"string","optional":false,"field":"Pap_Start_Date"},{"type":"string","optional":true,"field":"end_date"},{"type":"string","optional":true,"field":"created_at"},{"type":"string","optional":true,"field":"modified_at"},{"type":"int64","optional":false,"name":"org.apache.kafka.connect.data.Timestamp","version":1,"field":"Pap_Record_Created_Date"},{"type":"int64","optional":false,"name":"org.apache.kafka.connect.data.Timestamp","version":1,"field":"Pap_Record_Modified_Date"},{"type":"int64","optional":true,"field":"Pap_Pam_Serial_Number"},{"type":"string","optional":true,"field":"teacher_name"},{"type":"string","optional":true,"field":"center_name"},{"type":"string","optional":true,"field":"schedule_name"},{"type":"string","optional":true,"field":"country_name"}],"optional":false},"payload":{"Pap_Serial_Number":local_contact_id,"Pap_Program_Id":"20","Pap_Teacher_Id":2422,"Pap_Center_Id":"0721","Pap_Start_Date":"13/12/2019","end_date":"0000-00-00","created_at":"2019-10-17T12:53:19Z","modified_at":"2019-10-17T12:53:19Z","Pap_Record_Created_Date":1571316799000,"Pap_Record_Modified_Date":1571316799000,"Pap_Pam_Serial_Number":local_contact_id,"teacher_name":"Sumit Mathur","center_name":"ASHRAM - SPANDA HALL","schedule_name":"BSP - GENTS","country_name":"IN"}}
        msg_processor = TxnProcessor.ProgramAdv()
        odoo_rec = msg_processor.getOdooFormat(msg)
        pgm = TxnUpdater.ProgramAttUpdater()
        flag_dict = pgm.findMatch(self.registry, odoo_rec)
        self.assertTrue(flag_dict['flag'])
        msg = {"schema":{"type":"struct","fields":[{"type":"int64","optional":false,"field":"Pap_Serial_Number"},{"type":"string","optional":false,"field":"Pap_Program_Id"},{"type":"int16","optional":false,"field":"Pap_Teacher_Id"},{"type":"string","optional":false,"field":"Pap_Center_Id"},{"type":"string","optional":false,"field":"Pap_Start_Date"},{"type":"string","optional":true,"field":"end_date"},{"type":"string","optional":true,"field":"created_at"},{"type":"string","optional":true,"field":"modified_at"},{"type":"int64","optional":false,"name":"org.apache.kafka.connect.data.Timestamp","version":1,"field":"Pap_Record_Created_Date"},{"type":"int64","optional":false,"name":"org.apache.kafka.connect.data.Timestamp","version":1,"field":"Pap_Record_Modified_Date"},{"type":"int64","optional":true,"field":"Pap_Pam_Serial_Number"},{"type":"string","optional":true,"field":"teacher_name"},{"type":"string","optional":true,"field":"center_name"},{"type":"string","optional":true,"field":"schedule_name"},{"type":"string","optional":true,"field":"country_name"}],"optional":false},"payload":{"Pap_Serial_Number":local_contact_id+'343',"Pap_Program_Id":"67","Pap_Teacher_Id":2422,"Pap_Center_Id":"0721","Pap_Start_Date":"13/12/2019","end_date":"0000-00-00","created_at":"2019-10-17T12:53:19Z","modified_at":"2019-10-17T12:53:19Z","Pap_Record_Created_Date":1571316799000,"Pap_Record_Modified_Date":1571316799000,"Pap_Pam_Serial_Number":local_contact_id,"teacher_name":"Sumit Mathur","center_name":"ASHRAM - SPANDA HALL","schedule_name":"Shoonya","country_name":"IN"}}
        msg_processor = TxnProcessor.ProgramAdv()
        odoo_rec = msg_processor.getOdooFormat(msg)
        pgm = TxnUpdater.ProgramAttUpdater()
        flag_dict = pgm.findMatch(self.registry, odoo_rec)
        self.assertTrue(flag_dict['flag'])
        cr.commit()



if __name__ == '__main__':
    unittest.main()
