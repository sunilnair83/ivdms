import logging

from odoo import models, fields, api
from .constants import CHARITABLE_GROUPS, RELIGIOUS_GROUPS
from ..kafka_processors import RudrakshaDeekshaProcessor
from ..models.kafka import iso2, correction

_logger = logging.getLogger(__name__)

class AdiYogiPrasad (models.Model):
    _name = 'rudraksha.deeksha'
    _description = 'Rudraksha Deeksha'
    _order = "write_date desc"

    contact_id_fkey = fields.Many2one('res.partner', string='Contact', auto_join=True, index=True)
    name = fields.Char(string='Name')
    phone_country_code = fields.Char(string='Phone CC')
    phone = fields.Char(string='Registration Phone', index=True)
    email = fields.Char(string='Registration Email', index=True)
    whatsapp_country_code = fields.Char(string='WhatsApp CC')
    whatsapp_number = fields.Char(string='Registration WhatsApp', index=True)
    street = fields.Char(string='Street1')
    street2 = fields.Char(string='Street2')
    landmark = fields.Char(string='Landmark')
    city = fields.Char(string='City')
    state = fields.Char(string='State')
    country = fields.Char(string='Country')
    zip = fields.Char(string='Pincode')
    registration_date = fields.Datetime(string='Registration Date')
    reg_status = fields.Char(string='Registration Status')
    no_of_rudraksha = fields.Integer(string='No. of Rudraksha')
    # made_donation = fields.Boolean(string='Donation Made?')
    courier_supported = fields.Boolean(string='Courier Supported?')
    local_txn_id = fields.Char(string='Local Trans Id')
    reg_source = fields.Char(string='Registration Source',default='Outreach')
    active = fields.Boolean(string='Active', default=True)
    sso_id = fields.Char(string='SSO Id')
    utm_campaign = fields.Char(string='UTM Campaign')
    utm_source = fields.Char(string='UTM Source')
    utm_medium = fields.Char(string='UTM Medium')
    utm_content = fields.Char(string='UTM Content')
    utm_term = fields.Char(string='UTM Term')
    language_pref = fields.Char(string='Language Preference')

    # Related Fields
    pgm_tag_ids = fields.Many2many(related='contact_id_fkey.pgm_tag_ids',string='Program Tags')
    category_id = fields.Many2many(related='contact_id_fkey.category_id',string='Tags')
    partner_phone = fields.Char(related='contact_id_fkey.phone',string='Phone')
    partner_whatsapp_number = fields.Char(related='contact_id_fkey.whatsapp_number',string='Whatsapp')
    partner_email = fields.Char(related='contact_id_fkey.email',string='Email')
    center_id = fields.Many2one(related='contact_id_fkey.center_id',string='Center',store=True, index=True)
    region_id = fields.Many2one(related='center_id.region_id',string='Region',store=True)
    is_meditator = fields.Boolean(related='contact_id_fkey.is_meditator',store=True)
    whats_app_group = fields.Char(string='WhatsApp Group')

    # related fields from res_partner for email marketing
    email_validity = fields.Selection(related='contact_id_fkey.email_validity', store=True)
    dnd_email = fields.Boolean(related='contact_id_fkey.dnd_email', store=True)
    deceased = fields.Boolean(related='contact_id_fkey.deceased', store=True)
    is_caca_donor = fields.Boolean(related='contact_id_fkey.is_caca_donor', store=True)
    last_txn_date = fields.Date(related='contact_id_fkey.last_txn_date', store=True)
    influencer_type = fields.Selection(string="Influencer Type", related='contact_id_fkey.influencer_type', store=True)

    consignment_no = fields.Char(string='Consignment No')
    delivery_status = fields.Char(string='Delivery Status')
    status_updates = fields.Char(string='Status Update')

    def populate_contact_id(self,limit):
        contactModel = self.env['res.partner']
        recs = self.sudo().search([('contact_id_fkey','=',False)], limit=limit)
        for rec in recs:
            source_rec = {
                'first_name': rec.name,
                'last_name':'',
                'phone_countr_code':rec.phone_country_code,
                'phone': rec.phone,
                'email': rec.email,
                'whatsapp_country_code': rec.whatsapp_country_code,
                'whatsapp_number': rec.whatsapp_number,
                'street': rec.street,
                'street2': rec.street2,
                'city': rec.city,
                'state': rec.state,
                'country': rec.country,
                'zip': rec.zip,
                'registration_id':rec.local_txn_id,
                'sso_id': rec.sso_id,
            }
            _logger.info(str(source_rec))
            contact_dict = RudrakshaDeekshaProcessor.RudrakshaDeekshaProcessor().getOdooFormatContact(source_rec,iso2,correction)
            contact = contactModel.sudo().create_internal(contact_dict)
            rec.contact_id_fkey = contact[0].id
            self.env.cr.commit()


    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):

        context = self._context or {}
        if context.get('center_scope'):
            args += [('center_id', 'in', self.env.user.centers.ids)]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('contact_id_fkey.has_charitable_txn', '=', True)]
            if religious:
                rel_char_domain = (['|', ('contact_id_fkey.has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('contact_id_fkey.has_religious_txn', '=', True)]
            args = rel_char_domain + args

        return super(AdiYogiPrasad, self)._search(args, offset, limit, order, count=count,
                                                 access_rights_uid=access_rights_uid)


    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        context = self._context or {}
        if context.get('center_scope'):
            domain += [('center_id', 'in', self.env.user.centers.ids)]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('contact_id_fkey.has_charitable_txn', '=', True)]
            if religious:
                rel_char_domain = (['|', ('contact_id_fkey.has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('contact_id_fkey.has_religious_txn', '=', True)]
            domain = rel_char_domain + domain
        return super(AdiYogiPrasad, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                    orderby=orderby, lazy=lazy)

    def get_csv_handler(self):
        view = self.env.ref('isha_crm.rd_csv_handler_wizard_form')
        view_id = view and view.id or False
        context = dict(self._context or {})
        return {
            'name': 'Upload Courier Details',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'crm.csv.handler',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': context
        }