import logging

from odoo import models, fields, api
from odoo.exceptions import ValidationError
_logger = logging.getLogger(__name__)

class PrEvents (models.Model):
    _name = "pr.events"
    _description = 'PR Events'
    _inherit = 'mail.thread'
    _order = "write_date desc"
    _rec_name = "event_name"

    event_name = fields.Char(string='Event Name', required=True)
    _sql_constraints = [
        ('event_name_unique', 'unique(event_name)', 'Choose another Event Name - it has to be unique!')
    ]
    event_description = fields.Char(string='Event Description', required=True)
    active = fields.Boolean(String="Active", default=True, groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_regional_admin')

    start_date = fields.Datetime(string='Start Date', required=True)
    end_date = fields.Datetime(string='End Date', required=True)
    sg_participation = fields.Selection([('none', 'None'), ('attendee', 'Attendee'),('panellist','Panelist'),('main_speaker','Main Speaker')],
                                       string='Sadhguru Participation', required=True)
    bay_names = fields.Many2many('bay.name', column1='pr_event_id', column2='bay_id',
                                    string='Bay Names')
    event_type = fields.Selection([('virtual', 'Virtual'), ('in-person', 'In-Person'),('combination','Combination')],
                                  string='Event Type', required=True)
    event_access = fields.Selection([('public', 'Public'), ('private', 'Private')],
                                    string='Event Access', required=True)

    event_venue = fields.Char(string='Event Venue', required=False)
    country_id = fields.Many2one('res.country', 'Event Country', required=True)
    state_id = fields.Many2one('res.country.state', string='Event State')
    state = fields.Char(string='State Of Work Address', required=False)
    country = fields.Char(string='Country Of Work Address', required=False)
    city = fields.Char('Event City', help='Enter City')
    event_pincode = fields.Char(string="Event Pincode", required=False)
    organizing_company = fields.Many2one(string='Organizing Company', comodel_name='isha.company', ondelete='set null')
    primary_organizer = fields.Many2one(string='Primary Organizer', comodel_name='res.partner', index=True)
    pr_event_attendees = fields.One2many('event.attendees', 'event_id', string='Event Attendees')

    event_region_list = fields.Many2one('isha.region', string='Event Region', required=True, inverse='_inverse_region', store=True,
                                        compute='_compute_region_name', domain=lambda self: [("id", "in", self.env.user.centers.mapped('region_id').ids)])

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(PrEvents, self).fields_get(allfields, attributes=attributes)
        if self._context.get('restrict_fields_get_list'):
            not_selectable_fields = set(res.keys()) - set(self._context.get('restrict_fields_get_list'))
            for field in not_selectable_fields:
                res[field]['selectable'] = False  # to hide in Add Custom filter view
                res[field]['sortable'] = False  # to hide in group by view
                # res[field]['exportable'] = False # to hide in export list
                res[field]['store'] = False  # to hide in 'Select Columns' filter in tree views
        return res

    def unlink(self):
        for rec in self:
            rec_list = self.env['event.attendees'].search([('event_name', '=', rec.event_name)],
                limit=1)
            if len(rec_list) > 0:
                raise ValidationError(('Operation Denied !!!. \n One or more Events cannot be deleted as it is assigned to contact.'))
        return super(PrEvents, self).unlink()


    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}
        if context.get('region_scope'):
            argsRegionNames = []
            for rec in self.env.user.centers.mapped('region_id').ids:
                    isha_region = self.env['isha.region'].browse(rec)
                    argsRegionNames.append(isha_region.name)
            args += [('event_region_list', 'in', argsRegionNames)]
        return super(PrEvents, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)


    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        context = self._context or {}
        # if context.get('region_scope'):
        #     domain += [('center_id', 'in', self.env.user.centers.ids)]
        return super(PrEvents, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                         orderby=orderby, lazy=lazy)

    @api.onchange('country_id')
    def _onchange_country_id(self):
        self.ensure_one()
        self.country = self.country_id.code

    @api.onchange('state_id')
    def _onchange_state_id(self):
        self.ensure_one()
        self.state = self.state_id.name

    def _inverse_region(self):
        for prEvents in self:
            prEvents.event_region_list = prEvents.event_region_list

    @api.depends('event_pincode')
    def _compute_region_name(self):
        for rec in self:
            if rec.event_pincode:
                if not rec.country_id:
                    self.country_id = 104
                isha_pincode_center = self.env['isha.pincode.center'].search(
                    [('name', '=', rec.event_pincode),('countryiso_2', '=', self.country_id.code)])
                if len(isha_pincode_center) > 0:
                    isha_pincode_center = self.env['isha.pincode.center'].browse(isha_pincode_center.ids)[0]
                    if isha_pincode_center.region_name:
                        isha_region = self.env['isha.region'].search(
                            [('name', '=', isha_pincode_center.region_name)])[0]
                    else:
                        isha_region = self.env['isha.region'].search(
                            [('name', '=', 'Others')])[0]
                    isha_region = self.env['isha.region'].browse(isha_region.id)
                    if isha_region:
                        self.event_region_list = isha_region.id

class BayName(models.Model):
    _name = 'bay.name'
    _description = 'Bay Names'
    _rec_name = "bay_name"

    bay_name = fields.Char(string='Bay Name', required=True, translate=True)
    color = fields.Integer(string='Color Index')
    active = fields.Boolean(default=True,
                                help="The active field allows you to hide the category without removing it.")

class EventAttendees(models.Model):
    _name = 'event.attendees'
    _description = 'Event Attendees'
    _rec_name = "name"

    event_id = fields.Many2one(comodel_name='pr.events', string='Event Name',reuired=True)
    start_date = fields.Datetime(related="event_id.start_date")
    end_date = fields.Datetime(related="event_id.end_date")
    event_description = fields.Char(related="event_id.event_description")
    bay_name = fields.Many2one('bay.name', column1='pr_event_id', column2='bay_id',
                                    string='Bay Names')
    active = fields.Boolean(string="Active", default=True)
    attendance_notes = fields.Char(string='Attendance Notes')
    contact_id_fkey = fields.Many2one(string='Contact', comodel_name='res.partner', ondelete='set null',
                                          auto_join=True, delegate=True, index=True)
    attendees_name = fields.Char(related="contact_id_fkey.name", store=True)

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(EventAttendees, self).fields_get(allfields, attributes=attributes)
        if self._context.get('restrict_fields_get_list'):
            not_selectable_fields = set(res.keys()) - set(self._context.get('restrict_fields_get_list'))
            for field in not_selectable_fields:
                res[field]['selectable'] = False  # to hide in Add Custom filter view
                res[field]['sortable'] = False  # to hide in group by view
                # res[field]['exportable'] = False # to hide in export list
                res[field]['store'] = False  # to hide in 'Select Columns' filter in tree views
        return res

    @api.onchange('country_id')
    def _onchange_country_id(self):
        self.ensure_one()
        self.country = self.country_id.code

    @api.onchange('event_id')
    def _onchange_event_id(self):
        return {'domain': {'bay_name': [('id', 'in', self.event_id.bay_names.ids)]}}

    @api.constrains('bay_name')
    def _validate_bay_name(self):
        res = self.env['bay.name'].search([('id', 'in', self.event_id.bay_names.ids)])
        if len(res) == 0 and self.event_id.bay_names.ids != []:
            raise ValidationError('Bay Name is not present for event %s' % self.event_id.event_name)













