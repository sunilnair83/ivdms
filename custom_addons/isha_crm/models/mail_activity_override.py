import datetime
import hashlib
import hmac
import logging
import math
import multiprocessing
import random
import re
import threading
import time, os, copy
import requests
import boto3
import psutil
from botocore.exceptions import ClientError
import html2text
import traceback
from xmlrpc import client
import json
import odoorpc
import sendgrid
import werkzeug.urls
from odoo.addons.mail.models import mail_template
from sendgrid.helpers.mail import *
from werkzeug import urls
from odoo.addons.link_tracker.models.link_tracker import URL_REGEX

from odoo.tools import misc, plaintext2html
from odoo import _
from odoo import models, api, fields
from odoo import tools
from odoo.exceptions import UserError, ValidationError
from odoo.tools import config
from odoo.tools import ustr
from odoo.tools.safe_eval import safe_eval
from .constants import CHARITABLE_GROUPS, RELIGIOUS_GROUPS
from .email_marketing import dynamic_mail_res_partner_mapper, dynamic_txn_email_mapper
from ..GoldenContactAdv import GoldenContactAdv
from ..util.mail_util import MailUtil
from ...isha_base.configuration import Configuration
from odoo.http import request
from dateutil.relativedelta import relativedelta

_test_logger = logging.getLogger('odoo.tests')
logger = logging.getLogger('MailOverrides')
_logger = logging.getLogger(__name__)

class MailThreadOverride(models.AbstractModel):
	_inherit = 'mail.thread'


	# Audit log is passing self.env.user.partner_id.id as False.
	# Which is throwing error in query as cp.partner_id = false is not defined
	# Adding a valication check
	def _get_message_unread(self):
		partner_id = self.env.user.partner_id.id
		res = dict.fromkeys(self.ids, 0)
		if partner_id:
			if self.ids:
				# search for unread messages, directly in SQL to improve performances
				self._cr.execute(""" SELECT msg.res_id FROM mail_message msg
									 RIGHT JOIN mail_message_mail_channel_rel rel
									 ON rel.mail_message_id = msg.id
									 RIGHT JOIN mail_channel_partner cp
									 ON (cp.channel_id = rel.mail_channel_id AND cp.partner_id = %s AND
										(cp.seen_message_id IS NULL OR cp.seen_message_id < msg.id))
									 WHERE msg.model = %s AND msg.res_id = ANY(%s) AND
											msg.message_type != 'user_notification' AND
										   (msg.author_id IS NULL OR msg.author_id != %s) AND
										   (msg.message_type not in ('notification', 'user_notification') OR msg.model != 'mail.channel')""",
								 (partner_id, self._name, list(self.ids), partner_id,))
				for result in self._cr.fetchall():
					res[result[0]] += 1

			for record in self:
				record.message_unread_counter = res.get(record._origin.id, 0)
				record.message_unread = bool(record.message_unread_counter)
		else:
			for record in self:
				record.message_unread_counter = 0
				record.message_unread = False

	# Audit log is passing self.env.user.partner_id.id as False.
	# Which is throwing error in query as cp.partner_id = false is not defined
	# Adding a valication check
	def _get_message_needaction(self):
		res = dict.fromkeys(self.ids, 0)
		if self.env.user.partner_id.id:
			if self.ids:
				# search for unread messages, directly in SQL to improve performances
				self._cr.execute(""" SELECT msg.res_id FROM mail_message msg
									 RIGHT JOIN mail_message_res_partner_needaction_rel rel
									 ON rel.mail_message_id = msg.id AND rel.res_partner_id = %s AND (rel.is_read = false OR rel.is_read IS NULL)
									 WHERE msg.model = %s AND msg.res_id in %s AND msg.message_type != 'user_notification'""",
								 (self.env.user.partner_id.id, self._name, tuple(self.ids),))
				for result in self._cr.fetchall():
					res[result[0]] += 1

			for record in self:
				record.message_needaction_counter = res.get(record._origin.id, 0)
				record.message_needaction = bool(record.message_needaction_counter)
		else:
			for record in self:
				record.message_needaction_counter = 0
				record.message_needaction = False

	# Audit log is passing self.env.user.partner_id.id as False.
	# Which is throwing error in query as cp.partner_id = false is not defined
	# Adding a valication check
	def _compute_message_has_error(self):
		res = {}
		print(self.env.user)
		if self.env.user.partner_id.id:
			if self.ids:
				self._cr.execute(""" SELECT msg.res_id, COUNT(msg.res_id) FROM mail_message msg
									 RIGHT JOIN mail_message_res_partner_needaction_rel rel
									 ON rel.mail_message_id = msg.id AND rel.notification_status in ('exception','bounce')
									 WHERE msg.author_id = %s AND msg.model = %s AND msg.res_id in %s AND msg.message_type != 'user_notification'
									 GROUP BY msg.res_id""",
								 (self.env.user.partner_id.id, self._name, tuple(self.ids),))
				res.update(self._cr.fetchall())

			for record in self:
				record.message_has_error_counter = res.get(record._origin.id, 0)
				record.message_has_error = bool(record.message_has_error_counter)
		else:
			for record in self:
				record.message_has_error_counter = 0
				record.message_has_error = False


	def _compute_message_has_sms_error(self):
		res = {}
		if self.env.user.partner_id.id:
			if self.ids:
				self._cr.execute(""" SELECT msg.res_id, COUNT(msg.res_id) FROM mail_message msg
									 RIGHT JOIN mail_message_res_partner_needaction_rel rel
									 ON rel.mail_message_id = msg.id AND rel.notification_type = 'sms' AND rel.notification_status in ('exception')
									 WHERE msg.author_id = %s AND msg.model = %s AND msg.res_id in %s AND msg.message_type != 'user_notification'
									 GROUP BY msg.res_id""",
								 (self.env.user.partner_id.id, self._name, tuple(self.ids),))
				res.update(self._cr.fetchall())

			for record in self:
				record.message_has_sms_error = bool(res.get(record._origin.id, 0))
		else:
			for record in self:
				record.message_has_sms_error = False

class MailBlacklistOverride(models.Model):
	_inherit = "mail.blacklist"

	reason = fields.Selection([('hard_bounce', 'Hard Bounce'),('complaint', 'Complaint'),('manual','Manual'),('mig_unsubs','Migrated Unsubscription'),
							   ('whitelist','Allowed List'),('unknown', 'Unknown'),('tool','Blocklisted by Tool'),('unsubs_all','Unsubscribe All')],
							  string='Reason', index=True, tracking=True)

	def _add_rec(self, email, reason):
		record = self.env["mail.blacklist"].with_context(active_test=False).search([('email', '=', email)])
		if len(record) > 0:
			record.write({'active': True, 'reason':reason})
		else:
			record = self.create([{'email': email, 'reason':reason, 'active':True}])
		return record

	def _remove_rec(self):
		record = self.browse(self._context.get('active_ids'))
		if len(record) > 0:
			record.write({'active': False,'reason':'whitelist'})

		return self.env['partner.import.temp.wizard'].get_popup('Successfully moved '+str(len(record))+' emails out of blocked List.')


	@api.model_create_multi
	def create(self, values):
		# First of all, extract values to ensure emails are really unique (and don't modify values in place)
		new_values = []
		all_emails = []
		for value in values:
			email = tools.email_normalize(value.get('email'))
			if not email:
				raise UserError(_('Invalid email address %r') % value['email'])
			if email in all_emails:
				continue
			all_emails.append(email)
			new_value = dict(value, email=email)
			new_values.append(new_value)

		""" To avoid crash during import due to unique email, return the existing records if any """
		sql = '''SELECT email, id FROM mail_blacklist WHERE email = ANY(%s)'''
		emails = [v['email'] for v in new_values]
		self._cr.execute(sql, (emails,))
		bl_entries = dict(self._cr.fetchall())
		to_create = [v for v in new_values if v['email'] not in bl_entries]

		update_args = {'active': True}
		reason = values[0].get('reason',False)
		if reason:
			update_args['reason'] = reason
		self.env['mail.blacklist'].browse(bl_entries.values()).write(update_args)

		whilelisted = self.env['mail.blacklist'].browse(bl_entries.values()).filtered(lambda x: x.reason == 'whitelist')
		if len(whilelisted) > 0:
			whilelisted.write({'reason': reason or 'manual'})
		# TODO DBE Fixme : reorder ids according to incoming ids.
		results = super(MailBlacklistOverride, self).create(to_create)
		return self.env['mail.blacklist'].browse(bl_entries.values()) | results

class MailActivityOverride(models.Model):
	_inherit = 'mail.activity'

	@api.model
	def create(self, values):
		"""
		As center users are trying to log activity for center migration we may need sudo access to write to the contact
		as it may not belong to the current logged in user.
		"""
		activity = super(MailActivityOverride, self.sudo()).create(values)
		return activity

	def _get_athithi_users(self):
		partner = self.env['res.partner'].browse(self.env.context.get('default_res_id'))
		if partner and partner.influencer_type != False:
			if self.env.user.has_group('isha_crm.group_athithi_central_admin') or \
				self.env.user.has_group('isha_crm.group_athithi_central_viewer') or \
				self.env.user.has_group('isha_crm.group_athithi_regional_admin') or \
				self.env.user.has_group('isha_crm.group_athithi_regional_viewer') or \
				self.env.user.has_group('isha_crm.group_athithi_poc'):
				return ['|', '|', '|', '|', ('groups_id', '=', self.env.ref('isha_crm.group_athithi_central_admin').id),
						('groups_id', '=', self.env.ref('isha_crm.group_athithi_regional_admin').id),
						('groups_id', '=', self.env.ref('isha_crm.group_athithi_central_viewer').id),
						('groups_id', '=', self.env.ref('isha_crm.group_athithi_regional_viewer').id),
						('groups_id', '=', self.env.ref('isha_crm.group_athithi_poc').id)]

	user_id = fields.Many2one(
		# 'res.users', 'Assigned to',
		comodel_name='res.users',
		domain=_get_athithi_users,
		string="Assigned  To",
		default=lambda self: self.env.user,
		index=True, required=True)


class LinkTrackerOverride(models.Model):
	_inherit = "link.tracker.click"

	@api.model
	def add_click_custom(self, code, **route_values):
		""" Main API to add a click on a link. """
		tracker_code = self.env['link.tracker.code'].search([('code', '=', code)])
		if not tracker_code:
			return None

		# Removing IP dedup as the server is behind the WAF the incoming IP will always be the IP of WAF
		# ip = route_values.get('ip', False)
		# existing = self.search_count(['&', ('link_id', '=', tracker_code.link_id.id), ('ip', '=', ip)])
		# if existing:
		# 	return None

		route_values['link_id'] = tracker_code.link_id.id
		click_values = self._prepare_click_values_from_route(**route_values)

		return self.create(click_values)




class MailingTrace(models.Model):
	_inherit = 'mailing.trace'

	message_id = fields.Char(string='Message-ID', index=True)
	bounce_type = fields.Char(string="Bounce Type")
	bounce_sub_type = fields.Char(string="Bounce Sub Type")
	diagnostic_code = fields.Char(string="Diagnostic Code")
	res_id = fields.BigInt(string='Document ID')
	compliant_feedback_type = fields.Char(string="Compliant Feedback Type")
	complaint = fields.Datetime(string="Compliant")
	state = fields.Selection(compute="_compute_state",
							 selection=[('outgoing', 'Outgoing'),
										('exception', 'Exception'),
										('sent', 'Sent'),
										('opened', 'Opened'),
										('replied', 'Replied'),
										('bounced', 'Bounced'),
										('clicked', 'Clicked'),
										('ignored', 'Ignored')], store=True)

	@api.depends('sent', 'opened', 'clicked', 'replied', 'bounced', 'exception', 'ignored')
	def _compute_state(self):
		self.update({'state_update': fields.Datetime.now()})
		for stat in self:
			if stat.ignored:
				stat.state = 'ignored'
			elif stat.exception:
				stat.state = 'exception'
			elif stat.replied:
				stat.state = 'replied'
			elif stat.clicked:
				stat.state = 'clicked'
			elif stat.opened:
				stat.state = 'opened'
			elif stat.bounced:
				stat.state = 'bounced'
			elif stat.sent:
				stat.state = 'sent'
			else:
				stat.state = 'outgoing'

	def set_opened_custom(self, mass_mailing_id, res_id):
		traces = self.search([('mass_mailing_id', '=', mass_mailing_id), ('res_id', '=', res_id)])
		if len(traces) == 0:
			_logger.warning("######### Mail Trace out of order opened "+str(mass_mailing_id)+' '+str(res_id))
		else:
			traces.write({'opened': fields.Datetime.now(), 'bounced': False})
		return traces

	def set_clicked_custom(self,mass_mailing_id, res_id):
		traces = self.search([('mass_mailing_id', '=', mass_mailing_id), ('res_id', '=', res_id)])
		if len(traces) == 0:
			_logger.warning("######### Mail Trace out of order clicked "+str(mass_mailing_id)+' '+str(res_id))
		else:
			traces.write({'clicked': fields.Datetime.now(), 'opened': fields.Datetime.now()})
		return traces

	def set_bounced(self, mail_mail_ids=None, mail_message_ids=None):
		before = datetime.datetime.now()
		traces = super(MailingTrace, self).set_bounced(mail_mail_ids, mail_message_ids)
		contacts = None
		if traces and len(traces) == 1 and traces.email:
			contacts = self.env['res.partner'].search([('email', '=', traces.email)])
		if contacts:
			for contact in contacts:
				contact.write({'email_validity': 'bounced'})
				updated_emails = GoldenContactAdv.getEmailsDict(contact)
				contact.sudo().write(updated_emails)
		after = datetime.datetime.now()
		_logger.info("#######SET Bounced time: " + str(after - before))
		return traces

	def mark_click(self, url, ip):

		if 'mail/isha_mailing/' in url or 'mail/emailer/' in url:
			# Don't track unsubscribe as clicks
			if '/unsubscribe?' in url:
				return
			# For Manage subs and view emailer use a custom track url
			if '/manage?' in url:
				url = 'manage_subscription'
			if '/view?' in url:
				url = 'view_mailer'

		mailing_trace_record = self
		tracker = self.env['link.tracker'].sudo().search(
			[('url', '=', url), ('mass_mailing_id', '=', mailing_trace_record.mass_mailing_id.id)])
		if len(tracker) == 0:  # browse to handle the odoorpc
			tracker = self.env['link.tracker'].sudo().create({
				'mass_mailing_id': mailing_trace_record.mass_mailing_id.id,
				'url': url
			})
		self.env['link.tracker.click'].sudo().add_click_custom(
			tracker.code,
			ip= ip,
			country_code = False,
			mailing_trace_id = mailing_trace_record.id
		)

class LinkTrackerClickOverride(models.Model):
	_inherit = "link.tracker.click"

	actual_url = fields.Char(related='link_id.url')

class TestMassMailing(models.TransientModel):
	_inherit = 'mailing.mailing.test'

	mode = fields.Selection([('quick','Send'),('quick_dc','Send with Dynamic Content')], string='Send Mode')

	def send_mail_test(self):
		self.ensure_one()
		base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
		secret = self.env["ir.config_parameter"].sudo().get_param("database.secret")
		db_name = self.env.cr.dbname
		mailing = self.mass_mailing_id
		domain = safe_eval(mailing.mailing_domain)
		thread_name = 'Thread Mailing Test ' + str(mailing.id)
		try:
			mail_batch = self.env[mailing.mailing_model_real].search(domain,limit=1).ids
		except:
			raise UserError('Error in the Filter condition!!!')

		# turning off xmlrpc as it is not able to handle big int in materialized view
		# mode = 'quick_dc'
		# if mode == 'quick':
		# 	td = multiprocessing.Process(target=quick_send_process,
		# 								 args=(
		# 									 thread_name, mail_batch, mailing.mailing_model_real, mailing.id,
		# 									 mailing.template_id.body_html,
		# 									 mailing.email_from, mailing.subject, base_url, db_name, secret,self.email_to))
		# 	td.start()
		# 	time.sleep(1)
		# elif mode == 'quick_dc':
		emails = self.email_to.split(',')
		emails = [x.split('<')[1].replace('>','').lower() if '<' in x else x.lower() for x in emails]
		unsubs_rec = self.env['isha.mailing.unsubscriptions'].sudo().search([('unsubscription_email','in',emails),('subscription_id','=',mailing.dynamic_mailing_list.subscription_id.id)])
		if unsubs_rec:
			raise UserError(', '.join(unsubs_rec.mapped('unsubscription_email')) + " provided has an active unsubscription marked.\nPlz use a different test email.")


		# body_html = update_body_with_trackable_urls(self, mailing.template_id.body_html, mailing.id)
		body_html = mailing.template_id.body_html
		pre_header_val = (
				'<div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">' +
				(mailing.pre_header or '') + '</div>')
		body_html_mod = prepend_content_to_html(body_html, pre_header_val, False, False, False)
		email_subject = '[TEST MAIL] ' + (mailing.subject or '')
		for x in emails:
			td = multiprocessing.Process(target=mail_delivery_driver,
											args=(
											thread_name, mail_batch, mailing.mailing_model_real, mailing.id, body_html_mod,
											mailing.email_from, mailing.reply_to, email_subject, base_url,
											db_name, secret,mailing.dynamic_mailing_list.email_selection,
											False,x, 'localhost' in base_url,False))
			td.start()
			time.sleep(1)
		mailing.write({'testing_done':True})
		return True


class MailingTeams(models.Model):
	_name = 'isha.mailing.team'

	name = fields.Char(string='Team name')
	res_users = fields.Many2many('res.users', column1='team_id', column2='user_id', string='Mailing Team Members')

	def unlink(self):
		for rec in self:
			if (rec.id == self.env.ref('isha_crm.mailing_team_emedia').id or
					rec.id == self.env.ref('isha_crm.mailing_team_ipc').id):
				raise ValidationError('Cannot delete Emedia and IPC teams.')
		return super(MailingTeams, self).unlink()


class MassMailing(models.Model):
	_inherit = 'mailing.mailing'
	_rec_name = "mailing_name"
	_order = "create_date desc"

	MASS_MAILING_BUSINESS_MODELS = [
		'crm.lead',
		'event.registration',
		'hr.applicant',
		'res.partner',
		'event.track',
		'sale.order',
		'mailing.list',
		'mailing.contact',
		'partner.import.temp',
		'iec.mega.pgm','program.attendance','program.lead','sadhguru.exclusive','ieo.record',
		'isha.dynamic.mailing.lists',
		'programs.view.orm',
		'rudraksha.deeksha'
	]
	mailing_model_id = fields.Many2one('ir.model', string='Recipients Model',
									   domain=[('model', 'in', MASS_MAILING_BUSINESS_MODELS)],
									   default=lambda self: self.env.ref('mass_mailing.model_mailing_list').id)
	user_id = fields.Many2one('res.users', string='Responsible', tracking=True, default=lambda self: self.env.user, copy=False)

	@api.onchange('regions')
	def _get_default_mailing_team(self):
		teams = self.env.user.mailing_teams
		if teams:
			if self.env.ref('isha_crm.mailing_team_emedia').id in self.env.user.mailing_teams.ids:
				self.mailing_team = self.env.ref('isha_crm.mailing_team_emedia').id
			else:
				self.mailing_team = teams[0].id
		else:
			self.mailing_team = False

	mailing_team = fields.Many2one(comodel_name='isha.mailing.team', string='Team Sent For')

	@api.depends('mailing_team')
	def _compute_mailing_team_read_only(self):
		for rec in self:
			readonly = False if self.env.ref('isha_crm.mailing_team_emedia').id in self.env.user.mailing_teams.ids \
				else True
			rec.mailing_team_read_only = readonly

	mailing_team_read_only = fields.Boolean(store=False, compute=_compute_mailing_team_read_only)

	schedule_date =fields.Datetime(default=lambda self: fields.datetime.now())

	def _call_sqs_poller(self):
		base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
		if 'localhost' not in base_url:
			td = multiprocessing.Process(target=sqs_poller)
			td.start()
			time.sleep(1)
			_logger.info("Started SQS Poller....")

	def get_utc_schdule_date(self):
		for rec in self:
			if rec.schedule_date:
				rec.schedule_date_utc = str(rec.schedule_date)
			else:
				rec.schedule_date_utc = ''
	schedule_date_utc = fields.Char(compute=get_utc_schdule_date,string='Scheduled date ( UTC )')
	mailing_name = fields.Char(string='Email Campaign Name', required=True)
	pre_header = fields.Char(string='Pre-header')
	html_body_str = fields.Text(string='String HTML Source')
	state = fields.Selection([('draft', 'Draft'),  ('pending_approval', 'Pending Approval'),('pending_sending', 'Scheduled'),
							  ('sending', 'Sending'),('paused','Paused'), ('done', 'Sent'), ('blocked', 'Blocked'),
							  ('recurring', 'Recurring')],
							 string='Status',tracking=True, copy=False, default='draft',required=False,
							 group_expand='_group_expand_states')
	template_id = fields.Many2one('mail.template', string='Template', required=True,ondelete='restrict')

	dynamic_mailing_list = fields.Many2one('isha.dynamic.mailing.lists', string='Dynamic filter',ondelete='restrict')
	mailing_list_name = fields.Char(string='List name', related='dynamic_mailing_list.name')
	mailing_list_id = fields.Integer(string='List ID', related='dynamic_mailing_list.id')
	is_recurring_campaign = fields.Boolean(string='Recurring Campaign',copy=False)
	is_anniversaries = fields.Boolean(string='Anniversaries', copy=False)

	@api.depends('mailing_team')
	def _compute_is_recurring_campaign_read_only(self):
		for rec in self:
			if self.user_has_groups("isha_crm.group_mass_mailing_manager,isha_crm.group_mass_mailing_admin"):
				rec.is_recurring_campaign_read_only = False
			elif rec.mailing_team:
				if rec.mailing_team.id == self.env.ref('isha_crm.mailing_team_ipc').id:
					rec.is_recurring_campaign_read_only = False
				else:
					rec.is_recurring_campaign_read_only = True
			else:
				rec.is_recurring_campaign_read_only = True

	is_recurring_campaign_read_only = fields.Boolean(store=False, compute=_compute_is_recurring_campaign_read_only)
	trigger_field = fields.Many2one('ir.model.fields', string='Trigger Field')
	trigger_type = fields.Selection([('before','Before'),('after','After')],string='Trigger criteria')
	interval_number = fields.Integer(string='Interval')
	interval_type = fields.Selection([
		('hours', 'Hour(s)'),
		('days', 'Day(s)'),
		('weeks', 'Week(s)'),
		('months', 'Month(s)')], string='Interval Type')
	trigger_value = fields.Char(string='Trigger for ( UTC )',copy=False)

	def _compute_interval_standardized(self):
		factors = {'hours': 1,
				   'days': 24,
				   'weeks': 168,
				   'months': 720}

		return self.interval_number * factors[self.interval_type]

	parent_id = fields.Many2one('mailing.mailing', string='Parent Campaign', index=True, ondelete='cascade',copy=False)
	done_child_ids = fields.One2many('mailing.mailing', 'parent_id', string='Child Campaigns',domain=[('state','=','done')])
	child_ids = fields.One2many('mailing.mailing', 'parent_id', string='Child Campaigns',
								domain=[('state', '!=', 'done')])

	def generate_child_process(self):
		if not self.is_recurring_campaign or (not self.state == 'recurring' and not self.state == 'done'):
			raise UserError('Child process can be created only for recurring campaigns which are in Recurring/Sent state')

		if self.is_anniversaries:
			cur_date = datetime.datetime.utcnow().strftime('%Y-%m-%d')
			mailing_domain = eval(self.mailing_domain)
			if mailing_domain[0][0] == 'dynamic_date':
				mailing_domain[0] = ('dynamic_date', '=', cur_date)
			else:
				mailing_domain = str([('dynamic_date', '=', cur_date)] + mailing_domain)
			self.write({
				'mailing_domain': mailing_domain,
				'state': 'pending_sending',
			})
			# new_mailing.update_mail_template()
			self.env.cr.commit()
		else:
			field_name = self.trigger_field.name
			field_data_type = self.trigger_field.ttype

			from_clause, where_clause, where_clause_params = self.env[self.mailing_model_real].get_query_params(eval(self.dynamic_mailing_list.domain))
			where_str = where_clause and (" WHERE %s" % where_clause) or ''
			if field_data_type == 'date':
				where_str += ' and ' + field_name + ' >= \'%s\'' % datetime.datetime.utcnow().strftime('%Y-%m-%d')
			else:
				where_str += 'and ' + field_name + ' >= \'%s\'' % datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

			base_query = 'SELECT distinct(%s) as distinct_triggers FROM %s %s' % (field_name,from_clause,where_str)
			self.env.cr.execute(base_query,where_clause_params)
			if field_data_type == 'date':
				distinct_triggers = set([x['distinct_triggers'].strftime('%Y-%m-%d') for x in self.env.cr.dictfetchall() if x['distinct_triggers']])
			else:
				distinct_triggers = set([x['distinct_triggers'].strftime('%Y-%m-%d %H:%M:%S') for x in self.env.cr.dictfetchall() if x['distinct_triggers']])
			new_trigger_values = distinct_triggers - (set(self.child_ids.mapped('trigger_value')) | set(self.done_child_ids.mapped('trigger_value')))
			for value in new_trigger_values:
				if value:
					value = str(value)
					new_mailing = self.copy()
					if field_data_type == 'date':
						schedule_date = datetime.datetime.strptime(value,'%Y-%m-%d')
					else:
						schedule_date = datetime.datetime.strptime(value, '%Y-%m-%d %H:%M:%S')
					if self.trigger_type == 'before':
						schedule_date = schedule_date - datetime.timedelta(hours=self._compute_interval_standardized())
					else:
						schedule_date = schedule_date + datetime.timedelta(hours=self._compute_interval_standardized())

					new_mailing.write({
						'mailing_name': self.mailing_name + ' '+ value,
						'state':'pending_sending',
						'trigger_value': value,
						'schedule_date': schedule_date,
						'parent_id': self.id,
					})
					new_mailing.update_mail_template()
					self.env.cr.commit()

	@api.constrains('parent_id')
	def _check_parent_id(self):
		if not self._check_recursion():
			raise ValidationError(_('You can not create recursive Campaign.'))

	mailing_records_count = fields.Integer(string='Contact Count')
	dedup_stats_ids = fields.One2many('isha.mailing.dedup.stats','mailing_id',string='Dedup Stats')
	is_pgm_upd = fields.Boolean(string="Is Program Update?")

	testing_done = fields.Boolean(string='Done Testing', copy=False)
	load_testing = fields.Boolean(string='Load Testing',copy=False,default=False)
	link_tracker_ids = fields.One2many('link.tracker','mass_mailing_id', string='Link Trackers')
	link_track_count = fields.Integer(string='CTA', compute='_get_link_track_count')

	def _get_link_track_count(self):
		self.link_track_count = len(self.link_tracker_ids)

	def _get_default_threads(self):
		try:
			return int(self.sudo().env.ref('isha_crm.mass_mailing_threads').value)
		except Exception as ex:
			return 10

	process_ids = fields.Text(string='Process Ids')
	process_count = fields.Integer(string='No Of Threads',default=_get_default_threads,copy=False)
	process_info = fields.One2many('mass.mailing.process.info','mailing_id',string='Process Info')

	# @api.returns('self', lambda value: value.id)
	def copy(self, default=None):
		self.ensure_one()
		default = dict(default or {}, mailing_name=_('%s (copy)') % self.mailing_name)
		res = super(MassMailing, self).copy(default=default)
		return res

	def reconcile_process(self):
		if len(self.process_info.filtered(lambda x: x.pid in eval(self.process_ids)).mapped('pid')) == self.process_count:
			self.state = 'done'
			self.sent_date = datetime.datetime.now()
			try:
				# update the cron next call so that the next campaign can go out immediately
				cron = self.env.ref('mass_mailing.ir_cron_mass_mailing_queue')
				if cron.nextcall - datetime.datetime.utcnow() > datetime.timedelta(minutes=2):
					_logger.info('Email marketing Auto update Cron time for immediate trigger')
					cron.write(
						{
							'nextcall':datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=30),'%Y-%m-%d %H:%M:%S')
						}
					)
			except:
				_logger.info('ERROR: Email marketing Auto update Cron time for immediate trigger')
				pass
		return self.ids

	def action_block(self):
		self.action_stop_campaign()
		if self.state == 'recurring' and self.child_ids:
			for child in self.child_ids:
				child.action_stop_campaign()
			self.child_ids.write({'state': 'blocked'})
		self.write({'state': 'blocked'})

	def action_start_recurring_campaign(self):
		self.write({'state': 'recurring'})

	def action_approve(self):
		if self.mailing_records_count == 0:
			raise UserError('The campaign does not have any contact to send the mailer. Plz check the Dedup breakup for more info.')

		self.write({'state': 'pending_sending'})
		if self.dynamic_mailing_list.list_status != 'used':
			self.dynamic_mailing_list.write({'list_status':'used'})

		if self.schedule_date and self.schedule_date > fields.Datetime.now():
			return self.env['sh.message.wizard'].get_popup('Campaign successfully scheduled.')
		else:
			# Manual triggering is causing serialization error
			# So just allow the scheduler to pick it up
			return self.env['sh.message.wizard'].get_popup('Campaign successfully scheduled.')

			# try:
			# 	cron = self.env.ref('mass_mailing.ir_cron_mass_mailing_queue')
			# 	cron.method_direct_trigger()
			# 	return self.env['sh.message.wizard'].get_popup('Campaign successfully scheduled.')
			# except Exception as ex:
			# 	return self.env['sh.message.wizard'].get_popup('Campaign successfully scheduled.')


	def action_move_to_draft(self):
		self.write({'state': 'draft'})
		list_used_count = self.search([('dynamic_mailing_list','=',self.dynamic_mailing_list.id),('id','!=',self.id),('state','!=','draft')],count=True)
		if list_used_count == 0:
			self.dynamic_mailing_list.write({'list_status':'draft'})
		return self.env['sh.message.wizard'].get_popup('Campaign successfully updated.')



	def action_pending_approval(self):
		if not self.dedup_stats_ids.ids:
			raise UserError('Dedup Process is not done. Plz run the process atleast once to cross check the data.')
		if self.mailing_records_count == 0:
			raise UserError('The campaign does not have any contact to send the mailer. Plz check the Dedup breakup for more info.')

		self.write({'state': 'pending_approval'})
		if self.dynamic_mailing_list.list_status != 'used':
			self.dynamic_mailing_list.write({'list_status':'used'})
		return self.env['sh.message.wizard'].get_popup('Campaign successfully updated.')

	def action_stop_campaign(self):
		if self.process_ids:
			for x in eval(self.process_ids):
				if psutil.pid_exists(x):
					process = psutil.Process(x)
					if process.is_running():
						process.terminate()
						time.sleep(1)
			self.write({'state':'paused'})
			return self.env['sh.message.wizard'].get_popup('Campaign successfully Paused.')
		else:
			return self.env['sh.message.wizard'].get_popup('Campaign does not have associated process.')


	def get_regions(self):
		region_ids = self.env.user.centers.mapped('region_id').ids
		region_ids = list(set(region_ids))
		return [(6, 0, region_ids)]
	regions = fields.Many2many('isha.region', 'mailing_mailing_region_rel', string='Region', default=get_regions)


	whitelisted_email_from = fields.Many2one('isha.whitelist.domain', string='Email From')
	email_from = fields.Char(string='Send From string')

	@api.onchange('whitelisted_email_from')
	def populate_email_from(self):
		if self.whitelisted_email_from:
			self.email_from = self.whitelisted_email_from.email_formatted

	whitelisted_reply_to = fields.Many2one('isha.whitelist.domain', string='Reply-To')
	reply_to = fields.Char(string='Reply To string')
	@api.onchange('whitelisted_reply_to')
	def populate_reply_to(self):
		if self.whitelisted_reply_to:
			self.reply_to = self.whitelisted_reply_to.email_formatted

	suppression_campaign_list = fields.Many2many('mailing.mailing', relation='mailings_suppression_lists_rel',
												 column1='mailing_id', column2='suppression_id',
												 string='Campaign based Suppression',
												 domain=[('state', '=', 'done')])

	re_eng_suppression_campaign_list = fields.Many2many('mailing.mailing', relation='re_eng_mailings_suppression_lists_rel',
												 column1='mailing_id',column2='suppression_id', string='Campaigns',
												 domain="[('state','=','done'), ('dynamic_mailing_list', '=', dynamic_mailing_list)]")

	list_subset = fields.Selection([('not_sent', 'Not Sent'), ('not_opened', 'Not Sent or Not Opened'),
									('sent_but_not_opened', 'Sent but Not Opened'), ('opened', 'Opened'),
									('clicked', 'Clicked'), ('not_clicked', 'Opened but Not Clicked'),
									('not_clicked_all', 'Not Clicked')],
								   string="List sub-set")

	subset_combination = fields.Selection([('and', 'ALL Campaigns'), ('or', 'At least one Campaign')],
								   string="Combination")
	suppression_mailing_list = fields.Many2many('isha.dynamic.mailing.lists',relation='ml_suppression_lists_rel',column1='mailing_id',column2='ml_list_id', string='List based Suppression')

	def show_mail_traces(self):
		local_context = dict(
			self.env.context,
		)
		return {
			'name': 'Mail Traces',
			'type': 'ir.actions.act_window',
			'view_mode': 'tree,form',
			'domain': [('mass_mailing_id','=',self.id)],
			'view_id': False,
			'views': [(False, 'tree'), (False, 'form'),(False, 'graph'),(False,'pivot')],
			'res_model': 'mailing.trace',
			'target': 'current',
			'context': local_context
		}

	mailing_start_time = fields.Datetime(compute='_compute_statistics', string='Mailing Start Time')
	mailing_end_time = fields.Datetime(compute='_compute_statistics', string='Mailing End Time')
	mailing_time_taken = fields.Char(compute='_compute_statistics',string='Time Taken')
	mailing_time_taken_str = fields.Char(compute='_compute_statistics',string='Time Taken')

	unsubscription_ids = fields.One2many('isha.mailing.unsubscriptions','mailing_id')

	unsubs_count = fields.Integer(string='UnSubscription',compute='_compute_statistics')
	unsubs_ratio = fields.Float(string='UnSubscription Ratio',compute='_compute_statistics')
	unsubs_ratio_str = fields.Char(string='UnSubscription Ratio', compute='_compute_statistics')
	re_subs_count = fields.Integer(string='ReSubscription',compute='_compute_statistics')
	re_subs_ratio = fields.Float(string='ReSubscription Ratio',compute='_compute_statistics')
	re_subs_ratio_str = fields.Char(string='ReSubscription Ratio',compute='_compute_statistics')

	clicks_ratio = fields.Float(compute="_compute_statistics", string="Clicks Ratio")
	clicks_ratio_str = fields.Char(compute="_compute_statistics", string="Clicks Ratio")
	received_ratio = fields.Float(compute="_compute_statistics", string='Received Ratio')
	received_ratio_str = fields.Char(compute="_compute_statistics", string='Received Ratio')
	opened_ratio = fields.Float(compute="_compute_statistics", string='Opened Ratio')
	opened_ratio_str = fields.Char(compute="_compute_statistics", string='Opened Ratio')
	replied_ratio = fields.Float(compute="_compute_statistics", string='Replied Ratio')
	replied_ratio_str = fields.Char(compute="_compute_statistics", string='Replied Ratio')
	bounced_ratio = fields.Float(compute="_compute_statistics", string='Bounced Ratio')
	bounced_ratio_str = fields.Char(compute="_compute_statistics", string='Bounced Ratio')

	complaint = fields.Integer(string='Complaints',compute='_compute_statistics')
	complaint_ratio = fields.Float(string='Complaints ratio',compute='_compute_statistics')
	complaint_ratio_str = fields.Char(string='Complaints ratio',compute='_compute_statistics')

	hard_bounce = fields.Integer(string='Hard Bounce',compute='_compute_statistics')
	hard_bounce_ratio = fields.Float(string='Hard Bounce Ratio',compute='_compute_statistics')
	hard_bounce_ratio_str = fields.Char(string='Hard Bounce Ratio',compute='_compute_statistics')
	soft_bounce = fields.Integer(string='Soft Bounce',compute='_compute_statistics')
	soft_bounce_ratio = fields.Float(string='Soft Bounces Ratio',compute='_compute_statistics')
	soft_bounce_ratio_str = fields.Char(string='Soft Bounces Ratio',compute='_compute_statistics')
	ctor_ratio = fields.Float(string="CTOR",compute='_compute_statistics')
	ctor_ratio_str = fields.Char(string="CTOR",compute='_compute_statistics')

	def _compute_statistics(self):
		""" Compute statistics of the mass mailing """
		unsubs_dict = dict()
		self.env.cr.execute("""
			SELECT
				m.id as mailing_id,
				COUNT(CASE WHEN s.active = true THEN 1 ELSE null END) AS unsubs_count,
				COUNT(CASE WHEN s.active = false THEN 1 ELSE null END) AS re_subs_count
			FROM
				isha_mailing_unsubscriptions s
			RIGHT JOIN
				mailing_mailing m
				ON (m.id = s.mailing_id)
			WHERE
				m.id IN %s
			GROUP BY
				m.id
		""", (tuple(self.ids),))
		for row in self.env.cr.dictfetchall():
			unsubs_dict[row['mailing_id']] = row
		self.env.cr.execute("""
			SELECT
				m.id as mailing_id,
				COUNT(s.id) AS expected,
				m.mailing_records_count AS mailing_records_count,
				COUNT(CASE WHEN s.sent is not null THEN 1 ELSE null END) AS sent,
				COUNT(CASE WHEN s.scheduled is not null AND s.sent is null AND s.exception is null AND s.ignored is null AND s.bounced is null THEN 1 ELSE null END) AS scheduled,
				COUNT(CASE WHEN s.scheduled is not null AND s.sent is null AND s.exception is null AND s.ignored is not null THEN 1 ELSE null END) AS ignored,
				COUNT(CASE WHEN s.sent is not null AND s.exception is null AND s.bounced is null THEN 1 ELSE null END) AS delivered,
				COUNT(CASE WHEN s.opened is not null THEN 1 ELSE null END) AS opened,
				COUNT(CASE WHEN s.clicked is not null THEN 1 ELSE null END) AS clicked,
				COUNT(CASE WHEN s.replied is not null THEN 1 ELSE null END) AS replied,
				COUNT(CASE WHEN s.bounced is not null THEN 1 ELSE null END) AS bounced,
				COUNT(CASE WHEN s.bounce_type = 'Permanent' THEN 1 ELSE null END) AS hard_bounce,
				COUNT(CASE WHEN s.bounce_type is not null and s.bounce_type != 'Permanent' THEN 1 ELSE null END) AS soft_bounce,
				COUNT(CASE WHEN s.exception is not null THEN 1 ELSE null END) AS failed,
				COUNT(CASE WHEN s.complaint is not null THEN 1 ELSE null END) AS complaint,
				max(s.create_date) AS mailing_end_time,
				min(s.create_date) AS mailing_start_time
			FROM
				mailing_trace s
			RIGHT JOIN
				mailing_mailing m
				ON (m.id = s.mass_mailing_id)
			WHERE
				m.id IN %s
			GROUP BY
				m.id
		""", (tuple(self.ids),))
		for row in self.env.cr.dictfetchall():
			total = (row.pop('mailing_records_count')) or 1
			row['received_ratio'] = round(100.0 * row['delivered'] / total, 3)
			row['opened_ratio'] = round(100.0 * row['opened'] / total, 3)
			row['clicks_ratio'] = round(100.0 * row['clicked'] / total, 3)
			row['replied_ratio'] = round(100.0 * row['replied'] / total, 3)
			row['bounced_ratio'] = round(100.0 * row['bounced'] / total, 3)
			row['complaint_ratio'] = round(100.0 * row['complaint'] / total, 3)
			row['hard_bounce_ratio'] = round(100.0 * row['hard_bounce'] / total, 3)
			row['soft_bounce_ratio'] = round(100.0 * row['soft_bounce'] / total, 3)
			row['unsubs_count'] = unsubs_dict[row['mailing_id']]['unsubs_count']
			row['unsubs_ratio'] = round(100.0 * row['unsubs_count'] / total, 3)
			row['re_subs_count'] = unsubs_dict[row['mailing_id']]['re_subs_count']
			row['re_subs_ratio'] = round(100.0 * row['re_subs_count'] / total, 3)
			row['ctor_ratio'] = round(100 * row['clicked'] / row['opened'], 3) if row['clicked'] > 0  and row['opened'] > 0 else 0
			if row['mailing_start_time'] and row['mailing_end_time']:
				time_taken = row['mailing_end_time'] - row['mailing_start_time']
				hours, remainder = divmod(time_taken.total_seconds(), 3600)
				minutes, seconds = divmod(remainder, 60)
				row['mailing_time_taken'] = '%s hour(s) %s minute(s) %s second(s)' % (int(hours), int(minutes),int(seconds))
				row['mailing_time_taken_str'] = '%s:%s:%s' % (int(hours), int(minutes),int(seconds))
			else:
				row['mailing_time_taken'] = 'Not Started Yet'
				row['mailing_time_taken_str'] = 'Not Started Yet'
			for key in list(row):
				if key.endswith('_ratio'):
					new_key = "{0}_str".format(key)
					row[new_key] = "{0}%".format(row[key])
			self.browse(row.pop('mailing_id')).update(row)

	# @profile('/tmp/prof.profile')
	# @api.model
	# def _process_mass_mailing_queue(self):
	# 	before = datetime.datetime.now()
	# 	super(MassMailing, self)._process_mass_mailing_queue()
	# 	after = datetime.datetime.now()
	# 	_logger.info("######Profile time: " + str(after - before))

	@api.model
	def _process_mass_mailing_queue(self):
		# Ensure that no campaign is in sending state
		active_campaign = self.with_context(skip_checks=True).search([('state', '=', 'sending')], limit=1)
		if len(active_campaign) == 0:
			mass_mailing = self.with_context(skip_checks=True).search(
				[('state', 'in', ['pending_sending']), '|', ('schedule_date', '<', fields.Datetime.now()),
				 ('schedule_date', '=', False)],limit=1)
			if mass_mailing:
				user = mass_mailing.write_uid or self.env.user
				mass_mailing = mass_mailing.with_context(**user.with_user(user).context_get())
				mass_mailing.write({'state': 'sending'})
				self.env.cr.commit()
				mass_mailing.action_quick_send_odoorpc()
		else:
			active_campaign.reconcile_process()

	@api.onchange('dynamic_mailing_list')
	def populate_model_n_domain(self):
		if self.dynamic_mailing_list:
			if self.dynamic_mailing_list.model_model == 'mailing.list':
				mailing_contacts = self.env[self.dynamic_mailing_list.model.model].sudo().search(eval(self.dynamic_mailing_list.domain)).mapped('contact_ids').filtered(lambda x: x.opt_out is False and x.is_blacklisted is False)
				self.mailing_model_id = self.env['ir.model'].sudo().search([('model','=','mailing.contact')]).id
				self.mailing_domain = [('id','in',mailing_contacts.ids)]
				self.regions = [(6, 0, self.dynamic_mailing_list.regions.ids)]
				self.mailing_records_count = len(mailing_contacts)

			else:
				self.mailing_model_id = self.dynamic_mailing_list.model.id
				self.regions = [(6, 0, self.dynamic_mailing_list.regions.ids)]
				self.mailing_records_count = 0
				# reset the template as the change of mailing list could be to a different model
				self.template_id = False
				# rec.mailing_records_count = self.env[rec.dynamic_mailing_list.model.model].sudo().search(eval(rec.mailing_domain),count=True)

			return {
				'domain': {'template_id': [('model_id', '=', self.dynamic_mailing_list.model.id)],
						   'recurring_field': [('model_id', '=', self.dynamic_mailing_list.model.id),('ttype', 'in', ['datetime','date'])],
						   'suppression_list': [('state', '=', 'done')]}
			}

	def get_popup(self,stats_rec):
		if stats_rec:
			view = self.env.ref('isha_crm.isha_mailing_dedup_stats')
			view_id = view and view.id or False
			context = dict(self._context or {})
			return {
				'name': 'Mailing List Breakup',
				'type': 'ir.actions.act_window',
				'view_type': 'form',
				'view_mode': 'form',
				'res_model': 'isha.mailing.dedup.stats',
				'res_id':stats_rec.id,
				'views': [(view_id, 'form')],
				'view_id': view_id,
				'target': 'current',
				'context': context
			}

	def compute_contact_count(self):
		if self.state in ['draft','pending_approval','pending_sending']:
			start = datetime.datetime.now()
			_logger.info('########## duplicate removal start for ' + self.mailing_name)
			mailing_stats = self.dedup_handler_optimized(eval(self.mailing_domain), True)
			_logger.info('########## duplicate removal end for ' + self.mailing_name)
			end = datetime.datetime.now()
			if mailing_stats:
				self.mailing_records_count = mailing_stats['deduped_count']
				time_taken = end - start
				hours, remainder = divmod(time_taken.total_seconds(), 3600)
				minutes, seconds = divmod(remainder, 60)
				time_taken_str = '%s hour(s) %s minute(s) %s second(s)' % (int(hours), int(minutes),int(seconds))
				mailing_stats['time_taken'] = time_taken_str
				stats_rec = self.env['isha.mailing.dedup.stats'].search([('mailing_id','=',mailing_stats['mailing_id'])])
				if stats_rec:
					stats_rec.sudo().write(mailing_stats)
				else:
					stats_rec = self.env['isha.mailing.dedup.stats'].sudo().create([mailing_stats])

				return self.get_popup(stats_rec)
		else:
			return self.get_popup(self.env['isha.mailing.dedup.stats'].search([('mailing_id','=',self.id)]))


	def show_dedup_stats_direct(self):
		return self.get_popup(self.env['isha.mailing.dedup.stats'].search([('mailing_id', '=', self.id)]))

	def show_selected_recs(self):
		if self.dynamic_mailing_list:
			return self.dynamic_mailing_list.show_selected_recs()

	def _get_domain_allowed_groups(self):
		allowed_group_list = [self.env.ref('isha_crm.group_santosha').id,
						self.env.ref('isha_crm.group_sangam').id,
						self.env.ref('isha_crm.group_athithi_central_admin').id,
						self.env.ref('isha_crm.group_isha_vidhya').id,
						# self.env.ref('isha_ims.group_ims').id
						]
		allowed_set = set(allowed_group_list).intersection(set(self.env.user.groups_id.ids))
		if len(allowed_set) > 0:
			return [(6, 0, [x for x in allowed_set])]
		else:
			return [(6, 0, [self.env.ref('base.group_user').id])]

	allowed_groups = fields.Many2many('res.groups', 'res_groups_mailing_rel', 'mass_mailing_id', 'gid',
									  string='Allowed groups', default=_get_domain_allowed_groups)

	def _group_expand_states(self, states, domain, order):
		return ['draft', 'pending_approval', 'sending', 'done', 'pending_sending', 'recurring', 'paused', 'blocked']

	@api.model
	def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
		context = self._context or {}
		if context.get('region_scope'):
			domain += [('regions', 'in', self.env.user.centers.mapped('region_id').ids)]
		if context.get('group_scope'):
			domain += [('allowed_groups', 'in', self.env.user.groups_id.ids)]
		if context.get('team_scope'):
			if not self.env.user.mailing_teams.ids:
				domain += [('mailing_team', '=', False)]
			elif self.env.ref('isha_crm.mailing_team_emedia').id not in self.env.user.mailing_teams.ids:
				domain += ['|', ('mailing_team', '=', False), ('mailing_team', 'in', self.env.user.mailing_teams.ids)]
		return super(MassMailing, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
														   orderby=orderby, lazy=lazy)

	@api.model
	def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
		context = self._context or {}
		if not context.get('skip_checks',False):
			if context.get('region_scope'):
				args += [('regions', 'in', self.env.user.centers.mapped('region_id').ids)]
			if context.get('group_scope'):
				args += [('allowed_groups', 'in', self.env.user.groups_id.ids)]
			if context.get('team_scope'):
				if not self.env.user.mailing_teams.ids:
					args += [('mailing_team', '=', False)]
				elif self.env.ref('isha_crm.mailing_team_emedia').id not in self.env.user.mailing_teams.ids:
					args += ['|',('mailing_team', '=', False),('mailing_team', 'in', self.env.user.mailing_teams.ids)]
		return super(MassMailing, self)._search(args, offset, limit, order, count=count,
														access_rights_uid=access_rights_uid)

	@api.model
	def create(self, values):
		if 'dynamic_mailing_list' in values and 'template_id' in values:
			mailing_lists = self.env['isha.dynamic.mailing.lists'].search([('id', '=', values['dynamic_mailing_list'])])
			templates = self.env['mail.template'].search([('id', '=', values['template_id'])])
			if mailing_lists and templates and mailing_lists[0].subscription_id.is_pgm_update != templates[0].is_pgm_upd:
				raise UserError('Operation Denied !!!. \n For program update campaigns, the template and the category'
								' must both have the program update flag enabled.')
		return_value = super(MassMailing, self).create(values)
		return return_value

	def write(self, values):
		return_value = super(MassMailing, self).write(values)
		return return_value

	def unlink(self):
		for rec in self:
			if rec.state not in ['draft', 'pending_approval', 'pending_sending']:
				raise UserError('Operation Denied !!!. \n Mailing cannot be deleted once it has moved out of draft stage.')
		return super(MassMailing, self).unlink()

	@api.onchange('template_id')
	def update_mail_template(self):
		if self.template_id:
			string = self.template_id.body_html
			if self.mailing_model_real == 'isha.mailing.contact':
				if 'manage_subscription' in string and (string[string.index('manage_subscription')-1].isalnum() or string[string.index('manage_subscription')+len('manage_subscription')].isalnum()):
					raise UserError('Manage Subscription is not supported for Double Opt-in (Isha Mailing Contacts).\n'
										'Plz remove \n href="manage_subscription" from the email template.')
				if 'unsubscribe_from_list' not in string or (string[string.index('unsubscribe_from_list')-1].isalnum() or string[string.index('unsubscribe_from_list')+len('unsubscribe_from_list')].isalnum()):
					raise UserError('Template does not have un-subscription link.\n\n '
									'Plz include \n\nhref="unsubscribe_from_list" for Un-subscription link')
			elif ('manage_subscription' in string and (string[string.index('manage_subscription')-1].isalnum() or string[string.index('manage_subscription')+len('manage_subscription')].isalnum())) \
				or ('unsubscribe_from_list' in string and (string[string.index('unsubscribe_from_list')-1].isalnum() or string[string.index('unsubscribe_from_list')+len('unsubscribe_from_list')].isalnum())) \
				or 'unsubscribe_from_list' not in string:
				# or 'manage_subscription' not in self.template_id.body_html:
				raise UserError('Template does not have un-subscription / manage-subscription link.\n\n '
								'Plz include \n\nhref="unsubscribe_from_list" for Un-subscription link\n'
								'href="manage_subscription" for Manage-Subscription link.')
			if self.body_html != self.template_id.body_html:
				self.body_html = self.template_id.body_html
				self.body_arch = self.template_id.body_html
			self.is_pgm_upd = self.template_id.is_pgm_upd

			email_selection = self.dynamic_mailing_list.email_selection
			if self.template_id.is_pgm_upd and self.mailing_model_real not in ['res.partner', 'mailing.list']:
				email_selection = 'txn'

			if email_selection == 'contact':
				email_field = 'email'
			else:
				email_field = dynamic_txn_email_mapper[self.mailing_model_real]

			self.mailing_domain = eval(self.dynamic_mailing_list.domain) + self.get_default_domain(self.template_id.is_pgm_upd,email_field)
			self.testing_done = False

	#suri: check which user's privileages to use. the creator of template/mailing list/campaign
	#
	def get_default_domain(self, is_pgm_upd,email_field):
		if self.mailing_model_real == 'isha.mailing.contact':
			if self.parent_id and self.trigger_value:
				return [(self.trigger_field.name, '=', self.trigger_value)]
			return []
		default_domain = [[email_field, "!=", False]]
		if not is_pgm_upd:
			default_domain += [["dnd_email", "!=", True],
						  ["deceased", "=", False], ["influencer_type", "=", False],
						  '!', '&', ("is_caca_donor", "=", True), '|', ("last_txn_date", "=", "1900-01-01"),
						  ("last_txn_date", "=", False)]
		if self.parent_id and self.trigger_value:
			default_domain += [(self.trigger_field.name,'=',self.trigger_value)]
		return default_domain

	def action_quick_send(self):
		base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
		secret = self.env["ir.config_parameter"].sudo().get_param("database.secret")
		db_name = self.env.cr.dbname
		res_ids = self._get_remaining_recipients()
		self.write({'state': 'done', 'sent_date': fields.Datetime.now(),'mailing_records_count':len(res_ids)})
		split_size = math.ceil(len(res_ids) / 10) if len(res_ids) > 10 else 10
		thread_id = 0
		for mail_batch in tools.split_every(split_size, res_ids):
			thread_name = 'Thread ' + str(thread_id)
			td = multiprocessing.Process(target=quick_send_process,
										 args=(
											 thread_name, mail_batch, self.mailing_model_real, self.id, self.template_id.body_html,
											 self.email_from, self.subject, base_url, db_name, secret))
			td.start()
			time.sleep(1)
			thread_id += 1

	def trigger_load_testing(self):
		if self.load_testing:
			self.action_quick_send_odoorpc()


	def action_quick_send_odoorpc(self):
		base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
		secret = self.env["ir.config_parameter"].sudo().get_param("database.secret")

		db_name = self.env.cr.dbname
		res_ids = self._get_remaining_recipients()
		if len(res_ids) == 0:
			try:
				self.write({'state': 'done', 'sent_date': fields.Datetime.now()})
				self.env.cr.commit()
			except Exception as ex:
				time.sleep(2)
				tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
				_logger.error("ERROR: EMAIL MARKETING write error\n" + str({'state': 'done', 'sent_date': fields.Datetime.now()})
							  + "\n" + tb_ex)

				td = multiprocessing.Process(target=update_mailing_daemon,
											 args=(self.id, {'state': 'done', 'sent_date': fields.Datetime.now()}))
				td.start()
			return

		no_of_recs = len(res_ids) + self.expected if self.expected > 0 else len(res_ids)

		email_selection = self.dynamic_mailing_list.email_selection
		if self.is_pgm_upd and self.mailing_model_real not in ['res.partner','mailing.list']:
			email_selection = 'txn'

		if email_selection == 'contact':
			email_field = 'email'
		else:
			email_field = dynamic_txn_email_mapper[self.mailing_model_real]

		split_size = math.ceil(len(res_ids) / self.process_count) if len(res_ids) > self.process_count else self.process_count
		process_id = 0

		# body_html = update_body_with_trackable_urls(self, self.template_id.body_html, self.id)
		body_html = self.template_id.body_html
		pre_header_val = (
				'<div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">' +
				(self.pre_header or '') + '</div>')
		body_html_mod = prepend_content_to_html(body_html, pre_header_val, False, False, False)

		process_ids = []
		for mail_batch in tools.split_every(split_size, res_ids):
			thread_name = 'Thread ' + str(process_id)
			td = multiprocessing.Process(target=mail_delivery_driver,
										 args=(thread_name, mail_batch, self.mailing_model_real, self.id, body_html_mod,
											   self.email_from, self.reply_to,self.subject, base_url,
											   db_name, secret,email_selection,email_field, False,
											   'localhost' in base_url or self.load_testing,False))
			td.start()
			process_ids.append(td.pid)
			time.sleep(1)
			process_id += 1

		# Serialization error happening becoz of the compute fields of mailing_mailing when someone opens the tree view
		# which is preventing the process ids to be written to db
		# Adding threaded retry as the scheduler cron rolls back the transaction even for serialization error
		try:
			self.write({'mailing_records_count': no_of_recs, 'state': 'sending',
						'process_ids':process_ids,'process_count':len(process_ids)})
			self.env.cr.commit()
		except Exception as ex:
			tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
			_logger.error("ERROR: EMAIL MARKETING process_ids write error\n"+str({'id':self.id,'process_ids':process_ids})+"\n"+tb_ex)
			time.sleep(5)
			update_dict = {'mailing_records_count': no_of_recs, 'process_ids': process_ids, 'state': 'sending',
							'process_count': len(process_ids)}
			td = multiprocessing.Process(target=update_mailing_daemon,
										 args=(self.id, update_dict))
			td.start()

	def get_mailing_model_name(self, values):
		mailing_model_name = None
		if 'mailing_model_id' in values:
			model = self.env['ir.model'].search([('id', '=', values.get('mailing_model_id'))])
			if model:
				mailing_model_name = model[0].model
		else:
			mailing = self.env['mailing.mailing'].search([('id', '=', self.id)])
			if mailing:
				mailing_model_name = mailing[0].mailing_model_name
		return mailing_model_name

	def get_mailing_domain(self, values):
		mailing_domain = None
		if 'mailing_domain' in values:
			mailing_domain = values.get('mailing_domain')
		else:
			mailing = self.env['mailing.mailing'].search([('id', '=', self.id)])
			if mailing:
				mailing_domain = mailing[0].mailing_domain
		return mailing_domain

	def action_send_mail(self, res_ids=None):
		author_id = self.env.user.partner_id.id

		for mailing in self:
			if not res_ids:
				res_ids = mailing._get_remaining_recipients()
			if not res_ids:
				raise UserError(_('There are no recipients selected.'))

			composer_values = {
				'author_id': author_id,
				'attachment_ids': [(4, attachment.id) for attachment in mailing.attachment_ids],
				'body': mailing.body_html,
				'subject': mailing.subject,
				'model': mailing.mailing_model_real,
				'email_from': mailing.email_from,
				'record_name': False,
				'composition_mode': 'mass_mail',
				'mass_mailing_id': mailing.id,
				'mailing_list_ids': [(4, l.id) for l in mailing.contact_list_ids],
				'no_auto_thread': mailing.reply_to_mode != 'thread',
				'template_id': None,
				'mail_server_id': mailing.mail_server_id.id,
			}

			if mailing.reply_to_mode == 'email':
				composer_values['reply_to'] = mailing.reply_to

			before = datetime.datetime.now()
			composer = self.env['mail.compose.message'].with_context(active_ids=res_ids).create(composer_values)
			after = datetime.datetime.now()
			_logger.info("#######Composer CREATE time: " + str(after - before))

			extra_context = self._get_mass_mailing_context()
			composer = composer.with_context(active_ids=res_ids, **extra_context)
			# auto-commit except in testing mode
			auto_commit = not getattr(threading.currentThread(), 'testing', False)

			before = datetime.datetime.now()
			composer.send_mail(auto_commit=auto_commit)
			after = datetime.datetime.now()
			_logger.info("#######Composer SEND time: " + str(after - before))

			mailing.write({'state': 'done', 'sent_date': fields.Datetime.now()})
		return True

	def _get_remaining_recipients(self):
		before = datetime.datetime.now()
		ids = super(MassMailing, self.sudo())._get_remaining_recipients()
		after = datetime.datetime.now()
		_logger.info("####### Mailing Calculating remaining recipients time: " + str(after - before))
		_logger.info("#######Items to QUEUE: " + str(len(ids)))
		return ids

	def get_unsubscription_emails_for_category(self,from_clause,where_str,where_params,email_selection,email_field):
		subscription_id = self.dynamic_mailing_list.subscription_id.id
		if not subscription_id:
			raise UserError('Subscription Category is not present in Mailing List.')

		from_clause_unsubs = copy.deepcopy(from_clause) +' , isha_mailing_unsubscriptions imu'
		where_str_unsubs = copy.deepcopy(where_str) + ' and imu.active=true and imu.subscription_id = %s and lower(%s.%s) = lower(imu.unsubscription_email) ' % (subscription_id,email_selection,email_field)
		unsubs_query = "SELECT distinct lower(imu.unsubscription_email) AS email FROM %s %s " % (from_clause_unsubs, where_str_unsubs)
		_logger.info('Unsbs query \n'+unsubs_query + ' params -> ' + str(where_params))
		self.env.cr.execute(unsubs_query,where_params)
		unsubscription_emails = [x['email'] for x in self.env.cr.dictfetchall()]
		_logger.info("------"+str(len(unsubscription_emails)))
		self.env.cache.invalidate()  # to handle memory error

		return set(unsubscription_emails)


	def get_ml_suppression_list_query_params(self, ml_rec):
		# Email Selection
		email_mode = ml_rec.email_selection

		if email_mode == 'contact':
			email_field = 'email'
		else:
			email_field = dynamic_txn_email_mapper[ml_rec.model_model]

		additional_domain = self.env['mailing.mailing'].get_default_domain(False,email_field) if ml_rec.model_model != 'isha.mailing.contact' else []
		domain = eval(ml_rec.domain) + additional_domain

		cross_join_key = None
		if ml_rec.model_model not in ['res.partner', 'isha.mailing.contact']:
			email_map = dynamic_mail_res_partner_mapper.get(ml_rec.model_model)
			# add the res_partner mapping to get the cross join query if it is not a delegated model
			if ml_rec.model_model not in ['program.lead', 'program.attendance', 'sadhguru.exclusive']:
				domain.append([email_map + '.id', '!=', False])
			cross_join_key = self.env[ml_rec.model_model]._table + '__' + email_map

		if email_mode == 'contact':
			email_selection = cross_join_key or 'res_partner'
		else:
			email_selection = self.env[ml_rec.model_model]._table

		from_clause, where_clause, where_clause_params = self.env[ml_rec.model_model].get_query_params(domain)
		where_str = where_clause and (" WHERE %s" % where_clause) or ''

		# For isha.mailing.contact no_marketing flag is not needed
		if ml_rec.model_model == 'isha.mailing.contact':
			base_query = 'SELECT lower("%s".%s) as email FROM %s %s'

			full_ml_query = base_query % (email_selection, email_field, from_clause, where_str)
		else:
			base_query = 'SELECT lower("%s".%s) as email FROM %s %s'

			full_ml_query = base_query % (email_selection, email_field,from_clause, where_str)

		return full_ml_query,where_clause_params

	def get_suppression_list_emails(self,from_clause,where_str,where_params,email_selection,email_field):
		list_of_email_sets = list()

		# Re-engagement Based Supression
		for x in self.re_eng_suppression_campaign_list:
			suppression_emails = set()
			from_clause_supression = copy.deepcopy(from_clause) + ' , mailing_trace mt'
			subset_clause = ''
			remove_delta = False
			if self.list_subset == 'not_sent':
				# remove emails which are sent to the previous campaigns
				subset_clause = ' and mt.sent is not null '
			elif self.list_subset == 'not_opened':
				# remove opened emails
				subset_clause = ' and mt.opened is not null '
			elif self.list_subset == 'sent_but_not_opened':
				remove_delta = True
				subset_clause = ' and mt.opened is not null '
			elif self.list_subset == 'opened':
				remove_delta = True
				# remove non-opened emails
				subset_clause = ' and mt.opened is null '
			elif self.list_subset == 'clicked':
				remove_delta = True
				subset_clause = ' and mt.clicked is null '
			elif self.list_subset == 'not_clicked':
				remove_delta = True
				subset_clause = ' and (mt.opened is null or mt.clicked is not null) '
			elif self.list_subset == 'not_clicked_all':
				subset_clause = ' and (mt.clicked is not null) '

			where_str_supression = copy.deepcopy(where_str) + " and mt.mass_mailing_id=%s and mt.state != 'exception'" \
															  " and lower(%s.%s) = lower(mt.email) %s" % (
				x.id, email_selection,email_field, subset_clause)
			suppression_query = "SELECT distinct lower(mt.email) AS email FROM %s %s " % (from_clause_supression, where_str_supression)
			_logger.info('Suppresssion query \n' + suppression_query + ' params -> ' + str(where_params))
			self.env.cr.execute(suppression_query, where_params)
			for rec in self.env.cr.dictfetchall():
				suppression_emails.add(rec['email'])

			if remove_delta:
				from_clause_supression = copy.deepcopy(from_clause) + " left join mailing_trace mt on mt.mass_mailing_id=%s" \
																	  " and lower(%s.%s) = lower(mt.email)" % (
					x.id, email_selection, email_field)
				where_str_supression = copy.deepcopy(where_str) + " and mt.id is null "
				delta_query = "SELECT distinct lower(%s.%s) AS email FROM %s %s " % \
									(email_selection, email_field, from_clause_supression, where_str_supression)
				self.env.cr.execute(delta_query, where_params)
				for rec in self.env.cr.dictfetchall():
					suppression_emails.add(rec['email'])

			self.env.cache.invalidate()  # to handle memory error
			list_of_email_sets.append(suppression_emails)

		final_suppression_emails = set()
		if not self.subset_combination or self.subset_combination == 'and':
			final_suppression_emails = set.union(*list_of_email_sets) if list_of_email_sets else set()
		elif self.subset_combination == 'or':
			final_suppression_emails = set.intersection(*list_of_email_sets) if list_of_email_sets else set()

		# List Based supression
		base_campaign_list = "SELECT lower(%s.%s) AS email FROM %s %s " % (
		email_selection, email_field, from_clause, where_str)
		for ml_list in self.suppression_mailing_list:
			full_ml_query, where_clause_params = self.get_ml_suppression_list_query_params(ml_list)
			suppression_final_query = "(%s) INTERSECT (%s)" % (base_campaign_list, full_ml_query)
			self.env.cr.execute(suppression_final_query, where_params + where_clause_params)
			for x in self.env.cr.dictfetchall():
				final_suppression_emails.add(x['email'])
			self.env.cache.invalidate()  # to handle memory error

		# Campaign Based Supression
		for x in self.suppression_campaign_list:
			from_clause_supression = copy.deepcopy(from_clause) + ' , mailing_trace mt'
			where_str_supression = copy.deepcopy(where_str) + \
								   " and mt.mass_mailing_id=%s and mt.state != 'exception' and lower(%s.%s) = lower(mt.email)" % (
				x.id, email_selection,email_field)
			suppression_query = "SELECT distinct lower(mt.email) AS email FROM %s %s " % (from_clause_supression, where_str_supression)
			_logger.info('Suppresssion query \n' + suppression_query + ' params -> ' + str(where_params))
			self.env.cr.execute(suppression_query, where_params)
			for x in self.env.cr.dictfetchall():
				final_suppression_emails.add(x['email'])
			self.env.cache.invalidate()  # to handle memory error
		_logger.info("------" + str(len(final_suppression_emails)))

		return final_suppression_emails

	def get_blacklist_emails(self,from_clause,where_str,where_params,email_selection,email_field,is_pgm_upd,full_table_check=False):
		from_clause_blacklist = copy.deepcopy(from_clause) + ' , mail_blacklist mb'
		if full_table_check:
			where_str_blacklist = copy.deepcopy(where_str) + " and lower(%s.%s) = lower(mb.email)" % (email_selection, email_field)
		elif is_pgm_upd:
			where_str_blacklist = copy.deepcopy(
				where_str) + " and mb.active = true and (mb.reason not in  ('whitelist','mig_unsubs','unsubs_all') or mb.reason is null) and lower(%s.%s) = lower(mb.email)" % (
									email_selection,email_field)
		else:
			where_str_blacklist = copy.deepcopy(
				where_str) + " and mb.active = true and (mb.reason != 'whitelist' or mb.reason is null) and lower(%s.%s) = lower(mb.email)" % (
									email_selection,email_field)
		blacklist_query = "SELECT distinct lower(mb.email) AS email FROM %s %s " % (from_clause_blacklist , where_str_blacklist)
		_logger.info('Blacklist query \n' + blacklist_query + ' params -> ' + str(where_params))

		self.env.cr.execute(blacklist_query, where_params)
		blacklist_emails = [x['email'] for x in self.env.cr.dictfetchall()]
		_logger.info("------" + str(len(blacklist_emails)))
		self.env.cache.invalidate()  # to handle memory error
		return set(blacklist_emails)

	def get_unverified_emails(self,from_clause,where_str,where_params,email_selection,email_field):
		from_clause_verified = copy.deepcopy(from_clause) + ' , isha_email_verified_master ievm'
		where_str_verified = copy.deepcopy(
			where_str) + " and ievm.active = true and trim(lower(%s.%s)) = trim(lower(ievm.email))" % (
								  email_selection,email_field)
		verified_query = "SELECT trim(lower(ievm.email)) AS email FROM %s %s " % (from_clause_verified, where_str_verified)
		full_ml_list = "SELECT trim(lower(%s.%s)) AS email FROM %s %s " % (email_selection,email_field,from_clause,where_str)
		unverified_email_query = "(%s) EXCEPT (%s)" % (full_ml_list,verified_query)

		# where_params needs to be passed twice as we have 2 sub queries
		where_params_final = where_params + where_params

		_logger.info('Unverified query \n' + unverified_email_query + ' params -> ' + str(where_params_final))
		self.env.cr.execute(unverified_email_query,where_params_final)

		unverified_emails = [x['email'] for x in self.env.cr.dictfetchall()]
		self.env.cache.invalidate()  # to handle memory error
		_logger.info("------" + str(len(unverified_emails)))

		return set(unverified_emails)

	def get_subs_category_missing(self,from_clause,where_str,where_params,cross_join):
		subscription_id = self.dynamic_mailing_list.subscription_id.id

		# For isha.mailing.contact the subscription categories are stored in the txn table itself since it is double opt-in
		if self.mailing_model_real == 'isha.mailing.contact':
			from_clause_subs = copy.deepcopy(from_clause) + ' , mailing_contact_subscriptions_rel rel'
			where_str_subs = copy.deepcopy(
				where_str) + " and rel.isha_mailing_subscriptions_id = %s and rel.isha_mailing_contact_id = isha_mailing_contact.id" % (
									  subscription_id)
		else:  # For all the other models we should consider the res.partner's subscriptions
			from_clause_subs = copy.deepcopy(from_clause) + ' , isha_mailing_subscriptions_rel rel'
			where_str_subs = copy.deepcopy(
				where_str) + " and rel.isha_mailing_subscriptions_id = %s and rel.res_partner_id = %s.id" % (
									  subscription_id,cross_join)
		subs_query = "SELECT %s.id AS id FROM %s %s " % (self.env[self.mailing_model_real]._table, from_clause_subs, where_str_subs)
		subs_query = subs_query

		full_ml_list = 'SELECT %s.id FROM %s %s ' %(self.env[self.mailing_model_real]._table,from_clause,where_str)

		subs_missing_query = "(%s) EXCEPT (%s)" % (full_ml_list,subs_query)

		# where_params needs to be passed twice as we have 2 sub queries
		where_params_final = where_params + where_params

		_logger.info('Subs missing query \n' + subs_missing_query + ' params -> '+ str(where_params_final))

		self.env.cr.execute(subs_missing_query,where_params_final)
		subs_missing = [x['id'] for x in self.env.cr.dictfetchall()]
		_logger.info("------" + str(len(subs_missing)))
		self.env.cache.invalidate()  # to handle memory error
		return set(subs_missing)

	def get_restricted_records(self, from_clause, where_str, where_params, cross_join, is_pgm_upd):
		logger.info('where params: %s' % where_params)
		if self.mailing_model_real == 'isha.mailing.contact' or is_pgm_upd:
			return set()

		charitable = self.with_user(self.create_uid).user_has_groups(CHARITABLE_GROUPS)
		religious = self.with_user(self.create_uid).user_has_groups(RELIGIOUS_GROUPS)
		if charitable and religious:
			return set()
		elif charitable:
			# restrict_column = 'has_religious_txn'
			other_column = 'has_charitable_txn'
		elif religious:
			# restrict_column = 'has_charitable_txn'
			other_column = 'has_religious_txn'
		else:
			raise ValidationError('User does not have access to Santosha')

		table_name = self.env[self.mailing_model_real]._table
		restricted_where_clause = copy.deepcopy(where_str) + ' AND (%s.%s = false or %s.%s is null)' % \
								  (cross_join, other_column, cross_join, other_column)
		restricted_query = 'SELECT %s.id FROM %s %s ' %(self.env[self.mailing_model_real]._table, from_clause, restricted_where_clause)

		self.env.cr.execute(restricted_query, where_params)
		restricted_ids = [x['id'] for x in self.env.cr.dictfetchall()]
		_logger.info("Restricted ids------" + str(len(restricted_ids)))
		self.env.cache.invalidate()  # to handle memory error
		return set(restricted_ids)

	def dedup_handler_optimized(self, domain, return_stats = False):
		is_pgm_update = self.is_pgm_upd
		email_set = set()
		res_ids = []
		internal_dedup_count = 0
		invalid_emails = set()
		invalid_email_recs = set()
		unverified_email_recs = []
		unmapped_email_recs = []
		no_marketing_rec_ids = set()
		start = datetime.datetime.now()
		cross_join_key = False
		if self.mailing_model_real not in ['res.partner', 'isha.mailing.contact']:
			email_map = dynamic_mail_res_partner_mapper.get(self.mailing_model_real)
			# add the res_partner mapping to get the cross join query
			domain.append([email_map + '.id', '!=', False])
			cross_join_key = self.env[self.mailing_model_real]._table + '__' + email_map

		# Email Selection
		email_mode = self.dynamic_mailing_list.email_selection
		if self.mailing_model_real not in ['res.partner'] and self.is_pgm_upd:
			email_mode = 'txn'

		if email_mode == 'contact':
			email_selection = cross_join_key or 'res_partner'
			email_field = 'email'
		else:
			email_selection = self.env[self.mailing_model_real]._table
			email_field = dynamic_txn_email_mapper[self.mailing_model_real]

		from_clause, where_clause, where_clause_params = self.env[self.mailing_model_real].get_query_params(domain)
		where_str = where_clause and (" WHERE %s" % where_clause) or ''

		# For isha.mailing.contact no_marketing flag is not needed
		if self.mailing_model_real == 'isha.mailing.contact':
			base_query = 'SELECT "%s".id, lower("%s".%s) as email FROM %s %s'

			full_ml_query = base_query % (
				self.env[self.mailing_model_real]._table, email_selection, email_field, from_clause,
				where_str)
		else:
			base_query = 'SELECT "%s".id, lower("%s".%s) as email, "%s".no_marketing, "%s".region_name  FROM %s %s'

			full_ml_query = base_query % (
				self.env[self.mailing_model_real]._table, email_selection, email_field,cross_join_key or 'res_partner', cross_join_key or 'res_partner', from_clause, where_str)

		self.env.cr.execute(full_ml_query,where_clause_params)
		recs = self.env.cr.dictfetchall()

		# Get the unsubscribed emails to dedup the list
		unsubs_dedup = self.get_unsubscription_emails_for_category(from_clause,where_str,where_clause_params,email_selection, email_field)

		# Get the Suppression list emails
		suppression_emails = self.get_suppression_list_emails(from_clause,where_str,where_clause_params,email_selection, email_field)
		suppression_dedup_count = len(suppression_emails)

		# Get the Blacklisted emails
		blacklist_dedup_emails = self.get_blacklist_emails(from_clause,where_str,where_clause_params,email_selection, email_field,self.is_pgm_upd,False)

		# Get verified emails
		unverified_email = self.get_unverified_emails(from_clause,where_str,where_clause_params,email_selection, email_field)
		# Exclude the blacklisted emails from marking as unverified
		unverified_email = unverified_email - self.get_blacklist_emails(from_clause,where_str,where_clause_params,email_selection, email_field,self.is_pgm_upd,True)

		# Get Subs Category missing recs
		subs_dedup = self.get_subs_category_missing(from_clause,where_str,where_clause_params,cross_join_key or 'res_partner')

		restricted_recs = self.get_restricted_records(from_clause, where_str, where_clause_params, cross_join_key or 'res_partner', self.is_pgm_upd)

		"""
			1. Remove the duplicate from withing the list
			2. Remove the unsubscribed email irrespective of the current contact or txn
			3. Remove the email if it does not have the subscription assigned
			4. Remove the restricted records
			5. Remove the emails from suppression list
			6. Remove the all the blacklist
			7. Remove the email if it is not verified
			8. Remove the no_marketing recs
			9. Remove the email if it is not passing regex validation
		   10. Remove Others-others users if user is not a member of emedia & ipc team
		"""
		allow_send_to_unmapped_contacts = self.can_send_to_unmapped_contacts()
		for rec in recs:
			to_email_raw = rec['email']
			is_email_valid = tools.email_normalize(to_email_raw)
			rec_id = rec['id']
			if to_email_raw in email_set:
				internal_dedup_count += 1
			if to_email_raw in unverified_email:
				unverified_email_recs.append(rec_id)
			if self.mailing_model_real != 'isha.mailing.contact' and rec['no_marketing'] and not is_pgm_update:
				no_marketing_rec_ids.add(rec_id)
			if self.mailing_model_real != 'isha.mailing.contact' and rec['region_name'] == 'Others' and not allow_send_to_unmapped_contacts:
				unmapped_email_recs.append(rec_id)

			if not is_email_valid:
				invalid_emails.add(to_email_raw)
				invalid_email_recs.add(rec_id)

			if to_email_raw not in email_set and\
				to_email_raw not in unsubs_dedup and\
				rec_id not in subs_dedup and\
				rec_id not in restricted_recs and\
				to_email_raw not in suppression_emails and\
				to_email_raw not in blacklist_dedup_emails and\
				to_email_raw not in unverified_email and\
				rec_id not in no_marketing_rec_ids and\
				rec_id not in unmapped_email_recs and\
				is_email_valid:
				res_ids.append(rec_id)
				email_set.add(to_email_raw)
		end = datetime.datetime.now()
		mailing_stats = {
				'mailing_id':self.id,
				'source_ml_count':len(recs),
				'deduped_count':len(res_ids),
				'internal_dup_emails_count':internal_dedup_count,
				'unsubscription_emails':list(unsubs_dedup),
				'unsubscription_emails_count':len(unsubs_dedup),
				'no_subs_ids':list(subs_dedup),
				'no_subs_ids_count':len(subs_dedup),
				'invalid_emails': list(invalid_emails),
				'invalid_emails_count':len(invalid_emails),
				'invalid_email_recs':list(invalid_email_recs),
				'restricted_ids': list(restricted_recs),
				'restricted_ids_count': len(restricted_recs),
				'no_marketing_ids':list(no_marketing_rec_ids),
				'no_marketing_ids_count':len(no_marketing_rec_ids),
				'suppression_list_count':suppression_dedup_count,
				'blacklist_emails':list(blacklist_dedup_emails),
				'blacklist_emails_count':len(blacklist_dedup_emails),
				'unverified_email_recs':list(unverified_email_recs),
				'unverified_email_recs_count':len(unverified_email_recs),
				'unmapped_emails_count': len(unmapped_email_recs),
				'unmapped_emails_recs': list(unmapped_email_recs),
				'unverified_emails':list(unverified_email),
				'global_unsubs':self.env['isha.mailing.unsubscriptions'].search([('active','=',True),('subscription_id','=',self.dynamic_mailing_list.subscription_id.id)],count=True),
				# 'global_suppression_count':len(suppression_emails),
				'global_blacklist':self.env['mail.blacklist'].search([('active','=',True),('reason','!=','whitelisted')],count=True)
			}
		if return_stats:
			return mailing_stats
		else:
			time_taken = end - start
			hours, remainder = divmod(time_taken.total_seconds(), 3600)
			minutes, seconds = divmod(remainder, 60)
			time_taken_str = '%s hour(s) %s minute(s) %s second(s)' % (int(hours), int(minutes), int(seconds))
			mailing_stats['time_taken'] = time_taken_str
			stats_rec = self.env['isha.mailing.dedup.stats'].search([('mailing_id', '=', mailing_stats['mailing_id'])])
			if stats_rec:
				stats_rec.sudo().write(mailing_stats)
			else:
				stats_rec = self.env['isha.mailing.dedup.stats'].sudo().create([mailing_stats])
		return res_ids

	def can_send_to_unmapped_contacts(self):
		mailing_teams = self.env.user.mailing_teams.ids
		emedia_team_id = self.env.ref('isha_crm.mailing_team_emedia').id
		ipc_team_id = self.env.ref('isha_crm.mailing_team_ipc').id

		if emedia_team_id in mailing_teams or ipc_team_id in mailing_teams:
			return True
		return False

	# The orm based dedup is taking more than 5 mins for Just 10k recs. Moving to Sql based dedup.
	# def dedup_handler(self, recs, return_stats=False):
	# 	email_map = False
	# 	email_set = set()
	# 	res_ids = []
	# 	internal_dedup_count = 0
	# 	unsubs_dedup = []
	# 	subs_dedup = []
	# 	suppression_dedup_count = 0
	# 	blacklist_dedup_emails = []
	# 	unverified_email_recs = []
	#
	# 	subscription_id = self.dynamic_mailing_list.subscription_id.id
	# 	# Get the unsubscribed emails to dedup the list
	# 	unsubscription_emails = self.get_unsubscription_emails_for_category()
	#
	# 	# Get the Suppression list emails
	# 	suppression_emails = self.get_suppression_list_emails()
	#
	# 	# Get the Blacklisted emails
	# 	blacklist_emails = self.get_blacklist_emails()
	#
	# 	# Get verified emails
	# 	verified_emails = self.get_verified_email_masters()
	#
	# 	# Dynamic email mode
	# 	if self.mailing_model_real not in ['res.partner','mailing.contact']:
	# 		email_map = dynamic_mail_res_partner_mapper.get(self.mailing_model_real, False)
	#
	# 	# Email Selection
	# 	email_selection = self.dynamic_mailing_list.email_selection
	# 	if self.mailing_model_real not in ['res.partner','mailing.contact'] and self.is_pgm_upd:
	# 		email_selection = 'txn'
	#
	#
	# 	for rec in recs:
	# 		to_email_raw = rec[email_map]['email'] if email_map and email_selection == 'contact' else rec['email']
	# 		to_email_raw = to_email_raw.lower()
	# 		subscription_ids = rec[email_map]['ml_subscription_ids'].ids if email_map else rec['ml_subscription_ids'].ids
	#
	# 		"""
	# 			1. Remove the duplicate from withing the list
	# 			2. Remove the unsubscribed email irrespective of the current contact or txn
	# 			3. Remove the email if it does not have the subscription assigned
	# 			4. Remove the emails from suppression list
	# 			5. Remove the all the blacklist
	# 			6. Remove the email if it is not verified
	# 		"""
	# 		if to_email_raw in email_set:
	# 			internal_dedup_count += 1
	# 		if to_email_raw in unsubscription_emails:
	# 			unsubs_dedup.append(to_email_raw)
	# 		if subscription_id not in subscription_ids:
	# 			subs_dedup.append(rec.id)
	# 		if to_email_raw in suppression_emails:
	# 			suppression_dedup_count += 1
	# 		if to_email_raw in blacklist_emails:
	# 			blacklist_dedup_emails.append(to_email_raw)
	# 		if to_email_raw not in verified_emails:
	# 			unverified_email_recs.append(rec.id)
	#
	# 		if to_email_raw not in email_set and\
	# 			to_email_raw not in unsubscription_emails and\
	# 			subscription_id in subscription_ids and\
	# 			to_email_raw not in suppression_emails and\
	# 			to_email_raw not in blacklist_emails and\
	# 			to_email_raw in verified_emails:
	#
	# 			res_ids.append(rec.id)
	# 			email_set.add(to_email_raw)
	#
	#
	# 	if return_stats:
	# 		return {
	# 			'mailing_id':self.id,
	# 			'source_ml_count':len(recs),
	# 			'deduped_count':len(res_ids),
	# 			'internal_dup_emails_count':internal_dedup_count,
	# 			'unsubscription_emails':unsubs_dedup,
	# 			'unsubscription_emails_count':len(unsubs_dedup),
	# 			'no_subs_ids':subs_dedup,
	# 			'no_subs_ids_count':len(subs_dedup),
	# 			'suppression_list_count':suppression_dedup_count,
	# 			'blacklist_emails':blacklist_dedup_emails,
	# 			'blacklist_emails_count':len(blacklist_dedup_emails),
	# 			'unverified_email_recs':unverified_email_recs,
	# 			'unverified_email_recs_count':len(unverified_email_recs),
	# 			'global_unsubs':len(unsubscription_emails),
	# 			'global_suppression_count':len(suppression_emails),
	# 			'global_blacklist':len(blacklist_emails)
	# 		}
	# 	return res_ids


	def _get_recipients(self):
		if self.mailing_domain:
			domain = safe_eval(self.mailing_domain)
			try:
				_logger.info('########## duplicate removal start for ' + self.mailing_name)
				res_ids = self.dedup_handler_optimized(domain)
				_logger.info('########## duplicate removal end for ' + self.mailing_name)
			except ValueError:
				res_ids = []
				_logger.exception('Cannot get the mass mailing recipients, model: %s, domain: %s',
								  self.mailing_model_real, domain)
		else:
			res_ids = []
			domain = [('id', 'in', res_ids)]

		# randomly choose a fragment
		if self.contact_ab_pc < 100:
			contact_nbr = self.env[self.mailing_model_real].search_count(domain)
			topick = int(contact_nbr / 100.0 * self.contact_ab_pc)
			if self.campaign_id and self.unique_ab_testing:
				already_mailed = self.campaign_id._get_mailing_recipients()[self.campaign_id.id]
			else:
				already_mailed = set([])
			remaining = set(res_ids).difference(already_mailed)
			if topick > len(remaining):
				topick = len(remaining)
			res_ids = random.sample(remaining, topick)
		return res_ids

	def action_view_soft_bounced(self):
		soft_bounce_stats = self.mailing_trace_ids.filtered(lambda x: x.bounced and x.bounce_type != 'Permanent')
		res_ids = soft_bounce_stats.mapped('res_id')
		model_name = self.env['ir.model']._get(self.mailing_model_real).display_name
		return {
			'name': model_name,
			'type': 'ir.actions.act_window',
			'view_mode': 'tree',
			'res_model': self.mailing_model_real,
			'domain': [('id', 'in', res_ids)],
			'context': dict(self._context, create=False)
		}
	def action_view_hard_bounced(self):
		hard_bounce_stats = self.mailing_trace_ids.filtered(lambda x: x.bounced and x.bounce_type == 'Permanent')
		res_ids = hard_bounce_stats.mapped('res_id')
		model_name = self.env['ir.model']._get(self.mailing_model_real).display_name
		return {
			'name': model_name,
			'type': 'ir.actions.act_window',
			'view_mode': 'tree',
			'res_model': self.mailing_model_real,
			'domain': [('id', 'in', res_ids)],
			'context': dict(self._context, create=False)
		}

	def action_view_complaint(self):
		complaint_stats = self.mailing_trace_ids.filtered(lambda stat: stat.complaint)
		res_ids = complaint_stats.mapped('res_id')
		model_name = self.env['ir.model']._get(self.mailing_model_real).display_name
		return {
			'name': model_name,
			'type': 'ir.actions.act_window',
			'view_mode': 'tree',
			'res_model': self.mailing_model_real,
			'domain': [('id', 'in', res_ids)],
			'context': dict(self._context, create=False)
		}

	def action_view_unsubs(self):
		return {
			'name': "Un-Subscription from " +self.mailing_name,
			'type': 'ir.actions.act_window',
			'view_mode': 'tree,form',
			'res_model': 'isha.mailing.unsubscriptions',
			'domain': [('mailing_id', '=', self.id),('subscription_id','=',self.dynamic_mailing_list.subscription_id.id)],
			'context': dict(self._context, create=False)
		}

	def ses_mail_delivery(self, thread_name, chunk_res_ids, model, mass_mailing_id, body_html, email_from, reply_to,
							subject,base_url, db_name, secret, email_selection, email_field,test_email=False,
							local_mode=False, marketing_activity_id=False):
		# Amazon SES config
		ses_config = Configuration('AMAZON_SES')
		ses = None
		config_set = None
		try:
			_logger.info('Connecting to SES')
			if not local_mode:
				session = boto3.Session(aws_access_key_id=ses_config['ACCESS_KEY'],
										aws_secret_access_key=ses_config['SECRET'],
										region_name=ses_config['REGION'])

				ses = session.client('ses')
				config_set = ses_config['CONFIG_SET']
		except Exception as e:
			_logger.info('Failed to connect to Amazon SES')
			return

		# Dynamic place holders Config
		mako_env = mail_template.mako_template_env
		template = mako_env.from_string(tools.ustr(body_html))
		email_map = False
		# Dynamically pick contact's email from txn tables
		if model != 'res.partner' and email_selection == 'contact':
			email_map = dynamic_mail_res_partner_mapper.get(model, False)
		mailing_traces = []
		email_rec_list = self.env[model].browse(chunk_res_ids)

		# Get the marketing trace records if the campaign is triggered from marketing automation
		marketing_traces = {}
		if marketing_activity_id:
			marketing_traces = self.env['marketing.trace'].search_read([('activity_id', '=', marketing_activity_id),
																		('res_id', 'in', chunk_res_ids)],
																	fields=['id', 'res_id'])
			marketing_traces = {trace['res_id']:trace['id'] for trace in marketing_traces}
		for email_rec in email_rec_list:
			# Render Dynamic place holders
			temp_dict = {}
			temp_dict['object'] = email_rec
			body_html_rendered = template.render(temp_dict)

			base_url = base_url.rstrip('/')

			if test_email:
				to_email_raw = test_email
			elif email_map:
				to_email_raw = email_rec[email_map][email_field]
			else:  # Fallback to txn's email
				to_email_raw = email_rec[email_field]

			body_html_mod = transform_mail_body_links(base_url, db_name, secret, mass_mailing_id, email_rec,
													  body_html_rendered, to_email_raw)

			SENDER = email_from
			RECIPIENT = to_email_raw
			REPLYTO = reply_to
			SUBJECT = subject
			BODY_TEXT = html2text.html2text(body_html_mod)
			CHARSET = "UTF-8"

			try:
				if not local_mode:
					response = ses.send_email(
						Destination={
							'ToAddresses': [
								RECIPIENT,
							],
						},
						ReplyToAddresses=[
							REPLYTO,
						],
						Message={
							'Body': {
								'Html': {
									'Charset': CHARSET,
									'Data': body_html_mod,
								},
								'Text': {
									'Charset': CHARSET,
									'Data': BODY_TEXT,
								},
							},
							'Subject': {
								'Charset': CHARSET,
								'Data': SUBJECT,
							},
						},
						Source=SENDER,
						ConfigurationSetName=config_set,
					)
				else:
					if 'localhost' in base_url:
						_logger.info('Skip Mode - ' + to_email_raw)
					_logger.info(body_html_mod)
					# time.sleep(10)
					response = {'MessageId': str(datetime.datetime.now())}
				if marketing_activity_id:
					trace = {'trace_type': 'mail', 'email': to_email_raw, 'model': model, 'res_id': email_rec['id'],
								'mass_mailing_id': mass_mailing_id, 'message_id': response['MessageId'],
								'sent': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), 'state': 'sent',
								'marketing_trace_id':marketing_traces.get(email_rec['id'],None)}
				else:
					trace = {'trace_type': 'mail', 'email': to_email_raw, 'model': model, 'res_id': email_rec['id'],
								'mass_mailing_id': mass_mailing_id, 'message_id': response['MessageId'],
								'sent': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), 'state': 'sent'}

			except ClientError as e:
				_logger.info(e.response['Error']['Message'])
				if marketing_activity_id:
					trace = {'trace_type': 'mail', 'email': to_email_raw, 'model': model, 'res_id': email_rec['id'],
							 'mass_mailing_id': mass_mailing_id, 'state': 'exception',
							 'exception': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
							 'failure_type': e.response['Error']['Message'],
							 'marketing_trace_id':marketing_traces.get(email_rec['id'],None)}
				else:
					trace = {'trace_type': 'mail', 'email': to_email_raw, 'model': model, 'res_id': email_rec['id'],
							 'mass_mailing_id': mass_mailing_id, 'state': 'exception',
							 'exception': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
							 'failure_type': e.response['Error']['Message']}
			mailing_traces.append(trace)
		# Skip mail traces for mailing test action
		if not test_email or self._context.get('create_trace', False):
			try:
				self.env['mailing.trace'].create(mailing_traces)
			except:
				try:
					self.env['mailing.trace'].create(mailing_traces)
				except:
					_logger.error('Failed to save mailing trace: ' + str(mailing_traces))

			self.env.cr.commit()
		return True

# @api.model
# def _process_mass_mailing_queue(self):
# 	cron = self.env.ref('mass_mailing.ir_cron_mass_mailing_queue')
# this method will throw error if the cron job is currently running already.
# cron._try_lock()
# super(MassMailing, self)._process_mass_mailing_queue()


class MailMail(models.Model):
	_inherit = 'mail.mail'

	@api.model
	def process_email_queue(self, ids=None):
		"""Send immediately queued messages, committing after each
			message is sent - this is not transactional and should
			not be called during another transaction!

			:param list ids: optional list of emails ids to send. If passed
							no search is performed, and these ids are used
							instead.
			:param dict context: if a 'filters' key is present in context,
								this value will be used as an additional
								filter to further restrict the outgoing
								messages to send (by default all 'outgoing'
								messages are sent).
		"""
		filters = ['&',
					('state', '=', 'outgoing'),
					'|',
					('scheduled_date', '<', datetime.datetime.now()),
					('scheduled_date', '=', False)]
		if 'filters' in self._context:
			filters.extend(self._context['filters'])
		# TODO: make limit configurable

		res = None
		while True:
			filtered_ids = self.search(filters, limit=500).ids
			if len(filtered_ids) == 0:
				break
			if not ids:
				ids = filtered_ids
			else:
				ids = list(set(filtered_ids) & set(ids))
			ids.sort()

			try:
				# auto-commit except in testing mode
				auto_commit = not getattr(threading.currentThread(), 'testing', False)
				before = datetime.datetime.now()
				res = self.browse(ids).send(auto_commit=auto_commit)
				after = datetime.datetime.now()
				_logger.info("########send time: " + str(after - before))
			except Exception:
				_logger.exception("Failed processing mail queue")
		return res


def _replace_local_links(html, base_url=None):
	""" Replace local links by absolute links. It is required in various
	cases, for example when sending emails on chatter or sending mass
	mailings. It replaces

	 * href of links (mailto will not match the regex)
	 * src of images (base64 hardcoded data will not match the regex)
	 * styling using url like background-image: url

	It is done using regex because it is shorten than using an html parser
	to create a potentially complex soupe and hope to have a result that
	has not been harmed.
	"""
	if not html:
		return html

	html = ustr(html)

	def _sub_relative2absolute(match):
		# compute here to do it only if really necessary + cache will ensure it is done only once
		# if not base_url
		return match.group(1) + urls.url_join(_sub_relative2absolute.base_url, match.group(2))

	_sub_relative2absolute.base_url = base_url
	html = re.sub(r"""(<img(?=\s)[^>]*\ssrc=")(/[^/][^"]+)""", _sub_relative2absolute, html)
	html = re.sub(r"""(<a(?=\s)[^>]*\shref=")(/[^/][^"]+)""", _sub_relative2absolute, html)
	html = re.sub(r"""(<[^>]+\bstyle="[^"]+\burl\('?)(/[^/'][^'")]+)""", _sub_relative2absolute, html)

	return html


def _unsubscribe_token(secret, dbname, mailing_id, res_id, email):
	"""Generate a secure hash for this mailing list and parameters.

	This is appended to the unsubscription URL and then checked at
	unsubscription time to ensure no malicious unsubscriptions are
	performed.

	:param int res_id:
		ID of the resource that will be unsubscribed.

	:param str email:
		Email of the resource that will be unsubscribed.
	"""
	token = (dbname, mailing_id, int(res_id), tools.ustr(email))
	return hmac.new(secret.encode('utf-8'), repr(token).encode('utf-8'), hashlib.sha512).hexdigest()


def _get_unsubscribe_url(base_url, mailing_id, db, res_id, email_to, secret,token):
	url = werkzeug.urls.url_join(
		base_url, 'mail/isha_mailing/%(mailing_id)s/unsubscribe?%(params)s' % {
			'mailing_id': mailing_id,
			'params': werkzeug.urls.url_encode({
				'db': db,
				'res_id': res_id,
				'email': email_to,
				'token': token,
			}),
		}
	)
	return url

def _get_manage_subscription_url(base_url, mailing_id, db, res_id, email_to, secret,token):
	url = werkzeug.urls.url_join(
		base_url, 'mail/isha_mailing/%(mailing_id)s/manage?%(params)s' % {
			'mailing_id': mailing_id,
			'params': werkzeug.urls.url_encode({
				'db': db,
				'res_id': res_id,
				'email': email_to,
				'token': token,
			}),
		}
	)
	return url

def _get_view_mailer_url(base_url, mailing_id, db, res_id, email_to, secret,token):
	url = werkzeug.urls.url_join(
		base_url, 'mail/emailer/%(mailing_id)s/view?%(params)s' % {
			'mailing_id': mailing_id,
			'params': werkzeug.urls.url_encode({
				'db': db,
				'res_id': res_id,
				'email': email_to,
				'token': token,
			}),
		}
	)
	return url


# Clicks will be pushed from SNS. So discarding the creation of link.tracker
# def update_body_with_trackable_urls(self, body, mass_mailing_id):
#
# 	do_not_track_urls = ["unsubscribe_from_list", "manage_subscription", "view_mailer"]
# 	for match in re.findall(URL_REGEX, body):
# 		href = match[0]
# 		url = match[1].lower()
#
# 		if url in do_not_track_urls:
# 			continue
#
# 		tracker = self.env['link.tracker'].sudo().search(
# 			[('url', '=', match[1]), ('mass_mailing_id', '=', mass_mailing_id)])
# 		if len(tracker) > 0:  # browse to handle the odoorpc
# 			tracker = self.env['link.tracker'].sudo().browse(tracker.id)
# 		else:
# 			tracker = self.env['link.tracker'].sudo().create({
# 				'mass_mailing_id': mass_mailing_id,
# 				'url': match[1]
# 			})
#
# 		new_href = href.replace(match[1], tracker.short_url)
# 		body = body.replace(href, new_href)
# 		return body



def update_click_track_url(body, mass_mailing_id, res_id):
	for match in re.findall(URL_REGEX, body):
		href = match[0]
		url = match[1]

		parsed = werkzeug.urls.url_parse(url, scheme='http')

		if parsed.scheme.startswith('http') and parsed.path.startswith('/r/'):
			new_href = href.replace(url, url + '/ccm/' + str(mass_mailing_id) + '/' + str(res_id))
			body = body.replace(href, new_href)
	return body


def transform_mail_body_links(base_url, db_name, secret, mass_mailing_id, email_rec, body_html_rendered, to_email_raw):
	# Open's will be captured from SES config set
	# track_url = werkzeug.urls.url_join(
	# 	base_url, 'mail/track/%(mass_mailing_id)s/%(res_id)s/blank.gif?%(params)s' % {
	# 		'mass_mailing_id': mass_mailing_id,
	# 		'res_id': email_rec['id'],
	# 		'params': werkzeug.urls.url_encode({'db': db_name})
	# 	}
	# )
	# track_html = '<img src="%s" alt=""/>' % track_url
	# body_html_mod = tools.append_content_to_html(body_html_rendered, track_html, plaintext=False, container_tag='div')

	body_html_mod = body_html_rendered
	body_html_mod = _replace_local_links(body_html_mod, base_url)
	# Unsubscription Link
	token = _unsubscribe_token(secret, db_name, mass_mailing_id, email_rec['id'], to_email_raw)
	unsubscribe_url = _get_unsubscribe_url(base_url, mass_mailing_id, db_name, email_rec['id'], to_email_raw, secret, token)
	link_to_replace = 'unsubscribe_from_list'
	if link_to_replace in body_html_mod:
		if base_url+'/'+link_to_replace in body_html_mod:  # handle for plain text mailer
			body_html_mod = body_html_mod.replace(base_url+'/'+link_to_replace, unsubscribe_url if unsubscribe_url else '#')
		else:
			body_html_mod = body_html_mod.replace(link_to_replace, unsubscribe_url if unsubscribe_url else '#')

	unsubscribe_url = _get_manage_subscription_url(base_url, mass_mailing_id, db_name, email_rec['id'], to_email_raw, secret,
													token)
	# Manage Subscription Link
	link_to_replace = 'manage_subscription'
	if link_to_replace in body_html_mod:
		if base_url+'/'+link_to_replace in body_html_mod:  # handle for plain text mailer
			body_html_mod = body_html_mod.replace(base_url+'/'+link_to_replace, unsubscribe_url if unsubscribe_url else '#')
		else:
			body_html_mod = body_html_mod.replace(link_to_replace, unsubscribe_url if unsubscribe_url else '#')

	view_mailer_url = _get_view_mailer_url(base_url, mass_mailing_id, db_name, email_rec['id'], to_email_raw, secret,
													token)
	link_to_replace = 'view_mailer'
	if link_to_replace in body_html_mod:
		if base_url+'/'+link_to_replace in body_html_mod:  # handle for plain text mailer
			body_html_mod = body_html_mod.replace(base_url+'/'+link_to_replace, view_mailer_url if view_mailer_url else '#')
		else:
			body_html_mod = body_html_mod.replace(link_to_replace, view_mailer_url if view_mailer_url else '#')

	# click tracker
	if '/r/' in body_html_mod:
		body_html_mod = update_click_track_url(body_html_mod, mass_mailing_id, email_rec['id'])


	return body_html_mod

def quick_send_process(thread_name, res_ids, model, mass_mailing_id, body_html, email_from, subject, base_url, db_name,
						secret,test_email=False):
	thread_start_time = datetime.datetime.now()

	# Amazon SES config
	ses_config = Configuration('AMAZON_SES')
	try:
		_logger.info('Connecting to SES')
		session = boto3.Session(aws_access_key_id=ses_config['ACCESS_KEY'],
								aws_secret_access_key=ses_config['SECRET'],
								region_name=ses_config['REGION'])

		s3 = session.client('ses')
	except Exception as e:
		_logger.info('Failed to connect to Amazon SES')
		return
	rpc_config = Configuration('RPC')
	username = rpc_config['RPC_USERNAME']
	password = rpc_config['RPC_PASSWORD']
	url = rpc_config['RPC_URL']
	client.ServerProxy('{}/xmlrpc/2/common'.format(url))
	db = config['db_name']
	common = client.ServerProxy('{}/xmlrpc/2/common'.format(url))
	uid = common.authenticate(db, username, password, {})
	odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url))

	split_res_ids = [res_ids[i:i + 50] for i in range(0, len(res_ids), 50)]
	for chunk_res_ids in split_res_ids:
		before = datetime.datetime.now()
		email_rec_list = odoo_models.execute_kw(db, uid, password,
												model, 'search_read', [[['id', 'in', chunk_res_ids]]],
												{'fields': ['id', 'name', 'email']}
												)
		trace_list = []
		for email in email_rec_list:
			# Add Tracker
			track_url = werkzeug.urls.url_join(
				base_url, 'mail/track/%(mass_mailing_id)s/%(res_id)s/blank.gif?%(params)s' % {
					'mass_mailing_id': mass_mailing_id,
					'res_id': email['id'],
					'params': werkzeug.urls.url_encode({'db': db_name})
				}
			)
			track_html = '<img src="%s" alt=""/>' % track_url
			body_html_mod = tools.append_content_to_html(body_html, track_html, plaintext=False, container_tag='div')
			body_html_mod = _replace_local_links(body_html_mod, base_url)

			# dynamic placeholder
			body_html_mod = body_html_mod.replace('${object.name}',email['name'])

			base_url = base_url.rstrip('/')
			unsubscribe_url = _get_unsubscribe_url(base_url, mass_mailing_id, db, email['id'], email['email'], secret)
			link_to_replace = base_url + '/unsubscribe_from_list'
			if link_to_replace in body_html_mod:
				body_html_mod = body_html_mod.replace(link_to_replace, unsubscribe_url if unsubscribe_url else '#')
			if test_email:
				to_email_raw = test_email
			else:  # Fallback to txn's email
				to_email_raw = email['email']

			SENDER = email_from
			RECIPIENT = to_email_raw
			SUBJECT = subject
			BODY_TEXT = html2text.html2text(body_html_mod)
			CHARSET = "UTF-8"

			try:
				response = s3.send_email(
					Destination={
						'ToAddresses': [
							RECIPIENT,
						],
					},
					Message={
						'Body': {
							'Html': {
								'Charset': CHARSET,
								'Data': body_html_mod,
							},
							'Text': {
								'Charset': CHARSET,
								'Data': BODY_TEXT,
							},
						},
						'Subject': {
							'Charset': CHARSET,
							'Data': SUBJECT,
						},
					},
					Source=SENDER,
				)
				trace = {'trace_type': 'mail', 'email': to_email_raw, 'model': model, 'res_id': email['id'],
						 'mass_mailing_id': mass_mailing_id, 'message_id': response['MessageId'],
						 'sent': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), 'state': 'sent'}
			except ClientError as e:
				_logger.info(e.response['Error']['Message'])
				trace = {'trace_type': 'mail', 'email': to_email_raw, 'model': model, 'res_id': email['id'],
						 'mass_mailing_id': mass_mailing_id, 'message_id': response['MessageId'], 'state': 'exception',
						 'exception': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
						 'failure_type': e.response['Error']['Message']}

			# Skip mail traces for mailing test action
			if not test_email:
				try:
					odoo_models.execute_kw(db, uid, password, 'mailing.trace', 'create', [trace])
				except:
					try:
						odoo_models.execute_kw(db, uid, password, 'mailing.trace', 'create', [trace])
					except:
						_logger.error('Failed to save mailing trace: ' + str(trace))
		after = datetime.datetime.now()
		_logger.info(
				"######## XMLRPC Time taken to send " + str(len(email_rec_list)) + " mailing traces: " + str(after - before))

	thread_end_time = datetime.datetime.now()
	_logger.info(
		'########## XMLRPC Thread time' + thread_name + ' processing done for ' + str(len(res_ids)) + "  " + str(
			thread_end_time - thread_start_time))


# mail delivery driver
# Odoo RPC is used to hold the connection alive till all the mails are delievered.
# It internally calls the mail_delievery function with chunks of 50 records
def mail_delivery_driver(thread_name, res_ids, model, mass_mailing_id, body_html, email_from, reply_to, subject,
							base_url, db_name, secret, email_selection, email_field, test_email=False,
							local_mode=False,marketing_activity_id=False):
	thread_start_time = datetime.datetime.now()
	# JSON RPC Configs
	jrpc_config = Configuration('JRPC')
	db = config['db_name']
	jrpc_client = odoorpc.ODOO(host=jrpc_config['JRPC_URL'], port=jrpc_config['JRPC_PORT'], protocol=jrpc_config['JRPC_PROTOCOL'],timeout=900)
	jrpc_client.login(db, jrpc_config['JRPC_USERNAME'], jrpc_config['JRPC_PASSWORD'])

	split_res_ids = [res_ids[i:i + 50] for i in range(0, len(res_ids), 50)]
	for chunk_res_ids in split_res_ids:
		before = datetime.datetime.now()
		jrpc_client.env['mailing.mailing'].ses_mail_delivery([mass_mailing_id], thread_name, chunk_res_ids, model, mass_mailing_id, body_html, email_from, reply_to, subject,
							   base_url, db_name, secret, email_selection, email_field, test_email, local_mode,marketing_activity_id)
		after = datetime.datetime.now()
		_logger.info(
				"######## JSONRPC Time taken to send " + str(len(chunk_res_ids)) + " mailing traces: " + str(after - before))

	thread_end_time = datetime.datetime.now()
	_logger.info('########## JSONRPC Thread time' + thread_name + ' processing done for ' + str(len(res_ids)) + "  " +str(thread_end_time - thread_start_time))

	if not test_email:
		# create Process Info
		jrpc_client.env['mass.mailing.process.info'].create(
			{
				'mailing_id': mass_mailing_id,
				'pid': os.getpid(),
				'start_time': thread_start_time.strftime("%Y-%m-%d %H:%M:%S"),
				'end_time': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
			}
		)
		# Call reconciler to update the status of the campaign
		jrpc_client.env['mailing.mailing'].reconcile_process([mass_mailing_id])


def prepend_content_to_html(html, content, plaintext=True, preserve=False, container_tag=False):
    """ Prepend extra content at the end of an HTML snippet, trying
        to locate the end of the HTML document (<body>, <html>, or
        beginning of text), and converting the provided content in html unless ``plaintext``
        is False.
        Content conversion can be done in two ways:
        - wrapping it into a pre (preserve=True)
        - use plaintext2html (preserve=False, using container_tag to wrap the
            whole content)
        A side-effect of this method is to coerce all HTML tags to
        lowercase in ``html``, and strip enclosing <html> or <body> tags in
        content if ``plaintext`` is False.

        :param str html: html tagsoup (doesn't have to be XHTML)
        :param str content: extra content to append
        :param bool plaintext: whether content is plaintext and should
            be wrapped in a <pre/> tag.
        :param bool preserve: if content is plaintext, wrap it into a <pre>
            instead of converting it into html
    """
    html = ustr(html)
    if plaintext and preserve:
        content = u'\n<pre>%s</pre>\n' % misc.html_escape(ustr(content))
    elif plaintext:
        content = '\n%s\n' % plaintext2html(content, container_tag)
    else:
        content = re.sub(r'(?i)(</?(?:html|body|head|!\s*DOCTYPE)[^>]*>)', '', content)
        content = u'\n%s\n' % ustr(content)
    # Force all tags to lowercase
    html = re.sub(r'(</?)(\w+)([ >])',
        lambda m: '%s%s%s' % (m.group(1), m.group(2).lower(), m.group(3)), html)
    insert_location = html.find('<body>')
    if insert_location == -1:
        insert_location = html.find('<html>')
    if insert_location == -1:
        return '%s%s' % (content, html)
    return '%s%s%s' % (html[:insert_location], content, html[insert_location:])


def create_mail_trace(mail_trace_list):
	rpc_config = Configuration('RPC')
	username = rpc_config['RPC_USERNAME']
	password = rpc_config['RPC_PASSWORD']
	url = rpc_config['RPC_URL']
	client.ServerProxy('{}/xmlrpc/2/common'.format(url))
	db = config['db_name']
	common = client.ServerProxy('{}/xmlrpc/2/common'.format(url))
	uid = common.authenticate(db, username, password, {})
	odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url))
	before = datetime.datetime.now()
	for x in mail_trace_list:
		try:
			odoo_models.execute_kw(db, uid, password,
									'mailing.trace', 'create',
									[x])
		except:
			try:
				odoo_models.execute_kw(db, uid, password,
										'mailing.trace', 'create',
										[x])
			except:
				_logger.error('Failed to save mailing trace: ' + str(x))
	after = datetime.datetime.now()
	_logger.info(
		"######## XMLRPC Time taken to save " + len(mail_trace_list) + " mailing traces: " + str(after - before))


def update_mailing_daemon(mailing_id,update_dict):
	_logger.error('ERROR: EM: Retrying in daemon - '+str(update_dict))
	jrpc_config = Configuration('JRPC')
	db = config['db_name']
	jrpc_client = odoorpc.ODOO(host=jrpc_config['JRPC_URL'], port=jrpc_config['JRPC_PORT'], protocol=jrpc_config['JRPC_PROTOCOL'])
	jrpc_client.login(db, jrpc_config['JRPC_USERNAME'], jrpc_config['JRPC_PASSWORD'])
	mailing = jrpc_client.env['mailing.mailing'].browse(mailing_id)
	success = False
	while not success:
		try:
			mailing.write(update_dict)
			success = True
		except Exception as ex:
			_logger.error("ERROR: EM: Retry failed in daemon - "+str(update_dict))
			time.sleep(2)
			pass


def update_server(url, payload):
	"""
	Makes a POST call to the server.
	:param url: Host
	:param payload: data
	:return: True if status is 200 else False
	"""
	headers = {
		'Content-Type': 'text/html'
	}
	response = requests.request("POST", url, headers=headers, data=payload)
	return True if response.status_code == 200 else False

def sqs_poller():
	"""
		Polls SQS Queue to get the Event Notifications
	"""

	config = Configuration('AMAZON_SES')
	events_url = Configuration('EMAIL_MARKETING')['EVENTS_URL']
	session = boto3.Session(aws_access_key_id=config['ACCESS_KEY'],
							aws_secret_access_key=config['SECRET'],
							region_name=config['REGION'])
	sqs = session.client('sqs')

	queue_url = config['SQS_QUEUE']

	iteration = 0
	while True:
		iteration += 1
		_logger.info(str(iteration) + ' SQS iteration, pid '+str(os.getpid()))
		# Receive message from SQS queue
		response = sqs.receive_message(
			QueueUrl=queue_url,
			MaxNumberOfMessages=10,
			WaitTimeSeconds=20
		)

		for message in response.get("Messages", []):
			message_body = message["Body"]
			try:
				success = update_server(events_url,message_body.encode('UTF-8'))
				receipt_handle = message['ReceiptHandle']
				if success:
					# Delete received message from queue
					sqs.delete_message(
						QueueUrl=queue_url,
						ReceiptHandle=receipt_handle
					)
				else:
					_logger.error("Error updating Notification - " + message_body)
			except Exception as ex:
				tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
				_logger.error("Error updating Notification " + message_body + "  " + tb_ex)
