# -*- coding: utf-8 -*-
import json
import logging
import datetime  # Don't remove this import.. it is used by the moksha cron
import requests
from ...isha_base.configuration import Configuration

from odoo import _
from odoo import models, fields, api
from odoo.exceptions import ValidationError, UserError
from odoo.models import regex_field_agg, VALID_AGGREGATE_FUNCTIONS
from odoo.tools import OrderedSet
from .constants import CHARITABLE_GROUPS, RELIGIOUS_GROUPS
from .. import TxnUpdater

_logger = logging.getLogger(__name__)

class Programtag (models.Model):
    _name = 'isha.program.tag'
    _description = 'Program tags'
    _order = 'name'
    _parent_store = True

    name = fields.Char(string='Tag Name', required=True, translate=True)
    color = fields.Integer(string='Color Index')
    parent_id = fields.Many2one('isha.program.tag', string='Parent Systags', index=True, ondelete='cascade')
    child_ids = fields.One2many('isha.program.tag', 'parent_id', string='Child Systags')
    active = fields.Boolean(default=True, help="The active field allows you to hide the category without removing it.")
    parent_path = fields.Char(index=True)
    partner_ids = fields.Many2many('res.partner', column1='pgm_tag_ids', column2='partner_id', string='Partners')

    @api.constrains('parent_id')
    def _check_parent_id(self):
        if not self._check_recursion():
            raise ValidationError(_('You can not create recursive tags.'))


    def name_get(self):
        """ Return the categories' display name, including their direct
            parent by default.

            If ``context['partner_category_display']`` is ``'short'``, the short
            version of the category name (without the direct parent) is used.
            The default is the long version.
        """
        if self._context.get('partner_category_display') == 'short':
            return super(Programtag, self).name_get()

        res = []
        for program in self:
            names = []
            current = program
            while current:
                names.append(current.name)
                current = current.parent_id
            res.append((program.id, ' / '.join(reversed(names))))
        return res

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        if name:
            # Be sure name_search is symetric to name_get
            name = name.split(' / ')[-1]
            args = [('name', operator, name)] + args
        partner_pgm_tag_ids = self._search(args, limit=limit, access_rights_uid=name_get_uid)
        return self.browse(partner_pgm_tag_ids).name_get()


class ReligiousTag (models.Model):
    _name = 'isha.religious.tag'
    _description = 'Religious Tags'
    _order = 'name'
    _parent_store = True

    name = fields.Char(string='Tag Name', required=True, translate=True)
    color = fields.Integer(string='Color Index')
    parent_id = fields.Many2one('isha.religious.tag', string='Parent Systags', index=True, ondelete='cascade')
    child_ids = fields.One2many('isha.religious.tag', 'parent_id', string='Child Systags')
    active = fields.Boolean(default=True, help="The active field allows you to hide the category without removing it.")
    parent_path = fields.Char(index=True)
    partner_ids = fields.Many2many('res.partner', column1='religious_tag_ids', column2='partner_id', string='Partners')

    @api.constrains('parent_id')
    def _check_parent_id(self):
        if not self._check_recursion():
            raise ValidationError(_('You can not create recursive tags.'))


    def name_get(self):
        """ Return the categories' display name, including their direct
            parent by default.

            If ``context['partner_category_display']`` is ``'short'``, the short
            version of the category name (without the direct parent) is used.
            The default is the long version.
        """
        if self._context.get('partner_category_display') == 'short':
            return super(Programtag, self).name_get()

        res = []
        for program in self:
            names = []
            current = program
            while current:
                names.append(current.name)
                current = current.parent_id
            res.append((program.id, ' / '.join(reversed(names))))
        return res

    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        if name:
            # Be sure name_search is symetric to name_get
            name = name.split(' / ')[-1]
            args = [('name', operator, name)] + args
        partner_pgm_tag_ids = self._search(args, limit=limit, access_rights_uid=name_get_uid)
        return self.browse(partner_pgm_tag_ids).name_get()


class ProgramLead (models.Model):
    _name = 'program.lead'
    _description = 'Programs Lead'
    _order = "start_date desc"
    _rec_name = 'program_name'

    guid = fields.Char(string="Golden ID", index=True)
    local_contact_id = fields.Char(string="Local Contact ID", required=False, index=True)
    system_id = fields.Integer(string="System ID", required=False, index=True)
    local_trans_id = fields.Char(string="Local Trans ID", required=False,index=True)
    reg_status = fields.Char(string="Registration Status")
    reg_date_time = fields.Datetime(string="Registration Date", required=False)
    program_schedule_info = fields.Text(name="Program Info", required=False)
    local_program_type_id = fields.Text(name="Program Type Id", required=False,index=True)
    program_name = fields.Char(string="Program Name", required=False,index=True)
    start_date = fields.Date(string="Start Date", required=False, index=True)
    event_attendance_date = fields.Datetime(string="Attendance Date", required=False, index=True)
    teacher_name = fields.Char(string="Teacher Name", required=False)
    center_name = fields.Char(string="Center Name", required=False, index=True)
    active = fields.Boolean(string='active', default=True)
    bay_name = fields.Char(string='Bay Name')
    arrival_date = fields.Datetime(string='Arrival Date')
    departure_date = fields.Datetime(string='Departure Date')
    ie_hear_about = fields.Char(string='How did you hear about Inner Engineering?')
    utm_medium = fields.Char(string='UTM Medium')
    utm_campaign = fields.Char(string='UTM Campaign')
    utm_source = fields.Char(string='UTM Source')
    utm_term = fields.Char(string='UTM Term')
    utm_content = fields.Char(string='UTM Content')
    utm_referer = fields.Char(string='UTM Referer')
    ieo_class_status = fields.Char(string='IEO Class Status')
    checkin_status_day_2 = fields.Char(string='Checkin status day 2')
    whats_app_group = fields.Char(string='WhatsApp Group')
    trans_lang = fields.Char(string='Language')
    is_ie_done = fields.Boolean(string='Are you a Meditator')
    pgm_remark = fields.Char(string='Remarks')
    record_name = fields.Char(string='Contact Name')
    record_phone = fields.Char(string='Contact Phone')
    record_email = fields.Char(string='Contact Email')
    record_city = fields.Char(string='Contact City')
    record_state = fields.Char(string='Contact State')
    record_zip = fields.Char(string='Contact Zip')
    record_country = fields.Char(string='Submitted Country')
    record_nationality = fields.Char(string='Contact Nationality')
    record_center_id = fields.Many2one(string='Contact Center', comodel_name='isha.center', ondelete='set null')
    sso_id = fields.Char(string='SSO ID', index=True)

    webinar_event_id = fields.Char(string='Event Id')
    webinar_event_name = fields.Char(string='Event Name')
    webinar_event_date = fields.Datetime(string='Event Date')
    last_api_called = fields.Datetime(string='Date Verification Sent')
    #contact for the program_attendance
    query_from_user = fields.Text(string="Question From User", required=False)
    contact_id_fkey = fields.Many2one(string="Contact ID",comodel_name='res.partner', delegate=True, auto_join=True, index=True)
    pgm_type_master_id = fields.Many2one(string="Program Type ID",comodel_name='program.type.master', delegate=True,index=True)
    form_language = fields.Char(string="Form Language", help="The language in which the form was rendered")
    #new field added for PRS data:
    batch = fields.Char(string="Batch")

    reg_number = fields.Char(string='Registration number')
    meeting_id = fields.Char(string='Meeting ID')
    meeting_registrant_id = fields.Char(string='Meeting Registrant ID')
    additional_remarks = fields.Char(string='Additional Remarks')
    meeting_number = fields.Char(string='Meeting number')
    meeting_url = fields.Char(string='Meeting url')
    check_in_status = fields.Char(string='Check-in')
    day_1_attendance = fields.Char(string='Day 1 Attendance')
    day_1_duration_missed = fields.Integer(string='Day 1 Duration missed')
    day_2_attendance = fields.Char(string='Day 2 Attendance')
    day_2_duration_missed = fields.Integer(string='Day 2 Duration missed')
    day_3_attendance = fields.Char(string='Day 3 Attendance')
    day_3_duration_missed = fields.Integer(string='Day 3 Duration missed')
    day_4_attendance = fields.Char(string='Day 4 Attendance')
    day_4_duration_missed = fields.Integer(string='Day 4 Duration missed')
    day_5_attendance = fields.Char(string='Day 5 Attendance')
    day_5_duration_missed = fields.Integer(string='Day 5 Duration missed')
    sessions_attended = fields.Integer(string='Sessions Attended')
    support_needed = fields.Boolean(string='Support Needed')
    qualification = fields.Char(string='Qualification')
    agreed_to_terms = fields.Boolean(string='Agreed to Terms')

    selectable_fields = ['local_trans_id','reg_date_time','pgm_remark','webinar_event_id','webinar_event_name',
                         'webinar_event_date','program_name','contact_id_fkey','event_attendance_date','start_date',
                         'reg_status','trans_lang','last_api_called','name','phone','email','zip','whatsapp_number',
                         'whats_app_group','reg_date_time','region_name','center_id', 'nurturing_volunteer',
                         'pgm_tag_ids','category_id','bay_name' ,'country_id','state','city','gender','dnd_phone',
                         'dnd_sms','dnd_postmail','dnd_wa_ext','dnd_email','low_matches_count','ieo_date','ieo_r_date',
                         'ie_date','bsp_date','shoonya_date','samyama_date', 'record_country',
                         'sub_center','micro_center','sessions_attended', 'batch']

    def mark_unable_to_verify(self):
        not_verified_id = self.env['res.partner.category'].search([('name', '=', 'Unable to Verify')])[0].id
        self.contact_id_fkey.write({'category_id': [(4, not_verified_id)]})

    def cancel_registration(self):
        self.write({'reg_status': 'Cancelled'})
        not_verified_id = self.env['res.partner.category'].search([('name', '=', 'Unable to Verify')])[0].id
        self.contact_id_fkey.write({'category_id': [(3, not_verified_id)]})

    def trigger_create_pgm_att(self):
        view_id = self.env.ref('isha_crm.program_verification_wizard_form').id
        context = dict(self._context or {})
        return {
            'name': 'Manual Attendance Entry Form',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'isha.online.sathsang.api.wizard',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': context
        }

    @api.model
    def _read_group_raw(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        """
            Overrid for additional functionality of adding HAVING clause in groupby
        """
        self.check_access_rights('read')
        query = self._where_calc(domain)
        fields = fields or [f.name for f in self._fields.values() if f.store]

        groupby = [groupby] if isinstance(groupby, str) else list(OrderedSet(groupby))
        groupby_list = groupby[:1] if lazy else groupby
        annotated_groupbys = [self._read_group_process_groupby(gb, query) for gb in groupby_list]
        groupby_fields = [g['field'] for g in annotated_groupbys]
        order = orderby or ','.join([g for g in groupby_list])
        groupby_dict = {gb['groupby']: gb for gb in annotated_groupbys}

        self._apply_ir_rules(query, 'read')
        for gb in groupby_fields:
            assert gb in self._fields, "Unknown field %r in 'groupby'" % gb
            gb_field = self._fields[gb].base_field
            assert gb_field.store and gb_field.column_type, "Fields in 'groupby' must be regular database-persisted fields (no function or related fields), or function fields with store=True"

        aggregated_fields = []
        select_terms = []
        fnames = []  # list of fields to flush

        for fspec in fields:
            if fspec == 'sequence':
                continue
            if fspec == '__count':
                # the web client sometimes adds this pseudo-field in the list
                continue

            match = regex_field_agg.match(fspec)
            if not match:
                raise UserError(_("Invalid field specification %r.") % fspec)

            name, func, fname = match.groups()
            if func:
                # we have either 'name:func' or 'name:func(fname)'
                fname = fname or name
                field = self._fields.get(fname)
                if not field:
                    raise ValueError("Invalid field %r on model %r" % (fname, self._name))
                if not (field.base_field.store and field.base_field.column_type):
                    raise UserError(_("Cannot aggregate field %r.") % fname)
                if func not in VALID_AGGREGATE_FUNCTIONS:
                    raise UserError(_("Invalid aggregation function %r.") % func)
            else:
                # we have 'name', retrieve the aggregator on the field
                field = self._fields.get(name)
                if not field:
                    raise ValueError("Invalid field %r on model %r" % (name, self._name))
                if not (field.base_field.store and
                        field.base_field.column_type and field.group_operator):
                    continue
                func, fname = field.group_operator, name

            fnames.append(fname)

            if fname in groupby_fields:
                continue
            if name in aggregated_fields:
                raise UserError(_("Output name %r is used twice.") % name)
            aggregated_fields.append(name)

            expr = self._inherits_join_calc(self._table, fname, query)
            if func.lower() == 'count_distinct':
                term = 'COUNT(DISTINCT %s) AS "%s"' % (expr, name)
            else:
                term = '%s(%s) AS "%s"' % (func, expr, name)
            select_terms.append(term)

        for gb in annotated_groupbys:
            select_terms.append('%s as "%s" ' % (gb['qualified_field'], gb['groupby']))

        self._flush_search(domain, fields=fnames + groupby_fields)

        groupby_terms, orderby_terms = self._read_group_prepare(order, aggregated_fields, annotated_groupbys, query)
        from_clause, where_clause, where_clause_params = query.get_sql()
        if lazy and (len(groupby_fields) >= 2 or not self._context.get('group_by_no_leaf')):
            count_field = groupby_fields[0] if len(groupby_fields) >= 1 else '_'
        else:
            count_field = '_'
        count_field += '_count'

        prefix_terms = lambda prefix, terms: (prefix + " " + ",".join(terms)) if terms else ''
        prefix_term = lambda prefix, term: ('%s %s' % (prefix, term)) if term else ''
        having_clause = self._context.get('having_clause', None)
        # if having_clause:
        # 	print(groupby)
        # 	error_flag = False if 'contact_id_fkey' in groupby else True
        # 	if error_flag:
        # 		raise UserError(_("Please Add \"Contact\" from Group By or remove the \"Satsang Attended\" filter!!!."))
        query = """
            SELECT min("%(table)s".id) AS id, count("%(table)s".id) AS "%(count_field)s" %(extra_fields)s
            FROM %(from)s
            %(where)s
            %(groupby)s
            %(having)s
            %(orderby)s
            %(limit)s
            %(offset)s
        """ % {
            'table': self._table,
            'count_field': count_field,
            'extra_fields': prefix_terms(',', select_terms),
            'from': from_clause,
            'where': prefix_term('WHERE', where_clause),
            'groupby': prefix_terms('GROUP BY', groupby_terms),
            'having': having_clause if having_clause else '',
            'orderby': prefix_terms('ORDER BY', orderby_terms),
            'limit': prefix_term('LIMIT', int(limit) if limit else None),
            'offset': prefix_term('OFFSET', int(offset) if limit else None),
        }
        self._cr.execute(query, where_clause_params)
        fetched_data = self._cr.dictfetchall()

        if not groupby_fields:
            return fetched_data

        self._read_group_resolve_many2one_fields(fetched_data, annotated_groupbys)

        data = [{k: self._read_group_prepare_data(k, v, groupby_dict) for k, v in r.items()} for r in fetched_data]

        if self.env.context.get('fill_temporal') and data:
            data = self._read_group_fill_temporal(data, groupby, aggregated_fields,
                                                  annotated_groupbys)

        result = [self._read_group_format_result(d, annotated_groupbys, groupby, domain) for d in data]

        if lazy:
            # Right now, read_group only fill results in lazy mode (by default).
            # If you need to have the empty groups in 'eager' mode, then the
            # method _read_group_fill_results need to be completely reimplemented
            # in a sane way
            result = self._read_group_fill_results(
                domain, groupby_fields[0], groupby[len(annotated_groupbys):],
                aggregated_fields, count_field, result, read_group_order=order,
            )
        return result

    @api.model
    def load_views(self, views, options=None):
        context = self._context or {}
        if context.get('global_search'):
            global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
            self = self.with_user(global_data_user_id)
        return super(ProgramLead, self).load_views(views, options)

    def read(self, fields=None, load='_classic_read'):
        context = self._context or {}
        if context.get('global_search'):
            global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
            self = self.with_user(global_data_user_id)
        return super(ProgramLead, self).read(fields, load)

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self._context or {}
        # this is intended to separate the access to global search from the rest to keep it cleaner.
        # to use it, we can pass the 'global_search' in the context from the 'global search / Contacts' menu
        # Then we won't need to open up all contacts to the users. We can remove the record rule for Global search access.
        # The access to global data would then be only available to a special user. We would fetch the data with this
        # user's privileges and show to in the Global search results.
        if context.get('global_search'):
            global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
            self = self.with_user(global_data_user_id)
        return super(ProgramLead, self).search(args, offset, limit, order, count)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        context = self._context or {}
        if context.get('center_scope'):
            domain += [('center_id','in', self.env.user.centers.ids)] #,('program_name','=','Sadhana Support COVID-19'),('name','!=','OdooBot')]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('has_charitable_txn', '=', True)]
            if religious:
                rel_char_domain = (['|', ('has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('has_religious_txn', '=', True)]
            domain = rel_char_domain + domain
        return super(ProgramLead, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                   orderby=orderby, lazy=lazy)

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}
        # if self._context.get('having_clause', None):
        # 	raise UserError(_("Please Add \"Contact\" from Group By or remove the \"Satsang Attended\" filter!!!."))
        if context.get('center_scope'):
            args += [('center_id','in', self.env.user.centers.ids)] #,('program_name','=','Sadhana Support COVID-19'),('name','!=','OdooBot')]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('has_charitable_txn', '=', True)]
            if religious:
                rel_char_domain = (['|', ('has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('has_religious_txn', '=', True)]
            args = rel_char_domain + args

        # default - do not show any results
        if len(args) == 0:
            return []
        return super(ProgramLead, self)._search(args, offset, limit, order, count=count, access_rights_uid=access_rights_uid)

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(ProgramLead, self).fields_get(allfields, attributes=attributes)
        if self._context.get('restrict_fields_get_list'):
            not_selectable_fields = set(res.keys()) - set(self._context.get('restrict_fields_get_list'))
        else:
            not_selectable_fields = set(res.keys()) - set(self.selectable_fields)

        for field in not_selectable_fields:
            res[field]['selectable'] = False # to hide in Add Custom filter view
            res[field]['sortable'] = False # to hide in group by view
            #res[field]['exportable'] = False # to hide in export list
            res[field]['store'] = False # to hide in 'Select Columns' filter in tree views
        return res

    def contact_population(self,limit):
        start = datetime.datetime.now()
        recs = self.env['program.lead'].sudo().search([('contact_id_fkey', '=', 2),('program_name','in',[
                        "Online SCK Satsang","Online Satsang","Online IEO Satsang",
                        "Online Samyama Satsang","Online Pournami Satsang","Online Guided Shambhavi - 21 Days","Online Ananda Alai"])], limit=limit)
        for rec in recs:
            schedule = eval(rec['program_schedule_info'])
            if 'pii' in schedule:
                pii = schedule['pii']
                if not pii.get('phone_country_code', False):
                    pii.pop('phone', False)
                    pii.pop('phone_country_code', False)
                pii.update({'system_id': 44, 'local_contact_id': rec['id']})
                if rec.program_name == 'Online Pournami Satsang':
                    pii.update({'fmf_reg':True, 'pgm_tag_ids':[(4,self.env.ref('isha_crm.res_partner_pgm_tag_23').id)]})
                res_partner = self.env['res.partner'].sudo().create(pii)
                _logger.info("moksha satsang pii cron "+str(res_partner))
                rec.write({'contact_id_fkey': res_partner.id})
                self.env.cr.commit()

    def verify_pending_merged_records(self,limit=500):
        start = datetime.datetime.now().strftime("%Y-%m-%d")
        recs = self.search([('contact_id_fkey', '!=', 2),('program_name','in',['IE mandala',
                        'Online SCK Satsang','Online Satsang','Online IEO Satsang','Online Ananda Alai',
                        'Online Guided Shambhavi - 21 Days']),('reg_status','in',['PENDING','PENDING CONFIRMATION']),
                            ('start_date','>=',start),'|','|',('is_meditator','=',True),('shoonya_date','!=',False),
                            ('samyama_date','!=',False)], limit=limit)
        for rec in recs:
            if rec.contact_id_fkey.is_meditator and rec.program_name in ('Online Satsang','IE mandala','Online IEO Satsang','Online Ananda Alai','Online Guided Shambhavi - 21 Days'):
                rec.verify_pgm_lead()
                _logger.info("pending merged records cron " + str(rec.contact_id_fkey))
            elif rec.program_name == "Online SCK Satsang" and (rec.contact_id_fkey.shoonya_date or rec.contact_id_fkey.samyama_date):
                rec.verify_pgm_lead()
                _logger.info("pending merged records cron " + str(rec.contact_id_fkey))

    def verify_pgm_lead(self):
        rec = self

        rec.write({'reg_status': 'CONFIRMED'})

        # should remove the unable to verify tag once manually verified
        not_verified_id = self.env['res.partner.category'].search([('name', '=', 'Unable to Verify')])[0].id
        rec.contact_id_fkey.write({'category_id': [(3, not_verified_id)]})

        # Mandala verification email to be sent
        if rec.program_name == 'IE mandala':
            self.env['isha.online.sathsang.api.wizard'].sudo().send_email_notif(rec)
            self.env['isha.online.sathsang.api.wizard'].sudo().mark_meditator(rec)
        # For satsang manual verification set the entitlement and send out an email
        if 'satsang' in rec.program_name.lower() or rec.program_name == 'Online Ananda Alai':
            self.env['isha.online.sathsang.api.wizard'].sudo().call_prs_api(rec)

    def verify_registration(self):
        self.verify_pgm_lead()
        return self.env['sh.message.wizard'].get_popup('Verified Successfully and Email is sent')

    def resend_confirmation_email(self):
        transprogramlead_rec = self.env['transprogramlead'].sudo().create({
            'record_name': self.record_name,
            'record_phone': self.record_phone,
            'record_email': self.record_email,
            'program_name': 'Online Ananda Alai',
            'tzlangregion': self.bay_name
        })
        self.env['satsang.notification'].sudo().sendRegistrationEmailSMSCore(transprogramlead_rec)
        return self.env['sh.message.wizard'].get_popup('Email is sent!')

    def bulk_assign_eligible_subs(self):
        view = self.env.ref('isha_crm.bulk_eligible_subs_act_window_wizard_form')
        view_id = view and view.id or False
        context = dict(self._context or {},
                       region_scope=True,
                       mapped_field='contact_id_fkey',
                       mapped_model='res.partner',
                       restrict_fields='ml_subscription_ids',
                       restrict_values=[('child_ids','=',False)])
        return {
            'name': 'Bulk Modify Eligible Subscriptions',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'm2m.tag.manipulation',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': context
        }

class ProgramAttendance (models.Model):
    _name = 'program.attendance'
    _description = 'Programs attended'
    _order = "start_date desc"
    _rec_name = 'program_name'

    guid = fields.Char(string="Golden ID", index=True)
    local_contact_id = fields.Char(string="Local Contact ID", required=False, index=True)
    system_id = fields.Integer(string="System ID", required=False, index=True)
    local_trans_id = fields.Char(string="Local Trans ID", required=False,index=True)
    reg_status = fields.Char(string="Registration Status")
    reg_date_time = fields.Datetime(string="Registration Date", required=False)
    program_schedule_info = fields.Text(string="Program Info", required=False)
    local_program_type_id = fields.Text(string="Local Program Type Id", required=False, index=True)
    program_name = fields.Char(string="Program Name", required=False)
    start_date = fields.Date(string="Start Date", required=False, index=True)
    end_date = fields.Date(string="End Date", required=False, index=True)
    teacher_name = fields.Char(string="Teacher Name", required=False)
    center_name = fields.Char(string="Program Center Name", required=False, index=True)
    program_country_code = fields.Char(string="Program Country ISO2", required=False)
    active = fields.Boolean(string='active', default=True)
    bay_name = fields.Char(string='Bay Name')
    arrival_date = fields.Datetime(string='Arrival Date')
    departure_date = fields.Datetime(string='Departure Date')
    whats_app_group = fields.Char(string='WhatsApp Group')
    utm_campaign = fields.Char(string='UTM Campaign')
    utm_content = fields.Char(string='UTM Content')
    utm_medium = fields.Char(string='UTM Medium')
    utm_source = fields.Char(string='UTM Source')
    utm_term = fields.Char(string='UTM Term')
    utm_referer = fields.Char(string='UTM Referer')
    trans_lang = fields.Char(string='Language')
    sso_id = fields.Char(string='SSO ID')
    #email for registered email IDs
    record_email = fields.Char(string='Record Email')

    #contact for the program_attendance
    contact_id_fkey = fields.Many2one(string="Contact ID",comodel_name='res.partner', delegate=True, auto_join=True, index=True)
    partner_id = fields.Integer(string='Partner ID', related='contact_id_fkey.id')
    pgm_type_master_id = fields.Many2one(string="Program Type ID",comodel_name='program.type.master', delegate=True, index=True)

    @api.model
    def load_views(self, views, options=None):
        context = self._context or {}
        if context.get('global_search'):
            global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
            self = self.with_user(global_data_user_id)
        return super(ProgramAttendance, self).load_views(views, options)

    def read(self, fields=None, load='_classic_read'):
        context = self._context or {}
        if context.get('global_search'):
            global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
            self = self.with_user(global_data_user_id)
        return super(ProgramAttendance, self).read(fields, load)

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self._context or {}
        if context.get('global_search'):
            global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
            self = self.with_user(global_data_user_id)
        return super(ProgramAttendance, self).search(args, offset, limit, order, count)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        context = self._context or {}
        if context.get('center_scope'):
            domain += [('center_id','in', self.env.user.centers.ids)]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('has_charitable_txn', '=', True)]
            if religious:
                rel_char_domain = (['|', ('has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('has_religious_txn', '=', True)]
            domain = rel_char_domain + domain

        return super(ProgramAttendance, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                         orderby=orderby, lazy=lazy)

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(ProgramAttendance, self).fields_get(allfields, attributes=attributes)
        if self._context.get('restrict_fields_get_list'):
            not_selectable_fields = set(res.keys()) - set(self._context.get('restrict_fields_get_list'))
            for field in not_selectable_fields:
                res[field]['selectable'] = False # to hide in Add Custom filter view
                res[field]['sortable'] = False # to hide in group by view
                #res[field]['exportable'] = False # to hide in export list
                res[field]['store'] = False # to hide in 'Select Columns' filter in tree views
        return res

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}
        if context.get('center_scope'):
            args += [('center_id', 'in', self.env.user.centers.ids)]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('has_charitable_txn', '=', True)]
            if religious:
                rel_char_domain = (['|', ('has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('has_religious_txn', '=', True)]
            args = rel_char_domain + args

        # default - do not show any results
        if len(args) == 0:
            return []
        return super(ProgramAttendance, self)._search(args, offset, limit, order, count=count, access_rights_uid=access_rights_uid)


    @api.model_create_multi
    def create(self, vals_list):
        for val in vals_list:
            if 'pgm_type_master_id' not in val: # it is import flow
                txn = TxnUpdater.ProgramAttUpdater()
                val['pgm_type_master_id'] = txn.getPgmMaserId(self.env,val)

                pgm_mst = self.env['program.type.master'].search(
                    [('local_program_type_id', '=', val['local_program_type_id']), ('system_id', '=', int(val['system_id']))])
                if pgm_mst:
                    if pgm_mst[0].is_religious:
                        val['has_religious_txn'] = True
                    else:
                        val['has_charitable_txn'] = True
            val['local_modified_date'] = val['reg_date_time']

        ret_vals = super(ProgramAttendance, self).create(vals_list)
        return ret_vals

    @api.model
    def process_update_computed_fields(self):
        pgms = self.env['program.attendance'].search([('contact_id_fkey', '=', 2)])
        for x in pgms:
            contact = self.env['contact.map'].search([('local_contact_id', '=', x['local_contact_id']),('system_id','=',x['system_id'])])
            if len(contact) > 0:
                x.write({'contact_id_fkey':contact[0]['contact_id_fkey']})

        pgms = self.env['program.lead'].search([('contact_id_fkey', '=', 2)])
        for x in pgms:
            contact = self.env['contact.map'].search([('local_contact_id', '=', x['local_contact_id']),('system_id','=',x['system_id'])])
            if len(contact) > 0:
                x.write({'contact_id_fkey':contact[0]['contact_id_fkey']})

    # _logger.debug('Cron Job Completed Programs update')

    # entities = ['donation.isha.foundation', 'donation.isha.outreach', 'donation.isha.education'
    # 			, 'donation.isha.arogya', 'donation.isha.iii']
    # for entity in entities:
    # 	donations = self.env[entity].search([('contact_id_fkey', '=', 2)])
    # 	for x in donations:
    # 		contact = self.env['contact.map'].search([('local_contact_id', '=', x['local_patron_id']),('system_id','=',x['system_id'])])
    # 		if len(contact) > 0:
    # 			x.write({'contact_id_fkey':contact[0]['contact_id_fkey']})
    # _logger.debug('Cron Job Completed Donation update')

    # Get program attendance for contact_id_fkey and list of pgm_category
    def getProgramAttendance(self, contact_id, pgm_category=None):
        if pgm_category:
            filter_lambda = lambda r: r.pgm_type_master_id.category in pgm_category
        else:
            filter_lambda = lambda r: True

        pgm_recs = self.env['program.attendance'].search([('contact_id_fkey', '=', contact_id)]).filtered(
            filter_lambda)
        return pgm_recs


    # Create a program_attendance
    def createPgmAtt(self, trans_id, pgm_type_master_id, reg_status, reg_date_time, start_date, end_date, arrival_date,
                     departure_date, partner_id):
        pgm_master_rec = self.env['program.type.master'].browse(pgm_type_master_id)
        pgm_txn = {
            'local_contact_id': trans_id,
            'local_trans_id': trans_id,
            'reg_status': reg_status,
            'reg_date_time': reg_date_time,
            'system_id': pgm_master_rec.system_id,
            'local_program_type_id': pgm_type_master_id.local_program_type_id,
            'pgm_type_master_id': pgm_type_master_id,
            'start_date': start_date,
            'end_date': end_date,
            'arrival_date': arrival_date,
            'departure_date': departure_date,
            'contact_id_fkey': partner_id
        }
        pgm_rec = self.env['program.attendance'].create(pgm_txn)
        return pgm_rec

    def mark_inactive(self):
        self.write({'active': False})

    def bulk_assign_eligible_subs(self):
        view = self.env.ref('isha_crm.bulk_eligible_subs_act_window_wizard_form')
        view_id = view and view.id or False
        context = dict(self._context or {},
                       region_scope=True,
                       mapped_field='contact_id_fkey',
                       mapped_model='res.partner',
                       restrict_fields='ml_subscription_ids',
                       restrict_values=[('child_ids','=',False)])
        return {
            'name': 'Bulk Modify Eligible Subscriptions',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'm2m.tag.manipulation',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': context
        }


class IEOProgram (models.Model):
    _name = 'ieo.record'
    _description = "Inner engineering Online"
    _rec_name = 'contact_id_fkey'
    _order = "start_date desc"


    active = fields.Boolean(string='Active',default=True)
    local_user_id = fields.Integer(string="Local User ID",index=True)
    first_name = fields.Char(string='First Name')
    last_name = fields.Char(string='Last Name')
    email = fields.Char(string='Record Email')
    sex = fields.Char(string='Record Gender')
    resident_address1 = fields.Char(string='Record  Address 1')
    resident_address2 = fields.Char(string='Record Address 2')
    resident_city = fields.Char(string='Record City')
    resident_state = fields.Char(string='Record State')
    resident_zip = fields.Char(string='Record Zip')
    resident_country = fields.Char(string='Record Country')
    cust_type = fields.Char(string="Cust Type")
    last_upd_time = fields.Datetime(string='Last Updated Time')
    class_id = fields.Integer(string='Class No')
    course_completion_date = fields.Date(string='Course Completion Date')
    start_date = fields.Datetime(string='Start date')
    end_date = fields.Datetime(string='End date')
    last_login_date = fields.Date(string='Last Login Date')
    date_venue_id = fields.Integer(string='Venue Id')
    coupon = fields.Char(string='Coupon Code')
    lang_pref = fields.Char(string='Language Preference')
    record_center_id = fields.Many2one(string='Record Center', comodel_name='isha.center', ondelete='set null')
    record_region_id = fields.Many2one(string='Record Region', related='record_center_id.region_id', store=True)
    contact_id_fkey = fields.Many2one(string='Contact', comodel_name='res.partner',ondelete='set null',auto_join=True, index=True)

    # related fields
    name = fields.Char(string='Partner Name', related='contact_id_fkey.name', store=False)

    # related fields from res_partner for email marketing
    email_validity = fields.Selection(related='contact_id_fkey.email_validity',store=True)
    dnd_email = fields.Boolean(related='contact_id_fkey.dnd_email', store=True)
    deceased = fields.Boolean(related='contact_id_fkey.deceased', store=True)
    is_caca_donor = fields.Boolean(related='contact_id_fkey.is_caca_donor', store=True)
    last_txn_date = fields.Date(related='contact_id_fkey.last_txn_date', store=True)

    partner_phone = fields.Char(string='Phone' , related='contact_id_fkey.phone', store=False)
    partner_whatsapp = fields.Char(string='WhatsApp' , related='contact_id_fkey.whatsapp_number', store=False)
    partner_email = fields.Char(string='Email' , related='contact_id_fkey.email', store=False)
    partner_is_meditator = fields.Boolean(string='Meditator', related='contact_id_fkey.is_meditator', store=True)
    partner_iecso_r_date = fields.Date(string='IECSO Registered Date', related='contact_id_fkey.iecso_r_date', store=True)
    partner_dnd_phone = fields.Boolean(string='DND Phone', related='contact_id_fkey.dnd_phone', store=False)
    partner_dnd_email = fields.Boolean(string='DND Email', related='contact_id_fkey.dnd_email', store=False)
    partner_dnd_postmail = fields.Boolean(string='DND Postmail', related='contact_id_fkey.dnd_postmail', store=False)
    partner_pgm_tags = fields.Many2many(string='Program Tags', related='contact_id_fkey.pgm_tag_ids', store=False)
    partner_tags = fields.Many2many(string='Tags', related='contact_id_fkey.category_id', store=False)
    partner_street = fields.Char(string='Street', related='contact_id_fkey.street')
    partner_street2 = fields.Char(string='Street2', related='contact_id_fkey.street2')
    partner_city = fields.Char(string='City', related='contact_id_fkey.city')
    partner_state = fields.Char(string='State', related='contact_id_fkey.state')
    partner_country = fields.Char(string='Country Text', related='contact_id_fkey.country')
    country_id = fields.Many2one(string='Registered Country', related='contact_id_fkey.country_id', store=True)
    partner_center_id = fields.Many2one(string='Center', related='contact_id_fkey.center_id', store=True)
    partner_region_id = fields.Many2one(string='Region', related='partner_center_id.region_id', store=True)
    partner_ieo_date = fields.Date(string='IEO date', related='contact_id_fkey.ieo_date', store=True)
    partner_ie_date = fields.Date(string='IE date', related='contact_id_fkey.ie_date', store=True)
    influencer_type = fields.Selection(string="Influencer Type", related='contact_id_fkey.influencer_type', store=True)
    sso_id = fields.Char(string='SSO ID')

    # computed field
    progress_selection = [('1_not_started', 'Not Started'), ('2_started', 'Started'),
                          ('3_class_1', 'Class 1 Completed'), ('4_class_2', 'Class 2 Completed'),
                          ('5_class_3', 'Class 3 Completed'), ('6_class_4', 'Class 4 Completed'),
                          ('7_class_5', 'Class 5 Completed'), ('8_class_6', 'Class 6 Completed'),
                          ('90_class_7', 'Class 7 Completed'), ('91_course_completed', 'Course Completed'),
                          ('92_unknown', 'Unknown')]
    progress = fields.Selection(progress_selection,
                                string="Class Completed", compute='_compute_class_progress', store=True)

    @api.model
    def load_views(self, views, options=None):
        context = self._context or {}
        if context.get('global_search'):
            global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
            self = self.with_user(global_data_user_id)
        return super(IEOProgram, self).load_views(views, options)

    def read(self, fields=None, load='_classic_read'):
        context = self._context or {}
        if context.get('global_search'):
            global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
            self = self.with_user(global_data_user_id)
        return super(IEOProgram, self).read(fields, load)

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self._context or {}
        if context.get('global_search'):
            global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
            self = self.with_user(global_data_user_id)
        return super(IEOProgram, self).search(args, offset, limit, order, count)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        if 'class_id' in fields:
            fields.remove('class_id')
        context = self._context or {}
        if context.get('center_scope'):
            domain += [('partner_center_id','in', self.env.user.centers.ids)]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('contact_id_fkey.has_charitable_txn', '=', True)]
            if religious:
                rel_char_domain = (['|', ('contact_id_fkey.has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('contact_id_fkey.has_religious_txn', '=', True)]
            domain = rel_char_domain + domain

        return super(IEOProgram, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                  orderby=orderby, lazy=lazy)

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}

        if context.get('center_scope'):
            args += [('partner_center_id','in', self.env.user.centers.ids)]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('contact_id_fkey.has_charitable_txn', '=', True)]
            if religious:
                rel_char_domain = (['|', ('contact_id_fkey.has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('contact_id_fkey.has_religious_txn', '=', True)]
            args = rel_char_domain + args

        # default - do not show any results
        if len(args) == 0:
            return []
        return super(IEOProgram, self)._search(args, offset, limit, order, count=count, access_rights_uid=access_rights_uid)

    selectable_fields = ['lang_pref','partner_whatsapp','partner_dnd_phone','partner_dnd_email','partner_dnd_postmail',
                         'contact_id_fkey','partner_phone','partner_email','partner_is_meditator','partner_ie_date',
                         'partner_city','partner_state','partner_center_id','partner_street','partner_tags',
                         'partner_region_id','start_date','end_date','last_login_date','coupon','partner_street2',
                         'course_completion_date', 'progress', 'partner_ieo_date', 'country_id','partner_pgm_tags']

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(IEOProgram, self).fields_get(allfields, attributes=attributes)
        not_selectable_fields = set(res.keys()) - set(self.selectable_fields)
        for field in not_selectable_fields:
            res[field]['selectable'] = False # to hide in Add Custom filter view
            res[field]['sortable'] = False # to hide in group by view
            #res[field]['exportable'] = False # to hide in export list
            res[field]['store'] = False # to hide in 'Select Columns' filter in tree views
        return res

    @api.depends('class_id','last_login_date','course_completion_date')
    def _compute_class_progress(self):
        for rec in self:
            if rec.course_completion_date:
                rec.progress = '91_course_completed'
            else:
                if rec.class_id == 1:
                    if not rec.last_login_date:
                        rec.progress = '1_not_started'
                    elif rec.last_login_date:
                        rec.progress = '2_started'
                    else:
                        rec.progress = None
                elif rec.class_id == 2:
                    rec.progress = '3_class_1'
                elif rec.class_id == 3:
                    rec.progress = '4_class_2'
                elif rec.class_id == 4:
                    rec.progress = '5_class_3'
                elif rec.class_id == 5:
                    rec.progress = '6_class_4'
                elif rec.class_id == 6:
                    rec.progress = '7_class_5'
                elif rec.class_id == 7:
                    rec.progress = '8_class_6'
                elif rec.class_id > 7:
                    rec.progress = '92_unknown'
                else:
                    rec.progress = None

    def bulk_assign_eligible_subs(self):
        view = self.env.ref('isha_crm.bulk_eligible_subs_act_window_wizard_form')
        view_id = view and view.id or False
        context = dict(self._context or {},
                       region_scope=True,
                       mapped_field='contact_id_fkey',
                       mapped_model='res.partner',
                       restrict_fields='ml_subscription_ids',
                       restrict_values=[('child_ids','=',False)])
        return {
            'name': 'Bulk Modify Eligible Subscriptions',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'm2m.tag.manipulation',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': context
        }


class SadhguruExclusive(models.Model):
    _name = 'sadhguru.exclusive'
    _description = 'Sadhguru Exclusive'
    _order = "write_date desc"

    local_trans_id = fields.Char(string='Local Trans ID',index=True)
    active = fields.Boolean(string='active')
    email = fields.Char(string='Record Email')
    contact_id_fkey = fields.Many2one(string="Contact ID",comodel_name='res.partner', delegate=True, auto_join=True, index=True)
    current = fields.Boolean(string='Currently Active')
    reg_status = fields.Char(string='Registration Status', required=False)
    inquiry_date = fields.Date(string='Inquiry Date')
    subscription_date = fields.Datetime(string='Subscription Date')
    subscription_expiry_date = fields.Datetime(string='Subscription Expiry Date')
    subscription_type = fields.Char(string='Subscription Type')
    amount = fields.Float(string='Amount',default=0)
    currency = fields.Char(srting='Currency')
    currency_region = fields.Char(srting='Currency Region')
    cancellation_date = fields.Datetime(string='Cancellation Date')
    cancellation_reason = fields.Char(string='Cancellation reason')
    last_cancellation_date  = fields.Datetime(string='Last Cancellation Date')
    cancellation_count = fields.Integer(string='Cancellation Count',default=0)
    country = fields.Char(string='Registered Country')
    sgx_inquired = fields.Boolean(string='SgX Inquired')
    new_contact_created = fields.Boolean(string='Contact Created')
    utm_campaign = fields.Char(string='UTM Campaign')
    utm_content = fields.Char(string='UTM Content')
    utm_medium = fields.Char(string='UTM Medium')
    utm_source = fields.Char(string='UTM Source')
    utm_term = fields.Char(string='UTM Term')
    sso_id = fields.Char(string='SSO ID', index=True)
    modified_ts = fields.Datetime(string='Modified Ts')
    trial_days = fields.Integer(string='Trial Days')
    trial_conversion = fields.Boolean(string='Trial Conversion', default=False)
    renewal_type = fields.Char(string='Renewal Type')
    engagement_level = fields.Char(string='Engagement Level')

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(SadhguruExclusive, self).fields_get(allfields, attributes=attributes)
        if self._context.get('restrict_fields_get_list'):
            not_selectable_fields = set(res.keys()) - set(self._context.get('restrict_fields_get_list'))
            for field in not_selectable_fields:
                res[field]['selectable'] = False # to hide in Add Custom filter view
                res[field]['sortable'] = False # to hide in group by view
                res[field]['exportable'] = False # to hide in export list
                res[field]['store'] = False # to hide in 'Select Columns' filter in tree views
        return res

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        context = self._context or {}
        if context.get('center_scope'):
            domain += [('center_id','in', self.env.user.centers.ids)]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('has_charitable_txn', '=', True)]
            if religious:
                rel_char_domain = (['|', ('has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('has_religious_txn', '=', True)]
            domain = rel_char_domain + domain

        return super(SadhguruExclusive, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                         orderby=orderby, lazy=lazy)

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}
        if context.get('center_scope'):
            args += [('center_id','in', self.env.user.centers.ids)]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('has_charitable_txn', '=', True)]
            if religious:
                rel_char_domain = (['|', ('has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('has_religious_txn', '=', True)]
            args = rel_char_domain + args

        return super(SadhguruExclusive, self)._search(args, offset, limit, order, count=count, access_rights_uid=access_rights_uid)

    def send_sms_text_message(self,url,params):
        response = requests.get(url, params)
        _logger.info('SMS Campaign : ' + str(params) + ' \n ' + str(response.text))

    def send_whatsapp_message(self, template_id):
        crm_config = Configuration('CRM')

        url = crm_config['YELLOW_MESSENGER_WEBHOOK_URL']
        bot_id = crm_config['YELLOW_MESSENGER_BOT_ID']
        auth_token = crm_config['YELLOW_MESSENGER_AUTH_TOKEN']
        sender = crm_config['YELLOW_MESSENGER_AUTH_SENDER']

        querystring = {"bot": bot_id}

        payload = '''{   
    					"userDetails": {
    	       				"number": "%s"
    	   				},
    	   				"notification": {
    	   				    "templateId": "%s",
    	   				    "type": "whatsapp",
    	   				    "sender": "%s"
    	   				}
    	   			}
    	''' % (str(self.whatsapp_country_code) + str(self.whatsapp_number), template_id, sender)
        headers = {
            'x-auth-token': auth_token,
            'content-type': "application/json",
            'cache-control': "no-cache"
        }

        response = requests.request("POST", url, data=payload, headers=headers, params=querystring)

        res = {
            'action': 'whatsapp',
            'message_id': json.loads(response.text)['msgId']
        }
        return res

    def bulk_assign_eligible_subs(self):
        view = self.env.ref('isha_crm.bulk_eligible_subs_act_window_wizard_form')
        view_id = view and view.id or False
        context = dict(self._context or {},
                       region_scope=True,
                       mapped_field='contact_id_fkey',
                       mapped_model='res.partner',
                       restrict_fields='ml_subscription_ids',
                       restrict_values=[('child_ids','=',False)])
        return {
            'name': 'Bulk Modify Eligible Subscriptions',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'm2m.tag.manipulation',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': context
        }

    def push_in_app_notif(self, account_id, passcode, profile_data):
        import requests
        import json
        url = "https://api.clevertap.com/1/upload"
        payload = json.dumps({
            "d": [
                {
                    "identity": self.sso_id,
                    "type": "profile",
                    "profileData": profile_data
                }
            ]
        })
        headers = {
            'X-CleverTap-Account-Id': account_id,
            'X-CleverTap-Passcode': passcode,
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
        _logger.info(response.text)


    def push_notification(self, account_id, passcode, event_name):
        import requests
        import json
        url = "https://api.clevertap.com/1/upload"
        payload = json.dumps({
            "d": [
                {
                    "type": "event",
                    "identity": self.sso_id,
                    "evtName": event_name
                }
            ]
        })
        headers = {
            'X-CleverTap-Account-Id': account_id,
            'X-CleverTap-Passcode': passcode,
            'Content-Type': 'application/json'
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        _logger.info(payload+" "+response.text)

    def add_to_cc(self, cc_id, split_by_region=False):
        if split_by_region:
            split_campaign = self.env['call_campaign.call_campaign'].sudo().search([('parent_id','=',cc_id),
                                                          ('region_id','=',self.contact_id_fkey.center_id.region_id.id)])

            if len(split_campaign) == 0:
                base_campaign = self.env['call_campaign.call_campaign'].sudo().browse(cc_id)
                split_campaign = base_campaign.copy(
                    default={'title': base_campaign.title + ' - ' + str(self.contact_id_fkey.center_id.region_id.name),
                                'user_input_ids': [(6,0,[])], 'center_id': self.contact_id_fkey.center_id.id,
                                'parent_id': base_campaign.id})

            cc_id = split_campaign[0].id


        count = self.env['call_campaign.user_input'].search_count([('call_campaign_id','=',cc_id),('partner_id','=',self.contact_id_fkey.id)])
        if count == 0:
            self.env['call_campaign.user_input'].sudo().create({
                'call_campaign_id':cc_id,
                'partner_id':self.contact_id_fkey.id,
                'dynamic_field': self._context.get('activity_name',''),
                'marketing_trace_id': self._context.get('trace_id',None)
            })

    def empty_method(self):
        import time
        time.sleep(0.2)

class ProgramScheduleMaster (models.Model):
    _name = 'program.schedule.master'
    _description = 'Programs Conducted'

    name = fields.Char(string="Schedule Name", required=True, default="New Schedule",
                       help="It can either be auto-generated via a seq or can be put in manually via the user")
    program_type_id = fields.Many2one('program.type.master',string="Program Type")
    start_date = fields.Date(string="Start Date", required=False)
    end_date = fields.Date(string="End Date", required=False)
    active = fields.Boolean(string='active', default=True)
    no_of_batch_ids = fields.One2many('program.schedule.batch.master', 'program_schedule_id', 'No of Batches')


class ProgramScheduleBatches(models.Model):
    _name = "program.schedule.batch.master"
    _description = "Program Schedule Batch Master"
    _order = "sequence"
    _rec_name = "display_text"

    sequence = fields.Integer(default=10)
    batch_no = fields.Integer("Batch No")
    start_date = fields.Datetime("Start Date")
    end_date = fields.Datetime("End Date")
    program_schedule_id = fields.Many2one('program.schedule.master')
    display_text = fields.Char("Display Text", translate=True, required=True)

    @api.constrains('start_date')
    @api.constrains('end_date')
    def _constrains_start_date(self):
        for rec in self:
            if rec.start_date > rec.end_date:
                raise UserError(_("Start date should be lesser than end date"))

            if rec.end_date < rec.start_date:
                raise UserError(_("End date should be greater than start date"))


class ProgramTypeMaster(models.Model):
    _name = 'program.type.master'
    _description = 'Program Type Master'
    _rec_name = 'program_type'

    #_sql_constraints = [('unique_prog_name', 'unique(name)','An active Program with a similar name already exists. Please enter another one.')]

    pgm_code = fields.Char(string="Key", required=False)
    local_program_type_id = fields.Char(string="Local Program Type Master", required=True, index=True) # temp to map
    system_id = fields.Integer(string="System Id", index=True)
    program_type = fields.Char(string="Program Type", required=True, index=True)
    meditator_qualifier = fields.Boolean(string="Meditator Qualifier", required=True, default=False)
    program_segment = fields.Selection([('adv_seeker','Advanced Seeker'),('seeker','Seeker'),('follower','Follower'),('follower_ref','Follower-Referral')],string="Program Segment")
    program_group = fields.Char(string="Program Group", required=False)
    level = fields.Char(string="Level", required=False)
    active = fields.Boolean(string="Active", required=True, default=True)
    category = fields.Char(string="Category", required=False, index=True)
    is_lead = fields.Boolean(string="Program Lead", default=False)
    is_covid_support = fields.Boolean(string="Covid Support", default=False)
    is_religious = fields.Boolean(string="Religious Program", default=False)
    # type_program_attendance_id_fkey = fields.One2many('program.attendance', 'actual_program_type_id')

    @api.model_create_multi
    def create(self, vals_list):
        rec = super(ProgramTypeMaster, self).create(vals_list)
        return rec

class SystemList(models.Model):
    _name = 'system.list'
    _description = 'System List'
    _rec_name = 'system_name'

    #_sql_constraints = [('unique_prog_name', 'unique(name)','An active Program with a similar name already exists. Please enter another one.')]

    system_id = fields.Integer(string="System ID", required=True)
    system_name = fields.Char(string="System Name", required=True) # temp to map
    remarks = fields.Char(string='Remarks')

    _sql_constraints = [
        ('system_id_unique', 'unique(system_id)', 'Key should be unique')
    ]

    @api.model_create_multi
    def create(self, vals_list):
        rec = super(SystemList, self).create(vals_list)
        return rec

class ProgramStatus(models.Model):
    _name = 'isha.program.status'
    _description = 'Status mapping'

    schedule_status = fields.Char(string='Schedule Status')
    system_id = fields.Integer(String='system_id')
    mapping_status = fields.Char(string='Mapping Status')
    active = fields.Boolean(string='active',default=True)



class ProgramsView(models.Model):
    _auto = False
    _name = 'programs.view.orm'
    _description = 'Consolidated Programs'

    txn_id = fields.Integer(string='Txn Id')
    contact_id_fkey = fields.Many2one('res.partner',string='Contact', auto_join=True)
    name = fields.Char(string='Name')
    phone = fields.Char(string='Phone')
    phone2 = fields.Char(string='Phone2')
    phone3 = fields.Char(string='Phone3')
    phone4 = fields.Char(string='Phone4')
    email = fields.Char(string='Email')
    email2 = fields.Char(string='Email2')
    email3 = fields.Char(string='Email3')
    whatsapp_number = fields.Char(string='WhatsApp Number')
    center_id = fields.Many2one('isha.center',string='Center')
    street = fields.Char(string='Street')
    street2 = fields.Char(string='Street2')
    city = fields.Char(string='City')
    state_id = fields.Many2one('res.country.state', string='State')
    zip = fields.Char(string="Zip")
    country_id = fields.Many2one('res.country',string='Country')
    region_name = fields.Char(string='Region')
    is_meditator = fields.Boolean(string='Meditator')
    bay_name = fields.Char(string='Bay Name')
    program_name = fields.Char(string='Program Name')
    start_date = fields.Date(string='Start Date')
    ie_date = fields.Date(string='IE Date')
    ieo_date = fields.Date(string='IEO Date')
    ieo_r_date = fields.Date(string='IEO Registered Date')
    bsp_date = fields.Date(string='BSP Date')
    shoonya_date = fields.Date(string='Shoonya Date')
    samyama_date = fields.Date(string='Samyama Date')
    reg_date_time = fields.Datetime(string='Registration Date')
    reg_status = fields.Char(string='Registration Status')
    program_type = fields.Char(string='Program Type')
    program_category = fields.Char(string='Program Category')
    lang_pref = fields.Many2one('res.lang',string="Preferred Language")
    courier_tracking_no = fields.Char(string="Courier Tracking No")
    utm_campaign = fields.Char(string='UTM Campaign')
    utm_content = fields.Char(string='UTM Content')
    utm_medium = fields.Char(string='UTM Medium')
    utm_source = fields.Char(string='UTM Source')
    utm_term = fields.Char(string='UTM Term')
    utm_referer = fields.Char(string='UTM Referer')
    is_religious = fields.Boolean(string='Religious')
    dnd_phone = fields.Boolean(string='DND Phone')
    dnd_sms = fields.Boolean(string='DND SMS')
    dnd_postmail =fields.Boolean(string='DND Postmail')
    dnd_wa_ext = fields.Boolean(string='DND Whatsapp Extension')
    # related fields from res_partner for email marketing
    email_validity = fields.Char(string='Email Validity')
    dnd_email = fields.Boolean(string='DND Email')
    deceased = fields.Boolean(string='Deceased')
    is_caca_donor = fields.Boolean(string='CaCa donor')
    last_txn_date = fields.Date(string='Last Txn Date')
    influencer_type = fields.Char(string="Influencer Type")
    is_checkin_completed = fields.Boolean(string='Check-In Completed')
    time_zone = fields.Char(string='Time Zone')
    stream_lang = fields.Char(string='Stream Language')
    # Related Fields
    pgm_tag_ids = fields.Many2many(related='contact_id_fkey.pgm_tag_ids')
    category_id = fields.Many2many(related='contact_id_fkey.category_id')

    # Switching to MATERIALIZED VIEW to resolve perf issue... plz run this query to create the view
    """
    CREATE MATERIALIZED VIEW public.programs_view_orm
         AS
    select * from (
        select (pa.id::bigint)::bigint as id, pa.id as txn_id,contact_id_fkey,rp.name,rp.phone,rp.phone2,rp.phone3,rp.phone4,rp.email,rp.email2,rp.email3,rp.whatsapp_number,rp.is_meditator,rp.center_id,rp.region_name,rp.street,rp.street2,rp.city,rp.state_id,rp.zip,rp.country_id,rp.ieo_r_date,rp.ieo_date,rp.ie_date,rp.bsp_date,rp.shoonya_Date,rp.samyama_date,rp.email_validity,rp.dnd_email,rp.deceased,rp.is_caca_donor,rp.last_txn_date,rp.influencer_type,bay_name,program_name,start_date,reg_date_time,reg_status,mas.program_type,mas.category as program_category, null as lang_pref, null as courier_tracking_no,pa.utm_campaign,pa.utm_content,pa.utm_medium,pa.utm_source,pa.utm_term,pa.utm_referer,mas.is_religious from program_attendance pa, program_type_master mas, res_partner rp where mas.id=pa.pgm_type_master_id and rp.id=pa.contact_id_fkey and pa.active=true UNION
        select (pl.id::bigint+1000000000)::bigint as id,pl.id as txn_id,contact_id_fkey,rp.name,rp.phone,rp.phone2,rp.phone3,rp.phone4,rp.email,rp.email2,rp.email3,rp.whatsapp_number,rp.is_meditator,rp.center_id,rp.region_name,rp.street,rp.street2,rp.city,rp.state_id,rp.zip,rp.country_id,rp.ieo_r_date,rp.ieo_date,rp.ie_date,rp.bsp_date,rp.shoonya_Date,rp.samyama_date,rp.email_validity,rp.dnd_email,rp.deceased,rp.is_caca_donor,rp.last_txn_date,rp.influencer_type,bay_name,program_name,webinar_event_date as start_date,reg_date_time,reg_status,mas.program_type as program_type, mas.category as program_category, null as lang_pref, null as courier_tracking_no,pl.utm_campaign,pl.utm_content,pl.utm_medium,pl.utm_source,pl.utm_term,pl.utm_referer,mas.is_religious from program_lead pl, program_type_master mas, res_partner rp where mas.id=pl.pgm_type_master_id and rp.id=pl.contact_id_fkey and pl.active=true UNION
        select (ieo.id::bigint+2000000000)::bigint as id,ieo.id as txn_id,contact_id_fkey,rp.name,rp.phone,rp.phone2,rp.phone3,rp.phone4,rp.email,rp.email2,rp.email3,rp.whatsapp_number,rp.is_meditator,rp.center_id,rp.region_name,rp.street,rp.street2,rp.city,rp.state_id,rp.zip,rp.country_id,rp.ieo_r_date,rp.ieo_date,rp.ie_date,rp.bsp_date,rp.shoonya_Date,rp.samyama_date,rp.email_validity,rp.dnd_email,rp.deceased,rp.is_caca_donor,rp.last_txn_date,rp.influencer_type,null as bay_name,'IEO' as program_name,start_date,ieo.create_date as reg_date_time,CASE when course_completion_date is not null then 'COMPLETED' else 'CONFIRMED' END as reg_status,'IEO' as program_type, 'IEO' as program_category,null as lang_pref, null as courier_tracking_no,null as utm_campaign,null as utm_content,null as utm_medium,null as utm_source,null as utm_term,null as utm_referer,NULL::boolean AS is_religious from ieo_record ieo,res_partner rp where rp.id=ieo.contact_id_Fkey and ieo.active=true UNION
        select (sg.id::bigint+3000000000)::bigint as id,sg.id as txn_id,contact_id_fkey,rp.name,rp.phone,rp.phone2,rp.phone3,rp.phone4,rp.email,rp.email2,rp.email3,rp.whatsapp_number,rp.is_meditator,rp.center_id,rp.region_name,rp.street,rp.street2,rp.city,rp.state_id,rp.zip,rp.country_id,rp.ieo_r_date,rp.ieo_date,rp.ie_date,rp.bsp_date,rp.shoonya_Date,rp.samyama_date,rp.email_validity,rp.dnd_email,rp.deceased,rp.is_caca_donor,rp.last_txn_date,rp.influencer_type,null as bay_name,'Sadhguru Exclusive',subscription_date as start_date,sg.create_date as reg_date_time,reg_status,'Sadhguru Exclusive' as program_type, 'Sadhguru Exclusive' as program_category, null as lang_pref, null as courier_tracking_no,sg.utm_campaign,sg.utm_content,sg.utm_medium,sg.utm_source,sg.utm_term,null as utm_referer,NULL::boolean AS is_religious from sadhguru_exclusive sg, res_partner rp where rp.id=sg.contact_id_fkey and sg.active=true
    ) sub   ;  
    create index programs_view_orm_program_name on programs_view_orm(program_name);
    create index programs_view_orm_program_type on programs_view_orm(program_type);
    create index programs_view_orm_program_category on programs_view_orm(program_category);
    create index programs_view_orm_reg_status on programs_view_orm(reg_status);
    create index programs_view_orm_name on programs_view_orm(name);
    create index programs_view_orm_phone on programs_view_orm(phone);
    create index programs_view_orm_phone_2 on programs_view_orm(phone2);
    create index programs_view_orm_phone_3 on programs_view_orm(phone3);
    create index programs_view_orm_phone_4 on programs_view_orm(phone4);
    create index programs_view_orm_email on programs_view_orm(email);
    create index programs_view_orm_email_2 on programs_view_orm(email2);
    create index programs_view_orm_email_3 on programs_view_orm(email3);
    create index programs_view_orm_whatsapp_number on programs_view_orm(whatsapp_number);
    create index programs_view_center_id on programs_view_orm(center_id);
    create index programs_view_country_id on programs_view_orm(country_id);
    create index programs_view_region_name on programs_view_orm(region_name);
    create index programs_view_contact_id_fkey on programs_view_orm(contact_id_fkey);
    """

    @api.model
    def load_views(self, views, options=None):
        context = self._context or {}
        if context.get('global_search'):
            global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
            self = self.with_user(global_data_user_id)
        return super(ProgramsView, self).load_views(views, options)

    def read(self, fields=None, load='_classic_read'):
        context = self._context or {}
        if context.get('global_search'):
            global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
            self = self.with_user(global_data_user_id)
        return super(ProgramsView, self).read(fields, load)

    @api.model
    def search(self, args, offset=0, limit=None, order=None, count=False):
        context = self._context or {}
        if context.get('global_search'):
            global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
            self = self.with_user(global_data_user_id)
        return super(ProgramsView, self).search(args, offset, limit, order, count)

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(ProgramsView, self).fields_get(allfields, attributes=attributes)
        if self._context.get('restrict_fields_get_list'):
            not_selectable_fields = set(res.keys()) - set(self._context.get('restrict_fields_get_list'))
            for field in not_selectable_fields:
                res[field]['selectable'] = False # to hide in Add Custom filter view
                res[field]['sortable'] = False # to hide in group by view
                res[field]['exportable'] = False # to hide in export list
                res[field]['store'] = False # to hide in 'Select Columns' filter in tree views
        return res

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        # # default - do not show any results
        # if len(args) == 0:
        # 	return []
        context = self._context or {}
        order_prepend = ''
        if context.get('center_scope'):
            args += [('center_id', 'in', self.env.user.centers.ids)]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('is_religious', '=', False)]
            if religious:
                rel_char_domain = (['|', ('is_religious', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('is_religious', '=', True)]
            args = rel_char_domain + args

        return super(ProgramsView, self)._search(args, offset, limit, order, count=count,
                                                 access_rights_uid=access_rights_uid)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        context = self._context or {}
        if context.get('center_scope'):
            domain += [('center_id','in', self.env.user.centers.ids)]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('is_religious', '=', False)]
            if religious:
                rel_char_domain = (['|', ('is_religious', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('is_religious', '=', True)]
            domain = rel_char_domain + domain

        return super(ProgramsView, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                    orderby=orderby, lazy=lazy)

    def mark_inactive(self):
        # id<1000000000 means the model is program_attendance
        if self.id < 1000000000:
            rec = self.env['program.attendance'].sudo().browse(self.txn_id)
            rec.write({'active': False})

    def bulk_assign_eligible_subs(self):
        view = self.env.ref('isha_crm.bulk_eligible_subs_act_window_wizard_form')
        view_id = view and view.id or False
        context = dict(self._context or {},
                       region_scope=True,
                       mapped_field='contact_id_fkey',
                       mapped_model='res.partner',
                       restrict_fields='ml_subscription_ids',
                       restrict_values=[('child_ids','=',False)])
        return {
            'name': 'Bulk Modify Eligible Subscriptions',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'm2m.tag.manipulation',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': context
        }
