from odoo import models, fields, api, exceptions
import logging
import sys
from datetime import timedelta
_logger = logging.getLogger('ResPartner')
_logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
_logger.addHandler(ch)

class MukRestOverride (models.Model):
	_inherit = 'muk_rest.bearer_token'

	# Making the access token valid for a week instead of 3600 seconds
	@api.model_create_multi
	def create(self, vals_list):
		for val in vals_list:
			val['expires_in'] = val['expires_in'] + timedelta(days=365) - timedelta(seconds=3600)
		return super(MukRestOverride,self).create(vals_list)

