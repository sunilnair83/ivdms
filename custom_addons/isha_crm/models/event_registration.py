# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
import logging
import sys

from odoo.exceptions import UserError, AccessError

_logger = logging.getLogger('EventRegistration')
_logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
_logger.addHandler(ch)

class EventRegInherited(models.Model):
    _inherit = 'event.registration'

    # attendee
    partner_id = fields.Many2one(
        'res.partner', string='Contact',index=True)
    event_name = fields.Char(string='Event Name', related='event_id.name')
    name = fields.Char(string='Attendee Name', required=True)
    user_input = fields.Char(string='User Answer')

    @api.model
    def check_access_rights(self, operation, raise_exception=True):
        val = None
        try:
            val = super(EventRegInherited, self).check_access_rights(operation, raise_exception)
        except AccessError as e:
            if e.name == 'Only event users or managers are allowed to create or update registrations.':
                pass
            else:
                raise e
        return val

    @api.onchange('partner_id')
    def _onchange_partner(self):
        if self.partner_id:
            contact = self.partner_id
            if contact:
                self.name = contact.name or self.name
                self.email = contact.email or self.email
                self.phone = contact.phone or self.phone
                self.mobile = contact.mobile or self.mobile

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        if not self.env.user.has_group('event.group_event_manager'):
            event_id = self._context.get('active_id')
            if event_id:
                event = self.env['event.event'].browse(event_id)
                if event and event.center_id and fields.Date.today() > event.date_end.date():
                    raise UserError(_("Only Center Administrator can mark back-dated attendance"))

        return super(EventRegInherited, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)

    # @api.model
    # def default_get(self, fields_list):
    #     # browse the active id event
    #     if not self.env.user.has_group('event.group_event_manager'):
    #         event_id = self._context.get('active_id')
    #         if event_id:
    #             event = self.env['event.event'].browse(event_id)
    #             if event and \
    #                     (fields.Date.today() > event.date_end.date() or fields.Date.today() < event.date_begin.date()):
    #                 raise UserError(_("Only Center Administrator can mark back-dated attendance"))
    #
    #     return super(EventRegInherited, self).default_get(fields_list)


    def button_reg_close(self):
        """ Close Registration """
        for registration in self:
            today = fields.Datetime.now()
            # if registration.event_id.date_begin <= today and registration.event_id.state == 'confirm':
            registration.write({'state': 'done', 'date_closed': today})

    @api.model_create_multi
    def create(self, vals_list):
        rec_list = super(EventRegInherited, self).create(vals_list)
        for val in rec_list:
            if val.event_id.event_type_id not in [self.env.ref('isha_crm.event_type_4')]:
                if val['partner_id']:
                    val['state'] = 'done'
                else:
                    val['state'] = 'open'
            else:
                val['state'] = 'draft'
        return rec_list
