from odoo import models, fields, api


class Event(models.Model):
	_inherit = 'event.event'

	center_id = fields.Many2one('isha.center', string='Center', default=lambda self: self.env.user.centers.ids[0] if self.env.user.centers else False)

	def get_region_domain(self):
		list_ids = self.env.user.regions.ids
		if len(list_ids) == 0 and self.env.user.centers:
			list_ids = self.env.user.centers.mapped('region_id').ids
			list_ids = list(set(list_ids))

		return [('id','in',list_ids)]

	region_id = fields.Many2one('isha.region',string='Region', default=lambda self: self.env.user.centers[0].region_id.ids[0] if self.env.user.centers else False,domain=get_region_domain)
	date_tz = fields.Selection('_tz_get', string='Timezone', required=True,
							   default=lambda self: self.env.user.tz or 'Asia/Kolkata')
	event_type_mail_ids = fields.One2many(
		'event.type.mail', 'event_type_id', string='Mail Schedule',
		copy=False, default=None)
	name = fields.Char(
		string='Event Name', translate=True, required=False)
	question_text = fields.Char('Question text')

	# Registration fields
	registration_ids = fields.One2many(
		'event.registration', 'event_id', string='Attendees',
		readonly=False, states={'done': [('readonly', True)]}, copy=True)

	def getEventName(self, rec):
		temp = []
		if rec.event_type_id:
			temp.append(str(rec.event_type_id.name))
		if rec.center_id:
			temp.append(str(rec.center_id.name))
		if rec.date_begin:
			temp.append(rec.date_begin.strftime('%Y-%m-%d'))

		return temp

	@api.onchange('event_type_id')
	def _onchange_type(self):
		res = super(Event, self)._onchange_type()
		# bug fix
		if self.event_type_id and self.event_type_id.event_type_mail_ids and not self.event_type_id.use_mail_schedule:
			self.event_mail_ids = False

	@api.onchange('event_type_id','date_begin','center_id')
	def computeEventName(self):
		for rec in self:
			temp = self.getEventName(rec)
			rec.name = ' '.join(filter(None,temp))

	@api.model
	def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
		ret_val = super(Event, self).fields_view_get(
			view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
		if 'center_id' in ret_val['fields']:
			ret_val['fields']['center_id']['domain'] = [('id', 'in', self.env.user.centers.ids)]
		return ret_val

	@api.model
	def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
		context = self._context or {}
		if context.get('center_scope'):
			args += [('center_id','in', self.env.user.centers.ids)]

		return super(Event, self)._search(args, offset, limit, order, count=count, access_rights_uid=access_rights_uid)


	# @api.model_create_multi
	# def create(self, vals_list):
	# 	rec_list =  super(Event, self).create(vals_list)
	# 	for val in rec_list:
	# 		temp = self.getEventName(val)
	# 		val.update({'name': ' '.join(filter(None,temp))})
	# 	return rec_list

	def write(self, vals):
		super(Event, self).write(vals)
		rec = self[0]
		temp = self.getEventName(rec)
		ret_vals = super(Event, self).write({'name': ' '.join(filter(None,temp))})
		return ret_vals

