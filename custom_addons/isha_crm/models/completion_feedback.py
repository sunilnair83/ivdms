import logging

from odoo import models, fields, api
_logger = logging.getLogger(__name__)

class CompletionFeedback (models.Model):
    _name = "completion.feedback"
    _description = 'Completion Feedback'
    _order = "write_date desc"

    # Imported fields
    submitted_date = fields.Datetime(string="Submission Date")
    submitted_email = fields.Char(string='Email')
    feedback_for = fields.Char(string="Feedback For")
    event_date = fields.Date(string="Event date")
    derive_from_pgm = fields.Char(string="Wish to derive from pgm")
    benefits_since_pgm = fields.Char(string="Benefits since pgm")
    say_about_pgm = fields.Char(string="Wish to say about pgm")
    would_recommend_pgm = fields.Boolean(string="Would recommend pgm")
    interested_in_advanced_pgms = fields.Boolean(string="Interested in advanced pgms")
    other_way_to_support = fields.Char(string="Other way to support")
    add_to_mandala_wa_group = fields.Boolean(string="Add to Mandala WA group", _description="For Participants outside America")
    ease_of_login = fields.Char(string="Ease of login")
    video_stream_quality = fields.Char(string="Video stream quality")
    technical_support = fields.Boolean(string="Technical support")
    technical_requirements_clear = fields.Boolean(string="Technical requirements clear")
    other_technical_feedback = fields.Char(string="Other technical feedback")
    other_comments_hear_about_sadhguru = fields.Char(string="Other comments for Hear Abut Sadhguru")
    other_comments_hear_about_program = fields.Char(string="Other comments for Hear Abut Program")
    other_comments_vital_aspect_of_pgm = fields.Char(string="Other comments for Vital Aspect of Program")
    other_comments_motivation_to_sign_up = fields.Char(string="Other comments for Motivation to Signup")
    system_id = fields.Integer(string="System ID", required=False, index=True)
    local_trans_id = fields.Char(string="Local Trans ID", required=False)
    interested_in_volunteering = fields.Boolean(string="Volunteering Interest")
    interested_in_ftv = fields.Boolean(string="FTV Interest")
    occupation_feedback = fields.Char(string="Recent Occupation")
    telegram_number = fields.Char(string="Telegram Number")

    # Model fields
    active = fields.Boolean()

    # Other fields
    contact_id_fkey = fields.Many2one('res.partner', string='Contact', auto_join=True,index=True)
    name = fields.Char(related="contact_id_fkey.name")

    # adding email, deceased, is_caca_donor,last_txn_date, influencer_type,email_validity and dnd_email for email marketing
    email = fields.Char(related="contact_id_fkey.email")
    deceased = fields.Boolean(related="contact_id_fkey.deceased")
    is_caca_donor = fields.Boolean(related="contact_id_fkey.is_caca_donor")
    last_txn_date = fields.Date(related="contact_id_fkey.last_txn_date")
    dnd_email = fields.Boolean(related="contact_id_fkey.dnd_email")
    influencer_type = fields.Selection(related="contact_id_fkey.influencer_type")
    email_validity = fields.Selection(related='contact_id_fkey.email_validity')
    vol_duration = fields.Selection(related='contact_id_fkey.vol_duration', store=True)
    ie_date = fields.Date(related='contact_id_fkey.ie_date')

    program_tags = fields.Many2many(related='contact_id_fkey.pgm_tag_ids')
    skill_tag_ids = fields.Many2many(related='contact_id_fkey.skill_tag_ids')
    lang_ids = fields.Many2many(related='contact_id_fkey.lang_ids')

    sso_id = fields.Char(related="contact_id_fkey.sso_id")
    phone = fields.Char(related="contact_id_fkey.phone")
    whatsapp_number = fields.Char(related="contact_id_fkey.whatsapp_number")
    center_id = fields.Many2one(related="contact_id_fkey.center_id", store=True)
    region_name = fields.Char(related="contact_id_fkey.region_name", store=True)
    country = fields.Char(related="contact_id_fkey.country", store=True)
    occupation = fields.Char(related="contact_id_fkey.occupation", store=True)
    education = fields.Char(related="contact_id_fkey.education")
    work_company_name = fields.Char(related="contact_id_fkey.work_company_name")
    work_designation = fields.Char(related="contact_id_fkey.work_designation", store=True)
    phone_country_code = fields.Char(related="contact_id_fkey.phone_country_code")
    whatsapp_country_code = fields.Char(related="contact_id_fkey.whatsapp_country_code")
    is_meditator = fields.Boolean(related="contact_id_fkey.is_meditator", store=True)
    hear_about_sadhguru = fields.Many2many('hear.about.sadhguru.tags', column1='completion_id', column2='name', string='Hear about Sadhguru', readonly=True)
    hear_about_program = fields.Many2many('hear.about.program.tags', column1='completion_id', column2='name', string="Hear about program", readonly=True)
    motivation_to_sign_up = fields.Many2many('motivation.to.signup', column1='completion_id', column2='name', string="Motivation to sign up", readonly=True)
    vital_aspect_of_pgm = fields.Many2many('vital.aspect.pgm', column1='completion_id', column2='name', string="Most vital aspect of pgm", readonly=True)
    support_initiatives = fields.Many2many('support.initiatives', column1='completion_id', column2='name', string="Wish to support initiatives", readonly=True)
    how_to_stay_connected = fields.Many2many('how.to.stay.connected', column1='completion_id', column2='name', string="How to stay connected", readonly=True)
    programs_interested_in = fields.Many2many('program.list', relation='completion_feedback_program_interest_rel',
                                              column1='completion_id', column2='program_id',
                                              string="Programs Interested In")
    activities_to_support = fields.Many2many('isha.project.support', relation='completion_feedback_project_support_rel',
                                             column1='completion_id', column2='project_id',
                                             string="Activities to Volunteer")

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(CompletionFeedback, self).fields_get(allfields, attributes=attributes)
        if self._context.get('restrict_fields_get_list'):
            not_selectable_fields = set(res.keys()) - set(self._context.get('restrict_fields_get_list'))
            for field in not_selectable_fields:
                res[field]['selectable'] = False  # to hide in Add Custom filter view
                res[field]['sortable'] = False  # to hide in group by view
                # res[field]['exportable'] = False # to hide in export list
                res[field]['store'] = False  # to hide in 'Select Columns' filter in tree views
        return res

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        context = self._context or {}
        if context.get('center_scope'):
            domain += [('center_id', 'in', self.env.user.centers.ids)]

        return super(CompletionFeedback, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                         orderby=orderby, lazy=lazy)

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}
        if context.get('center_scope'):
            args += [('center_id', 'in', self.env.user.centers.ids)]

        # default - do not show any results
        if len(args) == 0:
            return []
        return super(CompletionFeedback, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)


class CompletionFeedbackAdvancedProgramList(models.Model):
    _name = "program.list"
    _description = 'Completion Feedback Advanced Programs List'

    name = fields.Char(string='Program Name')


class CompletionFeedbackOptionsHearAboutSadhguru(models.Model):
    _name = "hear.about.sadhguru.tags"
    _description = 'Completion Feedback Hear About Sadhguru Tags'

    name = fields.Char(string='Hear About Sadhguru Tags')

class CompletionFeedbackOptionsHearAboutProgram(models.Model):
    _name = "hear.about.program.tags"
    _description = 'Completion Feedback Hear About Program Tags'

    name = fields.Char(string='Hear About Program Tags')

class CompletionFeedbackOptionsMotivationToSignUp(models.Model):
    _name = "motivation.to.signup"
    _description = 'Completion Feedback Motivated to Sign Up for IE'

    name = fields.Char(string='Motivation to Signup for IE')

class CompletionFeedbackOptionsVitalAspectOfProgram(models.Model):
    _name = "vital.aspect.pgm"
    _description = 'Completion Feedback Vital Aspect Of the Program'

    name = fields.Char(string='Vital Aspect Of the Program')

class CompletionFeedbackOptionsSupportIshaInitiatives(models.Model):
    _name = "support.initiatives"
    _description = 'Completion Feedback Vital Aspect Of the Program'

    name = fields.Char(string='Support Initiatives')

class CompletionFeedbackOptionsNorthAmericaStayConnected(models.Model):
    _name = "how.to.stay.connected"
    _description = 'Completion Feedback North America Stay Connected'

    name = fields.Char(string='How To Stay Connected')
