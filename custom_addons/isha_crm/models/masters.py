import csv
import json
import logging
import os

from confluent_kafka import Producer

from odoo import api
from odoo import models, fields
from .. import isha_base_importer
from ..kafka_processors import EreceiptsEducationProcessor, IshaVidhyaOutreachProcessor
from ...isha_base.configuration import Configuration

logger = logging.getLogger("Masters")


class IVExchangeRate(models.Model):
	_name = 'iv.exchange.rate'
	_description = 'Isha Vidhya Exchange Rate'

	# effective_from = fields.Date(string='Effective From')
	# effective_to = fields.Date(string='Effective To')
	financial_year = fields.Char(string='Financial Year', required=True)
	currency = fields.Many2one(string='Currency', comodel_name='res.currency', required=True)
	rate = fields.Float(string='Exchange Rate', required=True)


class IVSubsidyBreakUp(models.Model):
	_name = 'iv.purpose.cost'
	_description = 'Isha Vidhya Purpose Cost'

	financial_year = fields.Char(string='Financial Year', required=True)
	purpose = fields.Char(string='Purpose', required=True)
	inr_cost = fields.Integer(string='Cost in INR')
	usd_cost = fields.Integer(string='Cost in USD')
	gbp_cost = fields.Integer(string='Cost in GBP')
	priority = fields.Integer(string='Priority')


class DonorCategory(models.Model):
	_name = 'iv.donor.org'
	_description = 'Isha Vidhya Donor Organizations'

	name = fields.Char(string='Donor Name')
	category = fields.Char(string='Donor Category')


class DonorCategory(models.Model):
	_name = 'iv.campaign.map'
	_description = 'Isha Vidhya Donor Category Mapping'

	isha_campaign_id = fields.Char(string='Isha Campaign Id')
	campaign_name = fields.Char(string='Campaign Name')


crm_config = isha_base_importer.Configuration('CRM')
with open(os.path.join(crm_config['CRM_ASSETS_PATH'], 'country-nationality-iso2-map.csv')) as f:
    iso2 = dict(filter(None, csv.reader(f)))
with open(os.path.join(crm_config['CRM_ASSETS_PATH'], 'eReceipts-country-nationality-corrections.csv')) as f:
    csvReader = []
    for x in csv.reader(f):
        csvReader.append([x[0].upper(), x[1].upper()])
    correction = dict(filter(None, csvReader))

metadata = {
			'iso2': iso2,
			'correction': correction
		}

class TempOutreachRecords(models.Model):
	_name = 'temp.outreach.records'
	_description = 'Temp Outreach Records'


	@api.depends('txn_date')
	def _compute_txn_date(self):
		for rec in self:
			if rec.txn_date:
				rec.transaction_date = rec.txn_date.timestamp() * 1000
			else:
				rec.transaction_date = None
	id = fields.Integer()
	sso_id = fields.Char()
	isha_transaction_id = fields.Char()
	txn_date = fields.Datetime()
	transaction_date = fields.BigInt(compute='_compute_txn_date')
	amount = fields.Float()
	isha_campaign_id = fields.Integer()
	campaign_id = fields.Integer()
	campaign_name = fields.Char()
	transaction_status = fields.Char()
	currency = fields.Char()
	udate = fields.Datetime()
	payment_gateway = fields.Char()
	purpose = fields.Char()
	payment_gateway_type = fields.Char()
	donor_name = fields.Char()
	address = fields.Char()
	city = fields.Char()
	state = fields.Char()
	country = fields.Char()
	nationality = fields.Char()
	mobile = fields.Char()
	pincode = fields.Char()
	email = fields.Char()
	pan_number = fields.Char()
	general_donation = fields.Float()
	govt_school_adoption_program = fields.Float()
	infrastructure_requirements = fields.Float()
	full_educational_support_qty = fields.Char()
	full_educational_support_total_amount = fields.Float()
	scholarship_qty = fields.Char()
	scholarship_total_amount = fields.Float()
	noon_meal_subsidy_qty = fields.Char()
	noon_meal_subsidy_total_amount = fields.Float()
	transport_subsidy_qty = fields.Char()
	transport_subsidy_total_amount = fields.Float()
	owner_name = fields.Char()

	def get_kafka_msg(self):

		msg = {
			'id': self.id if self.id else None,
			'sso_id': self.sso_id if self.sso_id else None,
			'isha_transaction_id': self.isha_transaction_id if self.isha_transaction_id else None,
			'transaction_date': self.transaction_date if self.transaction_date else None,
			'amount': self.amount if self.amount else None,
			'isha_campaign_id': self.isha_campaign_id if self.isha_campaign_id else None,
			'campaign_id': self.campaign_id if self.campaign_id else None,
			'campaign_name': self.campaign_name if self.campaign_name else None,
			'transaction_status': self.transaction_status if self.transaction_status else None,
			'currency': self.currency if self.currency else None,
			'udate': self.udate if self.udate else None,
			'payment_gateway': self.payment_gateway if self.payment_gateway else None,
			'purpose': self.purpose if self.purpose else None,
			'payment_gateway_type': self.payment_gateway_type if self.payment_gateway_type else None,
			'donor_name': self.donor_name if self.donor_name else None,
			'address': self.address if self.address else None,
			'city': self.city if self.city else None,
			'state': self.state if self.state else None,
			'country': self.country if self.country else None,
			'nationality': self.nationality if self.nationality else None,
			'mobile': self.mobile if self.mobile else None,
			'pincode': self.pincode if self.pincode else None,
			'email': self.email if self.email else None,
			'owner_name': self.owner_name if self.owner_name else None,
			'pan_number': self.pan_number if self.pan_number else None,
			'general_donation': self.general_donation if self.general_donation else None,
			'govt_school_adoption_program': self.govt_school_adoption_program if self.govt_school_adoption_program else None,
			'infrastructure_requirements': self.infrastructure_requirements if self.infrastructure_requirements else None,
			'full_educational_support_qty': self.full_educational_support_qty if self.full_educational_support_qty else None,
			'full_educational_support_total_amount': self.full_educational_support_total_amount if self.full_educational_support_total_amount else None,
			'scholarship_qty': self.scholarship_qty if self.scholarship_qty else None,
			'scholarship_total_amount': self.scholarship_total_amount if self.scholarship_total_amount else None,
			'noon_meal_subsidy_qty': self.noon_meal_subsidy_qty if self.noon_meal_subsidy_qty else None,
			'noon_meal_subsidy_total_amount': self.noon_meal_subsidy_total_amount if self.noon_meal_subsidy_total_amount else None,
			'transport_subsidy_qty': self.transport_subsidy_qty if self.transport_subsidy_qty else None,
			'transport_subsidy_total_amount': self.transport_subsidy_total_amount if self.transport_subsidy_total_amount else None,
		}

		return json.dumps(msg)

	def write(self, values):
		update = super(TempOutreachRecords, self).write(values)
		msg = self.get_kafka_msg()
		trigger_otr_sync(msg)
		# IshaVidhyaOutreachProcessor.IshaVidhyaOutreachProcessor().process_message(self, self.sudo().env, metadata)
		return update

	def create(self, values):
		update = super(TempOutreachRecords, self).create(values)
		msg = self.get_kafka_msg()
		trigger_otr_sync(msg)
		# IshaVidhyaOutreachProcessor.IshaVidhyaOutreachProcessor().process_message(self, self.sudo().env, metadata)
		return update

def trigger_otr_sync(msg):
	kafka_config = Configuration('QA_KAFKA')

	p = Producer({
		'bootstrap.servers': kafka_config['KAFKA_BOOTSTRAP_SERVERS'],
		'security.protocol': 'SSL',
		'ssl.ca.location': kafka_config['KAFKA_SSL_CA_CERT_PATH'],
		'ssl.certificate.location': kafka_config['KAFKA_SSL_CERT_PATH'],
	})
	logger.debug("Producer Started")

	# source db config
	p.produce('iv-test-mar22', msg.encode('utf-8'))
	p.flush(3)
	#
	# rpc_config = Configuration('RPC')
	# # odoo server config
	# username = rpc_config['RPC_USERNAME']
	# password = rpc_config['RPC_PASSWORD']
	# url = rpc_config['RPC_URL']
	# db = config['db_name']
	# common = client.ServerProxy('{}/xmlrpc/2/common'.format(url), allow_none=True)
	# uid = common.authenticate(db, username, password, {})
	# odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url), allow_none=True)
	# rec = {'src_msg': msg, 'processor': 'isha-vidhya',
	# 	   'process_msg': True}
	# flag_dict = odoo_models.execute_kw(db, uid, password,
	# 								   'kafka.errors', 'create',
	# 								   [rec])


class TempEreceiptsRecords(models.Model):
	_name = 'temp.erc.records'
	_description = 'Temp Ereceipts Records'

	row_id = fields.Integer()
	id = fields.Char()
	contact_id = fields.Char()
	modified_at = fields.Char()
	modified_by = fields.Char()
	project = fields.Char()
	mode_of_payment = fields.Char()
	amount = fields.Float()
	receipt_number = fields.Char()
	receipt_date = fields.Char()
	entity = fields.Char(default='IshaEducation')
	status = fields.Char(default='SUCCESS')
	active = fields.Integer(default=1)
	purpose_type = fields.Char()
	contact_type = fields.Char()
	first_name = fields.Char()
	nationality = fields.Char()
	phones = fields.Char()
	emails = fields.Char()
	address1 = fields.Char()
	town_city1 = fields.Char()
	state_province_region1 = fields.Char()
	postal_code1 = fields.Char()
	country1 = fields.Char()
	pan_number = fields.Char()
	passport_number = fields.Char()
	center = fields.Char()
	ebook_number = fields.Char()
	transfer_reference = fields.Char()
	bank_name = fields.Char()
	bank_branch = fields.Char()
	dd_chq_number = fields.Char()
	dd_chq_date = fields.Char()
	payment_gateway = fields.Char()
	short_year = fields.Char()
	sso_id = fields.Char()
	books_of_account = fields.Char()
	comments = fields.Char()
	phone = fields.Char()
	mobile = fields.Char()

	def get_kafka_msg(self):
		msg = { 'payload': {
		'row_id': self.row_id if self.row_id else None,
		'id': self.id if self.id else None,
		'contact_id': self.contact_id if self.contact_id else None,
		'modified_at': self.modified_at if self.modified_at else None,
		'modified_by': self.modified_by if self.modified_by else None,
		'project': self.project if self.project else None,
		'mode_of_payment': self.mode_of_payment if self.mode_of_payment else None,
		'amount': self.amount if self.amount else None,
		'receipt_number': self.receipt_number if self.receipt_number else None,
		'receipt_date': self.receipt_date if self.receipt_date else None,
		'entity': self.entity if self.entity else None,
		'status': self.status if self.status else None,
		'active': self.active if self.active else None,
		'purpose_type': self.purpose_type if self.purpose_type else None,
		'contact_type': self.contact_type if self.contact_type else None,
		'first_name': self.first_name if self.first_name else None,
		'nationality': self.nationality if self.nationality else None,
		'phones': self.phones if self.phones else None,
		'emails': self.emails if self.emails else None,
		'address1': self.address1 if self.address1 else None,
		'town_city1': self.town_city1 if self.town_city1 else None,
		'state_province_region1': self.state_province_region1 if self.state_province_region1 else None,
		'postal_code1': self.postal_code1 if self.postal_code1 else None,
		'country1': self.country1 if self.country1 else None,
		'pan_number': self.pan_number if self.pan_number else None,
		'passport_number': self.passport_number if self.passport_number else None,
		'center': self.center if self.center else None,
		'ebook_number': self.ebook_number if self.ebook_number else None,
		'transfer_reference': self.transfer_reference if self.transfer_reference else None,
		'bank_name': self.bank_name if self.bank_name else None,
		'bank_branch': self.bank_branch if self.bank_branch else None,
		'dd_chq_number': self.dd_chq_number if self.dd_chq_number else None,
		'dd_chq_date': self.dd_chq_date if self.dd_chq_date else None,
		'payment_gateway': self.payment_gateway if self.payment_gateway else None,
		'short_year': self.short_year if self.short_year else None,
		'sso_id': self.sso_id if self.sso_id else None,
		'books_of_account': self.books_of_account if self.books_of_account else None,
		'comments': self.comments if self.comments else None,
		'phone': self.phone if self.phone else None,
		'mobile': self.mobile if self.mobile else None,
		}}
		return msg


	def write(self, values):
		update = super(TempEreceiptsRecords, self).write(values)
		msg = self.get_kafka_msg()
		trigger_erc_sync(json.dumps(msg))
		# EreceiptsEducationProcessor.EreceiptsEducationProcessor().process_message(msg, self.sudo().env,
		# 																		  metadata)
		return update

	def create(self, values):
		update = super(TempEreceiptsRecords, self).create(values)
		msg = self.get_kafka_msg()
		trigger_erc_sync(json.dumps(msg))
		# EreceiptsEducationProcessor.EreceiptsEducationProcessor().process_message(msg, self.sudo().env,
		# 																		  metadata)
		return update

def trigger_erc_sync(msg):

	kafka_config = Configuration('QA_KAFKA')

	p = Producer({
		'bootstrap.servers': kafka_config['KAFKA_BOOTSTRAP_SERVERS'],
		'security.protocol': 'SSL',
		'ssl.ca.location': kafka_config['KAFKA_SSL_CA_CERT_PATH'],
		'ssl.certificate.location': kafka_config['KAFKA_SSL_CERT_PATH'],
	})
	logger.debug("Producer Started")

	# source db config
	p.produce('erc-test-mar22', msg.encode('utf-8'))
	p.flush(3)
	#
	# rpc_config = Configuration('RPC')
	# # odoo server config
	# username = rpc_config['RPC_USERNAME']
	# password = rpc_config['RPC_PASSWORD']
	# url = rpc_config['RPC_URL']
	# db = config['db_name']
	# common = client.ServerProxy('{}/xmlrpc/2/common'.format(url), allow_none=True)
	# uid = common.authenticate(db, username, password, {})
	# odoo_models = client.ServerProxy('{}/xmlrpc/2/object'.format(url), allow_none=True)
	# rec = {'src_msg': msg, 'topic': 'ereceipts', 'processor': 'ereceipts-edu',
	# 	   'process_msg': True}
	# flag_dict = odoo_models.execute_kw(db, uid, password,
	# 								   'kafka.errors', 'create',
	# 								   [rec])


