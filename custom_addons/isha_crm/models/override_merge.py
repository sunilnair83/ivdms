# -*- coding: utf-8 -*-

import logging
import sys

from werkzeug import urls

from odoo import _
from odoo import models, api, fields
from odoo.exceptions import UserError
from odoo.tools import mute_logger
import psycopg2
_logger = logging.getLogger('ResPartner')
_logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
_logger.addHandler(ch)
from .. import GoldenContactAdv

class OverrideMergeClass(models.TransientModel):
	_inherit = 'base.partner.merge.automatic.wizard'

	skip_sso_check = fields.Boolean(string='Skip SSO Check',default=False)
	@api.model
	def _get_ordered_partner(self, partner_ids):
		""" Helper : returns a `res.partner` recordset ordered by write_date_date fields
			:param partner_ids : list of partner ids to sort
		"""
		return self.env['res.partner'].browse(partner_ids).sorted(
			key=lambda p: p.write_date
		)

	@api.model
	def _update_foreign_keys(self, src_partners, dst_partner):
		""" Update all foreign key from the src_partner to dst_partner. All many2one fields will be updated.
			:param src_partners : merge source res.partner recordset (does not include destination one)
			:param dst_partner : record of destination res.partner
		"""
		_logger.debug('_update_foreign_keys for dst_partner: %s for src_partners: %s', dst_partner.id,
					  str(src_partners.ids))

		# find the many2one relation to a partner
		Partner = self.env['res.partner']
		relations = self._get_fk_on('res_partner')

		self.flush()

		for table, column in relations:
			if 'base_partner_merge_' in table:  # ignore two tables
				continue

			# get list of columns of current table (exept the current fk column)
			query = "SELECT column_name FROM information_schema.columns WHERE table_name LIKE '%s'" % (table)
			self._cr.execute(query, ())
			columns = []
			for data in self._cr.fetchall():
				if data[0] != column:
					columns.append(data[0])

			# do the update for the current table/column in SQL
			query_dic = {
				'table': table,
				'column': column,
				'value': columns[0],
			}
			if len(columns) <= 1:
				# unique key treated
				query = """
					UPDATE "%(table)s" as ___tu
					SET "%(column)s" = %%s
					WHERE
						"%(column)s" = %%s AND
						NOT EXISTS (
							SELECT 1
							FROM "%(table)s" as ___tw
							WHERE
								"%(column)s" = %%s AND
								___tu.%(value)s = ___tw.%(value)s
						)""" % query_dic
				for partner in src_partners:
					self._cr.execute(query, (dst_partner.id, partner.id, dst_partner.id))
			else:
				try:
					with mute_logger('odoo.sql_db'), self._cr.savepoint():
						query = 'UPDATE "%(table)s" SET "%(column)s" = %%s WHERE "%(column)s" IN %%s' % query_dic
						self._cr.execute(query, (dst_partner.id, tuple(src_partners.ids),))


						# [ISH-1355] removing this as it takes more than 20 seconds in prod
						# parent_id is not used in any way.. need to revisit this if we need parent_id

						# handle the recursivity with parent relation
						# if column == Partner._parent_name and table == 'res_partner':
						# 	query = """
						# 		WITH RECURSIVE cycle(id, parent_id) AS (
						# 				SELECT id, parent_id FROM res_partner
						# 			UNION
						# 				SELECT  cycle.id, res_partner.parent_id
						# 				FROM    res_partner, cycle
						# 				WHERE   res_partner.id = cycle.parent_id AND
						# 						cycle.id != cycle.parent_id
						# 		)
						# 		SELECT id FROM cycle WHERE id = parent_id AND id = %s
						# 	"""
						# 	self._cr.execute(query, (dst_partner.id,))
						# NOTE JEM : shouldn't we fetch the data ?
				except psycopg2.Error:
					# updating fails, most likely due to a violated unique constraint
					# keeping record with nonexistent partner_id is useless, better delete it
					query = 'DELETE FROM "%(table)s" WHERE "%(column)s" IN %%s' % query_dic
					self._cr.execute(query, (tuple(src_partners.ids),))

		self.invalidate_cache()

	def action_merge(self):
		""" Merge Contact button. Merge the selected partners, and redirect to
            the end screen (since there is no other wizard line to process.
        """
		if not self.partner_ids:
			self.write({'state': 'finished'})
			return {
				'type': 'ir.actions.act_window',
				'res_model': self._name,
				'res_id': self.id,
				'view_mode': 'form',
				'target': 'new',
			}

		ret_vals = self._merge(self.partner_ids.ids, self.dst_partner_id)

		if self.current_line_id:
			self.current_line_id.unlink()

		return self.env['sh.message.wizard'].get_popup('The merge process is scheduled Successfully.')

	def _merge(self, partner_ids, dst_partner=None, extra_checks=True):
		if len(set(partner_ids)) != 2:
			raise UserError(_(
				"Sorry merge can happen only with 2 contacts.. Please select exactly 2 contacts to merge."))

		if not self.skip_sso_check and not self._context.get('skip_sso_check', False):
			partners = self.env['res.partner'].sudo().browse(partner_ids)
			if partners[0]['sso_id'] and partners[1]['sso_id'] and partners[0]['sso_id'] != partners[1]['sso_id']:

				raise UserError(_("Sorry you cannot merge 2 contacts with different sso_id. Plz enable the 'Skip SSO Check' flag to still merge them."))


		# Bypasses all the record rules for merge alone..
		self = self.sudo()
		dst_partner = dst_partner.sudo()

		# partners = self.env['res.partner'].sudo().browse(partner_ids)
		# if partners[0]['sso_id'] and partners[1]['sso_id'] and partners[0]['sso_id'] != partners[1]['sso_id']:
		# 	raise UserError(_(
		# 		"Sorry you cannot merge 2 contacts with different sso_id."))
		#

		super(OverrideMergeClass, self)._merge(partner_ids,dst_partner,False)

		# Manually invoke computed fields for the dst partner
		dst_partner.is_zip_valid()
		if len(dst_partner.contact_program_attendance_id_fkey) > 0:
			dst_partner._compute_pgm_fields()
		dst_partner.display_name = dst_partner.name
		id = dst_partner[0].id

		# Note after merge the id of the lmp will be the same
		lmp_rec_list = self.env['low.match.pairs'].search([('id1','=',id),('id2','=',id)])
		models.Model.unlink(lmp_rec_list)
		dst_partner._compute_low_match_count()
		self.env.cr.commit()

	def get_min_date(self,winner_date, looser_date):
		ret_val = None
		if looser_date and winner_date:
			ret_val = min(looser_date, winner_date)
		elif winner_date:
			ret_val = winner_date
		else:
			ret_val = looser_date
		return ret_val

	def get_max_date(self,winner_date, looser_date):
		ret_val = None
		if looser_date and winner_date:
			ret_val = max(looser_date, winner_date)
		elif winner_date:
			ret_val = winner_date
		else:
			ret_val = looser_date
		return ret_val

	def get_influencer(self,winner_influencer_type, looser_influencer_type):
		ret_val = None
		if winner_influencer_type == 'influencer' or looser_influencer_type == 'influencer':
			ret_val = 'influencer'
		elif winner_influencer_type:
			ret_val = winner_influencer_type
		else:
			ret_val = looser_influencer_type
		return ret_val


	def custom_update_value(self,src_partner,dst_partner):
		goldenContact = GoldenContactAdv.GoldenContactAdv()
		update_dict = {}
		# display name
		update_dict['display_name'] = dst_partner['name']
		# Phone
		phone_dict = goldenContact.getPhonesDict(src_partner, dst_partner)
		update_dict.update(phone_dict)
		# Email
		email_dict = goldenContact.getEmailsDict(src_partner, dst_partner)
		update_dict.update(email_dict)
		#address
		address_key = ['street', 'street2', 'city', 'state', 'zip', 'country','country_id','state_id']
		address_key_2 = ['street2_1', 'street2_2', 'city2', 'state2', 'zip2', 'country2','country2_id','state2_id']
		if not dst_partner['zip2'] and src_partner['zip']:
			if dst_partner['zip'] != src_partner['zip']:
				update_dict.update({
					address_key_2[0]: src_partner[address_key[0]],
					address_key_2[1]: src_partner[address_key[1]],
					address_key_2[2]: src_partner[address_key[2]],
					address_key_2[3]: src_partner[address_key[3]],
					address_key_2[4]: src_partner[address_key[4]],
					address_key_2[5]: src_partner[address_key[5]],
					address_key_2[6]: src_partner[address_key[6]]
				})
			elif src_partner['zip2'] and dst_partner['zip'] != src_partner['zip2']:
				update_dict.update({
					address_key_2[0]: src_partner[address_key_2[0]],
					address_key_2[1]: src_partner[address_key_2[1]],
					address_key_2[2]: src_partner[address_key_2[2]],
					address_key_2[3]: src_partner[address_key_2[3]],
					address_key_2[4]: src_partner[address_key_2[4]],
					address_key_2[5]: src_partner[address_key_2[5]],
					address_key_2[6]: src_partner[address_key_2[6]]
				})

		update_dict['ie_date'] = self.get_min_date(dst_partner['ie_date'], src_partner['ie_date'])
		update_dict['bsp_date'] = self.get_min_date(dst_partner['bsp_date'], src_partner['bsp_date'])
		update_dict['shoonya_date'] = self.get_min_date(dst_partner['shoonya_date'], src_partner['shoonya_date'])
		update_dict['samyama_date'] = self.get_min_date(dst_partner['samyama_date'], src_partner['samyama_date'])
		update_dict['ieo_date'] = self.get_min_date(dst_partner['ieo_date'], src_partner['ieo_date'])
		update_dict['ieo_r_date'] = self.get_min_date(dst_partner['ieo_r_date'], src_partner['ieo_r_date'])
		update_dict['last_txn_date'] = self.get_max_date(dst_partner['last_txn_date'], src_partner['last_txn_date'])
		update_dict['religious_last_txn_date'] = self.get_max_date(dst_partner['religious_last_txn_date'], src_partner['religious_last_txn_date'])
		update_dict['charitable_last_txn_date'] = self.get_max_date(dst_partner['charitable_last_txn_date'], src_partner['charitable_last_txn_date'])
		update_dict['last_samarthak_date'] = self.get_max_date(dst_partner['last_samarthak_date'],
															   src_partner['last_samarthak_date'])
		update_dict['last_sanghamitra_date'] = self.get_max_date(dst_partner['last_sanghamitra_date'],
																 src_partner['last_sanghamitra_date'])
		update_dict['influencer_type'] = self.get_influencer(dst_partner['influencer_type'],
																 src_partner['influencer_type'])

		# Same logic is on kafka Flow -> GoldenContactAdv.mergeContact
		update_dict['is_annadhanam'] = src_partner['is_annadhanam'] or dst_partner['is_annadhanam']
		update_dict['is_caca_donor'] = src_partner['is_caca_donor'] or dst_partner['is_caca_donor']
		update_dict['has_religious_txn'] = src_partner['has_religious_txn'] or dst_partner['has_religious_txn']
		update_dict['has_charitable_txn'] = src_partner['has_charitable_txn'] or dst_partner['has_charitable_txn']
		update_dict['dnd_phone'] = src_partner['dnd_phone'] or dst_partner['dnd_phone']
		update_dict['dnd_email'] = src_partner['dnd_email'] or dst_partner['dnd_email']
		update_dict['dnd_sms'] = src_partner['dnd_sms'] or dst_partner['dnd_sms']
		update_dict['dnd_wa_ext'] = src_partner['dnd_wa_ext'] or dst_partner['dnd_wa_ext']
		update_dict['dnd_postmail'] = src_partner['dnd_postmail'] or dst_partner['dnd_postmail']
		update_dict['is_hni'] = src_partner['is_hni'] or dst_partner['is_hni']
		# update_dict['influencer_type'] = dst_partner['influencer_type'] or src_partner['influencer_type']
		update_dict['is_meditator'] = src_partner['is_meditator'] or dst_partner['is_meditator']
		update_dict['portal_verified'] = src_partner['portal_verified'] or dst_partner['portal_verified']
		update_dict['center_manually_edited'] = src_partner['center_manually_edited'] or dst_partner['center_manually_edited']
		if update_dict['is_meditator'] and update_dict['portal_verified']:
			update_dict.update({'forum_status': 'verified'})
		if not update_dict['is_meditator'] and update_dict['portal_verified']:
			update_dict.update({'forum_status': 'unverified'})
		if src_partner['center_manually_edited'] and not dst_partner['center_manually_edited']:
			update_dict['center_id'] = src_partner['center_id'].id
			update_dict['region_name'] = src_partner['center_id']['region_id'].name

		dst_partner.write(update_dict)
		self.env.cr.commit()

	@api.model
	def _update_values(self, src_partners, dst_partner):
		src_partner = src_partners[0]
		super(OverrideMergeClass, self)._update_values(src_partner, dst_partner)
		self.custom_update_value(src_partner,dst_partner)

	@api.model
	def default_get(self, fields):
		res = super(OverrideMergeClass, self).default_get(fields)
		active_ids = self.env.context.get('active_ids')
		if self.env.context.get('active_model') == 'low.match.pairs' and active_ids:
			res['state'] = 'selection'
			if len(active_ids) == 1:
				low_match = self.env['low.match.pairs'].browse(active_ids)
				res['partner_ids'] = [low_match['id1'].id,low_match['id2'].id]
				res['dst_partner_id'] = self._get_ordered_partner(res['partner_ids'])[-1].id
			else:
				res['partner_ids'] = [x['id1'].id for x in self.env['low.match.pairs'].browse(active_ids)]
				res['dst_partner_id'] = self._get_ordered_partner(res['partner_ids'])[-1].id
		elif self.env.context.get('active_model') == 'res.partner' and active_ids:
			res['state'] = 'selection'
			res['partner_ids'] = active_ids
			res['dst_partner_id'] = self._get_ordered_partner(active_ids)[-1].id
		return res

	def _log_merge_operation(self, src_partners, dst_partner):
		super(OverrideMergeClass, self)._log_merge_operation(src_partners, dst_partner)
		base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
		for src_partner in src_partners:
			url = urls.url_join(base_url, "web#id=%d&model=res.partner" % (dst_partner.id))
			link = 'Merged with the following contact: <a href="%s">%s (ID %d)</a>' % (url, dst_partner.name, dst_partner.id)
			src_partner.message_post(body='%s' % (link))
