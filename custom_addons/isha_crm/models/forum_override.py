
import logging
from odoo import api, fields, models, tools, SUPERUSER_ID, _
from odoo.exceptions import UserError, ValidationError, AccessError
_logger = logging.getLogger(__name__)


class Forum(models.Model):
	_inherit = 'forum.forum'

	def _default_groups(self):
		return self.env.ref('base.group_portal')

	groups_id = fields.Many2many('res.groups', 'res_groups_website_forum_rel', 'forum_id', 'gid', string='Allowed Groups'
								 , default=_default_groups)

	check_forum_restrain = fields.Boolean(string='Check Forum Restriction', default=True)
	mode = fields.Selection(selection_add=[('vtasks','Volunteer Tasks'),('no_answer','No Answers')])

class PostOverride(models.Model):
	_name = 'forum.post'
	_inherit = ['forum.post', 'image.mixin']

	task_id = fields.Many2one('isha.project.task', string='Project')
	task_eta = fields.Date(string='ETA')

	@api.depends('child_ids.create_uid', 'website_message_ids')
	def _get_child_count(self):
		"""
			Remove logic to count comments as answers
		"""

		def process(node):
			total = len(node.child_ids)  # + len(node.website_message_ids) +
			for child in node.child_ids:
				total += process(child)
			return total

		for post in self:
			post.child_count = process(post)

	def convert_answer_to_comment(self):
		""" Tools to convert an answer (forum.post) to a comment (mail.message).
		The original post is unlinked and a new comment is posted on the question
		using the post create_uid as the comment's author.
		Added logic to convert comments from answer to parent"""
		self.ensure_one()
		if not self.parent_id:
			return self.env['mail.message']

		# karma-based action check: use the post field that computed own/all value
		if not self.can_comment_convert:
			raise AccessError(_('%d karma required to convert an answer to a comment.') % self.karma_comment_convert)

		# post the message
		question = self.parent_id
		self_sudo = self.sudo()
		values = {
			'author_id': self_sudo.create_uid.partner_id.id,  # use sudo here because of access to res.users model
			'email_from': self_sudo.create_uid.email_formatted,  # use sudo here because of access to res.users model
			'body': tools.html_sanitize(self.content, sanitize_attributes=True, strip_style=True, strip_classes=True),
			'message_type': 'comment',
			'subtype': 'mail.mt_comment',
			'date': self.create_date,
		}
		new_message = question.with_user(self.create_uid).with_context(mail_create_nosubscribe=True).message_post(**values)

		# Convert all the comments of the answer to comments of parent
		for message in self_sudo.message_ids:
			message.with_user(message.create_uid).write({'res_id':question.id})
		# unlink the original answer, using SUPERUSER_ID to avoid karma issues
		self.sudo().unlink()

		return new_message

	@api.model
	def convert_comment_to_answer_and_flag(self, message_id, default=None):
		""" Removing the karma check for converting to answer"""
		comment = self.env['mail.message'].sudo().browse(message_id)
		post = self.browse(comment.res_id)
		if not comment.author_id or not comment.author_id.user_ids:  # only comment posted by users can be converted
			return False

		# check the message's author has not already an answer
		question = post.parent_id if post.parent_id else post
		post_create_uid = comment.author_id.user_ids[0]

		# create the new post
		post_values = {
			'forum_id': question.forum_id.id,
			'content': comment.body,
			'parent_id': question.id,
		}
		# done with the author user to have create_uid correctly set
		new_post = self.with_user(post_create_uid).create(post_values)

		# delete comment
		comment.unlink()

		# flag the answer
		new_post.flag()[0]
		return new_post

	@api.model
	def convert_comment_to_answer(self, message_id, default=None):
		""" Removing the condition to return false if the author has already a answer"""
		comment = self.env['mail.message'].sudo().browse(message_id)
		post = self.browse(comment.res_id)
		if not comment.author_id or not comment.author_id.user_ids:  # only comment posted by users can be converted
			return False

		# karma-based action check: must check the message's author to know if own / all
		is_author = comment.author_id.id == self.env.user.partner_id.id
		karma_own = post.forum_id.karma_comment_convert_own
		karma_all = post.forum_id.karma_comment_convert_all
		karma_convert = is_author and karma_own or karma_all
		can_convert = self.env.user.karma >= karma_convert
		if not can_convert:
			if is_author and karma_own < karma_all:
				raise AccessError(_('%d karma required to convert your comment to an answer.') % karma_own)
			else:
				raise AccessError(_('%d karma required to convert a comment to an answer.') % karma_all)

		# check the message's author has not already an answer
		question = post.parent_id if post.parent_id else post
		post_create_uid = comment.author_id.user_ids[0]
		# if any(answer.create_uid.id == post_create_uid.id for answer in question.child_ids):
		# 	return False

		# create the new post
		post_values = {
			'forum_id': question.forum_id.id,
			'content': comment.body,
			'parent_id': question.id,
		}
		# done with the author user to have create_uid correctly set
		new_post = self.with_user(post_create_uid).create(post_values)

		# delete comment
		comment.unlink()

		return new_post

	def unlink(self):
		"""
			Override text change karma to points
		"""
		for post in self:
			if not post.can_unlink:
				raise AccessError(_('%d points required to unlink a post.') % post.karma_unlink)
		# if unlinking an answer with accepted answer: remove provided karma
		for post in self:
			if post.is_correct:
				post.create_uid.sudo().add_karma(post.forum_id.karma_gen_answer_accepted * -1)
				self.env.user.sudo().add_karma(post.forum_id.karma_gen_answer_accepted * -1)
		return super(PostOverride, self).unlink()

	def unlink_comment(self, message_id):
		"""
			Override text change karma to points
		"""
		result = []
		for post in self:
			user = self.env.user
			comment = self.env['mail.message'].sudo().browse(message_id)
			if not comment.model == 'forum.post' or not comment.res_id == post.id:
				result.append(False)
				continue
			# karma-based action check: must check the message's author to know if own or all
			if comment.author_id.id == user.partner_id.id:
				karma_unlink = post.forum_id.karma_comment_unlink_own
			else:
				karma_unlink = post.forum_id.karma_comment_unlink_all

			can_unlink = user.karma >= karma_unlink
			if not can_unlink:
				raise AccessError(_('%d points required to unlink a comment.') % karma_unlink)
			result.append(comment.unlink())
		return result

	# Remove active flag for validate to make /validate depend only on can_close
	def validate(self):
		for post in self:
			if not post.can_moderate:
				raise AccessError(_('%d karma required to validate a post.') % post.forum_id.karma_moderate)
			# if state == pending, no karma previously added for the new question
			if post.state == 'pending':
				post.create_uid.sudo().add_karma(post.forum_id.karma_gen_question_new)
			post.write({
				'state': 'active',
				# 'active': True,
				'moderator_id': self.env.user.id,
			})
			post.post_notification()
		return True

class ForumTaskApplications(models.Model):
	_name = 'forum.task.applications'
	_description = 'Task Applications'
	_rec_name = 'applied_user'


	forum_post_id = fields.Many2one('forum.post',string='Task Name')
	forum_id = fields.Many2one(related='forum_post_id.forum_id',string='Forum', store=True)
	task_id = fields.Many2one(related='forum_post_id.task_id', string='Project Task', store=True)
	applied_user = fields.Many2one('res.users',string='Applied User')
	availability = fields.Char(string='Availability for the task')
	task_eta = fields.Datetime(string='ETA')
	active = fields.Boolean(string='Active', default=True)
	task_feedback = fields.Char(string='Feedback')
	task_rating = fields.Float(string='Rating')

	def assign_task(self):
		for rec in self:
			rec.task_id.user_id = rec.create_uid
			pre_assigned = rec.forum_post_id.child_ids.filtered(lambda x: x.is_correct is True)
			if len(pre_assigned) > 0:
				pre_assigned.is_correct = False
			rec.forum_post_id.is_correct = True
			application = rec.forum_post_id.child_ids.filtered(lambda x: x.create_uid == rec.create_uid)
			application.is_correct = True

	def write(self, vals):
		rec = super(ForumTaskApplications, self).write(vals)
		if 'task_rating' in vals:
			recs = self.search([('create_uid','=',self.create_uid.id),('task_feedback','!=',False)])
			star_points = sum(recs.mapped('task_rating'))
			self.create_uid.task_rating_aggregate = round((star_points/len(recs)),2)
		return rec

class Project(models.Model):
	_name = "isha.project.task"
	_description = "Isha Project Tasks"

	forum_task_applications = fields.One2many('forum.task.applications','task_id','Forum applications')
	forum_post_id = fields.Many2one('forum.post','Forum Post',domain=[('forum_id.mode','=','vtasks'),('parent_id','=',False)])

	task_eta = fields.Date(related='forum_post_id.task_eta')
	task_size = fields.Selection([('xs','XS'),('s','S'),('m','M'),('l','L')],string='Task Size')
	active = fields.Boolean(default=True)
	name = fields.Char(string='Title', tracking=True, required=True, index=True)

	def write(self, vals):
		rec = super(Project, self).write(vals)
		if 'forum_post_id' in vals:
			post = self.env['forum.post'].search([('id','=',vals['forum_post_id'])])
			post.task_id = self[0].id

		return rec

	@api.model_create_multi
	def create(self, vals_list):
		rec = super(Project, self).create(vals_list)
		for x in rec:
			if x.forum_post_id:
				post = self.env['forum.post'].search([('id', '=', x.forum_post_id.id)])
				post.task_id = x.id
		return rec

class ForumTag(models.Model):
	_name = 'forum.tag'
	_inherit = ['forum.tag', 'image.mixin']
