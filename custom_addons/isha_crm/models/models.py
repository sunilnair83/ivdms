# -*- coding: utf-8 -*-
import datetime
import json
import requests

from dateutil.relativedelta import relativedelta
import logging
import re
import threading
import traceback
from copy import deepcopy
from datetime import date
from datetime import timedelta

import phonenumbers
from phonenumbers import phonenumber

from odoo import _
from odoo import models, fields, api, SUPERUSER_ID, modules
from odoo.exceptions import UserError
from odoo.exceptions import ValidationError
from odoo.tools import config, formataddr
from .constants import CHARITABLE_GROUPS, RELIGIOUS_GROUPS
from .isha_program import IEOProgram
from .. import ContactProcessor, isha_base_importer
from .. import GoldenContactAdv
from .. import kafka_starter
from ..ContactProcessor import getTitlesRemoved
from ..services import ContactService
from .. import NameMatchEngine
import csv
import os

_logger = logging.getLogger(__name__)
selection_list_phone_type = [('mobile', 'Mobile'), ('office', 'Office'), ('home', 'Home'), ('other', 'Other')]

crm_config = isha_base_importer.Configuration('CRM')
with open(os.path.join(crm_config['CRM_ASSETS_PATH'], 'country-nationality-iso2-map.csv')) as f:
    iso2 = dict(filter(None, csv.reader(f)))
with open(os.path.join(crm_config['CRM_ASSETS_PATH'], 'eReceipts-country-nationality-corrections.csv')) as f:
    csvReader = []
    for x in csv.reader(f):
        csvReader.append([x[0].upper(), x[1].upper()])
    correction = dict(filter(None, csvReader))


class ResPartner (models.Model):
	_inherit = 'res.partner'
	_description = 'Isha CRM Contacts'
	_order = "write_date desc"

	guid = fields.Char(string='Golden ID', required=False, index=True)
	sso_id = fields.Char(string="SSO Id",required=False, index=True)
	prof_dr = fields.Char(string='Isha Title', required=False)
	suffix = fields.Selection([('ji', 'Ji'), ('navare', 'Navare'), ('garu', 'Garu')], string="Suffix")
	alias = fields.Char(string='Alias', required=False)
	alias2 = fields.Char(string='Alias 2', required=False)
	phone = fields.Char(string='Phone', required=False, index=True)
	phone_country_code = fields.Char(string='Phone Country Code', required=False)
	phone_is_valid = fields.Boolean(string='Valid Phone')
	phone_is_verified = fields.Boolean(string='Verified Phone')
	phone_type = fields.Selection(selection_list_phone_type, string='Phone Type')
	phone2 = fields.Char(string='Phone2', required=False, index=True)
	phone2_country_code = fields.Char(string='Phone2 Country Code', required=False)
	phone2_is_valid = fields.Boolean(string='Valid Phone2')
	phone2_is_verified = fields.Boolean(string='Verified Phone2')
	phone2_type = fields.Selection(selection_list_phone_type, string='Phone2 Type')
	phone3 = fields.Char(string='Phone3', required=False, index=True)
	phone3_country_code = fields.Char(string='Phone3 Country Code', required=False)
	phone3_is_valid = fields.Boolean(string='Valid Phone3')
	phone3_is_verified = fields.Boolean(string='Verified Phone3')
	phone3_type = fields.Selection(selection_list_phone_type, string='Phone3 Type')
	phone4 = fields.Char(string='Phone4', required=False, index=True)
	phone4_country_code = fields.Char(string='Phone4 Country Code', required=False)
	phone4_is_valid = fields.Boolean(string='Valid Phone4')
	phone4_is_verified = fields.Boolean(string='Verified Phone4')
	phone4_type = fields.Selection(selection_list_phone_type, string='Phone4 Type')
	whatsapp_number = fields.Char(string='WhatsApp Number', required=False)
	whatsapp_country_code = fields.Char(string='WhatsApp Country Code', required=False)
	whatsapp_validity = fields.Selection([('valid', 'Valid'), ('invalid', 'Invalid')], string='Whatsapp Validity')
	email = fields.Char(string='Email', required=False, index=True)
	email_is_bounced = fields.Boolean(string='Email Is Bounced', readonly=True)
	email_validity = fields.Selection([('valid', 'Valid'), ('bounced', 'Hard Bounce'),('soft_bounce','Soft Bounce'),('complaint','Complaint')], string='Email Validity')
	email2 = fields.Char(string='Email 2', required=False, index=True)
	email2_validity = fields.Selection([('valid', 'Valid'), ('bounced', 'Hard Bounce'),('soft_bounce','Soft Bounce'),('complaint','Complaint')], string='Email2 Validity')
	email2_is_bounced = fields.Boolean(string='Email 2 Is Bounced', readonly=True)
	email3 = fields.Char(string='Email 3', required=False, index=True)
	email3_validity = fields.Selection([('valid', 'Valid'), ('bounced', 'Hard Bounce'),('soft_bounce','Soft Bounce'),('complaint','Complaint')], string='Email3 Validity')
	email3_is_bounced = fields.Boolean(string='Email 3 Is Bounced', readonly=True)
	iv_email = fields.Char(string='Isha Vidhya Email', index=True)
	iv_email_manually_edited = fields.Boolean(string='Isha Vidhya Email Manually Edited?', readonly=True)
	state = fields.Char(string='State Text', required=False)
	country = fields.Char(string='Country ISO2', required=False)
	zip = fields.Char(string="Zip", required=False, index=True)
	is_valid_zip = fields.Boolean(string='Valid Zip')
	street2_1 = fields.Char(string='Street1 Of Alternate Address', required=False)
	street2_2 = fields.Char(string='Street2 Of Alternate Address', required=False)
	city2 = fields.Char(string='City Of Alternate Address', required=False)
	state2 = fields.Char(string='State Of Alternate Address', required=False)
	country2 = fields.Char(string='Country Of Alternate Address', required=False)
	zip2 = fields.Char(string='Pincode Of Alternate Address', required=False)
	work_street1 = fields.Char(string='Street1 Of Work Address', required=False)
	work_street2 = fields.Char(string='Street2 Of Work Address', required=False)
	work_city = fields.Char(string='City Of Work Address', required=False)
	work_state = fields.Char(string='State Of Work Address', required=False)
	work_country = fields.Char(string='Country Of Work Address', required=False)
	work_zip = fields.Char(string='Pincode Of Work Address', required=False)
	work_country_id = fields.Many2one('res.country', 'Country Id of Work')
	work_state_id = fields.Many2one('res.country.state', string='State Id Of Work Address')
	nationality_id = fields.Many2one('res.country', 'Nationality Id')
	country2_id = fields.Many2one('res.country', 'Country Id of Alternate Address')
	state2_id = fields.Many2one('res.country.state', string='State Id Of Alternate Address')
	gender = fields.Selection([('M', 'Male'), ('F', 'Female'), ('O', 'Others')],
							  string='Gender', required=False)
	occupation = fields.Char(string='Occupation', required=False)
	education = fields.Char(string='Education', required=False)
	dob = fields.Date(string='Date Of Birth', required=False)
	approx_dob = fields.Date(string='Approximate Date Of Birth', required=False)
	marital_status = fields.Selection([('single', 'Single'), ('married', 'Married')],
										string='Marital Status',required=False)
	nationality = fields.Char(string='Nationality', required=False)
	deceased = fields.Boolean(string='Deceased', required=False)
	contact_type = fields.Selection([('INDIVIDUAL', 'Individual'), ('ORGANIZATION', 'Organization')],
									string="Contact Type", default='INDIVIDUAL', required=False)
	companies = fields.Char(string="Company Name",required=False)
	aadhaar_no = fields.Char(string="Aadhaar",required=False)
	pan_no = fields.Char(string="PAN", required=False)
	passport_no = fields.Char(string="Passport", required=False)
	dnd_phone = fields.Boolean(string='DND Phone Flag', required=False)
	dnd_wa_ext = fields.Boolean(string='DND Whatsapp Extension', required=False)
	dnd_email = fields.Boolean(string='DND Email Flag', required=False)
	dnd_postmail = fields.Boolean(string='DND Post-mail Flag', required=False)
	dnd_sms = fields.Boolean(string='DND SMS', required=False)
	id_proofs = fields.Char(string='ID Proofs', required=False)
	no_match_n_merge = fields.Boolean(string='Should Match and Merge', default=False)
	center_id = fields.Many2one(string='Center', comodel_name='isha.center', ondelete='set null',tracking=True, index=True)
	sub_center = fields.Char(string='Sub-center')
	micro_center = fields.Char(string='Micro center')
	nurturing_volunteer = fields.Many2one(string='Nurturing Volunteer', comodel_name='res.partner', index=True)
	nurturing_remarks = fields.Text(string='Remarks')

	region_name = fields.Char(string='Region')#related="center_id.region_id.name")
	sector_country = fields.Char(string='Sector/District', related="center_id.sector_country", store=True)
	is_flagged = fields.Boolean(string='Flagged', required=False, index=True, default=False, tracking=True)
	center_manually_edited = fields.Boolean(string='Is Center Edited')
	practice_routine = fields.Selection([('regular','Regular'),('not_regular','Not Regular'),('not_doing','Not Doing')], string="Practice Routine")
	segment = fields.Selection([('adv_seeker','Advanced Seeker'),('seeker','Seeker'),('follower','Follower'),('follower_ref','Follower-Referral')],string="Segment",
							   default='follower')
	#node
	node_partner_id = fields.Many2one(string='Sangam Node', comodel_name='res.partner', index=True)


	# Skill tags
	skill_tag_ids = fields.Many2many('isha.skill.tag', column1='partner_id',column2='skill_tag_ids', string='Skill Tags')

	# program_lead
	contact_program_lead_id_fkey = fields.One2many('program.lead','contact_id_fkey', 'Program Lead Name')

	# IEO
	contact_ieo_id_fkey = fields.One2many('ieo.record','contact_id_fkey', 'IEO Records')
	ieo_date = fields.Date(string='IEO date', compute='_compute_ieo_date', store=True,index=True)
	ieo_r_date = fields.Date(string='IEO Registered date',index=True)
	iecso_r_date = fields.Date(string='IECSO Registered date', index=True)
	iecso_date = fields.Date(string='IECSO date', index=True)
	class_completed = fields.Char(string="Class Completed")
	ieo_progress = fields.Selection(IEOProgram.progress_selection, string="Class Completed")
	ieo_lang_pref = fields.Char(compute="_get_lang_pref",store=True,string='IEO Lang Pref')

	# Isha Vidhya
	iv_corp_vip_name = fields.Char(string="VIP Name")
	iv_corp_vip_phone = fields.Char(string="VIP Contact Number")
	iv_corp_vip_email = fields.Char(string="Email Address for VIP")
	iv_csr_secretary_name = fields.Char(string="Secretary CSR Name")
	iv_csr_secretary_phone = fields.Char(string="Secretary CSR Contact Number")
	iv_corp_reports_email = fields.Char(string="Email Address for Reports")
	iv_corp_registration_no = fields.Char(string="Registration Number")
	iv_last_donated_date = fields.Date(string='Last Donated Date')
	iv_donor_status = fields.Selection([('active', 'Active'), ('inactive', 'Inactive')], string='Donor Status',
									   compute='_compute_iv_donor_status')

	iv_remarks = fields.Char(string="Donor Remarks")

	# Sadhguru Exclusive
	contact_sgex_id_fkey = fields.One2many('sadhguru.exclusive', 'contact_id_fkey', 'Sadhguru Exclusive Records')

	#programs_view_orm
	programs_online_monthly = fields.One2many('programs.view.orm', 'contact_id_fkey',domain=lambda self: [('program_category', '=', 'Online Offering')])
	programs_and_events = fields.One2many('programs.view.orm', 'contact_id_fkey',domain=lambda self: [('program_category', '!=', 'Online Offering')])
	# programs_online_monthly = fields.One2many('programs.view.orm', 'contact_id_fkey', compute='_get_compute_online_programs')

	#Athithi Events
	contact_event_attendees_id_fkey = fields.One2many('event.attendees', 'contact_id_fkey', 'Athithi events Attended')

	# program_attendances of a contact
	contact_program_attendance_id_fkey = fields.One2many('program.attendance','contact_id_fkey', 'Program Schedule Name')
	pgm_tag_ids = fields.Many2many('isha.program.tag', column1='partner_id',column2='pgm_tag_ids', string='Program Tags', readonly=True)
	religious_tag_ids = fields.Many2many('isha.religious.tag', column1='partner_id',column2='religious_tag_ids',
										 string='Religious Tags', readonly=True,
										 groups="isha_crm.group_religious_santosha,isha_crm.group_all_contacts_religious,isha_crm.group_vanashree_grp,isha_crm.group_rudraksha_deeksha")
	pgm_center_id = fields.Many2one(string='Program Center', comodel_name='isha.center', ondelete='set null')

	caca_category = fields.Selection([('upasaka', 'Upasaka'), ('shoora', 'Shoora'), ('mitr', 'Mitr'),
									  ('veera', 'Veera'), ('param_veera', 'Param Veera'), ('rakshak', 'Rakshak'),
									  ('yodha', 'Yodha'), ('nayak', 'Nayak')], string="Cauvery Category",)
	donation_tags = fields.Many2many('isha.donation.tag', column1='partner_id',column2='donation_tag_id', readonly=True,
									 string='Donation Tags', groups='isha_crm.group_donation_tags,isha_crm.group_sangam')
	last_sanghamitra_date = fields.Date(string='Last Sanghamitra Date')
	last_samarthak_date = fields.Date(string='Last Samarthak Date')
	# program_attendances computed fields
	is_meditator = fields.Boolean(string='Meditator', required=False, index=True, default=False, store=True, compute='_compute_pgm_fields')
	ie_date = fields.Date(string = 'IE Date', required=False, index=True,store=True, compute='_compute_pgm_fields')
	samyama_date = fields.Date(string = 'Samyama Date', required=False, index=True,store=True, compute='_compute_pgm_fields')
	bsp_date = fields.Date(string = 'BSP Date', required=False, index=True,store=True, compute='_compute_pgm_fields')
	shoonya_date = fields.Date(string = 'Shoonya Date', required=False, index=True,store=True, compute='_compute_pgm_fields')
	last_txn_date = fields.Date(string='Last Txn Date', required=False, default=datetime.datetime(1900,1,1), index=True)#,store=True, compute='_compute_pgm_fields')
	religious_last_txn_date = fields.Date(string='Last R-Txn Date', required=False, index=True)#,store=True, compute='_compute_pgm_fields')
	charitable_last_txn_date = fields.Date(string='Last Ch-Txn Date', required=False, index=True)#,store=True, compute='_compute_pgm_fields')

	# donations fkeys
	contact_donations_outreach_id_fkey = fields.One2many('donation.isha.outreach','contact_id_fkey')
	contact_donations_foundation_id_fkey = fields.One2many('donation.isha.foundation','contact_id_fkey')
	contact_donations_education_id_fkey = fields.One2many('donation.isha.education','contact_id_fkey', string='Education Donations')
	contact_donations_iii_id_fkey = fields.One2many('donation.isha.iii','contact_id_fkey')
	contact_donations_arogya_id_fkey = fields.One2many('donation.isha.arogya','contact_id_fkey')

	has_religious_txn = fields.Boolean(string='Has Religious Txn')
	has_charitable_txn = fields.Boolean(string='Has Charitable Txn')

	# donations computed fields
	is_caca_donor = fields.Boolean(string='Cauvery Calling Donor', required=False, index=True, default=False) #, store=True, compute='_compute_don_outreach_fields')
	is_hni = fields.Boolean(string='HNI', required=False, index=True, default=False) #, store=True, compute='_compute_don_foundation_fields')
	is_annadhanam = fields.Boolean(string='Annadhanam Donor', required=False, index=True) #, default=False, store=True, compute='_compute_don_foundation_fields')
	last_donation_date = fields.Date(string='Last Donation', required=False)
	last_donation_amount = fields.Float(string="Last Donation Amount", required=False)

	total_foundation_amount = fields.Float(compute = "_compute_total_donation", store = False)
	total_outreach_amount = fields.Float(compute = "_compute_total_donation", store = False)
	total_iii_amount = fields.Float(compute = "_compute_total_donation", store = False)
	total_arogya_amount = fields.Float(compute = "_compute_total_donation", store = False)
	total_education_amount = fields.Float(compute = "_compute_total_donation", store = False)
	finance_filter = fields.Boolean(compute = "_compute_total_donation", store = False)

	total_donation = fields.Float(string="Total Donation", index=True, groups="isha_crm.group_sangam")

	# events
	contact_event_registration_id_fkey  = fields.One2many('event.registration','partner_id','Event Attendee name')

	# Meditation products
	contact_med_prod_id_fkey = fields.One2many('meditation.product','contact_id_fkey')

	contact_contact_map_id_fkey = fields.One2many('contact.map','contact_id_fkey')
	contact_low_match_pair_id_fkey = fields.One2many('low.match.pairs','id1')
	low_matches_count = fields.Integer(string='Low Match Count', store=True, compute="_compute_low_match_count")

	# micro volunteer fields
	bsp_willingness = fields.Char(string='BSP willingness')
	shoonya_willingness = fields.Char(string='Shoonya willingness')
	rm_vol_ids = fields.Many2many('isha.remote.volunteer', column1='partner_id', column2='rm_vol_id',
									 string='Remote Volunteering')
	proj_supp_ids = fields.Many2many('isha.project.support', column1='partner_id', column2='project_id',
									 string='Project Support')
	lang_ids = fields.Many2many('isha.language.tag', column1='partner_id', column2='lang_id',
									 string='Language')
	qualification_ids = fields.Many2many('isha.qualification.tag', column1='partner_id', column2='qual_id',
										 string='Qualification')

	vol_duration = fields.Selection([('5min','5 Minutes Daily'),('30min','30 Minutes Daily'),
									 ('1hr','1 hour Daily'),('2hr','2 Hours Daily'),
									 ('4hr','4 Hours and above daily'),
									 ('weekend','Weekends'),('3month','3 Months Fulltime in a year')],string='Availability')
	ashram_vol_days = fields.Integer(string='No of days volunteering in ashram')
	portfolio_link = fields.Text(string='Portfolio Link')
	practice_support = fields.Text(string='Practice Support')
	portal_verified = fields.Boolean(string='Profile verified', default=False)
	twitter_handle = fields.Char(string='Twitter Link', groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	facebook_id = fields.Char(string='Facebook Link', groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	facebook_likes = fields.Integer(string='Facebook Likes',
									groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	facebook_followers = fields.Integer(string='Facebook Followers',
										groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')

	instagram_id = fields.Char(string='Instagram Link', groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	linkedin_id = fields.Char(string='Linkedin Link', groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	linkedin_followers = fields.Integer(string='Linkedin Followers',
										groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	wikipedia_link = fields.Char(string='Wikipedia Link',
								 groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	other_links = fields.Char(string='Other Links',
								 groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	clubhouse_user = fields.Char(string='Clubhouse Username',
								 groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	clubhouse_followers = fields.Char(string='Clubhouse Followers',
								 groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	work_company_name = fields.Char(string='Work Company Name')
	work_designation = fields.Char(string='Work Designation')
	work_experience = fields.Integer(string='Work Experience')
	prof_status = fields.Selection([('home_maker','Home Maker'),('student','Student'),('employed','Employed'),('self_employed','Self Employed'),('retired','Retired')],
									string='Occupation Status')
	marketing_rec = fields.Boolean(string='Marketing Record', default=False)
	forum_status = fields.Selection([('unverified','Unverified'),
									('unable_to_verify','Unable to Verify'),('verified','Verified'),
									('verified_manually','Verified Manually'),
									('cancelled','Cancelled')
									],string='Forum Status')

	# These fields are created for Athithi
	system_id = fields.Integer(store=False)
	influencer_type = fields.Selection(
		[('influencer', 'Influencer'), ('pre_influencer', 'Pre-Influencer'), ('relative', 'Relative'),
		 ('do_not_know', 'Do Not Know'), ('unassigned', 'Unassigned')],
		string="Influencer Type")
	category = fields.Selection([('vajra', 'Vajra'), ('suvarna', 'Suvarna'), ('rajata', 'Rajata'), ('kamsyaka', 'Kamsyaka'), ('lauha', 'Lauha')],
                                string="Relationship Status",
                                groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	age = fields.Integer(string='Age', compute='_compute_age')
	designation1 = fields.Char(string='Designation 1')
	company1 = fields.Many2one(string='Company 1', comodel_name='isha.company', ondelete='set null')
	sector1 = fields.Char(string='Sector 1', related='company1.sector', store=True)
	industry1 = fields.Char(string='Industry 1', related='company1.industry', store=True)

	company2 = fields.Many2one(string='Company 2', comodel_name='isha.company', ondelete='set null')
	designation2 = fields.Char(string='Designation 2')
	sector2 = fields.Char(string='Sector 2', related='company2.sector')
	industry2 = fields.Char(string='Industry 2', related='company2.industry')

	company3 = fields.Many2one(string='Company 3', comodel_name='isha.company', ondelete='set null')
	designation3 = fields.Char(string='Designation 3')
	sector3 = fields.Char(string='Sector 3', related='company3.sector')
	industry3 = fields.Char(string='Industry 3', related='company3.industry')
	political_views = fields.Selection([('hard right', 'Hard Right'), ('right', 'Right'), ('center right', 'Center Right'), ('center', 'Center'),
                                        ('center left', 'Center Left'), ('left', 'Left'), ('hard left', 'Hard Left'),], string="Political Views",
									   groups = 'isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer')

	central_category = fields.Selection(
		[('a', 'A'), ('b', 'B'), ('c', 'C'), ('d', 'D')], string="Central Category",
		groups = 'isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	zonal_category = fields.Selection(
		[('a', 'A'), ('b', 'B'), ('c', 'C'), ('d', 'D')], string="Zonal Category",
		groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	influencer_tags = fields.Many2many('isha.influencer.tag', column1='partner_id', column2='influencer_tag_id',
									   string='Influencer Tags',
									   groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	source = fields.Selection([('ashram visit', 'Ashram Visit'), ('virtual events', 'Virtual Events'), ('regional events', 'Regional Events'),
                               ('golf', 'Golf'), ('ihs', 'IHS'), ('ila', 'ILA'), ('isha outreach', 'Isha Outreach'), ('ashram program', 'Ashram Program'),
                               ('msr', 'MSR'), ('dopt', 'DOPT'), ('rfr', 'RFR'), ('cc', 'CC'), ('others', 'Other (Please Specify)')], string="Source", required=False, groups = 'isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	other_source = fields.Char(string='Other Source', required=False,
									 groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	preferred_moc = fields.Char(string='Preferred Mode Of Contact', required=False,
									 groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	net_worth = fields.Selection([('Less than 100k', 'Less than 100k'), ('100k - 1M', '100k - 1M'), ('1M - 5M', '1M - 5M'), ('5M - 10M', '5M - 10M'),
								  ('10M - 100M', '10M - 100M'), ('100M - 500M', '100M - 500M'), ('500M - 1B', '500M - 1B'),
								  ('Greater than 1B', 'Greater than 1B')], string="Net Worth" ,
								 groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer')
	athithi_additional_info = fields.Text(string='Additional Info', required=False,
								groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')

	def _get_poc_domain(self):
		try:
			return [('groups_id', '=', self.env.ref('isha_crm.group_athithi_poc').id)]
		except Exception as ex:
			return []
		
	primary_poc = fields.Many2one(string='Primary POC', comodel_name='res.users', domain=_get_poc_domain,
								  groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	primary_poc_id = fields.Integer(string='Primary POC/ID', related='primary_poc.id')
	secondary_poc = fields.Many2one(string='Secondary POC', comodel_name='res.users', domain=_get_poc_domain,
									groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	other_poc = fields.Many2one(string='Other POC', comodel_name='res.users', domain=_get_poc_domain,
								groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	org_foundation_day = fields.Date(string='Org Foundation Day', groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	wedding_anniversary = fields.Date(string='Wedding Anniversary')

	twitter_followers = fields.Integer(string='Twitter Followers', required=False, groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')
	insta_followers = fields.Integer(string='Instagram Followers', required=False, groups='isha_crm.group_athithi_central_admin,isha_crm.group_athithi_central_viewer,isha_crm.group_athithi_regional_admin,isha_crm.group_athithi_regional_viewer,isha_crm.group_athithi_poc')

	# Fields related to email marketing
	ml_subscription_ids = fields.Many2many('isha.mailing.subscriptions','isha_mailing_subscriptions_rel',string='Eligible Subscriptions')
	ml_unsubscription_ids = fields.One2many('isha.mailing.unsubscriptions','partner_id',string='Unsubscriptions')


	# This field is used to determine whether the merge is low or high in import flow
	local_modified_date = fields.Datetime(store=False,string='Local Modified Date')
	local_contact_id = fields.Char(store=False)

	# Rudraksha Deeksha Fields
	rudraksha_reg = fields.Boolean(string='Rudraksha Registered', groups='isha_crm.group_religious_santosha,isha_crm.group_all_contacts_religious,isha_crm.group_vanashree_grp,isha_crm.group_rudraksha_deeksha')

	# Query optimizing fields
	fmf_reg = fields.Boolean(string='FMF Registered')
	goy_reg = fields.Boolean(string='GOY Registered')

	# No marketing
	no_marketing = fields.Boolean(string='Marketing Restricted')

	selectable_fields = ['id','name', 'category_id', 'street', 'street2', 'city', 'state_id', 'country_id',
						 'mobile', 'phone', 'phone2', 'phone3', 'whatsapp_number',
						 'companies','low_matches_count','rudraksha_reg','fmf_reg','goy_reg','ml_subscription_ids',
						 'email', 'email2', 'zip', 'occupation', 'dob', 'marital_status', 'nationality',
						 'deceased', 'dnd_email', 'dnd_phone', 'dnd_postmail', 'dnd_sms', 'dnd_wa_ext','ieo_r_date',
						 'center_id', 'ieo_date', 'ie_date', 'samyama_date', 'bsp_date', 'shoonya_date',
						 'iecso_date','iecso_r_date',
						 'is_meditator', 'last_txn_date','ieo_progress', 'segment','no_marketing',
						 'event_count', 'pgm_tag_ids', 'pgm_center_id',
						 'gender', 'node_partner_id','total_donation', 'region_name', 'sector_country',
						 'donotallow_stayarea_ids', 'donotallow_program_ids', 'donotallow_volunteering_ids',
						 'donotallow_general_ids', 'religious_tag_ids', 'has_religious_txn', 'has_charitable_txn',
						 'dna_stayarea_comments', 'dna_program_comments', 'dna_volunteering_comments',
						 'dna_general_comments',
						 'partner_severity', 'starmark_category_id', 'incident_date_closed', 'incident_date_assigned',
						 'guid', 'donation_tags','last_sanghamitra_date','last_samarthak_date', 'influencer_tags',
						 'category', 'central_category', 'zonal_category', 'primary_poc','primary_poc_id', 'secondary_poc', 'other_poc',
						 'company1', 'designation1', 'create_date', 'religious_last_txn_date', 'charitable_last_txn_date',
						 'sector1', 'industry1', 'company2', 'designation2', 'sector2', 'industry2', 'company3',
						 'designation3', 'sector3', 'industry3', 'caca_category','email_validity', 'influencer_type',
						 'is_caca_donor', 'twitter_followers', 'insta_followers', 'facebook_likes', 'facebook_followers',
						 'linkedin_followers', 'donation_tags', 'sub_center', 'micro_center', 'nurturing_volunteer',
						 'nurturing_remarks','athithi_additional_info','source','political_views']

	santosha_fields = ['id', 'approx_dob', 'ieo_date','bsp_date', 'center_id', 'city2', 'class_completed', 'country', 'country2',
					   'country2_id', 'dnd_email', 'dnd_phone', 'dnd_postmail', 'dnd_sms','dnd_wa_ext', 'dob', 'email', 'email_is_bounced', 'country_id',
					   'email2', 'email2_is_bounced' , 'email3', 'email3_is_bounced','gender','ie_date','ieo_r_date',
					   'iecso_date', 'iecso_r_date',
					   'ieo_lang_pref', 'last_txn_date', 'center_manually_edited', 'marital_status', 'marketing_rec',
					   'is_meditator', 'nationality', 'ashram_vol_days', 'occupation', 'prof_status', 'phone', 'phone_country_code',
					   'phone2', 'phone2_country_code', 'phone3', 'phone3_country_code', 'phone4', 'phone4_country_code', 'zip', 'zip2',
					   'work_zip', 'pgm_center_id','rudraksha_reg','fmf_reg','goy_reg','ml_subscription_ids',
					   'pgm_tag_ids', 'qualification_ids', 'region_name','rm_vol_ids', 'sso_id', 'samyama_date',
					   'segment', 'shoonya_date', 'skill_tag_ids', 'street2_1', 'street2_1','work_street1', 'work_street2',
					   'phone_is_valid', 'phone2_is_valid', 'phone3_is_valid', 'phone4_is_valid', 'phone_is_verified',
					   'phone2_is_verified', 'phone3_is_verified', 'phone4_is_verified', 'whatsapp_country_code', 'whatsapp_number',
					   'work_company_name', 'work_city', 'work_experience', 'work_designation', 'zip', 'zip2', 'facebook_id', 'linkedin_id',
					   'twitter_handle', 'instagram_id', 'create_uid', 'write_uid',' create_date', 'write_date', 'tz', 'nationality', 'email_normalized',
					   'title', 'contact_type', 'guid', 'display_name', 'industry_id', 'mobile','name','city','country','ieo_lang_pref','lang',
					   'company_id','portal_verified','state','street','street2','street2_1','street2_2','category_id',
					   'zip','zip2','work_designation', 'caca_category','email_validity','deceased', 'influencer_type',
					   'is_caca_donor', 'sector_country', 'no_marketing', 'sub_center', 'micro_center', 'nurturing_volunteer',
						 'nurturing_remarks', 'religious_tag_ids', 'has_religious_txn', 'has_charitable_txn',
					   'religious_last_txn_date', 'charitable_last_txn_date']

	athithi_fields = ['twitter_followers', 'insta_followers', 'facebook_likes', 'facebook_followers', 'linkedin_followers',
				  'donation_tags','company1','sector1','industry1','influencer_tags','primary_poc','athithi_additional_info','source','political_views']

	sangam_fields = [
		'influencer_type','last_sanghamitra_date','last_samarthak_date',
		'contact_donations_outreach_id_fkey', 'contact_donations_foundation_id_fkey',
		'contact_donations_education_id_fkey', 'contact_donations_iii_id_fkey', 'contact_donations_arogya_id_fkey',
		'is_caca_donor', 'is_hni', 'is_annadhanam', 'last_donation_date', 'last_donation_amount'
	]

	ims_fields = [
		'donotallow_stayarea_ids', 'donotallow_program_ids', 'donotallow_volunteering_ids',
		'donotallow_general_ids',
		'dna_stayarea_comments', 'dna_program_comments', 'dna_volunteering_comments',
		'dna_general_comments',
		'partner_severity', 'starmark_category_id', 'incident_date_closed', 'incident_date_assigned'
	]

	@api.model
	def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
		context = self._context or {}
		if context.get('center_scope'):
			domain += [('center_id','in', self.env.user.centers.ids)]
			charitable = self.user_has_groups(CHARITABLE_GROUPS)
			religious = self.user_has_groups(RELIGIOUS_GROUPS)
			rel_char_domain = []
			if charitable:
				rel_char_domain += [('has_charitable_txn', '=', True)]
			if religious:
				rel_char_domain = (['|', ('has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
					else [('has_religious_txn', '=', True)]
			domain = rel_char_domain + domain

		return super(ResPartner, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
													  orderby=orderby, lazy=lazy)

	@api.onchange('wikipedia_link')
	def _onchange_wiki_link(self):
		if self.wikipedia_link and not self.wikipedia_link.startswith('http'):
			self.wikipedia_link = 'http://' + self.wikipedia_link

	@api.onchange('other_links')
	def _onchange_other_links(self):
		if self.other_links and not self.other_links.startswith('http'):
			self.other_links = 'http://' + self.other_links

	@api.onchange('twitter_handle')
	def _onchange_twitter_link(self):
		if self.twitter_handle and not self.twitter_handle.startswith('http'):
			self.twitter_handle = 'http://' + self.twitter_handle

	@api.onchange('instagram_id')
	def _onchange_instagram_link(self):
		if self.instagram_id and not self.instagram_id.startswith('http'):
			self.instagram_id = 'http://' + self.instagram_id

	@api.onchange('facebook_id')
	def _onchange_facebook_link(self):
		if self.facebook_id and not self.facebook_id.startswith('http'):
			self.facebook_id = 'http://' + self.facebook_id

	@api.onchange('linkedin_id')
	def _onchange_linkedin_link(self):
		if self.linkedin_id and not self.linkedin_id.startswith('http'):
			self.linkedin_id = 'http://' + self.linkedin_id

	@api.onchange('athithi_additional_info')
	def _onchange_athithi_additional_info(self):
		if self.athithi_additional_info:
			for info_str in self.athithi_additional_info.split():
				if info_str.startswith('http'):
					info_link = '<a href="%s" target="_blank">%s </a>' % (info_str,info_str)
					self.athithi_additional_info = self.athithi_additional_info.replace(info_str,info_link)


	@api.depends('iv_last_donated_date')
	def _compute_iv_donor_status(self):
		for rec in self:
			if rec.iv_last_donated_date:
				if rec.iv_last_donated_date < (datetime.date.today() - relativedelta(months=15)):
					rec.iv_donor_status = 'inactive'
				else:
					rec.iv_donor_status = 'active'
			else:
				rec.iv_donor_status = None

	@api.depends('dob','approx_dob')
	def _compute_age(self):
		today = date.today()
		for rec in self:
			if rec.dob and rec.dob > today:
				raise ValidationError(_('Date of Birth can not be future date'))
			valid_dob = rec.dob or rec.approx_dob
			if valid_dob:
				rec.age = today.year - valid_dob.year - ((today.month, today.day) < (valid_dob.month, valid_dob.day))
			else:
				rec.age = None

	@api.model
	def load_views(self, views, options=None):
		context = self._context or {}
		if context.get('global_search'):
			global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
			self = self.with_user(global_data_user_id)
		return super(ResPartner, self).load_views(views, options)

	def read(self, fields=None, load='_classic_read'):
		context = self._context or {}
		if context.get('global_search'):
			global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
			self = self.with_user(global_data_user_id)
			# global_data_user_search = self.env['res.users'].sudo().search([('name', '=', 'Global data user (system related)')])
			# if global_data_user_search and len(global_data_user_search) > 0:
			# 	global_data_user_id = global_data_user_search[0].id
			# 	self = self.with_user(global_data_user_id)
		return super(ResPartner, self).read(fields, load)

	@api.model
	def search(self, args, offset=0, limit=None, order=None, count=False):
		context = self._context or {}
		# this is intended to separate the access to global search from the rest to keep it cleaner.
		# to use it, we can pass the 'global_search' in the context from the 'global search / Contacts' menu
		# Then we won't need to open up all contacts to the users. We can remove the record rule for Global search access.
		# The access to global data would then be only available to a special user. We would fetch the data with this
		# user's privileges and show to in the Global search results.
		if context.get('global_search'):
			global_data_user_id = self.env.ref('isha_crm.isha_global_data_user').id
			self = self.with_user(global_data_user_id)
		return super(ResPartner, self).search(args, offset, limit, order, count)

	@api.model
	def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
		# todo: search on all 3 companies
		context = self._context or {}
		order_prepend = ''
		if context.get('center_scope'):
			args += [('center_id', 'in', self.env.user.centers.ids)]
			charitable = self.user_has_groups(CHARITABLE_GROUPS)
			religious = self.user_has_groups(RELIGIOUS_GROUPS)
			rel_char_domain = []
			if charitable:
				rel_char_domain += [('has_charitable_txn', '=', True)]
			if religious:
				rel_char_domain = (['|', ('has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
					else [('has_religious_txn', '=', True)]
			args = rel_char_domain + args

		if context.get('scope_primary_contacts'):
			args += [('primary_poc', '=', self.env.user.id)]
		if context.get('scope_secondary_contacts'):
			args += [('secondary_poc', '=', self.env.user.id)]
		if context.get('scope_other_contacts'):
			args += [('other_poc', '=', self.env.user.id)]
		if context.get('fuzzy_name_search'): # Fuzzy name search from domain_filter
			fuzzy_token = context.get('fuzzy_name_search')[0]
			self.env.cr.execute("SELECT set_limit(0.4);")
			order_prepend = "similarity(%s.name, '%s') DESC" % (self._table,fuzzy_token)
		if context.get('fuzzy_phone_search'): # Fuzzy phone search from domain_filter
			fuzzy_token = context.get('fuzzy_phone_search')[0]
			self.env.cr.execute("SELECT set_limit(0.45);")
			order_prepend = "similarity(%s.phone, '%s') DESC" % (self._table, fuzzy_token)

		if order and len(order) > 0 and len(order_prepend) > 0:
			order = order_prepend + ", " + order
		elif len(order_prepend) > 0:
			order = order_prepend

		# default - do not show any results
		if len(args) == 0 and context.get('no_default_search'):
			return []
		results = super(ResPartner, self)._search(args, offset, limit, order, count=count,
												  access_rights_uid=access_rights_uid)

		if context.get('limit_search'):
			if not count and len(results) > 10:
				raise UserError('Search result too broad. Please refine your query to match less than 10 records.')
		return results

	def unlink(self):
		self.write({'active': False})

	# override onchange by phone_validation lib and do nothing
	def _onchange_phone_validation(self):
		return

	@api.depends('contact_low_match_pair_id_fkey')
	def _compute_low_match_count(self):
		"""Count the number of low matches this partner has for Smart Button
		Don't count inactive matches.
		"""
		for rec in self:
			rec.low_matches_count = len(rec.contact_low_match_pair_id_fkey.filtered('active'))

	def is_zip_valid(self):
		goldenContact = GoldenContactAdv.GoldenContactAdv()
		goldenContact.setEnvValues(self.env)
		for rec in self:
			center_dict = goldenContact.getCenterId(rec.city,rec.zip,rec.country,rec.center_manually_edited)
			rec.is_valid_zip = center_dict['is_valid_zip']
			if not rec.center_manually_edited and center_dict['is_valid_zip']:
				rec.center_id = center_dict['center_id']
				rec.region_name= center_dict['region_name']
				if 'zip' in center_dict:
					rec.zip = center_dict['zip']

	# @api.depends('phone_is_valid')
	# def _set_valid_first_phone(self):
	# 	_logger.debug('Phone validation check')
	# 	for rec in self:
	# 		if not rec.phone_is_valid:
	# 			phones_list = ['phone2','phone3','phone4']
	# 			for x in phones_list:
	# 				if rec[x]:
	# 					update_dict = {
	# 						'phone': rec[x],
	# 						'phone_is_valid': rec[x+'_is_valid'],
	# 						'phone_is_verified': rec[x+'_is_verified'],
	# 						'phone_country_code': rec[x+'_country_code'],
	# 						x: rec['phone'],
	# 						x+'_is_valid': rec['phone_is_valid'],
	# 						x+'_is_verified': rec['phone_is_verified'],
	# 						x+'_country_code': rec['phone_country_code'],
	# 					}
	# 					rec.update(update_dict)
	# @api.depends('contact_contact_map_id_fkey','contact_donations_foundation_id_fkey','contact_donations_outreach_id_fkey','contact_donations_education_id_fkey','contact_donations_arogya_id_fkey','contact_donations_iii_id_fkey')
	# def _compute_last_txn_date(self):
	# 	print('HIT last txn date')
	# 	for rec in self:
	# 		date_list = []
	# 		try:
	# 			if len(rec.contact_med_prod_id_fkey) > 0:
	# 				for x in rec.contact_med_prod_id_fkey:
	# 					if x.med_product_date:
	# 						date_list.append(x.med_product_date)
	# 			if len(rec.contact_donations_foundation_id_fkey) > 0:
	# 				for x in rec.contact_donations_foundation_id_fkey:
	# 					if x.don_receipt_date:
	# 						date_list.append(x.don_receipt_date )
	# 			if len(rec.contact_donations_education_id_fkey) > 0:
	# 				for x in rec.contact_donations_education_id_fkey:
	# 					if x.don_receipt_date:
	# 						date_list.append(x.don_receipt_date )
	# 			if len(rec.contact_donations_arogya_id_fkey) > 0:
	# 				for x in rec.contact_donations_arogya_id_fkey:
	# 					if x.don_receipt_date:
	# 						date_list.append(x.don_receipt_date )
	# 			if len(rec.contact_donations_iii_id_fkey) > 0:
	# 				for x in rec.contact_donations_foundation_id_fkey:
	# 					if x.don_receipt_date:
	# 						date_list.append(x.don_receipt_date )
	# 			last_txn_date = max(filter(None, date_list)) if len(date_list) > 0 else None
	# 			if last_txn_date and rec.last_txn_date:
	# 				rec.last_txn_date = max(last_txn_date, rec.last_txn_date)
	# 			elif last_txn_date:
	# 				rec.last_txn_date = rec.last_txn_date
	# 		except Exception as ex:
	# 			tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
	# 			_logger.debug(tb_ex)

	# @api.depends("contact_donations_foundation_id_fkey")
	# def
	# (self):
	# 	for rec in self:
	# 		rec.is_annadhanam = any(
	# 			x.purpose_id.project_name == 'Annadhanam' for x in rec.contact_donations_foundation_id_fkey)
	# 		rec.is_hni = sum(x.amount for x in rec.contact_donations_foundation_id_fkey) >= 50000
	#
	# @api.depends("contact_donations_outreach_id_fkey")
	# def _compute_don_outreach_fields(self):
	# 	for rec in self:
	# 		rec.is_caca_donor = any(
	# 			x.purpose_id.project_name.__contains__('Cauvery Calling') for x in rec.contact_donations_outreach_id_fkey)
	@api.depends("contact_ieo_id_fkey")
	def _compute_ieo_date(self):
		for rec in self:
			ieo_date = None
			last_txn_date = None
			ieo_progress = None
			ieo_recs = self.env['ieo.record'].search([('contact_id_fkey','=',rec.id)]).sorted(key=lambda p: p.write_date)
			for x in ieo_recs:
				if not ieo_progress:
					ieo_progress = x.progress
				if x.course_completion_date:
					if ieo_date is None:
						ieo_date = x.course_completion_date
						ieo_progress = x.progress
					elif ieo_date > x.course_completion_date:
						ieo_date = x.course_completion_date
						ieo_progress = x.progress
				if x.last_login_date:
					if last_txn_date is None:
						last_txn_date = x.last_login_date
					elif last_txn_date < x.last_login_date:
						last_txn_date = x.last_login_date
				elif x.last_upd_time and not last_txn_date:
					last_txn_date = x.last_upd_time.date()
				elif x.start_date and not last_txn_date:
					last_txn_date = x.start_date.date()
					# add pre-defined group by for sector/country in contact view
			if len(ieo_recs)>0:
				if ieo_recs[0].start_date:
					rec.ieo_r_date = ieo_recs[0].start_date.date()
				if ieo_progress:
					rec.ieo_progress = ieo_progress
				if last_txn_date:
					if not rec.last_txn_date or (rec.last_txn_date and rec.last_txn_date < last_txn_date):
						rec.last_txn_date = last_txn_date
						rec.charitable_last_txn_date = last_txn_date
				if ieo_date:
					rec.ieo_date = ieo_date
					rec.pgm_tag_ids = [(3, self.env.ref('isha_crm.res_partner_pgm_tag_19').id),(4, self.env.ref('isha_crm.res_partner_pgm_tag_17').id)]
				elif len(ieo_recs) > 0 and not ieo_date:
					rec.pgm_tag_ids = [(3, self.env.ref('isha_crm.res_partner_pgm_tag_17').id),(4, self.env.ref('isha_crm.res_partner_pgm_tag_19').id)]
					rec.ieo_date = None
				elif not ieo_date:
					rec.ieo_date = None
			else:
				rec.ieo_date = None

	def _get_lang_pref(self):
		for rec in self:
			if len(rec.contact_ieo_id_fkey)>0:
				rec.ieo_lang_pref = rec.contact_ieo_id_fkey[0].lang_pref
			else:
				rec.ieo_lang_pref = None

	@api.depends("contact_program_attendance_id_fkey")
	def _compute_pgm_fields(self):
		for rec in self:
			if rec['id'] != 2:
				update_val = {}
				last_txn_date = False
				is_meditator = False
				ie_date = False
				bsp_date = False
				shoonya_date = False
				samyama_date = False

				# Honour the iec_mega_pgm txn for meditator flag
				if rec.iecso_date:
					is_meditator = True
					ie_date = rec.iecso_date
					last_txn_date = rec.iecso_date

				# Below variable is to indicate if the pgm_tag_id is already updated.
				updated = 0
				for pgm in rec.contact_program_attendance_id_fkey:
					pgm_mst = pgm.pgm_type_master_id
					if len(pgm_mst) != 0:
						if pgm_mst.category == 'Inner Engineering':
							ie_date = pgm.start_date
							rec.update({'pgm_tag_ids':[(4, self.env.ref('isha_crm.res_partner_pgm_tag_1').id)]})
							if pgm.center_name:
								temp = self.env['isha.center'].search([('name','=',pgm.center_name)])
								if len(temp) >= 1:
									update_val.update({'pgm_center_id': temp[0].id})
									if not rec.center_id or rec.center_id.is_bucket_center:
										update_val.update({'center_id':temp[0].id,'region_name':temp[0].region_id.name})

						if pgm_mst.category == 'Bhava Spandana':
							bsp_date = pgm.start_date
							rec.update({'pgm_tag_ids': [(4, self.env.ref('isha_crm.res_partner_pgm_tag_10').id)]})
						if pgm_mst.category == 'Samyama':
							samyama_date = pgm.start_date
							rec.update({'pgm_tag_ids': [(4, self.env.ref('isha_crm.res_partner_pgm_tag_11').id)]})
						if pgm_mst.category == 'Shoonya':
							shoonya_date = pgm.start_date
							rec.update({'pgm_tag_ids': [(4, self.env.ref('isha_crm.res_partner_pgm_tag_13').id)]})
							# below logic is for contacts who have more than one program type with category Hatha Yoga
						if updated == 0:
							if pgm_mst.program_type == 'Yogasanas' or re.search("^.*Days Hatha Yoga$",pgm_mst.program_type) or re.search("^.*Day Hatha Yoga$", pgm_mst.program_type):
								rec.update({'pgm_tag_ids': [(4, self.env.ref('isha_crm.res_partner_pgm_tag_15').id)]})
								updated = 1
						if pgm_mst.category == 'Uyir Nokkam':
							rec.update({'pgm_tag_ids': [(4, self.env.ref('isha_crm.res_partner_pgm_tag_18').id)]})
						if pgm_mst.category == 'Shivanga Sadhana':
							rec.update({'category_id': [(4, self.env.ref('isha_crm.res_partner_category_19').id)]})
						is_meditator = is_meditator or pgm_mst.meditator_qualifier
						if last_txn_date and pgm['start_date']:
							last_txn_date = max(last_txn_date, pgm['start_date'])
						elif pgm['start_date']:
							last_txn_date = pgm['start_date']
				if len(rec.contact_program_attendance_id_fkey) > 0:
					if is_meditator and rec.portal_verified:
						update_val.update({'forum_status':'verified'})
					if not is_meditator and rec.portal_verified:
						update_val.update({'forum_status':'unverified'})
					update_val['is_meditator'] = is_meditator
					update_val['ie_date'] = ie_date if ie_date else None
					update_val['bsp_date'] = bsp_date if bsp_date else None
					update_val['shoonya_date'] = shoonya_date if shoonya_date else None
					update_val['samyama_date'] = samyama_date if samyama_date else None
					update_val['last_txn_date'] = last_txn_date if last_txn_date and last_txn_date > rec['last_txn_date'] else rec['last_txn_date']
					rec.update(update_val)

	@api.depends("contact_program_lead_id_fkey")
	def _compute_pgm_lead_fields(self):
		for rec in self:
			if rec['id'] != 2:
				# below is to update the pgm tag for Surya Shakti yogaveera
				pgm_lead_ids = rec.contact_program_lead_id_fkey.ids
				for pgm in self.env['program.lead'].browse(pgm_lead_ids):
					if pgm.program_name == 'Surya Shakthi Yogaveera Training Registrations' and pgm.reg_status == 'ATTENDED' and rec.is_meditator is not True:
						rec.update({'pgm_tag_ids': [(4, self.env.ref('isha_crm.res_partner_pgm_tag_24').id)]})

	@api.onchange('center_id')
	def _onchange_center_id(self):
		if self.center_id:
			self.center_manually_edited = True
			self.region_name = self.center_id.region_id.name

	@api.onchange('zip')
	def _validate_zip_ipc(self):
		if self.zip and self.country == 'IN':
			rec = self.env['isha.pincode.center'].search([('name','=',self.zip),('countryiso_2','=','IN')])
			if len(rec) == 0:
				return {'warning': {'title': "Warning", 'message': "Invalid Zip!!!", 'type': 'notification'}}
			if len(rec) > 0:
				self.city = rec[0].citydistricttown

	@api.onchange('phone')
	def _onchange_phone(self):
		if self.phone:
			self.phone_is_valid = True
			self.phone_is_verified = True
		else:
			self.phone_is_valid = False
			self.phone_is_verified = False

	@api.onchange('phone2')
	def _onchange_phone2(self):
		if self.phone2:
			self.phone2_is_valid = True
			self.phone2_is_verified = True
		else:
			self.phone2_is_valid = False
			self.phone2_is_verified = False

	@api.onchange('phone3')
	def _onchange_phone3(self):
		if self.phone3:
			self.phone3_is_valid = True
			self.phone3_is_verified = True
		else:
			self.phone3_is_valid = False
			self.phone3_is_verified = False

	@api.onchange('phone4')
	def _onchange_phone4(self):
		if self.phone4:
			self.phone4_is_valid = True
			self.phone4_is_verified = True
		else:
			self.phone4_is_valid = False
			self.phone4_is_verified = False

	@api.onchange('nationality_id')
	def _onchange_nationality(self):
		self.ensure_one()
		self.nationality = self.nationality_id.code

	@api.onchange('country_id')
	def _onchange_country_id(self):
		self.ensure_one()
		self.country = self.country_id.code

	@api.onchange('state_id')
	def _onchange_state_id(self):
		self.ensure_one()
		self.state = self.state_id.name

	@api.onchange('country2_id')
	def _onchange_country2_id(self):
		self.ensure_one()
		self.country2 = self.country2_id.code

	@api.onchange('state2_id')
	def _onchange_state2_id(self):
		self.ensure_one()
		self.state2 = self.state2_id.name

	@api.onchange('work_country_id')
	def _onchange_work_country_id(self):
		self.ensure_one()
		self.work_country = self.work_country_id.code

	@api.onchange('work_state_id')
	def _onchange_work_state_id(self):
		self.ensure_one()
		self.work_state = self.work_state_id.name

	@api.depends('name', 'email')
	def _compute_email_formatted(self):
		for partner in self:
			if partner.email:
				try:
					partner.email_formatted = formataddr((partner.name or u"False", partner.email or u"False"))
				except Exception as ex:
					tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
					_logger.error("ERROR: "+tb_ex)
					partner.email_formatted = ''
			else:
				partner.email_formatted = ''

	@api.model
	def fields_get(self, allfields=None, attributes=None):
		res = super(ResPartner, self).fields_get(allfields, attributes=attributes)
		if self._context.get('restrict_fields_get_list'):
			not_selectable_fields = set(res.keys()) - set(self._context.get('restrict_fields_get_list'))
			for field in not_selectable_fields:
				res[field]['selectable'] = False  # to hide in Add Custom filter view
				res[field]['sortable'] = False  # to hide in group by view
				# res[field]['exportable'] = False # to hide in export list
				res[field]['store'] = False  # to hide in 'Select Columns' filter in tree views
		else:
			not_selectable_fields = set(res.keys()) - set(self.selectable_fields)
			searchable_list = self.santosha_fields
			if self.env.user.has_group('isha_crm.group_sangam'):
				searchable_list += self.sangam_fields
			if self.env.user.has_group('isha_ims.group_ims'):
				searchable_list += self.ims_fields
			if self.env.user.has_group('isha_crm.group_athithi_central_admin') or \
				self.env.user.has_group('isha_crm.group_athithi_central_viewer') or\
				self.env.user.has_group('isha_crm.group_athithi_regional_admin') or \
				self.env.user.has_group('isha_crm.group_athithi_regional_viewer') or \
				self.env.user.has_group('isha_crm.group_athithi_poc'):
				searchable_list += self.athithi_fields
			non_searchable_fields = set(res.keys()) - set(searchable_list)
			for field in not_selectable_fields:
				res[field]['selectable'] = False # to hide in Add Custom filter view
				res[field]['sortable'] = False # to hide in group by view
				res[field]['exportable'] = False # to hide in export list
				res[field]['store'] = False # to hide in 'Select Columns' filter in tree views
			for field in non_searchable_fields:
				res[field]['searchable'] = False
				# writing this because even if this field is in santosha_fields, its still coming in non_searchable_fields
				if field == 'create_date':
					res[field]['searchable'] = True
		return res

	def name_phone_get(self):
		result = []
		for rec in self:
			name = ', '.join(filter(None, [rec.name, rec.phone, rec.email]))
			result.append((rec.id, name))
		return result

	def name_get(self):
		write_date = self._context.get('include_write_date')
		if write_date:
			result = []
			for rec in self:
				write_date_time = (rec.write_date + timedelta(hours=5, minutes=30)).strftime("%d/%m/%Y %H:%M:%S")
				name = ', '.join(filter(None, [rec.name, rec.phone, write_date_time]))
				result.append((rec.id, name))
			return result
		else:
			return super(ResPartner, self).name_get()

	@api.model
	def _name_search(self, name='', args=None, operator='ilike', limit=100):
		source_args = []
		# This condition is for the merge wizard to pre-populate the dst_partner from partner_ids
		# ref -> base/wizard/base_partner_merge_views.xml
		if args and len(args) > 0 and len(name)==0:
			return super(ResPartner,self)._name_search(name,args,operator,limit)

		if args and len(args) > 0:
			source_args = args
		# Phone Search
		regexnum = re.compile(r'^[+]?([0-9]\d*|0)$')
		if name is not None and regexnum.match(name):
			if len(name) < 6:
				return [(False,"Please Type more than 6 digits")]
			else:
				rec_list = []
				if len(self.env.user.centers.ids) > 0:
					rec_list = self.env['res.partner'].search(source_args + [('phone','=like',name+"%"),('center_id','in', self.env.user.centers.ids)], limit=limit)
				if len(rec_list) == 0:
					rec_list = self.env['res.partner'].search(source_args + [('phone', '=like', name + "%")],
															  limit=limit)
				if rec_list:
					return rec_list.name_phone_get()
		elif name is not None:
			if len(name) < 5:
				return [(False, "Please Type more than 5 characters")]
			rec_list = []
			# Email search
			if '@' in name:
				if len(self.env.user.centers.ids) > 0:
					rec_list = self.env['res.partner'].search(source_args +
						[('email', '=ilike', name + '%'),('center_id', 'in', self.env.user.centers.ids)], limit=limit)
				if len(rec_list) == 0:
					rec_list = self.env['res.partner'].search(source_args +
						[('email', '=ilike', name + '%')], limit=limit)
				if rec_list:
					return rec_list.name_phone_get()
			else:
				# Name Search
				if len(self.env.user.centers.ids) > 0:
					rec_list = self.env['res.partner'].search(source_args + [('name', 'ilike', name),('center_id','in', self.env.user.centers.ids)],limit=limit)
				if len(rec_list) == 0:
					rec_list = self.env['res.partner'].search(source_args + [('name', 'ilike', name)], limit=limit)
				if rec_list:
					return rec_list.name_phone_get()
		return []

	# @profile('/tmp/res_write.profile')
	def write(self, values):
		# FIXME: This is affecting the forum profile submission as we show only phone 1 but get error for other phones
		# if 'kafka_flow' not in values:
		# 	for rec in self:
		# 		rec._mandate_phone_code(values)
		# 		rec._validate_values(values)
		# else:
		# 	values.pop('kafka_flow')
		if 'whatsapp_number' in values and self.whatsapp_validity == 'invalid':
			values['whatsapp_validity'] = None
		values.pop('kafka_flow',None)
		if 'active' in values and not values['active']:
			_logger.info('Contact being set as inactive: id: ' + str(self.id))

		if not self.env.user.has_group('isha_crm.group_athithi_central_admin') and not self.env.user.has_group('isha_crm.group_athithi_regional_admin') and\
			('primary_poc' in values or 'secondary_poc' in values or 'other_poc' in values):
			raise UserError('This user has no privilege to change the POC assignment')
		if ('iv_email' in values and 'import_file' not in self._context and self.env.user.login != 'admin'
			and self.env.user.login != '__system__'):
			values['iv_email_manually_edited'] = True
		#athithi tracking check
		for rec in self:
			if rec.influencer_type:
				rec.track_field_changes(values)
		ret_vals = super(ResPartner,self).write(values)
		if ('zip' in values or 'country_id' in values) and 'center_id' not in values:
			self.is_zip_valid()

		not_verified_id = self.env['res.partner.category'].search([('name', '=', 'Unable to Verify')])[0].id
		verified_id = self.env['res.partner.category'].search([('name', '=', 'Verified - Manually')])[0].id
		if verified_id in self.category_id.ids and not_verified_id in self.category_id.ids:
			self.write({'category_id': [(3, not_verified_id)]})

		# Don't set the validity unless it is from sso
		# update_vals = {}
		# # clear the bounced flag if an email is updated without providing the bounced info.
		# if 'email' in values and 'email_validity' not in values:
		# 	update_vals['email_validity'] = 'valid'
		# if 'email2' in values and 'email2_validity' not in values:
		# 	update_vals['email2_validity'] = 'valid'
		# if 'email3' in values and 'email3_validity' not in values:
		# 	update_vals['email3_validity'] = 'valid'
		#
		# if len(update_vals) > 0:
		# 	self.sudo().write(update_vals)

		return ret_vals

	def track_field_changes(self, values):
		update_keys = values.keys()
		old_val_dict = {}
		dict_fields = self.fields_get(update_keys)
		for key in update_keys:
			if self[key] != values[key]:
				field_name = dict_fields[key]['string']
				field_type = dict_fields[key]['type']
				if field_type == 'many2many' or field_type == 'many2one':
					old_val = self[key].mapped('name')
				else:
					old_val = self[key]
				if self[key]:
					old_val_dict[field_name] = str(old_val)
				else:
					old_val_dict[field_name] = None
		if old_val_dict != {}:
			self.message_post(body='%s' % str(old_val_dict))

	def unlink(self):
		self.write({'active':False})

	def _mandate_phone_code(self, val):
		phone_cc_error_list = []
		phone_error_list = []
		phone = None
		phone_country_code = None
		phone2 = None
		phone2_country_code = None
		phone3 = None
		phone3_country_code = None
		phone4 = None
		phone4_country_code = None
		whatsapp_number = None
		whatsapp_country_code = None
		# if either phone or country code has changed, take the latest values.
		# we want to mandate only for currently edited phones.
		if 'phone' in val or 'phone_country_code' in val:
			phone = val['phone'] if 'phone' in val else self.phone
			phone_country_code = val['phone_country_code'] if 'phone_country_code' in val else self.phone_country_code
		if 'phone2' in val or 'phone2_country_code' in val:
			phone2 = val['phone2'] if 'phone2' in val else self.phone2
			phone2_country_code = val['phone2_country_code'] if 'phone2_country_code' in val else self.phone2_country_code
		if 'phone3' in val or 'phone3_country_code' in val:
			phone3 = val['phone3'] if 'phone3' in val else self.phone3
			phone3_country_code = val['phone3_country_code'] if 'phone3_country_code' in val else self.phone3_country_code
		if 'phone4' in val or 'phone4_country_code' in val:
			phone4 = val['phone4'] if 'phone4' in val else self.phone4
			phone4_country_code = val['phone4_country_code'] if 'phone4_country_code' in val else self.phone4_country_code
		if 'whatsapp_number' in val or 'whatsapp_country_code' in val:
			whatsapp_number = val['whatsapp_number'] if 'whatsapp_number' in val else self.whatsapp_number
			whatsapp_country_code = val['whatsapp_country_code'] if 'whatsapp_country_code' in val else self.whatsapp_country_code
		if phone and not phone_country_code:
			phone_cc_error_list.append('Phone 1')
		if phone2 and not phone2_country_code:
			phone_cc_error_list.append('Phone 2')
		if phone3 and not phone3_country_code:
			phone_cc_error_list.append('Phone 3')
		if phone4 and not phone4_country_code:
			phone_cc_error_list.append('Phone 4')
		if whatsapp_number and not whatsapp_country_code:
			phone_cc_error_list.append('Whatsapp number')

		phone_type = val['phone_type'] if 'phone_type' in val else self.phone_type
		phone2_type = val['phone2_type'] if 'phone2_type' in val else self.phone2_type
		phone3_type = val['phone3_type'] if 'phone3_type' in val else self.phone3_type
		phone4_type = val['phone4_type'] if 'phone4_type' in val else self.phone4_type
		if phone_type and not phone:
			phone_error_list.append('Phone 1')
		if phone2_type and not phone2:
			phone_error_list.append('Phone 2')
		if phone3_type and not phone3:
			phone_error_list.append('Phone 3')
		if phone4_type and not phone4:
			phone_error_list.append('Phone 4')

		string_msg = ''
		if len(phone_cc_error_list) > 0:
			string_msg = 'Country code is missing for ' + ', '.join(phone_cc_error_list) + '\n'
		if len(phone_error_list) > 0:
			string_msg = 'Phone value is missing for ' + ', '.join(phone_error_list)
		if len(string_msg) > 0:
			raise UserError(string_msg)

	# @profile('/tmp/res_create.profile')
	@api.model_create_multi
	def create(self, vals_list):
		return_list_ids = []
		product_upload = False
		# FIXME: This is affecting the forum profile submission as we show only phone 1 but get error for other phones
		# if ('import_file' not in self._context or not self._context['import_file']) and 'kafka_import' not in self._context:
		# 	for val in vals_list:
		# 		self._mandate_phone_code(val)
		if 'ivdms_import_file' in self._context or 'kafka_import' in self._context:
			for val in vals_list:
				val['has_charitable_txn'] = True
				if 'country' in val and val['country']:
					country = val['country'].upper()
					if country in iso2:
						country = iso2[country]
					elif country in correction and correction[country] in iso2:
						country = iso2[correction[country]]
					val['country'] = country

		if 'model' in self.env.context.keys():
			active_model = self.env.context.get('model')
			if active_model == 'meditation.product':
				product_upload = True
				for val in vals_list:
					val['system_id'] = 20  # Yantra Data
		for val in vals_list:
			if 'no_match_n_merge' in val and val['no_match_n_merge']:
				# create new contact
				return self.phase4Create(vals_list)

			create_contact = False
			if 'anonymous' in val['name'].lower():
			# if 'ivdms_import_file' in self._context and not val['phone'] and not val['email']:
				# Link to anonymous contact if no phone or email is present.
				try:
					rec = self.env.ref('isha_crm.res_partner_anonymous')
				except ValueError:
					create_contact = True
			else:
				create_contact = True

			if create_contact:
				contact_processor = ContactProcessor.crmContact()
				# system 0
				odoo_dict = contact_processor.getOdooFormat(val, {}, {})
				# FIXME: portal flow is getting affected
				# self._validate_values(odoo_dict)
				odoo_dict = self.cleanse_values(odoo_dict)
				if 'ivdms_import_file' in self._context or 'kafka_import' in self._context:
					odoo_dict['low_auth_data'] = True
					odoo_dict['system_id'] = 47
				rec = self.create_internal(odoo_dict)
			return_list_ids.append(rec.id)
		if product_upload:
			for val in vals_list:
				val.pop('system_id', None)
		return self.env['res.partner'].browse(return_list_ids)

	def is_phone_valid(self, rec_changes, country_code_field, phone_number_field):
		country_code = None
		phone_number = None
		if phone_number_field in rec_changes or country_code_field in rec_changes:
			# in edit case, if one of the fields is not edited, get the current value.
			country_code = rec_changes[country_code_field] if country_code_field in rec_changes else self[country_code_field]
			phone_number = rec_changes[phone_number_field] if phone_number_field in rec_changes else self[phone_number_field]

		# if both country code and number are not present, then consider as valid.
		if not country_code and not phone_number:
			return True
		pn = phonenumber.PhoneNumber(country_code=country_code, national_number=phone_number)
		return phonenumbers.is_valid_number(pn)

	def cleanse_values(self, odoo_rec):
		name_dict = getTitlesRemoved(odoo_rec['name'])
		odoo_rec['name'] = name_dict['name']
		odoo_rec['prof_dr'] = name_dict['title'] if odoo_rec['prof_dr'] is None else odoo_rec['prof_dr']

		odoo_rec['phone_src'] = odoo_rec['phone']
		odoo_rec['phone2_src'] = odoo_rec['phone2']
		odoo_rec['phone3_src'] = odoo_rec['phone3']
		odoo_rec['phone4_src'] = odoo_rec['phone4']

		odoo_rec['phone_is_valid'] = True
		odoo_rec['phone2_is_valid'] = True
		odoo_rec['phone3_is_valid'] = True
		odoo_rec['phone4_is_valid'] = True

		odoo_rec['phone_is_verified'] = False
		odoo_rec['phone2_is_verified'] = False
		odoo_rec['phone3_is_verified'] = False
		odoo_rec['phone4_is_verified'] = False
		return odoo_rec

	def _validate_values(self, odoo_rec):
		p1_valid = self.is_phone_valid(odoo_rec, 'phone_country_code', 'phone')
		p2_valid = self.is_phone_valid(odoo_rec, 'phone2_country_code', 'phone2')
		p3_valid = self.is_phone_valid(odoo_rec, 'phone3_country_code', 'phone3')
		p4_valid = self.is_phone_valid(odoo_rec, 'phone4_country_code', 'phone4')
		whatsapp_valid = self.is_phone_valid(odoo_rec, 'whatsapp_country_code', 'whatsapp_number')

		invalid_phone_list = []
		if not p1_valid:
			invalid_phone_list.append('Phone 1')
		if not p2_valid:
			invalid_phone_list.append('Phone 2')
		if not p3_valid:
			invalid_phone_list.append('Phone 3')
		if not p4_valid:
			invalid_phone_list.append('Phone 4')
		if not whatsapp_valid:
			invalid_phone_list.append('Whatsapp Number')

		if len(invalid_phone_list) > 0:
			raise UserError('Invalid phone number or country code for ' + ', '.join(invalid_phone_list))

	def create_internal(self, val):
		if 'email' in val and val['email']:
			val['email'] = val['email'].lower()
		if 'email2' in val and val['email2']:
			val['email2'] = val['email2'].lower()
		if 'email3' in val and val['email3']:
			val['email3'] = val['email3'].lower()
		goldenContact = GoldenContactAdv.GoldenContactAdv()
		flag_dict = goldenContact.createOrUpdate(self.env, val)
		rec = flag_dict['rec']
		return rec

	def create_internal_flag(self, val):
		goldenContact = GoldenContactAdv.GoldenContactAdv()
		flag_dict = goldenContact.createOrUpdate(self.env, val)
		return flag_dict

	def phase4Create(self, src_msg):
		if 'has_religious_txn' not in src_msg or not src_msg['has_religious_txn']:
			src_msg['has_charitable_txn'] = True
		self.set_whatsapp_number(src_msg)
		if 'contact_type' not in src_msg or not src_msg['contact_type']:
			src_msg['contact_type'] = 'INDIVIDUAL'
		return super(ResPartner, self).create(src_msg)

	def set_whatsapp_number(self, src_msg):
		if 'whatsapp_number' not in src_msg or not src_msg['whatsapp_number'] and 'phone' in src_msg and src_msg['phone']:
			src_msg['whatsapp_number'] = src_msg['phone']
			if 'phone_country_code' in src_msg and src_msg['phone_country_code']:
				src_msg['whatsapp_country_code'] = src_msg['phone_country_code']
			elif 'country_id' in src_msg and src_msg['country_id']:
				country = self.env['res.country'].search([('id', '=', src_msg['country_id'])])
				if country:
					src_msg['whatsapp_country_code'] = str(country.phone_code)
				else:
					src_msg['whatsapp_country_code'] = '91'
			else:
				src_msg['whatsapp_country_code'] = '91'

	def update_internal(self, vals):
		if 'id' in vals:
			temp = {}
			update_dict = {}
			goldenContact = GoldenContactAdv.GoldenContactAdv()
			contact = self.env['res.partner'].browse([vals['id']])

			temp['phone'] = vals['phone'] if 'phone' in vals else None
			temp['phone_country_code'] = vals['phone_country_code'] if 'phone_country_code' in vals else None
			temp['phone_is_valid'] = True
			temp['phone_is_verified'] = False
			temp['email'] = vals['email'] if 'email' in vals else None
			temp['nationality'] = vals['nationality'] if 'nationality' in vals else None

			# Phones
			phone_dict = goldenContact.getPhonesDict(contact, temp)
			# Emails
			email_dict = goldenContact.getEmailsDict(contact, temp)

			update_dict.update(phone_dict)
			update_dict.update(email_dict)
			if temp['nationality']:
				update_dict.update({'nationality': temp['nationality']})

			contact.update(update_dict)
			return True
		else:
			return False

	def getHighMatch(self, name, phone, email,lenient_search=False, exact_email_search=False):
		match_list = self.getMatchPartnerList(name, phone, email,lenient_search, exact_email_search)
		if match_list['is_high_match']:
			return match_list['matches']
		else:
			return []

	def getMatchPartnerList(self, name, phone, email,lenient_search=False, exact_email_search=False):
		goldenContact = GoldenContactAdv.GoldenContactAdv()
		return goldenContact.getMatchList(self.env, name, phone, email,lenient_search, exact_email_search)

	def apiSearchContact(self, name, phone, email,lenient_search=False):
		goldenContact = GoldenContactAdv.GoldenContactAdv()
		return goldenContact.getApiSearchList(self.env, name, phone, email,lenient_search)

	def apiSearchContactLOM(self, name, phone, email):
		goldenContact = ContactService.ContactService()
		# goldenContact = GoldenContactAdv.GoldenContactAdv()
		return goldenContact.getApiSearchList(self.sudo().env, name, phone, email)

	def center_filter_server_action(self):
		center_list = self.env.user.centers.ids
		tree_id = self.env.ref("isha_crm.view_partner_tree")
		form_id = self.env.ref("isha_crm.view_partner_form")
		pivot_id = self.env.ref("isha_crm.view_volunteer_pivot")
		graph_id = self.env.ref("isha_crm.view_volunteer_graph")
		return {
			'type': 'ir.actions.act_window',
			'name': 'Grid view on BSP Date (row) and IE Date (col)',
			'view_type': 'form',
			'view_mode': 'pivot,tree,form',
			'res_model': 'res.partner',
			'domain': [('center_id','in', center_list)],
			# if you don't want to specify form for example
			# (False, 'form') just pass False
			'views': [(pivot_id.id, 'pivot'),(tree_id.id, 'tree'), (form_id.id, 'form')],
			'target': 'current'
		}

	@api.model
	def update_contact_segments(self):
		_logger.info('Cron - Updating contact segments')
		query_update_adv_seekers = '''UPDATE public.res_partner rp
			SET segment='adv_seeker'
			FROM
				(select contact_id_fkey as partner_id from program_attendance pa, program_type_master ptm
					where pa.pgm_type_master_id = ptm.id and ptm.program_segment = 'adv_seeker'
					and pa.active=true and reg_status in ('COMPLETED','CONFIRMED')) pa
			WHERE pa.partner_id = rp.id and (segment is null or (segment != 'adv_seeker'));'''

		self.env.cr.execute(query_update_adv_seekers)
		self.env.cr.commit()

		query_update_seekers = '''update res_partner rp set segment='seeker'
			from
				(select contact_id_fkey as partner_id from program_attendance pa, program_type_master ptm
					where pa.pgm_type_master_id = ptm.id and ptm.program_segment = 'seeker'
					and pa.active=true and reg_status in ('COMPLETED','CONFIRMED')) pa
			where pa.partner_id = rp.id and (segment is null or (segment != 'adv_seeker' and segment != 'seeker'));'''

		self.env.cr.execute(query_update_seekers)
		self.env.cr.commit()

		query_update_seekers2 = '''update res_partner rp set segment='seeker'
			where (segment is null or (segment != 'adv_seeker' and segment != 'seeker'))
			and (is_meditator=true or ieo_date is not null);'''

		self.env.cr.execute(query_update_seekers2)
		self.env.cr.commit()

		query_update_follower_referrals = '''update res_partner rp set segment='follower_ref'
			from
				(select distinct(contact_id_fkey) as partner_id from ieo_record ieo
					where ieo.active=true and last_login_date is null and cust_type='FREE') ieo
			where ieo.partner_id = rp.id and rp.segment is null
				and ieo.partner_id not in ( select contact_id_fkey from ieo_record where last_login_date is not null );'''

		self.env.cr.execute(query_update_follower_referrals)
		self.env.cr.commit()

		query_remove_invalid_referrals = '''update res_partner rp set segment=null where id in (
			select rp.id from res_partner rp, contact_map cm
			where rp.id = cm.contact_id_fkey and rp.segment='follower_ref' group by rp.id having count(*) > 1);'''
		self.env.cr.execute(query_remove_invalid_referrals)
		self.env.cr.commit()

		query_set_followers = '''update res_partner rp set segment='follower' where segment is null;'''
		self.env.cr.execute(query_set_followers)
		self.env.cr.commit()

	@api.model
	def update_caca_categories(self):
		_logger.info('Cron - Updating CaCa categories')
		query_update_upasaka = '''update res_partner as rp set caca_category = 'upasaka' where id in
		(select contact_id_fkey from res_partner rp, donation_isha_outreach otr, isha_purpose_project_mapping ppmap
		 where otr.contact_id_fkey = rp.id and otr.purpose_id = ppmap.id and ppmap.project_name ilike 'Cauvery%' and
		  rp.region_name = 'Karnataka' group by contact_id_fkey having sum(amount) > 0 and sum(amount) < 420);'''
		self.env.cr.execute(query_update_upasaka)
		self.env.cr.commit()

		query_update_shoora = '''update res_partner as rp set caca_category = 'shoora' where id in
		(select contact_id_fkey from res_partner rp, donation_isha_outreach otr, isha_purpose_project_mapping ppmap
		 where otr.contact_id_fkey = rp.id and otr.purpose_id = ppmap.id and ppmap.project_name ilike 'Cauvery%' and
		  rp.region_name = 'Karnataka' group by contact_id_fkey having sum(amount) >= 420 and sum(amount) < 4200);'''
		self.env.cr.execute(query_update_shoora)
		self.env.cr.commit()

		query_update_mitr = '''update res_partner as rp set caca_category = 'mitr' where id in
		(select contact_id_fkey from res_partner rp, donation_isha_outreach otr, isha_purpose_project_mapping ppmap
		 where otr.contact_id_fkey = rp.id and otr.purpose_id = ppmap.id and ppmap.project_name ilike 'Cauvery%' and
		 		  rp.region_name = 'Karnataka' group by contact_id_fkey having sum(amount) >= 4200 and sum(amount) < 42000);'''

		self.env.cr.execute(query_update_mitr)
		self.env.cr.commit()

		query_update_veera = '''update res_partner as rp set caca_category = 'veera' where id in
		(select contact_id_fkey from res_partner rp, donation_isha_outreach otr, isha_purpose_project_mapping ppmap
		 where otr.contact_id_fkey = rp.id and otr.purpose_id = ppmap.id and ppmap.project_name ilike 'Cauvery%' and
		  rp.region_name = 'Karnataka' group by contact_id_fkey having sum(amount) >= 42000 and sum(amount) < 420000);'''
		self.env.cr.execute(query_update_veera)
		self.env.cr.commit()

		query_update_param_veera = '''update res_partner as rp set caca_category = 'param_veera' where id in
		(select contact_id_fkey from res_partner rp, donation_isha_outreach otr, isha_purpose_project_mapping ppmap
		 where otr.contact_id_fkey = rp.id and otr.purpose_id = ppmap.id and ppmap.project_name ilike 'Cauvery%' and
		  rp.region_name = 'Karnataka' group by contact_id_fkey having sum(amount) >= 420000 and sum(amount) < 4200000);'''
		self.env.cr.execute(query_update_param_veera)
		self.env.cr.commit()

		query_update_rakshak = '''update res_partner as rp set caca_category = 'rakshak' where id in
		(select contact_id_fkey from res_partner rp, donation_isha_outreach otr, isha_purpose_project_mapping ppmap
		 where otr.contact_id_fkey = rp.id and otr.purpose_id = ppmap.id and ppmap.project_name ilike 'Cauvery%' and
		  rp.region_name = 'Karnataka' group by contact_id_fkey having sum(amount) >= 4200000 and sum(amount) < 42000000);'''
		self.env.cr.execute(query_update_rakshak)
		self.env.cr.commit()

		query_update_yodha = '''update res_partner as rp set caca_category = 'yodha' where id in
		(select contact_id_fkey from res_partner rp, donation_isha_outreach otr, isha_purpose_project_mapping ppmap
		 where otr.contact_id_fkey = rp.id and otr.purpose_id = ppmap.id and ppmap.project_name ilike 'Cauvery%' and
		  rp.region_name = 'Karnataka' group by contact_id_fkey having sum(amount) >= 42000000 and sum(amount) < 420000000);'''
		self.env.cr.execute(query_update_yodha)
		self.env.cr.commit()

		query_update_nayak = '''update res_partner as rp set caca_category = 'nayak' where id in
		(select contact_id_fkey from res_partner rp, donation_isha_outreach otr, isha_purpose_project_mapping ppmap
		 where otr.contact_id_fkey = rp.id and otr.purpose_id = ppmap.id and ppmap.project_name ilike 'Cauvery%' and
		  rp.region_name = 'Karnataka' group by contact_id_fkey having sum(amount) >= 420000000);'''
		self.env.cr.execute(query_update_nayak)
		self.env.cr.commit()

	@api.model
	def recompute_donation_all(self):
		_logger.info('Cron - Computing Total Donations')
		sql_query = '''UPDATE public.res_partner cv
			SET total_donation = sub_q.total_amount
			FROM
				(
					select contact_id_fkey,sum(amount) as total_amount from donations_view dv
						where contact_id_fkey in (
							SELECT distinct o.contact_id_fkey
							FROM public.donations_view AS o
							where active=1 and date(o.create_date) = date(NOW())) and active=1 group by contact_id_fkey
				) AS sub_q
			WHERE sub_q.contact_id_fkey = cv.id;'''
		self.env.cr.execute(sql_query)
		self.env.cr.commit()

		ishanga_cat_id = self.env.ref('isha_crm.res_partner_category_20').id
		query_ishanga_tags = '''insert into res_partner_res_partner_category_rel (category_id,partner_id)
			(select ''' + str(ishanga_cat_id) + ''',contact_id_fkey from ishanga_donation_registration dv where
			dv.is_ishaga_active = true and dv.contact_id_fkey is not null and dv.write_date > now() - interval '2 days')
			 ON CONFLICT (category_id, partner_id) DO NOTHING'''
		self.env.cr.execute(query_ishanga_tags)
		self.env.cr.commit()

		_logger.info('Cron - Computing Total Donations Complete')

	@api.model
	def delete_freeyoga_tags(self):
		_logger.info('Cron - Deleting Free Yoga Tags')
		free_yoga_tag_id = self.env.ref('isha_crm.res_partner_pgm_tag_24').id
		sql_query = '''delete from isha_program_tag_res_partner_rel rel using program_lead pl, res_partner rp
						where pl.contact_id_fkey = rp.id and rp.id = rel.partner_id and
						pl.program_name = 'Surya Shakthi Yogaveera Training Registrations' and rp.is_meditator = true
						and rel.pgm_tag_ids = ''' + str(free_yoga_tag_id) + ''';'''
		self.env.cr.execute(sql_query)
		self.env.cr.commit()
		_logger.info('Cron - Deleting Free Yoga Tags Complete')

	@api.model
	def compute_samarthak(self):
		_logger.info('Cron - Computing Samarthak')
		samarthak_tag = self.env.ref('isha_crm.donation_tag_samarthak').id
		model_mapper = {
			'IshaFoundation': 'donation_isha_foundation',
			'IshaOutreach': 'donation_isha_outreach',
			'IshaEducation': 'donation_isha_education',
			'IIIS': 'donation_isha_iii',
			'IshaArogya': 'donation_isha_arogya'
		}

		sql_query = '''
				select id,dv.contact_id_fkey,amount,don_receipt_date,dv.entity_name,dv.is_samarthak,sq.max_create_date,sq.max_date from donations_view dv join
				(select contact_id_fkey,max(don_receipt_date) as max_date,max(create_date) as max_create_date from donations_view where
						active='1' and
						contact_id_fkey in (
							select contact_id_fkey from donations_view where date(create_date)=date(now()) and contact_id_fkey is not null
						)
						and purpose_id not in (
							select id from isha_purpose_project_mapping where project_name in ('Placeholder','Cottage','Feb Program Fees','February Program','Fees','Program Fee','Rejuvenation','Rent','Samskriti','Subscription','Thaimai Class')
						) and project not ilike '% fee'
					group by contact_id_fkey having sum(amount) > 200000
				) sq on dv.contact_id_fkey = sq.contact_id_fkey
				and dv.active='1' and
				 dv.purpose_id not in (
					select id from isha_purpose_project_mapping where project_name in ('Placeholder','Cottage','Feb Program Fees','February Program','Fees','Program Fee','Rejuvenation','Rent','Samskriti','Subscription','Thaimai Class')
				) and dv.project not ilike '% fee'
				and dv.don_receipt_date between max_date - interval '1 year' and max_date
				order by dv.contact_id_fkey,dv.don_receipt_date,dv.create_date;
		'''
		self.env.cr.execute(sql_query)
		to_update_dict = []
		contact_to_donations = dict()
		for row in self.env.cr.dictfetchall():
			if row['contact_id_fkey'] in contact_to_donations:
				contact_to_donations[row['contact_id_fkey']].append(row)
			else:
				contact_to_donations[row['contact_id_fkey']] = [row]
		for contact in contact_to_donations:
			total_donation = 0
			for txn in contact_to_donations[contact]:
				total_donation += float(txn['amount'])
				if total_donation >= 200000.00 and not txn['is_samarthak']:
					txn['is_samarthak'] = True
					to_update_dict.append(txn)
		for txn in to_update_dict:
			update_query = 'UPDATE %s SET is_samarthak=true WHERE id = %s'%(model_mapper[txn['entity_name']],txn['id'])
			self.env.cr.execute(update_query)
			update_query = "UPDATE res_partner SET last_samarthak_date = '%s' WHERE id = %s" % (str(txn['max_date']), txn['contact_id_fkey'])
			self.env.cr.execute(update_query)
			update_query = "INSERT INTO isha_donation_tag_res_partner_rel (partner_id,donation_tag_id) VALUES (%s,%s) ON CONFLICT DO NOTHING" %\
						   (txn['contact_id_fkey'],samarthak_tag)
			self.env.cr.execute(update_query)
			self.env.cr.commit()
		_logger.info('Cron - Computing Samarthak Complete')

	def compute_sanghamitra_donation(self):
		_logger.info('Cron - Computing Sanghmitra')
		sql_query = '''
				update res_partner rp set last_sanghamitra_date=sq.max_date from
			(select contact_id_fkey,max(don_receipt_date) as max_date,max(create_date) as max_create_date from donations_view where
					active='1' and
					date(create_date)=date(now()) and
					contact_id_fkey is not null
					and purpose_id in (
						select id from isha_purpose_project_mapping where purpose in ('Isha Sanghamitra Corpus Fund - Recurring', 'Isha Sanghamitra Corpus Fund FCRA- Recurring')
					)
				group by contact_id_fkey
			) sq where
			rp.id = sq.contact_id_fkey
		'''
		self.env.cr.execute(sql_query)
		self.env.cr.commit()

		sql_query = '''
		insert into isha_donation_tag_res_partner_rel (partner_id,donation_tag_id)
			(select contact_id_fkey,(select id from isha_donation_tag where name = 'Sanghamitra Recurring') as tag  from donations_view where
				active='1' and
				date(create_date)=date(now()) and
				contact_id_fkey is not null
				and purpose_id in (
					select id from isha_purpose_project_mapping where purpose in ('Isha Sanghamitra Corpus Fund - Recurring', 'Isha Sanghamitra Corpus Fund FCRA- Recurring')
				)
			group by contact_id_fkey)
		ON CONFLICT DO NOTHING
		'''
		self.env.cr.execute(sql_query)
		self.env.cr.commit()
		_logger.info('Cron - Computing Sanghmitra Complete')

	def _compute_total_donation(self):
		user_group_flag = self.env.user.has_group('isha_crm.group_sangam')
		for rec in self:
			flag = False
			rec.finance_filter = False
			if user_group_flag:
				func_call = 'select finance_filter( '+ str(rec.id)+' )'
				self.env.cr.execute(func_call)
				for result in self.env.cr.dictfetchall():
					flag = result['finance_filter']
					rec.finance_filter = flag

			if flag:
				donation_recs = self.env['donation.isha.foundation'].browse(rec.contact_donations_foundation_id_fkey.ids)
				don = 0
				for x in donation_recs:
					don += x.amount
				rec.total_foundation_amount = don
				donation_recs = self.env['donation.isha.arogya'].browse(rec.contact_donations_arogya_id_fkey.ids)
				don = 0
				for x in donation_recs:
					don += x.amount
				rec.total_arogya_amount = don
				donation_recs = self.env['donation.isha.education'].browse(rec.contact_donations_education_id_fkey.ids)
				don = 0
				for x in donation_recs:
					don += x.amount
				rec.total_education_amount = don
				donation_recs = self.env['donation.isha.iii'].browse(rec.contact_donations_iii_id_fkey.ids)
				don = 0
				for x in donation_recs:
					don += x.amount
				rec.total_iii_amount = don
				donation_recs = self.env['donation.isha.outreach'].browse(rec.contact_donations_outreach_id_fkey.ids)
				don = 0
				for x in donation_recs:
					don += x.amount
				rec.total_outreach_amount = don
			else:
				rec.total_foundation_amount = 0
				rec.total_arogya_amount = 0
				rec.total_iii_amount = 0
				rec.total_education_amount = 0
				rec.total_outreach_amount = 0

	def flag_contact(self):
		if self.influencer_type:
			self.write({'influencer_type': None})
		else:
			self.write({'influencer_type': 'influencer'})

	def execute_query(self, sql_query):
		_logger.info('Cron - Execute SQL Query')
		self.env.cr.execute(sql_query)
		self.env.cr.commit()

	# @api.model
	# def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
	# 	result = super(ResPartner, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
	# 	if 'toolbar' in result:
	# 		doc = result['toolbar']['action']
	# 		if self._context.get('active_model') == 'call_campaign.call_campaign':
	# 			for x in doc:
	# 				if x['name'] == 'Add Volunteers' or x['name'] == 'Add People to call':
	# 					x['context'] = self.env.context
	# 					# ctx_campaign = self._context.get('campaign')
	# 					# x['context'] = "{'campaign': ctx_campaign}"
	#
	# 	return result
	def start_kafka(self):
		kafka_starter.kafkaLoader()

	def monitor_kafka(self):
		kafka_starter.monitor_kafka(self.env)

	def refresh_iv_donor_status(self):
		active_donors = self.search([('iv_donor_status', '=', 'active'),
									 ('iv_last_donated_date', '<', (datetime.datetime.now() - relativedelta(months=15)))])
		if active_donors:
			active_donors.write({'iv_donor_status': 'inactive'})

	def monitor_kafka_errors(self):
		kafka_starter.monitor_kafka_errors(self.env)

	def monitor_consumer_lag(self):
		kafka_starter.monitor_consumer_lag(self.env)

	def monthly_report_influencers(self):
		total_influencers = self.env['res.partner'].search_count([('influencer_type', '!=', False)])
		last_month = len(self.env['res.partner'].search([('influencer_type', '!=', False)]).filtered(lambda r: r.write_date >= datetime.datetime.now()-timedelta(days=30)))
		lmp_count = self.env['low.match.pairs'].search_count(
			['|', ('id1_influencer_type', '!=', False), ('id2_influencer_type', '!=', False)])/2
		body = '''
				Total Influencers = %d<br/>
				Influencers Last Month = %d <br/>
				Potential matches for Influencers = %d
				''' % (total_influencers,last_month,lmp_count)
		template_browse = self.env.ref('isha_crm.influencers_report_mail_template')
		if template_browse:
			values = template_browse.generate_email(3, fields=None)
			values['body_html'] = values['body_html'].replace('$$',body)
			mail_mail_obj = self.env['mail.mail']
			msg_id = mail_mail_obj.sudo().create(values)
			msg_id.send(False)
			return True

	def bulk_assign_eligible_subs(self):
		view = self.env.ref('isha_crm.bulk_eligible_subs_act_window_wizard_form')
		view_id = view and view.id or False
		context = dict(self._context or {},
						region_scope=True,
						restrict_fields='ml_subscription_ids',
						restrict_values=[('child_ids','=',False)])
		return {
			'name': 'Bulk Modify Eligible Subscriptions',
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'm2m.tag.manipulation',
			'views': [(view_id, 'form')],
			'view_id': view_id,
			'target': 'new',
			'context': context
		}

class LowMatchPairs(models.Model):
	_name = "low.match.pairs"

	id1 = fields.Many2one(string="Contact ID1", comodel_name='res.partner', required=True, auto_join=True, index=True)
	guid1 = fields.Char(string="GUID1")
	id2 = fields.Many2one(string="Contact ID2", comodel_name='res.partner', required=True, auto_join=True, index=True)
	guid2 = fields.Char(string="GUID2")
	active = fields.Boolean(string='active', default=True)
	is_high_match = fields.Boolean(string='High Match')
	match_level = fields.Selection([('high','High'),('low','Low'),('relatives','Relatives'), ('high_sso', 'High match but SSOs different')],string='Match Level')

	name = fields.Char(string='Name', related='id1.name',store=True)
	category_id = fields.Many2many(related='id1.category_id')
	pgm_tag_ids = fields.Many2many(related='id1.pgm_tag_ids')
	phone = fields.Char(string='Phone', related='id1.phone')
	phone2 = fields.Char(string='Alt Phone 2', related='id1.phone2')
	phone3 = fields.Char(string='Alt Phone 3', related='id1.phone3')
	phone4 = fields.Char(string='Alt Phone 4', related='id1.phone4')
	email = fields.Char(string='Email', related='id1.email')
	email2 = fields.Char(string='Alt Email 2', related='id1.email2')
	email3 = fields.Char(string='Alt Email 3', related='id1.email3')
	street = fields.Char(string="Street", related='id1.street')
	street2 = fields.Char(string="Street2", related='id1.street2')
	city = fields.Char(string='City Of Alt Address', related='id1.city')
	state = fields.Char(string='State', related='id1.state')
	country = fields.Char(string='Country ISO2', related='id1.country')
	zip = fields.Char(string="Zip", related='id1.zip')
	street2_1 = fields.Char(string='Street1 Of Alt Address', related='id1.street2_1')
	street2_2 = fields.Char(string='Street2 Of Alt Address', related='id1.street2_2')
	city2 = fields.Char(string='City Of Alt Address', related='id1.city2')
	state2 = fields.Char(string='State Of Alt Address', related='id1.state2')
	country2 = fields.Char(string='Country Of Alt Address', related='id1.country2')
	zip2 = fields.Char(string='Pincode Of Alt Address', related='id1.zip2')

	id2_name = fields.Char(string='Name', related='id2.name',store=True)
	id2_category_id = fields.Many2many(related='id2.category_id')
	id2_pgm_tag_ids = fields.Many2many(related='id2.pgm_tag_ids')
	id2_phone = fields.Char(string='Phone', related='id2.phone')
	id2_phone2 = fields.Char(string='Alt Phone 2', related='id2.phone2')
	id2_phone3 = fields.Char(string='Alt Phone 3', related='id2.phone3')
	id2_phone4 = fields.Char(string='Alt Phone 4', related='id2.phone4')
	id2_email = fields.Char(string='Email', related='id2.email')
	id2_email2 = fields.Char(string='Alt Email 2', related='id2.email2')
	id2_email3 = fields.Char(string='Alt Email 3', related='id2.email3')
	id2_street = fields.Char(string="Street", related='id2.street')
	id2_street2 = fields.Char(string="Street2", related='id2.street2')
	id2_city = fields.Char(string='City Of Alt Address', related='id2.city')
	id2_state = fields.Char(string='State', related='id2.state')
	id2_country = fields.Char(string='Country ISO2', related='id2.country')
	id2_zip = fields.Char(string="Zip", related='id2.zip')
	id2_street2_1 = fields.Char(string='Street1 Of Alt Address', related='id2.street2_1')
	id2_street2_2 = fields.Char(string='Street2 Of Alt Address', related='id2.street2_2')
	id2_city2 = fields.Char(string='City Of Alt Address', related='id2.city2')
	id2_state2 = fields.Char(string='State Of Alt Address', related='id2.state2')
	id2_country2 = fields.Char(string='Country Of Alt Address', related='id2.country2')
	id2_zip2 = fields.Char(string='Pincode Of Alt Address', related='id2.zip2')

	id1_center_id = fields.Many2one('isha.center', compute='_compute_center_id', store=True, index=True)
	id2_center_id = fields.Many2one('isha.center', compute='_compute_center_id', store=True, index=True)

	id1_influencer_type = fields.Boolean(string='id1 Influencer Type', compute='_compute_influencer', store=True)
	id2_influencer_type = fields.Boolean(string='id2 Influencer Type', compute='_compute_influencer', store=True)

	def flag_id1(self):
		self.id1.flag_contact()
	def flag_id2(self):
		self.id2.flag_contact()

	def merge_lmp(self):
		return {
			'type':'ir.actions.act_window',
			'binding_model':"low.match.pairs",
			'res_model' : "base.partner.merge.automatic.wizard",
			'view_mode' : "form",
			'target': "new"
		}

	def remove_lmp(self):
		self.unlink()
		return

	def mark_relations(self):
		local_context = dict(
			self.env.context,
			default_left_partner_id=self.id1.id,
			default_right_partner_id=self.id2.id
		)
		return {
			'type': 'ir.actions.act_window',
			'view_mode': 'form',
			'res_model': 'res.partner.relation',
			'target': 'new',
			'context': local_context
		}
	@api.depends('id1.influencer_type','id2.influencer_type')
	def _compute_influencer(self):
		for rec in self:
			rec.id1_influencer_type = True if rec.id1.influencer_type else False
			rec.id2_influencer_type = True if rec.id2.influencer_type else False

	@api.depends('id1.center_id','id2.center_id')
	def _compute_center_id(self):
		for rec in self:
			rec.id1_center_id = rec.id1.center_id
			rec.id2_center_id = rec.id2.center_id

	@api.model
	def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
		context = self._context or {}
		if context.get('center_scope'):
			domain += ['|',('id1_center_id','in', self.env.user.centers.ids),('id2_center_id','in', self.env.user.centers.ids)]

		return super(LowMatchPairs, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
													  orderby=orderby, lazy=lazy)

	@api.model
	def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
		context = self._context or {}
		if context.get('center_scope'):
			args += ['|',('id1_center_id','in', self.env.user.centers.ids),('id2_center_id','in', self.env.user.centers.ids)]

		return super(LowMatchPairs, self)._search(args, offset, limit, order, count=count, access_rights_uid=access_rights_uid)

	@api.model_create_multi
	def create(self, vals_list):
		custom_val = []
		for val in vals_list:
			custom_val.append(val)
			val_rev = deepcopy(val)
			val_rev.update({'id1': val['id2'], 'guid1': val['guid2'], 'id2': val['id1'], 'guid2': val['guid1']})
			custom_val.append(val_rev)

		total_recs = super(LowMatchPairs, self).create(custom_val)
		influencer_recs = total_recs.filtered(lambda r: r.id1.influencer_type and r.match_level == 'low').sorted(
			key=lambda p: p.id
		)
		if len(influencer_recs)!=0:
			for x in influencer_recs:
				self.send_mail_from_template(x.id)
		return total_recs

	def send_email_notif(self,subject, body, recs):
		su_id = self.env['res.partner'].browse(SUPERUSER_ID)
		# post the message
		mail_values = {
			'email_from': su_id.email,
			'model': None,
			'res_id': None,
			'subject': subject,
			'body_html': body,
			'attachment_ids': [],
			'auto_delete': True,
		}
		mail_values['recipient_ids'] = recs
		mail = self.env['mail.mail'].sudo().create(mail_values)
		return mail.send(False)

	def send_mail_from_template(self, lmp_rec):
		template_browse = self.env.ref('isha_crm.influencers_mail_template')
		if template_browse:
			values = template_browse.generate_email(lmp_rec, fields=None)
			mail_mail_obj = self.env['mail.mail']
			msg_id = mail_mail_obj.sudo().create(values)
			msg_id.send(False)
			return True

	def unlink(self):
		reverseVal = []
		for val in self:
			val_list = self.env['low.match.pairs'].search([('id1', '=', val.id2.id), ('id2', '=', val.id1.id)])
			reverseVal.append(val_list[0].id)
		models.Model.unlink(self.env['low.match.pairs'].browse(reverseVal))
		return models.Model.unlink(self)

	def autoMergeHighMatches(self):
		DB_NAME = config['db_name']
		registry = modules.registry.Registry(DB_NAME)
		merge_engine_thread = threading.Thread(target=self.merge_engine_handler, args=(_logger, registry))
		merge_engine_thread.start()
		_logger.info("Started the thread for Auto merge High matches......")

	def merge_engine_handler(self, logger, registry):
		UID = SUPERUSER_ID
		cr = registry.cursor()
		try:
			with api.Environment.manage():
				env = api.Environment(cr, UID, {})
				while True:
					rec = env['low.match.pairs'].search(['|',('is_high_match', '=', True),('match_level','=','high')], limit=1)
					if len(rec)==0:
						break
					env['low.match.pairs'].merge_engine(rec)
					cr.commit()
				cr.commit()
				cr.close()
		except Exception as ex:
			tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
			logger.info(tb_ex)
		finally:
			logger.info("Auto Merge High Match Completed")
			if cr and not cr.closed:
				cr.close()

	def merge_engine(self, rec):
		merge_model = self.env['base.partner.merge.automatic.wizard']
		if rec.id1.write_date > rec.id2.write_date:
			src_ptn_list = [rec.id2.id, rec.id1.id]
			dst_ptn = rec.id1
		else:
			src_ptn_list = [rec.id1.id, rec.id2.id]
			dst_ptn = rec.id2
		merge_model._merge(src_ptn_list,dst_ptn,False)

	def merge_engine_odoorpc(self):
		merge_model = self.env['base.partner.merge.automatic.wizard'].with_context(skip_sso_check=True)
		name_match_engine = NameMatchEngine.NameMatchEngine()
		match_level = name_match_engine.compareNamesWithVariations(self.id1.name,self.id2.name)
		if match_level not in [NameMatchEngine.HIGH_MATCH,NameMatchEngine.EXACT_MATCH]:
			raise UserError('ERROR: The lmp entry is not a high Match')
		if self.id1.sso_id and not self.id2.sso_id:
			src_ptn_list = [self.id2.id, self.id1.id]
			dst_ptn = self.id1
			final_write_date = self.id1.write_date.strftime('%Y-%m-%d %H:%M:%S')
			winner_rec = self.id1.id
		elif self.id2.sso_id and not self.id1.sso_id:
			src_ptn_list = [self.id1.id, self.id2.id]
			dst_ptn = self.id2
			final_write_date = self.id2.write_date.strftime('%Y-%m-%d %H:%M:%S')
			winner_rec = self.id2.id
		elif self.id1.write_date > self.id2.write_date:
			src_ptn_list = [self.id2.id, self.id1.id]
			dst_ptn = self.id1
			final_write_date = self.id1.write_date.strftime('%Y-%m-%d %H:%M:%S')
			winner_rec = self.id1.id
		else:
			src_ptn_list = [self.id1.id, self.id2.id]
			dst_ptn = self.id2
			final_write_date = self.id2.write_date.strftime('%Y-%m-%d %H:%M:%S')
			winner_rec = self.id2.id

		merge_model._merge(src_ptn_list,dst_ptn,False)
		sql = "update res_partner set write_date = '%s' where id = %d" % (final_write_date,winner_rec)
		self.env.cr.execute(sql)

class ContactMap(models.Model):
	_name = 'contact.map'
	_sql_constraints = [('unique_contact_id', 'unique(local_contact_id, system_id)',
						 'Cannot Have Duplicate Local Contact ID From Same Source System')]
	_order = "write_date desc"

	local_contact_id = fields.Char(string="Local Contact Id", index=True)
	system_id = fields.Integer(string="Source System Id")
	contact_id_fkey = fields.Many2one(string="Contact ID", comodel_name='res.partner', index=True)
	prof_dr = fields.Char(string='Title', required=False)
	suffix = fields.Selection([('ji', 'Ji'), ('navare', 'Navare'), ('garu', 'Garu')], string="Suffix")
	sso_id = fields.Char(string="SSO Id", required=False, index=True)
	guid = fields.Char(string="Golden Id", index=True, store=True, related='contact_id_fkey.guid')
	name = fields.Char(string="Name", required=False)
	title = fields.Char(string="title", required=False)
	phone = fields.Char(string="Phone", required=False)
	phone2 = fields.Char(string='Phone2', required=False)
	whatsapp_number = fields.Char(string='WhatsApp Number', required=False)
	whatsapp_country_code = fields.Char(string='WhatsApp Country Code', required=False)
	email = fields.Char(string='Email', required=False)
	email2 = fields.Char(string='Alt Email', required=False)
	street1 = fields.Char(string='Street1', required=False)
	street2 = fields.Char(string='Street2', required=False)
	city = fields.Char(string='City', required=False)
	state = fields.Char(string='State', required=False)
	country = fields.Char(string='Country', required=False)
	zip = fields.Char(string='Pincode', required=False)
	work_street1 = fields.Char(string='Street1 Of Work Address', required=False)
	work_street2 = fields.Char(string='Street2 Of Work Address', required=False)
	work_city = fields.Char(string='City Of Work Address', required=False)
	work_state = fields.Char(string='State Of Work Address', required=False)
	work_country = fields.Char(string='Country Of Work Address', required=False)
	work_zip = fields.Char(string='Pincode Of Work Address', required=False)
	gender = fields.Selection([('M', 'Male'), ('F', 'Female'), ('O', 'Others')], string='Gender', required=False)
	occupation = fields.Char(string='Occupation', required=False)
	dob = fields.Date(string='Date Of Birth', required=False)
	marital_status = fields.Char(string='Marital Status', required=False)
	nationality = fields.Char(string='Nationality', required=False)
	deceased = fields.Boolean(string='Deceased', required=False)
	contact_type = fields.Char(string='Contact Type', required=False)
	companies = fields.Char(string="Company", required=False)
	aadhaar_no = fields.Char(string="Aadhaar", required=False)
	pan_no = fields.Char(string="PAN", required=False)
	passport_no = fields.Char(string="Passport", required=False)
	dnd_phone = fields.Boolean(string='DND Phone Flag', required=False)
	dnd_email = fields.Boolean(string='DND Email Flag', required=False)
	dnd_postmail = fields.Boolean(string='DND Post-mail Flag', required=False)
	active = fields.Boolean(string='active', default=True)
	id_proofs = fields.Char(string='ID Proofs', required=False)
	local_modified_date = fields.Datetime(string='Local Modified Date')

	pgm_att_ids = fields.Many2many('program.attendance', string='Programs', store=False, compute='_set_pgm_att_ids')
	foundation_don = fields.Many2many('donation.isha.foundation', string='Foundation Donation', store=False,
									  compute='_set_pgm_att_ids')
	outreach_don = fields.Many2many('donation.isha.outreach', string='Outreach Donation', store=False,
									compute='_set_pgm_att_ids')
	iii_don = fields.Many2many('donation.isha.iii', string='III Donation', store=False, compute='_set_pgm_att_ids')
	education_don = fields.Many2many('donation.isha.education', string='Education Donation', store=False,
									 compute='_set_pgm_att_ids')
	arogya_don = fields.Many2many('donation.isha.arogya', string='Arogya Donation', store=False,
								  compute='_set_pgm_att_ids')

	@api.depends('local_contact_id', 'system_id')
	def _set_pgm_att_ids(self):
		for rec in self:
			rec['pgm_att_ids'] = [x['id'] for x in self.env['program.attendance'].search(
				[('local_contact_id', '=', rec['local_contact_id']), ('system_id', '=', rec['system_id'])])]
			rec['foundation_don'] = [x['id'] for x in self.env['donation.isha.foundation'].search(
				[('local_patron_id', '=', rec['local_contact_id']), ('system_id', '=', rec['system_id'])])]
			rec['outreach_don'] = [x['id'] for x in self.env['donation.isha.outreach'].search(
				[('local_patron_id', '=', rec['local_contact_id']), ('system_id', '=', rec['system_id'])])]
			rec['iii_don'] = [x['id'] for x in self.env['donation.isha.iii'].search(
				[('local_patron_id', '=', rec['local_contact_id']), ('system_id', '=', rec['system_id'])])]
			rec['education_don'] = [x['id'] for x in self.env['donation.isha.education'].search(
				[('local_patron_id', '=', rec['local_contact_id']), ('system_id', '=', rec['system_id'])])]
			rec['arogya_don'] = [x['id'] for x in self.env['donation.isha.arogya'].search(
				[('local_patron_id', '=', rec['local_contact_id']), ('system_id', '=', rec['system_id'])])]


class Nodes(models.Model):
	_name = 'isha.contact.nodes'
	_description = 'Node Volunteer assigned for each contact'

	node = fields.Char(string="Node", required=True)
	email = fields.Char(string='Email', required=True, index=True)
	contactid_node_fkey = fields.Many2one(string="Contact Id", comodel_name='res.partner', required=True, index=True)
	active = fields.Boolean(string='active', default=True)


class ResPartnerImportTemp(models.Model):
	_name = 'partner.import.temp'
	_description = 'Temp table to import Res Partner'

	name = fields.Char(string='Name')
	phone = fields.Char(string='Phone')
	phone_cc = fields.Char(string='Phone Country Code')
	email = fields.Char(string='Email')
	mode = fields.Selection([('whatsapp_tagging','WhatsApp Tagging'),('phone_match','Phone Match'),('email_match','Email Match')],string='Upload Mode')
	match_count = fields.Integer(string='Match count')
	no_match_n_merge = fields.Boolean(sting='No Match & Merge', default=False)
	match_level = fields.Selection([('exact','Exact'),('high','High'),('low','Low'),('no','No'),('res_access','Restricted Access')], string='Match Level')
	partner_id = fields.Many2one('res.partner', string='Matched Contact',index=True)
	center_id = fields.Many2one(related='partner_id.center_id', store=True)
	region_name = fields.Char(related='partner_id.region_name', store=True)
	is_meditator = fields.Boolean(related='partner_id.is_meditator',store=True)
	influencer_type = fields.Selection(string="Influencer Type",
									   related='partner_id.influencer_type', store=True)
	is_starmarked = fields.Selection([('yes', 'Yes'), ('no', 'No')], string="Is Starmarked ?")
	def phone_mach(self, vals):
		result = []
		for val in vals:
			_logger.info('Bulk import partner direct phone match')

			self.env.cr.execute(
				'select id from res_partner where ( phone = \'' + val['phone'] + '\' or phone2 = \'' + val['phone'] + '\' or phone3 = \''
				+ val['phone'] + '\' or phone4 = \'' + val['phone'] + '\' ) and active = true')
			recs = self.env.cr.dictfetchall()
			if len(recs)>0:
				for rec in recs:
					val.update({'partner_id':rec['id'],'match_level':'high','match_count':len(recs)})
					result.append(val)
				continue

			val.update({'match_level': 'no'})
			result.append(val)
		return result

	def email_match(self, vals):
		result = []
		for val in vals:
			_logger.info('Bulk import partner direct email match')

			self.env.cr.execute(
				'select id from res_partner where ( email = \'' + val['email'] + '\' or email2 = \'' + val['email'] + '\' or email3 = \''
				+ val['email'] + '\' ) and active = true')
			recs = self.env.cr.dictfetchall()
			if len(recs)>0:
				for rec in recs:
					val.update({'partner_id':rec['id'],'match_level':'high','match_count':len(recs)})
					result.append(val)
				continue

			val.update({'match_level': 'no'})
			result.append(val)
		return result

	def whatsapp_tagging(self, vals):
		result = []
		for val in vals:
			_logger.info('Bulk import partner whatsapp tagging')
			try:
				self.env.cr.execute('select id,whatsapp_country_code from res_partner where whatsapp_number = \''+val['phone']+'\' and active = true')
			except Exception as ex:
				tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
				print(tb_ex)
			recs = self.env.cr.dictfetchall()
			if len(recs)>0:
				match_count = len(recs)
				for rec in recs:
					if rec['whatsapp_country_code'] == val['phone_cc']:
						val.update({'partner_id':rec['id'],'match_level':'exact','match_count':match_count})
					else:
						val.update({'partner_id': rec['id'], 'match_level': 'high','match_count':match_count})
					result.append(val)
				continue
			self.env.cr.execute('select id from res_partner where phone = \''+val['phone']+'\' and active = true')
			recs = self.env.cr.dictfetchall()
			if len(recs)>0:
				match_count = len(recs)
				for rec in recs:
					val.update({'partner_id':rec['id'],'match_level':'high','match_count':match_count})
					result.append(val)
				continue
			self.env.cr.execute('select id from res_partner where phone2 = \''+val['phone']+'\' and active = true')
			recs = self.env.cr.dictfetchall()
			if len(recs)>0:
				match_count = len(recs)
				for rec in recs:
					val.update({'partner_id':rec['id'],'match_level':'high','match_count':match_count})
					result.append(val)
				continue
			self.env.cr.execute('select id from res_partner where phone3 = \''+val['phone']+'\' and active = true')
			recs = self.env.cr.dictfetchall()
			if len(recs)>0:
				match_count = len(recs)
				for rec in recs:
					val.update({'partner_id':rec['id'],'match_level':'high','match_count':match_count})
					result.append(val)
				continue
			self.env.cr.execute('select id from res_partner where phone4 = \''+val['phone']+'\' and active = true')
			recs = self.env.cr.dictfetchall()
			if len(recs)>0:
				match_count = len(recs)
				for rec in recs:
					val.update({'partner_id':rec['id'],'match_level':'high','match_count':match_count})
					result.append(val)
				continue

			val.update({'match_level':'no'})
			result.append(val)

		return result

	def unlink_all_recs(self):
		recs = self.search(self._context.get('active_domain'))
		no_of_records = len(recs)
		recs.unlink()
		return self.env["sh.message.wizard"].get_popup('successfully deleted '+str(no_of_records)+' records')
	def getRecentPartner(self, rec_list):
		recent_partner = rec_list[0]
		for i in range(len(rec_list)-1):
			if rec_list[i+1].write_date > recent_partner.write_date:
				recent_partner = rec_list[i+1]
		return recent_partner

	# suri: check as per the user's privileges
	#
	def checkRestrictedAccess(self, contact):
		if contact.influencer_type or (contact.is_caca_donor is True and str(contact.last_txn_date) == '1900-01-01'):
			return True
		else:
			return False

	@api.model_create_multi
	def create(self, vals_list):
		for val in vals_list:
			if 'no_match_n_merge' not in val or not val['no_match_n_merge']:
				if 'mode' in val:
					# FIXME: different modes in the single batch of create
					if val['mode'] == 'whatsapp_tagging':
						vals_list = self.whatsapp_tagging(vals_list)
						break
					elif val['mode'] == 'phone_match':
						vals_list = self.phone_mach(vals_list)
						break
					elif val['mode'] == 'email_match':
						vals_list = self.email_match(vals_list)
						break
				else:
					if 'name' not in val or not val['name']:
						raise UserError('Name is Mandatory field')
					match_list = self.env['res.partner'].getMatchPartnerList(val['name'],val['phone'],val['email'],False)
					match_count = len(match_list['matches'])
					val['match_count'] = match_count
					if match_list['is_high_match']:
						if len(match_list['matches']) == 1:
							val['match_level'] = 'exact' if not self.checkRestrictedAccess(match_list['matches'][0]) else 'res_access'
							val['partner_id'] = match_list['matches'][0].id
							val['is_starmarked'] = None if self._context.get('is_from_ims') is False else 'yes' if match_list['matches'][0].starmark_category_id else 'no'
						elif len(match_list['matches']) > 1:
							contact = self.getRecentPartner(match_list['matches'])
							val['match_level'] = 'high' if not self.checkRestrictedAccess(contact) else 'res_access'
							val['partner_id'] = contact.id
							self.get_starmark_info(match_list, val)
					else:
						if len(match_list['matches']) >= 1:
							contact = self.getRecentPartner(match_list['matches'])
							val['match_level'] = 'low' if not self.checkRestrictedAccess(contact) else 'res_access'
							val['partner_id'] = contact.id
							self.get_starmark_info(match_list, val)
						elif len(match_list['matches']) == 0:
							val['match_level'] = 'no'
		recs = super(ResPartnerImportTemp, self).create(vals_list)
		#suri: check usage
		#
		recs.sudo().filtered(lambda x: x.sudo().partner_id.influencer_type
						 or (x.sudo().partner_id.is_caca_donor is True and str(x.sudo().partner_id.last_txn_date) == '1900-01-01')
						 	).write({'match_level':'res_access'})
		return recs

	def get_starmark_info(self, match_list, val):
		if self._context.get('is_from_ims') is False:
			val['is_starmarked'] = None
		else:
			val['is_starmarked'] = 'no'
			for rec in match_list['matches']:
				if rec.starmark_category_id:
					val['is_starmarked'] = 'yes'
		return

class ResPartnerImportTempWizard(models.TransientModel):
	""" wizard to perform action on imported partner """

	_name = 'partner.import.temp.wizard'
	_description = 'wizard to perform action on imported partner'

	@api.model
	def _get_bulk_actions(self):
		choises = []
		if self.env.user.has_group('isha_crm.group_flag_contact'):
			choises += [('flag_influencer','Flag Influencer')]
		if self.env.user.has_group('isha_crm.group_athithi_central_admin') or self.env.user.has_group(
            'isha_crm.group_athithi_central_viewer') or self.env.user.has_group(
            'isha_crm.group_athithi_regional_admin') or self.env.user.has_group(
            'isha_crm.group_athithi_regional_viewer') or self.env.user.has_group('isha_crm.group_athithi_poc'):
			choises += [('influencer_tag', 'Assign Influenencer tags')]
		else:
			choises += [('tag', 'Assign Tags'), ('campaign', 'Allocate Call Campaign'),('correct_whatsapp_number','Correct WhatsApp Number'),
            ('wa_campaign','Add to Whatsapp Campaign'),('send_sms','Send SMS')]
		return choises

	selected_count = fields.Integer(string='Selected count')
	category_id = fields.Many2one('res.partner.category',string='Tags')
	influencer_tags = fields.Many2many('isha.influencer.tag', column1='partner_id', column2='influencer_tag_id',
                                   string='Influencer Tags')
	wa_capmaig_id = fields.Many2one('whatsapp.campaign',string='Whatsapp campaign')
	# campaign_id = fields.Many2one('call_campaign.call_campaign', string='Campaign') # Field present in call_campaign module due to import error
	# caller_id = fields.Many2one('call_campaign.caller', string='Pre-assign Caller (Optional)', required=False) # Field present in call_campaign module due to import error
	bulk_action = fields.Selection(selection=_get_bulk_actions, string='Select an action')

	@api.model
	def default_get(self, fields):
		# <field name="context">{}</field>
		result = super(ResPartnerImportTempWizard, self).default_get(fields)
		result['selected_count'] = len(self._context.get('active_ids'))
		return result

# suri: check usage
	#
	def getPartnerRecList(self, active_ids):
		active_recs = self.env['partner.import.temp'].browse(active_ids)
		partner_list = [x.partner_id.id for x in active_recs]
		return self.env['res.partner'].browse(partner_list).filtered(
			lambda r: (not r.influencer_type) and (
					r.is_caca_donor is not True or (r.is_caca_donor is True and str(r.last_txn_date) != '1900-01-01')))

	def getInfluencerPartnerRecList(self, active_ids):
		active_recs = self.env['partner.import.temp'].browse(active_ids)
		partner_list = [x.partner_id.id for x in active_recs]
		return self.env['res.partner'].browse(partner_list).filtered(
				lambda r: r.influencer_type and (
					r.is_caca_donor is not True or (r.is_caca_donor is True and str(r.last_txn_date) != '1900-01-01')))

	def get_popup(self, message):
		view = self.env.ref('sh_message.sh_message_wizard')
		view_id = view and view.id or False
		context = dict(self._context or {})
		context['message'] = message
		return {
			'name': 'Success',
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'sh.message.wizard',
			'views': [(view_id, 'form')],
			'view_id': view_id,
			'target': 'new',
			'context': context
		}

	def action_assign_tags(self):
		active_ids = self._context.get('active_ids')
		partner_rec_list = self.getPartnerRecList(active_ids)
		partner_rec_list.write({
			'category_id': [(4,self.category_id.id)]
		})
		return self.get_popup('Successfully Assigned Tags to '+str(len(partner_rec_list))+' Contact(s)')

	def action_assign_influencer_tags(self):
		active_ids = self._context.get('active_ids')
		influencer_partner_rec_list = self.getInfluencerPartnerRecList(active_ids)
		influencer_partner_rec_list.write({
			'influencer_tags': [(4, self.influencer_tags.id)]
		})
		return self.get_popup('Successfully Assigned Influencer Tags to ' + str(len(influencer_partner_rec_list)) + ' Contact(s)')

	def action_correct_whatsapp_number(self):
		active_ids = self._context.get('active_ids')
		partner_temp_rec_list = self.env['partner.import.temp'].browse(active_ids).filtered(
			lambda r: (r.match_level != 'res_access') and (r.mode == 'whatsapp_tagging') and (r.match_level == 'high'))
		for x in partner_temp_rec_list:
			x.partner_id.write({'whatsapp_number':x['phone'],'whatsapp_country_code':x['phone_cc']})

		return self.get_popup('Successfully corrected Whatsapp number for '+str(len(partner_temp_rec_list))+' Contact(s)')


	def action_bulk_flag_influencers(self):
		active_ids = self._context.get('active_ids')
		partner_rec_list = self.getPartnerRecList(active_ids)
		partner_rec_list.write({
			'influencer_type': 'influencer'
		})
		return self.get_popup('Successfully flagged influencer for '+str(len(partner_rec_list))+' Contact(s)')

	def action_bulk_assign_to_campaign(self):
		UserInput = self.env['call_campaign.user_input']
		create_list = []
		partner_list = self._context.get('active_ids')
		alloc_obj = self.env['isha.call_campaign.contact.allocation'].\
			with_context(active_model='partner.import.temp.wizard',active_ids=partner_list).sudo().create({
			'campaign_id':self.campaign_id.id,
			'caller_id':self.caller_id.id
		})
		ret_view = alloc_obj.action_contacts_to_leads()
		alloc_obj.unlink()
		return ret_view

	def action_bulk_assign_to_wa_campaign(self):
		partner_list = self._context.get('active_ids')
		alloc_obj = self.env['whatsapp.campaign.contact.allocation'].\
			with_context(active_model='partner.import.temp.wizard',active_ids=partner_list).sudo().create({
			'campaign_id':self.wa_capmaig_id.id,
			'selected_count':len(partner_list)
		})
		ret_view = alloc_obj.action_append_contacts()
		alloc_obj.unlink()
		return ret_view

	def action_send_sms(self):
		local_context = dict(
			self.env.context,
			res_partner_mapping='partner_id'
		)
		local_context['active_ids']= self.env['partner.import.temp'].browse(self._context.get('active_ids')).filtered(
			lambda r: (r.match_level != 'res_access')).ids
		return dict(
			name="Send SMS Text Message",
			binding_model="partner.import.temp",
			res_model="isha.sms.composer.wizard",
			binding_views="list",
			view_mode="form",
			target="new",
			type= 'ir.actions.act_window',
			context=local_context
		)

class IshaSourceContactReassign(models.TransientModel):
	""" wizard to reassign source contacts to different Contact """

	_name = 'isha.source.contact.reassign'
	_description = 'Re-assign source contacts to contact'

	partner_id = fields.Many2one('res.partner', string='Contact', required=True)#, ondelete='set null')
	selected_count = fields.Integer(string='Selected count')

	# contact_ids = fields.One2many(string='Contacts',comodel_name='res.partner')

	@api.model
	def default_get(self, fields):
		# <field name="context">{}</field>
		result = super(IshaSourceContactReassign, self).default_get(fields)
		result['selected_count'] = len(self._context.get('active_ids'))
		return result


	def action_source_contacts_to_contact(self):
		contact_map_ids = self._context.get('active_ids')
		dst_partner = self.partner_id
		contact_map_list = self.env['contact.map'].browse(contact_map_ids)
		update_models_list = [{'model': 'program.attendance', 'local_id': 'local_contact_id', 'system_id': 'system_id'},
							  {'model': 'donation.isha.foundation', 'local_id': 'local_patron_id',
							   'system_id': 'system_id'},
							  {'model': 'donation.isha.outreach', 'local_id': 'local_patron_id',
							   'system_id': 'system_id'},
							  {'model': 'donation.isha.iii', 'local_id': 'local_patron_id', 'system_id': 'system_id'},
							  {'model': 'donation.isha.education', 'local_id': 'local_patron_id',
							   'system_id': 'system_id'},
							  {'model': 'donation.isha.arogya', 'local_id': 'local_patron_id',
							   'system_id': 'system_id'}]
		for contact_map in contact_map_list:

			# Update Transactions
			for update_model in update_models_list:
				local_id = contact_map['local_contact_id']
				system_id = contact_map['system_id']
				txns = self.env[update_model['model']].search(
					[(update_model['local_id'], '=', local_id), (update_model['system_id'], '=', system_id)])
				for txn in txns:
					txn.write({'contact_id_fkey': dst_partner.id, 'guid': dst_partner.guid})

			# Update contact map
			contact_map.write({'contact_id_fkey': dst_partner.id, 'guid': dst_partner.guid})


class InfluencerTag(models.Model):
	_name = 'isha.influencer.tag'
	_description = 'Influencer tags'
	_order = 'name'
	_parent_store = True

	name = fields.Char(string='Tag Name', required=True, translate=True)
	color = fields.Integer(string='Color Index')
	parent_id = fields.Many2one('isha.influencer.tag', string='Parent Systags', index=True, ondelete='cascade')
	child_ids = fields.One2many('isha.influencer.tag', 'parent_id', string='Child Systags')
	active = fields.Boolean(default=True, help="The active field allows you to hide the category without removing it.")
	parent_path = fields.Char(index=True)
	partner_ids = fields.Many2many('res.partner', column1='influencer_tag_id', column2='partner_id', string='Partners')

	@api.constrains('parent_id')
	def _check_parent_id(self):
		if not self._check_recursion():
			raise ValidationError(_('You can not create recursive tags.'))


class Company(models.Model):
	_name = 'isha.company'

	_sql_constraints = [('name_unique', 'unique(name)', 'Choose another Company Name - Company Name has to be unique!')]

	name = fields.Char(string='Company Name', required=True, copy=False)
	sector = fields.Char(string='Sector', required=True)
	industry = fields.Char(string='Industry', required=True)

	def unlink(self):
		for rec in self:
			rec_list = self.env['res.partner'].search(['|',('company1', 'in', rec.ids),'|', ('company2', 'in', rec.ids),('company3', 'in', rec.ids)], limit=1)
			if len(rec_list) > 0:
				raise ValidationError(_('Operation Denied !!!. \n One or more Company cannot be deleted as it is assigned to contact.'))
		return super(Company, self).unlink()


class IecouserIsmumbaiiec(models.Model):
	_name = 'isha.iecousercheckismumbaiiec'

	email = fields.Char(string='Email', required=False, index=True)
	ismumbaiiec = fields.Boolean(default=False,string='isMumbaiIEC')
	partialpaid = fields.Integer(string='partialPaid')
	currency = fields.Char(string='Curency')

