# -*- coding: utf-8 -*-
import base64

from odoo import models, fields, api, _, tools, SUPERUSER_ID
from odoo.exceptions import UserError, ValidationError
from odoo.http import request, content_disposition, Response
import datetime, hashlib, hmac, werkzeug
import io
from odoo.tools import pycompat
from dateutil.relativedelta import relativedelta

dynamic_mail_res_partner_mapper = {
    'program.lead': 'contact_id_fkey',
    'program.attendance': 'contact_id_fkey',
    'sadhguru.exclusive': 'contact_id_fkey',
    'ieo.record': 'contact_id_fkey',
    'iec.mega.pgm': 'partner_id',
    'programs.view.orm': 'contact_id_fkey',
    'rudraksha.deeksha': 'contact_id_fkey',
    'isha.mailing.contact': 'contact_id_fkey',
    'completion.feedback': 'contact_id_fkey'
}

dynamic_txn_email_mapper = {
    'program.lead': 'record_email',
    'program.attendance': 'record_email',
    'sadhguru.exclusive': 'email',
    'ieo.record': 'email',
    'iec.mega.pgm': 'email',
    'programs.view.orm': 'email',
    'rudraksha.deeksha': 'email',
    'isha.mailing.contact': 'email',
    'completion.feedback': 'submitted_email'
}

class DynamicMailingLists(models.Model):
    _name = 'isha.dynamic.mailing.lists'
    _description = 'Dynamic mailing lists'
    _sql_constraints = [
        ('unique_ml_name', 'unique(name)', 'Mailing list name already exists.\nPlease choose a different name.')
    ]


    # active = fields.Boolean(string='Active',default=True)
    name = fields.Char(string='Mailing List Name',index=True)
    domain = fields.Text(string='Filter condition')
    model = fields.Many2one('ir.model', string='Data Source')
    subscription_id = fields.Many2one('isha.mailing.subscriptions',string='Subscription Category',domain="[('child_ids','=',False)]")
    model_model = fields.Char(store=False,string='Model',related='model.model')
    mode = fields.Selection([('fil_con','Filtered Condition - ( Dynamic )'),('sel_con','Selected Contacts - ( Static )')],
                            string='Selection Mode', default='fil_con')
    description = fields.Text(string='Description')

    email_selection = fields.Selection([('contact', 'Most Recent'),('txn', 'Linked to this initiative')],string='Mail Address to use')
    list_type = fields.Char(string='List Type',compute='_get_list_type',store=True)
    list_id = fields.Integer(related='id', string='List Id')

    list_count = fields.Integer(string='List Count (General)')
    pgm_upd_list_count = fields.Integer(string='List Count (Program Update)')

    list_status = fields.Selection([('draft','Draft'),('used','Used')],string='List Status',dafault='draft')

    def get_list_count(self):
        if self.email_selection == 'contact':
            email_field = 'email'
        else:
            email_field = dynamic_txn_email_mapper[self.model_model]
        if self.model.model == 'isha.mailing.contact':
            domain = eval(self.domain)
        else:
            domain = eval(self.domain) + self.env['mailing.mailing'].get_default_domain(False,email_field)
        self.list_count = self.env[self.model_model].sudo().search_count(domain)

        return self.env['sh.message.wizard'].get_popup('Counts refreshed Successfully')

    def get_list_count_pgm_update(self):
        if self.email_selection == 'contact':
            email_field = 'email'
        else:
            email_field = dynamic_txn_email_mapper[self.model_model]
        if self.model.model == 'isha.mailing.contact':
            domain = eval(self.domain)
        else:
            domain = eval(self.domain) + self.env['mailing.mailing'].get_default_domain(True,email_field)
        self.pgm_upd_list_count = self.env[self.model_model].sudo().search_count(domain)

        return self.env['sh.message.wizard'].get_popup('Counts refreshed Successfully')


    def get_list_status(self):
        if self.env['mailing.mailing'].search_count([('dynamic_mailing_list','=',self.id),('state','!=','draft')]) == 0:
            self.list_status = 'draft'
            return self.env['sh.message.wizard'].get_popup('List Status   ->   Draft')
        else:
            self.list_status = 'used'
            return self.env['sh.message.wizard'].get_popup('List Status   ->   Used')

    @api.onchange('domain')
    def _get_default_mailing_team(self):
        teams = self.env.user.mailing_teams
        if teams:
            if self.env.ref('isha_crm.mailing_team_emedia').id in self.env.user.mailing_teams.ids:
                self.team_id = self.env.ref('isha_crm.mailing_team_emedia').id
            else:
                self.team_id = teams[0].id
        else:
            self.team_id = False

    team_id = fields.Many2one(comodel_name='isha.mailing.team', string='Team Name')


    @api.depends('team_id')
    def _compute_mailing_team_read_only(self):
        for rec in self:
            readonly = False if self.env.ref('isha_crm.mailing_team_emedia').id in self.env.user.mailing_teams.ids \
                else True
            rec.mailing_team_read_only = readonly

    mailing_team_read_only = fields.Boolean(store=False, compute=_compute_mailing_team_read_only)

    @api.depends('mode')
    def _get_list_type(self):
        for rec in self:
            if rec.mode == 'sel_con':
                rec.list_type = 'Static'
            elif rec.mode == 'fil_con':
                rec.list_type = 'Dynamic'
            else:
                rec.list_type = None

    def get_regions(self):
        region_ids = self.env.user.centers.mapped('region_id').ids
        region_ids = list(set(region_ids))
        return [(6,0,region_ids)]
    regions = fields.Many2many('isha.region','dynamic_mailing_list_region_rel',string='Region',default=get_regions)

    @api.onchange('mode')
    def alter_domain(self):
        if self.model.model in ['ieo.record','iec.mega.pgm']:
            default_centers = [('partner_center_id','in',self.env.user.centers.ids)]
        else:
            default_centers = [('center_id','in',self.env.user.centers.ids)]

        if self.model_model == 'mailing.list':
            self.domain = [('id','in',self._context.get('active_ids'))]
        else:
            if self.mode == 'sel_con':
                self.domain = [('id','in',self._context.get('active_ids'))] + default_centers
            elif self.mode == 'fil_con':
                self.domain = (self._context.get('active_domain') or []) + default_centers

    def show_selected_recs(self):
        # Need to revisit if this is needed to allow the user to edit the existing mailing list
        # request.session['mailing_list_id'] = self.id
        local_context = dict(
            self.env.context,
        )
        if self.email_selection == 'contact':
            email_field = 'email'
        else:
            email_field = dynamic_txn_email_mapper[self.model_model]
        if self.model.model == 'isha.mailing.contact':
            domain = eval(self.domain)
        else:
            domain = eval(self.domain) + self.env['mailing.mailing'].get_default_domain(False,email_field)
        return {
            'name': 'Mailing List Selection',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'domain': domain,
            'view_id': False,
            'views': [(False, 'tree'),(False,'form')],
            'res_model': self.model.model,
            'target': 'current',
            'context': local_context
        }

    def dynamic_mailing_list(self):

        local_context = dict(
            self.env.context,
            default_domain=self._context.get('active_domain'),
            default_model=self.env['ir.model'].search([('model', '=', self._context.get('active_model'))]).id,
        )
        # print(self._context.get('active_model'))
        if self._context.get('active_model') in ['res.partner']:
            local_context['default_email_selection'] = 'contact'
        if self._context.get('active_model') in ['isha.mailing.contact']:
            local_context['default_email_selection'] = 'txn'

        view_id = self.env.ref('isha_crm.isha_dynamic_ml_form_view1').id
        action = {
             'type': 'ir.actions.act_window',
             'view_mode': 'form',
             'view_id': view_id,
             'views': [(view_id, 'form')],
             'res_model': 'isha.dynamic.mailing.lists',
             'target': 'new',
             'context': local_context
         }
         # Below will catch the exception when default_domain is Null
        Check_empty_domain = local_context.get('default_domain')

        if Check_empty_domain == None:
            action = {}

        if request.session.get('mailing_list_id',None):
            action['res_id'] = request.session.pop('mailing_list_id',None)
        return action

    def unlink(self):
        try:
            return super(DynamicMailingLists, self).unlink()
        except Exception as ex:
            raise UserError('Operation Denied  !!!\nMailing list has already been used to send emails.')

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        context = self._context or {}
        if context.get('region_scope'):
            domain += [('regions', 'in', self.env.user.centers.mapped('region_id').ids)]
        if context.get('team_scope'):
            if not self.env.user.mailing_teams.ids:
                domain += [('team_id','=', False)]
            elif self.env.ref('isha_crm.mailing_team_emedia').id not in self.env.user.mailing_teams.ids:
                domain += ['|',('team_id','=', False),('team_id', 'in', self.env.user.mailing_teams.ids)]

        return super(DynamicMailingLists, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                         orderby=orderby, lazy=lazy)

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}
        if context.get('region_scope'):
            args += [('regions', 'in', self.env.user.centers.mapped('region_id').ids)]
        if context.get('team_scope'):
            if not self.env.user.mailing_teams.ids:
                args += [('team_id','=', False)]
            elif self.env.ref('isha_crm.mailing_team_emedia').id not in self.env.user.mailing_teams.ids:
                args += ['|',('team_id','=', False),('team_id', 'in', self.env.user.mailing_teams.ids)]

        return super(DynamicMailingLists, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)



class MailingDedupStats(models.Model):
    _name = 'isha.mailing.dedup.stats'
    _description = 'Mass Mailing Dedup Stats'
    _rec_name = 'mailing_id'

    mailing_id = fields.Many2one('mailing.mailing',string='Mailing')
    subscription_category = fields.Char(related='mailing_id.dynamic_mailing_list.subscription_id.name')

    source_ml_count = fields.Integer(string='Source Count')

    deduped_count = fields.Integer(string='Deduped Count')
    deduped_percentage = fields.Text(string='Dedup percentage',compute='get_percentage',store=False)
    def _get_dup_count(self):
        self.dup_count = self.source_ml_count - self.deduped_count


    dup_count = fields.Integer(compute='_get_dup_count',string='Dup Count')
    dup_percentage = fields.Text(string='Dup percentage', compute='get_percentage', store=False)

    # internal_dup_emails = fields.Text(string='Internal Duplicates')
    internal_dup_emails_count = fields.Integer(string='Internal Duplicates Count')
    internal_dup_percentage = fields.Text(string='Internal Dup percentage', compute='get_percentage', store=False)

    invalid_emails = fields.Text(string='Invalid Emails')
    invalid_emails_count = fields.Integer(string='Invalid Emails')
    invalid_email_recs = fields.Text(string='Invalid Recs')
    invalid_emails_percentage = fields.Text(string='Invalid Emails', compute='get_percentage', store=False)

    unsubscription_emails = fields.Text(char='Unsubscription Emails')
    unsubscription_emails_count = fields.Integer(string='Unsubscription Emails Count')
    global_unsubs = fields.Integer(string='Global Unsubscription')
    unsubs_dup_percentage = fields.Text(string='Unsubs Dup percentage', compute='get_percentage', store=False)

    no_marketing_ids = fields.Text(string='Marketing Restricted')
    no_marketing_ids_count = fields.Integer(string='Marketing Restricted Count')
    no_marketing_dup_percentage = fields.Text(string='Marketing Restricted percentage', compute='get_percentage', store=False)

    no_subs_ids = fields.Text(string='No Subscription')
    no_subs_ids_count = fields.Integer(string='No Subscription Count')
    no_subs_dup_percentage = fields.Text(string='No Subs Dup percentage', compute='get_percentage', store=False)

    restricted_ids = fields.Text(string='Restricted Records')
    restricted_ids_count = fields.Integer(string='Restricted Records Count')
    restricted_ids_dup_percentage = fields.Text(string='Restricted Records Dup percentage', compute='get_percentage', store=False)

    suppression_list_count = fields.Integer(string='Suppression Count')
    global_suppression_count = fields.Integer(string='Global Suppression')
    suppression_dup_percentage = fields.Text(string='Suppression Dup percentage', compute='get_percentage', store=False)

    blacklist_emails = fields.Text(string='Blacklist Emails')
    blacklist_emails_count = fields.Integer(string='Blacklist Emails Count')
    global_blacklist = fields.Integer(string='Global Blacklist')
    blacklist_dup_percentage = fields.Text(string='Blacklist Dup percentage', compute='get_percentage', store=False)

    unverified_email_recs = fields.Text(string='Unverified Email Recs')
    unverified_emails = fields.Text(string='Unverified Emails')
    unverified_email_recs_count = fields.Integer(string='Unverified Email Count')
    unverified_email_recs_percentage = fields.Text(string='Un Verified percentage', compute='get_percentage', store=False)

    unmapped_emails_recs = fields.Text(string="Unmapped Emails")
    unmapped_emails_count = fields.Integer(string='Unmapped Email Count')
    unmapped_emails_recs_percentage = fields.Text(string='Un Mapped percentage', compute='get_percentage', store=False)

    time_taken = fields.Text(string='Time Taken')


    def get_percentage(self):
        deduped_percentage = round((self.deduped_count / self.source_ml_count) * 100,2) if self.source_ml_count > 0 else 0
        self.deduped_percentage = """<div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: %s%%;" aria-valuenow="%s" aria-valuemin="0" aria-valuemax="100">%s %%</div>
                                    </div>"""%(deduped_percentage,deduped_percentage,deduped_percentage)
        dup_percentage = round((self.dup_count / self.source_ml_count) * 100,2) if self.source_ml_count > 0 else 0
        self.dup_percentage = """<div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: %s%%;" aria-valuenow="%s" aria-valuemin="0" aria-valuemax="100">%s %%</div>
                                    </div>"""%(dup_percentage,dup_percentage,dup_percentage)
        internal_dup_percentage = round((self.internal_dup_emails_count / self.source_ml_count) * 100,2) if self.source_ml_count > 0 else 0
        self.internal_dup_percentage = """<div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: %s%%;" aria-valuenow="%s" aria-valuemin="0" aria-valuemax="100">%s %%</div>
                                    </div>"""%(internal_dup_percentage,internal_dup_percentage,internal_dup_percentage)
        unsubs_dup_percentage = round((self.unsubscription_emails_count / self.source_ml_count) * 100,2) if self.source_ml_count > 0 else 0
        self.unsubs_dup_percentage = """<div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: %s%%;" aria-valuenow="%s" aria-valuemin="0" aria-valuemax="100">%s %%</div>
                                    </div>"""%(unsubs_dup_percentage,unsubs_dup_percentage,unsubs_dup_percentage)
        no_subs_dup_percentage = round((self.no_subs_ids_count / self.source_ml_count) * 100,2) if self.source_ml_count > 0 else 0
        self.no_subs_dup_percentage = """<div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: %s%%;" aria-valuenow="%s" aria-valuemin="0" aria-valuemax="100">%s %%</div>
                                    </div>"""%(no_subs_dup_percentage,no_subs_dup_percentage,no_subs_dup_percentage)

        restricted_ids_dup_percentage = round((self.restricted_ids_count / self.source_ml_count) * 100, 2) if self.source_ml_count > 0 else 0
        self.restricted_ids_dup_percentage = """<div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: %s%%;" aria-valuenow="%s" aria-valuemin="0" aria-valuemax="100">%s %%</div>
                                    </div>""" % (restricted_ids_dup_percentage, restricted_ids_dup_percentage, restricted_ids_dup_percentage)

        no_marketing_dup_percentage = round((self.no_marketing_ids_count / self.source_ml_count) * 100,2) if self.source_ml_count > 0 else 0
        self.no_marketing_dup_percentage = """<div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: %s%%;" aria-valuenow="%s" aria-valuemin="0" aria-valuemax="100">%s %%</div>
                                    </div>"""%(no_marketing_dup_percentage,no_marketing_dup_percentage,no_marketing_dup_percentage)
        suppression_dup_percentage = round((self.suppression_list_count / self.source_ml_count) * 100,2) if self.source_ml_count > 0 else 0
        self.suppression_dup_percentage = """<div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: %s%%;" aria-valuenow="%s" aria-valuemin="0" aria-valuemax="100">%s %%</div>
                                    </div>"""%(suppression_dup_percentage,suppression_dup_percentage,suppression_dup_percentage)
        blacklist_dup_percentage = round((self.blacklist_emails_count / self.source_ml_count) * 100,2) if self.source_ml_count > 0 else 0
        self.blacklist_dup_percentage = """<div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: %s%%;" aria-valuenow="%s" aria-valuemin="0" aria-valuemax="100">%s %%</div>
                                    </div>"""%(blacklist_dup_percentage,blacklist_dup_percentage,blacklist_dup_percentage)
        unverified_email_recs_percentage = round((self.unverified_email_recs_count / self.source_ml_count) * 100,2) if self.source_ml_count > 0 else 0
        self.unverified_email_recs_percentage = """<div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: %s%%;" aria-valuenow="%s" aria-valuemin="0" aria-valuemax="100">%s %%</div>
                                    </div>"""%(unverified_email_recs_percentage,unverified_email_recs_percentage,unverified_email_recs_percentage)
        unmapped_emails_recs_percentage = round((self.unmapped_emails_count / self.source_ml_count) * 100,2) if self.source_ml_count > 0 else 0
        self.unmapped_emails_recs_percentage = """<div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: %s%%;" aria-valuenow="%s" aria-valuemin="0" aria-valuemax="100">%s %%</div>
                                    </div>"""%(unmapped_emails_recs_percentage,unmapped_emails_recs_percentage,unmapped_emails_recs_percentage)
        invalid_email_recs_percentage = round((self.invalid_emails_count / self.source_ml_count) * 100,2) if self.invalid_emails_count > 0 else 0
        self.invalid_emails_percentage = """<div class="progress">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: %s%%;" aria-valuenow="%s" aria-valuemin="0" aria-valuemax="100">%s %%</div>
                                    </div>"""%(invalid_email_recs_percentage,invalid_email_recs_percentage,invalid_email_recs_percentage)

    def show_em_unverified(self):
        mailing = self.mailing_id
        local_context = dict(
            self.env.context
        )
        return {
            'name': 'Unverified Emails - ' + mailing.mailing_name,
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'domain': [('id', 'in', eval(self.unverified_email_recs))],
            'view_id': False,
            'views': [(False, 'tree'), (False, 'form')],
            'res_model': mailing.mailing_model_name,
            'target': 'current',
            'context': local_context
        }

    def show_em_unmapped(self):
        mailing = self.mailing_id
        local_context = dict(
            self.env.context
        )
        return {
            'name': 'Unverified Emails - ' + mailing.mailing_name,
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'domain': [('id', 'in', eval(self.unmapped_emails_recs))],
            'view_id': False,
            'views': [(False, 'tree'), (False, 'form')],
            'res_model': mailing.mailing_model_name,
            'target': 'current',
            'context': local_context
        }

    def from_data(self, fields, rows):
        fp = io.BytesIO()
        writer = pycompat.csv_writer(fp, quoting=1)

        writer.writerow(fields)

        for data in rows:
            row = []
            for d in data:
                # Spreadsheet apps tend to detect formulas on leading =, + and -
                if isinstance(d, str) and d.startswith(('=', '-', '+')):
                    d = "'" + d

                row.append(pycompat.to_text(d))
            writer.writerow(row)
        return fp.getvalue()

    def _get_unverified_dump(self):
        export_data = [[x] for x in eval(self.unverified_emails)]
        self.unverified_email_dump = base64.b64encode(self.from_data(['email'], export_data))

    unverified_email_dump = fields.Binary(compute=_get_unverified_dump,store=False)


    def _get_invalid_dump(self):
        export_data = [[x] for x in eval(self.invalid_emails)]
        self.invalid_email_dump = base64.b64encode(self.from_data(['email'], export_data))

    invalid_email_dump = fields.Binary(compute=_get_invalid_dump,store=False)

    def _get_download_button(self):
        if self.unverified_email_recs_count > 0 and (self.env.user.has_group('isha_crm.group_mass_mailing_admin') or self.env.user.has_group('isha_crm.group_mass_mailing_manager')):
            self.unverified_emails_download = """
                                    <a id="download_unverified" class="btn-lg btn-info oe_read_only" style="border-radius: 20px; border:0px; font-weight:bold;"
                                        href="/web/content/?model=isha.mailing.dedup.stats&id=%s&filename=%s&field=%s&download=true&name_file=%s" target="_blank">
                                        <span class="fa fa-download"/>
                                    </a>
            """ % (self.id,'unverified_email_campaign_'+str(self.id)+'_'+str(int(datetime.datetime.now().timestamp()))+'.csv','unverified_email_dump','unverified_email_campaign_'+str(self.id)+'_'+str(int(datetime.datetime.now().timestamp()))+'.csv')
        else:
            self.unverified_emails_download = ""
        if self.invalid_emails_count > 0 and (self.env.user.has_group('isha_crm.group_mass_mailing_admin') or self.env.user.has_group('isha_crm.group_mass_mailing_manager')):
            self.invalid_email_download = """
                                    <a id="download_invalid_emails" class="btn-lg btn-info oe_read_only" style="border-radius: 20px; border:0px; font-weight:bold;"
                                        href="/web/content/?model=isha.mailing.dedup.stats&id=%s&filename=%s&field=%s&download=true&name_file=%s" target="_blank">
                                        <span class="fa fa-download"/>
                                    </a>
            """ % (self.id,'invalid_email_campaign_'+str(self.id)+'_'+str(int(datetime.datetime.now().timestamp()))+'.csv','invalid_email_dump','invalid_email_campaign_'+str(self.id)+'_'+str(int(datetime.datetime.now().timestamp()))+'.csv')
        else:
            self.invalid_email_download = ""

    unverified_emails_download = fields.Text(compute=_get_download_button,strore=False)
    invalid_email_download = fields.Text(compute=_get_download_button,strore=False)

    def show_em_blacklisted(self):
        mailing = self.mailing_id
        local_context = dict(
            self.env.context
        )
        return {
            'name': 'Blacklisted Emails set within '+mailing.mailing_name,
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'domain':[('email','in',eval(self.blacklist_emails))],
            'view_id': False,
            'views': [(False, 'tree'),(False,'form')],
            'res_model': "mail.blacklist",
            'target': 'current',
            'context': local_context
        }

    def show_em_unsubs(self):
        mailing = self.mailing_id
        local_context = dict(
            self.env.context
        )
        return {
            'name': 'Unsubscriptions set within '+mailing.mailing_name,
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'domain':[('unsubscription_email','in',eval(self.unsubscription_emails)),('subscription_id','=',mailing.dynamic_mailing_list.subscription_id.id)],
            'view_id': False,
            'views': [(False, 'tree'),(False,'form')],
            'res_model': 'isha.mailing.unsubscriptions',
            'target': 'current',
            'context': local_context
        }

    def show_em_no_subs(self):
        mailing = self.mailing_id
        local_context = dict(
            self.env.context
        )
        return {
            'name': 'Missing Subscription Category',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'domain':[('id','in',eval(self.no_subs_ids))],
            'view_id': False,
            'views': [(False, 'tree'),(False,'form')],
            'res_model': mailing.mailing_model_name,
            'target': 'current',
            'context': local_context
        }

    def show_em_restricted_recs(self):
        mailing = self.mailing_id
        local_context = dict(
            self.env.context
        )
        return {
            'name': 'Restricted Records Category',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'domain': [('id', 'in', eval(self.restricted_ids))],
            'view_id': False,
            'views': [(False, 'tree'), (False, 'form')],
            'res_model': mailing.mailing_model_name,
            'target': 'current',
            'context': local_context
        }
    def show_em_invalid_email_recs(self):
        mailing = self.mailing_id
        local_context = dict(
            self.env.context
        )
        return {
            'name': 'Invalid Email',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'domain': [('id', 'in', eval(self.invalid_email_recs))],
            'view_id': False,
            'views': [(False, 'tree'), (False, 'form')],
            'res_model': mailing.mailing_model_name,
            'target': 'current',
            'context': local_context
        }

    def show_no_marketing_recs(self):
        mailing = self.mailing_id
        local_context = dict(
            self.env.context
        )
        return {
            'name': 'Missing Subscription Category',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'domain':[('id','in',eval(self.no_marketing_ids) if self.no_marketing_ids else [])],
            'view_id': False,
            'views': [(False, 'tree'),(False,'form')],
            'res_model': mailing.mailing_model_name,
            'target': 'current',
            'context': local_context
        }
class MailingSubscriptions(models.Model):
    _name = 'isha.mailing.subscriptions'
    _description = 'Isha Mailing Subscriptions'
    _inherit = ['mail.thread']

    name = fields.Char(string='Category',index=True,tracking=True)
    color = fields.Integer(string='Color')
    dynamic_ml_ids = fields.One2many('isha.dynamic.mailing.lists','subscription_id',string='Active Mailing Lists')
    is_public = fields.Boolean(string='Public visibility',default=False)
    is_pgm_update = fields.Boolean(string='Is Program Update?',default=False)
    active = fields.Boolean(sting='Active',default=True)
    description = fields.Text(string='Description')

    def get_default_regions(self):
        region_ids = self.env.user.centers.mapped('region_id').ids
        return [(6,0,region_ids)]
    def _get_region_domain(self):
        return [('id','in',self.env.user.centers.mapped('region_id').ids)]
    regions = fields.Many2many('isha.region','isha_subs_region_rel',string='Region',domain=_get_region_domain,default=get_default_regions)

    # Hierarchical Categories
    parent_id = fields.Many2one('isha.mailing.subscriptions', string='Parent Category',index=True,tracking=True)
    child_ids = fields.One2many('isha.mailing.subscriptions','parent_id',string='Child Categories')

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        context = self._context or {}
        if context.get('region_scope'):
            domain += [('regions', 'in', self.env.user.centers.mapped('region_id').ids)]
        return super(MailingSubscriptions, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                         orderby=orderby, lazy=lazy)

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}
        if context.get('region_scope'):
            args += [('regions', 'in', self.env.user.centers.mapped('region_id').ids)]
        return super(MailingSubscriptions, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)


    @api.returns('self', lambda value: value.id)
    def copy(self, default=None):
        self.ensure_one()
        default = dict(default or {},
                       name=_('%s (copy)') % self.name)
        res = super(MailingSubscriptions, self).copy(default=default)
        return res

    @api.constrains('parent_id')
    def _check_parent_id(self):
        if not self._check_recursion():
            raise ValidationError(_('You can not create recursive tags.'))
        if self.parent_id and self.name in self.parent_id.child_ids.filtered(lambda x:x.id != self.id).mapped('name'):
            raise ValidationError('Category with the name ' + self.name + ' already exists under this parent. Plz choose a different name')



    def name_get(self):
        """ Return the categories' display name, including their direct
            parent by default.

            If ``context['partner_category_display']`` is ``'short'``, the short
            version of the category name (without the direct parent) is used.
            The default is the long version.
        """
        if self._context.get('partner_category_display') == 'short':
            return super(MailingSubscriptions, self).name_get()

        res = []
        for category in self:
            names = []
            current = category
            while current:
                names.append(current.name)
                current = current.parent_id
            res.append((category.id, ' > '.join(names)))
        return res


    @api.model
    def _name_search(self, name, args=None, operator='ilike', limit=100, name_get_uid=None):
        args = args or []
        if name:
            # Be sure name_search is symetric to name_get
            name = name.split(' -> ')[-1]
            args = [('name', operator, name)] + args
        partner_pgm_tag_ids = self._search(args, limit=limit, access_rights_uid=name_get_uid)
        return self.browse(partner_pgm_tag_ids).name_get()

    def write(self,vals):
        if 'name' in vals:
            recs = self.env['res.partner'].sudo().search([('ml_subscription_ids','in',self.ids)],count=True)
            if recs>0:
                return UserError("Category Already Assigned to Contacts.\n Renaming is blocked.")

        return super(MailingSubscriptions,self).write(vals)


class WhitelistDomain(models.Model):
    _name = 'isha.whitelist.domain'
    _description = 'Whitelisted Emails'
    _rec_name = 'email_formatted'
    name = fields.Char(string="User Name", required=True)
    email = fields.Char(string='Email', required=True)
    email_formatted = fields.Char(string='Whitelisted Id', store=True, compute='_get_value_formatted')

    @api.depends('name', 'email')
    def _get_value_formatted(self):
        if self.name and self.email:
            self.email_formatted = tools.formataddr((self.name, self.email))


class MailSubsRules(models.Model):
    _name = 'isha.subscription.rules'
    _description = 'Rule Based Auto Subscriptions'
    _inherit = ['mail.thread']

    name = fields.Char(string='Rule Name')
    subscription_id = fields.Many2one('isha.mailing.subscriptions',string='Category',domain="[('child_ids','=',False)]")
    state = fields.Selection([('draft','Draft'),('todo','To Do'),('inprogress','In-progress'),('done','Deployed')],
                             string='State',tracking=True,default='draft')
    description = fields.Text(string='Rule Description')
    def get_default_regions(self):
        region_ids = self.env.user.centers.mapped('region_id').ids
        return [(6,0,region_ids)]
    def _get_region_domain(self):
        return [('id','in',self.env.user.centers.mapped('region_id').ids)]
    regions = fields.Many2many('isha.region','isha_subs_rules_region_rel',string='Region',domain=_get_region_domain,default=get_default_regions)


    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        context = self._context or {}
        if context.get('region_scope'):
            domain += [('regions', 'in', self.env.user.centers.mapped('region_id').ids)]
        return super(MailSubsRules, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                         orderby=orderby, lazy=lazy)

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}
        if context.get('region_scope'):
            args += [('regions', 'in', self.env.user.centers.mapped('region_id').ids)]
        return super(MailSubsRules, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)

    def send_email_notif(self, subject, header, body, email_to):
        su_id = self.env['res.partner'].browse(SUPERUSER_ID)
        body_str = self.env.ref('isha_crm.generic_mail_template').body_html.replace('$$HEADING',header).replace('$$BODY',body)
        # post the message
        mail_values = {
            'email_from': su_id.email,
            'email_to':email_to,
            'subject': subject,
            'body_html': body_str,
            'auto_delete': True,
        }
        mail = self.env['mail.mail'].sudo().create(mail_values)
        return mail.send(False)

    def freeze_rule(self):
        self.write({'state':'todo'})
        header = 'New Auto Subscription Rule is in To Do'
        body = """
        <p>Namaskaram,</p><br/><br/>
        <p>%s rule has been moved to To Do. Plz take necessary actions.</p><br/><br/>
        <a style="background-color: #875A7B; padding: 8px 16px 8px 16px; text-decoration: none; color: #fff; border-radius: 5px; font-size:13px;" 
         href="/web#model=isha.subscription.rules&id=%s">View Record</a>
         <br/><br/><br/>
         <p>Pranam</p>
        """%(self.name,self.id)
        subject = 'Ishangam New Auto Subscription Rule Added'
        self.send_email_notif(subject,header,body,self.sudo().env.ref('isha_crm.email_notif_mass_mailing_upd').value)
        return self.env['sh.message.wizard'].get_popup('Requirements are successfully frozen. IT team will take it forward.')

    def inprogress_trigger(self):
        self.write({'state': 'inprogress'})
        header = 'Update for rule based subscription'
        body = """
        <p>Namaskaram %s,</p><br/><br/>
        <p><b>%s</b> auto subscription rule that you had created is now in In-Progress state.</p><br/><br/>
        <a style="background-color: #875A7B; padding: 8px 16px 8px 16px; text-decoration: none; color: #fff; border-radius: 5px; font-size:13px;" 
         href="/web#model=isha.subscription.rules&id=%s">View Record</a>
         <br/><br/><br/>
         <p>Pranam</p>
        """%(self.create_uid.name,self.name,self.id)
        subject = 'Ishangam %s Rule update'%(self.name)
        self.send_email_notif(subject,header,body,self.create_uid.email)
        return self.env['sh.message.wizard'].get_popup('Successfully updated the status.')

    def deployed_trigger(self):
        self.write({'state': 'done'})
        header = 'Update for rule based subscription'
        body = """
        <p>Namaskaram %s,</p><br/><br/>
        <p><b>%s</b> rule that you had created is successfully deployed.</p><br/><br/>
        <a style="background-color: #875A7B; padding: 8px 16px 8px 16px; text-decoration: none; color: #fff; border-radius: 5px; font-size:13px;" 
         href="/web#model=isha.subscription.rules&id=%s">View Record</a>
         <br/><br/><br/>
         <p>Pranam</p>
        """%(self.create_uid.name,self.name,self.id)
        subject = 'Ishangam %s Rule update'%(self.name)
        self.send_email_notif(subject,header,body,self.create_uid.email)
        return self.env['sh.message.wizard'].get_popup('Successfully updated the status.')




class MailingUnSubscriptions(models.Model):
    _name = 'isha.mailing.unsubscriptions'
    _inherit = ['mail.thread']
    _description = 'Isha Mailing Un-Subscriptions'
    _rec_name = 'subscription_id'

    partner_id = fields.Many2one('res.partner',string='Contact',index=True)
    res_id = fields.Integer(string='Data Source ID')
    model = fields.Char(string='Data Source')
    subscription_id = fields.Many2one('isha.mailing.subscriptions', string='Subscription Category',index=True)
    unsubscription_date = fields.Datetime(string='Unsubscription Date',index=True,tracking=True)
    opt_in_date = fields.Datetime(string='Manual Opt-In Date',index=True,tracking=True)
    unsubscription_email = fields.Char(string='Unsubscribed Email',index=True)
    reason = fields.Text(string='Un-Subscription Reason',tracking=True)
    active = fields.Boolean(sting='Active',default=True)
    mailing_id = fields.Many2one('mailing.mailing', string='Mailing',tracking=True)

    def manual_update_opt_in_action(self):
        rec = self.browse(self._context.get('active_id'))
        unsubs = self.sudo().search([('unsubscription_email','=',rec.unsubscription_email),
                                     ('subscription_id','=',rec.subscription_id.id)])
        if unsubs:
            unsubs.write({'opt_in_date':datetime.datetime.now(),'active':False,'reason':'Manual Opt-In from backend'})
        view = self.env.ref('sh_message.sh_message_wizard')
        view_id = view and view.id or False
        context = dict(self._context or {})
        context['message'] = 'Successfully Opted-In ' + rec.unsubscription_email + ' from ' + rec.subscription_id.name
        return {
            'name': 'Success',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sh.message.wizard',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': context
        }


    def manual_unsubscribe_action(self):
        rec = self.browse(self._context.get('active_id'))
        unsubs = self.sudo().with_context(active_test=False).search([('unsubscription_email', '=', rec.unsubscription_email),
                                     ('subscription_id', '=', rec.subscription_id.id)])
        if unsubs:
            unsubs.write({'unsubscription_date': datetime.datetime.now(), 'active': True, 'reason': 'Manual Opt-Out from backend'})
        view = self.env.ref('sh_message.sh_message_wizard')
        view_id = view and view.id or False
        context = dict(self._context or {})
        context[
            'message'] = 'Successfully UnSubscribed ' + rec.unsubscription_email + ' from ' + rec.subscription_id.name
        return {
            'name': 'Success',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sh.message.wizard',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': context
        }

class MassMailingProcessInfo(models.Model):
    _name = 'mass.mailing.process.info'
    _description = 'Mass Mailing Process Info'

    mailing_id = fields.Many2one('mailing.mailing',string='Mailing')
    pid = fields.Integer(string='PID')
    start_time = fields.Datetime(string='Start Time')
    end_time= fields.Datetime(string='End Time')


class EmailMasters(models.Model):
    _name = 'isha.email.verified.master'
    _description = 'List of verified emails'
    _sql_constraints = [
        ('unique_email', 'unique (email)', 'Email address already exists!')
    ]
    email = fields.Char(string='Email Id',index=True)
    active = fields.Boolean(string='Active',default=True)
    soft_bounce_count = fields.Integer(string='Soft Bounce count')

    def get_csv_wizard(self):
        view = self.env.ref('isha_crm.email_marketing_dedup_wizard_form')
        view_id = view and view.id or False
        context = dict(self._context or {})
        return {
            'name': 'Verified master Dedup Engine',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'crm.csv.handler',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': context
        }

    @api.model_create_multi
    def create(self, values):
        # First of all, extract values to ensure emails are really unique (and don't modify values in place)
        new_values = []
        all_emails = []
        for value in values:
            email = value.get('email').lower() if value.get('email') else None
            if not email or email in all_emails:
                continue
            all_emails.append(email)
            new_value = dict(value, email=email)
            new_values.append(new_value)

        """ To avoid crash during import due to unique email, return the existing records if any """
        sql = '''SELECT email, id FROM isha_email_verified_master WHERE email = ANY(%s)'''
        emails = [v['email'] for v in new_values]
        self._cr.execute(sql, (emails,))
        master_entries = dict(self._cr.fetchall())
        to_create = [v for v in new_values if v['email'] not in master_entries]
        self.env['isha.email.verified.master'].browse(master_entries.values()).write({'active': True})
        # TODO DBE Fixme : reorder ids according to incoming ids.
        results = super(EmailMasters, self).create(to_create)
        return self.env['isha.email.verified.master'].browse(master_entries.values()) | results

    def update_soft_bounce_count(self):
        today = datetime.date.today()
        yesterday = today - datetime.timedelta(days=1)

        soft_bounces = self.env['mailing.trace'].search([('create_date', '>=', yesterday), ('create_date', '<', today),
                                                         ('bounce_type', '!=', None), ('bounce_type', '!=', 'Permanent')])
        soft_bounced_emails = soft_bounces.mapped('email')
        email_master = self.env['isha.email.verified.master'].search([('email', 'in', soft_bounced_emails)])
        if email_master:
            for email_rec in email_master:
                email_rec['soft_bounce_count'] = email_rec['soft_bounce_count'] + 1

        success_emails = self.env['mailing.trace'].search([('create_date', '>=', yesterday), ('create_date', '<', today),
                                                         ('sent', '!=', None), ('exception', '=', None), ('bounced', '=', None)])
        success_emails = success_emails.mapped('email')
        email_master = self.env['isha.email.verified.master'].search([('email', 'in', success_emails)])
        if email_master:
            for email_rec in email_master:
                email_rec['soft_bounce_count'] = 0

        soft_bounce_limit = self.env['ir.config_parameter'].sudo().get_param('isha.email.soft_bounce_limit')
        if not soft_bounce_limit:
            raise Exception('Config parameter not set isha.email.soft_bounce_limit')
        email_master = self.env['isha.email.verified.master'].search([('soft_bounce_count', '>=', int(soft_bounce_limit))])
        if email_master:
            for email_rec in email_master:
                self.env['mail.blacklist'].create({
                    'email': email_rec['email'],
                    'reason': 'hard_bounce'
                })


class WizardDedupIshaMailingContacts(models.TransientModel):

    _name = 'wizard.isha.mailing.contacts.dedup'
    _description = 'wizard to dedup Non-CRM contacts'

    list_identifier = fields.Char(string='List Identifier', required=True)

    def dedup_mailing_contacts(self):
        # Switching to query based Dedup as this is having memory error.
        # contacts = self.env['isha.mailing.contact'].search([('list_identifier', '=', self.list_identifier)], order='write_date desc')
        # dedup_map = {}
        # for contact in contacts:
        #     subscription_id = contact.ml_subscription_ids[0].id if contact.ml_subscription_ids else None
        #     key = contact.email + str(subscription_id)
        #     if key not in dedup_map:
        #         dedup_map[key] = list()
        #     dedup_map[key].append(contact)
        # duplicates = []
        # for key in dedup_map:
        #     duplicates.extend(dedup_map[key][1:])
        # for rec in duplicates:
        #     rec.sudo().unlink()
        query = """
            delete from isha_mailing_contact where id in (
                select min(id) from isha_mailing_contact ml,mailing_contact_subscriptions_rel rel where
                ml.id = rel.isha_mailing_contact_id and list_identifier = '%s'
                group by list_identifier,isha_mailing_subscriptions_id,email
                having count(*) > 1
            )
        """ % self.list_identifier
        self.env.cr.execute(query)
        self.env.cr.commit()


class IshaMailingContacts(models.Model):
    _name = 'isha.mailing.contact'
    _description = 'Isha Mailing Contacts'

    name = fields.Char(string='Name')
    email = fields.Char(string='Email', index=True, required=True)
    country = fields.Char(string='Country ISO2')
    country_id = fields.Many2one('res.country', string='Country')
    language = fields.Char(string='Language ISO2')
    language_id = fields.Many2one('res.lang', string='Language')
    ml_subscription_ids = fields.Many2many('isha.mailing.subscriptions', 'mailing_contact_subscriptions_rel',
                                           string='Subscriptions')
    un_verified_subs_ids = fields.Many2many('isha.mailing.subscriptions', 'mailing_contact_unverified_subs_rel', string='Unverified Subscriptions')
    system_id = fields.Integer(string='System Id')
    contact_id_fkey = fields.Many2one('res.partner', string='Matched Contact', ondelete='set null', auto_join=True, index=True)
    list_identifier = fields.Char(string='List Identifier')

    @api.onchange('list_identifier')
    def _get_default_mailing_team(self):
        teams = self.env.user.mailing_teams
        if teams:
            if self.env.ref('isha_crm.mailing_team_emedia').id in self.env.user.mailing_teams.ids:
                self.team_id = self.env.ref('isha_crm.mailing_team_emedia').id
            else:
                self.team_id = teams[0].id
        else:
            self.team_id = False

    team_id = fields.Many2one(comodel_name='isha.mailing.team', string='Team Name')


    @api.depends('team_id')
    def _compute_mailing_team_read_only(self):
        for rec in self:
            readonly = False if self.env.ref('isha_crm.mailing_team_emedia').id in self.env.user.mailing_teams.ids \
                else True
            rec.mailing_team_read_only = readonly

    mailing_team_read_only = fields.Boolean(store=False, compute=_compute_mailing_team_read_only)

    # reusable datetime field
    dynamic_datetime = fields.Datetime(string='Dynamic Datetime')
    dynamic_date = fields.Date(string='Dynamic Date')

    # resuable fields
    dynamic_field_1 = fields.Char(string='Dynamic Field 1')
    dynamic_field_2 = fields.Char(string='Dynamic Field 2')
    dynamic_field_3 = fields.Char(string='Dynamic Field 3')
    dynamic_field_4 = fields.Char(string='Dynamic Field 4')
    dynamic_field_5 = fields.Char(string='Dynamic Field 5')
    dynamic_field_6 = fields.Char(string='Dynamic Field 6')
    dynamic_field_7 = fields.Char(string='Dynamic Field 7')
    dynamic_field_8 = fields.Char(string='Dynamic Field 8')
    dynamic_field_9 = fields.Char(string='Dynamic Field 9')
    dynamic_field_10 = fields.Char(string='Dynamic Field 10')

    center_id = fields.Many2one('isha.center', string='Center')
    region_id = fields.Many2one(related='center_id.region_id',string='Region',store=True)
    # related fields from res_partner
    pgm_tag_ids = fields.Many2many(related='contact_id_fkey.pgm_tag_ids')
    category_id = fields.Many2many(related='contact_id_fkey.category_id')
    is_meditator = fields.Boolean(related='contact_id_fkey.is_meditator')

    @api.model_create_multi
    def create(self, vals_list):
        for val in vals_list:
            val['email'] = val['email'].strip() if ('email' in val and val['email']) else None
            if self._context.get('import_file', False):
                if 'list_identifier' not in val or not val['list_identifier']:
                    raise ValidationError('List Identifier cannot be empty')
                if 'ml_subscription_ids' not in val or not val['ml_subscription_ids']:
                    raise ValidationError('Subscriptions cannot be empty')
                if ('team_id' not in val or not val['team_id']) and ('center_id' not in val or not val['center_id']):
                    raise ValidationError('Either Center or Team should have a valid value')

        return super(IshaMailingContacts, self).create(vals_list)

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        context = self._context or {}
        if context.get('center_scope') and self.env.ref('isha_crm.mailing_team_emedia').id not in self.env.user.mailing_teams.ids:
            domain += [('center_id', 'in', self.env.user.centers.ids)]
        if context.get('team_scope'):
            if not self.env.user.mailing_teams.ids:
                domain += [('team_id','=', False)]
            elif self.env.ref('isha_crm.mailing_team_emedia').id not in self.env.user.mailing_teams.ids:
                domain += ['|',('team_id','=', False),('team_id', 'in', self.env.user.mailing_teams.ids)]

        return super(IshaMailingContacts, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                         orderby=orderby, lazy=lazy)

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}
        if context.get('center_scope') and self.env.ref('isha_crm.mailing_team_emedia').id not in self.env.user.mailing_teams.ids:
            args += [('center_id', 'in', self.env.user.centers.ids)]
        if context.get('team_scope'):
            if not self.env.user.mailing_teams.ids:
                args += [('team_id','=', False)]
            elif self.env.ref('isha_crm.mailing_team_emedia').id not in self.env.user.mailing_teams.ids:
                args += ['|',('team_id','=', False),('team_id', 'in', self.env.user.mailing_teams.ids)]

        return super(IshaMailingContacts, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)

    def _verification_token(self, res_id):
        """Generate a secure hash for double opt-in

        :param int res_id:
            ID of the subscription category to verify
        """
        secret = self.env["ir.config_parameter"].sudo().get_param("database.secret")
        token = (self.env.cr.dbname, self.id, int(res_id), tools.ustr(self.email))
        return hmac.new(secret.encode('utf-8'), repr(token).encode('utf-8'), hashlib.sha512).hexdigest()

    def _get_verification_url(self, res_id):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        base_url = base_url.rstrip('/')
        db = self.env.cr.dbname
        url = werkzeug.urls.url_join(
            base_url, 'mail/isha_mailing/%(mailing_contact_id)s/verify_subscription?%(params)s' % {
                'mailing_contact_id': self.id,
                'params': werkzeug.urls.url_encode({
                    'db': db,
                    'res_id': res_id,
                    'email': self.email,
                    'token': self._verification_token(res_id),
                }),
            }
        )
        return url

    def send_verificaion_email(self, res_id):
        self.env.context = dict(self.env.context)
        self.env.context.update({
            'token_url': self._get_verification_url(res_id),
            'sub_category': self.env['isha.mailing.subscriptions'].browse(int(res_id)).name
        })
        mail_template = self.env.ref('isha_crm.isha_subscription_verification_template')
        values = mail_template.generate_email(self.id, fields=None)
        msg_id = self.env['mail.mail'].sudo().create(values)
        msg_id.send(False)
        return True

    def match_n_merge(self):
        for rec in self:
            partner_id = self.env['res.partner'].sudo().create({
                'name': rec.name,
                'email': rec.email,
                'country': rec.country,
                'system_id': rec.system_id,
                'local_contact_id': rec.id,
                'low_auth_data': True
            })
            rec.contact_id_fkey = partner_id
