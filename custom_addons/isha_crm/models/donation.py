# -*- coding: utf-8 -*-

import json
from datetime import datetime
from datetime import date
from xmlrpc import client

from odoo import models, fields, api, tools
from odoo.exceptions import ValidationError
from odoo.tools import config
from ..kafka_processors import IshaVidhyaUtil
from ...isha_base.configuration import Configuration
import csv
import os
from .. import isha_base_importer


def get_group_by_set():
    return {"is_samarthak","mode_of_payment","project_name","purpose","don_receipt_date","inkind","donation_type","amount","contact_region"}


def get_non_filter_set():
    return {'active','status','project','purpose_id','center'} # ,'purpose_donation_outreach_id_fkey','purpose_donation_foundation_id_fkey','purpose_donation_education_id_fkey','purpose_donation_iii_id_fkey','purpose_donation_arogya_id_fkey'}


crm_config = isha_base_importer.Configuration('CRM')
with open(os.path.join(crm_config['CRM_ASSETS_PATH'], 'eReceipts-country-nationality-corrections.csv')) as f:
    csvReader = []
    for x in csv.reader(f):
        csvReader.append([x[0].upper(), x[1].upper()])
    correction = dict(filter(None, csvReader))


class DonationTag(models.Model):
    _name = 'isha.donation.tag'
    _description = 'Donation tags'
    _order = 'name'
    _parent_store = True

    name = fields.Char(string='Tag Name', required=True, translate=True)
    color = fields.Integer(string='Color Index')
    parent_id = fields.Many2one('isha.donation.tag', string='Parent Systags', index=True, ondelete='cascade')
    child_ids = fields.One2many('isha.donation.tag', 'parent_id', string='Child Systags')
    active = fields.Boolean(default=True, help="The active field allows you to hide the category without removing it.")
    parent_path = fields.Char(index=True)
    partner_ids = fields.Many2many('res.partner', column1='donation_tag_id', column2='partner_id', string='Partners')

    @api.constrains('parent_id')
    def _check_parent_id(self):
        if not self._check_recursion():
            raise ValidationError(_('You can not create recursive tags.'))


class DonationIshaFoundation(models.Model):
    _name = 'donation.isha.foundation'
    _description = 'Isha Foundation Donations'
    _order = "don_receipt_date desc"
    _rec_name = 'receipt_number'

    guid = fields.Char(string="Golden ID", index=True)
    local_trans_id =  fields.Char(string="Trans ID",required=False)
    local_donor_id =  fields.Char(string="Donor ID",required=False, index=True)
    donor_poc_id =  fields.Char(string="Donor POC ID",required=False)
    local_patron_id =  fields.Char(string="Patron ID",required=False, index=True)
    patron_poc_id =  fields.Char(string="Patron POC ID",required=False)
    donation_type =  fields.Char(string="Donation Type",required=False)
    mode_of_payment =  fields.Char(string="Mode of Payment",required=False)
    inkind =  fields.Char(string="Inkind",required=False)
    amount =  fields.Float(string="Amount",required=False)
    receipt_number =  fields.Char(string="Receipt No",required=False)
    don_receipt_date = fields.Date(string='Receipt Date', required=False, index=True)
    active =  fields.Integer(string="Active",required=False,default=True)
    status =  fields.Char(string="Status",required=False)
    system_id = fields.Integer(string='System ID',required=False, index=True)
    purpose_id = fields.Many2one(string='Purpose ID',comodel_name='isha.purpose.project.mapping', delegate=True)
    center = fields.Char(string="Center", required=False)
    contact_name = fields.Char(string="Contact Name", required=False)
    contact_phone = fields.Char(string="Contact Phone", required=False)
    contact_email = fields.Char(string="Contact Email", required=False)
    project = fields.Char(string="Purpose", required=False)
    #contact for isha_foundation
    contact_id_fkey = fields.Many2one(string="Contact ID",comodel_name='res.partner', auto_join=True, index=True)
    is_meditator = fields.Boolean(string='Meditator', related='contact_id_fkey.is_meditator')
    # contact_center = fields.Many2one('isha.center', related='contact_id_fkey.center_id')
    contact_region = fields.Many2one('isha.region', related='contact_id_fkey.center_id.region_id',store=True)
    is_samarthak = fields.Boolean(default=False, string='Samarthak', groups='isha_crm.group_donation_tags,isha_crm.group_sangam')

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(DonationIshaFoundation, self).fields_get(allfields, attributes=attributes)
        # not_selectable_fields = set(res.keys()) - get_group_by_set()
        # for field in not_selectable_fields:
        #     res[field]['sortable'] = False  # to hide in group by view
        for field in get_non_filter_set():
            res[field]['sortable'] = False  # to hide in group by view
            res[field]['searchable'] = False
        return res


    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        # default - do not show any results
        if len(args) == 0:
            return []
        return super(DonationIshaFoundation, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)

    def update_tags(self,gen_tags, val):
        tag_id = [27]
        tag_name = ['%ishanga%']
        for x in range(len(tag_id)):
            if tag_id[x] not in gen_tags:
                query = '''insert into res_partner_res_partner_category_rel
                        select %d,%d from donation_isha_foundation pv where
                        pv.guid = '%s' and pv.project ilike '%s'
                        ON CONFLICT (category_id, partner_id) DO NOTHING''' % (tag_id[x],val['contact_id_fkey'].id, val['guid'], tag_name[x])
                self.env.cr.execute(query)

    @api.model_create_multi
    def create(self, vals_list):
        ref = super(DonationIshaFoundation, self).create(vals_list)
        # for val in ref:
        #     if val['contact_id_fkey'].id == 2:  # temp user
        #         rec = self.env['contact.map'].search([('local_contact_id', '=', val['local_patron_id']),('system_id', '=', val['system_id'])])
        #         if len(rec) > 0:
        #             val['guid'] = rec[0]['guid']
        #             val['contact_id_fkey'] = rec[0]['contact_id_fkey'].id
        #     if val['contact_id_fkey'].id != 2:
        #         try:
        #             gen_tags = val['contact_id_fkey'].category_id.ids
        #             self.update_tags(gen_tags,val)
        #             # Has Meditator room
        #             # if 26 not in gen_tags:
        #             #     self.env.cr.execute(
        #             #         'insert into res_partner_res_partner_category_rel select 26, %d from contact_map where values(%s,%s) ON CONFLICT (partner_id, pgm_tag_ids) DO NOTHING',
        #             #         (val['contact_id_fkey'].id, 9))
        #
        #             self.env.cr.commit()
        #         except Exception as ex:
        #             tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        #             pass
        return ref

class DonationIshaOutreach(models.Model):
    _name = 'donation.isha.outreach'
    _description = 'Isha Outreach Donations'
    _order = "don_receipt_date desc"

    guid = fields.Char(string="Golden ID", index=True)
    local_trans_id =  fields.Char(string="Trans ID",required=False)
    local_donor_id =  fields.Char(string="Donor ID",required=False, index=True)
    donor_poc_id =  fields.Char(string="Donor POC ID",required=False)
    local_patron_id =  fields.Char(string="Patron ID",required=False, index=True)
    patron_poc_id =  fields.Char(string="Patron POC ID",required=False)
    donation_type =  fields.Char(string="Donation Type",required=False)
    project = fields.Char(string="Purpose",required=False)
    mode_of_payment =  fields.Char(string="Mode of Payment",required=False)
    inkind =  fields.Char(string="Inkind",required=False)
    amount =  fields.Float(string="Amount",required=False)
    receipt_number =  fields.Char(string="Receipt No",required=False)
    don_receipt_date = fields.Date(string='Receipt Date', required=False, index=True)
    active =  fields.Integer(string="Active",required=False,default=True)
    status =  fields.Char(string="Status",required=False)
    system_id = fields.Integer(string='System ID',required=False, index=True)
    purpose_id = fields.Many2one(string='Purpose ID',comodel_name='isha.purpose.project.mapping', delegate=True)
    center = fields.Char(string="Center", required=False)
    contact_name = fields.Char(string="Contact Name", required=False)
    contact_phone = fields.Char(string="Contact Phone", required=False)
    contact_email = fields.Char(string="Contact Email", required=False)

    #contact for isha_outreach
    contact_id_fkey = fields.Many2one(string="Contact ID",comodel_name='res.partner', auto_join=True, index=True)
    is_meditator = fields.Boolean(string='Meditator', related='contact_id_fkey.is_meditator')
    contact_region = fields.Many2one('isha.region', related='contact_id_fkey.center_id.region_id',store=True)
    is_samarthak = fields.Boolean(default=False, string='Samarthak', groups='isha_crm.group_donation_tags,isha_crm.group_sangam')

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(DonationIshaOutreach, self).fields_get(allfields, attributes=attributes)
        # not_selectable_fields = set(res.keys()) - get_group_by_set()
        # for field in not_selectable_fields:
        #     res[field]['sortable'] = False  # to hide in group by view
        for field in get_non_filter_set():
            res[field]['sortable'] = False  # to hide in group by view
            res[field]['searchable'] = False
        return res

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        # default - do not show any results
        if len(args) == 0:
            return []
        return super(DonationIshaOutreach, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)


class DonationIshaEducation(models.Model):
    _name = 'donation.isha.education'
    _description = 'Isha Education Donations'
    _order = "donation_date desc"

    guid = fields.Char(string="Golden ID", index=True)
    local_trans_id =  fields.Char(string="Trans ID",required=False)
    local_donor_id =  fields.Char(string="Donor ID",required=False, index=True)
    donor_poc_id =  fields.Char(string="Donor POC ID",required=False)
    local_patron_id =  fields.Char(string="Patron ID",required=False, index=True)
    patron_poc_id =  fields.Char(string="Patron POC ID",required=False)
    donation_type =  fields.Char(string="Donation Type",required=False)
    project = fields.Char(string="Purpose",required=False)
    mode_of_payment =  fields.Char(string="Mode of Payment",required=False)
    inkind = fields.Char(string="Inkind",required=False)
    inkind_remarks = fields.Char(string="Inkind Remarks")
    amount = fields.Float(string="Donation Amount",required=False)
    receipt_number =  fields.Char(string="Receipt No",required=False)
    don_receipt_date = fields.Date(string='Receipt Date', required=False, index=True)
    active =  fields.Integer(string="Active",required=False,default=True)
    status =  fields.Char(string="Status",required=False)
    system_id = fields.Integer(string='System ID',required=False, index=True)
    purpose_id = fields.Many2one(string='Purpose ID',comodel_name='isha.purpose.project.mapping', delegate=True)
    center = fields.Char(string="Center", required=False)
    contact_name = fields.Char(string="Donor Name", required=False)
    contact_phone = fields.Char(string="Phone", required=False)
    contact_mobile = fields.Char(string="Mobile")
    contact_address = fields.Char(string="Address")
    contact_city = fields.Char(string="City")
    contact_state = fields.Char(string="State")
    contact_pincode = fields.Char(string="Pincode")
    contact_country = fields.Char(string="Country")
    contact_email = fields.Char(string="Donation Email", required=False)
    ebook_number = fields.Char(string="E-Book Number")
    ereceipt_record_id = fields.Char(string="Ereceipt Record ID")
    transfer_reference = fields.Char(string="Transfer Reference")
    bank_name = fields.Char(string="Bank Name")
    bank_branch = fields.Char(string="Bank Branch")
    pan_card = fields.Char(string="PAN Card")
    dd_chq_number = fields.Char(string="DD/Cheque Number")
    dd_chq_date = fields.Date(string='DD/Cheque Date')
    payment_gateway = fields.Char(string='Payment Gateway')
    short_year = fields.Char(string='Short year')
    general_donation_amount = fields.Float(string="General")
    govt_school_adoption_prgm_amount = fields.Float(string="GSSP")
    infrastructure_requirements_amount = fields.Float(string="Infra")
    full_educational_support_qty = fields.Integer(string="FE-Count")
    full_educational_support_amount = fields.Float(string="FE-Amount")
    scholarship_qty = fields.Integer(string="Sch-Count")
    scholarship_amount = fields.Float(string="Sch-Amount")
    transport_subsidy_qty = fields.Integer(string="Transport-Count")
    transport_subsidy_amount = fields.Float(string="Transport-Amount")
    noon_meal_subsidy_qty = fields.Integer(string="NM-Count")
    noon_meal_subsidy_amount = fields.Float(string="NM-Amount")

    classroom_qty = fields.Integer(string="Classroom Qty")
    classroom_amount = fields.Float(string="Classroom amount")
    library_books_qty = fields.Integer(string="Libarary books Qty")
    library_books_amount = fields.Float(string="Libarary books amount")
    computer_qty = fields.Integer(string="Compurter & Accessiores Qty")
    computer_amount = fields.Float(string="Compurter & Accessiores amount")
    projector_qty = fields.Integer(string="Projector Qty")
    projector_amount = fields.Float(string="Projector amount")
    learning_aids_qty = fields.Integer(string="Classroom Learing Aids Qty")
    learning_aids_amount = fields.Float(string="Classroom Learing Aids amount")
    kitchen_equip_qty = fields.Integer(string="Kitchen Equipments Qty")
    kitchen_equip_amount = fields.Float(string="Kitchen Equipments amount")
    sports_equip_qty = fields.Integer(string="Sports Equipments Qty")
    sports_equip_amount = fields.Float(string="Sports Equipments amount")
    furniture_qty = fields.Integer(string="Furniture Qty")
    furniture_amount = fields.Float(string="Furniture amount")
    school_bus_qty = fields.Integer(string="School Bus Qty")
    school_bus_amount = fields.Float(string="School Bus amount")
    ro_plant_qty = fields.Integer(string="R.O. Plant Qty")
    ro_plant_amount = fields.Float(string="R.O. Plant amount")

    fes_new_qty = fields.Integer(string="Full Edu Support - New")
    fes_renewal_qty = fields.Integer(string="Full Edu Support - Renew")
    scho_new_qty = fields.Integer(string="Scholarship - New")
    scho_renewal_qty = fields.Integer(string="Scholarship - Renew")
    campaign_id = fields.Char(string='Campaign Id')
    campaign_name = fields.Char(string='Campaign Name')
    donation_date = fields.Date(string='Donation Date')
    currency = fields.Many2one(string='Currency', comodel_name='res.currency')
    exchange_rate = fields.Float(string="Exchange Rate")
    inr_amount = fields.Float(string="INR Amount")
    corpus = fields.Float(string="Corpus")
    subsidy_breakup_modified_by = fields.Char(string='Subsidy Breakup Updt by')
    comments = fields.Char(string="Comments")
    gender_pref = fields.Selection([('M', 'Male'), ('F', 'Female')], string='Gender Preference')
    single_parent_pref = fields.Boolean(string='Single Parent Pref')
    location_pref = fields.Selection([('SGP', 'SGP'), ('ERD', 'ERD'), ('NGK', 'NGK'), ('TUT', 'TUT'), ('VIL', 'VIL'),
                                      ('CUD', 'CUD'), ('VAN', 'VAN'), ('DHA', 'DHA'), ('KRR', 'KRR'), ('ARG', 'ARG')],
                                     string='Location Preference')
    orphan_pref = fields.Boolean(string='Orphan Pref')
    grade_pref = fields.Selection([('LKG', 'LKG'), ('UKG', 'UKG'), ('I', 'I'), ('II', 'II'), ('III', 'III'),
                                   ('IV', 'IV'), ('V', 'V'), ('VI', 'VI'), ('VII', 'VII'), ('VIII', 'VIII'),
                                   ('IX', 'IX'), ('X', 'X'), ('XI', 'XI'), ('XII', 'XII')], string='Grade Pref')
    platform = fields.Char(string="Platform")
    campaign_link = fields.Char(string="Campaign Link")
    fundraiser_name = fields.Char(string="Fundraiser Name")
    fr_project_id = fields.Char(string="Project Id")
    fr_project_name = fields.Char(string="Project Name")
    fr_project_link = fields.Char(string="Project Link")
    fr_converted_currency = fields.Many2one(string='Converted Currency', comodel_name='res.currency')
    fr_converted_amount = fields.Float(string="Converted Amount")
    fr_retention_amount = fields.Float(string="Retention Amount")
    fr_payable_amount = fields.Float(string="Payable Amount")
    books_of_account = fields.Char(string="Books of Account")
    iv_donor_category = fields.Char(string='Donor Category')
    consolidated_receipt_type = fields.Selection([('parent', 'Parent'), ('child', 'Child')], string='Consolidated Type')
    uc_required = fields.Selection([('uc_required', 'UC Required'), ('report_required', 'Report Required'),
                               ('not_needed', 'Not Needed')], string='UC Required')
    iv_donation = fields.Boolean(string='Isha Vidhya Donation')

    attachments = fields.Many2many(comodel_name='ir.attachment', relation='isha_edu_donation_file_attachment',
                                   column1='donation', column2='attachment', string='Attachments')
    #contact for isha_outreach
    contact_id_fkey = fields.Many2one(string="Contact ID", comodel_name='res.partner', delegate=True, auto_join=True, index=True)
    contact_region = fields.Many2one('isha.region', related='contact_id_fkey.center_id.region_id',store=True)
    is_samarthak = fields.Boolean(default=False, string='Samarthak', groups='isha_crm.group_donation_tags,isha_crm.group_sangam')

    def get_filter_set(self):
        return {
            'donation_date', 'name', 'iv_donor_category', 'amount', 'exchange_rate', 'inr_amount', 'currency',
            'contact_email', 'dd_chq_number', 'project', 'receipt_number', 'street', 'city', 'state', 'zip',
            'country_id', 'transfer_reference', 'don_receipt_date', 'payment_gateway', 'dd_chq_date', 'bank_name',
            'bank_branch', 'inkind', 'inkind_remarks', 'gender_pref', 'single_parent_pref', 'location_pref',
            'orphan_pref', 'grade_pref', 'platform', 'consolidated_receipt_type', 'center',
            'phone2', 'phone', 'general_donation_amount', 'govt_school_adoption_prgm_amount', 'ebook_number',
            'infrastructure_requirements_amount', 'full_educational_support_qty', 'full_educational_support_amount',
            'scholarship_qty', 'scholarship_amount', 'transport_subsidy_qty', 'transport_subsidy_amount',
            'noon_meal_subsidy_qty', 'noon_meal_subsidy_amount', 'corpus', 'campaign_id', 'campaign_name',
            'campaign_link', 'mode_of_payment', 'fr_project_id', 'fr_project_name', 'fr_project_link',
            'fr_converted_currency', 'fr_payable_amount', 'fes_new_qty', 'fes_renewal_qty', 'scho_new_qty',
            'scho_renewal_qty', 'uc_required', 'books_of_account', 'subsidy_breakup_modified_by',
            'fundraiser_name', 'fr_converted_amount', 'fr_retention_amount', 'comments', 'create_date'}

    def get_group_by_set(self):
        return {
            'donation_date', 'name', 'iv_donor_category', 'amount', 'exchange_rate', 'inr_amount', 'currency',
            'contact_email', 'dd_chq_number', 'project', 'receipt_number', 'street', 'city', 'state', 'zip',
            'country_id', 'transfer_reference', 'don_receipt_date', 'payment_gateway', 'dd_chq_date', 'bank_name',
            'bank_branch', 'inkind', 'inkind_remarks', 'gender_pref', 'single_parent_pref', 'location_pref',
            'orphan_pref', 'grade_pref', 'platform', 'consolidated_receipt_type',
            'phone2', 'phone', 'general_donation_amount', 'govt_school_adoption_prgm_amount', 'ebook_number',
            'infrastructure_requirements_amount', 'full_educational_support_qty', 'full_educational_support_amount',
            'scholarship_qty', 'scholarship_amount', 'transport_subsidy_qty', 'transport_subsidy_amount',
            'noon_meal_subsidy_qty', 'noon_meal_subsidy_amount', 'corpus', 'campaign_id', 'campaign_name',
            'campaign_link', 'mode_of_payment', 'fr_project_id', 'fr_project_name', 'fr_project_link',
            'fr_converted_currency', 'fr_payable_amount', 'fes_new_qty', 'fes_renewal_qty', 'scho_new_qty',
            'scho_renewal_qty', 'uc_required', 'books_of_account', 'subsidy_breakup_modified_by',
            'fundraiser_name', 'fr_converted_amount', 'fr_retention_amount', 'comments'}

    @api.model
    def check_condition_show_dialog(self, record_id, data_changed):
        breakup_sum = (data_changed['general_donation_amount'] + data_changed['govt_school_adoption_prgm_amount'] +
                   data_changed['infrastructure_requirements_amount'] + data_changed[
                       'full_educational_support_amount'] + data_changed['scholarship_amount'] + data_changed[
                       'transport_subsidy_amount'] + data_changed['noon_meal_subsidy_amount'] + data_changed['corpus'])
        if breakup_sum < data_changed['amount'] and breakup_sum >= (data_changed['amount'] - data_changed['amount'] * 3/100):
            return True
        else:
            return False

    def validate_import_mandatory_fields(self, val):
        mandatory_fiels = ['local_trans_id', 'ereceipt_record_id', 'books_of_account', 'center', 'donation_date',
                           'ebook_number', 'receipt_number', 'transfer_reference', 'don_receipt_date', 'amount',
                           'currency', 'exchange_rate', 'inr_amount', 'mode_of_payment', 'contact_name',
                           'iv_donor_category']
        for field in mandatory_fiels:
            if not val[field]:
                raise Exception(field + ' is mandatory.')

    def verify_breakup(self):
        self.write({'subsidy_breakup_modified_by': 'Verified manually - ' + self.env.user.login})

    @api.model_create_multi
    def create(self, vals_list):
        if 'import_file' in self._context or 'kafka_import' in self._context:
            return self.handle_imports(vals_list)
        else:
            return super(DonationIshaEducation, self).create(vals_list)

    def handle_imports(self, vals_list):
        return_list_ids = []
        if 'import_file' in self._context:
            for val in vals_list:
                self.validate_import_mandatory_fields(val)
                if 'anonymous' not in val['contact_name'] and not val['contact_mobile'] and not val['contact_email']:
                    raise Exception('Phone or Email is mandatory for non-anonymous contacts.')

        for val in vals_list:
            val['local_contact_id'] = val['local_trans_id'] if 'local_trans_id' in val else None
            val['name'] = val['contact_name'] if 'contact_name' in val else None
            val['phone'] = val['contact_mobile'] if 'contact_mobile' in val else None
            val['phone2'] = val['contact_phone'] if 'contact_phone' in val else None
            val['email'] = val['contact_email'] if 'contact_email' in val else None
            val['iv_email'] = val['contact_email'] if 'contact_email' in val else None
            val['street'] = val['contact_address'] if 'contact_address' in val else None
            val['city'] = val['contact_city'] if 'contact_city' in val else None
            val['state'] = val['contact_state'] if 'contact_state' in val else None
            val['zip'] = val['contact_pincode'] if 'contact_pincode' in val else None
            val['country'] = val['contact_country'] if 'contact_country' in val else None
            val['system_id'] = 47
            val['subsidy_breakup_modified_by'] = 'Source'
            val['purpose'] = val['project']
            val['entity'] = 'IshaEducation'

            if val['dd_chq_number'] and val['dd_chq_number'] != '0':
                val['transfer_reference'] = val['dd_chq_number']
            else:
                val['dd_chq_number'] = None

            if val['currency']:
                currencies = self.env['res.currency'].search([('name', '=', val['currency'])])
                if currencies:
                    val['currency'] = currencies[0].id
                # if val['currency'].upper() == 'INR':
                #     val['currency'] = self.env.ref('base.INR').id
                # elif val['currency'].upper() == 'USD':
                #     val['currency'] = self.env.ref('base.USD').id
                # elif val['currency'].upper() == 'GBP':
                #     val['currency'] = self.env.ref('base.GBP').id
            if val['fr_converted_currency']:
                currencies = self.env['res.currency'].search([('name', '=', val['fr_converted_currency'])])
                if currencies:
                    val['fr_converted_currency'] = currencies[0].id
                # if val['fr_converted_currency'] == 'INR':
                #     val['fr_converted_currency'] = self.env.ref('base.INR').id
                # elif val['fr_converted_currency'] == 'USD':
                #     val['fr_converted_currency'] = self.env.ref('base.USD').id
                # elif val['fr_converted_currency'].upper() == 'GBP':
                #     val['fr_converted_currency'] = self.env.ref('base.GBP').id
            else:
                val['fr_converted_currency'] = None

            if 'kafka_import' in self._context:
                if val['contact_country'] and val['contact_country'].upper() in correction and\
                        correction[val['contact_country'].upper()]:
                    val['contact_country'] = correction[val['contact_country'].upper()]

                if 'amount' in val and val['amount']:
                    val['amount'] = val['amount'].strip().replace(',', '')
                if 'general_donation_amount' in val and val['general_donation_amount']:
                    val['general_donation_amount'] = val['general_donation_amount'].strip().replace(',', '')
                if 'govt_school_adoption_prgm_amount' in val and val['govt_school_adoption_prgm_amount']:
                    val['govt_school_adoption_prgm_amount'] = val[
                        'govt_school_adoption_prgm_amount'].strip().replace(',', '')
                if 'infrastructure_requirements_amount' in val and val['infrastructure_requirements_amount']:
                    val['infrastructure_requirements_amount'] = val[
                        'infrastructure_requirements_amount'].strip().replace(',', '')
                if 'full_educational_support_amount' in val and val['full_educational_support_amount']:
                    val['full_educational_support_amount'] = val['full_educational_support_amount'].strip().replace(
                        ',', '')
                if 'scholarship_amount' in val and val['scholarship_amount']:
                    val['scholarship_amount'] = val['scholarship_amount'].strip().replace(',', '')
                if 'transport_subsidy_amount' in val and val['transport_subsidy_amount']:
                    val['transport_subsidy_amount'] = val['transport_subsidy_amount'].strip().replace(',', '')
                if 'noon_meal_subsidy_amount' in val and val['noon_meal_subsidy_amount']:
                    val['noon_meal_subsidy_amount'] = val['noon_meal_subsidy_amount'].strip().replace(',', '')
                if 'fr_converted_amount' in val and val['fr_converted_amount']:
                    val['fr_converted_amount'] = val['fr_converted_amount'].strip().replace(',', '')
                if 'fr_retention_amount' in val and val['fr_retention_amount']:
                    val['fr_retention_amount'] = val['fr_retention_amount'].strip().replace(',', '')
                if 'fr_payable_amount' in val and val['fr_payable_amount']:
                    val['fr_payable_amount'] = val['fr_payable_amount'].strip().replace(',', '')

                if val['donation_date']:
                    try:
                        val['donation_date'] = datetime.strptime(val['donation_date'], '%m/%d/%Y')
                    except:
                        val['donation_date'] = datetime.strptime(val['donation_date'], '%m/%d/%y')
                else:
                    val['donation_date'] = None

                if val['don_receipt_date']:
                    try:
                        val['don_receipt_date'] = datetime.strptime(val['don_receipt_date'], '%m/%d/%Y')
                    except:
                        val['don_receipt_date'] = datetime.strptime(val['don_receipt_date'], '%m/%d/%y')
                else:
                    val['don_receipt_date'] = val['donation_date']

                if not val['donation_date']:
                    val['donation_date'] = val['don_receipt_date']

                if val['dd_chq_date']:
                    try:
                        val['dd_chq_date'] = datetime.strptime(val['dd_chq_date'], '%m/%d/%Y')
                    except:
                        val['dd_chq_date'] = datetime.strptime(val['dd_chq_date'], '%m/%d/%y')
                else:
                    val['dd_chq_date'] = None

            if 'import_file' in self._context:
                if ('receipt_number' not in val or not val['receipt_number']) and ('transfer_reference' not in val or
                                                                                   not val['transfer_reference']):
                    raise Exception('Either receipt_number or transfer_reference must be present.')
                donation_date = None
                if val['donation_date']:
                    # donation_date = datetime.strptime(val['donation_date'], '%Y-%m-%d')
                    donation_date = val['donation_date']
                if donation_date:
                    val['short_year'] = IshaVidhyaUtil.get_short_financial_year(donation_date)
                    if donation_date > date.today():
                        raise Exception('Donation date cannot be a future date')
            val['iv_last_donated_date'] = val['donation_date']
            val['last_txn_date'] = val['donation_date']
            val['charitable_last_txn_date'] = val['donation_date']
            val['has_charitable_txn'] = True

            if 'local_trans_id' in val and val['local_trans_id']:

            # if ('receipt_number' in val and val['receipt_number'] and 'transfer_reference' in val and val['transfer_reference'] and
            #         'dd_chq_number' in val and val['dd_chq_number'] and 'amount' in val and val['amount']):
            #     duplicates = self.search([('receipt_number', '=', val['receipt_number']),
            #                               ('transfer_reference', '=', val['transfer_reference']),
            #                               ('dd_chq_number', '=', val['dd_chq_number']), ('amount', '=', val['amount'])])
                duplicates = self.search([('system_id', '=', 47), ('local_trans_id', '=', val['local_trans_id'])])
                if duplicates:
                    raise Exception('Donation record already exists with the same local_trans_id: ' + val['local_trans_id'])
                    # duplicates[0].write(val)
                    # donation_rec = duplicates[0]
                    # contact_id = donation_rec.contact_id_fkey.id
                    # return_list_ids.append(duplicates[0].id)
                else:
                    donation_rec = super(DonationIshaEducation, self).create(vals_list)
                    return_list_ids.append(donation_rec.id)
                    contact_id = donation_rec.contact_id_fkey.id
                if 'import_file' in self._context:
                    if ('receipt_number' not in val or not val['receipt_number']) and (
                            'transfer_reference' not in val or
                            not val['transfer_reference']):
                        raise Exception('Either receipt_number or transfer_reference must be present.')

                    fes_qty = int(val['full_educational_support_qty']) if val['full_educational_support_qty'] else 0
                    scho_qty = int(val['scholarship_qty']) if val['scholarship_qty'] else 0
                    donation_rec.update(IshaVidhyaUtil.get_new_renewal_info(self.env, donation_date,
                                                                            contact_id, fes_qty, scho_qty,
                                                                            val['transfer_reference'],
                                                                            val['receipt_number']))
            else:
                raise Exception('local_trans_id is not provided.')

        return self.env['donation.isha.education'].browse(return_list_ids)

    def write(self, values):
        if self.env.user.login != 'admin' and self.env.user.login != '__system__':
            if ('general_donation_amount' in values or 'govt_school_adoption_prgm_amount' in values or
                'infrastructure_requirements_amount' in values or 'full_educational_support_qty' in values or
                'full_educational_support_amount' in values or 'scholarship_qty' in values or
                'scholarship_amount' in values or 'transport_subsidy_qty' in values or
                'transport_subsidy_amount' in values or 'noon_meal_subsidy_qty' in values or
                'noon_meal_subsidy_amount' in values or 'corpus' in values):
                values['subsidy_breakup_modified_by'] = self.env.user.login

            general_donation_amount = values['general_donation_amount'] if 'general_donation_amount' in values else self.general_donation_amount
            govt_school_adoption_prgm_amount = values['govt_school_adoption_prgm_amount'] if 'govt_school_adoption_prgm_amount' in values else self.govt_school_adoption_prgm_amount
            infrastructure_requirements_amount = values['infrastructure_requirements_amount'] if 'infrastructure_requirements_amount' in values else self.infrastructure_requirements_amount
            full_educational_support_amount = values['full_educational_support_amount'] if 'full_educational_support_amount' in values else self.full_educational_support_amount
            scholarship_amount = values['scholarship_amount'] if 'scholarship_amount' in values else self.scholarship_amount
            transport_subsidy_amount = values['transport_subsidy_amount'] if 'transport_subsidy_amount' in values else self.transport_subsidy_amount
            noon_meal_subsidy_amount = values['noon_meal_subsidy_amount'] if 'noon_meal_subsidy_amount' in values else self.noon_meal_subsidy_amount
            corpus = values['corpus'] if 'corpus' in values else self.corpus
            amount = values['amount'] if 'amount' in values else self.amount

            breakup_sum = round(general_donation_amount + govt_school_adoption_prgm_amount + \
                          infrastructure_requirements_amount + full_educational_support_amount + scholarship_amount + \
                          transport_subsidy_amount + noon_meal_subsidy_amount + corpus, 2)
            if breakup_sum > amount or breakup_sum < (amount - amount * 3 / 100):
                raise ValidationError('Subsidy breakup is not within 3% deviation from the donation amount')

            classroom_amount = values['classroom_amount'] if 'classroom_amount' in values else self.classroom_amount
            library_books_amount = values['library_books_amount'] if 'library_books_amount' in values else self.library_books_amount
            computer_amount = values['computer_amount'] if 'computer_amount' in values else self.computer_amount
            projector_amount = values['projector_amount'] if 'projector_amount' in values else self.projector_amount
            learning_aids_amount = values['learning_aids_amount'] if 'learning_aids_amount' in values else self.learning_aids_amount
            kitchen_equip_amount = values['kitchen_equip_amount'] if 'kitchen_equip_amount' in values else self.kitchen_equip_amount
            sports_equip_amount = values['sports_equip_amount'] if 'sports_equip_amount' in values else self.sports_equip_amount
            furniture_amount = values['furniture_amount'] if 'furniture_amount' in values else self.furniture_amount
            school_bus_amount = values['school_bus_amount'] if 'school_bus_amount' in values else self.school_bus_amount
            ro_plant_amount = values['ro_plant_amount'] if 'ro_plant_amount' in values else self.ro_plant_amount

            if ((classroom_amount + library_books_amount + computer_amount + projector_amount + learning_aids_amount +
                kitchen_equip_amount + sports_equip_amount + furniture_amount + school_bus_amount + ro_plant_amount)
                    != infrastructure_requirements_amount):
                raise ValidationError('Infrastructure total amount is not matching the breakup amounts')

        return super(DonationIshaEducation, self).write(values)

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(DonationIshaEducation, self).fields_get(allfields, attributes=attributes)
        not_selectable_fields = set(res.keys()) - self.get_group_by_set()
        for field in not_selectable_fields:
            res[field]['sortable'] = False  # to hide in group by view
        non_searchable_fields = set(res.keys()) - self.get_filter_set()
        for field in non_searchable_fields:
            res[field]['searchable'] = False
        return res

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        custom_domains = self._get_custom_domains()
        domain = domain + custom_domains
        return super(DonationIshaEducation, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                    orderby=orderby, lazy=lazy)

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}

        # default - do not show any results
        if len(args) == 0 and context.get('no_default_search'):
            return []
        custom_domains = self._get_custom_domains()
        args = args + custom_domains
        return super(DonationIshaEducation, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)

    def _get_custom_domains(self):
        args = []
        context = self._context or {}
        if context.get('filter_current_fy_donations'):
            cur_time = datetime.now()
            if cur_time.month > 3:
                start_date = cur_time.strftime('%Y-04-01')
            else:
                fy = IshaVidhyaUtil._get_financial_year(cur_time.year - 1, cur_time.month)
                start_date = fy['from_date']
            args += [('don_receipt_date', '>=', start_date)]
        if context.get('filter_previous_month_donations'):
            cur_time = datetime.now()
            start_date = datetime(cur_time.year, cur_time.month - 1, 1)
            end_date = datetime(cur_time.year, cur_time.month, 1)
            args += [('don_receipt_date', '>=', start_date), ('don_receipt_date', '<', end_date)]
        return args


class DonationTKBP(models.Model):
    _name = 'donation.tkbp'
    _description = 'Thenkailaya Bakthi Peravai Donations'
    _order = "don_receipt_date desc"
    _rec_name = 'project_name'

    guid = fields.Char(string="Golden ID", index=True)
    local_trans_id =  fields.Char(string="Trans ID",required=False)
    local_donor_id =  fields.Char(string="Donor ID",required=False, index=True)
    donor_poc_id =  fields.Char(string="Donor POC ID",required=False)
    local_patron_id =  fields.Char(string="Patron ID",required=False, index=True)
    patron_poc_id =  fields.Char(string="Patron POC ID",required=False)
    donation_type =  fields.Char(string="Donation Type",required=False)
    project = fields.Char(string="Purpose",required=False)
    mode_of_payment =  fields.Char(string="Mode of Payment",required=False)
    inkind =  fields.Char(string="Inkind",required=False)
    amount =  fields.Float(string="Amount",required=False)
    receipt_number =  fields.Char(string="Receipt No",required=False)
    don_receipt_date = fields.Date(string='Receipt Date', required=False, index=True)
    active =  fields.Integer(string="Active",required=False,default=True)
    status =  fields.Char(string="Status",required=False)
    system_id = fields.Integer(string='System ID',required=False, index=True)
    purpose_id = fields.Many2one(string='Purpose ID',comodel_name='isha.purpose.project.mapping', delegate=True)
    center = fields.Char(string="Center", required=False)
    contact_name = fields.Char(string="Contact Name", required=False)
    contact_phone = fields.Char(string="Contact Phone", required=False)
    contact_email = fields.Char(string="Contact Email", required=False)
    ebook_number = fields.Char(string="eBook Number")
    books_of_account = fields.Char(string="Books of Account")

    #contact for isha_outreach
    contact_id_fkey = fields.Many2one(string="Contact ID",comodel_name='res.partner', auto_join=True, index=True)
    contact_region = fields.Many2one('isha.region', related='contact_id_fkey.center_id.region_id',store=True)
    is_samarthak = fields.Boolean(default=False, string='Samarthak', groups='isha_crm.group_donation_tags,isha_crm.group_sangam')

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(DonationTKBP, self).fields_get(allfields, attributes=attributes)
        # not_selectable_fields = set(res.keys()) - get_group_by_set()
        # for field in not_selectable_fields:
        #     res[field]['sortable'] = False  # to hide in group by view
        for field in get_non_filter_set():
            res[field]['sortable'] = False  # to hide in group by view
            res[field]['searchable'] = False
        return res

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}

        # default - do not show any results
        if len(args) == 0 and context.get('no_default_search'):
            return []
        return super(DonationTKBP, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)


class DonationShriYogini(models.Model):
    _name = 'donation.shri.yogini'
    _description = 'Shri Yogini Trust Donations'
    _order = "don_receipt_date desc"
    _rec_name = 'project_name'

    guid = fields.Char(string="Golden ID", index=True)
    local_trans_id =  fields.Char(string="Trans ID",required=False)
    local_donor_id =  fields.Char(string="Donor ID",required=False, index=True)
    donor_poc_id =  fields.Char(string="Donor POC ID",required=False)
    local_patron_id =  fields.Char(string="Patron ID",required=False, index=True)
    patron_poc_id =  fields.Char(string="Patron POC ID",required=False)
    donation_type =  fields.Char(string="Donation Type",required=False)
    project = fields.Char(string="Purpose",required=False)
    mode_of_payment =  fields.Char(string="Mode of Payment",required=False)
    inkind =  fields.Char(string="Inkind",required=False)
    amount =  fields.Float(string="Amount",required=False)
    receipt_number =  fields.Char(string="Receipt No",required=False)
    don_receipt_date = fields.Date(string='Receipt Date', required=False, index=True)
    active =  fields.Integer(string="Active",required=False,default=True)
    status =  fields.Char(string="Status",required=False)
    system_id = fields.Integer(string='System ID',required=False, index=True)
    purpose_id = fields.Many2one(string='Purpose ID',comodel_name='isha.purpose.project.mapping', delegate=True)
    center = fields.Char(string="Center", required=False)
    contact_name = fields.Char(string="Contact Name", required=False)
    contact_phone = fields.Char(string="Contact Phone", required=False)
    contact_email = fields.Char(string="Contact Email", required=False)
    ebook_number = fields.Char(string="eBook Number")
    books_of_account = fields.Char(string="Books of Account")

    #contact for isha_outreach
    contact_id_fkey = fields.Many2one(string="Contact ID",comodel_name='res.partner', auto_join=True, index=True)
    contact_region = fields.Many2one('isha.region', related='contact_id_fkey.center_id.region_id',store=True)
    is_samarthak = fields.Boolean(default=False, string='Samarthak', groups='isha_crm.group_donation_tags,isha_crm.group_sangam')

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(DonationShriYogini, self).fields_get(allfields, attributes=attributes)
        # not_selectable_fields = set(res.keys()) - get_group_by_set()
        # for field in not_selectable_fields:
        #     res[field]['sortable'] = False  # to hide in group by view
        for field in get_non_filter_set():
            res[field]['sortable'] = False  # to hide in group by view
            res[field]['searchable'] = False
        return res

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}

        # default - do not show any results
        if len(args) == 0 and context.get('no_default_search'):
            return []
        return super(DonationShriYogini, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)


class DonationIshaUtsav(models.Model):
    _name = 'donation.isha.utsav'
    _description = 'Isha Utsav Donations'
    _order = "don_receipt_date desc"
    _rec_name = 'project_name'

    guid = fields.Char(string="Golden ID", index=True)
    local_trans_id = fields.Char(string="Trans ID",required=False)
    local_donor_id = fields.Char(string="Donor ID",required=False, index=True)
    donor_poc_id = fields.Char(string="Donor POC ID",required=False)
    local_patron_id = fields.Char(string="Patron ID",required=False, index=True)
    patron_poc_id = fields.Char(string="Patron POC ID",required=False)
    donation_type = fields.Char(string="Donation Type",required=False)
    project = fields.Char(string="Purpose",required=False)
    mode_of_payment = fields.Char(string="Mode of Payment",required=False)
    inkind = fields.Char(string="Inkind",required=False)
    amount = fields.Float(string="Amount",required=False)
    receipt_number = fields.Char(string="Receipt No",required=False)
    don_receipt_date = fields.Date(string='Receipt Date', required=False, index=True)
    active = fields.Integer(string="Active",required=False,default=True)
    status = fields.Char(string="Status",required=False)
    system_id = fields.Integer(string='System ID',required=False, index=True)
    purpose_id = fields.Many2one(string='Purpose ID',comodel_name='isha.purpose.project.mapping', delegate=True)
    center = fields.Char(string="Center", required=False)
    contact_name = fields.Char(string="Contact Name", required=False)
    contact_phone = fields.Char(string="Contact Phone", required=False)
    contact_email = fields.Char(string="Contact Email", required=False)
    ebook_number = fields.Char(string="eBook Number")
    books_of_account = fields.Char(string="Books of Account")

    #contact for isha_outreach
    contact_id_fkey = fields.Many2one(string="Contact ID",comodel_name='res.partner', auto_join=True, index=True)
    contact_region = fields.Many2one('isha.region', related='contact_id_fkey.center_id.region_id',store=True)
    is_samarthak = fields.Boolean(default=False, string='Samarthak', groups='isha_crm.group_donation_tags,isha_crm.group_sangam')

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(DonationIshaUtsav, self).fields_get(allfields, attributes=attributes)
        # not_selectable_fields = set(res.keys()) - get_group_by_set()
        # for field in not_selectable_fields:
        #     res[field]['sortable'] = False  # to hide in group by view
        for field in get_non_filter_set():
            res[field]['sortable'] = False  # to hide in group by view
            res[field]['searchable'] = False
        return res

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}

        # default - do not show any results
        if len(args) == 0 and context.get('no_default_search'):
            return []
        return super(DonationIshaUtsav, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)


class DonationSacredWalks(models.Model):
    _name = 'donation.sacred.walks'
    _description = 'Sacred Walks Donations'
    _order = "don_receipt_date desc"
    _rec_name = 'project_name'

    guid = fields.Char(string="Golden ID", index=True)
    local_trans_id = fields.Char(string="Trans ID",required=False)
    local_donor_id = fields.Char(string="Donor ID",required=False, index=True)
    donor_poc_id = fields.Char(string="Donor POC ID",required=False)
    local_patron_id = fields.Char(string="Patron ID",required=False, index=True)
    patron_poc_id = fields.Char(string="Patron POC ID",required=False)
    donation_type = fields.Char(string="Donation Type",required=False)
    project = fields.Char(string="Purpose",required=False)
    mode_of_payment = fields.Char(string="Mode of Payment",required=False)
    inkind = fields.Char(string="Inkind",required=False)
    amount = fields.Float(string="Amount",required=False)
    receipt_number = fields.Char(string="Receipt No",required=False)
    don_receipt_date = fields.Date(string='Receipt Date', required=False, index=True)
    active = fields.Integer(string="Active",required=False,default=True)
    status = fields.Char(string="Status",required=False)
    system_id = fields.Integer(string='System ID',required=False, index=True)
    purpose_id = fields.Many2one(string='Purpose ID',comodel_name='isha.purpose.project.mapping', delegate=True)
    center = fields.Char(string="Center", required=False)
    contact_name = fields.Char(string="Contact Name", required=False)
    contact_phone = fields.Char(string="Contact Phone", required=False)
    contact_email = fields.Char(string="Contact Email", required=False)
    ebook_number = fields.Char(string="eBook Number")
    books_of_account = fields.Char(string="Books of Account")

    #contact for isha_outreach
    contact_id_fkey = fields.Many2one(string="Contact ID",comodel_name='res.partner', auto_join=True, index=True)
    contact_region = fields.Many2one('isha.region', related='contact_id_fkey.center_id.region_id',store=True)
    is_samarthak = fields.Boolean(default=False, string='Samarthak', groups='isha_crm.group_donation_tags,isha_crm.group_sangam')

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(DonationSacredWalks, self).fields_get(allfields, attributes=attributes)
        # not_selectable_fields = set(res.keys()) - get_group_by_set()
        # for field in not_selectable_fields:
        #     res[field]['sortable'] = False  # to hide in group by view
        for field in get_non_filter_set():
            res[field]['sortable'] = False  # to hide in group by view
            res[field]['searchable'] = False
        return res

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}

        # default - do not show any results
        if len(args) == 0 and context.get('no_default_search'):
            return []
        return super(DonationSacredWalks, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)


class DonationIshaArogya(models.Model):
    _name = 'donation.isha.arogya'
    _description = 'Isha Arogya Donations'
    _order = "don_receipt_date desc"

    guid = fields.Char(string="Golden ID", index=True)
    local_trans_id =  fields.Char(string="Trans ID",required=False)
    local_donor_id =  fields.Char(string="Donor ID",required=False, index=True)
    donor_poc_id =  fields.Char(string="Donor POC ID",required=False)
    local_patron_id =  fields.Char(string="Patron ID",required=False, index=True)
    patron_poc_id =  fields.Char(string="Patron POC ID",required=False)
    donation_type =  fields.Char(string="Donation Type",required=False)
    project = fields.Char(string="Purpose",required=False)
    mode_of_payment =  fields.Char(string="Mode of Payment",required=False)
    inkind =  fields.Char(string="Inkind",required=False)
    amount =  fields.Float(string="Amount",required=False)
    receipt_number =  fields.Char(string="Receipt No",required=False)
    don_receipt_date = fields.Date(string='Receipt Date', required=False, index=True)
    active =  fields.Integer(string="Active",required=False,default=True)
    status =  fields.Char(string="Status",required=False)
    system_id = fields.Integer(string='System ID',required=False, index=True)
    purpose_id = fields.Many2one(string='Purpose ID',comodel_name='isha.purpose.project.mapping', delegate=True)
    center = fields.Char(string="Center", required=False)
    contact_name = fields.Char(string="Contact Name", required=False)
    contact_phone = fields.Char(string="Contact Phone", required=False)
    contact_email = fields.Char(string="Contact Email", required=False)

    #contact for isha_outreach
    contact_id_fkey = fields.Many2one(string="Contact ID",comodel_name='res.partner', auto_join=True, index=True)
    is_meditator = fields.Boolean(String='Meditator', related='contact_id_fkey.is_meditator')
    contact_region = fields.Many2one('isha.region', related='contact_id_fkey.center_id.region_id',store=True)
    is_samarthak = fields.Boolean(default=False, string='Samarthak', groups='isha_crm.group_donation_tags,isha_crm.group_sangam')

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(DonationIshaArogya, self).fields_get(allfields, attributes=attributes)
        # not_selectable_fields = set(res.keys()) - get_group_by_set()
        # for field in not_selectable_fields:
        #     res[field]['sortable'] = False  # to hide in group by view
        for field in get_non_filter_set():
            res[field]['sortable'] = False  # to hide in group by view
            res[field]['searchable'] = False
        return res

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        # default - do not show any results
        if len(args) == 0:
            return []
        return super(DonationIshaArogya, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)


class DonationIshaIII(models.Model):
    _name = 'donation.isha.iii'
    _description = 'Isha III Donations'
    _order = "don_receipt_date desc"

    guid = fields.Char(string="Golden ID", index=True)
    local_trans_id = fields.Char(string="Trans ID", required=False)
    local_donor_id = fields.Char(string="Donor ID", required=False, index=True)
    donor_poc_id = fields.Char(string="Donor POC ID", required=False)
    local_patron_id = fields.Char(string="Patron ID", required=False, index=True)
    patron_poc_id = fields.Char(string="Patron POC ID", required=False)
    donation_type = fields.Char(string="Donation Type", required=False)
    project = fields.Char(string="Purpose", required=False)
    mode_of_payment = fields.Char(string="Mode of Payment", required=False)
    inkind = fields.Char(string="Inkind", required=False)
    amount = fields.Float(string="Amount", required=False)
    receipt_number = fields.Char(string="Receipt No", required=False)
    don_receipt_date = fields.Date(string='Receipt Date', required=False, index=True)
    active = fields.Integer(string="Active", required=False,default=True)
    status = fields.Char(string="Status", required=False)
    system_id = fields.Integer(string='System ID', required=False, index=True)
    purpose_id = fields.Many2one(string='Purpose ID',comodel_name='isha.purpose.project.mapping', delegate=True)
    center = fields.Char(string="Center", required=False)
    contact_name = fields.Char(string="Contact Name", required=False)
    contact_phone = fields.Char(string="Contact Phone", required=False)
    contact_email = fields.Char(string="Contact Email", required=False)

    # contact for isha_outreach
    contact_id_fkey = fields.Many2one(string="Contact ID", comodel_name='res.partner', auto_join=True, index=True)
    is_meditator = fields.Boolean(String='Meditator', related='contact_id_fkey.is_meditator')
    contact_region = fields.Many2one('isha.region', related='contact_id_fkey.center_id.region_id',store=True)
    is_samarthak = fields.Boolean(default=False, string='Samarthak', groups='isha_crm.group_donation_tags,isha_crm.group_sangam')

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(DonationIshaIII, self).fields_get(allfields, attributes=attributes)
        # not_selectable_fields = set(res.keys()) - get_group_by_set()
        # for field in not_selectable_fields:
        #     res[field]['sortable'] = False  # to hide in group by view
        for field in get_non_filter_set():
            res[field]['sortable'] = False  # to hide in group by view
            res[field]['searchable'] = False
        return res
    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        # default - do not show any results
        if len(args) == 0:
            return []
        return super(DonationIshaIII, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)


class IshaPurposeProjectMapping(models.Model):
    _name = 'isha.purpose.project.mapping'
    _description = 'Purpose to Project Mapping'
    _rec_name = 'id'

    purpose =  fields.Char(string="Purpose Master",required=True, index=True)
    project_name =  fields.Char(string="Project",required=False)
    entity =  fields.Char(string="Entity",required=False)
    system_id = fields.Integer(string='System ID',required=False)
    active = fields.Boolean(string='active', default=True)

    @api.model_create_multi
    def create(self, vals_list):
        if 'import_file' in self._context or 'kafka_import' in self._context:
            return_list_ids = []
            for val in vals_list:
                # handle import of donation records by just returning the matching project or else the dummy one.
                # if val['project_name'] == 'dummy':
                duplicates = self.search([('purpose', '=', val['purpose']), ('entity', '=', val['entity'])])
                if duplicates:
                    return_list_ids.append(duplicates[0].id)
                else:
                    return_list_ids.append(self.env.ref('isha_crm.isha_purpose_project_mapping_dummy').id)
            return self.browse(return_list_ids)
        else:
            return super(IshaPurposeProjectMapping, self).create(vals_list)


    # purpose_donation_outreach_id_fkey = fields.One2many('donation.isha.outreach', 'purpose_id')
    # purpose_donation_foundation_id_fkey = fields.One2many('donation.isha.foundation', 'purpose_id')
    # purpose_donation_education_id_fkey = fields.One2many('donation.isha.education', 'purpose_id')
    # purpose_donation_iii_id_fkey = fields.One2many('donation.isha.iii', 'purpose_id')
    # purpose_donation_arogya_id_fkey = fields.One2many('donation.isha.arogya', 'purpose_id')


class DonationsView(models.Model):
    _auto = False
    _name = 'donations.view.orm'
    entity_id = fields.Integer(string='Entity ID')
    local_trans_id = fields.Char(string='Local Trans ID')
    local_donor_id = fields.Char(string='Local Donor IO')
    donor_poc_id = fields.Char(string='Donor Poc IO')
    local_patron_id = fields.Char(string='Local Patron IO')
    status = fields.Char(string='Status')
    patron_poc_id = fields.Char(string='Patron Poc Id')
    donation_type = fields.Char(string='Donation Type')
    txn_purpose = fields.Char(string='Txn Purpose')
    purpose = fields.Char(string='Mapped Purpose')
    project_name = fields.Char(string='Project Name')
    mode_of_payment = fields.Char(string='Mode Of Payment')
    inkind = fields.Char(string='Inkind')
    amount = fields.Float(string='Amount')
    receipt_number = fields.Char(string='Receipt Number')
    don_receipt_date = fields.Date(string='Receipt Date')
    active = fields.Integer(string='active')
    system_id = fields.Integer(string='System Id')
    contact_id_fkey = fields.Many2one('res.partner',string="Contact")
    is_meditator = fields.Boolean(String='Meditator')
    purpose_id = fields.Many2one(string='Purpose ID',comodel_name='isha.purpose.project.mapping')
    create_date = fields.Datetime(string='Create Date')
    entity_name = fields.Char(string='Entity Name')
    is_samarthak = fields.Boolean(string='Is Samarthak')

    def init(self):
        tools.drop_view_if_exists(self._cr, 'donations_view_orm')
        # This query assumes that each txn tables ids are within 1000000000
        self._cr.execute(
        """
CREATE OR REPLACE VIEW public.donations_view_orm
 AS
select * from (
 SELECT 
    (donation_isha_foundation.id::bigint)::bigint as id,
    donation_isha_foundation.id as entity_id,
    donation_isha_foundation.local_trans_id,
    donation_isha_foundation.local_donor_id,
    donation_isha_foundation.donor_poc_id,
    donation_isha_foundation.local_patron_id,
    donation_isha_foundation.status,
    donation_isha_foundation.patron_poc_id,
    donation_isha_foundation.donation_type,
    donation_isha_foundation.project as txn_purpose,
    donation_isha_foundation.mode_of_payment,
    donation_isha_foundation.inkind,
    donation_isha_foundation.amount,
    donation_isha_foundation.receipt_number,
    donation_isha_foundation.don_receipt_date,
    donation_isha_foundation.active,
    donation_isha_foundation.system_id,
    donation_isha_foundation.contact_id_fkey,
    donation_isha_foundation.purpose_id,
	mapp.purpose,
	mapp.project_name,
    donation_isha_foundation.create_date,
    'IshaFoundation'::text AS entity_name,
    donation_isha_foundation.is_samarthak,
    rp.is_meditator As is_meditator
   FROM donation_isha_foundation, isha_purpose_project_mapping  mapp, res_partner rp 
   where 
	mapp.id=donation_isha_foundation.purpose_id and rp.id = donation_isha_foundation.contact_id_fkey
UNION
 SELECT 
    (donation_isha_education.id::bigint+1000000000)::bigint as id,
    donation_isha_education.id as entity_id,
    donation_isha_education.local_trans_id,
    donation_isha_education.local_donor_id,
    donation_isha_education.donor_poc_id,
    donation_isha_education.local_patron_id,
    donation_isha_education.status,
    donation_isha_education.patron_poc_id,
    donation_isha_education.donation_type,
    donation_isha_education.project as txn_purpose,
    donation_isha_education.mode_of_payment,
    donation_isha_education.inkind,
    donation_isha_education.amount,
    donation_isha_education.receipt_number,
    donation_isha_education.don_receipt_date,
    donation_isha_education.active,
    donation_isha_education.system_id,
    donation_isha_education.contact_id_fkey,
    donation_isha_education.purpose_id,
	mapp.purpose,
	mapp.project_name,
    donation_isha_education.create_date,
    'IshaEducation'::text AS entity_name,
    donation_isha_education.is_samarthak,
    rp.is_meditator As is_meditator
   FROM donation_isha_education, isha_purpose_project_mapping  mapp, res_partner rp
   where 
	mapp.id=donation_isha_education.purpose_id and rp.id = donation_isha_education.contact_id_fkey
UNION
 SELECT
    (donation_isha_arogya.id::bigint+2000000000)::bigint as id,  
    donation_isha_arogya.id as entity_id,
    donation_isha_arogya.local_trans_id,
    donation_isha_arogya.local_donor_id,
    donation_isha_arogya.donor_poc_id,
    donation_isha_arogya.local_patron_id,
    donation_isha_arogya.status,
    donation_isha_arogya.patron_poc_id,
    donation_isha_arogya.donation_type,
    donation_isha_arogya.project as txn_purpose,
    donation_isha_arogya.mode_of_payment,
    donation_isha_arogya.inkind,
    donation_isha_arogya.amount,
    donation_isha_arogya.receipt_number,
    donation_isha_arogya.don_receipt_date,
    donation_isha_arogya.active,
    donation_isha_arogya.system_id,
    donation_isha_arogya.contact_id_fkey,
    donation_isha_arogya.purpose_id,
	mapp.purpose,
	mapp.project_name,
    donation_isha_arogya.create_date,
    'IshaArogya'::text AS entity_name,
    donation_isha_arogya.is_samarthak,
    rp.is_meditator As is_meditator
   FROM donation_isha_arogya, isha_purpose_project_mapping  mapp, res_partner rp
   where 
	mapp.id=donation_isha_arogya.purpose_id and rp.id = donation_isha_arogya.contact_id_fkey
UNION
 SELECT
    (donation_isha_iii.id::bigint+3000000000)::bigint as id,   
    donation_isha_iii.id as entity_id,
    donation_isha_iii.local_trans_id,
    donation_isha_iii.local_donor_id,
    donation_isha_iii.donor_poc_id,
    donation_isha_iii.local_patron_id,
    donation_isha_iii.status,
    donation_isha_iii.patron_poc_id,
    donation_isha_iii.donation_type,
    donation_isha_iii.project as txn_purpose,
    donation_isha_iii.mode_of_payment,
    donation_isha_iii.inkind,
    donation_isha_iii.amount,
    donation_isha_iii.receipt_number,
    donation_isha_iii.don_receipt_date,
    donation_isha_iii.active,
    donation_isha_iii.system_id,
    donation_isha_iii.contact_id_fkey,
    donation_isha_iii.purpose_id,
	mapp.purpose,
	mapp.project_name,
    donation_isha_iii.create_date,
    'IIIS'::text AS entity_name,
    donation_isha_iii.is_samarthak,
    rp.is_meditator As is_meditator
   FROM donation_isha_iii, isha_purpose_project_mapping  mapp, res_partner rp
   where 
	mapp.id=donation_isha_iii.purpose_id and rp.id = donation_isha_iii.contact_id_fkey
UNION
 SELECT 
    (donation_isha_outreach.id::bigint+4000000000)::bigint as id,   
    donation_isha_outreach.id as entity_id,
    donation_isha_outreach.local_trans_id,
    donation_isha_outreach.local_donor_id,
    donation_isha_outreach.donor_poc_id,
    donation_isha_outreach.local_patron_id,
    donation_isha_outreach.status,
    donation_isha_outreach.patron_poc_id,
    donation_isha_outreach.donation_type,
    donation_isha_outreach.project as txn_purpose,
    donation_isha_outreach.mode_of_payment,
    donation_isha_outreach.inkind,
    donation_isha_outreach.amount,
    donation_isha_outreach.receipt_number,
    donation_isha_outreach.don_receipt_date,
    donation_isha_outreach.active,
    donation_isha_outreach.system_id,
    donation_isha_outreach.contact_id_fkey,
    donation_isha_outreach.purpose_id,
	mapp.purpose,
	mapp.project_name,
    donation_isha_outreach.create_date,
    'IshaOutreach'::text AS entity_name,
    donation_isha_outreach.is_samarthak,
    rp.is_meditator As is_meditator
   FROM donation_isha_outreach, isha_purpose_project_mapping  mapp, res_partner rp
   where 
	mapp.id=donation_isha_outreach.purpose_id and rp.id = donation_isha_outreach.contact_id_fkey) as sub
        """)

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(DonationsView, self).fields_get(allfields, attributes=attributes)

        return res


    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        # default - do not show any results
        if len(args) == 0:
            return []
        return super(DonationsView, self)._search(args, offset, limit, order, count=count,
                                                      access_rights_uid=access_rights_uid)
