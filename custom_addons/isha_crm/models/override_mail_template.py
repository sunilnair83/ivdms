from odoo import models,fields, api
from odoo.exceptions import UserError
from .email_marketing import dynamic_mail_res_partner_mapper
class MailTemplateOverride(models.Model):
	_inherit = 'mail.template'


	html_body_str = fields.Text(string='String HTML Source')
	#category = fields.Selection([('satsang', 'Satsang'), ('ieo', 'IEO'), ('monthly_newsletters', 'Monthly Newsletters'), ('sadhguru_exclusive', 'Sadhguru Exclusive'),  ('webinars', 'Webinars')], 'Category')
	selectable_fields = ['name', 'category']
	mail_body_creation = fields.Selection([('curate', 'Curate Mail Body'), ('upload', 'Upload HTML source')], default='upload', required=True)
	body_arch = fields.Html(string='Body', translate=False)
	def _get_model_domain(self):
		if self._context.get('restrict_models',False):
			return [('id','in',self.env['ir.model'].sudo().search([('model','in',['res.partner']+list(dynamic_mail_res_partner_mapper.keys()))]).ids)]
		return []
	model_id = fields.Many2one('ir.model', 'Template for', help="The type of document this template can be used with",domain=_get_model_domain)

	is_pgm_upd = fields.Boolean(string="Is Program Update?")
	cta_links = fields.Text(string='CTA')

	def _get_default_mailing_team(self):
		teams = self.env.user.mailing_teams
		if teams:
			for team in teams:
				if team.id == self.env.ref('isha_crm.mailing_team_emedia').id:
					return team.id
			return teams.ids[0]
		return None

	team_id = fields.Many2one(comodel_name='isha.mailing.team', string='Team Name', default=_get_default_mailing_team)

	@api.depends('team_id')
	def _compute_mailing_team_read_only(self):
		for rec in self:
			readonly = False if self.env.ref('isha_crm.mailing_team_emedia').id in self.env.user.mailing_teams.ids \
				else True
			rec.mailing_team_read_only = readonly

	mailing_team_read_only = fields.Boolean(store=False, compute=_compute_mailing_team_read_only)


	@api.model
	def create(self, values):

		return_value =  super(MailTemplateOverride, self).create(values)
		if return_value.mail_body_creation and return_value.mail_body_creation == 'upload':
			if 'html_body_str' in values and values['html_body_str'] and values['html_body_str'] != return_value.body_html:
				return_value.body_html = values['html_body_str']
		return return_value

	def write(self, values):
		return_value = super(MailTemplateOverride, self).write(values)
		for rec in self:
			if rec.mail_body_creation and rec.mail_body_creation == 'upload':
				if rec.html_body_str and rec.html_body_str != rec.body_html:
					rec.body_html = rec.html_body_str

		return return_value

	@api.model
	def fields_get(self, allfields=None, attributes=None):
		template = super(MailTemplateOverride, self).fields_get(allfields, attributes=attributes)
		not_selectable_fields = set(template.keys()) - set(self.selectable_fields)
		for field in not_selectable_fields:
			template[field]['selectable'] = False  # to hide in Add Custom filter view
		return template

	def unlink(self):
		try:
			return super(MailTemplateOverride, self).unlink()
		except Exception as ex:
			raise UserError('Operation Denied !!!. \n Template cannot be deleted once it is used.')

	@api.model
	def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
		context = self._context or {}
		if context.get('my_team'):
			if not self.env.user.mailing_teams.ids:
				domain += [('team_id', '=', False)]
			elif self.env.ref('isha_crm.mailing_team_emedia').id not in self.env.user.mailing_teams.ids:
				domain += [('team_id', 'in', self.env.user.mailing_teams.ids)]
		elif context.get('team_scope'):
			if not self.env.user.mailing_teams.ids:
				domain += [('team_id', '=', False)]
			elif self.env.ref('isha_crm.mailing_team_emedia').id not in self.env.user.mailing_teams.ids:
				domain += ['|', ('team_id', '=', False), ('team_id', 'in', self.env.user.mailing_teams.ids)]
		return super(MailTemplateOverride, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
														   orderby=orderby, lazy=lazy)

	@api.model
	def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
		context = self._context or {}
		if context.get('my_team'):
			if not self.env.user.mailing_teams.ids:
				args += [('team_id', '=', False)]
			elif self.env.ref('isha_crm.mailing_team_emedia').id not in self.env.user.mailing_teams.ids:
				args += [('team_id', 'in', self.env.user.mailing_teams.ids)]
		elif context.get('team_scope'):
			if not self.env.user.mailing_teams.ids:
				args += [('team_id', '=', False)]
			elif self.env.ref('isha_crm.mailing_team_emedia').id not in self.env.user.mailing_teams.ids:
				args += ['|',('team_id', '=', False),('team_id', 'in', self.env.user.mailing_teams.ids)]
		return super(MailTemplateOverride, self)._search(args, offset, limit, order, count=count,
														access_rights_uid=access_rights_uid)
