from odoo import models, fields, api, exceptions, modules
from odoo.exceptions import ValidationError, UserError
from odoo import SUPERUSER_ID, _
import logging

class IshaPincodeCenter (models.Model):
	_name = 'isha.pincode.center'
	_description = 'Pincode to center mapping'

	name = fields.Char(string='Pincode', required=True, index=True)
	center_id = fields.Many2one('isha.center', string='Center')
	old_center_id = fields.Many2one('isha.center', string='Old Center')
	citydistricttown = fields.Char('City or District')
	state_id = fields.Many2one('res.country.state')
	country_id = fields.Many2one('res.country')

	active = fields.Boolean(string='active', default=True)
	state = fields.Char(string='State', required=False)
	countryiso_2 = fields.Char('Country Code')
	defaultcitytype = fields.Char('Default city type')
	defaultcity = fields.Boolean('Is City Default')
	region_name = fields.Char(related='center_id.region_id.name', string="region_name", store=True)

	@api.model
	def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
		context = self._context or {}
		if context.get('centers_scope'):
			domain += [('center_id', 'in',
						self.env.user.centers.ids)]
		return super(IshaPincodeCenter, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
												   orderby=orderby, lazy=lazy)

	@api.model
	def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
		context = self._context or {}
		if context.get('centers_scope'):
			args += [('center_id', 'in',
					  self.env.user.centers.ids)]
		if len(args) == 0:
			return []
		return super(IshaPincodeCenter, self)._search(args, offset, limit, order, count=count,
												access_rights_uid=access_rights_uid)

	@api.model
	def create(self, val):
		if 'name' in val and 'countryiso_2' in val:
			rec = self.search([('name','=',val['name']),('countryiso_2','=',val['countryiso_2'])])
			if len(rec) > 0 and val['center_id'] != rec.center_id.id:
				# new_center = self.env['isha.center'].search([('name','=','')])
				rec.write({'old_center_id':rec.center_id.id,'center_id':val['center_id']})
				return rec
		return super(IshaPincodeCenter, self).create(val)

class IshaRegion(models.Model):
	_name = 'isha.region'

	_sql_constraints = [('unique_isha_region_name', 'unique(name)', 'Region name should be unique')]

	name = fields.Char(string='Region Name', required=True)
	active = fields.Boolean(string='active', default=True)
	centers = fields.One2many('isha.center','region_id','Centers')
	users = fields.Many2many('res.users', column1='region_id', column2='user_id', string='Region Admins')
	sms_provider_base_url = fields.Char('Url')
	sms_provider_sender_id = fields.Char('Sender Id')
	sms_provider_username = fields.Char('Username/Client Id')
	sms_provider_password = fields.Char('Password/ApiKey', password="True")
	sms_provider_fl = fields.Integer('Flash message',default=0)
	sms_provider_gwid = fields.Integer('Message Type')


class IshaCenter(models.Model):
	_name = 'isha.center'

	_sql_constraints = [('unique_isha_region_name', 'unique(name)', 'Region name should be unique')]

	name = fields.Char(string='Center Name', required=True)
	region_id = fields.Many2one('isha.region', 'Region', required=True)
	sector_country = fields.Char(string='Sector/District')
	is_bucket_center = fields.Boolean('Is Bucket')

	admins = fields.Many2many('res.users', column1='center_id', column2='user_id', string='Admins')
	pincodes = fields.One2many('isha.pincode.center', 'center_id', 'Pincodes')
	state_ids = fields.Many2many('res.country.state', 'isha_center_res_state_rel',column1='center_id',column2='state_id', string='States')
	total_center_count = fields.Integer(string='No Of Meditators', store=True, readonly=True,
										compute='_compute_center_count')
	total_bsp_count = fields.Integer(string='BSP Count', store=True, readonly=True, compute='_compute_center_count')
	total_ie_count = fields.Integer(string='IE Count', store=True, readonly=True, compute='_compute_center_count')
	total_samyama_count = fields.Integer(string='Samyama Count', store=True, readonly=True,
										 compute='_compute_center_count')
	partner_ids = fields.One2many(
		string='Contacts',
		comodel_name='res.partner',
		inverse_name='center_id',
	)

	active = fields.Boolean(string='active', default=True)
	admin_count = fields.Integer(string="Admin count", compute="compute_admin_count",store=False)
	vol_cod_name = fields.Char(string='Volunteer Co-ordinator name')
	vol_cod_phone = fields.Char(string='Volunteer Co-ordinator phone')

	def compute_admin_count(self):
		for rec in self:
			rec.admin_count = len(rec.admins) if rec.admins else 0

	@api.model
	def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
		context = self._context or {}
		if context.get('dashboard_center_scope'):
			args += [('id','in', self.env.user.centers.ids)]

		return super(IshaCenter, self)._search(args, offset, limit, order, count=count, access_rights_uid=access_rights_uid)

	def dashboard_count_action(self):
		center_list = self.env.user.centers.ids
		kanban_id = self.env.ref("isha_crm.dashboard_count_kanban_view2")
		return {
			'type': 'ir.actions.act_window',
			'name': 'Dashboard',
			'view_mode': 'kanban',
			'res_model': 'isha.center',
			'domain': [('id', 'in', center_list)],
			# if you don't want to specify form for example
			# (False, 'form') just pass False
			'views': [(kanban_id.id, 'kanban')],
			'target': 'current'
		}

	# @api.depends('partner_ids.center_id', 'partner_ids.pgm_tag_ids')
	def _compute_center_count(self):
		for rec in self:
			rec.total_center_count = self.env['res.partner'].search_count([('center_id', '=', rec['id'])])
			rec.total_bsp_count = self.env['res.partner'].search_count(
				[('center_id', '=', rec['id']), ('pgm_tag_ids', '=ilike', 'BSP')])
			rec.total_ie_count = self.env['res.partner'].search_count(
				[('center_id', '=', rec['id']), ('pgm_tag_ids', '=ilike', 'IE')])
			rec.total_samyama_count = self.env['res.partner'].search_count(
				[('center_id', '=', rec['id']), ('pgm_tag_ids', '=ilike', 'Samyama')])

	def dashboard_server_action(self):
		for center in self:
			tree_id = self.env.ref("isha_crm.view_partner_tree")
			form_id = self.env.ref("isha_crm.view_partner_form")
			pivot_id = self.env.ref("isha_crm.view_volunteer_pivot")
			graph_id = self.env.ref("isha_crm.view_volunteer_graph")
			return {
				'type': 'ir.actions.act_window',
				'name': 'All',
				'view_type': 'form',
				'view_mode': 'tree,form,pivot',
				'res_model': 'res.partner',
				'domain': [('center_id', '=', center.id)],
				# if you don't want to specify form for example
				# (False, 'form') just pass False
				'views': [(tree_id.id, 'tree'), (form_id.id, 'form'), (pivot_id.id, 'pivot')],
				'target': 'current'
			}

	def get_ie_action(self):
		for center in self:
			tree_id = self.env.ref("isha_crm.view_partner_tree")
			form_id = self.env.ref("isha_crm.view_partner_form")
			return {
				'type': 'ir.actions.act_window',
				'name': 'IE',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'res_model': 'res.partner',
				'domain': [('center_id', '=', center.id), ('pgm_tag_ids', '=ilike', 'IE')],
				# if you don't want to specify form for example
				# (False, 'form') just pass False
				'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
				'target': 'current'
			}

	def get_bsp_action(self):
		for center in self:
			tree_id = self.env.ref("isha_crm.view_partner_tree")
			form_id = self.env.ref("isha_crm.view_partner_form")
			return {
				'type': 'ir.actions.act_window',
				'name': 'BSP',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'res_model': 'res.partner',
				'domain': [('center_id', '=', center.id), ('pgm_tag_ids', '=ilike', 'BSP')],
				# if you don't want to specify form for example
				# (False, 'form') just pass False
				'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
				'target': 'current'
			}

	def get_samyama_action(self):
		for center in self:
			tree_id = self.env.ref("isha_crm.view_partner_tree")
			form_id = self.env.ref("isha_crm.view_partner_form")
			return {
				'type': 'ir.actions.act_window',
				'name': 'Samyama',
				'view_type': 'form',
				'view_mode': 'tree,form',
				'res_model': 'res.partner',
				'domain': [('center_id', '=', center.id), ('pgm_tag_ids', '=ilike', 'Samyama')],
				# if you don't want to specify form for example
				# (False, 'form') just pass False
				'views': [(tree_id.id, 'tree'), (form_id.id, 'form')],
				'target': 'current'
			}

class IshaCenterEventUser(models.TransientModel):
	_name = 'isha.center.event.user'

	selected_count = fields.Integer(string='Selected count', readonly=True)

	@api.model
	def default_get(self, fields):
		result = super(IshaCenterEventUser, self).default_get(fields)
		result['selected_count'] = len(self._context.get('active_ids')) if self._context.get('active_ids') else 0
		return result
	def custom_create_event_user_for_centers(self):
		centers = self._context.get('active_ids')
		user_model = self.env['res.users']
		login = ['_event_user1','_event_user2','_event_user3']
		user_ids = []
		for center in self.env['isha.center'].browse(centers):
			for x in login:
				user_id = user_model.sudo().create({
					'name' : center.name + x,
					'no_match_n_merge':True,
					'login' : center.name + x,
					'centers':[(6,0,[center.id])],
					'password': 'Ishaevent@123',
					'groups_id': [(6,0,[self.env.ref('event.group_event_user').id,self.env.ref('base.group_user').id])]
				})

class ResCountry(models.Model):
	_inherit = 'res.country'
	center_id = fields.Many2one('isha.center', string='Center')

class ResState(models.Model):
	_inherit = 'res.country.state'
	center_ids = fields.Many2many('isha.center', 'isha_center_res_state_rel', column1='state_id',column2='center_id', string='Centers')
