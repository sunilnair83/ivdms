# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    sms_provider_base_url = fields.Char('Url', default='http://api.smscountry.com/SMSCwebservice_bulk.aspx', config_parameter='isha_crm.sms_provider_base_url')
    sms_provider_sender_id = fields.Char('Sender Id', default='ISHATD', config_parameter='isha_crm.sms_provider_sender_id')
    sms_provider_username = fields.Char('Username',default='eastrco', config_parameter='isha_crm.sms_provider_username')
    sms_provider_password = fields.Char('Password', password="True", default='', config_parameter='isha_crm.sms_provider_password')
    ieco_api_s2 = fields.Char('S2 ieco api url',default='https://mokshatwo.sushumna.isha.in/api/ieco/getdetailstwomoksha', config_parameter='isha_crm.ieco_api_s2')
    ieco_apac_api_url = fields.Char('S2 ieco api url',default='https://crmt.ishayoga.live/_api/moksha/checkMeditator?email=thulasidhar@gmail.com&check_meditator=True', config_parameter='isha_crm.ieco_apac_api_url')
    ieco_apac_api_token = fields.Char('S2 ieco api url',default='Te$t1ZS5mc43No3f00wUe1OI8fh7HUXH', config_parameter='isha_crm.ieco_apac_api_token')
    ieco_pms_entity = fields.Char('PMS entity',default='IFINC',config_parameter='isha_crm.ieco_pms_entity')