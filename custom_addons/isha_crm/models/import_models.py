from odoo import models, fields, api, tools
import traceback
import datetime

class ImportIshaVidhyaDonations(models.Model):
    _name = 'import.donation.isha.vidhya'
    _description = 'Import Isha Vidhya Donations'
    _order = "create_date desc"

    local_trans_id = fields.Char()
    ereceipt_record_id = fields.Char()
    books_of_account = fields.Char()
    center = fields.Char()
    donation_date = fields.Date()
    ebook_number = fields.Char()
    receipt_number = fields.Char()
    transfer_reference = fields.Char()
    don_receipt_date = fields.Date()
    amount = fields.Float()
    currency = fields.Char()
    exchange_rate = fields.Float()
    inr_amount = fields.Float()
    fr_converted_currency = fields.Char()
    fr_converted_amount = fields.Float()
    fr_retention_amount = fields.Float()
    fr_payable_amount = fields.Float()
    mode_of_payment = fields.Char()
    dd_chq_number = fields.Char()
    dd_chq_date = fields.Date()
    bank_name = fields.Char()
    bank_branch = fields.Char()
    contact_name = fields.Char()
    contact_address = fields.Char()
    contact_city = fields.Char()
    contact_state = fields.Char()
    contact_country = fields.Char()
    contact_pincode = fields.Char()
    contact_mobile = fields.Char()
    contact_email = fields.Char()
    iv_donor_category = fields.Char()
    nationality = fields.Char()
    project = fields.Char()
    general_donation_amount = fields.Float()
    govt_school_adoption_prgm_amount = fields.Float()
    infrastructure_requirements_amount = fields.Float()
    full_educational_support_qty = fields.Integer()
    full_educational_support_amount = fields.Float()
    scholarship_qty = fields.Integer()
    scholarship_amount = fields.Float()
    transport_subsidy_qty = fields.Integer()
    transport_subsidy_amount = fields.Float()
    noon_meal_subsidy_qty = fields.Integer()
    noon_meal_subsidy_amount = fields.Float()
    corpus = fields.Float()
    pan_card = fields.Char()
    platform = fields.Char()
    fr_project_id = fields.Char()
    fr_project_name = fields.Char()
    fr_project_link = fields.Char()
    campaign_id = fields.Char()
    campaign_name = fields.Char()
    campaign_link = fields.Char()
    fundraiser_name = fields.Char()
    comments = fields.Char()
    consolidated_receipt = fields.Char()
    classroom_qty = fields.Integer()
    classroom_amount = fields.Float()
    library_books_qty = fields.Integer()
    library_books_amount = fields.Float()
    computer_qty = fields.Integer()
    computer_amount = fields.Float()
    projector_qty = fields.Integer()
    projector_amount = fields.Float()
    learning_aids_qty = fields.Integer()
    learning_aids_amount = fields.Float()
    kitchen_equip_qty = fields.Integer()
    kitchen_equip_amount = fields.Float()
    sports_equip_qty = fields.Integer()
    sports_equip_amount = fields.Float()
    furniture_qty = fields.Integer()
    furniture_amount = fields.Float()
    school_bus_qty = fields.Integer()
    school_bus_amount = fields.Float()
    ro_plant_qty = fields.Integer()
    ro_plant_amount = fields.Float()
    error = fields.Char(string='Error Message')
    stack_trace = fields.Text()

    def import_iv_data(self):
        records = self.search([('error', '=', None)])
        if records:
            success_count = 0
            failed_count = 0
            for rec in records:
                try:
                    msg = self.get_msg(rec)
                    self.env['donation.isha.education'].with_context(import_file=True)\
                        .with_context(ivdms_import_file=True).create(msg)
                except Exception as ex:
                    failed_count += 1
                    tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                    rec.write({'error': str(ex),
                               'stack_trace': tb_ex})
                else:
                    success_count += 1
                    rec.unlink()
                    self.env.cr.commit()

            template = self.env.ref('isha_crm.ivdms_data_import_status')
            template.email_to = self.env['ir.config_parameter'].sudo().get_param('isha_crm.ivdms_data_import_status_to_email_ids')
            # get random partner
            partner = self.env['res.partner'].search([], limit=1)
            values = template.generate_email(partner.id, fields=None)
            values['subject'] = values['subject'].replace('$$cur_date', str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')))
            values['body_html'] = values['body_html'].replace('$$total', str(success_count + failed_count))
            values['body_html'] = values['body_html'].replace('$$success', str(success_count))
            values['body_html'] = values['body_html'].replace('$$failed', str(failed_count))

            mail_mail_obj = self.env['mail.mail']
            msg_id = mail_mail_obj.sudo().create(values)
            msg_id.send(False)

    def get_msg(self, rec):
        return {
            'local_trans_id': rec.local_trans_id,
            'ereceipt_record_id': rec.ereceipt_record_id,
            'books_of_account': rec.books_of_account,
            'center': rec.center,
            'donation_date': rec.donation_date,
            'ebook_number': rec.ebook_number,
            'receipt_number': rec.receipt_number,
            'transfer_reference': rec.transfer_reference,
            'don_receipt_date': rec.don_receipt_date,
            'amount': rec.amount,
            'currency': rec.currency,
            'exchange_rate': rec.exchange_rate,
            'inr_amount': rec.inr_amount,
            'fr_converted_currency': rec.fr_converted_currency,
            'fr_converted_amount': rec.fr_converted_amount,
            'fr_retention_amount': rec.fr_retention_amount,
            'fr_payable_amount': rec.fr_payable_amount,
            'mode_of_payment': rec.mode_of_payment,
            'dd_chq_number': rec.dd_chq_number,
            'dd_chq_date': rec.dd_chq_date,
            'bank_name': rec.bank_name,
            'bank_branch': rec.bank_branch,
            'contact_name': rec.contact_name,
            'contact_address': rec.contact_address,
            'contact_city': rec.contact_city,
            'contact_state': rec.contact_state,
            'contact_country': rec.contact_country,
            'contact_pincode': rec.contact_pincode,
            'contact_mobile': rec.contact_mobile,
            'contact_email': rec.contact_email,
            'iv_donor_category': rec.iv_donor_category,
            'nationality': rec.nationality,
            'project': rec.project,
            'general_donation_amount': rec.general_donation_amount,
            'govt_school_adoption_prgm_amount': rec.govt_school_adoption_prgm_amount,
            'infrastructure_requirements_amount': rec.infrastructure_requirements_amount,
            'full_educational_support_qty': rec.full_educational_support_qty,
            'full_educational_support_amount': rec.full_educational_support_amount,
            'scholarship_qty': rec.scholarship_qty,
            'scholarship_amount': rec.scholarship_amount,
            'transport_subsidy_qty': rec.transport_subsidy_qty,
            'transport_subsidy_amount': rec.transport_subsidy_amount,
            'noon_meal_subsidy_qty': rec.noon_meal_subsidy_qty,
            'noon_meal_subsidy_amount': rec.noon_meal_subsidy_amount,
            'corpus': rec.corpus,
            'pan_card': rec.pan_card,
            'platform': rec.platform,
            'fr_project_id': rec.fr_project_id,
            'fr_project_name': rec.fr_project_name,
            'fr_project_link': rec.fr_project_link,
            'campaign_id': rec.campaign_id,
            'campaign_name': rec.campaign_name,
            'campaign_link': rec.campaign_link,
            'fundraiser_name': rec.fundraiser_name,
            'comments': rec.comments,
            'consolidated_receipt_type': rec.consolidated_receipt,
            'classroom_qty': rec.classroom_qty,
            'classroom_amount': rec.classroom_amount,
            'library_books_qty': rec.library_books_qty,
            'library_books_amount': rec.library_books_amount,
            'computer_qty': rec.computer_qty,
            'computer_amount': rec.computer_amount,
            'projector_qty': rec.projector_qty,
            'projector_amount': rec.projector_amount,
            'learning_aids_qty': rec.learning_aids_qty,
            'learning_aids_amount': rec.learning_aids_amount,
            'kitchen_equip_qty': rec.kitchen_equip_qty,
            'kitchen_equip_amount': rec.kitchen_equip_amount,
            'sports_equip_qty': rec.sports_equip_qty,
            'sports_equip_amount': rec.sports_equip_amount,
            'furniture_qty': rec.furniture_qty,
            'furniture_amount': rec.furniture_amount,
            'school_bus_qty': rec.school_bus_qty,
            'school_bus_amount': rec.school_bus_amount,
            'ro_plant_qty': rec.ro_plant_qty,
            'ro_plant_amount': rec.ro_plant_amount,
        }
# @api.model_create_multi
    # def create(self, vals_list):
    #     return_list_ids = []
    #     if 'import_file' in self._context:
    #         for val in vals_list:
    #             print('local_trans_id: ' + val['local_trans_id'])
    #         print('-------------------')
