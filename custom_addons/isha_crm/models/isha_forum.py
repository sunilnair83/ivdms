# -*- coding: utf-8 -*-
from werkzeug import urls

from odoo.addons.http_routing.models.ir_http import slug
from odoo import models, fields, api, exceptions, modules
from odoo.exceptions import ValidationError, UserError
from odoo import SUPERUSER_ID, _
import logging
import sys
import re
import traceback
from copy import deepcopy

_logger = logging.getLogger('ProgramAttendance')
_logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler(sys.stdout)
_logger.addHandler(ch)

# class Post(models.Model):
#     _inherit = 'forum.post'
#
#     def post_notification(self):
#         for post in self:
#             tag_partners = post.tag_ids.mapped('message_partner_ids')
#             partners = post.sudo().message_partner_ids | tag_partners
#
#             base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
#             if post.state == 'active' and post.parent_id:
#                 link_url = urls.url_join(base_url,
#                               "forum/%s/question/%s" % (slug(post.parent_id.forum_id), slug(post.parent_id)))
#                 for p in partners:
#                     if p.phone:
#                         self.env['sms.api']._send_smcountry_sms({
#                                     'number': p.phone,
#                                     'message': "Namaskaram, A new answer to, " + post.parent_id.name + ", has been posted. " + link_url + ". Pranam"
#                                 })
#                 # if post.parent_id.create_uid.phone:
#                 #     self.env['sms.api']._send_smcountry_sms({
#                 #         'number': post.parent_id.create_uid.phone,
#                 #         'message': "Namaskaram, A new answer to, " + post.parent_id.name + ", has been posted. " +
#                 #                    link_url
#                 #     })
#             elif post.state == 'active' and not post.parent_id:
#
#                 link_url = urls.url_join(base_url,
#                               "forum/%s/question/%s" % (slug(post.forum_id), slug(post)))
#                 for p in partners:
#                     if p.phone:
#                         self.env['sms.api']._send_smcountry_sms({
#                                     'number': p.phone,
#                                     'message': "Namaskaram, A new question, " + post.name + ", has been posted. " + link_url + ", Pranam"
#                                 })
#             # if post.state == 'active' and post.parent_id:
#             #     post.parent_id.message_post_with_view(
#             #         'website_forum.forum_post_template_new_answer',
#             #         subject=_('Re: %s') % post.parent_id.name,
#             #         partner_ids=[(4, p.id) for p in tag_partners],
#             #         subtype_id=self.env['ir.model.data'].xmlid_to_res_id('website_forum.mt_answer_new'))
#             # elif post.state == 'active' and not post.parent_id:
#             #     post.message_post_with_view(
#             #         'website_forum.forum_post_template_new_question',
#             #         subject=post.name,
#             #         partner_ids=[(4, p.id) for p in tag_partners],
#             #         subtype_id=self.env['ir.model.data'].xmlid_to_res_id('website_forum.mt_question_new'))
#             # elif post.state == 'pending' and not post.parent_id:
#             #     # TDE FIXME: in master, you should probably use a subtype;
#             #     # however here we remove subtype but set partner_ids
#             #     partners = post.sudo().message_partner_ids | tag_partners
#             #     partners = partners.filtered(lambda partner: partner.user_ids and any(user.karma >= post.forum_id.karma_moderate for user in partner.user_ids))
#             #
#             #     post.message_post_with_view(
#             #         'website_forum.forum_post_template_validation',
#             #         subject=post.name,
#             #         partner_ids=partners.ids,
#             #         subtype_id=self.env['ir.model.data'].xmlid_to_res_id('mail.mt_note'))
#         return super(Post, self).post_notification()
