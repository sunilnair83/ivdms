from datetime import datetime, timedelta

import odoo


class ProgramAttUpdater:

    pgmModel = None
    leadModel = None
    mapModel = None
    CR = None

    def createPgm(self, registry, src_rec):
        UID = odoo.SUPERUSER_ID
        CR = registry.cursor()
        self.CR = CR
        with odoo.api.Environment.manage():
            ENV = odoo.api.Environment(CR, UID, {})
            flag_dict = self.createPgmInternal(ENV, src_rec)
            self.CR.commit()
            self.CR.close()
            self.CR = None
            return flag_dict

    def first_and_last_day_of_month(self, any_day):
        if any_day:
            try:
                any_day = datetime.strptime(any_day,'%Y-%m-%d')
                next_month = any_day.replace(day=28) + timedelta(days=4)
                return (any_day.replace(day=1), next_month - timedelta(days=next_month.day))
            except:
                pass
        return None

    def createPgmInternal(self, ENV, src_rec):
        self.pgmModel = ENV['program.attendance']
        self.leadModel = ENV['program.lead']
        self.mapModel = ENV['contact.map']
        self.resPartner = ENV['res.partner']
        pgm_mst = self.get_mapping_pgm_master(ENV,src_rec)
        if len(pgm_mst) != 0:
            src_rec.update({'pgm_type_master_id': pgm_mst[0]['id']})
        else:
            src_rec.update({'pgm_type_master_id': ENV.ref('isha_crm.pgm_type_master_dummy').id})
        src_rec.update({'reg_status':self.get_mapping_status(ENV,src_rec)})
        if len(pgm_mst) != 0 and pgm_mst[0]['is_lead']:
            rec = self.createOrUpdate(src_rec, self.leadModel)
        else:
            src_rec.pop('ie_hear_about', None)
            src_rec.pop('ieo_class_status', None)
            src_rec.pop('checkin_status_day_2', None)
            src_rec.pop('event_attendance_date', None)
            src_rec.pop('webinar_event_id', None)
            src_rec.pop('webinar_event_name', None)
            src_rec.pop('webinar_event_date', None)
            rec = self.createOrUpdate(src_rec, self.pgmModel)
        flag_dict = {'flag': True, 'rec': rec}
        return flag_dict

    def createOrUpdate(self, src_rec, env_model):
        update_rec = self.getExistingRec(src_rec, env_model)
        if len(update_rec) > 0:  # if it already exists just update it
            if src_rec['system_id'] == 13: # Selective Update for PRS
                self.selectiveUpdate(update_rec,src_rec,env_model)
            else:
                update_rec[0].sudo().write(src_rec)
            return update_rec[0]

        else:
            if 'contact_id_fkey' not in src_rec:
                self.setContactId(src_rec)

            record = env_model.sudo().create(src_rec)
            return record

    def selectiveUpdate(self,update_rec,src_rec,env_model):
        if src_rec['system_id'] == 13 and src_rec['program_name'] == 'Online Satsang':
            for rec in update_rec:
                if not rec['event_attendance_date']:
                    rec.write({'reg_status':src_rec['reg_status'],'event_attendance_date':src_rec['event_attendance_date']})
        elif src_rec['system_id'] == 13 and ('shivanga' in src_rec['local_program_type_id'].lower() or src_rec['program_name'] == 'MSR 2021 Seating Passes'):
            for rec in update_rec:
                if rec['local_trans_id'] == src_rec['local_trans_id']:
                    rec.sudo().write(src_rec)
                    return
            for rec in update_rec:
                if rec['reg_status'] == 'INQUIRED':
                    rec.sudo().write(src_rec)
                    return
            if src_rec['reg_status'] == 'CONFIRMED':
                env_model.sudo().create(src_rec)
        elif src_rec['system_id'] == 13 and src_rec['program_name'] in ('Surya Shakthi Yogaveera Training Registrations','Online Offerings Webinar Registration'):
            for rec in update_rec:
                if rec['local_trans_id'] == src_rec['local_trans_id']:
                    rec.sudo().write(src_rec)
                    return
            for rec in update_rec:
                if rec['reg_status'] != 'ATTENDED':
                        rec.sudo().write(src_rec)
                        return
            if src_rec['reg_status'] == 'ATTENDED' and len(update_rec.filtered(lambda x: x.reg_status == 'ATTENDED')) == 0:
                env_model.sudo().create(src_rec)

        else:
            update_rec[0].sudo().write(src_rec)

    def setContactId(self,src_rec):
        rec_list = self.mapModel.search(
            [('local_contact_id', '=', src_rec['local_contact_id']), ('system_id', '=', str(src_rec['system_id']))])
        if len(rec_list) > 0:
            rec = rec_list[0]
            src_rec['contact_id_fkey'] = rec['contact_id_fkey'].id
            src_rec['guid'] = rec['guid']
        else:
            src_rec['contact_id_fkey'] = 2  # temp user

    def getPgmMaserId(self, env, rec):
        pgm_mst = self.get_mapping_pgm_master(env,rec)
        if len(pgm_mst) !=0 :
            return pgm_mst[0]['id']
        return env.ref('isha_crm.pgm_type_master_dummy').id

    def get_mapping_pgm_master(self, env, rec):
        if rec['local_program_type_id'] is not None:
            pgm_mst = env['program.type.master'].search([('local_program_type_id','=',rec['local_program_type_id']),('system_id','=',rec['system_id'])])
            return pgm_mst
        else:
            return env['program.type.master']

    def get_mapping_status(self, env, rec):
        if rec['reg_status'] is not None:
            pgm_status = env['isha.program.status'].search([('schedule_status','ilike',rec['reg_status']),('system_id','=',rec['system_id'])])
            if len(pgm_status)!=0:
                return pgm_status[0].mapping_status

        return rec['reg_status'].upper() if rec['reg_status'] else None


# check usages
    def set_last_txn_date(self, partner_rec, start_date):
        if partner_rec.last_txn_date and start_date:
            partner_rec.last_txn_date = max(partner_rec.last_txn_date, datetime.strptime(start_date, '%Y-%m-%d').date())
        elif start_date:
            partner_rec.last_txn_date = start_date

    def set_prog_flag(self, partner_rec, pgm_mst_rec):
        if len(pgm_mst_rec) != 0:
            partner_rec.is_meditator = partner_rec.is_meditator or pgm_mst_rec[0].meditator_qualifier


    def getExistingRec(self, src_rec, env_model):
        # Additional PRS constraints for 'Online Monthly Satsang Confirmation' -> ISH-495, ISH-612
        if src_rec['system_id'] == 13 and src_rec['program_name'] in ["Online Pournami Satsang","Online Satsang",
                                                                      "Online IEO Satsang","Online SCK Satsang"]:
            if 'contact_id_fkey' not in src_rec:
                self.setContactId(src_rec)

            rec = env_model.sudo().search([('pgm_type_master_id', '=', src_rec['pgm_type_master_id']),
                                           ('start_date','=', src_rec['start_date']),('contact_id_fkey','=',src_rec['contact_id_fkey'])
                                           ])
            if len(rec)>0:
                return rec

            return env_model.sudo()

        # Modify PRS for handling drop offs in shivanga sadhana and  'MSR 2021 Seating Passes'
        if src_rec['system_id'] == 13 and ('shivanga' in src_rec['local_program_type_id'].lower() or src_rec['program_name'] == 'MSR 2021 Seating Passes'):
            # Direct search on the Local trans id
            rec = env_model.sudo().search([('local_trans_id','=',src_rec['local_trans_id']),('system_id','=',src_rec['system_id'])])
            if len(rec)>0:
                return rec

            if 'contact_id_fkey' not in src_rec:
                self.setContactId(src_rec)

            # Check for any txn for the same contact for the same program and start_date
            rec = env_model.sudo().search([('pgm_type_master_id', '=', src_rec['pgm_type_master_id']),
                                           ('start_date','=', src_rec['start_date']),('contact_id_fkey','=',src_rec['contact_id_fkey'])
                                           ])

            return rec
        # Modify PRS for handling duplicate records in "Surya Shakthi Yogaveera Training Registrations" and
        # "Online Offerings Webinar Registration"
        if src_rec['system_id'] == 13 and (src_rec['program_name'] == 'Surya Shakthi Yogaveera Training Registrations' or src_rec['program_name'] == 'Online Offerings Webinar Registration'):
            rec = env_model.sudo().search(
                [('pgm_type_master_id', '=', src_rec['pgm_type_master_id']),('webinar_event_id', '=', src_rec['webinar_event_id']), ('contact_id_fkey', '=', src_rec['contact_id_fkey'])])

            return rec

        return env_model.sudo().search([('local_trans_id','=',src_rec['local_trans_id']),('system_id','=',src_rec['system_id'])])

    def closeCursorWhenException(self):
        if self.CR is not None:
            self.CR.close()

class DonationsUpdater:
    model_mapper = {
        'ishafoundation':'donation.isha.foundation',
        'ishaoutreach':'donation.isha.outreach',
        'ishaeducation':'donation.isha.education',
        'ishainstituteofinnersciences':'donation.isha.iii',
        'ishaarogyaresearchfoundation':'donation.isha.arogya',
		'shriyoginitrust': 'donation.shri.yogini',
		'thenkailayabakthiperavai': 'donation.tkbp',
		'ishautsav': 'donation.isha.utsav',
		'sacredwalks': 'donation.sacred.walks',
    }
    
    donationModel = None
    contactMap = None
    CR = None

    def createDonation(self, registry, src_rec):
        UID = odoo.SUPERUSER_ID
        CR = registry.cursor()
        self.CR = CR
        with odoo.api.Environment.manage():
            ENV = odoo.api.Environment(CR, UID, {})
            flag_dict = self.createDonationInternal(ENV, src_rec)
            self.CR.commit()
            self.CR.close()
            self.CR = None
            return flag_dict

    def createDonationInternal(self, ENV, src_rec):
        entity = src_rec['entity'].lower()
        self.donationModel = ENV[self.model_mapper.get(entity)]
        self.contactMap = ENV['contact.map']

        update_rec = self.donationModel.sudo().search(
            [('local_trans_id', '=', src_rec['local_trans_id']), ('system_id', '=', str(src_rec['system_id']))])
        if update_rec:
            if len(update_rec) == 1:
                update_rec.sudo().write(src_rec)
                return {'flag': True, 'rec': update_rec}
            else:
                raise Exception('More than 1 matching records found for local_trans_id: ' + src_rec['local_trans_id'] +
                                ' and system: ' + str(src_rec['system_id']))
        else:
            purpose_id = self.getPurposeId(ENV, src_rec)
            if (purpose_id == ENV.ref('isha_crm.isha_purpose_project_mapping_dummy').id
                    and src_rec['project'] and 'program fee' in src_rec['project'].lower()):
                purpose_id = ENV['isha.purpose.project.mapping'].create({
                    'purpose': src_rec['project'],
                    'entity': src_rec['entity'],
                    'project_name': 'Program Fee'
                }).id

            src_rec.update({'purpose_id': purpose_id})
            src_rec.pop('entity', None)
            # Requirement as per FR
            # If a record from ERECEIPTS(system_id = 8) has receipt_number with 'a' suffix then it is modified.
            # So if a record from DMS(system_id = 7) comes with same receipt_number without suffix set DMS to active false
            # else set the ERECEIPTS to active false
            self.activeFieldCheck(src_rec)
            if 'contact_id_fkey' not in src_rec or 'guid' not in src_rec:
                rec_list = self.contactMap.sudo().search([('local_contact_id', '=', src_rec['local_patron_id']),
                                                          ('system_id', '=', src_rec['system_id'])])
                if len(rec_list) > 0:
                    rec = rec_list[0]
                    src_rec['contact_id_fkey'] = rec['contact_id_fkey'].id
                    src_rec['guid'] = rec['guid']
                else:
                    src_rec['contact_id_fkey'] = 2  # temp user

            rec = self.donationModel.sudo().create(src_rec)
            return {'flag': True, 'rec': rec}

    def activeFieldCheck(self, src_rec):
        if src_rec['system_id'] == 7 and src_rec['receipt_number'] is not None:
            rec_no = src_rec['receipt_number']+'a'
            modified_ereceipts_txn = self.donationModel.sudo().search([('receipt_number','=',rec_no),('system_id','=',8)])
            if len(modified_ereceipts_txn) > 0:
                src_rec['active'] = 0
            else:
                src_rec['active'] = 1 if src_rec['status'] != 'FAILED' else 0

            ereceipts_txn = self.donationModel.sudo().search([('receipt_number','=',src_rec['receipt_number']),('system_id','=',8)])
            ereceipts_txn.sudo().write({'active':0})

        elif src_rec['system_id'] == 8 and src_rec['receipt_number'] is not None:
            if src_rec['receipt_number'][-1] == 'a':
                receipt_number = src_rec['receipt_number'][:-1]
                dms_txn = self.donationModel.sudo().search([('receipt_number','=',receipt_number),('system_id','=',7)])
                dms_txn.sudo().write({'active':0})
                src_rec['active'] = 1
            else:
                dms_txn = self.donationModel.sudo().search([('receipt_number','=',src_rec['receipt_number']),('system_id','=',7)])
                src_rec['active'] = 1 if len(dms_txn) == 0 else 0
        if src_rec['status'] == 'SUCCESS':
            src_rec['active'] = 1
        else:
            src_rec['active'] = 0

    def getPurposeId(self, env, rec):
        if rec['project'] is not None:
            purpose_id = self.getMappingProject(env, rec)
            if len(purpose_id) != 0:
                return purpose_id[0].id
        return env.ref('isha_crm.isha_purpose_project_mapping_dummy').id

    def contactCreateFlag(self, env, txn_dict, contactMsgDict):
        project_rec = self.getMappingProject(env, txn_dict)
        if txn_dict['entity'] != 'IshaOutreach' and 'don_receipt_date' in txn_dict:
            contactMsgDict['last_txn_date'] = txn_dict['don_receipt_date']
        contactMsgDict.update(self.get_don_flags(self.getMappingProject(env, txn_dict)))
        if len(project_rec) == 1:
            txn_dict['purpose_id'] = project_rec[0].id
        if (len(project_rec) == 1 and project_rec[0].project_name == 'Cottage') \
                or (txn_dict['entity'] == 'IshaFoundation' and txn_dict['center'] == 'Residential Accommodation' \
                    and (txn_dict['project'] == "Corpus Fund FCRA" or txn_dict['project'] == "Donation")) \
                or (txn_dict['entity'] == 'IshaFoundation' and txn_dict['center'] == 'Ashram Cottage' \
                    and txn_dict['project'].startswith("Rent")):
            txn_dict['contact_name'] = contactMsgDict['name']
            txn_dict['contact_phone'] = contactMsgDict['phone']
            txn_dict['contact_email'] = contactMsgDict['email']
            txn_dict['contact_id_fkey'] = None
            txn_dict['active'] = 0
            txn_dict['guid'] = None
            return False
        else:
            return True

    def getMappingProject(self, env, rec):
        project_rec = env['isha.purpose.project.mapping'].search([('purpose', '=', rec['project']), ('entity', '=', rec['entity'])], limit=1)
        return project_rec


    def get_don_flags(self, project_rec):
        don_flags= {'is_annadhanam':False,'is_caca_donor':False}
        if len(project_rec) != 0:
            project = project_rec[0].project_name
            if project == 'Annadhanam':
                don_flags['is_annadhanam'] = True

            elif project == 'Cauvery Calling - Awareness And Project Management' \
                or project == 'Cauvery Calling - CSR Awareness And Project Management' \
                or project == 'Cauvery Calling - Tree Saplings' \
                or project == 'Cauvery Calling - CSR Tree Saplings':
                don_flags['is_caca_donor'] = True
        return don_flags


    def closeCursorWhenException(self):
        if self.CR is not None:
            self.CR.close()

