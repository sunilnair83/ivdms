# Copyright 2015 Anubía, soluciones en la nube,SL (http://www.anubia.es)
# Copyright 2017 Onestein (http://www.onestein.eu)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import logging

from odoo import api, models
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class Base(models.AbstractModel):
    _inherit = 'base'

    @api.model
    def load(self, fields, data):
        '''Overriding this method we only allow its execution
        if current user belongs to the group allowed for CSV data import.
        An exception is raised otherwise, and also log the import attempt.
        '''
        current_user = self.env.user
        allowed_group = 'base_import_security_group.group_import_csv'
        allowed_group_regional_athithi = 'isha_crm.group_athithi_regional_admin'
        allowed_group_central_athithi = 'isha_crm.group_athithi_central_admin'
        allowed_group_id = self.env.ref(
            allowed_group,
            raise_if_not_found=False
        )
        model_name = self._name
        length = len(data)
        data_updated = []
        index_of_influencer_type = 0
        if model_name == 'event.attendees':
            fields.append('system_id')
            for field in fields:
                index_of_influencer_type = index_of_influencer_type + 1
                if field == 'influencer_type':
                    index_influencer_type = index_of_influencer_type
            for i in range(0, length, 1):
                # adding system_id as 45 for event_attendees
                data[i].append('45')
                if not data[i][index_influencer_type - 1]:
                    msg = 'Influencer type should be present at row: %s' % (i + 1)
                    raise ValidationError(msg)
        if not allowed_group_id or current_user.has_group(allowed_group):
            if (current_user.has_group(allowed_group_regional_athithi) or current_user.has_group(allowed_group_central_athithi)) and model_name == 'res.partner':
                fields.append('system_id')
                for i in range(0, length, 1):
                    # adding system_id as 45 for athithi res_partner import
                    data[i].append('45')
                    for x in data[i]:
                        if x == '0':
                            data_updated.append('')
                        else:
                            data_updated.append(x)
                    data[i] = data_updated
                    data_updated = []
            res = super().load(fields=fields, data=data)
        else:
            msg = ('User (ID: %s) is not allowed to import data '
                   'in model %s.') % (self.env.uid, self._name)
            _logger.info(msg)
            raise ValidationError(msg)
            messages = []
            info = {}
            messages.append(
                dict(info, type='error', message=msg, moreinfo=None))
            res = {'ids': None, 'messages': messages}
        return res
