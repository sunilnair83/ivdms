odoo.define('web.ListImport', function (require) {
    "use strict";

    var KanbanController = require('web.KanbanController');
    var KanbanView = require('web.KanbanView');
    var ListController = require('web.ListController');
    var ListView = require('web.ListView');
    var session = require('web.session');

    var ImportViewMixin = {

    init: function (viewInfo, params) {

        var self = this
        var result = self._super.apply(self, arguments);
        var base_group = 'base_import_security_group.group_import_csv';
        var importEnabled_attr = self.controllerParams.importEnabled;
        var atithi_regional_group = 'isha_crm.group_athithi_regional_admin';
        var atithi_central_group = 'isha_crm.group_athithi_central_admin';

        session.user_has_group(atithi_regional_group).then(
        function (res){
            session.user_has_group(atithi_central_group).then(function (central){
            session.user_has_group(base_group).then(function (csv){
            if(importEnabled_attr){
                if(self.controllerParams.modelName == 'res.partner'){
                    if((res || central) & csv)
                    {self.controllerParams.importEnabled = true;}
                    else
                    {self.controllerParams.importEnabled = false;}
                    }
                else
                    if(csv)
                    {
                       self.controllerParams.importEnabled = importEnabled_attr;
                    }
                    else
                    {
                       self.controllerParams.importEnabled = false;
                    }
                }
               }); });
            });
        },

    };

    ListView.include({
    init: function () {
        this._super.apply(this, arguments);
        ImportViewMixin.init.apply(this, arguments);
        },
    });

    KanbanView.include({
    init: function () {
        this._super.apply(this, arguments);
        ImportViewMixin.init.apply(this, arguments);
        },
    });
});
