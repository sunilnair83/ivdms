import configparser
import os
from .common.crypto import is_encrypted, decrypt

WORD_SEPARATOR = "_"
SUSHUMNA_ENVIRONMENT_VARIABLE_PREFIX = "SUSHUMNA" + WORD_SEPARATOR
SUSHUMNA_CONFIG_KEY = SUSHUMNA_ENVIRONMENT_VARIABLE_PREFIX + "CONFIG"
SUSHUMNA_CONFIG_SECRET = SUSHUMNA_CONFIG_KEY + WORD_SEPARATOR + "SECRET"


class Configuration:
    __config = None
    __secret = None
    __sections = {}

    def __new__(cls, section):

        # Initialize the config parser object based on the Configuration file
        if Configuration.__config is None:
            # Read teh necessary environment variables
            config_file_name = os.environ.get(SUSHUMNA_CONFIG_KEY, None)

            # Create a config parser instance for the configuration file
            if config_file_name is None or not os.path.exists(config_file_name):
                raise EnvironmentError(
                    '\n\nSushumna Configuration file [' + str(config_file_name) + '] was not found.' +
                    'Please ensure the key [' + str(SUSHUMNA_CONFIG_KEY) + '] is defined as an ' +
                    'environment variable and points to a valid configuration.')

            # Store the decryption secret for reference
            Configuration.__secret = os.environ.get(SUSHUMNA_CONFIG_SECRET, None)

            Configuration.__config = configparser.ConfigParser()
            Configuration.__config.read(config_file_name)

        # Create singleton instance for the section if one dos not exist
        if section not in Configuration.__sections:
            # Initialize instance and set an empty cache
            instance = object.__new__(cls)
            instance._cache = {}
            instance._section_name = section
            instance._config_section = Configuration.__config[section]

            # Add instance to singleton directory
            Configuration.__sections[section] = instance

        return Configuration.__sections[section]

    def __getitem__(self, item):
        """ Gets a particular key in the configuration """

        if item not in self._cache:
            self._cache[item] = self.get(item)

        return self._cache[item]

    def get(self, key, default=None):
        """ Gets a particular configuration key from the section """

        value = self.get_from_environment(key)
        value = value if value else self._config_section.get(key, default)

        # Decrypt value if encrypted
        if value is not None:
            value = value if not is_encrypted(value) else decrypt(value, self.__secret)

        return value

    def getboolean(self, item):
        self._cache[item] = self._config_section.getboolean(item)
        return self._cache[item]

    @staticmethod
    def get_from_environment(key):
        """ Gets a configuration key from the environment by adding the environment prefix """

        environment_key = SUSHUMNA_ENVIRONMENT_VARIABLE_PREFIX + key
        return os.environ.get(environment_key, None)
