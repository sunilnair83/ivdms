# -*- coding: utf-8 -*-
{
    'name': "Isha Add-on Base",

    'summary': """
        Contains the common foundation that all Isha Addons share""",

    'description': """
        Include the following components:
            - Configuration Management
            - Secrets / Key management
    """,

    'author': "Isha IT Apps Team",
    'website': "www.ishafoundation.org",
    'category': 'app',
    'version': '13.0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],
}