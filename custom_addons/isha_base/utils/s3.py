from boto3.session import Session


class S3Bucket:
    __instance = None
    _resource = None
    _bucket = None

    def __new__(cls, bucket_name, access_key, secret_key):
        """
        Singleton implementation for S3 Bucket
        :param bucket_name:
        :param access_key:
        :param secret_key:
        :return:
        """

        if S3Bucket.__instance is None:
            instance = object.__new__(cls)
            session = Session(access_key, secret_key)
            instance._resource = session.resource('s3')
            instance._bucket = instance._resource.Bucket(bucket_name)

            if instance._bucket.creation_date is None:
                return None

            S3Bucket.__instance = instance

        return S3Bucket.__instance

    def __contains__(self, item):
        """
        Checks if an object by the key exists in the bucket
        :param item: Object key
        :return: True if object exists, false otherwise
        """
        objs = list(self._bucket.objects.filter(Prefix=item))
        return len(objs) > 0 and objs[0].key == item

    def __getitem__(self, item):
        """
        Retrieves the object by key as a byte array
        :param item:
        :return:
        """
        try:
            return self._bucket.Object(item).get()['Body'].read()
        except Exception:
            return None

    def __setitem__(self, key, value):
        """
        Stores an object byte array by key
        :param key: Key for object
        :param value: Object data as byte array
        """

        if not isinstance(value, bytes):
            ValueError("The supplied value for S3 storage must be 'bytes'")

        self._bucket.put_object(Key=key, Body=value)

    def __delitem__(self, key):
        """
        Deletes an object from the S3 bucket by key
        :param key: Key of the object
        """

        try:
            self._bucket.Object(key).delete()
        except Exception:
            pass
