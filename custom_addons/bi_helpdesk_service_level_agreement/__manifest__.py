# -*- coding: utf-8 -*-
# Part of Browseinfo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Website HelpDesk Service Level Agreement-Helpdesk SLA Odoo',
    'version': '13.0.0.2',
    'category': 'website',
    'sequence': 100,
    'summary': 'Website helpdesk SLA website support SLA service level SLA support SLA service SLA Help desk Service Level Agreement customer Service Level Agreement Helpdesk SLA Level Customer Helpdesk Support Ticket website support ticket support Issue tracking system',
    'description': """bi_helpdesk_service_level_agreement
	
	his app allow your helpdesk team to have helpdesk service level agreement(SLA) on Help Desk.

service level SLA
helpdesk SLA
SLA helpdesk
support SLA
SLA
Service Level Agreement
Service-Level Agreement
Helpdesk Service Level Agreement
helpdesk Service-Level Agreement
helpdesk SLA
help desk SLA
SLA level
support Service-Level Agreement

customer SLA
client Sla
client Service-Level Agreement

Main Features:
- Allow to you to setup SLA Levels under configuration.

- Allow you to link SLA level on your Customer form.
client contracting in Odoo 
customer contracting 
service contracting 
customer service contract
contracting in Odoo
conctract with client 
helpdesk contract




SLA/Helpdesk SLA Levels
Helpdesk SLA Analysis
Helpdesk SLA Team
Helpdesk SLA Levels
	
	
	""",
    'author': 'BrowseInfo',
    'website' : 'https://www.browseinfo.in',
    'price': 49,
    'currency': 'EUR',
    'depends': ['base','website',
                # 'website_sale',
                'bi_website_support_ticket'],
    'data':[
    'security/ir.model.access.csv',
    'views/service_level_agreement_view.xml',
    'data/cron.xml',
    'data/mail_templates.xml',
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'live_test_url':'https://youtu.be/GXv1aaeTBhY',
    'images':['static/description/Banner.png'],
}
