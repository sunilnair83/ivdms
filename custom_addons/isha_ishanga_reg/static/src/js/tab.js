$(document).ready(function() {

	var regexCustomAmount = /^[0-9]*$/;
	$("#income").keyup(function() {

	calculate7PercentIncome();

	});
	$(".fred").change(function() {
	changePaymentType();
	});

    $('.nu_donation_form').submit(function(e) {
     $('#recurring_amount_error_msg').html('');
    var currency =$("#hidden_donation_currency").val();
    var hidden_amount =$("#hidden_donation_amount").val();
    var donation_type = $("input[name='freq']:checked").val();
    var isRecurring =donation_type;
        var amount =0;
        if(donation_type==1){
        amount =$(".custom_amount_rr").val();
        }
        else{
         amount =$(".custom_amount").val();
        }
       amount=hidden_amount
       $("#hidden_donation_amount").val(amount);
        if(isRecurring == 1)
    {
      if(currency == 'USD'){
        minAmt = 25;
        currencySymbol = currency;
      }else{
        minAmt = 200;
        currencySymbol = '₹';

      }


    if(hidden_amount < minAmt){
      $('html, body').animate({
        scrollTop: $('.amount-blog').offset().top
      }, 500);
      $('#recurring_amount_error_msg').html('Minimum amount must be '+currencySymbol+' '+minAmt);
      return false;
    }
  }else{
      var ishangaFlag = $('#ishanga_flag').val();
      if(currency == 'INR'){
        if(ishangaFlag == 'N'){
          minAmt = 1;
        }else{
          minAmt = 1;
        }
        currencySymbol = '₹';
      }else{
        minAmt = 1;
        currencySymbol = currency;
      }
      if(hidden_amount < minAmt){
      $('#error_donation_amt_msg').html('Minimum amount must be '+currencySymbol+' '+minAmt);
      return false;
    }

  }
    });
  function reset_prefilled_data(){
  		$('#display_donation_msg').html('');
		$('#donation_currency option[value=INR]').removeAttr("selected", true).change();
		$('#donation_currency option[value=INR]').attr("selected", true).change();
		$('#income').val('');
		$('#income_precent').html('');
		$('#custom_amount').val('');
		$('#custom_amount_rr').val('');
  }
  function changePaymentType(){
  		// reset the prefilled data
		reset_prefilled_data();

        var donation_type = $("input[name='freq']:checked").val();
        if(donation_type==1){
                $("#recurring_donation_patch").css({"display":"block"});
                $("#one_time_amount_block").css({"display":"none"});
                $(".caca-recurring").show();
                $("#donation_type").val(1);
                $("#donation_currency option[value='USD']").hide();
                $("#donation_currency").val("INR");
                changeCurrency();

        }
        if(donation_type==0){
                $("#recurring_donation_patch").css({"display":"none"});
                $("#one_time_amount_block").css({"display":"block"});
                $(".caca-recurring").hide();
                $("#donation_type").val(0);
                $("#donation_currency option[value='USD']").show();
                changeCurrency();
        }

    }

    $("#donation_currency").change(function(){
    changeCurrency();
    });
	function changeCurrency(){
	        $('#display_donation_msg').html('');
            $('#hidden_donation_amount').val('');
            $('#error_donation_amt_msg').html('');
            $('.custom_amount').val('');
            $('.custom_amount_rr').val('');
            var currency = $("#donation_currency").val();
            if(currency == 'INR'){
                  $('.rupees').html('INR');
                  document.getElementById("hidden_donation_currency").value = currency;

              }else if(currency == 'USD'){
                  $('.rupees').html('USD');
                  document.getElementById("hidden_donation_currency").value = currency;

              }

	}
    $(".custom_amount").keyup(function(){
        changeCustomAmount("custom_amount");

    });
    $(".custom_amount_rr").keyup(function(){
        changeCustomAmount("custom_amount_rr");

    });
	function changeCustomAmount(cla_name) {

	    var customAmt = $("."+cla_name+"").val();
         customAmt = parseInt(customAmt);
         document.getElementById("display_donation_msg").style.display='none';
         document.getElementById("display_donation_msg").innerHTML="";
        var currency = $("#donation_currency").val();
        var dayOfMonth = $("#dayOfMonth").val();
        var donation_type = $("input[name='freq']:checked").val();
        if(customAmt!=""&& customAmt!='NaN' && customAmt!='undefined'){
           if(currency == 'INR'){
              var num = customAmt.toLocaleString('en-IN', {
                style: 'currency',
                currency: 'INR'
              });
              num = num.replace(".00", " ");
              num = num.replace("₹", " ");
              var donationMsg = 'Your Contribution of ₹ '+num+' is greatly appreciated';
            }else if(currency == 'USD'){
              var nf = new Intl.NumberFormat();
              var num = nf.format(customAmt);
              var donationMsg = 'Your Contribution of $ '+num+' is greatly appreciated';
            }
            if(donation_type==1){
                var donationMsg = 'Donate ₹ '+num+' monthly on the '+dayOfMonth+ 'th of each month until cancelled by you';
            }
          document.getElementById("display_donation_msg").style.display='block';
          document.getElementById("display_donation_msg").innerHTML=donationMsg;
          document.getElementById("hidden_donation_msg").value = donationMsg;
          document.getElementById("hidden_donation_amount").value = customAmt;
          document.getElementById("hidden_donation_currency").value = currency;
          }
	}
    // Add cal for 7% on salary cal
	function calculate7PercentIncome() {
		var amount = $('#income').val();
		var cal_amt=0;
		var donation_type = $('#donation_type').val();
		var selectedDonationCurrency = $('#donation_currency').val();
		if (amount != '') {
			var percentEarnings = Math.round((amount * 7) / 100);
			if (selectedDonationCurrency == 'INR') {
				var currencySymbol = '₹';
			} else if (selectedDonationCurrency == 'USD') {
				var currencySymbol = '$';
			}
			$('#income_precent').html(currencySymbol + ' ' + percentEarnings);
            if(donation_type==0)
			    $('.custom_amount').val(percentEarnings);
			else
			  $('.custom_amount_rr').val(percentEarnings);
		} else {
			$('#income_precent').html('');
			$('#custom_amount').val('');
		}
		if(donation_type==0){
		changeCustomAmount('custom_amount');
		}
		else{
		changeCustomAmount('custom_amount_rr');
		}

	}

	 function check80GTax(){
    $("#want_80G_error_msg").html("");
    var isChecked = $("input[name='want_80G']:checked").val();
    var citizenship = $("input[name='nationality']:checked").val();
     var donationVal = $("#hidden_donation_amount").val();
     var donatePurposeId = document.getElementById('hidden_donate_purpose_id').value;
     var currency = $('#hidden_donation_currency').val();
     var isRecurring = $('#is_recurring').val();

     if(isRecurring == '1'){
        if(currency == 'INR'){
          var NoOfInstallments = $("#no_of_months").val();
          var finalAmount = parseInt(donationVal) * parseInt(12);
        }else{
          var convertedAmount = $('#converted_amount').val();
          var finalAmount = parseInt(convertedAmount) * parseInt(12);
        }
      }else{
        if(currency == 'INR'){
          var finalAmount = donationVal;
        }else{
          var finalAmount = $('#converted_amount').val();
        }
      }

    if(isChecked == "yes" ){
      $("#no_80G_consent").prop("checked", false);
      //document.getElementById('want_80g_exemtion_msg').style.display = "block";
      document.getElementById('pancard_input').style.display = "block";

      if(finalAmount < 50000 || citizenship == 'no'){
        document.getElementById('aadhar_checkbox').style.display = "block";
      }
        $("input[name=show_aadhar]").attr('checked', false);
       document.getElementById('aadhar_input').style.display = "none";
      document.getElementById('agree_80g_tax_div').style.display = "none";
    }else if(isChecked == "no"){
      //document.getElementById('want_80g_exemtion_msg').style.display = "none";
      $('#adharcard').val('');
      $('#adharcard_error_msg').html("");

      // if((citizenship == 'yes' && finalAmount >= 50000) || donatePurposeId == 'SGMT'){
      //   $('#no_80G_consent_msg').html('I hereby declare that I do not wish to avail 80G donation');
      // }else{
      //   $('#no_80G_consent_msg').html('I hereby declare that I do not wish to avail 80G donation and hence I am not providing my PAN / Aadhar');
      // }

      if((finalAmount < 50000 && donatePurposeId != 'SGMT') || citizenship != 'yes'){
        $('#pancard').val('');
        $('#pancard_error_msg').html("");
        document.getElementById('pancard_input').style.display = "none";
      }else if(donatePurposeId == 'SGMT' && $('#pancard').val() == ''){
        $('#pancard_input').removeClass('error');
      }
      document.getElementById('aadhar_input').style.display = "none";
      $("input[name=show_aadhar]").attr('checked', false);
      document.getElementById('aadhar_checkbox').style.display = "none";
      if(citizenship == 'yes'){
        document.getElementById('agree_80g_tax_div').style.display = "block";
      }
    }

  }


});
