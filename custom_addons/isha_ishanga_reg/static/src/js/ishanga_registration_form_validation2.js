// load the state while load the page when pincode is entred
$(document).ready(function () {
     var fixState = false;
     var in_pincode_regx =/^(\+\d{1,3}[- ]?)?\d{6}$/;
     var all_pincode_regx =/^[ A-Za-z0-9\-\ ]{3,10}$/;
     var regexAddress = /^[ A-Za-z0-9\-\/&_ ,.#()]{10,}$/;
     var regexcity = /^[a-zA-Z ]+$/u;
  $('#state_name_text').change(function(){
      validate_state_name();

  });
  function validate_state_name()
  { var inputtxt=  $('#state_name_text').val()
  var letters = /^[A-Za-z]+$/;
  $('#state_name_aplha_error').css({"display":"none"});
  if(inputtxt.match(letters))
  {

      return true;
}
  else
  {
     $('#state_name_aplha_error').css({"display":"block"});
    return false;
  }
}
     function is_pan_valid(){
        var is_valid =true;
         var aadhar_input = $('#pancard').val();

        if ($('#pancard').hasClass('input_required')){
         if(aadhar_input!=""){
             var regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
             if(regex.test(aadhar_input)){
                 is_valid =true;
                 }
             else{
                 $('#pan_err').css({"display":"block"});
                 is_valid =false;
                 }
             }
             else{

              $('.pancard_error_msg').css({"display":"block"});
                    is_valid =false;
             }
             }
             return is_valid;
     }
     function is_aadhar_valid(){
       var is_valid =true;
         var aadhar_input = $('#aadhar_input').val();
         if ($('#aadhar_input').hasClass('input_required')){
         if(aadhar_input!=""){
             var regex = /[0-9]{12}/;
             if(regex.test(aadhar_input)){
                 is_valid =true;
                 }
             else{
                 $('#aadhar_err').css({"display":"block"});
                  is_valid =false;
                 }
             }
             else{

                $('#adharcard_error_msg').css({"display":"block"});
                  is_valid =false;

             }
             }
             return is_valid;
     }

     function is_passport_valid(){
         var is_valid =true;
         var passport_number = $('#passport_number').val();
         //no Passport valiation needed
//         if ($('#passport_number').hasClass('input_required')){
//         if(passport_number!=""){
//             var regex =  /^[A-PR-WY][1-9]\d\s?\d{4}[1-9]$/ig;
//             if(regex.test(passport_number)){
//                 is_valid =true;
//                 }
//             else{
//                // $('#valid_passport_error_msg').css({"display":"block"});
//                  is_valid =true;
//                 }
//             }
//             else{
//                    $('#passport_number_error_msg').css({"display":"block"});
//                  is_valid =false;
//             }
//             }
             return is_valid;
     }
     $('#no_80G_consent').change(function(){
     var is_cc =$('#no_80G_consent').is(':checked');
     if (is_cc)
     {  $('#no_80G_consent').val('on');
     }
     else{
      $('#no_80G_consent').val('');
     }

     });

    function address_validation(){
    var is_proceed =true;
    var add_line1 = $("#addessline1").val();
    var city = $("#city").val();
    var zip_code = $("#pincode").val();
    $('.zip_error_msg1').css({"display":"none"});
    $('.zip_error_msg2').css({"display":"none"});
    $('#address2_error_msg').css({"display":"none"});
    $('#city_error_msg1').css({"display":"none"});

    if (!regexAddress.test(add_line1))
    {  is_proceed =false;
        $('#address2_error_msg').css({"display":"block"});
    }
    if (!regexcity.test(city))
    {  is_proceed =false;
     $('#city_error_msg1').css({"display":"block"});
    }
    var country_code =$("#selected_country option:selected").attr('code');
    if (country_code=="IN"){
         if(!in_pincode_regx.test(zip_code))
            {  is_proceed =false;

             $('.zip_error_msg1').css({"display":"block"});
            }
    }

    else{
        if(!all_pincode_regx.test(zip_code))
            {  is_proceed =false;
             $('.zip_error_msg2').css({"display":"block"});
            }

    }
    if (is_proceed)
      {
      if($('#state_name_text').val()!=""){
      is_proceed =validate_state_name();
      }
      }

    return is_proceed;


    }
     $('#person_detial_formsubmit').click(function(e){
         valid = true;
         console.log("clicked");
         var is_proceed =$('#is_proceed').val();
         if(is_proceed=='no'){
         e.preventDefault();

         var ddd =0;
         $( "input[class='input_required']").each(function( index ) {
              console.log( index + ": " + $( this ).text() );
              var val=$(this).val();
              var id_attr=$(this).attr('id');
             if(val=="")
              {
                ddd=ddd+1;
                var err_class = "."+id_attr+'_input_required_msg';
                 $(err_class).css({
                   "display": "block"
              });


              }

            });
            $( "select[class='input_required']").each(function( index ) {
              console.log( index + ": " + $( this ).text() );
              var val=$(this).val();
              var id_attr=$(this).attr('id');
             if(val=="")
              {
                ddd=ddd+1;
                var err_class = "."+id_attr+'_input_required_msg';
                 $(err_class).css({
                   "display": "block"
              });


              }

            });
        console.log(ddd);
        if(ddd>0)
            valid = false;
        var want_80G_yes = $('#want_80G_yes').is(':checked')
        var want_80G_no = $('#want_80G_no').is(':checked')
        if(want_80G_yes == false && want_80G_no == false)
        {
            $('.want_80G_error_msg').css({
                "display": "block"
            });
            valid = false;
        }
         var nationality_yes = $('#nationality_yes').is(':checked')
        var nationality_no = $('#nationality_no').is(':checked')
        if(nationality_no == false && nationality_yes == false)
        {
            $('.nation_error_msg').css({
                "display": "block"
            });
            valid = false;
        }
        var agree_tnc = $('#agree_tnc').is(':checked');
        if(!agree_tnc){
             $('.agree_tnc_error_msg').css({
                "display": "block"
            });
            valid = false;
        }
       var is_panvalid = is_pan_valid();
       var is_aadharvalid = is_aadhar_valid();
       var is_passportvalid = is_passport_valid();
       var is_address_valid = address_validation();

        console.log(is_panvalid);
        if(valid){
            var dd = $('#passport_image_file').val();
            $('.passport_error_msg').css({"color":"black"});

            if(dd!="")
            {
                 if (/\s/.test($('#passport_image_file')[0].files[0].name)) {
                    $('.passport_error_msg').css({"color":"red"});
                    $('.passport_error_msg').css({"display":"block"});
                    valid= false;
                    }
                var file_size = $('#passport_image_file')[0].files[0].size/1024;
                if (file_size>500){
                    $('.passport_error_msg').css({"display":"block"});
                    $('.passport_size_msg').css({"color":"red"});
                    valid= false;
                    }
            }
        }
        console.log("eeee");
        console.log(valid);

        if (is_address_valid && valid && is_panvalid && is_aadharvalid && is_passportvalid)
        {
          console.log("proced form submit");
        $('.personal_details_form').submit();

        }


}
else
{ return true;
}

  });


    function getBase64(file) {
   var reader = new FileReader();
   reader.readAsDataURL(file);
   reader.onload = function () {
     console.log(reader.result);
     var img_str = reader.result;
     var bb_str=img_str.split(',');
     $('#upload_file_text').val(bb_str[1]);

   };
   reader.onerror = function (error) {
     console.log('Error: ', error);
   };
}

    function validationonPANadhar()
    {
        var amt = $("#overall_amount_inr").val();
        var want_80G_yes = $('#want_80G_yes').is(':checked');
        var want_80G_no = $('#want_80G_no').is(':checked');
        var nationlity_yes = $('#nationality_yes').is(':checked');
        var nationlity_no = $('#nationality_no').is(':checked');
        $('#pancard').removeClass('input_required');
        $('#aadhar_input').removeClass('input_required');
        $('#passport_number').removeClass('input_required');
        $('#no_80G_consent').removeClass('input_required');
        $('#pan_card_input').css({"display":"none"});
         $('#agree_80g_tax_div').css({"display":"none"});
         $('#pan_terms_checkbox').css({"display":"none"});


        if(amt>=50000)
        {
               if (nationlity_yes==true && want_80G_yes==true)
               {    $('#pan_card_input').css({"display":"block"});
                    $('#pancard').addClass('input_required');
               }
                if (nationlity_yes==true && want_80G_no==true)
               {    $('#pan_card_input').css({"display":"block"});
                    $('#pancard').addClass('input_required');
                    $('#agree_80g_tax_div').css({"display":"block"});
                    $('#no_80G_consent').addClass('input_required');
               }
               if (nationlity_no==true && want_80G_yes==true)
               {   $('#pan_card_input').css({"display":"block"});
                   $('#pan_terms_checkbox').css({"display":"block"});
                    $('#pancard').addClass('input_required');
               }
                if (nationlity_no==true && want_80G_no==true)
                {  $('#pan_card_input').css({"display":"none"});
                   $('#pan_terms_checkbox').css({"display":"none"});
                   $('#pancard').removeClass('input_required');
                   $('#agree_80g_tax_div').css({"display":"block"});
                   $('#no_80G_consent').addClass('input_required');
               }


        }

        if(amt<50000)
        {
             if (nationlity_yes==true && want_80G_yes==true)
               {  $('#pan_card_input').css({"display":"block"});
                  $('#pan_terms_checkbox').css({"display":"block"});
                    $('#pancard').addClass('input_required');
               }
               if (nationlity_yes==true && want_80G_no==true)
               {    $('#agree_80g_tax_div').css({"display":"block"});
                    $('#no_80G_consent').addClass('input_required');
               }
               if (nationlity_no==true && want_80G_yes==true)
               {   $('#pan_card_input').css({"display":"block"});
                   $('#pan_terms_checkbox').css({"display":"block"});
                    $('#pancard').addClass('input_required');
               }
                if (nationlity_no==true && want_80G_no==true)
                {  $('#pan_card_input').css({"display":"none"});
                    $('#pan_terms_checkbox').css({"display":"none"});
                    $('#pancard').removeClass('input_required');
                    $('#agree_80g_tax_div').css({"display":"block"});
                   $('#no_80G_consent').addClass('input_required');
               }

        }
    }


    // Pan Card Validation
    function valid_pancard(){
        var show_aadhar_bool = $('#show_aadhar').is(':checked');
        if(show_aadhar_bool == false){
            var pancard = $("#pancard").val();
            var regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
            if(!pancard){
                $('.pancard_error_msg').css({
                    "display": "block"
                });
                valid = false;
            }else{
                 $('.pan_err').css({
                    "display": "none"
                });
                 $('.pancard_error_msg').css({
                    "display":"none"
                });
                valid = false;
            }
        }
    }

    // Load Profile Logic
    var fixState = false;
    load_profile();
    var jspgremstatecoll =[];
    var jspgremcountrycoll =[];
    $("#selected_fcra_nationality").trigger('change');
    $("#countrycode").trigger('click');
    $("#current_country").trigger("click");
    $('#selected_state option').each(function(ivar,jvar){
        var objvar ={
            "valueattr":$(jvar).attr("value"),
            "statenameattr":$(jvar).attr("statename")
        };
        jspgremstatecoll.push(objvar);
    });
    $('#selected_fcra_nationality option').each(function(ivar,jvar){
        var objvar ={
            "valueattr":$(jvar).attr("value"),
            "ccattr":$(jvar).attr("ccatr")
        };
        jspgremcountrycoll.push(objvar);
    });

    // AAdhar Card Validation
    function valid_aadhar_Card(){
        var show_aadhar_bool = $('#show_aadhar').is(':checked');
        $('#aadhar_card').attr("required",false);
        if(show_aadhar_bool == true){
            $('#aadhar_card').attr("required",true);
            var aadhar_card_vals = $("#aadhar_card").val();
            var regex = /[0-9]{12}/;
            if(!aadhar_card_vals){
                $('.adharcard_error_msg').css({
                    "display": "none"
                });
                valid = false;
            }else if(!regex.test(aadhar_card_vals)){

                $('#aadhar_card').val($('#aadhar_card').val().toUpperCase());
                $('.aadhar_err').css({"display":"block"});
                valid = true;
            }else{
                $('.aadhar_err').css({"display":"none"});
                valid = false;
            }
        }
    }

    // valid_want80g validation
    function valid_want80g(){
        var want_80G_yes = $('#want_80G_yes').is(':checked')
        var want_80G_no = $('#want_80G_no').is(':checked')
        if(want_80G_yes == false && want_80G_no == false)
        {
            $('.want_80G_error_msg').css({
                "display": "block"
            });
            valid = false;
        }
        else{
            $('.want_80G_error_msg').css({
                "display": "none"
            });
            valid = true;
        }
    }

    // valid_citizen_check validation
    function valid_citizen_check(){
        var nationality_yes = $('#nationality_yes').is(':checked')
        var nationality_no = $('#nationality_no').is(':checked')
        if(nationality_yes == false && nationality_no == false)
        {
            $('.nation_error_msg').css({
                "display": "block"
            });
            valid = false;
        }
        else{
            $('.nation_error_msg').css({
                "display": "none"
            });
            valid = true;
        }
    }

    $('.passport_image_input').css({"display":"none"});
    $('#passport_image_file').css({"display":"none"});

    $( "#agree_tnc" ).change(function(){
        agree_tnc_bool = $('#agree_tnc').is(':checked');
        if(agree_tnc_bool == false){
             $('.agree_tnc_error_msg').css({
                    "display": "block"
             });
            return false;
        }else{
            $('.agree_tnc_error_msg').css({
                    "display": "none"
             });
            return true;
        }
    });
    $( "#no_80G_consent").change(function(){
        no_80G_consent_bool = $('#no_80G_consent').is(':checked');
        if(no_80G_consent_bool == false){
             $('.agree_no80g_consent_error_msg').css({
                    "display": "block"
             });
            return false;
        }else{
            $('.agree_no80g_consent_error_msg').css({
                    "display": "none"
             });
              $('.no_80G_consent_input_required_msg').css({
                    "display": "none"
             });

            return true;
        }

    });

    // fname Validation
    $( "#first_name" ).blur(function(){
        var f_name = $("#first_name").val();
        var regex = /^[A-Z]+$/i;
        if(!f_name){
              $('.name_error_msg').css({
                   "display": "block"
              });
              return false;
        }else if(!regex.test(f_name)){
             $('.name_error_msg').css({
                "display": "none"
            });
            $('.fname_error_msg').css({
                "display":"block"
            });
            return false;
        }else{
             $('.name_error_msg').css({
                "display": "none"
            });
            $('.fname_error_msg').css({
                "display":"none"
            });
            return false;
        }
    });

    // lastname Validation
    $( "#last_name" ).blur(function(){
        var last_name = $("#last_name").val();
        var regex = /^[A-Z]+$/i;
        if(!last_name){
              $('.lname_error_msg').css({
                   "display": "block"
              });
              return false;
        }else if(!regex.test(last_name)){
             $('.lname_error_msg').css({
                "display": "none"
            });
            $('.lname_error_msg1').css({
                "display":"block"
            });
            return false;
        }else{
             $('.lname_error_msg').css({
                "display": "none"
            });
            $('.lname_error_msg1').css({
                "display":"none"
            });
            return false;
        }
    });

    // Email Validation
    $( "#email" ).blur(function(){
        var email = $("#email").val();
        var regexEmail = /([a-zA-Z0-9][-a-zA-Z0-9_\+\.]*[a-zA-Z0-9])@([a-zA-Z0-9][-a-zA-Z0-9\.]*[a-zA-Z0-9]\.(arpa|root|aero|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw|life)|([0-9]{1,3}\.{3}[0-9]{1,3}))$/i;
        if(!email){
              $('.email_error_msg').css({
                   "display": "block"
              });
              return false;
        }else if(!regexEmail.test(email)){
             $('.email_error_msg').css({
                "display": "none"
            });
            $('.email_error_msg1').css({
                "display":"block"
            });
            return false;
        }else{
             $('.email_error_msg').css({
                "display": "none"
            });
            $('.email_error_msg1').css({
                "display":"none"
            });
            return false;
        }
    });
    $( "#country_code" ).blur(function(){
        var country_code = $("#country_code").val();
        if(!country_code){
              $('.country_error_msg').css({
                   "display": "block"
              });
              return false;
         }else{
             $('.country_error_msg').css({
                "display": "none"
            });
            return false;
        }
    });
    // Mobile Number Validation
    $( "#contact_no" ).blur(function(){
        var contact_no = $("#contact_no").val();
        var regexContactNo = /^\d{0,15}$/;
        if(!contact_no){
              $('.number_error_msg').css({
                   "display": "block"
              });
              return false;
        }else if(!regexContactNo.test(contact_no)){
             $('.number_error_msg').css({
                "display": "none"
            });
            $('.number_error_msg1').css({
                "display":"block"
            });
            return false;
        }else{
             $('.number_error_msg').css({
                "display": "none"
            });
             $('.number_error_msg1').css({
                "display":"none"
            });
            return false;
        }
    });

    // Pan Card Number Validation
    $( "#pancard" ).blur(function(){
        var pancard = $("#pancard").val();
        var regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
        if(!pancard){
            $('.pancard_error_msg').css({
                "display": "block"
            });
            return false;
        }else if(!regex.test(pancard)){
            $('.pancard_error_msg').css({
                "display": "none"
            });
            $('#pancard').val($('#pancard').val().toUpperCase());
            $('.pan_err').css({
                "display":"block"
            });
            return false;
        }else{
             $('.pan_err').css({
                "display": "none"
            });
             $('.pancard_error_msg').css({
                "display":"none"
            });
            return false;
        }
    });
    // Pass Number Validation
    // hide the passport validation
//    $( "#passport_number" ).blur(function(){
//        var passport_number = $("#passport_number").val();
//        var regex =  /^[A-PR-WY][1-9]\d\s?\d{4}[1-9]$/ig;
//        if(!passport_number){
//            $('.passport_number_error_msg').css({
//                "display": "block"
//            });
//            return false;
//        }else if(!regex.test(passport_number)){
//             $('.passport_number_error_msg').css({
//                "display": "none"
//            });
////            $('.valid_passport_error_msg').css({
////                "display":"block"
////            });
////            return false;
//        }else{
//             $('.passport_number_error_msg').css({
//                "display": "none"
//            });
////             $('.valid_passport_error_msg').css({
////                "display":"none"
////            });
////            return false;
//        }
//    });

    // Aadhar validation
    $( "#aadhar_card" ).blur(function(){
        var aadhar_input = $("#aadhar_card").val();
        var regex = /[0-9]{12}/;
        if(!aadhar_input){
            $('.adharcard_error_msg').css({
                "display": "block"
            });
            return false;
        }else if(!regex.test(aadhar_input)){
            $('.adharcard_error_msg').css({
                "display": "none"
            });
            $('#aadhar_card').val($('#aadhar_card').val().toUpperCase());
            $('.aadhar_err').css({"display":"block"});
            return false;
        }else{
            $('.adharcard_error_msg').css({
                "display": "none"
            });
            $('.aadhar_err').css({"display":"none"});
            return false;
        }
    });

    $("#show_aadhar").on("click",function() {
        var show_aadhar = $('#show_aadhar').is(':checked');
        $('.pan_err').css({"display":"none"});
        $('.pancard_error_msg ').css({"display":"none"});
        if(show_aadhar==true){
            $('#pancard').val("");
            $('#pancard').removeClass("input_required");
            $('#aadhar_card').addClass("input_required");
            $('#aadhar_card').val("");
            $('#pancard').attr("required",false);
            $('#aadhar_card').attr("required",true);
            $('#aadhar_input').css({"display":"block"});

        }
        else{
            $('#pancard').addClass("input_required");
             $('#aadhar_card').removeClass("input_required");
            $('#pancard').attr("required",true);
            $('#aadhar_card').attr("required",false);
             $('#aadhar_input').css({"display":"none"});
        }


    });
    // City validation
    $( "#city" ).blur(function(){
        var city = $("#city").val();
        var regexCity = /^[a-zA-Z ]+$/u;
        if(!city){
              $('.city_error_msg').css({
                   "display": "block"
              });
              return false;
        }else if(!regexCity.test(city)){
            $('.city_error_msg').css({
                "display": "none"
            });
            $('.city_error_msg1').css({"display":"block"});
            return false;
        }else{
            $('.city_error_msg').css({
                "display": "none"
            });
            $('.city_error_msg1').css({
                "display":"none"
            });
            return false;
        }
    });
    // Address Line validation
    $( "#addessline1" ).blur(function(){
        var addessline1 = $("#addessline1").val();
        var regexAddress = /^[ A-Za-z0-9\-\/&_ ,.#()]{10,}$/;
        if(!addessline1){
              $('.address1_error_msg').css({
                   "display": "block"
              });
              return false;
        }else if(!regexAddress.test(addessline1)){
            $('.address1_error_msg').css({
                "display": "none"
            });
            $('.address2_error_msg').css({"display":"block"});
            return false;
        }else{
             $('.address1_error_msg').css({
                "display": "none"
            });
            $('.address2_error_msg').css({
                "display": "none"
            });
            return false;
        }
    });
    $( "#pincode" ).blur(function(){
        var pincode = $("#pincode").val();
        var regexZipCodeIndia = /^(\+\d{1,3}[- ]?)?\d{6}$/;
        var selected_country_code = $("#selected_country option:selected").attr('code');
        if(!pincode){
              $('.zip_error_msg').css({
                   "display": "block"
              });
              return false;
        }
        else if(selected_country_code=="IN" && !regexZipCodeIndia.test(pincode)){
            $('.zip_error_msg').css({
                "display": "none"
            });
            $('.zip_error_msg1').css({
                "display":"block"
            });
            return false;
        }
        else{
            $('.zip_error_msg').css({
                "display": "none"
            });
            $('.zip_error_msg1').css({
                "display":"none"
            });
            return false;
        }
    });

    $( "#nationality" ).blur(function(){
        if($('#nationality').not(':checked'))
        {
             $('.nation_error_msg').css({
                   "display": "block"
              });
              return false;
        }else{
             $('.nation_error_msg').css({
                "display": "none"
            });
            return false;
        }
    });

     $('#selected_country').change(function() {
        fixState = true;
         $("#passport_image_file").val('');
        load_country_states();
        validation_country();
    });
    $(".nationality").change(function() {
	checkNationality();
	});
    function checkNationality(){
     console.log("fdff");
      var isChecked = $("input[name='nationality']:checked").val();
      console.log(isChecked);
        if(isChecked=="Yes")
         {
            $('#show_address_msg').html('Entering an Indian address is recommended');
         }
         else{
           $('#show_address_msg').html('Entering a non-indian address is mandatory');

         }

    }
    function validation_country(){
          $('.country_validation_message').css({
                "display": "none"
            });
        var country = $('#selected_country option:selected').val();
        var region_id =$('#selected_country option:selected').attr('region_id');
        var na_region_id =$('#na_region_id').val();
        if (na_region_id==region_id){
            $('.country_validation_message').css({
                "display": "block"
            });
            $(".submit_button_hide").attr("disabled", true);
            return false;
        }else{
            $(".submit_button_hide").attr("disabled", false);
        }

        var nationality = $("#selected_fcra_nationality").val();
        var selected_country = $("#selected_country").val();
        var selected_country_code = $("#selected_country option:selected").attr('code');
        $('#passport_info_msg').css({"display":"none"});
        $('#addessline1').attr('minlength', '10');
        if(selected_country_code=="IN")
        {
        $('#pincode').attr('maxlength', '6');
        $('#pincode').attr('minlength', '6');
        $('#pincode').attr('pattern', '^[a-zA-Z0-9]*$');
        }
        else{
            $('#pincode').attr('maxlength', '10');
            $('#pincode').attr('minlength', '3');
            $('#pincode').attr('pattern', '^[a-zA-Z0-9]*$');
        }
        if(selected_country_code != 'IN' && !nationality){
           $('.passport_image_input').css({"display":"block"});
           $('#passport_image_file').css({"display":"block"});
           $('#passport_image_file').addClass("input_required");
           $('#passport_image_file').attr("required",true);
           $('#passport_info_msg').css({"display":"block"});
        }
       else{
            $('.passport_image_input').css({"display":"none"});
            $('#passport_image_file').attr("required",false);
            $('#passport_image_file').removeClass("input_required");
        }

    }


    function load_country_states() {
        var state = document.getElementById("selected_state");
        var countryid =  $('#selected_country option:selected').attr('countryid');
        $('#selected_state').attr("required",false);
       // $("#selected_state option").hide();
       var show_count=0;
       $("#hidden_selected_state option").removeClass("show_value");
        var selected_country_options = "<option id='state_val' value=''>Select State</option>";
        $("#hidden_selected_state option[id="+countryid+"]").addClass("show_value");
        $('#hidden_selected_state option[class=show_value]').each(function(){
                var state_value = $(this).val();
                show_count=show_count+1;
                var countryid = $(this).attr('countryid');
                var id = $(this).attr('id');
                var statename = $(this).attr('statename');
           selected_country_options=selected_country_options + ('<option countryid ='+countryid+' id=' + countryid + '  value="' + statename + '">' + statename + '</option>');
        });
            $("#selected_state").empty();
            $("#selected_state").append(selected_country_options);

        if(show_count==0)
        {
         $(".state_dropdown").hide();
         $(".state_text").removeClass("state_hide");
         $("#state_name_text").addClass("input_required");
         $('#selected_state').removeClass("input_required");
        }else{
         $(".state_dropdown").show();
         $(".state_text").addClass("state_hide");
         $("#state_name_text").removeClass("input_required");
         $('#selected_state').addClass("input_required");
        }
}
    // Passport Image validation
    $('#passport_image_file').bind('change', function() {
        var is_proceed =true;
         $('.passport_error_msg').css({"display":"none"});
         $('.passport_error_msg').css({"color":"black"});
         $('.passport_image_file_input_required_msg').css({"display":"none"});
        //alert('This file size is: ' + this.files[0].size/1024/1024 + "MiB");
        if (/\s/.test(this.files[0].name)) {
            $('.passport_error_msg').css({"color":"red"});
            is_proceed= false;
            // It has any kind of whitespace
        }
        var file_size = this.files[0].size/1024;
        if (file_size>500){
            $('.passport_error_msg').css({"display":"block"});
            $('.passport_size_msg').css({"color":"red"});
            is_proceed= false;
        }
        if(is_proceed)
            getBase64(this.files[0]);
        return is_proceed;

    });
    // Citizen Check option based boolean show
    $('input[type=radio][name="citizen_check"]').change(function() {
        var is_citizen_check = $(this).val();
        $("#selected_fcra_nationality").val('');
         $("#selected_country").val('');
         $("#addessline1").val('');
         $("#passport_number").val('');
         $("#city").val('');
         $("#pincode").val('');
         $("#state_name_text").val('');
         $("#selected_state").val('');
         $("#passport_image_file").val('');
        var amt = $("#overall_amount_inr").val();
//        var want_80G_yes = $('#want_80G_yes').is(':checked')
//        var want_80G_no = $('#want_80G_no').is(':checked')
//        console.log(want_80G_yes);
//        if(amt>=50000){
//            $('.pan_card_input').css({"display":"block"});
////            $('.pan_terms_checkbox').css({"display":"block"});
//        }
        validationonPANadhar();
        if (is_citizen_check=='yes'){
            $('.indian_address_err').css({"display":"block"});
            $('.non_indian_address_err').css({"display":"none"});
            $('.nationality_input').css({"display":"none"});
            $('.passport_number_input').css({"display":"none"});
             $('#passport_number').removeClass('input_required');
            $('#passport_image_file').css({"display":"none"});
            $('#passport_image_file').attr("required",false);
            $('.nation_error_msg').css({"display":"none"});
            $("#selected_country option[value=IN]").attr("disabled",false);
            $("#selected_country option[value=IN]").show();

        }
        else{
            $('.indian_address_err').css({"display":"none"});
            $('.non_indian_address_err').css({"display":"block"});
            $('.nationality_input').css({"display":"block"});
            $('.passport_number_input').css({"display":"block"});
//            $('#passport_number').addClass('input_required');
            $('.passport_image_input').css({"display":"none"});
            $('.nation_error_msg').css({"display":"none"});
            $("#selected_fcra_nationality option[value=IN]").hide();
            $("#selected_country option[value=IN]").attr("disabled",true);
            $("#selected_country option[value=IN]").hide();

        }

    });

    // want_80G option based boolean show
    $('input[type=radio][name="want_80G"]').change(function() {
        var nationality_yes = $('#nationality_yes').is(':checked')
        var nationality_no = $('#nationality_no').is(':checked')
        var is_want_80G = $(this).val();

        var amt = $("#overall_amount_inr").val();
         $('.pan_card_input').css({"display":"block"});
            $('.pan_terms_checkbox').css({"display":"block"});
            $('.want_80G_error_msg').css({"display":"none"});
            $('#agree_80g_tax_div').css({"display":"none"});
           console.log(amt);
            validationonPANadhar();

    });

    function is_not_empty_or_null(objtotestvar){
        var flg = false;
        try{
            if(objtotestvar && objtotestvar !=null && objtotestvar !="") {
                flg = true;
            }

        }
        catch(exvar){
        }
        finally{
            return flg;
        }
    }
    function load_profile() {
        console.log('hello');
        var initObjvar;
        var addobjvar;
        try {
            data = $('#profile_info').val();
            initObjvar = $.parseJSON(data);
            console.log(initObjvar)

            if ('firstName' in initObjvar['basicProfile'] && is_not_empty_or_null(initObjvar['basicProfile']['firstName']) && $('#first_name')) {
                $('#first_name').val(initObjvar['basicProfile']['firstName']);
                $('#first_name').prop("readonly",false);
            }
            if ('lastName' in initObjvar['basicProfile'] && is_not_empty_or_null(initObjvar['basicProfile']['lastName'])
            && $('#last_name')) {
                $('#last_name').val(initObjvar['basicProfile']['lastName']);
                $('#last_name').prop("readonly",false);
            }
            if ('email' in initObjvar['basicProfile'] && is_not_empty_or_null(initObjvar['basicProfile']['email'])
            && $('#email')) {
                $('#email').val(initObjvar['basicProfile']['email']);
                $('#email').prop("readonly",true);
            }
//
//
//
//            //debugger;
            if ('nationality' in initObjvar['extendedProfile'] && is_not_empty_or_null(initObjvar['extendedProfile']['nationality'])
            && $('select#selected_fcra_nationality')) {
                var countfiltvar = _.filter(jspgremcountrycoll, (a) => a.ccattr == initObjvar['extendedProfile']['nationality']);
                if(countfiltvar.length > 0) {
                    $('select#selected_fcra_nationality').val(countfiltvar[0]["valueattr"]);
                }
            }
            if ('phone' in initObjvar["basicProfile"]) {
                if ('countryCode' in initObjvar["basicProfile"]["phone"] && is_not_empty_or_null(initObjvar['basicProfile']['phone']['countryCode'])
                && $("select#country_code") && $("select#countrycode option") && $("select#countrycode option:selected")) {
                    $("select#country_code").val(initObjvar['basicProfile']['phone']['countryCode']);
                    $("select#country_code").trigger('change');
                    //                 $("select#countrycode option").prop("disabled", true);
                    //                 $("select#countrycode option:selected").prop("disabled", false);
                }
                console.log(initObjvar['basicProfile']['phone']['number']);
                if ('number' in initObjvar["basicProfile"]["phone"] && is_not_empty_or_null(initObjvar['basicProfile']['phone']['number'])
                && $("input#contact_no")) {

                    $("input#contact_no").val(initObjvar['basicProfile']['phone']['number']);
                    $("input#contact_no").prop("readonly",false);
                }
            }
            addobjvar = initObjvar['addresses'];
            if (addobjvar.length > 0) {
                addobjvar = addobjvar[0];
               //debugger;
                if ('country' in addobjvar && is_not_empty_or_null(addobjvar['country']) && $('#selected_country')) {
                    var countfiltvar = _.filter(jspgremcountrycoll, (a) => a.ccattr == addobjvar['country']);
                    if(countfiltvar.length > 0)
                    {
                        $('#selected_country').val(countfiltvar[0]["valueattr"]);
                      //  load_states();
                    }
                }
//                else if($("input[name='country']").val()) {
//                    $('#selected_country').val($("input[name='country']").val());
//                    load_states();
//                }
                if ('addressLine1' in addobjvar && is_not_empty_or_null(addobjvar['addressLine1']) && $('#addessline1')) {
                    $('#addessline1').val(addobjvar['addressLine1']);
                }
                //debugger
                if ('state' in addobjvar && is_not_empty_or_null(addobjvar['state']) && $('#selected_state')) {
                    let statevar = _.filter(jspgremstatecoll, (a) => a.statenameattr == addobjvar['state'])
                    if(statevar.length > 0){
                        $('#selected_state').val(statevar[0]["valueattr"]);
                    }
                }

                //debugger;
                if ('townVillageDistrict' in addobjvar && is_not_empty_or_null(addobjvar['townVillageDistrict'])) {
                    $('#city').val(addobjvar['townVillageDistrict']);
                }
                if ('pincode' in addobjvar && is_not_empty_or_null(addobjvar['pincode'])) {
                    $('#pincode').val(addobjvar['pincode']);
                }
//                if ('pincode' in addobjvar && is_not_empty_or_null(addobjvar['pincode'])) {
//                    $('#pincode').val(addobjvar['pincode']);
//                }
                // setting the states and also the fixstate variable to true so that the onchange of country
                // won't lead to removing the populated state on first load
                if ('stateId' in addobjvar && is_not_empty_or_null(addobjvar['stateId'])) {
                    fixState = true
                    $('#selected_state').val(addobjvar['stateId']);
                }else {
                    fixState = true
                    $('#state_text_field').val(addobjvar['state']);
                }


            }
        } catch (exvar) {
            console.log(exvar);
        }
//        finally {
//            load_states();
//        }

    }


//    function validate_dob(){
//        // DOB Validation
//        var date_Val = $('#dob').val();
//        var todaysDate = new Date();
//        var year = todaysDate.getFullYear()-72;
//        var month = ("0" + (todaysDate.getMonth() + 1)).slice(-2);
//        var day = ("0" + todaysDate.getDate()).slice(-2);
//        var max_date = (year +"-"+ month +"-"+ day);
//        console.log(max_date);
//        if (max_date > date_Val){
//            valid = false;
//        }
//        if (valid == false) {
//         $('.date_err').css({
//                    "display": "block"
//                });
////            return swal({
////                backdrop: false,
////                title: 'Date Of Birth ',
////                text: "Please enter a valid DOB",
////                type: 'error'
////            });
//        }
//        return valid;
//    }
});
