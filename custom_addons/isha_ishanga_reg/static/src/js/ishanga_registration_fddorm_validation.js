// load the state while load the page when pincode is entred
$(document).ready(function () {
     var fixState = false;
    $('.personal_details_form').submit(function(e) {
        valid = true;
        var f_name = $("#first_name").val();
        if(!f_name){
              $('.name_error_msg').css({
                   "display": "block"
              });
              valid = false;
        }
        var last_name = $("#last_name").val();
        if(!last_name){
              $('.lname_error_msg').css({
                   "display": "block"
              });
              valid = false;
        }
        var email = $("#email").val();
        if(!email){
              $('.email_error_msg').css({
                   "display": "block"
              });
              valid = false;
        }
        var country_code = $("#country_code").val();
        var contact_no = $("#contact_no").val();
        if(!contact_no){
              $('.number_error_msg').css({
                   "display": "block"
              });
              valid = false;
        }


        var selected_country = $("#selected_country").val();
        var city = $("#city").val();
        if(!city){
              $('.city_error_msg').css({
                   "display": "block"
              });
              valid = false;
        }
        var addessline1 = $("#addessline1").val();
        if(!addessline1){
              $('.address1_error_msg').css({
                   "display": "block"
              });
              valid = false;
        }
        var pincode = $("#pincode").val();
        if(!pincode){
              $('.zip_error_msg').css({
                   "display": "block"
              });
              valid = false;
        }
        valid_citizen_check();
        valid_want80g();
        valid_pancard();
        valid_aadhar_Card();
        var agree_tnc = $('#agree_tnc').is(':checked');
        if(agree_tnc){
            valid = true;
        }else{
            $('.agree_tnc_error_msg').css({
                "display": "block"
            });
            valid = false;
        }
        console.log(valid);
        if(valid){
            var dd = $('#passport_image_file').val();
            if(dd!="")
            {
                 if (/\s/.test($('#passport_image_file')[0].files[0].name)) {
                    $('#passport_error_msg').css({"color":"red"});
                    valid= false;
                    }
                var file_size = $('#passport_image_file')[0].files[0].size/1024;
                if (file_size>500){
                    $('#passport_error_msg').css({"display":"block"});
                    $('#passport_size_msg').css({"color":"red"});
                    valid= false;
                    }
            }
        }

        return valid;
    });

    $('#clickme').click(function(){
    alert("fdfdf");
    outread_data_validation();
    });
    function getBase64(file) {
   var reader = new FileReader();
   reader.readAsDataURL(file);
   reader.onload = function () {
     console.log(reader.result);
   };
   reader.onerror = function (error) {
     console.log('Error: ', error);
   };
}

    function outread_data_validation(){
        var data =[];
        var data_values=[];
        data_values= {
            "request_id": 1234,
            "is_validation_request":"True",
            "first_name": "fdf",
            "donation_amount": 34,
            "donation_currency": "INR",
            "donate_purpose_id": "Program",
            "converted_currency": "INR",
            "converted_amount": 43,
            "is_recurring": 0,
            "donate_for": "ishanga",
            "last_name": "rere",
            "email": "jk@gmail.com",
            "country_code": 91,
            "phone_number": 9043181831,
            "want_80G": "yes",
            "nationality":"yes",
            "country": "IN",
            "state_name": "dsf",
            "city": "dfdfd",
            "addessline1": "dfs fsd fs  fsfsf",
            "pincode": "45454444434",
            "pancard": "GHJMK6578H",
            "aadhar_card": "345435435432",
            "agree_tnc": "on",
            "no_80G_consent": "on",
            "sso_id": "3443434",
            "lang": "en",
            "initiative": "contribute",
            "initiative_short_code": "CON",
            "donation_title": "Ishanga 7%"
        }
        var want_80G_yes = $('#want_80G_yes').is(':checked');
        var want_80G_no = $('#want_80G_no').is(':checked');
        console.log(want_80G_yes);
        if(want_80G_yes == true){
            want_80G = "yes";
        }else{
             want_80G = "no";
        }

        var nationality_yes = $('#nationality_yes').is(':checked');
        var nationality_no = $('#nationality_no').is(':checked');
       if(nationality_yes == true){
           citizen_check = "yes";
       }else{
            citizen_check = "no";
       }

         data_values["lang"]="en"
         data_values["is_validation_request"]="True"
         data_values["initiative"]="contribute"
         data_values["initiative_short_code"]="CON"
         data_values["donation_title"]="Ishanga 7%"
         data_values["agree_tnc"]="on"
         data_values["donate_for"]="donate_for"
         data_values["is_validation_request"]="True"
         data_values["converted_currency"]="INR"

         data_values["first_name"]= $("#first_name").val();
         data_values["last_name"]= $("#last_name").val();
         data_values["email"]= $("#email").val();
          data_values["country_code"]=$("#country_code").val();
          data_values["phone_number"]=$("#contact_no").val();
          data_values["nationality"]=citizen_check;

         var donation_type =$('donation_type').val();
         data_values["is_recurring"]= 1;
         data_values["no_of_months"]=12;
         data_values["day_of_month"]=$('#day_of_month').val();
         data_values["until_cancelled_flag"]='';
         if (donation_type=="onetime"){
             data_values["is_recurring"]= 0;
             data_values["no_of_months"]='';
             data_values["day_of_month"]='';
         }


        data_values['sso_id'] = $('#sso_id').val();
        data_values['donation_amount'] = $('#hidden_donation_amount').val();
        data_values['donation_currency'] = $('#hidden_donation_currency').val();
        data_values['converted_amount'] = $('#coversion_amt_inr').val();

        data_values['want_80G'] = want_80G;
        data_values['country'] = $('#selected_country option:selected').attr("code")
        data_values['fcra_nationality'] = $('#selected_fcra_nationality').val();
        data_values['nationality_country'] =$('#selected_fcra_nationality option:selected').text();
        data_values['passport_number'] = $('#passport_number').val();
        data_values['country_code'] = $('#country_code').val();
        data_values['state_name'] = $('#state_name_text').val();
        data_values['city'] = $('#city').val();
        data_values['addessline1'] = $('#addessline1').val();
        data_values['pincode'] = $('#pincode').val();
        data_values['pancard'] = $('#pancard').val();
        data_values['aadhar_card'] = $('#aadhar_card').val();
        data_values['agree_tnc'] = $('#agree_tnc').val();
        data_values['no_80G_consent'] = $('#no_80G_consent').val();
        var dd = $('#passport_image_file').val();
            if(dd!="")
            { data_values['passport'] = $('#passport_image_file').val();
            var file = $('#passport_image_file')[0].files[0];
            var ss = getBase64(file);
            data_values['passport_file_extension'] =file.type;
            }

        console.log(data_values);

        var saveData = $.ajax({
              type: 'POST',
               crossDomain: true,
               dataType: 'json',
              url: "https://uat.ishaoutreach.org/api/ishanga/new-registration",
              data: JSON.stringify(data_values),
              success: function(resultData) { alert(resultData);
                   var response_from_outreach =JSON.stringify(resultData);
                   response_from_outreach =JSON.parse(response_from_outreach);
                   alert(response_from_outreach['status_code']);
                   if (response_from_outreach['status_code']==200)
                   {
                        alert("Completed");
                   }
                   else{
                        var err_msg =response_from_outreach['message'];
                        $('#erro_msg').html('');
                        $('#erro_msg').html(err_msg);
                   }
               }
        });
        return false;
    }

    // Pan Card Validation
    function valid_pancard(){
        var show_aadhar_bool = $('#show_aadhar').is(':checked');
        if(show_aadhar_bool == false){
            var pancard = $("#pancard").val();
            var regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
            if(!pancard){
                $('.pancard_error_msg').css({
                    "display": "block"
                });
                valid = false;
            }else{
                 $('.pan_err').css({
                    "display": "none"
                });
                 $('.pancard_error_msg').css({
                    "display":"none"
                });
                valid = false;
            }
        }
    }

    // Load Profile Logic
    var fixState = false;
    load_profile();
    var jspgremstatecoll =[];
    var jspgremcountrycoll =[];
    $("#selected_fcra_nationality").trigger('change');
    $("#countrycode").trigger('click');
    $("#current_country").trigger("click");
    $('#selected_state option').each(function(ivar,jvar){
        var objvar ={
            "valueattr":$(jvar).attr("value"),
            "statenameattr":$(jvar).attr("statename")
        };
        jspgremstatecoll.push(objvar);
    });
    $('#selected_fcra_nationality option').each(function(ivar,jvar){
        var objvar ={
            "valueattr":$(jvar).attr("value"),
            "ccattr":$(jvar).attr("ccatr")
        };
        jspgremcountrycoll.push(objvar);
    });

    // AAdhar Card Validation
    function valid_aadhar_Card(){
        var show_aadhar_bool = $('#show_aadhar').is(':checked');
        $('#aadhar_card').attr("required",false);
        if(show_aadhar_bool == true){
            $('#aadhar_card').attr("required",true);
            var aadhar_card_vals = $("#aadhar_card").val();
            var regex = /[0-9]{12}/;
            if(!aadhar_card_vals){
                $('.adharcard_error_msg').css({
                    "display": "none"
                });
                valid = false;
            }else if(!regex.test(aadhar_card_vals)){

                $('#aadhar_card').val($('#aadhar_card').val().toUpperCase());
                $('.aadhar_err').css({"display":"block"});
                valid = true;
            }else{
                $('.aadhar_err').css({"display":"none"});
                valid = false;
            }
        }
    }

    // valid_want80g validation
    function valid_want80g(){
        var want_80G_yes = $('#want_80G_yes').is(':checked')
        var want_80G_no = $('#want_80G_no').is(':checked')
        if(want_80G_yes == false && want_80G_no == false)
        {
            $('.want_80G_error_msg').css({
                "display": "block"
            });
            valid = false;
        }
        else{
            $('.want_80G_error_msg').css({
                "display": "none"
            });
            valid = true;
        }
    }

    // valid_citizen_check validation
    function valid_citizen_check(){
        var nationality_yes = $('#nationality_yes').is(':checked')
        var nationality_no = $('#nationality_no').is(':checked')
        if(nationality_yes == false && nationality_no == false)
        {
            $('.nation_error_msg').css({
                "display": "block"
            });
            valid = false;
        }
        else{
            $('.nation_error_msg').css({
                "display": "none"
            });
            valid = true;
        }
    }

    $('.passport_image_input').css({"display":"none"});
    $('.passport_image_file').css({"display":"none"});

    $( "#agree_tnc" ).change(function(){
        agree_tnc_bool = $('#agree_tnc').is(':checked');
        if(agree_tnc_bool == false){
             $('.agree_tnc_error_msg').css({
                    "display": "block"
             });
            return false;
        }else{
            $('.agree_tnc_error_msg').css({
                    "display": "none"
             });
            return true;
        }
    });
    $( "#no_80G_consent").change(function(){
        no_80G_consent_bool = $('#no_80G_consent').is(':checked');
        if(no_80G_consent_bool == false){
             $('.agree_no80g_consent_error_msg').css({
                    "display": "block"
             });
            return false;
        }else{
            $('.agree_no80g_consent_error_msg').css({
                    "display": "none"
             });
            return true;
        }

    });

    // fname Validation
    $( "#first_name" ).blur(function(){
        var f_name = $("#first_name").val();
        var regex = /^[A-Z]+$/i;
        if(!f_name){
              $('.name_error_msg').css({
                   "display": "block"
              });
              return false;
        }else if(!regex.test(f_name)){
             $('.name_error_msg').css({
                "display": "none"
            });
            $('.fname_error_msg').css({
                "display":"block"
            });
            return false;
        }else{
             $('.name_error_msg').css({
                "display": "none"
            });
            $('.fname_error_msg').css({
                "display":"none"
            });
            return false;
        }
    });

    // lastname Validation
    $( "#last_name" ).blur(function(){
        var last_name = $("#last_name").val();
        var regex = /^[A-Z]+$/i;
        if(!last_name){
              $('.lname_error_msg').css({
                   "display": "block"
              });
              return false;
        }else if(!regex.test(last_name)){
             $('.lname_error_msg').css({
                "display": "none"
            });
            $('.lname_error_msg1').css({
                "display":"block"
            });
            return false;
        }else{
             $('.lname_error_msg').css({
                "display": "none"
            });
            $('.lname_error_msg1').css({
                "display":"none"
            });
            return false;
        }
    });

    // Email Validation
    $( "#email" ).blur(function(){
        var email = $("#email").val();
        var regexEmail = /([a-zA-Z0-9][-a-zA-Z0-9_\+\.]*[a-zA-Z0-9])@([a-zA-Z0-9][-a-zA-Z0-9\.]*[a-zA-Z0-9]\.(arpa|root|aero|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw|life)|([0-9]{1,3}\.{3}[0-9]{1,3}))$/i;
        if(!email){
              $('.email_error_msg').css({
                   "display": "block"
              });
              return false;
        }else if(!regexEmail.test(email)){
             $('.email_error_msg').css({
                "display": "none"
            });
            $('.email_error_msg1').css({
                "display":"block"
            });
            return false;
        }else{
             $('.email_error_msg').css({
                "display": "none"
            });
            $('.email_error_msg1').css({
                "display":"none"
            });
            return false;
        }
    });
    $( "#country_code" ).blur(function(){
        var country_code = $("#country_code").val();
        if(!country_code){
              $('.country_error_msg').css({
                   "display": "block"
              });
              return false;
         }else{
             $('.country_error_msg').css({
                "display": "none"
            });
            return false;
        }
    });
    // Mobile Number Validation
    $( "#contact_no" ).blur(function(){
        var contact_no = $("#contact_no").val();
        var regexContactNo = /^\d{0,15}$/;
        if(!contact_no){
              $('.number_error_msg').css({
                   "display": "block"
              });
              return false;
        }else if(!regexContactNo.test(contact_no)){
             $('.number_error_msg').css({
                "display": "none"
            });
            $('.number_error_msg1').css({
                "display":"block"
            });
            return false;
        }else{
             $('.number_error_msg').css({
                "display": "none"
            });
             $('.number_error_msg1').css({
                "display":"none"
            });
            return false;
        }
    });

    // Pan Card Number Validation
    $( "#pancard" ).blur(function(){
        var pancard = $("#pancard").val();
        var regex = /[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
        if(!pancard){
            $('.pancard_error_msg').css({
                "display": "block"
            });
            return false;
        }else if(!regex.test(pancard)){
            $('.pancard_error_msg').css({
                "display": "none"
            });
            $('#pancard').val($('#pancard').val().toUpperCase());
            $('.pan_err').css({
                "display":"block"
            });
            return false;
        }else{
             $('.pan_err').css({
                "display": "none"
            });
             $('.pancard_error_msg').css({
                "display":"none"
            });
            return false;
        }
    });
    // Pass Number Validation
    $( "#passport_number" ).blur(function(){
        var passport_number = $("#passport_number").val();
        var regex =  /^[A-PR-WY][1-9]\d\s?\d{4}[1-9]$/ig;
        if(!passport_number){
            $('.passport_number_error_msg').css({
                "display": "block"
            });
            return false;
        }else if(!regex.test(passport_number)){
             $('.passport_number_error_msg').css({
                "display": "none"
            });
            $('.valid_passport_error_msg').css({
                "display":"block"
            });
            return false;
        }else{
             $('.passport_number_error_msg').css({
                "display": "none"
            });
             $('.valid_passport_error_msg').css({
                "display":"none"
            });
            return false;
        }
    });

    // Aadhar validation
    $( "#aadhar_card" ).blur(function(){
        var aadhar_input = $("#aadhar_card").val();
        var regex = /[0-9]{12}/;
        if(!aadhar_input){
            $('.adharcard_error_msg').css({
                "display": "block"
            });
            return false;
        }else if(!regex.test(aadhar_input)){
            $('.adharcard_error_msg').css({
                "display": "none"
            });
            $('#aadhar_card').val($('#aadhar_card').val().toUpperCase());
            $('.aadhar_err').css({"display":"block"});
            return false;
        }else{
            $('.adharcard_error_msg').css({
                "display": "none"
            });
            $('.aadhar_err').css({"display":"none"});
            return false;
        }
    });

    $("#show_aadhar").on("click",function() {
        var show_aadhar = $('#show_aadhar').is(':checked');
        if(show_aadhar==true){
            $('.pancard').val("");
            $('.aadhar_card').val("");
            $('.pancard').attr("required",false);
            $('.aadhar_card').attr("required",true);
            $('#aadhar_input').css({"display":"block"});
        }
        else{
            $('.pancard').val("");
            $('.aadhar_card').val("");
            $('.pancard').attr("required",true);
            $('.aadhar_card').attr("required",false);
             $('#aadhar_input').css({"display":"none"});
        }


    });
    // City validation
    $( "#city" ).blur(function(){
        var city = $("#city").val();
        var regexCity = /^[a-zA-Z ]+$/u;
        if(!city){
              $('.city_error_msg').css({
                   "display": "block"
              });
              return false;
        }else if(!regexCity.test(city)){
            $('.city_error_msg').css({
                "display": "none"
            });
            $('.city_error_msg1').css({"display":"block"});
            return false;
        }else{
            $('.city_error_msg').css({
                "display": "none"
            });
            $('.city_error_msg1').css({
                "display":"none"
            });
            return false;
        }
    });
    // Address Line validation
    $( "#addessline1" ).blur(function(){
        var addessline1 = $("#addessline1").val();
        var regexAddress = /^[ A-Za-z0-9\-\/&_ ,.#()]{10,}$/;
        if(!addessline1){
              $('.address1_error_msg').css({
                   "display": "block"
              });
              return false;
        }else if(!regexAddress.test(addessline1)){
            $('.address1_error_msg').css({
                "display": "none"
            });
            $('.address2_error_msg').css({"display":"block"});
            return false;
        }else{
             $('.address1_error_msg').css({
                "display": "none"
            });
            $('.address2_error_msg').css({
                "display": "none"
            });
            return false;
        }
    });
    $( "#pincode" ).blur(function(){
        var pincode = $("#pincode").val();
        var regexZipCodeIndia = /^(\+\d{1,3}[- ]?)?\d{6}$/;
        if(!pincode){
              $('.zip_error_msg').css({
                   "display": "block"
              });
              return false;
        }else if(!regexZipCodeIndia.test(pincode)){
            $('.zip_error_msg').css({
                "display": "none"
            });
            $('.zip_error_msg1').css({
                "display":"block"
            });
            return false;
        }else{
            $('.zip_error_msg').css({
                "display": "none"
            });
            $('.zip_error_msg1').css({
                "display":"none"
            });
            return false;
        }
    });

    $( "#nationality" ).blur(function(){
        if($('#nationality').not(':checked'))
        {
             $('.nation_error_msg').css({
                   "display": "block"
              });
              return false;
        }else{
             $('.nation_error_msg').css({
                "display": "none"
            });
            return false;
        }
    });

     $('#selected_country').change(function() {
        fixState = true;
         $("#passport_image_file").val('');
        load_country_states();
        validation_country();
    });
    $(".nationality").change(function() {
	checkNationality();
	});
    function checkNationality(){
     console.log("fdff");
      var isChecked = $("input[name='nationality']:checked").val();
      console.log(isChecked);
        if(isChecked=="Yes")
         {
            $('#show_address_msg').html('Entering an Indian address is recommended');
         }
         else{
           $('#show_address_msg').html('Entering a non-indian address is mandatory');

         }

    }
    function validation_country(){

        var country = $('#selected_country option:selected').val();
        var region_id =$('#selected_country option:selected').attr('region_id');
        var na_region_id =$('#na_region_id').val();
        if (na_region_id==region_id){
            $('.country_validation_message').css({
                "display": "block"
            });
            $(".submit_button_hide").attr("disabled", true);
            return false;
        }else{
            $(".submit_button_hide").attr("disabled", false);
        }

        var nationality = $("#selected_fcra_nationality").val();
        var selected_country = $("#selected_country").val();
        var selected_country_code = $("#selected_country option:selected").attr('code');
        $('#passport_info_msg').css({"display":"none"});
        if(selected_country_code != 'IN' && !nationality){
           $('.passport_image_input').css({"display":"block"});
           $('.passport_image_file').css({"display":"block"});
           $('.passport_image_file').attr("required",true);
           $('#passport_info_msg').css({"display":"block"});
        }
       else{
            $('.passport_image_input').css({"display":"none"});
            $('.passport_image_file').attr("required",false);
        }

    }


    function load_country_states() {
        var state = document.getElementById("selected_state");
        var countryid =  $('#selected_country option:selected').val();
        console.log(countryid)
        $('#selected_state').attr("required",false);
        $("#selected_state option").hide();
        $("#selected_state option").removeClass("show_value");
        $("#selected_state option[id=default_country]").hide();
        $("#selected_state option[id="+countryid+"]").show();
        $("#selected_state option[id="+countryid+"]").addClass("show_value");
        var x = document.getElementById("selected_state").options.length;
        console.log(x)
        var show_count=0;
        $('#selected_state option[class=show_value]').each(function(){
        show_count=show_count+1;
        });
        console.log(show_count)

        if(show_count==0)
        {
         $(".state_dropdown").hide();
         $(".state_text").removeClass("state_hide");


        }else{
        $(".state_dropdown").show();
         $(".state_text").addClass("state_hide");
        }
}
    // Passport Image validation
    $('.input_file').bind('change', function() {
        var is_proceed =true;
         $('#passport_error_msg').css({"display":"none"});
         $('#passport_error_msg').css({"color":"black"});
        //alert('This file size is: ' + this.files[0].size/1024/1024 + "MiB");
        if (/\s/.test(this.files[0].name)) {
            $('#passport_error_msg').css({"color":"red"});
            is_proceed= false;
            // It has any kind of whitespace
        }
        var file_size = this.files[0].size/1024;
        if (file_size>500){
            $('#passport_error_msg').css({"display":"block"});
            $('#passport_size_msg').css({"color":"red"});
            is_proceed= false;
        }
        return is_proceed;

    });
    // Citizen Check option based boolean show
    $('input[type=radio][name="citizen_check"]').change(function() {
        var is_citizen_check = $(this).val();
        $("#selected_fcra_nationality").val('');
         $("#selected_country").val('');
         $("#addessline1").val('');
         $("#passport_number").val('');
         $("#city").val('');
         $("#pincode").val('');
         $("#state_name_text").val('');
         $("#selected_state").val('');
         $("#passport_image_file").val('');
        var amt = $("#overall_amount_inr").val();
        var want_80G_yes = $('#want_80G_yes').is(':checked')
        var want_80G_no = $('#want_80G_no').is(':checked')
//        console.log(want_80G_yes);
        if(amt>50000){
            $('.pan_card_input').css({"display":"block"});
//            $('.pan_terms_checkbox').css({"display":"block"});
        }
        if (is_citizen_check=='yes'){
            $('.indian_address_err').css({"display":"block"});
            $('.non_indian_address_err').css({"display":"none"});
            $('.nationality_input').css({"display":"none"});
            $('.passport_number_input').css({"display":"none"});
            $('.passport_image_file').css({"display":"none"});
            $('.passport_image_file').attr("required",false);
            $('.nation_error_msg').css({"display":"none"});
            $("#selected_country option[value=IN]").show();

            if(amt>50000){
                $('.pan_card_input').css({"display":"block"});
            }
            if(amt<50000 && want_80G_yes == true){
                $('.pan_card_input').css({"display":"block"});
                $('.pan_terms_checkbox').css({"display":"block"});
            }
            if(amt<50000 && want_80G_yes == false){
                $('.pan_card_input').css({"display":"block"});
                $('.pan_terms_checkbox').css({"display":"none"});
                $('#agree_80g_tax_div').css({"display":"none"});
            }
            if(amt>50000 && want_80G_yes == true){
                $('.pan_card_input').css({"display":"block"});
                $('.pan_terms_checkbox').css({"display":"none"});
                $('#agree_80g_tax_div').css({"display":"none"});
            }
            if(amt>50000 && want_80G_yes == false){
                $('.pan_card_input').css({"display":"block"});
                $('.pan_terms_checkbox').css({"display":"none"});
                $('#agree_80g_tax_div').css({"display":"block"});
            }
            if(amt>50000 && want_80G_no == true){
                $('.pan_card_input').css({"display":"block"});
//                $('.pan_terms_checkbox').css({"display":"none"});
                $('#agree_80g_tax_div').css({"display":"block"});
            }
            if(amt>50000 && want_80G_yes == false && want_80G_no == false){
                $('#agree_80g_tax_div').css({"display":"none"});
            }
            if(amt<50000 && want_80G_yes == false && want_80G_no == false){
                $('.pan_card_input').css({"display":"none"});
            }
            if(amt<50000 && want_80G_no == true){
                $('#agree_80g_tax_div').css({"display":"block"});
                $('.pan_card_input').css({"display":"none"});
            }
        }
        else{
            $('.indian_address_err').css({"display":"none"});
            $('.non_indian_address_err').css({"display":"block"});
            $('.nationality_input').css({"display":"block"});
            $('.passport_number_input').css({"display":"block"});
            $('.passport_image_input').css({"display":"none"});
            $('.nation_error_msg').css({"display":"none"});
            $("#selected_fcra_nationality option[value=IN]").hide();
            $("#selected_country option[value=IN]").hide();
            if(amt>50000){
                $('.pan_card_input').css({"display":"none"});
                $('#agree_80g_tax_div').css({"display":"none"});
                $('.passport_info_msg').css({"display":"none"});
            }
            if(amt<50000){
                $('.pan_card_input').css({"display":"none"});
                $('#agree_80g_tax_div').css({"display":"none"});
                $('.passport_info_msg').css({"display":"none"});
            }
            if(amt<50000 && want_80G_yes == true){
                $('.pan_card_input').css({"display":"block"});
            }
            if(amt>50000 && want_80G_yes == true){
                $('.pan_card_input').css({"display":"block"});
                $('.pan_terms_checkbox').css({"display":"block"});
            }

            if(amt>50000 && want_80G_yes == false && want_80G_no == false){
                $('#agree_80g_tax_div').css({"display":"none"});
            }
        }
    });

    // want_80G option based boolean show
    $('input[type=radio][name="want_80G"]').change(function() {
        var nationality_yes = $('#nationality_yes').is(':checked')
        var nationality_no = $('#nationality_no').is(':checked')
        var is_want_80G = $(this).val();
        var amt = $("#overall_amount_inr").val();
        console.log(amt);
        if (is_want_80G=='yes'){
            $('.pan_card_input').css({"display":"block"});
            $('.pan_terms_checkbox').css({"display":"block"});
            $('.want_80G_error_msg').css({"display":"none"});
            $('#agree_80g_tax_div').css({"display":"none"});
            if(amt>50000 && nationality_yes==true){
                $('.pan_terms_checkbox').css({"display":"block"});
            }
            if(amt<50000 && is_want_80G == 'yes'){
                $('.pan_card_input').css({"display":"block"});
                $('.pan_terms_checkbox').css({"display":"none"});
            }
            if(amt<50000 && is_want_80G == 'yes' && nationality_no==true){
                $('.pan_card_input').css({"display":"block"});
                $('.pan_terms_checkbox').css({"display":"block"});
            }
            if(amt>50000 && is_want_80G=='yes' && nationality_no==true){
                $('.pan_card_input').css({"display":"block"});
                $('.pan_terms_checkbox').css({"display":"block"});
                $('#agree_80g_tax_div').css({"display":"block"});

            }
            if(amt>50000 && is_want_80G=='yes' && nationality_no==false){
                $('.pan_card_input').css({"display":"block"});
                $('.pan_terms_checkbox').css({"display":"none"});

            }
            if(amt>50000 && is_want_80G=='no' && nationality_no==false){
                $('.pan_card_input').css({"display":"block"});
                $('.pan_terms_checkbox').css({"display":"block"});
            }
            if(amt<50000 && is_want_80G == 'yes' && nationality_no==false){
                $('.pan_card_input').css({"display":"block"});
                $('.pan_terms_checkbox').css({"display":"block"});
            }
        }
        else{
           $('.pan_card_input').css({"display":"none"});
           $('.pan_terms_checkbox').css({"display":"none"});
           $('.aadhar_input').css({"display":"none"});
           $('.want_80G_error_msg').css({"display":"none"});
           $('#agree_80g_tax_div').css({"display":"block"});
           if(amt>50000 && nationality_yes==true){
                $('.pan_card_input').css({"display":"block"});
           }
           if(amt>50000 && nationality_no==true){
                $('.pan_terms_checkbox').css({"display":"none"});
                $('#agree_80g_tax_div').css({"display":"none"});
           }
           if(amt<50000 && nationality_yes==true){
                $('#agree_80g_tax_div').css({"display":"block"});
            }
           if(amt<50000 && nationality_no==true){
                $('#agree_80g_tax_div').css({"display":"none"});
           }
           if(amt>50000 && nationality_no == false){
                $('#agree_80g_tax_div').css({"display":"block"});
           }
           if(amt<50000 && nationality_yes==false){
                 $('#agree_80g_tax_div').css({"display":"none"});
           }
           if(amt>50000 && nationality_no == false && nationality_yes==false ){
                $('#agree_80g_tax_div').css({"display":"none"});
           }
//           if(amt>50000 && is_want_80G == 'yes'){
//                $('.pan_card_input').css({"display":"block"});
//                $('.pan_terms_checkbox').css({"display":"block"});
//            }

        }
    });

    function load_states() {
        var state_dropdown = document.getElementById("stateid");
        var country_dropdown = document.getElementById("current_country");
        var country_id = country_dropdown.value;
        //    if(country_id==104){
        //        $('#stateid').attr("required",true);
        //         $('.state_id').addClass("required");
        //
        //    }
        //    else{
        //    $('#stateid').attr("required",false);
        //      $('.state_id').removeClass("required");
        //    }
        $('#stateid').attr("required",false);
        $('.state_id').removeClass("required");
        var idvar = 0;
        var cc=0;
        Array.from(state_dropdown.options).forEach(item => {
            var id = item.getAttribute("countryid");
            if (id == country_id || id==0) {
                item.style.display = "block";
                item.disabled = false;
                cc =cc+1;
                //            if (idvar == 0)
                //                idvar = item.index;
            } else {
                item.style.display = "none";
                item.disabled = true;
            }
        });

        if (fixState==false){
            $('#stateid').val("");
            $('#state_text_field').val('');
        }
        if(cc>1){
            $('#stateid').attr("required",true);
            $('#state_text_field').attr("required",false);
            $('.state_id').addClass("required");
            $('.state_text').removeClass("required");
            $('.state_dropdown').css({"display":"block"});
            $('.state_txt').css({"display":"none"});
        }else{
           $('#stateid').attr("required",false);
            $('#state_text_field').attr("required",true);
            $('.state_id').removeClass("required");
            $('.state_text').addClass("required");
            $('.state_txt').css({"display":"block"});
            $('.state_dropdown').css({"display":"none"});
        }
    }
    function is_not_empty_or_null(objtotestvar){
        var flg = false;
        try{
            if(objtotestvar && objtotestvar !=null && objtotestvar !="") {
                flg = true;
            }

        }
        catch(exvar){
        }
        finally{
            return flg;
        }
    }
    function load_profile() {
        console.log('hello');
        var initObjvar;
        var addobjvar;
        try {
            data = $('#profile_info').val();
            initObjvar = $.parseJSON(data);
            console.log(initObjvar)

            if ('firstName' in initObjvar['basicProfile'] && is_not_empty_or_null(initObjvar['basicProfile']['firstName']) && $('#first_name')) {
                $('#first_name').val(initObjvar['basicProfile']['firstName']);
                $('#first_name').prop("readonly",false);
            }
            if ('lastName' in initObjvar['basicProfile'] && is_not_empty_or_null(initObjvar['basicProfile']['lastName'])
            && $('#last_name')) {
                $('#last_name').val(initObjvar['basicProfile']['lastName']);
                $('#last_name').prop("readonly",false);
            }
            if ('email' in initObjvar['basicProfile'] && is_not_empty_or_null(initObjvar['basicProfile']['email'])
            && $('#email')) {
                $('#email').val(initObjvar['basicProfile']['email']);
                $('#email').prop("readonly",true);
            }
//
//
//
//            //debugger;
            if ('nationality' in initObjvar['extendedProfile'] && is_not_empty_or_null(initObjvar['extendedProfile']['nationality'])
            && $('select#selected_fcra_nationality')) {
                var countfiltvar = _.filter(jspgremcountrycoll, (a) => a.ccattr == initObjvar['extendedProfile']['nationality']);
                if(countfiltvar.length > 0) {
                    $('select#selected_fcra_nationality').val(countfiltvar[0]["valueattr"]);
                }
            }
            if ('phone' in initObjvar["basicProfile"]) {
                if ('countryCode' in initObjvar["basicProfile"]["phone"] && is_not_empty_or_null(initObjvar['basicProfile']['phone']['countryCode'])
                && $("select#country_code") && $("select#countrycode option") && $("select#countrycode option:selected")) {
                    $("select#country_code").val(initObjvar['basicProfile']['phone']['countryCode']);
                    $("select#country_code").trigger('change');
                    //                 $("select#countrycode option").prop("disabled", true);
                    //                 $("select#countrycode option:selected").prop("disabled", false);
                }
                console.log(initObjvar['basicProfile']['phone']['number']);
                if ('number' in initObjvar["basicProfile"]["phone"] && is_not_empty_or_null(initObjvar['basicProfile']['phone']['number'])
                && $("input#contact_no")) {

                    $("input#contact_no").val(initObjvar['basicProfile']['phone']['number']);
                    $("input#contact_no").prop("readonly",false);
                }
            }
            addobjvar = initObjvar['addresses'];
            if (addobjvar.length > 0) {
                addobjvar = addobjvar[0];
               //debugger;
                if ('country' in addobjvar && is_not_empty_or_null(addobjvar['country']) && $('#selected_country')) {
                    var countfiltvar = _.filter(jspgremcountrycoll, (a) => a.ccattr == addobjvar['country']);
                    if(countfiltvar.length > 0)
                    {
                        $('#selected_country').val(countfiltvar[0]["valueattr"]);
                        load_states();
                    }
                }
//                else if($("input[name='country']").val()) {
//                    $('#selected_country').val($("input[name='country']").val());
//                    load_states();
//                }
                if ('addressLine1' in addobjvar && is_not_empty_or_null(addobjvar['addressLine1']) && $('#addessline1')) {
                    $('#addessline1').val(addobjvar['addressLine1']);
                }
                //debugger
                if ('state' in addobjvar && is_not_empty_or_null(addobjvar['state']) && $('#selected_state')) {
                    let statevar = _.filter(jspgremstatecoll, (a) => a.statenameattr == addobjvar['state'])
                    if(statevar.length > 0){
                        $('#selected_state').val(statevar[0]["valueattr"]);
                    }
                }

                //debugger;
                if ('townVillageDistrict' in addobjvar && is_not_empty_or_null(addobjvar['townVillageDistrict'])) {
                    $('#city').val(addobjvar['townVillageDistrict']);
                }
                if ('pincode' in addobjvar && is_not_empty_or_null(addobjvar['pincode'])) {
                    $('#pincode').val(addobjvar['pincode']);
                }
//                if ('pincode' in addobjvar && is_not_empty_or_null(addobjvar['pincode'])) {
//                    $('#pincode').val(addobjvar['pincode']);
//                }
                // setting the states and also the fixstate variable to true so that the onchange of country
                // won't lead to removing the populated state on first load
                if ('stateId' in addobjvar && is_not_empty_or_null(addobjvar['stateId'])) {
                    fixState = true
                    $('#selected_state').val(addobjvar['stateId']);
                }else {
                    fixState = true
                    $('#state_text_field').val(addobjvar['state']);
                }


            }
        } catch (exvar) {
            console.log(exvar);
        }
//        finally {
//            load_states();
//        }

    }


//    function validate_dob(){
//        // DOB Validation
//        var date_Val = $('#dob').val();
//        var todaysDate = new Date();
//        var year = todaysDate.getFullYear()-72;
//        var month = ("0" + (todaysDate.getMonth() + 1)).slice(-2);
//        var day = ("0" + todaysDate.getDate()).slice(-2);
//        var max_date = (year +"-"+ month +"-"+ day);
//        console.log(max_date);
//        if (max_date > date_Val){
//            valid = false;
//        }
//        if (valid == false) {
//         $('.date_err').css({
//                    "display": "block"
//                });
////            return swal({
////                backdrop: false,
////                title: 'Date Of Birth ',
////                text: "Please enter a valid DOB",
////                type: 'error'
////            });
//        }
//        return valid;
//    }
});

