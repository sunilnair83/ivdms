import base64
import json
from datetime import date, timedelta, datetime
from odoo import http
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.fields import Integer
from odoo.http import request
from odoo.tools import ustr
import logging
import traceback
import hashlib
import hmac
import requests
import werkzeug
import werkzeug.utils
import werkzeug.wrappers
from ..isha_crm_importer import Configuration
from ...isha_crm import NameMatchEngine
from ...isha_crm.GoldenContactAdv import EXACT_MATCH,HIGH_MATCH,LOW_MATCH,NO_MATCH
import re

_logger = logging.getLogger(__name__)
import json
from datetime import date, timedelta, datetime


class IshangaNURegistration(http.Controller):

    def sso_logout(self):
        sso_config = Configuration('SSO')
        ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')

        cururls = str(request.httprequest.url).replace("http://", "http://", 1)
        base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
        kshetragna_sso_login_url = base_url + '/ishanga_registration?origin=Donate&tmp=""'
        ret_list = {'request_url': str(cururls),
                    "callback_url": kshetragna_sso_login_url,
                    "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
        }
        data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
        data_enc = data_enc.replace("/", "\\/")
        signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                             digestmod=hashlib.sha256).hexdigest()
        ret_list["hash_value"] = signature
        ret_list['sso_logouturl'] = request.env['ir.config_parameter'].sudo().get_param(
            'satsang.SSO_LOGIN_URL1').rstrip('/') + '/cm'
        return ret_list

    # Api to get full profile data
    def sso_get_full_data(self, sso_id):
        # request_url = "https://uat-profile.isha.in/services/pms/api/full-profiles/" + sso_id
        request_url = request.env['ir.config_parameter'].sudo().get_param(
            'satsang.SSO_PMS_ENDPOINT1') + "full-profiles/" + sso_id
        authreq = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SYSTEM_TOKEN1')
        headers = {
            "Authorization": "Bearer " + authreq,
            'X-legal-entity': 'IF',
            'x-user': sso_id
        }
        profile = requests.get(request_url, headers=headers)
        return profile.json()

    def hash_hmac(self, data, secret):
        data_enc = json.dumps(data['session_id'], sort_keys=True, separators=(',', ':'))
        signature = hmac.new(bytes(secret, 'utf-8'), bytes(data_enc, 'utf-8'), digestmod=hashlib.sha256).hexdigest()
        return signature

    def replaceEmptyString(self,src_msg):
        for x in src_msg:
            if src_msg[x] == '':
                src_msg[x] = None
            elif type(src_msg[x]) == str:
                src_msg[x] = src_msg[x].replace('\x00', '')  # Handle Byte String
                src_msg[x] = src_msg[x].strip()
        return src_msg

    # Api to get the profile data
    def sso_get_user_profile(self, url, payload):
        dp = {"data": json.dumps(payload)}
        url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
        # request.env['satsang.tempdebuglog'].sudo().create({
        #     'logtext': "sso_get_user_profile config " + url
        # })
        return requests.post(url, data=dp)

    def check_program_exist(self, sso_id,cor):
        """Check the condition the Program is started or not in the User Country of residence
        @return:is_valid - True /False"""
        profile = sso_id
        is_valid = False
        prog_date=False
        schedule_id=False
        progrm_id=False
        if profile:
            user_country_code = cor
            country_rec = request.env['res.country'].sudo().search([('code', '=', user_country_code)], order="name asc")
            today = date.today()
            prog_schedule = request.env['isha.program.schedule.timezonelist'].sudo().search(
                [("active", "=", True), ('start_date', '>=', today.strftime('%Y-%m-%d')),
                 ("end_date", ">=", today.strftime('%Y-%m-%d'))])
            if prog_schedule:
                if country_rec.id in prog_schedule.country_ids.ids:
                    is_valid = True
                    progrm_id = prog_schedule.program_id.id
                    schedule_id=prog_schedule.id
                    prog_date=prog_schedule.start_date

        return {"valid": is_valid, "prog_date": prog_date,"schedule_id":schedule_id,"progrm_id":progrm_id}

    def check_user_isishanga(self, sso_id):
        """ Check the login user is Ishanga or not
        @return:is_vaild -True or False
        """
        is_exist = False
        profile_info = self.sso_get_full_data(str(sso_id))
        login_email = profile_info["basicProfile"]["email"]
        f_name = profile_info["basicProfile"]["firstName"] if profile_info["basicProfile"]["firstName"] else ""
        last_name = profile_info["basicProfile"]["lastName"] if profile_info["basicProfile"]["lastName"]  else ""
        partner_categ = request.env['res.partner.category'].sudo().search([("name", "=", "7% Ishanga")])
        matchEngine = NameMatchEngine.NameMatchEngine()
        name = str(f_name)+' '+str(last_name)
        partner_id = request.env['res.partner'].sudo().search(['|','|','|',("sso_id", "=", sso_id),
                                                               ('email','=',login_email),
                                                               ('email2','=',login_email),
                                                               ('email3','=',login_email)])

        partner_id |= request.env['contact.map'].sudo().search([('email', '=', login_email)]).mapped('contact_id_fkey')
        msg = "Success"
        ishanga_partner_id =False
        partner_rec_exist = request.env['contact.map'].sudo().search([('sso_id', '=', sso_id)]).mapped('contact_id_fkey')
        if partner_id:
            for partner in partner_id:
                if partner.sso_id == sso_id or matchEngine.compareNamesWithVariations(partner.name, name) in (EXACT_MATCH, HIGH_MATCH):
                    partner_rec_exist |= partner
        if not partner_rec_exist:
            rec_exist = request.env['ishanga.donation.registration'].sudo().search(
                [("sso_id", "=", sso_id),
                 ("is_ishaga_active", "=", True)],limit=1)
            if rec_exist:
                is_exist = True
                ishanga_partner_id = rec_exist.contact_id_fkey.id if rec_exist.contact_id_fkey else False

        if partner_rec_exist:
            rec_exist = [i.id for i in partner_rec_exist if partner_categ.id in i.category_id.ids]

            if not rec_exist:
                rec_exist = request.env['ishanga.donation.registration'].sudo().search(
                    ['|',("contact_id_fkey", "in", partner_rec_exist.ids),("sso_id", "=",sso_id),
                     ("is_ishaga_active", "=", True)],limit=1).mapped('contact_id_fkey.id')
            if rec_exist:
                is_exist = True
                ishanga_partner_id=rec_exist[0]
        else:
            msg = "Invaid SSO ID"
        return {"valid":is_exist, "msg": msg,"partner_id":ishanga_partner_id}


    @http.route('/ishanga_success', type='http', auth="public", methods=["GET", "POST"], website=True, csrf=False)
    def ishanga_success(self, **post):
       if request.httprequest.method == "GET":
          values={
              'status':'no_program',
          }
          return request.render('isha_ishanga_reg.donation_user_message_page', values)
          # return request.render("isha_ishanga_reg.ishanga_registration_success_new",values)

    def get_ishalife_in_url(self,ssoid):
        url = ""
        don_rec = request.env['ishanga.donation.registration'].sudo().search([("sso_id", "=",ssoid)])
        if not don_rec:
            profile_details = self.sso_get_full_data(ssoid)
            country_code = profile_details['basicProfile']['countryOfResidence']
        if don_rec:
            if don_rec.country:
                country_code = don_rec.country.code
            else:
                country_code = "IN"
        country_data = request.env['res.country'].sudo().search([('code', '=', country_code)])
        _logger.info("------------------country_data----------------")
        _logger.info(str(country_code))
        if country_data:
            # request.env['ishanga.configuration'].sudo().search([("country_ids", '=', rec_country.id)])
            res_q = request.env['ishanga.configuration'].sudo().search([("country_ids","=",country_data.id)],limit=1)
            _logger.info("------------------res_q----------------")
            _logger.info(str(res_q))
            if res_q:
               url = res_q.value
               if url =="middle.east@ishafoundation.org" or url =="shoppe@ishalife.co.za":
                  base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
                  url = base_url + '/ishanga/support_mail?mail_to='+(str(url))
        return url,country_code

    @http.route('/online/bog_nanmaipage_registratio_complete', type='http', auth="public", methods=["GET", "POST"],
                website=True, csrf=False)
    def redirectoishalife(self, **post):
        # print(post)
        # print(request.httprequest.method)
        if request.httprequest.method == "POST":
            if post.get("isha_life_url"):
                ishalife_in_url = post.get("isha_life_url")
                # for SA issue , allow to redirect to form in client side to avoid redirecton problm
                data ={}
                data['isha_life_url']=ishalife_in_url
                return request.render('isha_ishanga_reg.ishanga_isha_life_url_redirection_page', data)
                # return werkzeug.utils.redirect(ishalife_in_url)


    @http.route('/online/bog_nanmaipage_registration', type='http', auth="public", methods=["GET", "POST"], website=True, csrf=False)
    def bond_of_grace_page(self, **post):
        if request.httprequest.method == "POST":
            sso_id = post.get('sso_id')
            ishalife_in_url,cc_code = self.get_ishalife_in_url(sso_id)
            _logger.info("------------------ishalife_in_url----------------")
            # _logger.info(ishalife_in_url)
            program_registration_status ="interest_to_buy"
            if post.get("reg_id"):
                user_rec = request.env['ishanga.nu.program.registration'].sudo().browse(int(post.get("reg_id")))
                if user_rec and "yes_haveit" in post:
                    program_registration_status ="already_have"
                    user_rec.write({"program_registration_status":program_registration_status})
                    return request.render("isha_ishanga_reg.donation_user_message_page", {"status":"display_text"})
            vals = {"ishanga_flag": post.get('ishanga_flag'), "sso_id": post.get('sso_id'),
                    "profile_info": post.get("profile_info"), "session_id": post.get("page_session_id"),
                    "program_registration_status": "",
                    "action_url": "/online/bog_nanmaipage_registratio_complete",
                    "reg_id": "",
                    "isha_life_url":ishalife_in_url
                    }
            partner_categ = request.env['res.partner.category'].sudo().search([("name", "=", "7% Ishanga")])
            partner_id = request.env['res.partner'].sudo().search([("sso_id", "=", post.get('sso_id'))],limit=1)
            is_old_ishanga= False
            if partner_id:
                rec_exist = [i.id for i in partner_id.category_id if i.id == partner_categ.id]
                if rec_exist:
                    is_old_ishanga = True
            if is_old_ishanga==True:
                return request.render("isha_ishanga_reg.ishanga_auth_template", vals)
            else:
                # ishalife_in_url = request.env['ir.config_parameter'].sudo().get_param('ishanga7.ishanga7_ishalife_in_url')
                return werkzeug.utils.redirect(ishalife_in_url)
            # return request.render("isha_ishanga_reg.bog_ishanga_nanmai_template", values)

    @http.route('/user_msg', type='http', auth="public", methods=["GET", "POST"], website=True, csrf=False)
    def ishanga_successnew(self, **post):
        if request.httprequest.method == "GET":
            values = {
                'message': 'ffdfd',
            }
            return request.render("isha_ishanga_reg.ishanga_nanmai_template", values)
            # return request.render("isha_ishanga_reg.user_msg", values)

    @http.route('/nu/registration/thankpage', type='http', auth="public", methods=["GET", "POST"], website=True, csrf=False)
    def registration_thankyoupage(self, **post):

        return request.render("isha_ishanga_reg.not_attending_progrm_tmp", values)

    @http.route('/ishanga_duplicate', type='http', auth="public", methods=["GET", "POST"], website=True, csrf=False)
    def ishanga_duplicate(self, **post):
        if request.httprequest.method == "GET":
            return request.render("isha_ishanga_reg.ishanga_duplicate_registration")

    @http.route(['/ishanga_registration'], csrf=False, methods=["GET", "POST"],type='http', auth="public", website=True)
    def ishanga_registration(self,origin="", **post):
        if request.httprequest.method == "GET":
             _logger.info("------------------ISHANGA FIRST STEP ----------------")

             validation_errors = []
             try:
                 ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                 cururls = str(request.httprequest.url).replace("http://", "https://", 1)
                 sso_log = {'request_url': str(cururls),
                            "callback_url": str(cururls),
                            "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                            "hash_value": "",
                            "action": "0",
                            "legal_entity": "IF",
                            "force_consent": "1"
                            }
                 ret_list = {'request_url': str(cururls),
                             "callback_url": str(cururls),
                             "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                             }
                 data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
                 data_enc = data_enc.replace("/", "\\/")
                 signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                                      digestmod=hashlib.sha256).hexdigest()
                 sso_log["hash_value"] = signature
                 sso_log["sso_logurl"] = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_URL1')
                 values = {
                     'sso_log': sso_log
                 }
                 return request.render('isha_livingspaces.livingspacessso', values)
             except Exception as ex:
                 _logger.error(
                     "Error caught during request creation")
                 validation_errors.append("Unknown server error. See server logs.")
                 validation_errors.append(ex)
                 values = {'language': ''}
                 return request.render('isha_livingspaces.exception', values)
        if request.httprequest.method == "POST" and "session_id" in request.httprequest.form:
            _logger.info("------------------ISHANGA POST ----------------")
            ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
            api_key = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.SSO_API_KEY1')  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
            session_id = request.httprequest.form['session_id']
            hash_val = self.hash_hmac({'session_id': session_id}, ret)
            data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
            request_url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
            sso_user_profile = eval(self.sso_get_user_profile(request_url, data).text)
            auto_mail = sso_user_profile["autologin_email"]
            profile_id = sso_user_profile["autologin_profile_id"]
            fullprofileresponsedatVar = request.env['livingspaces.ssoapi'].sudo().get_fullprofileresponse_DTO()
            if "autologin_email" in sso_user_profile:
                fullprofileresponsedatVar["basicProfile"]["email"] = sso_user_profile["autologin_email"]
                fullprofileresponsedatVar["profileId"] = sso_user_profile["autologin_profile_id"]
            fullprofileresponsedatVar = self.sso_get_full_data(sso_user_profile['autologin_profile_id'])
            print(fullprofileresponsedatVar)
            print(fullprofileresponsedatVar['basicProfile']['email'])
        # if True:
        #
        #     fullprofileresponsedatVar = {
        #         "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
        #         "basicProfile": {
        #             "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
        #             "createdDate": "2020-05-05T10:54:05Z",
        #             "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
        #             "lastModifiedDate": "2020-06-02T11:37:57Z",
        #             "id": 43083,
        #             "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
        #             "email": "krishna.t@ishafoundation.org",
        #             "firstName": "K",
        #             "lastName": "T",
        #             "profilePic": "https://lh3.googleusercontent.com/-I2gNoP1r5kg/AAAAAAAAAAI/AAAAAAAAAAA/AAKWJJOfNXvcjzTF3XhKfjkoCRnjfDrP3Q/photo.jpg?size=250",
        #             "phone": {
        #                 "countryCode": "91",
        #                 "number": "7867765697"
        #             },
        #             "gender": "MALE",
        #             "dob": "1950-06-01",
        #             "countryOfResidence": "IN"
        #         },
        #         "extendedProfile": {
        #             "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x2",
        #             "createdDate": "2020-05-05T10:54:05Z",
        #             "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
        #             "lastModifiedDate": "2020-06-02T11:37:57Z",
        #             "id": 43081,
        #             "passportNum": "sfswe",
        #             "nationality": "IN",
        #             "pan": "11",
        #             "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3"
        #         },
        #         "documents": [],
        #         "attendedPrograms": [],
        #         "profileSettingsConfig": {
        #             "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
        #             "createdDate": "2020-05-05T10:54:05Z",
        #             "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
        #             "lastModifiedDate": "2020-05-05T10:54:05Z",
        #             "id": 43086,
        #             "nameLocked": False,
        #             "isPhoneVerified": False,
        #             "phoneVerificationDate": None,
        #             "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3"
        #         },
        #         "addresses": [
        #             # {
        #             #     "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
        #             #     "createdDate": "2020-05-22T06:36:08Z",
        #             #     "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
        #             #     "lastModifiedDate": "2020-06-02T11:37:57Z",
        #             #     "id": 18750,
        #             #     "addressType": "RESIDENCE",
        #             #     "addressLine1": "a1",
        #             #     "addressLine2": "a2",
        #             #     "townVillageDistrict": "a",
        #             #     "city": "Secunderabad",
        #             #     "state": "Telangana",
        #             #     "country": "IN",
        #             #     "pincode": "500055",
        #             #     "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3"
        #             # }
        #         ]
        #     }
        #     session_id = 44

            # profile_info = json.dumps(fullprofileresponsedatVar)
            profile_info = json.dumps(fullprofileresponsedatVar)

            is_program_avaible = self.check_program_exist(fullprofileresponsedatVar['profileId'],fullprofileresponsedatVar['basicProfile']['countryOfResidence'])
            user_respne = self.check_user_isishanga(fullprofileresponsedatVar['profileId'])
            ishanga_flag = 'N'
            if user_respne.get("valid") == True:
                ishanga_flag = 'Y'
            values = {"ishanga_flag": ishanga_flag, "sso_id": fullprofileresponsedatVar['profileId'],
                      "profile_info": profile_info, "session_id": session_id}
            sso_id =fullprofileresponsedatVar['profileId']
            if origin=="Donate" or origin.strip() == "'Donate'":
                if user_respne.get("valid") == True:
                    return self.open_outreach_donationpage(sso_id, ishanga_flag, session_id)
                else:
                    # while click on on change email button on new load the logout values
                    logout_values = self.sso_logout()
                    values['logout_values'] = logout_values
                    values['show_header'] = True
                    # values['ishagan_header'] = True
                    return request.render('isha_ishanga_reg.ishanga_new_registration', values)

            # if ishanga and program is avaible then follow up
            if user_respne.get("valid") == True:
                    if is_program_avaible.get("valid") == True:
                        values['prog_date'] = is_program_avaible.get("prog_date")
                        values['program_schedule_id'] = is_program_avaible.get("schedule_id")
                        event_start = is_program_avaible.get("prog_date")
                        event_date1 = datetime.strptime(str(event_start), '%Y-%m-%d').strftime('%d-%b-%Y')
                        event_date2 = datetime.strptime(str(event_start), '%Y-%m-%d').strftime('%d-%b')
                        values['program_id'] = is_program_avaible.get("progrm_id")
                        values['event_date1'] = event_date1
                        values['event_date2'] = event_date2
                        terms_url = request.env['ir.config_parameter'].sudo().get_param('ishanga7.ishanga7_term_condition_in')
                        if fullprofileresponsedatVar['basicProfile']['countryOfResidence']!="IN":
                            terms_url = request.env['ir.config_parameter'].sudo().get_param('ishanga7.ishanga7_term_condition_non_in')
                        values['terms_url'] = terms_url
                        values['even_time'] = "11 AM IST"
                        # values['show_header'] = True
                        return request.render('isha_ishanga_reg.ishanga_nanmai_template', values)
                    if is_program_avaible.get("valid") != True:
                       values['status']="no_program"
                        # for ishagna user allow them to display thankyou msg with  donateurl for redirection
                       return request.render('isha_ishanga_reg.donation_user_message_page',values)
                        # for already ishanga - redirect to outreach with sessionid,ssoid
                        # return self.open_outreach_donationpage(sso_id, ishanga_flag, session_id)

           # for register action directly goes to auth page instead of change email page
            if user_respne.get("valid") == False:
                action_url = "/online/donationpage"
                vals = {"ishanga_flag": ishanga_flag, "sso_id":sso_id,
                        "profile_info": profile_info, "session_id": session_id,
                        "action_url": action_url}
                return request.render("isha_ishanga_reg.ishanga_auth_template", vals)


            else:
                    # base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
                    # return request.render('isha_ishanga_reg.auth_template', {})
                    # session_id ="dfdfd"
                    user_data = json.dumps(values)
                    encode_user_data = base64.b64encode(user_data.encode("UTF-8"))
                    s1 = encode_user_data.decode("UTF-8")
                    base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
                    redirect_url = base_url + "/online/donationpage?data=" + s1
                    print(redirect_url)
                    return werkzeug.utils.redirect(redirect_url)


    @http.route(['/nu/registration'], csrf=False, methods=["GET", "POST"], type='http', auth="public", website=True)
    def nu_registration(self, **post):
        data ={}
        data['ishanga_flag'] = post.get("ishanga_flag")
        data['prog_date'] = post.get("prog_date")
        data['program_schedule_id'] = post.get("program_schedule_id")
        data['program_id'] = post.get("program_id")
        data['sso_id'] =post.get("sso_id")
        data['donation_id'] =post.get("donation_reg_id")
        pg_date = post.get("prog_date")
        event_date1 =datetime.strptime(str(pg_date), '%Y-%m-%d').strftime('%d-%b-%Y')
        data['event_date1'] = event_date1
        profile_details = self.sso_get_full_data(post.get('sso_id'))
        terms_url = request.env['ir.config_parameter'].sudo().get_param('ishanga7.ishanga7_term_condition_in')
        if profile_details['basicProfile']['countryOfResidence'] != "IN":
            terms_url = request.env['ir.config_parameter'].sudo().get_param(
                'ishanga7.ishanga7_term_condition_non_in')
        data['terms_url'] = terms_url

        print(data)

        return request.render('isha_ishanga_reg.ishanga_nanmai_template', data)

    @http.route(['/ishanga/support_mail'], csrf=False, type='http', auth="public", website=True)
    def ishanga_support_mail(self, **post):
        """ Redirect to client(mail account)"""
        if True:
            data={}
            if post.get("mail_to"):
                mail_to = "mailto:"+str(post.get("mail_to"))
            data["mail_to"]=mail_to
            return request.render('isha_ishanga_reg.ishanga_mail_support_page', data)

    @http.route(['/donation/authpage'], csrf=False, type='http', auth="public", website=True)
    def authpage(self, **post):
        print(post)
        if request.httprequest.method == "POST" and "session_id" in request.httprequest.form:
            print("load page")
        action_url = "/online/donationpage"
        vals = {"ishanga_flag": post.get('ishanga_flag'), "sso_id": post.get('sso_id'),
                "profile_info": post.get("profile_info"), "session_id": post.get("page_session_id"),
                "action_url": action_url}
        return request.render("isha_ishanga_reg.ishanga_auth_template", vals)

    def check_pending_recurring(self,sso_id,ishanga_flag):
        """  check for the current sso user has already any pending transaction for recurring type if yes, then display only the onetime view to login user in donation page"""
        is_pending_exit =False
        _logger.info("CHeck Recurring ---------")
        if ishanga_flag=="N":
            rec = request.env['ishanga.nu.payment.transaction'].sudo().search([("donation_type","=","recurring"),
                                                                                   ("type","=","kafka_response"),
                                                                                    ("sso_id","=",sso_id),
                                                                                    ("donation_status", "!=", '')
                                                                                   ],order="id desc",limit=1)
            _logger.info("ishanga_flag---------")
            _logger.info(str(ishanga_flag))
            _logger.info(str(rec))
            if rec:
                if rec.donation_status == "pending_activation":
                    is_pending_exit=True
        return is_pending_exit

    @http.route(['/online/donationpage'], csrf=False, type='http', auth="public", website=True)
    def onlinedonation(self, **post):
        values = {}
        print(post)
        session_id = ""
        profile_info = ""
        sso_id = ""
        ishanga_flag = "Y"
        print(request.httprequest.method)
        if post.get("data"):
            application_data = post.get('data')
            decode_application_data = base64.b64decode(application_data)
            decode_data = decode_application_data.decode("UTF-8")
            json_list_params = json.loads(decode_data)
            profile_info = json_list_params.get('profile')
            sso_id = json_list_params.get('sso_id')
            session_id = json_list_params.get('session_id')
            ishanga_flag = json_list_params.get('ishanga_flag')
        if request.httprequest.method == "GET" or post.get('method') == 'GET':
            if post.get('method') == 'GET':
                sso_id = post.get("sso_id")
                # check the sso_id is working or not
                profile_info = post.get("sso_id")
                ishanga_flag = post.get("ishanga_flag")
                session_id = post.get("page_session_id")
            re =str(date.today()).split('-')
            print(re)
            date_val =re[2]
            date_arr =[]
            x = range(1, 32)

            for n in x:
                date_arr.append(n)
            is_pendig_recurring_exist = self.check_pending_recurring(sso_id,ishanga_flag)
            _logger.info("==========is_pendig_recurring_exist======")
            _logger.info(str(is_pendig_recurring_exist))
            values = {
                "sso_id": sso_id,
                "profile_info": profile_info,
                "min_amount": 200,
                "ishanga_flag": ishanga_flag,
                "page_session_id": session_id,
                "today_date":int(date_val),
                "date_arr":date_arr,
                "show_recurring":True if not is_pendig_recurring_exist else False,
                "show_header":False,
                "ishagan_header":True,
            }
            _logger.info("=======Reader Page=====")
            _logger.info(str(is_pendig_recurring_exist))
            return request.render('isha_ishanga_reg.donation_templ_view', values)
        elif request.httprequest.method == "POST":
            print(post)
            donation_type = "onetime"
            amount = post.get('custom_amount_onetim')
            if post.get("freq") == '1':
                donation_type = "recurring"
                amount = post.get('custom_amount_recu')

            donation_type = donation_type
            donation_currency = post.get("hidden_donation_currency")
            contribute_amount = amount
            converted_amount = amount
            session_id = post.get('page_session_id')
            profile_info = post.get('profile_info')
            total_amount = 0
            day_of_month = post.get("dayOfMonth")
            values = {"session_id": session_id, "day_of_month": day_of_month, "donation_amt": contribute_amount,
                      "donation_type": donation_type, "donation_currency": donation_currency, "profile": profile_info}
            application_data = json.dumps(values)
            tmpurl = request.httprequest.url.replace('donationpage', 'person_details')
            # application_data = "profile_info"+profile_info+"donation_type=" + donation_type+ '&donation_currency=' + str(donation_currency)
            encode_application_data = base64.b64encode(application_data.encode("UTF-8"))
            s1 = encode_application_data.decode("UTF-8")
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
            redirect_url = base_url + "/online/person_details?values=" + s1
            print(redirect_url)
            return werkzeug.utils.redirect(redirect_url)

    @http.route(['/registration_status'], csrf=False, type='http', auth="public", website=True)
    def registration_status(self, **post):
        if post.get("reg_id"):
            user_reg_rec = request.env['ishanga.nu.program.registration'].sudo().search([("id", "=", int(post.get("reg_id")))])
            if user_reg_rec:
                if user_reg_rec.program_registration_status == "interest_to_buy":
                    user_reg_rec.write({"message": "Redirect to ishalife","is_auth":True})
                    ishalife_in_url,cc_code = self.get_ishalife_in_url(user_reg_rec.sso_id)
                    return werkzeug.utils.redirect(ishalife_in_url)

                if user_reg_rec.program_registration_status == "already_have":
                    user_reg_rec.write({"message": "Registration is completed", "program_registration_status": "complete","is_auth":True})
                    show_recurring = True
                    read_recurring = request.env['ishanga.nu.payment.transaction'].sudo().search([("sso_id","=",user_reg_rec.sso_id),
                                                                                                  ("type","=","request"),
                                                                                                  ("donation_type","=","recurring")
                                                                                            ],order="write_date desc",limit =1)
                    pg_date = user_reg_rec.program_schedule_id.start_date
                    if read_recurring:
                        show_recurring=False
                    # r_val = {"reg_status": "Completed","show_recurring":show_recurring,"pg_date":pg_date}
                    event_date1 = datetime.strptime(pg_date, '%Y-%m-%d').strftime('%d-%b-%Y')
                    event_date2 = datetime.strptime(pg_date, '%Y-%m-%d').strftime('%d-%b')
                    r_val['status'] = "nu_success"
                    r_val['event_date1'] = event_date1
                    r_val['event_date2'] = event_date2
                    r_val['even_time'] = "11 AM IST"
                    r_val['show_recurring'] = show_recurring
                    # for ishagna user allow them to display thankyou msg with  donateurl for redirection
                    return request.render('isha_ishanga_reg.donation_user_message_page', r_val)
                    # return request.render("isha_ishanga_reg.ishanga_registration_success", r_val)

    @http.route(['/online/nanmaipage_registration'], csrf=False, type='http', auth="public", website=True)
    def onlinenanmaipage(self, **post):
        if request.httprequest.method == "POST":

            _logger.info("----------------------------------")
            _logger.info("*** POST Method -- Online Namai Page Details")
            _logger.info("####POST VLAUES IN NU REGSITRATION")
            _logger.info(str(post))
            if post.get("nanmai_check")=="yes":
                action_url = "/registration_status"
                # if post.get('already_order') == '':
                #     status = "already_have"
                #     message = "Already they have NU Yantra"
                # else:
                status = "interest_to_buy"
                last_name =""
                first_name =""
                email =""
                addressline=""
                city=""
                state=""
                pincode=""
                country=""
                phone=""
                profile_details = self.sso_get_full_data(post.get('sso_id'))
                if profile_details!="":
                    last_name = profile_details["basicProfile"]['lastName']
                    first_name = profile_details["basicProfile"]['firstName']
                    email = profile_details["basicProfile"]['email']
                    if len(profile_details['addresses']) > 0:
                        address_dict = self.replaceEmptyString(profile_details['addresses'][0])
                        street = address_dict['addressLine1']
                        street2 = address_dict['addressLine2']
                        if address_dict['townVillageDistrict']:
                            if street2 and type(street2) == str:
                                street2 = street2 + ', ' + address_dict['townVillageDistrict']
                            else:
                                street2 = address_dict['townVillageDistrict']
                        city = address_dict['city']
                        state = address_dict['state']
                        country = address_dict['country']
                        zip = address_dict['pincode']
                        addressline = str(street)+''+str(street2)
                    country_rec = request.env['res.country'].sudo().search([("code", "=", str(country))])
                    if profile_details['basicProfile']['phone']['countryCode']:
                        phone= str(profile_details['basicProfile']['phone']['countryCode'])+''+str(profile_details['basicProfile']['phone']['number'])
                    country_rec_id= False
                    if country_rec:
                        country_rec_id=country_rec.id
                partner_details = self.check_user_isishanga(post.get('sso_id'))
                rec = {"sso_id": post.get('sso_id'),
                       "is_ishaga_active": True if post.get('ishanga_flag') == 'Y' else False,
                       "program_registration_status": status,
                       "program_id": int(post.get('program_id')),
                       "last_name":last_name,
                       "first_name":first_name,
                       "email":email,
                       "profile_info":profile_details,
                       "program_schedule_id": int(post.get('program_schedule_id')),
                       "addressline": addressline,
                       "city": city,
                       "state": state,
                       "pincode": pincode,
                       "country_id": country_rec_id,
                       "phone":phone,
                       "contact_id_fkey":partner_details.get("partner_id")
                       }
                _logger.info("CREATE NU ENTRY " + str(rec))
                if post.get("donation_reg_id"):
                    rec['donation_reg_id'] = int(post.get("donation_reg_id"))
                chck_rec = request.env['ishanga.nu.program.registration'].sudo().search(
                            [("sso_id", "=", post.get('sso_id')),
                             ("program_schedule_id", "=", int(post.get('program_schedule_id'))),
                             ("program_id", "=", int(post.get('program_id')))
                             ],limit=1)
                if not chck_rec:
                    ishalife_in_url,cc_code = self.get_ishalife_in_url(post.get('sso_id'))
                    rec['isha_life_url'] = ishalife_in_url
                    rec['country_code'] = cc_code
                    user_reg_rec = request.env['ishanga.nu.program.registration'].sudo().create(rec)
                    if user_reg_rec.donation_reg_id:
                        donation_rec = user_reg_rec.donation_reg_id
                        user_reg_rec.write({"addressline":donation_rec.addressline,
                                             "city":donation_rec.city,
                                             "phone":str(donation_rec.countrycode) +""+str(donation_rec.mobile),
                                             "state":donation_rec.state,
                                             "is_ishaga_active":donation_rec.is_ishaga_active,
                                             "pincode":donation_rec.pincode,
                                             "nationality_id":donation_rec.nationality_id.id,
                                             "country_id":donation_rec.country.id})
                    # hided the code , to sent auto mail on eery registration -BISHKA team will manualaly do it
                    user_reg_rec.send_registration_succes_mail()
                else:
                    chck_rec.write(rec)
                    user_reg_rec =chck_rec
                pg_date = user_reg_rec.program_schedule_id.start_date
                event_date1 = datetime.strptime(str(pg_date), '%Y-%m-%d').strftime('%d-%b-%Y')
                # if user_reg_rec:
                #     if user_reg_rec.program_registration_status == "interest_to_buy":
                #         user_reg_rec.write({"message": "Redirect to ishalife", "is_auth": True})
                #         ishalife_in_url = request.env['ir.config_parameter'].sudo().get_param(
                #             'ishanga7.ishalife_in_url')
                #         return werkzeug.utils.redirect(ishalife_in_url)
                #
                #     if user_reg_rec.program_registration_status == "already_have":
                #         user_reg_rec.write(
                #             {"message": "Registration is completed", "program_registration_status": "complete",
                #              "is_auth": True})
                #         show_recurring = True
                #         read_recurring = request.env['ishanga.nu.payment.transaction'].sudo().search(
                #             [("sso_id", "=", user_reg_rec.sso_id),
                #              ("type", "=", "request"),
                #              ("donation_type", "=", "recurring")
                #              ], order="write_date desc", limit=1)
                #         pg_date = user_reg_rec.program_schedule_id.start_date
                #         if read_recurring:
                #             show_recurring = False
                #         r_val={}
                #         # r_val = {"reg_status": "Completed","show_recurring":show_recurring,"pg_date":pg_date}
                #         event_date1 = pg_date.strftime('%d-%b-%Y')
                #         event_date2 = pg_date.strftime('%d-%b')
                #         r_val['status'] = "nu_success"
                #         r_val['event_date1'] = event_date1
                #         r_val['event_date2'] = event_date2
                #         r_val['even_time'] = "11 AM IST"
                #         r_val['show_recurring'] = show_recurring
                #         # for ishagna user allow them to display thankyou msg with  donateurl for redirection
                #         return request.render('isha_ishanga_reg.donation_user_message_page', r_val)
                profile_details = self.sso_get_full_data(post.get('sso_id'))
                terms_url = request.env['ir.config_parameter'].sudo().get_param('ishanga7.ishanga7_term_condition_in')
                if profile_details['basicProfile']['countryOfResidence'] != "IN":
                    terms_url = request.env['ir.config_parameter'].sudo().get_param(
                        'ishanga7.ishanga7_term_condition_non_in')
                vals = {"ishanga_flag": post.get('ishanga_flag'), "sso_id": post.get('sso_id'),
                        "profile_info": post.get("profile_info"), "session_id": post.get("page_session_id"),
                        "program_registration_status": status,
                        "action_url": "",
                        "event_date1": event_date1,
                        "terms_url":terms_url,
                        "reg_id": user_reg_rec.id}

                return request.render("isha_ishanga_reg.bog_ishanga_nanmai_template", vals)
            else:
                return request.render("isha_ishanga_reg.donation_user_message_page", {"status":"display_text"})

    def open_outreach_donationpage(self, sso_id, ishanga_flag, session_id):
        """
        :param record_id: rec id
        :return: redirect to payment page
        """
        try:
            _logger.info("-iner redireciton----")
            _logger.info("-iner redireciton----")
            callbackUrl = request.env['ir.config_parameter'].sudo().get_param('ishanga7.outreach_redirect_dontate_url')
            values = {"url": callbackUrl,
                      "session_id": session_id,
                      "ishanga_flag": ishanga_flag,
                      "sso_id": sso_id
                      }
            return request.render("isha_ishanga_reg.direct_outreach_redirection_template", values)
        except Exception as e:
            values = {"status": 'Error', 'message': "There is error in sending mail",
                      'email': ""}
            tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
            _logger.info(tb_ex)
            return request.render('isha_ishanga_reg.exception', values)

    def redirect_to_outreach(self, sso_id, request_id, session_id):
        """
        :param record_id: rec id
        :return: redirect to payment page
        """
        try:
            _logger.info("-iner redireciton----")
            _logger.info("-iner redireciton----")
            callbackUrl = request.env['ir.config_parameter'].sudo().get_param('ishanga7.outreach_redirect_url')
            values = {"url": callbackUrl,
                      "ssoId": sso_id,
                      "requestId": request_id,
                      "session_id": session_id
                      }
            return request.render("isha_ishanga_reg.outreach_redirection_template", values)
        except Exception as e:
            values = {"status": 'Error', 'message': "There is error in sending mail",
                      'email': ""}
            tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
            _logger.info(tb_ex)
            return request.render('isha_ishanga_reg.exception', values)

    def validation_text_field(self,state_name):
        """ Validate text field"""
        status =True
        msg=""
        state_name = state_name
        match = re.match('^[a-zA-Z ]+$',state_name)
        if match == None:
            msg = "Please enter valid state name"
            status = False

        return status,msg


    @http.route(['/online/person_details'], type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def donationpage(self, **post):
        """ """
        print(request.httprequest.method)
        if (post.get("values")):
            application_data = post.get('values')
            decode_application_data = base64.b64decode(application_data)
            decode_data = decode_application_data.decode("UTF-8")
            json_list_params = json.loads(decode_data)
            profile_info = json_list_params.get('profile')
            if profile_info=='' or profile_info==None:
                return request.render("isha_ishanga_reg.donation_user_message_page", {"status": "no_profile"})
            pprofile = self.sso_get_full_data(profile_info)
            session_id = json_list_params.get('session_id')
        # session_id = "f417837ad9adf48ea397ebbf9f07a3c338799333"
        if request.httprequest.method == "GET":
            _logger.info("----------------------------------")
            _logger.info("*** GET Method -- Online Personal Details")
            _logger.info("----------------------------------")
            _logger.info("*** session_id")
            _logger.info(str(session_id))
            data = {}
            overall_amount_inr = json_list_params.get('donation_amt')
            coversion_amt_inr = json_list_params.get('donation_amt')
            donation_message = "Your Contribution of ₹ " + str(coversion_amt_inr) + " is greatly appreciated"
            if json_list_params.get('donation_currency') == 'USD':
                # api_cur_url = "http://apilayer.net/api/live?access_key=d05f7e5bf5b90f7a377abb6f01d3ed7a&currencies=INR&source=USD&format=1"
                # cur_response = requests.post(api_cur_url)
                # text_vl = cur_response.text
                # currency_conversion_arr = json.loads(text_vl)
                # currency_convr_value = currency_conversion_arr['quotes']['USDINR']
                currency_convr_value = request.env['ir.config_parameter'].sudo().get_param(
                    'ishanga7.outreach_donation_usd_conversion_amount')
                amount = float(json_list_params.get('donation_amt')) * (float(currency_convr_value))
                coversion_amt_inr = float(amount)
                coversion_amt_inr = round(coversion_amt_inr)
                overall_amount_inr = coversion_amt_inr
                donation_message = "We have converted your donation amount of " + str(
                    json_list_params.get('donation_amt')) + " US Dollars (USD) to " + str(
                    coversion_amt_inr) + " Indian Rupees (INR). You will see this INR value in your receipt and the payment screens."
            if json_list_params.get('donation_type') == "recurring":
                overall_amount_inr = float(overall_amount_inr) * 12
                donation_message =  "Donate ₹ "+str(json_list_params.get('donation_amt'))+" monthly on the "+str(json_list_params.get('day_of_month'))+"th of each month until cancelled by you"
            print(json_list_params.get('donation_type'))
            load_country_list={'ZA',
                    'UG',
                    'DK',
                    'VN',
                    'TH',
                    'PH',
                    'ID',
                    'NL',
                    'SI',
                    'GB',
                    'CH',
                    'HR',
                    'LT',
                    'KR',
                    'IT',
                    'BG',
                    'AE',
                    'JP',
                    'BE',
                    'SG',
                    'KE',
                    'LB',
                    'OM',
                    'NZ',
                    'AT',
                    'FI',
                    'QA',
                    'MU',
                    'RO',
                    'MY',
                    'BH',
                    'SE',
                    'CN',
                    'PL',
                    'DE',
                    'AU',
                    'IE',
                    'NO',
                    'FR',
                    'BN',
                    'TW',
                    'HK',
                    'MO',
                    'IR',
                    'IQ',
                    'JO',
                    'KW',
                    'PK',
                    'PS',
                    'SA',
                    'SY',
                    'YE',
                    'AX',
                    'AD',
                    'CY',
                    'CZ',
                    'EE',
                    'FO',
                    'GI',
                    'GR',
                    'GG',
                    'VA',
                    'HU',
                    'IM',
                    'JE',
                    'LV',
                    'LI',
                    'LU',
                    'MT',
                    'MC',
                    'MK',
                    'PT',
                    'SM',
                    'SK',
                    'ES',
                     'TR',
                    'SJ'}
            all_countries = request.env['res.country'].sudo().search([('code','in', list(load_country_list))], order="name asc")
            nationality_countries = request.env['res.country'].sudo().search([('code','!=','IN')], order="name asc")
            in_countries = request.env['res.country'].sudo().search([('code', '=', 'IN')], order="name asc")
            list_of_country = all_countries+in_countries
            state_list = request.env['res.country.state'].sudo().search([("country_id","in",list_of_country.ids),("country_id.code","!=","CN")])
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
            back_url = base_url+"/ishanga_registration?origin='Register'"
            donate_url = base_url+"/ishanga_registration?origin='Donate'"
            data = {"sso_id": pprofile.get("profileId"),
                    "profile_info": json.dumps(pprofile),
                    "donation_type": json_list_params.get('donation_type'),
                    "donation_currency": json_list_params.get('donation_currency'),
                    "donation_amt": json_list_params.get('donation_amt'),
                    "day_of_month": json_list_params.get('day_of_month'),
                    'nationality_countries':nationality_countries,
                    "coversion_amt_inr": coversion_amt_inr,
                    "overall_amount_inr": overall_amount_inr,
                    'donation_message': donation_message,
                    "session_id": session_id,
                    'countries': all_countries,
                    'in_countries': in_countries,
                    'states': state_list,
                    "show_header": False,
                    "ishagan_header":True,
                    "back_url":back_url,
                    "donate_url":donate_url,
                    }
            # return request.render('isha_livingspaces.livingspacessso', data)

            pass
        elif request.httprequest.method == "POST":
            _logger.info("----------------------------------")
            _logger.info("*** POST Method -- Online Personal Details")
            country_id = post.get("country")
            profile_info = post.get("profile_info")
            # added State name validation on odoo
            if post.get('state_name_text')!="":
                valid_true,msg = self.validation_text_field(post.get('state_name_text'))
                if not valid_true:
                    datad = {"message": msg}
                    return request.render('isha_ishanga_reg.user_msg', datad)

            if country_id:
                country_id = request.env['res.country'].sudo().search([("code", "=", str(post.get("country")))])
            fcra_nationality_id = post.get("fcra_nationality")
            if fcra_nationality_id:
                fcra_nationality_id = request.env['res.country'].sudo().search(
                    [("code", "=", str(fcra_nationality_id))])

            if post.get('citizen_check') == 'yes':
                citizen_check = "indian"
            else:
                citizen_check = "non_indian"
            if post.get("want_80G") == 'yes':
                want_80G = 'yes'
            else:
                want_80G = 'no'

            if post.get("no_80G_consent") == 'on' or post.get("no_80G_consent") == 'yes':
                no_80G_consent = 'yes'
            else:
                no_80G_consent = 'no'
            # passport
            passport_file_type = ""
            passport_image_fileephoto = ""
            # passportdatas1=""
            passportdatas_v = ""
            if post.get("passport_image_file"):
                passport_file_type = post.get("passport_image_file").filename.split('.')[1]
                passport_image_fileephoto = request.httprequest.files.getlist('passport_image_file')
                passport = passport_image_fileephoto[0].read()
                # passportdatas1 = base64.b64encode(passport)
                passportdatas_v = base64.b64encode(passport).replace(b'\n', b'')

            if post.get("email"):
                email_text = str(post.get("email"))
            else:
                email_text = ''
            sso_id = post.get("sso_id")
            # partner_id_rec = request.env['res.partner'].sudo().search([("sso_id", "=", sso_id)], order="id desc",
            #                                                           limit=1)
            partner_id_rec=False
            if not partner_id_rec:
                profile_info_values = json.loads(profile_info)
                basic_profile = self.replaceEmptyString(profile_info_values['basicProfile'])
                phone_dict = self.replaceEmptyString(profile_info_values['basicProfile']['phone'])
                extended_profile = self.replaceEmptyString(profile_info_values['extendedProfile'])
                name = basic_profile['firstName']
                if basic_profile['lastName'] and len(basic_profile['lastName']) > 0:
                    name = name + " " + basic_profile['lastName']
                phone = None
                if phone_dict['number']:
                    phone = phone_dict['countryCode'] if phone_dict['countryCode'] else ''
                    phone += phone_dict['number']
                email = basic_profile['email']
                nationality = extended_profile['nationality']
                passport = extended_profile['passportNum'] if extended_profile['passportNum'] else None
                pan = extended_profile['pan'] if extended_profile['pan'] else None
                street = None
                street2 = None
                state = None
                city = None
                country = None
                zip = None
                if len(profile_info_values['addresses']) > 0:
                    address_dict = self.replaceEmptyString(profile_info_values['addresses'][0])
                    street = address_dict['addressLine1']
                    street2 = address_dict['addressLine2']
                    if address_dict['townVillageDistrict']:
                        if street2 and type(street2) == str:
                            street2 = street2 + ', ' + address_dict['townVillageDistrict']
                        else:
                            street2 = address_dict['townVillageDistrict']
                    city = address_dict['city']
                    state = address_dict['state']
                    country = address_dict['country']
                    zip = address_dict['pincode']
                state_rec = request.env['res.country.state'].sudo().search([("name", "=", str(state))])
                country_rec = request.env['res.country'].sudo().search([("code", "=", str(country))])
                whatsapp_number = ""
                whatsapp_cc = ""
                if basic_profile.get('whatsapp', False):
                    whatsapp_cc = basic_profile.get('whatsapp').get('countryCode', None)
                    whatsapp_number = basic_profile.get('whatsapp').get('number', None)
                rec_dict = {
                    'name': name,
                    'phone_country_code': profile_info_values['basicProfile']['phone']['countryCode'],
                    'phone': profile_info_values['basicProfile']['phone']['number'],
                    'email': email,
                    'city': city,
                    'state_id': state_rec.id,
                    'state': state_rec.name,
                    'zip': zip,
                    'country_id': country_rec.id,
                    'country': country_rec.code,
                    'whatsapp_country_code': whatsapp_cc,
                    'whatsapp_number': whatsapp_number,
                    'sso_id': profile_info_values['profileId']
                }
                print(rec_dict)
                _logger.info('---Created PArtner start-')
                _logger.info('---partnert values-')
                # _logger.info(rec_dict)
                res_partner_obj = request.env['res.partner']
                # partner_id_rec = res_partner_obj.sudo().create(rec_dict)
                # _logger.info('---Created PArtner END-')
                # _logger.info(partner_id_rec)
                # _logger.info('---partnert created-')
            state_val =post.get("state_name_text")
            user_state_val = post.get("state_name")
            if user_state_val=="":
                user_state_val = post.get("state_name_text")
            # if country_id.code=="IN":
            #     state_val = post.get("state_name")
            isha_life_url=""
            isha_life_url,cc_code = self.get_ishalife_in_url(sso_id)
            rec_values = {
                "sso_id": sso_id,
                "first_name": post.get("first_name") if post.get("first_name") else '',
                "last_name": post.get("last_name") if post.get("last_name") else '',
                "countrycode": post.get("country_code") if post.get("country_code") else '',
                "mobile": post.get("phone_number") if post.get("phone_number") else '',
                "email": email_text,
                "nationality_id": fcra_nationality_id.id if fcra_nationality_id else 104,
                "country": country_id.id if country_id else 104,
                "state": user_state_val,
                "city": post.get("city") if post.get("city") else '',
                "addressline": post.get("addessline1") if post.get("addessline1") else '',
                "pincode": post.get("pincode") if post.get("pincode") else '',
                "citizenship": citizen_check,
                "want_80g_tax": want_80G,
                "passport_number": post.get('passport_number_input') if post.get('passport_number_input') else '',
                "passport_image": passportdatas_v if post.get("passport_image_file") else False,
                "passport_file_type": passport_file_type,
                "pan_no": post.get("pancard") if post.get("pancard") else '',
                'no_80G_consent': no_80G_consent,
                "aadhar_no": post.get("aadhar_card") if post.get("aadhar_card") else '',
                "profile_info":rec_dict,
                "donate_url":post.get("donate_url"),
                "isha_life_url":isha_life_url,
                "contribute_amount": post.get('hidden_donation_amount'),
                "converted_amount": post.get('coversion_amt_inr')
            }
            ishanga_nu_prog_reg_obj = request.env['ishanga.donation.registration']
            payment_tranasaction_obj = request.env['ishanga.nu.payment.transaction']

            recdyn = ishanga_nu_prog_reg_obj.sudo().search([("sso_id", "=", sso_id)], order="id desc", limit=1)
            if not recdyn:
                _logger.info(str(rec_values))
                recdyn = ishanga_nu_prog_reg_obj.sudo().create(rec_values)
                _logger.info("*** Record Created "+ str(recdyn.id))
            else:
                recdyn.sudo().write(rec_values)


            # recdyn.sudo().write({"contact_id_fkey": partner_id_rec.id})
            _logger.info('PArtner created and Upted in dobnation')
            # _logger.info('Donnation created')
            # request.env.cr.commit()
            # print("********")
            values = {}
            print(post)
            vals = {"sso_id": sso_id,
                    "type": "request",
                    "donation_type": post.get('donation_type'),
                    "contribute_amount": post.get('hidden_donation_amount'),
                    "converted_amount": post.get('coversion_amt_inr'),
                    "donation_currency": post.get('hidden_donation_currency'),
                    "currency": "INR",
                    "status": "Request sent from MOKSHA to OUTREACH",
                    "message": "Request sent from MOKSHA to OUTREACH",
                    "session_id": session_id,
                    "donation_reg_id": recdyn.id,
                    "date": date.today()}
            payment_tranasaction_rec = payment_tranasaction_obj.sudo().create(vals)
            payment_tranasaction_rec.sudo().write({"tracking_id": payment_tranasaction_rec.id})
            url = "https://www.ishaoutreach.org/api/ishanga/new-registration"
            data = {}
            coversion_amt_inr = float(post.get('coversion_amt_inr'))
            coversion_amt_inr_fd = round(coversion_amt_inr)
            data = {"request_id": payment_tranasaction_rec.id,
                    "donation_amount": post.get('hidden_donation_amount'),
                    "donation_currency": post.get('hidden_donation_currency'),
                    "donate_purpose_id": "Program",
                    "converted_amount": coversion_amt_inr_fd,
                    "converted_currency": "INR",
                    "is_recurring": 0 if post.get('donation_type') == 'onetime' else 1,
                    "no_of_months":'' if post.get('donation_type') == 'onetime' else 12,
                    "day_of_month":'' if post.get('donation_type') == 'onetime' else post.get('day_of_month') ,
                    "until_cancelled_flag":'' if post.get('donation_type') == 'onetime' else 1,
                    "donate_for": "ishanga",
                    "first_name": post.get("first_name"),
                    "last_name": post.get("last_name"),
                    "email": post.get("email"),
                    "country_code": post.get("country_code"),
                    "phone_number": post.get("phone_number"),
                    "nationality": post.get('citizen_check'),
                    "want_80G": want_80G,
                    "fcra_nationality":fcra_nationality_id.code if fcra_nationality_id else '',
                    "nationality_country":fcra_nationality_id.name if fcra_nationality_id else '',
                    "country": country_id.code,
                    "passport_number": post.get("passport_number"),
                    "state_name": user_state_val,
                    "city": post.get("city"),
                    "addessline1": post.get("addessline1"),
                    "pincode": post.get("pincode"),
                    "pancard": post.get("pancard"),
                    "aadhar_card": post.get("aadhar_card"),
                    "agree_tnc": "on",
                    "no_80G_consent": 'on' if no_80G_consent=="yes" else '',
                    "sso_id": sso_id,
                    "lang": "en",
                    "initiative": "contribute",
                    "initiative_short_code": "CON",
                    "donation_title": "Ishanga 7%"
                    }
            if post.get('passport_image_file') or request.httprequest.files.getlist('passport_image_file'):
                if passportdatas_v:
                    data['passport'] = passportdatas_v.decode('UTF-8')
                    data['passport_file_extension'] = passport_file_type
            _logger.info('data-outreach ' + str(data))
            try:
                # if True:
                #     print("dd")
                response = requests.post(url, data=json.dumps(data))
                values = response.text
                print(values)
                print(type(values))
                response_txt = json.loads(values)
                if response_txt['status_code'] == 200:
                    print("Success")
                    return self.redirect_to_outreach(sso_id, payment_tranasaction_rec.id, session_id)
                else:
                    print("Failed")
                    print(response_txt)
                    _logger.info('response_txt')
                    _logger.info(str(response_txt))
                    data={"message":response_txt.get('message')}
                    return request.render('isha_ishanga_reg.user_msg', data)
            except Exception as ex2:
                _logger.info('error in fmf_api_async_push')
                tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
                _logger.error(tb_ex)
                print(ex2)
        return request.render('isha_ishanga_reg.donation_personal_details_view', data)

    @http.route('/read_gateway_response', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def read_outreach_response(self, **post):
        """
        Read the Gateway(outrach response)
        :param post: pass first name in templates
        :return:
        """

        values = {}
        _logger.info("*** Inside the rspisne")
        _logger.info("post value")
        _logger.info(str(post))

        request_id = post.get("request_id")
        transaction_id = post.get("transaction_id")
        is_passport_verified = post.get("is_passport_verified")
        is_recurring = post.get("is_recurring")
        status = post.get("status")
        error_code = post.get("error_code")
        message = post.get("message")
        # post["request_id"] = "49"
        # post["transaction_id"] = "1232"
        # post["amount"] = "123232"
        # post["is_passport_verified"] ="0"
        # post["is_recurring"] = "1"
        # post["status"] = "failed"
        # post["error_code"] = "404"
        # post["message"] = "message"
        # post["redirection_url"] = "ddd"
        # post["subscription_id"] = "343"
        # print(post)
        record_rec = request.env['ishanga.nu.payment.transaction'].sudo().search([("id", "=", post.get("request_id"))])
        sso_id = record_rec.sso_id
        _logger.info("Trnaction rec")
        _logger.info(str(record_rec))
        trans_values = {"sso_id": sso_id,
                        "type": "response",
                        "donation_type": record_rec.donation_type,
                        "contribute_amount": record_rec.contribute_amount,
                        "converted_amount": record_rec.converted_amount,
                        "donation_currency": record_rec.donation_currency,
                        "currency": "INR",
                        "session_id": record_rec.contribute_amount,
                        "donation_reg_id": record_rec.donation_reg_id.id,
                        "transaction_id": post.get("transaction_id"),
                        "tracking_id": record_rec.id,
                        "status": post.get("status"),
                        "message": post.get("message"),
                        "date": date.today()}
        request.env['ishanga.nu.payment.transaction'].sudo().create(trans_values)
        user_country = record_rec.donation_reg_id.country.code
        cor=user_country if record_rec.donation_reg_id.country else ''
        is_program_avaible = self.check_program_exist(sso_id,cor)
        retry_url = request.env['ir.config_parameter'].sudo().get_param('ishanga7.outreach_redirect_url')
        show_progm_info =False
        show_recurring=False

        if post.get("status")=="success" or post.get("status")=="authorized":
            if is_program_avaible.get("valid") == True:
                show_progm_info=True
            if record_rec.donation_type != 'recurring':
                show_recurring = True

        values = {
            "person_name": record_rec.id,
            "request_id": post.get("request_id"),
            "transaction_id": post.get("transaction_id"),
            "amount": record_rec.converted_amount,
            "is_passport_verified": "True" if post.get("is_passport_verified") == '1' else "False",
            "is_recurring": "True" if post.get("is_recurring") == '1' else "False",
            "status": post.get("status"),
            "error_code": post.get("error_code"),
            "message": post.get("message"),
            "redirection_url": "ddd",
            "subscription_id": "342",
            "show_progm_info":show_progm_info,
            # "show_progm_info":True,
            "show_recurring":show_recurring,
            "email_to": "7percent@ishafoundation.org",
            "request_id":record_rec.id,
            "sso_id":sso_id,
            "session_id":record_rec.session_id,
            "retry_url":retry_url,

        }
        values['ishanga_flag'] = True
        values['sso_id'] = record_rec.sso_id
        values['prog_date'] = str(is_program_avaible.get("prog_date"))
        values['program_schedule_id'] = is_program_avaible.get("schedule_id")
        values['program_id'] = is_program_avaible.get("progrm_id")
        values['donation_reg_id'] = int(record_rec.donation_reg_id.id) if record_rec.donation_reg_id else False
        return request.render('isha_ishanga_reg.donation_user_message_page', values)

    @http.route(['/check_is_ishanga'], type='json', auth="public", website=True)
    def verify_is_ishanga(self, **post):
        """ Check the sso user is ishanga or not  by cehcking in moksha system and Ishagna system too
        by 2 checkpoints 1.check the record exist in old records(Ishanga) 2.recors is exist or no in moksha
        """
        # sso_id = "OOmnnIYMFwMNsK1raKPnNjqUDeF3"

        sso_id = post.get("sso_id")
        respne = self.check_user_isishanga(sso_id)
        is_valid = respne.get("valid")
        if respne.get("valid") == True:
            return {"status": "Success", "status_code": 200,
                    "is_ishagna_active": True if is_valid else False
                    }
        if respne.get("valid") == False and respne.get('msg') == 'Success':
            return {"status": "Success", "status_code": 200,
                    "is_ishagna_active": True if is_valid else False
                    }
        if respne.get("valid") == False and respne.get('msg') == 'Invaid SSO ID':
            return {"status": "Success", "status_code": 404,
                    "is_ishagna_active": False,
                    "message": "Invalid SSo ID"
                    }

            # is_exist = self.(sso_id)
            # partner_categ = request.env['res.partner.category'].sudo().search([("name","=","7% Ishanga")])
            # partner_id = request.env['res.partner'].sudo().search([("sso_id","=",sso_id)])
            # if partner_id:
            #     is_exist = [i.id for i in partner_id.category_id if i.id==partner_categ.id]
            #     if not is_exist:
            #         is_exist  = request.env['ishanga.nu.program.registration'].sudo().search([("contact_id_fkey", "=", partner_id.id),
            #                                                                                   ("is_ishaga_active","=", True)])
            return {"status": "Success", "status_code": 200,
                    "is_ishagna_active": True if is_exist else False
                    }
        else:
            return {"status": "Success", "status_code": 404,
                    "is_ishagna_active": False,
                    "message": "Invalid SSo ID"
                    }
