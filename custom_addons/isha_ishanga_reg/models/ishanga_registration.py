import logging
from odoo import api, models, fields
from datetime import date, timedelta, datetime
import requests
import json
_logger = logging.getLogger(__name__)
from ...isha_crm import NameMatchEngine
from ...isha_crm.GoldenContactAdv import EXACT_MATCH,HIGH_MATCH,LOW_MATCH,NO_MATCH


null = None
false = False
true = True

class IshaprogramSchedule(models.Model):
    _name = "isha.program.schedule"
    _rec_name = "program_name"

    program_name = fields.Char(string="Program Name")
    active = fields.Boolean(string="Active")


class IshaprogramScheduletimezonelist(models.Model):
    _name = "isha.program.schedule.timezonelist"
    _rec_name = "program_id"

    program_id = fields.Many2one("isha.program.schedule")
    start_date = fields.Date()
    end_date = fields.Date()
    start_time = fields.Char(string="Start Time")
    end_time = fields.Char(string="End Time")
    active = fields.Boolean(string="Active")
    country_ids = fields.Many2many("res.country")

    def get_month_start(self):
        """ conver the start date format"""
        start_date = self.start_date
        pg_date = str(start_date)
        event_date1 = datetime.strptime(str(pg_date), '%Y-%m-%d').strftime('%d-%b-%Y')
        return event_date1

    def get_first_of_month(self):
        """ conver the start date month"""
        start_date = self.start_date
        pg_date = datetime.strptime(str(start_date), '%Y-%m-%d').replace(day=1)
        event_date1 = datetime.strptime(str(pg_date.date()), '%Y-%m-%d').strftime('%d-%b-%Y')
        return event_date1


class IshangaDonationRegistration(models.Model):
    _name = 'ishanga.donation.registration'
    _rec_name = "first_name"

    first_name = fields.Char(string="First Name")
    last_name = fields.Char(string="Last Name")
    countrycode = fields.Char(string='Country Tel code')
    mobile = fields.Char(string='Mobile')
    email = fields.Char(string='Email')
    nationality_id = fields.Many2one('res.country', 'Nationality')
    country_id = fields.Many2one('res.country', 'Country')
    addressline = fields.Char(string="Address")
    city = fields.Char(string='City', required=False)
    state = fields.Char(string='State', required=False)
    country = fields.Many2one('res.country', 'Country of residence')
    pincode = fields.Char(string='Pin Code', required=False)
    pan_no = fields.Char(string="PAN Number")
    aadhar_no = fields.Char(string="Aadhar Number")
    passport_number = fields.Char(string="Passport")
    passport_image = fields.Binary(string="Passport Copy")
    passport_file_type = fields.Char()
    is_ishaga_active = fields.Boolean(string="Is Ishanga")
    donation_origin = fields.Char(string="Donation Origin")
    contact_id_fkey = fields.Many2one('res.partner', 'Partner')
    is_tax_benefit = fields.Boolean(string="Is Tax Benefit")
    citizenship = fields.Selection([("indian", "Indian"), ("non_indian", "Non-Indian")])
    want_80g_tax = fields.Selection([("yes", "Yes"), ("no", "No")], string='Do you want 80G tax benefit?')
    no_80G_consent = fields.Selection([("yes", "Yes"), ("no", "No")], string='no_80G_consent')
    currency_id = fields.Many2one('res.currency')
    contribute_amount = fields.Float(string="Contribute Amount")
    converted_amount = fields.Float(string="Amount in INR")
    donation_date = fields.Date(string="On which Date the donation will happen for recurring")
    sso_id = fields.Char(string="User sso id")
    program_id = fields.Many2one("isha.program.schedule")
    timezonelist_id = fields.Many2one("isha.program.schedule.timezonelist")
    profile_info = fields.Text(string="Profile info")
    donation_type = fields.Selection([("onetime", "One-time"), ("recurring", "Recurring")])
    payment_transaction_ids = fields.One2many("ishanga.nu.payment.transaction", "donation_reg_id")
    isha_life_url = fields.Char(string="Isha life redirect URL")
    donate_url = fields.Char(string="Donate URL")
    center_id = fields.Many2one('isha.center', string='Center',related="contact_id_fkey.center_id",store=True)
    region_id = fields.Many2one('isha.region',string='Region', related='center_id.region_id', store=True)

    def update_usa_currency_conversation_amount(self):
        """ Calling API to update the USA current currecy conversion amount"""

        api_cur_url = "http://apilayer.net/api/live?access_key=7b8b41f24edbb085e579ce25944d6075&currencies=INR&source=USD&format=1"
        cur_response = requests.post(api_cur_url)
        text_vl = cur_response.text
        currency_conversion_arr = json.loads(text_vl)
        currency_convr_value = currency_conversion_arr['quotes']['USDINR']

        _logger.info("-currency_convr_value--")
        _logger.info(currency_convr_value)
        self.env['ir.config_parameter'].sudo().set_param('ishanga7.outreach_donation_usd_conversion_amount',currency_convr_value)
        return True

    def send_registration_succes_mail(self):
        if self:
            mail_template_id = self.env.ref('isha_ishanga_reg.mail_template_ishanga_donation_reg')
            try:
                self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(self.id,
                                                                                       force_send=True)
            except Exception as e:
                print(e)

    def write(self, vals):
        if vals.get('is_ishaga_active'):
            if vals.get('is_ishaga_active') == True:
                reg_rec_id = self.env['ishanga.nu.program.registration'].sudo().search([("donation_reg_id","=",self.id)],order="id desc",limit=1)
                if reg_rec_id:
                    reg_rec_id.write({"is_ishaga_active":True})
                mail_template_id = self.env.ref('isha_ishanga_reg.mail_template_ishanga_donation_reg')
                try:
                    self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(self.id,
                                                                                           force_send=True)
                except Exception as e:
                    print(e)
        return super(IshangaDonationRegistration, self).write(vals)

    def update_old_entry_ishanag(self):
        res = self.env['ishanga.nu.program.registration'].sudo().search([("donation_reg_id","!=",False)])
        for r in res:
            donation_rec = r.donation_reg_id
            r.write({
                "addressline": donation_rec.addressline,
                "city": donation_rec.city,
                "is_ishaga_active":donation_rec.is_ishaga_active,
                "phone": str(donation_rec.countrycode) + "" + str(donation_rec.mobile),
                "state": donation_rec.state,
                "contact_id_fkey":donation_rec.contact_id_fkey.id if donation_rec.contact_id_fkey else r.contact_id_fkey,
                "pincode": donation_rec.pincode,
                "nationality_id": donation_rec.nationality_id.id,
                "country_id": donation_rec.country.id,
                "country_code":donation_rec.country.code
            })

    def contact_cron(self, limit=500):
        recs = self.env['ishanga.donation.registration'].sudo().search([('contact_id_fkey', '=', False)], limit=limit)
        for rec in recs:
            try:
                null = None
                false = False
                true = True
                profile_info = eval(rec.profile_info)
                profile_info.update({
                    'phone': '+' + rec.countrycode + rec.mobile,
                    'street': rec.addressline,
                    'city': rec.city,
                    'state': rec.state,
                    'country_id': rec.country.id,
                    'country': rec.country.code,
                    'zip': rec.pincode,
                    'system_id': 56,
                    'local_contact_id': str(rec.id)
                })
                res_partner_rec = self.env['res.partner'].sudo().create(profile_info)
                if rec.is_ishaga_active:
                    res_partner_rec.category_id = [(4, self.env.ref('isha_crm.res_partner_category_20').id)]
                rec.contact_id_fkey = res_partner_rec.id
                _logger.info('Ishanga Contact Cron ' + str(rec.id) + ' ' + str(res_partner_rec.id))
                self.env.cr.commit()
            except Exception as ex:
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                _logger.error(tb_ex)



class IshangaReddgistration(models.Model):
    _name = 'ishanga.nu.program.registration'
    _description = 'Isha Ishanga Registration'
    _rec_name = 'program_id'

    donation_reg_id = fields.Many2one("ishanga.donation.registration")
    program_schedule_id = fields.Many2one("isha.program.schedule.timezonelist")
    program_id = fields.Many2one("isha.program.schedule")
    sso_id = fields.Char(string="User sso id")
    email = fields.Char(string="Email")
    first_name = fields.Char(string="First Name")
    last_name = fields.Char(string="Last Name")
    phone = fields.Char(string="Phone")
    profile_info = fields.Char(string="Profile Info")
    is_ishaga_active = fields.Boolean(string="Is Ishanga")
    contact_id_fkey = fields.Many2one('res.partner', 'Partner')
    country_code = fields.Char(string="Country code")
    nationality_id = fields.Many2one('res.country', 'Nationality')
    country_id = fields.Many2one('res.country', 'Country')
    addressline = fields.Char(string="Address")
    city = fields.Char(string='City', required=False)
    state = fields.Char(string='State', required=False)
    pincode = fields.Char(string='Pin Code', required=False)
    is_auth = fields.Boolean(string="Is Auth")
    is_buyed = fields.Boolean(string="Is Buyed")
    program_registration_status = fields.Selection(
        [("complete", "Complete"), ("already_have", "Already Had"), ("interest_to_buy", "Intereset to buy"),
         ("pending", "Pending"), ("initated", "Payment Initated to IshaLife"),
         ("failed", "Failed")])
    message = fields.Char(string="Info")
    isha_life_url = fields.Char(string="Isha life redirect URL")
    center_id = fields.Many2one('isha.center', string='Center', related="contact_id_fkey.center_id", store=True)
    region_id = fields.Many2one('isha.region', string='Region', related='center_id.region_id', store=True)

    def replaceEmptyString(self, src_msg):
        for x in src_msg:
            if src_msg[x] == '':
                src_msg[x] = None
            elif type(src_msg[x]) == str:
                src_msg[x] = src_msg[x].replace('\x00', '')  # Handle Byte String
                src_msg[x] = src_msg[x].strip()
        return src_msg

    def check_user_isishanga_overview(self, rec_id):
        """ Check the login user is Ishanga or not
        @return:is_vaild -True or False
        """
        is_exist = False
        if rec_id.profile_info:
            profile_info  =  eval(rec_id.profile_info)
            sso_id = rec_id.sso_id
            login_email = profile_info["basicProfile"]["email"]
            f_name = profile_info["basicProfile"]["firstName"] if profile_info["basicProfile"]["firstName"] else ""
            last_name = profile_info["basicProfile"]["lastName"] if profile_info["basicProfile"]["lastName"]  else ""
            partner_categ = self.env['res.partner.category'].sudo().search([("name", "=", "7% Ishanga")])
            matchEngine = NameMatchEngine.NameMatchEngine()
            name = str(f_name)+' '+str(last_name)
            partner_id = self.env['res.partner'].sudo().search(['|','|','|',("sso_id", "=", sso_id),
                                                                   ('email','=',login_email),
                                                                   ('email2','=',login_email),
                                                                   ('email3','=',login_email)])
            partner_id |= self.env['contact.map'].sudo().search([('email', '=', login_email)]).mapped('contact_id_fkey')
            msg = "Success"

            ishanga_partner_id =False
            partner_rec_exist = self.env['contact.map'].sudo().search([('sso_id', '=', sso_id)]).mapped('contact_id_fkey')
            if partner_id:
                for partner in partner_id:
                    if partner.sso_id == sso_id or matchEngine.compareNamesWithVariations(partner.name, name) in (EXACT_MATCH, HIGH_MATCH):
                        partner_rec_exist |= partner
            if not partner_rec_exist:
                rec_exist = self.env['ishanga.donation.registration'].sudo().search(
                    [("sso_id", "=", sso_id),
                     ("is_ishaga_active", "=", True)],limit=1)
                if rec_exist:
                    is_exist = True
                    ishanga_partner_id =  rec_exist.contact_id_fkey.id if rec_exist.contact_id_fkey else False

            if partner_rec_exist:
                rec_exist = [i.id for i in partner_rec_exist if partner_categ.id in i.category_id.ids]
                if not rec_exist:
                    rec_exist = self.env['ishanga.donation.registration'].sudo().search(
                        ['|', ("contact_id_fkey", "in", partner_rec_exist.ids), ("sso_id", "=", sso_id),
                         ("is_ishaga_active", "=", True)], limit=1).mapped('contact_id_fkey.id')
                if rec_exist:
                    is_exist = True
                    ishanga_partner_id = rec_exist[0]
            else:
                msg = "Invaid SSO ID"
            return {"valid":is_exist, "msg": msg,"partner_id":ishanga_partner_id}


    def update_old_rec(self):
        res = self.env['ishanga.nu.program.registration'].sudo().search([("donation_reg_id", "=",False)])
        for r in res:
            if r.profile_info:
                profile_details = eval(r.profile_info)
                print(profile_details['profileId'])
                partner_details = self.check_user_isishanga_overview(r)
                print(profile_details)
                print(profile_details['profileId'])
                country_rec = self.env['res.country'].sudo()
                update_val ={}
                if profile_details['basicProfile']['phone']['countryCode']:
                    phone = str(profile_details['basicProfile']['phone']['countryCode']) + '' + str(
                        profile_details['basicProfile']['phone']['number'])
                    update_val['phone']=phone
                if len(profile_details['addresses']) > 0:
                    address_dict = self.replaceEmptyString(profile_details['addresses'][0])
                    street = address_dict['addressLine1']
                    street2 = address_dict['addressLine2']
                    if address_dict['townVillageDistrict']:
                        if street2 and type(street2) == str:
                            street2 = street2 + ', ' + address_dict['townVillageDistrict']
                        else:
                            street2 = address_dict['townVillageDistrict']
                    city = address_dict['city']
                    state = address_dict['state']
                    country = address_dict['country']
                    zip = address_dict['pincode']
                    addressline = str(street) + "" + str(street2)
                    country_rec = self.env['res.country'].sudo().search([("code", "=", str(country))])
                if country_rec and len(profile_details['addresses']) > 0:
                    country = country_rec.id
                    update_val["addressline"]=addressline
                    update_val["city"]=city
                    update_val["state"]= state
                    update_val["pincode"]=zip
                    update_val["country_id"]=country
                    update_val["contact_id_fkey"]=partner_details.get("partner_id")
                    update_val["phone"]=phone
                else:
                    update_val['contact_id_fkey']=partner_details.get("partner_id")
                if len(update_val)>0:
                    r.write(update_val)


    def send_registration_complete_mail(self):
        """ Send succes mail to user aft the regitration"""
        if self:
            mail_template_id = self.env.ref('isha_ishanga_reg.mail_template_ishanga_program_reg')
            if self.country_code == "IN":
                mail_template_id = self.env.ref('isha_ishanga_reg.mail_template_ishanga_program_reg_in')
            try:
                self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(self.id, force_send=True)
            except Exception as e:
                print(e)

    def send_registration_confirm_in_mail(self):
        """ Send succes mail to user aft the regitration"""
        if self:
            # mail_template_id = self.env.ref('isha_ishanga_reg.mail_template_ishanga_program_reg')
            mail_template_id = self.env.ref('isha_ishanga_reg.mail_template_bof_in')
            try:
                self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(self.id, force_send=True)
            except Exception as e:
                print(e)

    def send_registration_succes_mail(self):
        """ Send succes mail to user aft the regitration"""
        if self:
            # mail_template_id = self.env.ref('isha_ishanga_reg.mail_template_ishanga_program_reg')
            mail_template_id = self.env.ref('isha_ishanga_reg.mail_template_bof_non_in')
            if self.country_code=="IN":
                mail_template_id = self.env.ref('isha_ishanga_reg.mail_template_bof_in')
            try:
                self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(self.id, force_send=True)
            except Exception as e:
                print(e)


class IshaNUPaymentTransaction(models.Model):
    _name = 'ishanga.nu.payment.transaction'
    _description = 'Isha payment transaction'
    _rec_name = 'donation_reg_id'

    model_id = fields.Many2one("ir.models")
    type = fields.Selection([("request", "Request to Outreach"), ("response", "Response from Outreach"),
                             ('kafka_response', 'KAFKA Response')])
    donation_type = fields.Selection([("onetime", "One-time"), ("recurring", "Recurring")])
    contribute_amount = fields.Float(string="Contribute Amount")
    converted_amount = fields.Float(string="Amount in INR")
    currency = fields.Char(string='currency')
    sso_id = fields.Char(string="User SSO Profile")
    session_id = fields.Char(string="User Session id")
    status = fields.Char(string="Status")
    status_code = fields.Char(string="Status Code")
    message= fields.Char(string="Details")
    transaction_id =fields.Char(string="transaction_id")
    tracking_id =fields.Char(string="tracking_id")
    registration_id = fields.Many2one("ishanga.nu.program.registration")
    donation_reg_id = fields.Many2one("ishanga.donation.registration")
    date = fields.Date()
    donation_currency = fields.Char(string="Donation Currency")
    donation_status = fields.Selection([("aborted", "Aborted"),
                                        ("timeout", "Timeout"),
                                        ("success", "Success"),
                                        ("awaited", "Awaited"),
                                        ("unsuccessful", "Unsuccessful"),
                                        ("active", "Active"),
                                        ("failure", "failure"),
                                        ('failed', 'failed'),
                                        ('notfound', 'Not Found'),
                                        ("refunded", "Refunded"),
                                        ("completed", "Completed"),
                                        ("initiated", "Initiated"),
                                        ("invalid", "InValid"),
                                        ("autocancelled", "autocancelled"),
                                        ("cancelled", "cancelled"),
                                        ("cancel_by_admin", " Cancel by Admin"),
                                        ("activation_failed", "Activation Failed"),
                                        ("pending_activation", "Pending Activation"),
                                        ("verificationPending", "Verification Pending")])




class Ishanga_Configuration(models.Model):
    _name = 'ishanga.configuration'
    _description = 'Isha Configuration'
    _rec_name ="region_id"

    region_id = fields.Many2one('isha.region', required=True, string='Region Name')
    region = fields.Char(string="Region Texts")
    country_ids = fields.Many2many('res.country',string='Country')
    value = fields.Text(string="Value", required=True)


