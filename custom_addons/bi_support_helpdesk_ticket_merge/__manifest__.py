# -*- coding: utf-8 -*-
# Part of Browseinfo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Helpdesk Support Ticket Merge in Odoo',
    'version': '13.0.0.1',
    'category' : 'Website',
    'author' : 'BrowseInfo',
    'summary': 'Support Ticket merge tickets of helpdesk support system Merge Helpdesk Support Tickets merge support Issues merge multiple tickets merging multiple tickets merger support ticket merger combine support ticket combine Customer Helpdesk Support Ticket merge',
    'description': '''
        Helpdesk Support Ticket Merge
       Merge Helpdesk Support Tickets and Issues
	   

- Allow support user to merge multiple tickets into one from list view of tickets.
- Allow them to merge ticket into one existing ticket by considering it as primary ticket.
- Allow them to create new ticket also after merging multiple tickets.
- Show Secondary tickets on primary ticket form.
- After merge all secondary tickets will be archive in system and will not appear in list but in future if you want to see you can still make them active again.
- For more details please see Video.
	   
	   
	    ''',
    'author': 'BrowseInfo',
    'website' : 'https://www.browseinfo.in',
    'price': 19,
    'currency': 'EUR',
    'depends': ['base','website',
                # 'website_sale',
                'bi_website_support_ticket'],
    'data': [
    'security/ir.model.access.csv',
    'views/merge_ticket_view.xml',
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'live_test_url':'https://youtu.be/tOE5pZC1N-c8',
    'images':['static/description/Banner.png'],
}
