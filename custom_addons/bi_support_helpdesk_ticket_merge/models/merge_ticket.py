# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date, datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError

class SupportTicketMerge(models.TransientModel):
    _name = 'merge.ticket'
    _rec_name = 'primary_ticket_id'

    responsible_user = fields.Many2one('res.users', string="Responsible User")
    support_team_id = fields.Many2one('support.team', string="Support Team", required=True)
    primary_ticket_id = fields.Many2one('support.ticket', string="Winner Ticket",domain=[('is_secondary_ticket', '=', False)])
    is_sure = fields.Boolean('Are You Sure ??',default=False)
    is_new_ticket = fields.Boolean('Create New Ticket ??',default=False)
    subject = fields.Char('New Ticket Subject',default='/')
    merge_reason = fields.Char('Merge Reason', required=False, default='/')
    ticket_line = fields.One2many('merge.support.ticket','merge_id')
    type_of_ticket_id = fields.Many2one('ticket.type',string='Ticket Type')
    partner_id = fields.Many2one('res.partner',string="Customer")

    @api.model
    def default_get(self, fields):
        context = dict(self._context or {})
        active_model = context.get('active_model')
        active_ids = context.get('active_ids')
        tickets = self.env[active_model].browse(active_ids)

        if len(tickets) <= 1:
            raise UserError(_("Select More than 1 ticket to merge Tickets!!!"))

        if any((ticket.stage_id.name == 'Closed') or (ticket.is_ticket_closed == True) for ticket in tickets):
            raise UserError(_("You can not merge Closed tickets"))

        if any(ticket.partner_id != tickets[0].partner_id for ticket in tickets):
            raise UserError(_("In order to merge multiple ticket at once, they must belong to the same commercial partner."))

        if any(ticket.type_of_ticket_id != tickets[0].type_of_ticket_id for ticket in tickets):
            raise UserError(_("In order to merge multiple ticket at once, Ticket type must be same."))


        if any((ticket.stage_id.name == 'Closed') for ticket in tickets):
            raise UserError(_("You can not merge Closed tickets"))

        rec = {}
        merge_list = []
        for ticket in tickets:
            merge_list.append((0,0,{
                'ticket_sequence' : ticket.sequence,
                'ticket_id' : ticket.id,
                }))
            rec.update({'ticket_line' : merge_list})
        rec.update({
            'responsible_user' : self.env['res.users'].search([],limit=1).id,
            'support_team_id' : tickets[0].support_team_id.id,
            'type_of_ticket_id' : tickets[0].type_of_ticket_id.id,
            'partner_id' : tickets[0].partner_id.id,
            })
        return rec

    @api.onchange('primary_ticket_id')
    def pouplate_ticket_values(self):
        for rec in self:
            if rec.primary_ticket_id:
                rec.responsible_user = rec.primary_ticket_id.user_id.id
                rec.support_team_id = rec.primary_ticket_id.support_team_id.id

    def merge_tickets(self):
        ticket_obj = self.env['support.ticket']
        description_list = []
        ticket_list = []
        for wizard in self:
            if wizard.is_new_ticket == True:
                for ticket in wizard.ticket_line:
                    description_list.append(ticket.ticket_id.name)

                vals = {
                'name' : wizard.subject,
                'description': "\n".join(description_list),
                'email_from': wizard.partner_id.email,
                'phone': wizard.partner_id.phone,
                'partner_id': wizard.partner_id.id,
                'type_of_ticket_id' : wizard.type_of_ticket_id.id,
                'date_create' : datetime.today().date(),
                'support_team_id' : wizard.ticket_line[0].ticket_id.support_team_id.id or False,
                'support_ticket_category' : wizard.ticket_line[0].ticket_id.support_ticket_category.id or False,
                'ticket_source' : wizard.ticket_line[0].ticket_id.ticket_source,
                'user_id' : wizard.ticket_line[0].ticket_id.user_id.id or False,
                'team_leader_id' : wizard.ticket_line[0].ticket_id.team_leader_id.id or False,
                }

                ticket_obj.create(vals)
                return {'type': 'ir.actions.act_window_close'}

            for ticket in wizard.ticket_line:
                ticket_list.append(ticket.ticket_id.id)

            if wizard.is_new_ticket != True:
                if wizard.is_sure == True:
                    for ticket in wizard.ticket_line:
                        if ticket.ticket_id.description:
                            description_list.append(ticket.ticket_id.description)

                    if wizard.primary_ticket_id:
                        if not wizard.primary_ticket_id.id in ticket_list:
                            raise UserError(_("Primary Ticket is not found in Ticket Lines!!!"))
                        for ticket in wizard.ticket_line:
                            if wizard.primary_ticket_id == ticket.ticket_id:
                                primary_ticket = ticket_obj.browse(ticket.ticket_id.id)
                                primary_ticket.write({'is_primary_ticket' : True,'description': "\n".join(description_list)})
                            if wizard.primary_ticket_id != ticket.ticket_id:
                                primary_ticket = ticket_obj.browse(ticket.ticket_id.id)
                                primary_ticket.write({'is_secondary_ticket' : True,'secondary_ticket_id':wizard.primary_ticket_id.id})
                else:
                    raise UserError(_("Please be sure you want to merge Tickets!!!"))

        return {'type': 'ir.actions.act_window_close'} 


class SupportTicketMergeLine(models.TransientModel):
    _name = 'merge.support.ticket'

    merge_id = fields.Many2one('merge.ticket', string="Merge Ticket")
    ticket_sequence = fields.Char(string='Support Ticket')
    ticket_id = fields.Many2one('support.ticket', string="Subject")


class TicketType(models.Model):
    _name = 'ticket.type'

    name = fields.Char('Ticket Type')

    _sql_constraints = [
        ('name_uniq', 'unique(name)', 'Ticket Type must be unique!'),
    ]


class TicketTypeSubject(models.Model):
    _name = 'ticket.type.subject'

    name = fields.Char('Ticket Type Subject')


class SupportTicketMergeInherit(models.Model):
    _inherit = 'support.ticket'

    @api.depends('is_primary_ticket')
    def _secondary_count(self):
        self.secondary_count = 0
        for ticket in self:
            if ticket.is_primary_ticket == True:
                tickets = self.env['support.ticket'].search([('is_secondary_ticket','=',True),('secondary_ticket_id','=',ticket.id)])
                ticket.update({'secondary_count' : len(tickets)})
        return True

    type_of_ticket_id = fields.Many2one('ticket.type','Type of Ticket')
    type_of_subject_id = fields.Many2one('ticket.type.subject','Type of Subject')
    is_primary_ticket = fields.Boolean('Primary Ticket',default=False)
    is_secondary_ticket = fields.Boolean('Secondary Ticket',default=False)
    secondary_ticket_id = fields.Many2one('support.ticket',default=False)
    secondary_count = fields.Integer('Secondary Count',compute="_secondary_count",default=0.0)

    def toggle_active(self):
        for ticket in self:
            upda_val = {'active': not ticket.active}
            if ticket.is_secondary_ticket == True:
                upda_val.update({
                    'is_secondary_ticket' : False,
                    'secondary_ticket_id' : False
                    })
            ticket.write(upda_val)


        return True

    def merge_ticket_view(self):
        self.ensure_one()
        views = [(self.env.ref('bi_website_support_ticket.support_ticket_tree_view').id, 'tree'), (self.env.ref('bi_website_support_ticket.support_ticket_form_view').id, 'form')]
        merge_list = [x.id for x in self.env['support.ticket'].search([('is_secondary_ticket','=',True),('secondary_ticket_id','=',self.id)])]

        return {
            'name': 'Ticket Merge View',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'res_model': 'support.ticket',
            'view_id': False,
            'views': views,
            'domain': [('id', 'in', merge_list)],
        }