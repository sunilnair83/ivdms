# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, tools
import logging, time, multiprocessing, traceback, math
from odoo.addons.isha_crm.models.mail_activity_override import prepend_content_to_html, mail_delivery_driver, update_mailing_daemon
from odoo.addons.isha_crm.models.email_marketing import dynamic_txn_email_mapper
_logger = logging.getLogger(__name__)

class MassMailing(models.Model):
    _inherit = 'mailing.mailing'

    use_in_marketing_automation_comm = fields.Boolean(
        string='Specific mailing used in marketing campaign', default=False,
        help='Marketing campaigns use mass mailings with some specific behavior; this field is used to indicate its statistics may be suspicious.')
    marketing_activity_ids = fields.One2many('marketing.activity', 'mass_mailing_id', string='Marketing Activities', copy=False)

    # TODO: remove in master
    def convert_links(self):
        """Override convert_links so we can add marketing automation campaign instead of mass mail campaign"""
        res = {}
        done = self.env['mailing.mailing']
        for mass_mailing in self:
            if self.env.context.get('default_marketing_activity_id'):
                activity = self.env['marketing.activity'].browse(self.env.context['default_marketing_activity_id'])
                vals = {
                    'mass_mailing_id': self.id,
                    'campaign_id': activity.campaign_id.utm_campaign_id.id,
                    'source_id': activity.utm_source_id.id,
                    'medium_id': self.medium_id.id,
                }
                res[mass_mailing.id] = self.env['link.tracker'].convert_links(
                    self.body_html or '',
                    vals,
                    blacklist=['/unsubscribe_from_list']
                )
                done |= mass_mailing
        res.update(super(MassMailing, self - done).convert_links())
        return res

    def _get_link_tracker_values(self):
        res = super(MassMailing, self)._get_link_tracker_values()
        if self.env.context.get('default_marketing_activity_id'):
            activity = self.env['marketing.activity'].browse(self.env.context['default_marketing_activity_id'])
            res['campaign_id'] = activity.campaign_id.utm_campaign_id.id
            res['source_id'] = activity.utm_source_id.id
        return res

    def automation_email_trigger(self, res_id, test_email, marketing_activity_id):
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        secret = self.env["ir.config_parameter"].sudo().get_param("database.secret")

        db_name = self.env.cr.dbname
        res_ids = res_id
        if len(res_ids) == 0:
            try:
                self.write({'state': 'done', 'sent_date': fields.Datetime.now()})
                self.env.cr.commit()
            except Exception as ex:
                time.sleep(2)
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                _logger.error(
                    "ERROR: EMAIL MARKETING write error\n" + str({'state': 'done', 'sent_date': fields.Datetime.now()})
                    + "\n" + tb_ex)

                td = multiprocessing.Process(target=update_mailing_daemon,
                                             args=(self.id, {'state': 'done', 'sent_date': fields.Datetime.now()}))
                td.start()
            return

        no_of_recs = len(res_ids) + self.expected if self.expected > 0 else len(res_ids)

        email_selection = self.dynamic_mailing_list.email_selection
        if self.is_pgm_upd and self.mailing_model_real not in ['res.partner', 'mailing.list']:
            email_selection = 'txn'

        if email_selection == 'contact':
            email_field = 'email'
        else:
            email_field = dynamic_txn_email_mapper[self.mailing_model_real]

        split_size = math.ceil(len(res_ids) / self.process_count) if len(
            res_ids) > self.process_count else self.process_count
        process_id = 0

        # body_html = update_body_with_trackable_urls(self, self.template_id.body_html, self.id)
        body_html = self.template_id.body_html
        pre_header_val = (
                '<div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">' +
                (self.pre_header or '') + '</div>')
        body_html_mod = prepend_content_to_html(body_html, pre_header_val, False, False, False)

        process_ids = []

        if test_email:
            for x in test_email.split(','):
                self.with_context(create_trace=True).ses_mail_delivery('Test mail', res_ids, self.mailing_model_real,
                                       self.id, body_html,
                                       self.email_from, self.reply_to, self.subject, base_url, db_name, secret,
                                       email_selection, email_field, x,
                                       'localhost' in base_url or self.load_testing, marketing_activity_id)
        else:
            for mail_batch in tools.split_every(split_size, res_ids):
                thread_name = 'Thread ' + str(process_id)
                td = multiprocessing.Process(target=mail_delivery_driver,
                                             args=(thread_name, mail_batch, self.mailing_model_real, self.id, body_html_mod,
                                                   self.email_from, self.reply_to, self.subject, base_url,
                                                   db_name, secret, email_selection, email_field, test_email,
                                                   'localhost' in base_url or self.load_testing, marketing_activity_id))
                td.start()
                process_ids.append(td.pid)
                time.sleep(1)
                process_id += 1

            # Serialization error happening becoz of the compute fields of mailing_mailing when someone opens the tree view
            # which is preventing the process ids to be written to db
            # Adding threaded retry as the scheduler cron rolls back the transaction even for serialization error
            try:
                self.write({'mailing_records_count': no_of_recs, 'state': 'sending',
                            'process_ids': process_ids, 'process_count': len(process_ids)})
                self.env.cr.commit()
            except Exception as ex:
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                _logger.error("ERROR: EMAIL MARKETING process_ids write error\n" + str(
                    {'id': self.id, 'process_ids': process_ids}) + "\n" + tb_ex)
                time.sleep(5)
                update_dict = {'mailing_records_count': no_of_recs, 'process_ids': process_ids, 'state': 'sending',
                               'process_count': len(process_ids)}
                td = multiprocessing.Process(target=update_mailing_daemon,
                                             args=(self.id, update_dict))
                td.start()