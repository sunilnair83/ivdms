from odoo import models,fields


class CallCampaignUserInput(models.Model):
    _inherit = "call_campaign.user_input"

    marketing_trace_id = fields.Integer(string='Trace id')
