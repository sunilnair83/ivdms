from odoo import api, fields, models, tools, _
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError
from odoo.tools.safe_eval import safe_eval

import logging
_logger = logging.getLogger(__name__)

class ResUsersOverride(models.Model):
    _inherit = 'res.users'

    support_ticket_category = fields.Many2many('support.ticket.category',string='Events')
    support_ticket_team_ids = fields.Many2many('support.team','res_user_helpdesk_rel','user_id','support_team_id',string="Teams")
    exotel_ivr_number = fields.Char(string='Registered IVR number',index=True)

    @api.onchange('support_ticket_team_ids')
    def check_event(self):
        for rec in self:
            for team in rec.support_ticket_team_ids:
                if team.support_ticket_category not in rec.support_ticket_category:
                    rec.support_ticket_category = [(4,team.support_ticket_category.id)]
