# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

import logging

from odoo import fields, models

_logger = logging.getLogger(__name__)
      
# class AccountAnalyticLine(models.Model):
#     _inherit = 'account.analytic.line'
#
#     ticket_id = fields.Many2one('support.ticket','Support Ticket')

# class ProjectTask(models.Model):
#     _inherit = 'project.task'
#
#     ticket_id = fields.Many2one('support.ticket',
#         string='Support Ticket',track_visibility='onchange',change_default=True)
#
# class ProjectTaskType(models.Model):
#     _inherit = 'project.task.type'
#
#     ticket_stage = fields.Boolean("Support Ticket Stage")

# class account_invoice(models.Model):
#     _inherit='account.move'
#
#     invoice_support_ticket_id  =  fields.Many2one('support.ticket', 'Support Ticket')

class ir_attachment(models.Model):
    _inherit='ir.attachment'

    support_ticket_id  =  fields.Many2one('support.ticket', 'Support Ticket')

class Website(models.Model):

    _inherit = "website"
    
    def get_website_config(self):
        config_ids = self.env["ir.config_parameter"].sudo().get_param('bi_website_support_ticket.support_ticket_visible') 
        return str(config_ids) 
    
    def get_support_team_list(self):            
        support_team_ids=self.env['support.team'].sudo().search([])
        return support_team_ids
        
    def get_ticket_details(self):            
        partner_brw = self.env['res.users'].browse(self._uid)
        ticket_ids = self.env['support.ticket'].search(['|',('partner_id','=',partner_brw.partner_id.id),('user_id','=',partner_brw.id)])
        return ticket_ids
        
    def get_ticket_type(self):            
        ticket_type_ids = self.env['support.ticket.type'].sudo().search([])
        return ticket_type_ids      

    def get_ticket_stage(self):
        ticket_stage_ids = self.env['support.stage'].sudo().search([('id','not in',[self.sudo().env.ref('bi_website_lock_ticket.support_stage5').id,self.sudo().env.ref('bi_website_lock_ticket.support_stage9').id])])
        return ticket_stage_ids

    def get_mail_templates(self):
        templates = self.env['mail.template'].sudo().search([('model','=','support.ticket'),('body_html','!=',False)])
        return templates

    def get_support_team_ivr(self):
        teams = self.env['support.team'].sudo().search([('is_ivr_visible','=',True)])
        return teams
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
