# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

import logging

from odoo import api, fields, models
from odoo.tools.safe_eval import safe_eval

_logger = logging.getLogger(__name__)

class SupportTeam(models.Model):

    _name = "support.team"   
    _inherit = ['mail.alias.mixin', 'mail.thread']
    _description = "Support Team"    
    
    name = fields.Char('Helpdesk Team', required=True, translate=True)    
    alias_id = fields.Many2one('mail.alias', string='Alias', required=True, help="The email address associated with this channel. New emails received will automatically create new leads assigned to the channel.")
    user_id = fields.Many2one('res.users',string="Assigned to")
    parent_team_ids = fields.Many2many('support.team','support_team_parent_team_rel','parent_team','support_team',string='Parent teams')
    team_member = fields.Many2many('res.users','res_user_helpdesk_rel','support_team_id','user_id',string="Team Member")
    level = fields.Selection([('s_level_2','Level 2'),('s_level_1','Level 1')])
    team_leader = fields.Many2one('res.users',string="Team Leader")
    support_ticket_category = fields.Many2one('support.ticket.category',string="Event" , default=lambda self: self.env.user.support_ticket_category[0] if len(self.env.user.support_ticket_category)>0 else False)
    is_ivr_visible = fields.Boolean(string='Visibility for Portal Users',index=True)

    def get_alias_values(self):
        values = super(SupportTeam, self).get_alias_values()
        
        values['alias_defaults'] = defaults = safe_eval(self.alias_defaults or "{}")
        defaults['support_team_id'] = self.id
        return values

    def get_alias_model_name(self, vals):
        return 'support.ticket'

    @api.model_create_multi
    def create(self, vals):
        rec = super(SupportTeam, self).create(vals)
        for team in rec:
            if team.team_leader and team.team_leader.id not in team.team_member.ids:
                team.team_member = [(4,team.team_leader.id)]
            if team.user_id and team.user_id.id not in team.user_id.ids:
                team.team_member = [(4,team.user_id.id)]
        return rec


    def write(self, vals):
        result = super(SupportTeam, self).write(vals)
        if 'alias_defaults' in vals:
            for team in self:
                team.alias_id.write(team.get_alias_values())
        for team in self:
            if team.team_leader and team.team_leader.id not in team.team_member.ids:
                team.team_member = [(4,team.team_leader.id)]
            if team.user_id and team.user_id.id not in team.team_member.ids:
                team.team_member = [(4,team.user_id.id)]

        return result
