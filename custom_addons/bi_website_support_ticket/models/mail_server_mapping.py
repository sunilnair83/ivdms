
from odoo import api, fields, models, tools, _
import logging
_logger = logging.getLogger(__name__)

class SupportCategory(models.Model):
    _name = "support.mail.mapping"
    _description = "Email server to Helpdesk mapping"

    email = fields.Char('Inbox Email id', required=True)
    reply_email = fields.Char('Reply Email id')
    category_id = fields.Many2one('support.ticket.category', string='Mapped Event')
    team_id = fields.Many2one('support.team', string='Mapped Team')
