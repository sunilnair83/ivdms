# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
import base64
import json
from datetime import datetime

from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager

import odoo.http as http
from odoo import _
from odoo.http import request
from odoo.osv.expression import OR


class SupportTicket(CustomerPortal):

    def _prepare_portal_layout_values(self):
        values = super(SupportTicket, self)._prepare_portal_layout_values()
        return values
        
    @http.route(['/my/ticket', '/my/ticket/page/<int:page>','/my/ticket/domain/<string:domain_val>'], type='http', auth="user", website=True)
    def portal_my_ticket(self, page=1, date_begin=None, date_end=None, sortby=None,search_in='all',search='', groupby='none',domain_val=False,**kw):
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        support_ticket = request.env['support.ticket']
        searchbar_sortings = {
            'create_date_asc': {'label': _('Created Date (Asc)'), 'order': 'date_create'},
            'create_date_desc': {'label': _('Created Date (Desc)'), 'order': 'date_create desc'},
            'last_chatter_update_asc': {'label': _(' Last Communication On (Asc)'), 'order': 'last_chatter_update'},
            'last_chatter_update_desc': {'label': _(' Last Communication On (Desc)'), 'order': 'last_chatter_update desc'},
            'write_date_asc': {'label': _('Last Ticket Updated On (Asc)'), 'order': 'write_date'},
            'write_date_desc': {'label': _('Last Ticket Updated On (Desc)'), 'order': 'write_date desc'},
            'user_id': {'label': _('Assignee'), 'order': 'user_id'},
        }
        searchbar_inputs = {
            'sequence': {'input': 'sequence', 'label': _('Search in Ticket No.')},
            'contact_name': {'input': 'contact_name', 'label': _('Search in Contact Name')},
            'email_from': {'input': 'email_from', 'label': _('Search in Email')},
            'phone': {'input': 'phone', 'label': _('Search in Phone')},
            'name': {'input': 'name', 'label': _('Search in Subject')},
            'stage_id': {'input': 'stage_id', 'label': _('Search in Stage')},
            'all': {'input': 'all', 'label': _('Search in All')},
        }
        searchbar_groupby = {
            'none': {'input': 'none', 'label': _('None')},
            'sequence': {'input': 'sequence', 'label': _('sequence')},
            'user_id': {'input': 'user_id', 'label': _('Assignee')},
            'stage_id': {'input': 'stage_id', 'label': _('Stage')},
            'support_team_id': {'input': 'support_team_id', 'label': _('Helpdesk Team')},
            'date_create': {'input': 'date_create', 'label': _('Created Date')},
            'category': {'input': 'category', 'label': _('Category')},
        }
        if not domain_val:
            domain = ['|','&','&',('support_ticket_category','in',request.env.user.support_ticket_category.ids+[request.env.ref('bi_website_support_ticket.common_event_type').id]),('support_team_id','in',request.env.user.support_ticket_team_ids.ids+[request.env.ref('bi_website_support_ticket.common_support_team').id]),('user_id','in',[request.env.user.id,False]),('user_id','=',request.env.user.id)]
        else:
            domain = eval(domain_val)
        # search
        if search and search_in:
            search_domain = []
            if search_in in ('sequence', 'all'):
                search_domain = OR([search_domain, [('sequence', 'ilike', search)]])
            if search_in in ('email_from', 'all'):
                search_domain = OR([search_domain, [('email_from', 'ilike', search)]])
            if search_in in ('phone', 'all'):
                search_domain = OR([search_domain, [('phone', 'ilike', search)]])
            if search_in in ('contact_name', 'all'):
                search_domain = OR([search_domain, [('contact_name', 'ilike', search)]])
            if search_in in ('name', 'all'):
                search_domain = OR([search_domain, [('name', 'ilike', search)]])
            if search_in in ('stage_id', 'all'):
                search_domain = OR([search_domain, [('stage_id', 'ilike', search)]])
            domain += search_domain

        # default sort by value
        if not sortby:
            sortby = 'create_date_desc'
        order = searchbar_sortings[sortby]['order']
        if groupby != 'none':
            archive_groups = request.env['support.ticket'].sudo().read_group(domain,[groupby],[groupby],orderby=order)
            archive_groups_limit = archive_groups[(page - 1) * self._items_per_page : (page - 1) * self._items_per_page+self._items_per_page]
            for x in archive_groups_limit:
                if x[groupby] and type(x[groupby]) != str and len(x[groupby]) > 1:
                    x['label'] = x[groupby][1]
                elif x[groupby]:
                    x['label'] = x[groupby]
                else:
                    x['label'] = 'UNDEFINED'

            ticket_count = len(archive_groups)
            supports = archive_groups_limit

        else:
            # count for pager
            ticket_count = support_ticket.search_count(domain)
            values.update({
                'ticket_count': ticket_count,
            })
            # search the count to display, according to the pager data
            partner = request.env.user.partner_id
            supports = support_ticket.search(domain, order=order, limit=self._items_per_page,
                                             offset=(page - 1) * self._items_per_page)
            request.session['my_ticket_history'] = supports.ids[:100]

        # make pager
        pager = portal_pager(
            url="/my/ticket",
            url_args={'date_begin': date_begin, 'date_end': date_end, 'sortby': sortby,'search_in': search_in, 'search': search,'groupby':groupby},
            total=ticket_count,
            page=page,
            step=self._items_per_page
        )

        values.update({
            'supports': supports,
            'page_name': 'ticket',
            'pager': pager,
            'searchbar_groupby':searchbar_groupby,
            'searchbar_sortings': searchbar_sortings,
            'searchbar_inputs': searchbar_inputs,
            'search_in': search_in,
            'sortby':sortby,
            'groupby':groupby,
            # 'archive_groups': archive_groups,
            'default_url': '/my/ticket',
        })
        if groupby != 'none':
            return request.render("bi_website_support_ticket.portal_my_ticket_group_by_view", values)
        else:
            return request.render("bi_website_support_ticket.portal_my_ticket", values)

    @http.route(['/support_ticket/<int:ticket_id>'], type="http", auth="user", website=True)
    def support_ticket_agent(self, ticket_id = False,**kw):
        if ticket_id:
            ticket = request.env['support.ticket'].sudo().browse(ticket_id)
            if request.httprequest.method == "GET":
                if 'close' in kw:
                    ticket.sudo().set_to_close()
                    return request.redirect('/ticket/view/detail/' + str(ticket_id))
                if 're_open' in kw:
                    ticket.sudo().set_to_reset()
                    return request.redirect('/ticket/view/detail/' + str(ticket_id))
                if 'assign_manager' in kw:
                    try:
                        ticket.sudo().assign_to_manager()
                    except:
                        pass
                    return request.redirect('/my/ticket')
                if 'escalate' in kw:
                    try:
                        ticket.sudo().change_level()
                    except:
                        pass
                    return request.redirect('/my/ticket')
                if 'assign_myself' in kw:
                    if ticket.user_id:
                        return request.redirect('/my/ticket')
                    if not ticket.user_id:
                        ticket.sudo().write({'user_id' : request.env.user.id})
                        return request.redirect('/ticket/view/detail/' + str(ticket_id))
                if 'unassign_myself' in kw:
                    if ticket.user_id.id == request.env.user.id:
                        ticket.sudo().write({'user_id' : None})
                    return request.redirect('/my/ticket')



            elif request.httprequest.method == "POST":
                name = kw['name']
                description = kw['description']
                email_from = kw['email_from']
                phone = kw['phone']
                contact_name = kw['contact_name']
                contact_feedback = kw['contact_feedback'] if 'contact_feedback' in kw else ticket.contact_feedback
                support_team_id = int(kw['support_team_id']) if kw['support_team_id'] else ticket.support_team_id.id
                category = int(kw['category']) if kw['category'] else ticket.category.id
                stage_id = int(kw['stage_id']) if kw.get('stage_id',None) else ticket.stage_id.id

                vals = {
                    'name': name,
                    'description': description,
                    'email_from': email_from,
                    'phone': phone,
                    'contact_name': contact_name or False,
                    'contact_feedback': contact_feedback or ticket.contact_feedback,
                    'support_team_id':support_team_id,
                    'category':category,
                    'stage_id':stage_id
                }
                if support_team_id and request.env['support.team'].sudo().browse(support_team_id).support_ticket_category != ticket.support_ticket_category:
                    vals['support_ticket_category'] = request.env['support.team'].sudo().browse(support_team_id).support_ticket_category.id
                ticket.write(vals)
                return request.redirect('/ticket/view/detail/'+str(ticket_id))

            values = {'description':ticket.description,
                      'contact_name':ticket.contact_name,
                      'name':ticket.name,
                      'id':ticket.id,
                      'user_ids': ticket.name,
                      'email': ticket.email_from,
                      'phone': ticket.phone,
                      'contact_feedback':ticket.contact_feedback,
                      'support_team':ticket.support_team_id,
                      'category':ticket.category,
                      'stage_id':ticket.stage_id}

            return request.render('bi_website_support_ticket.submit_support_ticket_agent', values)

        
    @http.route('/support_ticket', type="http", auth="public", website=True)
    def submit_support_ticket(self, **kw):
        """Let's public and registered user submit a support ticket"""
        name = ""
        if http.request.env.user.name != "Public user":
            name = http.request.env.user.name
        
        customer = http.request.env.user.partner_id.name
        email = http.request.env.user.partner_id.email
        phone = http.request.env.user.partner_id.phone
        values = {'partner_id' : customer,'user_ids': name,'email':email,'phone':phone}
        
        return http.request.render('bi_website_support_ticket.submit_support_ticket', values)
    
    @http.route('/support_ticket/thanks', type="http", auth="public", website=True)
    def support_ticket_thanks(self, **post):
        """Displays a thank you page after the user submits a support ticket"""
        if post.get('debug'):
            return request.render("bi_website_support_ticket.support_thank_you")


        partner_brw = request.env['res.users'].sudo().browse(request._uid)
        Attachments = request.env['ir.attachment']
        upload_file = post['upload']

        
        if post['support_team_id'] == '':
            support_team_obj = False
            user_id = False
            team_leader_id = False
        else:
            support_team_obj = request.env['support.team'].sudo().browse(int(post['support_team_id']))
            user_id = support_team_obj.user_id.id
            team_leader_id = support_team_obj.user_id.id
            
        name = post['name']
        description = post['description']
        email_from = post['email_from']
        phone = post['phone']
        category = post['ticket_id']
        priority = post['priority']
        date_create = datetime.now()
        support_team_id = post['support_team_id']
        
        vals = {
                'name':name,
                'description':description,
                'email_from': email_from,
                'phone': phone,
                'category': category,
                'priority' : priority,
                'partner_id': partner_brw.partner_id.id,
                'date_create' : date_create,
                'support_team_id' : support_team_id or False,
                'user_id' : user_id or False,
                'team_leader_id' : team_leader_id or False,
                }

        support_ticket_obj = request.env['support.ticket'].sudo().create(vals)
        if upload_file:
            attachment_id = Attachments.sudo().create({
                'name': upload_file.filename,
                'type': 'binary',
                'datas': base64.encodestring(upload_file.read()),
                'name': upload_file.filename,
                'public': True,
                'res_model': 'ir.ui.view',
                'res_id': False,
                'support_ticket_id' : support_ticket_obj.id,
            }) 

        helpdesk_manager_id = request.env['ir.model.data'].sudo().\
        get_object_reference('bi_website_support_ticket','group_support_manager')[1]
        group_manager = request.env['res.groups'].sudo().browse(helpdesk_manager_id)
        if group_manager.users:
            for group_manager in group_manager.users:
                template_id = request.env['ir.model.data'].sudo().get_object_reference(
                                                      'bi_website_support_ticket',
                                                      'email_template_support_ticket')[1]
                email_template_obj = request.env['mail.template'].sudo().browse(template_id)
                if template_id:
                    values = email_template_obj.generate_email(support_ticket_obj.id, fields=None)
                    values['email_from'] = group_manager.partner_id.email
                    if email_from:
                        values['email_to'] = email_from
                    else:
                        values['email_to'] = request.env.user.email
                    values['author_id'] = group_manager.partner_id.id
                    mail_mail_obj = request.env['mail.mail']
                    msg_id = mail_mail_obj.sudo().create(values)
                    if upload_file:
                        msg_id.attachment_ids=[(6,0,[attachment_id.id])]
                    if msg_id:
                        mail_mail_obj.send([msg_id])
                        
        return request.render("bi_website_support_ticket.support_thank_you")
    
    @http.route('/ticket/view', type="http", auth="user", website=True)
    def ticket_view_list(self, **kw):
        """Displays a list of ticket owned by the logged in user"""
        return http.request.render('bi_website_support_ticket.ticket_view_list')
    
    @http.route(['/ticket/view/detail/<model("support.ticket"):ticket>'],type='http',auth="public",website=True)
    def support_ticket_view(self, ticket, category='', search='', **kwargs):
        
        context = dict(request.env.context or {})
        ticket_obj = request.env['support.ticket']
        context.update(active_id=ticket.id)
        
        ticket_data_list = []
        ticket_data = ticket_obj.sudo().browse(int(ticket))
        
        for items in ticket_data:
            ticket_data_list.append(items)
            
        return http.request.render('bi_website_support_ticket.support_ticket_view',{
            'ticket_data_list': ticket
        }) 

    @http.route(['/ticket/message'],type='http',auth="public",website=True)
    def ticket_message(self, **post):
        
        Attachments = request.env['ir.attachment']
        upload_file = post['upload']
        
        if ',' in post.get('ticket_id'):
            bcd = post.get('ticket_id').split(',')
        else : 
            bcd = [post.get('ticket_id')]
            
        support_obj = request.env['support.ticket'].sudo().search([('id','=',bcd)])            
            
        if upload_file:
            attachment_id = Attachments.sudo().create({
                'name': upload_file.filename,
                'type': 'binary',
                'datas': base64.encodestring(upload_file.read()),
                'datas_fname': upload_file.filename,
                'public': True,
                'res_model': 'ir.ui.view',
                'support_ticket_id' : support_obj.id,
            }) 
        
        context = dict(request.env.context or {})
        ticket_obj = request.env['support.ticket']
        if post.get( 'message' ):
            message_id1 = support_obj.message_post() 
                
            message_id1.body = post.get( 'message' )
            message_id1.type = 'comment'
            message_id1.subtype = 'mt_comment'
            message_id1.model = 'support.ticket'
            message_id1.res_id = post.get( 'ticket_id' )
                    
        return http.request.render('bi_website_support_ticket.support_message_thank_you') 
        
    @http.route(['/ticket/comment/<model("support.ticket"):ticket>'],type='http',auth="public",website=True)
    def ticket_comment_page(self, ticket,**post):  
        
        return http.request.render('bi_website_support_ticket.support_ticket_comment',{'ticket': ticket}) 
     
    @http.route(['/support_ticket/comment/send'],type='http',auth="public",website=True)
    def ticket_comment(self, **post):
        
        context = dict(request.env.context or {})
        if post.get('ticket_id'):
            ticket_obj = request.env['support.ticket'].sudo().browse(int(post['ticket_id']))
            ticket_obj.update({
                    'customer_rating' : post['customer_rating'],            
                    'comment' : post['comment'],
            })
        return http.request.render('bi_website_support_ticket.support_rating_thank_you')         


    @http.route(['/suport_ticket/render'], auth="user", website=True)
    def support_ticket_render_template(self, **kw):
        templates = request.env['mail.template'].sudo().browse(int(kw['category']))
        value = templates.generate_email(int(kw['ticket_id']))
        return json.dumps(value)


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
