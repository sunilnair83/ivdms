odoo.define('bi_website_support_ticket.test', ['web.ajax'], function (require) {
    "use strict";

    var ajax = require('web.ajax');
	console.log('ajax loaded');
    // some code here
        // Parameters for form submission
    $('.o_wprofile_editor_form').ajaxForm({
        url: '/suport_ticket/render',
        type: 'POST',                       // submission type
        dataType: 'json',
        success: function(response, status, xhr, wfe){
        try{
			var d = response.body_html;
			var node_e = new DOMParser().parseFromString(d, 'text/html').body.childNodes[0];
			var item = document.getElementsByClassName('note-editable')[0];
			item.replaceChild(node_e, item.childNodes[0]);
        }catch(err){
			var d = '<p>Template is empty or not supported</p>';
			var node_e = new DOMParser().parseFromString(d, 'text/html').body.childNodes[0];
			var item = document.getElementsByClassName('note-editable')[0];
			item.replaceChild(node_e, item.childNodes[0]);
        }
        },
        error:function(jqXHR, textStatus, errorThrown){ // failure of AJAX request
            console.log('error');
			var d = '<p>Template is empty or not supported</p>';
			var node_e = new DOMParser().parseFromString(d, 'text/html').body.childNodes[0];
			var item = document.getElementsByClassName('note-editable')[0];
			item.replaceChild(node_e, item.childNodes[0]);
        }});


    return;
});
