odoo.define('bi_website_support_ticket.modify_chatter', function (require) {
"use strict";
var core = require('web.core');
var Widget = require('web.Widget');
var config = require('web.config');
var _t = core._t;
var QWeb = core.qweb;
var BaseChatter = require('mail.Chatter');

var ChatterModified = BaseChatter.include({
    _renderButtons: function () {
//    	console.log(this);
    	var show_log_note = true;
    	// hide log note on support ticket
    	if (this.record.model == 'support.ticket')
    		show_log_note = false;
        return QWeb.render('mail.chatter.Buttons', {
            newMessageButton: !!this.fields.thread,
            logNoteButton: show_log_note,//this.hasLogButton,
            scheduleActivityButton: !!this.fields.activity,
            isMobile: config.device.isMobile,
        });
    },

});
});

odoo.define('bi_website_support_ticket.mail_composer', function (require) {
"use strict";
var BaseComposer = require('mail.composer.Chatter');
var mailUtils = require('mail.utils');
var session = require('web.session');

var core = require('web.core');
var viewDialogs = require('web.view_dialogs');

var _t = core._t;
var ComposerModified = BaseComposer.include({

    _checkSuggestedPartners: function (checkedSuggestedPartners) {
        var self = this;
        var checkDone = new Promise(function (resolve, reject) {
            var recipients = _.filter(checkedSuggestedPartners, function (recipient) {
                return recipient.checked;
            });
            var recipientsToFind = _.filter(recipients, function (recipient) {
                return (! recipient.partner_id);
            });
            var namesToFind = _.pluck(recipientsToFind, 'full_name');
            var recipientsToCheck = _.filter(recipients, function (recipient) {
                return (recipient.partner_id && ! recipient.email_address);
            });
            var recipientIDs = _.pluck(_.filter(recipients, function (recipient) {
                return recipient.partner_id && recipient.email_address;
            }), 'partner_id');

            var namesToRemove = [];
            var recipientIDsToRemove = [];

            // have unknown names
            //   -> call message_get_partner_info_from_emails to try to find
            //      partner_id
            var def;
            if (namesToFind.length > 0) {
                def = self._rpc({
                    route: '/mail/get_partner_info',
                    params: {
                        model: self._model,
                        res_ids: [self.context.default_res_id],
                        emails: namesToFind,
                    },
                });
            }

            // for unknown names + incomplete partners
            //   -> open popup - cancel = remove from recipients
            Promise.resolve(def).then(function (result) {
                result = result || [];
                var emailDefs = [];
                var recipientPopups = result.concat(recipientsToCheck);

                _.each(recipientPopups, function (partnerInfo) {
                    var prom = new Promise(function(innerResolve, innerReject) {
						// do not bring the popup for creating the contact
//						console.log(self);
						if (self._model == 'support.ticket'){
							innerResolve();
						}else{
							var partnerName = partnerInfo.full_name;
							var partnerID = partnerInfo.partner_id;
							var parsedEmail = mailUtils.parseEmail(partnerName);

							var dialog = new viewDialogs.FormViewDialog(self, {
								res_model: 'res.partner',
								res_id: partnerID,
								context: {
									active_model: self._model,
									active_id: self.context.default_res_id,
									force_email: true,
									ref: 'compound_context',
									default_name: parsedEmail[0],
									default_email: parsedEmail[1],
								},
								title: _t("Please complete customer's informations"),
								disable_multiple_selection: true,
							}).open();
							dialog.on('closed', self, function () {
								innerResolve();
							});
							dialog.opened().then(function () {
								dialog.form_view.on('on_button_cancel', self, function () {
									namesToRemove.push(partnerName);
									if (partnerID) {
										recipientIDsToRemove.push(partnerID);
									}
								});
							});

						}
					});
                    emailDefs.push(prom);
                });
                Promise.all(emailDefs).then(function () {
                    var newNamesToFind = _.difference(namesToFind, namesToRemove);
                    var def;
                    if (newNamesToFind.length > 0) {
                        def = self._rpc({
                            route: '/mail/get_partner_info',
                            params: {
                                model: self._model,
                                res_ids: [self.context.default_res_id],
                                emails: namesToFind,
                                link_mail: true,
                            },
                        });
                    }
                    Promise.resolve(def).then(function (result) {
                        result = result || [];
                        var recipientPopups = result.concat(recipientsToCheck);
                        _.each(recipientPopups, function (partnerInfo) {
                            if (
                                partnerInfo.partner_id &&
                                _.indexOf(partnerInfo.partner_id, recipientIDsToRemove) === -1
                            ) {
                                recipientIDs.push(partnerInfo.partner_id);
                            }
                        });
                    }).then(function () {
                        resolve(recipientIDs);
                    });
                });
            });
        });

        return checkDone;
    },

});


});
