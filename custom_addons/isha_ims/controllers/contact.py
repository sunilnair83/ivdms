import datetime
import logging
from datetime import timedelta

from odoo.addons.muk_rest import tools

import odoo
from odoo import fields
from odoo.http import request

_logger = logging.getLogger(__name__)


class ContactController(odoo.http.Controller):

    @odoo.http.route([
        '/api/contact/modifiedon'
    ], auth="none", type='json', methods=['POST'], csrf = False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def get_contacts_modified_on(self, **kw):
        _logger.info('get_contacts_modified_on - start')
        date_in = request.params.get('date')
        cur_date_obj = fields.Date.from_string(date_in)
        prev_date_obj = cur_date_obj - timedelta(days=1)
        prev_date_str = fields.Date.to_string(prev_date_obj)
        timestamp_from = prev_date_str + ' 18:29:59'
        cur_date_str = fields.Date.to_string(cur_date_obj)
        timestamp_to = cur_date_str + ' 18:30:00'
        _logger.info('get_contacts_modified_on - before search query')
        contact_results = request.env['res.partner'].search([('write_date', '>', timestamp_from),
                                                             ('write_date', '<=', timestamp_to), ('starmark_category_id', '!=', False)])
        _logger.info('get_contacts_modified_on - after search query')
        ids = []
        # if contact_results:
        #     for result in contact_results:
        #         ids.append(result.id)
        #
        # if len(ids) > 0:
        #     contact_ids = request.env['res.partner'].search([('id', 'in', ids)])

        # contacts = contact_results.read()
        result = []
        # contacts = [contact for contact in contact_results if contact.read()]
        # contacts = contact_results.read()
        if contact_results:
            for contact in contact_results:
                if filter_incident_stage(contact):
                    result.append(_get_starmark_info(contact))
        _logger.info('get_contacts_modified_on - end')
        return result


    @odoo.http.route([
        '/api/contact/search'
    ], auth="none", type='json', methods=['POST'], csrf = False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def search_contacts(self, **kw):
        name = request.params.get('name')
        phone = request.params.get('phone')
        email = request.params.get('email')
        lenient_search = request.params.get('lenient_search', False)
        relaxed_search = request.params.get('relaxed_search', False)
        if relaxed_search:
            domain = []
            if email and phone:
                domain = ['|', '|', '|', '|', '|', '|',
                    ('phone', '=', phone), ('phone2', '=', phone), ('phone3', '=', phone), ('phone4', '=', phone),
                    ('email', '=', email), ('email2', '=', email), ('email3', '=', email)]
            elif email:
                email_domain = ['|', '|', ('email', '=', email), ('email2', '=', email), ('email3', '=', email)]
                domain.extend(email_domain)
            elif phone:
                phone_domain = ['|', '|', '|', ('phone', '=', phone), ('phone2', '=', phone), ('phone3', '=', phone), ('phone4', '=', phone)]
                domain.extend(phone_domain)
            if len(domain) > 0:
                contact_results = request.env['res.partner'].search(domain)
            else:
                contact_results = request.env['res.partner']
        else:
            contact_results = request.env['res.partner'].getHighMatch(name, phone, email, lenient_search)
        # ids = []
        final_list = []
        for contact in contact_results:
            if len(contact.starmark_category_id)>0 and filter_incident_stage(contact):
                final_list.append(_get_starmark_info(contact))

        _logger.info("Starmark api - " + str(request.params)+" search result " + str(contact_results)+" final result " + str(final_list))
        # if contact_results:
        #     for result in contact_results:
        #         ids.append(result.id)
        # contact_ids = request.env['res.partner'].search([('id', 'in', ids)])

        # contacts = contact_ids.read()
        # result = []
        # if contacts:
        #     for contact in contacts:
        #         result.append(_get_starmark_info(contact))
        return final_list


    @odoo.http.route([
        '/api/contact/get'
    ], auth="none", type='json', methods=['POST'], csrf = False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def get_contact(self, **kw):
        cdi_ids = request.params.get('cdi_ids')
        contact_ids = request.env['res.partner'].search([('guid', 'in', cdi_ids), ('starmark_category_id', '!=', False)])
        # contacts = contact_ids.read()
        results = []
        if contact_ids and len(contact_ids) > 0:
            for contact in contact_ids:
                if filter_incident_stage(contact):
                    results.append(_get_starmark_info(contact))
        return results

def _get_dna_info(category, dna_ids, comments):
    values = []
    for dna_id in dna_ids:
        dna_info = request.env['incident.donotallows'].search([('id', '=', dna_id)])
        values.append(dna_info.name)
    return {'categoryName': category,
            'values': values,
            'remarks': comments}


def _get_starmark_info(contact_rec):
    contact = contact_rec.read(['starmark_category_id', 'donotallow_stayarea_ids', 'dna_stayarea_comments',
                                'donotallow_program_ids', 'dna_program_comments', 'donotallow_volunteering_ids',
                                'dna_volunteering_comments', 'donotallow_general_ids', 'dna_general_comments',
                                'partner_severity', 'incident_date_closed', 'id', 'name', 'phone', 'email', 'email2',
                                'email3', 'guid'])[0]
    _logger.info('_get_starmark_info - start')
    dnaInfo = []
    isStarMarked = False
    color = None
    if 'starmark_category_id' in contact and contact['starmark_category_id']:
        isStarMarked = True
        color = contact['starmark_category_id'][1]
    if 'donotallow_stayarea_ids' in contact and contact['donotallow_stayarea_ids']:
        dnaInfo.append(
            _get_dna_info("Do Not Allow Stay", contact['donotallow_stayarea_ids'], contact['dna_stayarea_comments']))
    if 'donotallow_program_ids' in contact and contact['donotallow_program_ids']:
        dnaInfo.append(
            _get_dna_info("Do Not Allow Programs", contact['donotallow_program_ids'], contact['dna_program_comments']))
    if 'donotallow_volunteering_ids' in contact and contact['donotallow_volunteering_ids']:
        dnaInfo.append(_get_dna_info("Do Not Allow Volunteering", contact['donotallow_volunteering_ids'],
                                     contact['dna_volunteering_comments']))
    if 'donotallow_general_ids' in contact and contact['donotallow_general_ids']:
        dnaInfo.append(
            _get_dna_info("Do Not Allow General", contact['donotallow_general_ids'], contact['dna_general_comments']))

    severity = None
    if 'partner_severity' in contact and contact['partner_severity']:
        severity_map = {
            '1': 'LOW',
            '2': 'MEDIUM',
            '3': 'HIGH',
            '4': 'SEVERE'
        }
        severity = severity_map[contact['partner_severity']]

    star_mark_expiry_millis = None
    if 'incident_date_closed' in contact and contact['incident_date_closed']:
        epoch = datetime.datetime.utcfromtimestamp(0)
        star_mark_expiry_millis = (contact['incident_date_closed'] - epoch).total_seconds() * 1000.0

    incident_type_partner = [ x for x in contact_rec.mapped('approved_incident_by_partner_ids.incident_types.name')]
    incident_type_author = [ x for x in contact_rec.mapped('approved_incident_by_author_ids.incident_types.name')]
    _logger.info('_get_starmark_info - end')
    return {
        'id': contact['id'],
        'name': contact['name'] if contact['name'] else None,
        'phone': contact['phone'] if contact['phone'] else None,
        'email': contact['email'] if contact['email'] else None,
        'email2': contact['email2'] if contact['email2'] else None,
        'email3': contact['email3'] if contact['email3'] else None,
        'guid': contact['guid'],
        'isStarMarked': isStarMarked,
        'color': color,
        'severity': severity,
        'starMarkReleaseDate': star_mark_expiry_millis,
        'dnaInfo': dnaInfo,
        'incidentTypes':incident_type_partner + incident_type_author
    }

def filter_incident_stage(partner):
    valid_stage_type = [ request.env.ref('isha_ims.incident_stage_type_approved').id,
                         request.env.ref('isha_ims.incident_stage_type_freeze').id,
                         request.env.ref('isha_ims.incident_stage_type_unfreeze').id
                         ]
    valid_incidents = partner.approved_incident_by_partner_ids.filtered(lambda x: x.stage_id.type_id.id  in valid_stage_type) \
                      | partner.approved_incident_by_author_ids.filtered(lambda x: x.stage_id.type_id.id  in valid_stage_type)
    if len(valid_incidents) > 0:
        return True
    else:
        return False