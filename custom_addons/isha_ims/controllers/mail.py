import logging
from odoo import http
from odoo.addons.mail.controllers.main import MailController

_logger = logging.getLogger(__name__)


class IncidentMailController(MailController):

    # Handle mail view links. We do not use standard Odoo url, because we
    # show incidents to authorized users only
    @http.route("/mail/view/incident/<int:incident_id>",
                type='http', auth='user', website=True)
    def mail_action_view_incident(self, incident_id, **kwargs):
        return super(
            IncidentMailController, self
        ).mail_action_view(
            model='incident.incident', res_id=incident_id, **kwargs)
