from odoo.exceptions import (AccessError,
                             UserError)
from .common import IncidentCase


class TestIncidentSimpleFlow(IncidentCase):
    """Test incident Simple Flow
    """

    def test_090_simple_flow_assign_self(self):
        self.assertEqual(self.incident_1.stage_id, self.stage_draft)

        # Make incident sent
        self.incident_1.write({'stage_id': self.stage_sent.id})
        self.assertEqual(self.incident_1.stage_id, self.stage_sent)
        self.assertFalse(self.incident_1.date_assigned)
        self.assertFalse(self.incident_1.user_id)

        # Assign incident to incident manager
        manager = self.incident_manager

        # If no user specified for wizard, current user will be automaticaly
        # selected
        AssignWizard = self.env['incident.wizard.assign']
        assign_wizard = AssignWizard.with_user(manager).create({
            'incident_id': self.incident_1.id,
        })

        assign_wizard.do_assign()
        self.assertEqual(self.incident_1.user_id, manager)
        self.assertTrue(self.incident_1.date_assigned)

        # Undo assign incident
        manager = self.incident_manager
        self.incident_1.with_user(manager).write({'user_id': False})
        self.assertFalse(self.incident_1.user_id)
        self.assertFalse(self.incident_1.date_assigned)

    def test_100_simple_flow_su_confirm(self):

        self.assertEqual(self.incident_1.stage_id, self.stage_draft)
        self.assertFalse(self.incident_1.can_be_closed)

        # Make incident sent
        self.incident_1.write({'stage_id': self.stage_sent.id})
        self.assertEqual(self.incident_1.stage_id, self.stage_sent)
        self.assertTrue(self.incident_1.can_be_closed)

        # Make incident confirmed
        self._close_incident(self.incident_1, self.stage_confirmed)
        self.assertFalse(self.incident_1.can_be_closed)

    def test_110_simple_flow_su_reject(self):

        self.assertEqual(self.incident_1.stage_id, self.stage_draft)

        # Make incident sent
        self.incident_1.write({'stage_id': self.stage_sent.id})
        self.assertEqual(self.incident_1.stage_id, self.stage_sent)

        # Make incident rejected
        with self.assertRaises(UserError):
            # No response text provided, but it is required
            self._close_incident(self.incident_1, self.stage_rejected)

        self._close_incident(
            self.incident_1, self.stage_rejected,
            response_text='<p>Rejected!</p>')

    def test_120_simple_flow_access_confirm_access_error(self):
        user = self.incident_user

        self.assertEqual(self.incident_1.stage_id, self.stage_draft)

        # Make incident sent
        self.incident_1.with_user(user).write({'stage_id': self.stage_sent.id})
        self.assertEqual(self.incident_1.stage_id, self.stage_sent)

        # Make incident confirmed
        with self.assertRaises(AccessError):
            self._close_incident(
                self.incident_1, self.stage_confirmed, user=user)

    def test_130_simple_flow_access_confirm_access_ok(self):
        user = self.incident_user
        manager = self.incident_manager

        self.assertEqual(self.incident_1.stage_id, self.stage_draft)

        # Make incident sent
        self.incident_1.with_user(user).write({'stage_id': self.stage_sent.id})
        self.assertEqual(self.incident_1.stage_id, self.stage_sent)
        self.assertFalse(self.incident_1.date_closed)
        self.assertFalse(self.incident_1.closed_by_id)

        # Make incident confirmed (manager belongs to group that allowed to
        # confirm incidents)
        self._close_incident(self.incident_1, self.stage_confirmed, user=manager)
        self.assertTrue(self.incident_1.date_closed)
        self.assertEqual(self.incident_1.closed_by_id, manager)

    def test_140_simple_flow_access_reject_access_error_user_1(self):
        user = self.incident_user

        self.assertEqual(self.incident_1.stage_id, self.stage_draft)

        # Make incident sent
        self.incident_1.with_user(user).write({'stage_id': self.stage_sent.id})
        self.assertEqual(self.incident_1.stage_id, self.stage_sent)

        # Make incident confirmed
        # (User cannot reject incident)
        with self.assertRaises(AccessError):
            self.incident_1.with_user(user).write(
                {'stage_id': self.stage_rejected.id})

    def test_145_simple_flow_access_reject_access_error_user_2(self):
        user = self.incident_user
        manager = self.incident_manager

        self.assertEqual(self.incident_1.stage_id, self.stage_draft)

        # Make incident sent
        self.incident_1.with_user(user).write({'stage_id': self.stage_sent.id})
        self.assertEqual(self.incident_1.stage_id, self.stage_sent)

        # Make incident reject
        # (Manager alson cannot reject incident)
        with self.assertRaises(AccessError):
            self.incident_1.with_user(manager).write(
                {'stage_id': self.stage_rejected.id})

    def test_150_simple_flow_access_reject_access_ok(self):
        user = self.incident_user
        manager_2 = self.incident_manager_2

        self.assertEqual(self.incident_1.stage_id, self.stage_draft)

        # Make incident sent
        self.incident_1.with_user(user).write({'stage_id': self.stage_sent.id})
        self.assertEqual(self.incident_1.stage_id, self.stage_sent)
        self.assertFalse(self.incident_1.date_closed)
        self.assertFalse(self.incident_1.closed_by_id)

        # Make incident rejected
        # Manager 2 can do it, because it is in list of allowed users for
        # this route
        self.incident_1.with_user(manager_2).write(
            {'stage_id': self.stage_rejected.id})
        self.assertEqual(self.incident_1.stage_id, self.stage_rejected)
        self.assertTrue(self.incident_1.date_closed)
        self.assertEqual(self.incident_1.closed_by_id, manager_2)

    def test_155_simple_flow_circle(self):
        # draft -> sent -> rejected -> draft
        user = self.incident_user
        manager_2 = self.incident_manager_2

        self.assertEqual(self.incident_1.stage_id, self.stage_draft)

        # Make incident sent
        self.incident_1.with_user(user).write({'stage_id': self.stage_sent.id})
        self.assertEqual(self.incident_1.stage_id, self.stage_sent)
        self.assertFalse(self.incident_1.date_closed)
        self.assertFalse(self.incident_1.closed_by_id)

        # Make incident rejected
        self.incident_1.with_user(manager_2).write(
            {'stage_id': self.stage_rejected.id})
        self.assertEqual(self.incident_1.stage_id, self.stage_rejected)
        self.assertTrue(self.incident_1.date_closed)
        self.assertEqual(self.incident_1.closed_by_id, manager_2)

        # Go to draft state again
        self.incident_1.with_user(user).write({'stage_id': self.stage_draft.id})
        self.assertEqual(self.incident_1.stage_id, self.stage_draft)
        self.assertFalse(self.incident_1.closed)

    def test_160_simple_flow_assign_closed(self):
        # Closed incidents cannot be reassigned.
        self.incident_1.stage_id = self.stage_sent.id
        self.assertEqual(self.incident_1.stage_id, self.stage_sent)

        # Make incident confirmed (manager belongs to group that allowed to
        # confirm incidents)
        self._close_incident(self.incident_1, self.stage_confirmed)
        self.assertTrue(self.incident_1.date_closed)

        # Assign incident to manager
        with self.assertRaises(UserError):
            self.incident_1.action_incident_assign()
