from odoo import exceptions
from odoo.tests.common import TransactionCase, tagged


@tagged('-standard', 'my_custom_tag')
class ImsIncident(TransactionCase):

    def setUp(self, *args, **kwargs):
        result = super().setUp(*args, **kwargs)

        groups = [self.ref('isha_ims.group_ims_reporter'), self.ref('base.group_partner_manager')]
        self.basic_user = self.env['res.users'].create({
            'login': 'test_ims_basic_reporter@yourcompany.example.com',
            'partner_id': self.env['res.partner'].create({
                'name': 'test ims basic reporter',
                'email': 'test_ims_basic_reporter@yourcompany.example.com'
            }).id,
            'groups_id': [
                (6, 0, groups)
            ]
        })

        self.env= self.env(user=self.basic_user)
        return result

    def test_default_incident_text(self):

        # create an incident as a basic reporter.
        incident = self.env['incident.incident'].create({
            'name': 'Test incident',
            'location': "test location",
            'department_id': self.ref('isha_ims.incident_department_dhyanalinga'),
            'incident_type': self.ref('isha_ims.incident_type_drugs'),
            'incident_text': 'sample incident text',
            'reporter_phone_country': self.ref('base.in'),
            'reporter_phone': '9876543210',
            'reporter_email': 'some reporter email',
            'partner_name': 'some partner',
            'partner_phone_country': self.ref('base.in'),
            'partner_phone': '9876543210',
            'partner_email': 'partner email1',
            'country': self.ref('base.in'),
            'nationality': self.ref('base.us'),
            'person_type_id': self.ref('isha_ims.incident_persontype_staff'),
            'medical_description': 'some medical desc.'
        })

        # report the incident
        incident.write({
            'stage_id': self.ref('isha_ims.stage_report')
        })

        # swith to an advanced reporter.
        self.adv_user = self.env['res.users'].sudo().create({
            'login': 'test_ims_adv_reporter',
            'partner_id': self.env['res.partner'].create({
                'name': "test ims ADV reporter"
            }).id,
            'groups_id': [
                (6, 0, [self.ref('isha_ims.group_ims_adv_reporter')])
            ]
        })
        self.env= self.env(user=self.adv_user)

        # create a contact to be used as an agitator.
        agitator = self.env['res.partner'].sudo().create({
            'name': "test Agitator"
        })

        # link the agitator to the incident
        incident.write({
            'partner_id': agitator.id
        })

        # try to change it to open stage
        incident.write({
            'stage_id': self.ref('isha_ims.stage_open')
        })



