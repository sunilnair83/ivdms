from odoo import exceptions
from odoo.tools.misc import mute_logger
from .common import AccessRightsCase


class TestIncidentAccessRights(AccessRightsCase):
    """Test incident access rules
    """

    def test_090_user_incident_type_access(self):
        with mute_logger('odoo.models'):
            with self.assertRaises(exceptions.AccessError):
                self.usimple_type.name   # pylint: disable=pointless-statement

        # Subscribe demo-user to 'simple incident' type
        self.simple_type.message_subscribe(self.demo_user.partner_id.ids)

        # Ensure, after subscription simple_type is readable
        self.assertTrue(self.usimple_type.name)

    def test_100_user_incident_access(self):
        self.simple_type.message_subscribe(self.demo_user.partner_id.ids)

        stage_draft = self.uenv.ref(
            'isha_ims.incident_stage_type_simple_draft')
        stage_sent = self.uenv.ref(
            'isha_ims.incident_stage_type_simple_sent')

        # Create incident
        incident = self.uenv['incident.incident'].create({
            'type_id': self.usimple_type.id,
            'category_id': self.ucategory_demo_general.id,
            'incident_text': 'Demo'
        })
        self.assertEqual(incident.stage_id, stage_draft)

        # Move incident to sent stage
        incident.write({'stage_id': stage_sent.id})
        self.assertEqual(incident.stage_id, stage_sent)

    def test_110_manager_incident_type_access(self):
        # Ensure, simple_type is readable for manager
        self.assertTrue(self.msimple_type.name)

    def test_120_manager_incident_access(self):
        stage_draft = self.menv.ref(
            'isha_ims.incident_stage_type_simple_draft')
        stage_sent = self.menv.ref(
            'isha_ims.incident_stage_type_simple_sent')

        # Create incident
        incident = self.menv['incident.incident'].create({
            'type_id': self.msimple_type.id,
            'category_id': self.mcategory_demo_general.id,
            'incident_text': 'Demo'
        })
        self.assertEqual(incident.stage_id, stage_draft)

        # Move incident to sent stage
        incident.write({'stage_id': stage_sent.id})
        self.assertEqual(incident.stage_id, stage_sent)

    def test_130_user_sent_incident_manager_confirm(self):
        self.simple_type.message_subscribe(self.demo_user.partner_id.ids)

        # Create incident
        incident = self.uenv['incident.incident'].create({
            'type_id': self.usimple_type.id,
            'category_id': self.ucategory_demo_general.id,
            'incident_text': 'Demo'
        })

        # Move incident to sent stage
        incident.write({
            'stage_id': self.uenv.ref(
                'isha_ims.incident_stage_type_simple_sent').id,
        })
        self.assertEqual(
            incident.stage_id,
            self.uenv.ref('isha_ims.incident_stage_type_simple_sent'))

        # Manager read incident
        mincident = self.menv['incident.incident'].browse(incident.id)

        mincident.write({
            'stage_id': self.menv.ref(
                'isha_ims.incident_stage_type_simple_confirmed').id,
        })
        self.assertEqual(
            mincident.stage_id,
            self.menv.ref(
                'isha_ims.incident_stage_type_simple_confirmed'))

    def test_140_access_incident_read(self):
        incident = self.menv['incident.incident'].create({
            'type_id': self.msimple_type.id,
            'category_id': self.mcategory_demo_general.id,
            'incident_text': 'Demo'
        })
        incident.message_subscribe(self.demo_user.partner_id.ids)
        self._read_incident_fields(self.demo_user, incident)

    def test_150_access_incident_read_write_unlink_denied(self):
        incident = self.menv['incident.incident'].create({
            'type_id': self.msimple_type.id,
            'category_id': self.mcategory_demo_general.id,
            'incident_text': 'Demo'
        })

        with mute_logger('odoo.models'):
            with self.assertRaises(exceptions.AccessError):
                with self.env.cr.savepoint():
                    self._read_incident_fields(self.demo_user, incident)

            with self.assertRaises(exceptions.AccessError):
                with self.env.cr.savepoint():
                    incident.with_user(self.demo_user).write({
                        'incident_text': 'Test',
                    })

            with self.assertRaises(exceptions.AccessError):
                with self.env.cr.savepoint():
                    incident.with_user(self.demo_user).unlink()
