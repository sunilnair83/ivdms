from .common import IncidentCase


class TestIncidentBase(IncidentCase):

    def setUp(self):
        super(TestIncidentBase, self).setUp()
        self.Incident = self.env['incident.incident']

    def test_default_incident_text(self):

        incident = self.Incident.new({
            'type_id': self.access_type.id,
            'category_id': self.tec_configuration_category.id,
            'user_id': self.incident_manager.id,
        })
        self.assertFalse(incident.incident_text)

        incident.onchange_type_id()

        self.assertEqual(incident.incident_text,
                         incident.type_id.default_incident_text)

    def test_default_response_text(self):
        incident = self.incident_2

        close_route = self.env.ref(
            'isha_ims.incident_stage_route_type_access_sent_to_rejected')
        close_stage = self.env.ref(
            'isha_ims.incident_stage_route_type_access_sent_to_rejected')

        incident.stage_id = close_stage.id

        incident_closing = self.env['incident.wizard.close'].create({
            'incident_id': incident.id,
            'close_route_id': close_route.id,
        })
        self.assertFalse(incident_closing.response_text)

        incident_closing.onchange_close_route_id()

        self.assertEqual(incident_closing.response_text,
                         close_route.default_response_text)

        incident_closing.action_close_incident()

        self.assertTrue(incident.closed)

        self.assertEqual(incident.response_text,
                         close_route.default_response_text)
