from .common import IncidentCase


class TestIncidentAuthor(IncidentCase):
    def setUp(self):
        super(TestIncidentAuthor, self).setUp()
        self.author_default = self.env.ref('base.partner_root')
        self.partner_default = False
        self.author1 = self.env.ref('base.res_partner_address_2')
        self.partner1 = self.env.ref('base.res_partner_1')
        self.group = 'isha_ims.group_incident_user_can_change_author'
        self.group_change_author = self.env.ref(self.group)

    def test_incident_author_on_change(self):
        incident = self.env['incident.incident'].new({
            'type_id': self.simple_type.id,
            'category_id': self.general_category.id,
            'stage_id': self.stage_draft.id,
            'author_id': self.author_default.id,
            'incident_text': 'Test incident',
        })

        self.assertEqual(incident.stage_id.id, self.stage_draft.id)
        self.assertEqual(incident.author_id.id, self.author_default.id)
        self.assertEqual(incident.partner_id.id, self.partner_default)

        incident.author_id = self.author1.id
        incident._onchange_author_id()
        self.assertEqual(incident.author_id.id, self.author1.id)
        self.assertEqual(incident.partner_id.id, self.partner1.id)

    def test_can_change_author(self):
        incident = self.env['incident.incident'].with_user(
            self.incident_manager).create({
                'type_id': self.simple_type.id,
                'category_id': self.general_category.id,
                'incident_text': 'Test incident'})

        self.assertEqual(self.incident_manager.has_group(self.group), False)

        self.assertEqual(incident.stage_id.id, self.stage_draft.id)
        self.assertEqual(incident.can_change_author, False)

        self.incident_manager.groups_id += self.group_change_author
        self.assertEqual(self.incident_manager.has_group(self.group), True)
        self.assertEqual(incident.can_change_author, True)

        incident.stage_id = self.stage_sent
        self.assertEqual(incident.stage_id.id, self.stage_sent.id)
        self.assertEqual(incident.can_change_author, False)

    def test_author_compute(self):
        partner = self.env.ref('base.res_partner_3')
        author = self.env.ref('base.res_partner_address_5')

        self.assertFalse(partner.incident_ids)
        self.assertEqual(partner.incident_count, 0)
        self.assertFalse(author.incident_ids)
        self.assertEqual(author.incident_count, 0)

        incident = self.env['incident.incident'].create({
            'type_id': self.simple_type.id,
            'category_id': self.general_category.id,
            'incident_text': 'Test incident',
            'partner_id': partner.id,
        })
        self.assertTrue(partner.incident_ids)
        self.assertEqual(partner.incident_count, 1)
        self.assertFalse(author.incident_ids)
        self.assertEqual(author.incident_count, 0)
        self.assertNotIn(author, incident.message_partner_ids)
        self.assertNotIn(partner, incident.message_partner_ids)

        incident = self.env['incident.incident'].create({
            'type_id': self.simple_type.id,
            'category_id': self.general_category.id,
            'incident_text': 'Test incident',
            'author_id': partner.id,
            'partner_id': False,
        })
        self.assertTrue(partner.incident_ids)
        self.assertEqual(partner.incident_count, 2)
        self.assertFalse(author.incident_ids)
        self.assertEqual(author.incident_count, 0)
        self.assertNotIn(author, incident.message_partner_ids)
        self.assertIn(partner, incident.message_partner_ids)

        incident = self.env['incident.incident'].create({
            'type_id': self.simple_type.id,
            'category_id': self.general_category.id,
            'incident_text': 'Test incident',
            'author_id': author.id,
        })
        self.assertTrue(partner.incident_ids)
        self.assertEqual(partner.incident_count, 3)
        self.assertTrue(author.incident_ids)
        self.assertEqual(author.incident_count, 1)
        self.assertIn(author, incident.message_partner_ids)
        self.assertNotIn(partner, incident.message_partner_ids)

        incident = self.env['incident.incident'].create({
            'type_id': self.simple_type.id,
            'category_id': self.general_category.id,
            'incident_text': 'Test incident',
            'author_id': author.id,
            'partner_id': False,
        })
        self.assertTrue(partner.incident_ids)
        self.assertEqual(partner.incident_count, 3)
        self.assertTrue(author.incident_ids)
        self.assertEqual(author.incident_count, 2)
        self.assertIn(author, incident.message_partner_ids)
        self.assertNotIn(partner, incident.message_partner_ids)

        incident.author_id = partner
        self.assertIn(author, incident.message_partner_ids)
        self.assertIn(partner, incident.message_partner_ids)

        # Test actions
        act = partner.action_show_related_incidents()
        self.assertEqual(
            self.env[act['res_model']].search(act['domain']),
            partner.incident_ids)
        act = author.action_show_related_incidents()
        self.assertEqual(
            self.env[act['res_model']].search(act['domain']),
            author.incident_ids)
        req = self.env['incident.incident'].with_context(
            dict(act['context'])).create({
                'type_id': self.simple_type.id,
                'incident_text': 'Test',
            })
        self.assertEqual(req.partner_id, partner)
        self.assertEqual(req.author_id, author)

    def test_compute_partner_of_demo_incident(self):
        self.assertEqual(
            self.env.ref(
                'isha_ims.incident_incident_type_sequence_demo_1'
            ).partner_id,
            self.env.ref('base.res_partner_2'))
