# Changelog

## Version 1.24.0

Request name in title displayed as `h2` instead of `h1` as before


## Version 1.20.0

Add global settings:
- 'Automatically remove events older then',
- 'Event Live Time',
- 'Event Live Time Uom'


## Version 1.17.0

- Make it possible to change incident category for already created incident
- New incident event *Category Changed*
- Show *Requests* stat-button on user's form


## Version 1.16.4

#### Version 1.16.4
Add security groups user_see_all_incidents and user_write_all_incidents and rules for this groups


## Version 1.16.2

#### Version 1.16.2
Added isha_ims_survey to incident settings list.


## Version 1.16.1

#### Version 1.16.1
Added dynamic_popover widget to description field on incident tree view.


## Version 1.16.0

Added `active` field to Request Stage


## Version 1.15.6

More information in error messages


## Version 1.13.11

#### Version 1.13.11
Added the ability to include incident and response texts to mail notifications.


## Version 1.13.5

#### Version 1.13.5
- Automatically subscribe incident author to incident


