# -*- coding: utf-8 -*-
###############################################################################

#
###############################################################################

from odoo import models, fields

class IncidentType(models.Model):
    _name = "ims.incident.type"
    _description = "Incident type"

    name = fields.Char('Incident Type')
    code = fields.Char('Incident Type Code')
    active = fields.Boolean(default=True, help="To make this record in active.", track_visibility='onchange')

class DoNotAllows(models.Model):
    _name = "incident.donotallows"
    _description = "Do not Allows"

    name = fields.Char('Name', size=256, required=True)
    code = fields.Char('Code', size=100, required=True)
    donotallows_category = fields.Selection([('stayarea', 'Do not Allow Stay Areas'),
                                             ('program', 'Do not Allow Programs'),
                                             ('volunteering', 'Do not Allow Volunteering'),
                                             ('general', 'Do not Allow General')])
    active = fields.Boolean(default=True, help="To make this record in active.", track_visibility='onchange')

    incident_stayarea_ids = fields.Many2many('res.partner', column1='donotallow_stayarea_ids',column2='incident_stayarea_ids')
    incident_program_ids = fields.Many2many('res.partner', column1='donotallow_program_ids',column2='incident_program_ids')
    incident_volunteering_ids = fields.Many2many('res.partner', column1='donotallow_volunteering_ids',column2='incident_volunteering_ids')
    incident_general_ids = fields.Many2many('res.partner', column1='donotallow_general_ids',column2='incident_general_ids')


    _sql_constraints = [
        ('unique_donotallow_code',
         'unique(code)', 'Sorry! this code is already exists! Code should be unique!!'),
        ('unique_donotallow_name',
         'unique(name)', 'Sorry! this Group Name is already exists! Name should be unique!!')
    ]


class PersonType(models.Model):
    _name = "incident.persontype"
    _description = "Person Type"

    name = fields.Char('Name', size=256, required=True)
    # code = fields.Char('Code', size=16, required=True)
    # moderation_team_id = fields.Many2one('moderation.team', string='Moderation Team', required=False)
    india_moderation_team_ids = fields.Many2many('moderation.team', 'person_type_india_moderation_team_rel', column1='person_type_ids', column2='moderation_team_ids', string='Moderation Teams - Indian', required=True)
    overseas_moderation_team_ids = fields.Many2many('moderation.team', 'person_type_overseas_moderation_team_rel', column1='person_type_ids', column2='moderation_team_ids', string='Moderation Teams - Overseas', required=True)
    active = fields.Boolean(default=True, help="To make this record in active.", track_visibility='onchange')

    _sql_constraints = [
        ('unique_persontype_name',
         'unique(name)', 'Sorry! this PersonType name already exists! Name should be unique!!')
    ]


class IdProofsType(models.Model):
    _name = "incident.idproofstype"
    _description = "Id Proofs Type"

    name = fields.Char('Id Proof Type Name', size=256, required=True)
    # code = fields.Char('Code', size=16, required=True)
    issuer = fields.Selection([('govt', 'Government'),
                                 ('isha', 'Isha'),
                                 ('others', 'Others')], string="Issuer", default='govt')

    active = fields.Boolean(default=True, help="To make this record in active.", track_visibility='onchange')


class Departments(models.Model):
    _name = "incident.department"
    _description = "Department"

    name = fields.Char('Name', size=256, required=True)
    code = fields.Char('Code')
    active = fields.Boolean(default=True, help="To make this record in active.", track_visibility='onchange')

    _sql_constraints = [
        ('unique_department_name',
         'unique(name)', 'Sorry! this name is already exists! Name should be unique!!'),
       ]


class StarMarkCategory(models.Model):
    _name = "starmark.category"
    _description = "Star Mark Category"

    name = fields.Char()
    code = fields.Char()
    sm_color = fields.Char(string="Color")

    active = fields.Boolean(default=True, index=True)

    time_limit = fields.Integer(string='Time Limit (days)')

    # transition_to_ids = fields.Many2many('starmark.category', relation='category_transition_rel',
    #                                      column1='category_ids', column2='transition_to_ids', string='Transition To')
    transition_to_id = fields.Many2one('starmark.category', 'Transition To', required=False)

    _sql_constraints = [
        # ('unique_starmark_code',
        #  'unique(code)', 'Sorry! Code should be unique!!'),
        ('unique_starmark_name',
         'unique(name)', 'Sorry! Name should be unique!!')
    ]


