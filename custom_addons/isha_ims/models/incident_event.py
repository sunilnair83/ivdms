import logging
import datetime
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class IncidentEvent(models.Model):
    _name = 'incident.event'
    _description = 'Incident Event'
    _order = 'date DESC, id DESC'
    _log_access = False

    event_type_id = fields.Many2one(
        'incident.event.type', required=True, readonly=True)
    event_code = fields.Char(
        related='event_type_id.code', readonly=True)
    incident_id = fields.Many2one(
        'incident.incident', index=True, required=True, readonly=True,
        ondelete='cascade')
    date = fields.Datetime(
        default=fields.Datetime.now, required=True, index=True, readonly=True)
    user_id = fields.Many2one('res.users', required=True, readonly=True)

    # Assign related events
    old_user_id = fields.Many2one('res.users', readonly=True)
    new_user_id = fields.Many2one('res.users', readonly=True)

    # Change incident description
    old_text = fields.Html(readonly=True)
    new_text = fields.Html(readonly=True)

    # Incident stage change
    route_id = fields.Many2one('incident.stage.route', readonly=True)
    old_stage_id = fields.Many2one('incident.stage', readonly=True)
    new_stage_id = fields.Many2one('incident.stage', readonly=True)

    # Incident Category Change
    old_category_id = fields.Many2one('incident.category', readonly=True)
    new_category_id = fields.Many2one('incident.category', readonly=True)

    def name_get(self):
        res = []
        for record in self:
            res.append((
                record.id,
                "%s [%s]" % (
                    record.incident_id.name,
                    record.event_type_id.display_name)
            ))
        return res

    def get_context(self):
        """ Used in notifications and actions to be backward compatible
        """
        self.ensure_one()
        return {
            'old_user': self.old_user_id,
            'new_user': self.new_user_id,
            'old_text': self.old_text,
            'new_text': self.new_text,
            'route': self.route_id,
            'old_stage': self.old_stage_id,
            'new_stage': self.new_stage_id,
            'incident_event': self,
        }

    @api.model
    def _scheduler_vacuum(self, days=False):
        """ Run vacuum for events.
            Delete all events older than <days>
        """
        if days:
            _logger.warning(
                "Passing incident event's time to live in scheduler is"
                " deprecated! Please, update cron job to "
                "call '_scheduler_vacuum' without arguments")

        if self.env.user.company_id.incident_event_live_time_uom == 'days':
            delta = relativedelta(
                days=+self.env.user.company_id.incident_event_live_time)
        elif self.env.user.company_id.incident_event_live_time_uom == 'weeks':
            delta = relativedelta(
                days=+self.env.user.company_id.incident_event_live_time*7)
        elif self.env.user.company_id.incident_event_live_time_uom == 'months':
            delta = relativedelta(
                months=+self.env.user.company_id.incident_event_live_time)
        vacuum_date = datetime.datetime.now() - delta
        if self.env.user.company_id.incident_event_auto_remove:
            self.sudo().search(
                [('date', '<', fields.Datetime.to_string(vacuum_date))],
            ).unlink()
