from odoo import api, models, fields


class ResUsers(models.Model):
    _inherit = 'res.users'

    # team_id = fields.Many2one('moderation.team', string='Team')
    team_ids = fields.Many2many('moderation.team', relation='res_users_moderation_team_rel', 
        column1='res_user_ids', column2='moderation_team_ids', string='Teams')
    # moderator_ids = fields.Many2many('res.users', relation='users_moderators_rel',
    #                                  column1='reporter', column2='moderator', string='Moderators')

    def action_show_related_incidents(self):
        return self.partner_id.action_show_related_incidents()

    # @api.model
    # def fields_view_get(self, view_id=None, view_type='form',
    #                     toolbar=False, submenu=False):
    #     result = super(ResUsers, self).fields_view_get(
    #         view_id=view_id, view_type=view_type,
    #         toolbar=toolbar, submenu=submenu)
    #     if view_type == 'form':
    #         moderator_group_ref = self.env.ref('isha_ims.group_ims_moderator')
    #         moderator_ids = self.env['res.users'].search([('groups_id', 'in', [moderator_group_ref.id])]).ids
    #         result['fields']['moderator_ids']['domain'] = [('id', 'in', moderator_ids)]
    #     return result
