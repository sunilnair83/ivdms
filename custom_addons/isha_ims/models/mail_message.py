import logging
from odoo import models, fields, api, _, exceptions
from odoo.osv import expression
from datetime import timedelta
from lxml import etree
import time
import json
from .. import isha_base_importer
_logger = logging.getLogger(__name__)


class MailMessage(models.Model):
    _inherit = 'mail.message'


    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        domain_list=['|','&',('model','not in',['res.partner','incident.incident']),('id', '!=', False),'&',('model','in',['res.partner','incident.incident']),('subject', '=', None)]
        for x in domain_list:
            args.append(x)
        return super(MailMessage, self)._search(
                args, offset=offset, limit=limit, order=order,
                count=count, access_rights_uid=access_rights_uid)