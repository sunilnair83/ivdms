from odoo import fields, models, api

FEATURE_MODULES = [
    'generic_assignment_hr',
    'generic_assignment_team',
    'isha_ims_assignment',
    'isha_ims_action',
    'isha_ims_action_condition',
    'isha_ims_action_project',
    'isha_ims_action_subincident',
    'isha_ims_sla',
    'isha_ims_sla_log',
    'isha_ims_field',
    'isha_ims_kind',
    'isha_ims_parent',
    'isha_ims_priority',
    'isha_ims_related_doc',
    'isha_ims_related_incidents',
    'isha_ims_reopen_as',
    'isha_ims_route_auto',
    'isha_ims_service',
    'isha_ims_tag',
    'isha_ims_mail',
    'isha_ims_api',
    'isha_ims_survey',
    'isha_ims_action_survey',
    'isha_ims_action_tag',
]


class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    # Modules available
    module_generic_assignment_hr = fields.Boolean(
        string="HR Assignments")
    module_generic_assignment_team = fields.Boolean(
        string="Team Assignments")
    module_isha_ims_assignment = fields.Boolean(
        string="Use Custom Assignment Policies")
    module_isha_ims_action = fields.Boolean(
        string="Use Automated Actions")
    module_isha_ims_action_condition = fields.Boolean(
        string="Conditional Actions")
    module_isha_ims_action_project = fields.Boolean(
        string="Tasks")
    module_isha_ims_action_subincident = fields.Boolean(
        string="Subincidents")
    module_isha_ims_sla = fields.Boolean(
        string="Use Service Level Agreements")
    module_isha_ims_sla_log = fields.Boolean(
        string="Log Service Level")
    module_isha_ims_field = fields.Boolean(
        string="Use Custom Fields in Incidents")
    module_isha_ims_kind = fields.Boolean(
        string="Kinds of Incidents")
    module_isha_ims_parent = fields.Boolean(
        string="Use Subincidents")
    module_isha_ims_priority = fields.Boolean(
        string="Prioritize Incidents")
    module_isha_ims_related_doc = fields.Boolean(
        string="Documents Related to Incidents")
    module_isha_ims_related_incidents = fields.Boolean(
        string="Related Incidents")
    module_isha_ims_reopen_as = fields.Boolean(
        string="Reopen Incidents")
    module_isha_ims_route_auto = fields.Boolean(
        string="Use Automatic Routes")
    module_isha_ims_service = fields.Boolean(
        string="Use Services")
    module_isha_ims_tag = fields.Boolean(
        string="Use Tags")
    module_isha_ims_mail = fields.Boolean(
        string="Use Mail Sources")
    module_isha_ims_api = fields.Boolean(
        string="Use API endpoints")
    module_isha_ims_survey = fields.Boolean(
        string="Surveys")
    module_isha_ims_action_survey = fields.Boolean(
        string="Surveys")
    module_isha_ims_action_tag = fields.Boolean(
        string="Tags")

    # Modules available (helpers)
    need_install_generic_assignment_hr = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_generic_assignment_team = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_assignment = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_action = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_action_condition = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_action_project = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_action_subincident = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_sla = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_sla_log = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_field = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_kind = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_parent = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_priority = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_related_doc = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_related_incidents = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_reopen_as = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_route_auto = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_service = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_tag = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_mail = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_api = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_survey = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_action_survey = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)
    need_install_isha_ims_action_tag = fields.Boolean(
        compute="_compute_isha_ims_modules_can_install", readonly=True)

    incident_event_live_time = fields.Integer(
        related='company_id.incident_event_live_time', readonly=False)
    incident_event_live_time_uom = fields.Selection(
        related='company_id.incident_event_live_time_uom',
        readonly=False,
        help='Units of Measure'
    )
    incident_event_auto_remove = fields.Boolean(
        related='company_id.incident_event_auto_remove', readonly=False)

    @api.depends('company_id')
    def _compute_isha_ims_modules_can_install(self):
        available_module_names = self.env['ir.module.module'].search([
            ('name', 'in', FEATURE_MODULES),
            ('state', '!=', 'uninstallable'),
        ]).mapped('name')
        for record in self:
            for module in FEATURE_MODULES:
                record['need_install_%s' % module] = (
                    module not in available_module_names
                )
