from odoo import models, fields, api

class PartnerCategory(models.Model):
    _name = 'partner.category'
    _description = 'Partner Category'

    color = fields.Integer()  # for many2many_tags widget
    # Defined in generic.mixin.name_with_code
    name = fields.Char()
    code = fields.Char()
    category_ptn_ids = fields.One2many(
        'partner.category.mapping','category_id' ,'Star Marking', index=True,
        required=False, ondelete="restrict", track_visibility='onchange')
    transition_id = fields.Many2one('partner.category', string='Transition')
    active = fields.Boolean(default=True, index=True)
    description = fields.Text(translate=True)
    help_html = fields.Html(translate=True)


    partner_ids = fields.One2many(
        'res.partner', 'starmark_category_id', 'Partners', readonly=True)
    partner_count = fields.Integer(
        'All Partners', compute='_compute_partner_count', readonly=True)

    @api.depends('partner_ids')
    def _compute_partner_count(self):
        for record in self:
            record.partner_count = len(record.partner_ids)

    # Copied from incident_category. no Clue!!!
    def name_get(self):
        # This is required to avoid access rights errors when tracking values
        # in chatter. (At least in Odoo 10.0)
        return super(PartnerCategory, self.sudo()).name_get()
