from odoo import api, models, fields


class ResCountry(models.Model):
    _inherit = 'res.country'

    # This is used to change the display of the options in the phone country code dropdown
    def name_get(self):
        res = []
        if self.env.context.get('phone_code', False):
            for record in self:
                res.append((record.id, "{} [{}] (+{})".format(record.name, record.code, record.phone_code)))
        else:
            # Do a for and set here the standard display name, for example if the standard display name were name, you should do the next for
            for record in self:
                res.append((record.id, record.name))
        return res
