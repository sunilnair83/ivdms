from odoo import models, fields


class IncidentLocation(models.Model):
    _name = "incident.location"
    _description = "Incident location"

    name = fields.Char('Incident Location')
    code = fields.Char('Incident Location Code', help="Do not modify this. This is only for internal reference.", readonly=True)
    active = fields.Boolean(
        default=True, help="To make this record in active.", track_visibility='onchange')
