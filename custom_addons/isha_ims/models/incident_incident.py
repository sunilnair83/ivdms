import logging
import json
import phonenumbers
import re
import datetime
from lxml import etree
from odoo import models, fields, api, tools, _, exceptions, SUPERUSER_ID
from odoo.osv import expression
from .. import isha_base_importer
from ...isha_base.configuration import Configuration
from confluent_kafka import Producer

_logger = logging.getLogger(__name__)

try:
    from html2text import HTML2Text
except ImportError as error:
    _logger.debug(error)

TRACK_FIELD_CHANGES = set((
    'stage_id', 'user_id', 'category_id', 'incident_text',
    'partner_id', 'team_ids'))
REQUEST_TEXT_SAMPLE_MAX_LINES = 3
KANBAN_READONLY_FIELDS = set(('category_id', 'stage_id'))


def html2text(html):
    """ covert html to text, ignoring images and tables
    """
    if not html:
        return ""

    ht = HTML2Text()
    ht.ignore_images = True
    ht.ignore_tables = True
    ht.ignore_emphasis = True
    ht.ignore_links = True
    return ht.handle(html)


class IncidentIncident(models.Model):
    _name = "incident.incident"
    _inherit = [
        'mail.thread',
        'mail.activity.mixin',
        'generic.mixin.track.changes',
    ]
    _description = 'Incident'
    _order = 'create_date DESC'
    _needaction = True
    # show_agitator_information = fields.Boolean(
    #     compute='_show_agitator_information', default=True)
    # show_incident_information = fields.Boolean(
    #     compute='_show_incident_information', default=True)
    # show_emergency_information = fields.Boolean(
    #     compute='_show_emergency_information', default=True)
    # show_reporter_information = fields.Boolean(
    #     compute='_show_reporter_information', default=True)

    # type_id = fields.Many2one(
    #     'incident.type', 'Incident Type', ondelete='restrict',
    #     index=True, required=True, track_visibility='always')
    # type_color = fields.Char(related="type_id.color")

    date_closed = fields.Datetime('Expiry Date', copy=False)
    date_assigned = fields.Datetime('Assigned Date', copy=False)

    is_adv_user = fields.Boolean(compute='_compute_user_group')
    show_sm_btn = fields.Boolean(compute='_compute_to_show_sm_btn')
    is_approved = fields.Boolean(compute='_compute_partner_status')
    show_sm_ribbon = fields.Boolean(compute='_compute_show_sm_ribbon', default=False)

    # Agitator Primary Fields
    partner_id = fields.Many2one(
        'res.partner', string='Contact', track_visibility='onchange', help="Partner related to this incident",index=True)
    crm_partner_sm = fields.Char(
        related="partner_id.starmark_category_id.sm_color", store=False, readonly=True)
    partner_sm_cat = fields.Char(related="partner_id.starmark_category_id.name", store=True, readonly=True)
    partner_sm_code = fields.Char(related="partner_id.starmark_category_id.code")
    partner_name = fields.Char(string="Agitator Name", required=True, track_visibility='always')
    partner_phone_country = fields.Many2one(
        'res.country', string='Country Code', track_visibility='onchange')
    partner_phone = fields.Char(string="Agitator Phone", track_visibility='onchange')
    partner_email = fields.Char(string="Agitator Email", track_visibility='onchange')
    partner_photo = fields.Binary(string="Photo", track_visibility='onchange')
    gender = fields.Selection(
        [('M', "Male"), ('F', "Female"), ('O', 'Others')], required=False, track_visibility='onchange')

    last_autoupdated_partner_photo = fields.Binary(string="Last Autoupdated Agitator Photo")  # only for ui

    # Agitator Secondary Fields
    country = fields.Many2one('res.country', string='Country', required=True, track_visibility='onchange')
    zip = fields.Char(string='zip', track_visibility='onchange')
    state = fields.Many2one('res.country.state', string='State', track_visibility='onchange')
    city = fields.Char(string='City', track_visibility='onchange')
    street_1 = fields.Char(string='Address Line 1', track_visibility='onchange')
    street_2 = fields.Char(string='Address Line 2', track_visibility='onchange')
    nationality = fields.Many2one('res.country', string="Nationality", required=True, track_visibility='onchange')
    is_overseas = fields.Boolean('Is Overseas', track_visibility='onchange')
    occupation = fields.Char('Occupation', track_visibility='onchange')
    dob = fields.Date(string='DOB', track_visibility='onchange')
    approx_dob = fields.Date(string='Approximate Date Of Birth', required=False)
    age = fields.Integer(string='Age', compute='_compute_age')
    person_type_id = fields.Many2one('incident.persontype', 'Person Type', required=True, track_visibility='onchange')
    govt_id_type = fields.Many2one('incident.idproofstype', String="Govt. ID Type", domain=[
        ('active', '=', True), ('issuer', '=', 'govt')], track_visibility='onchange')
    govt_issued_id = fields.Char(string='Govt Issued ID', track_visibility='onchange')
    govt_id_image = fields.Binary(string='Govt ID Image', track_visibility='onchange')
    isha_id_type = fields.Many2one('incident.idproofstype', String="Isha ID Type", domain=[
        ('active', '=', True), ('issuer', '=', 'isha')], track_visibility='onchange')
    isha_issued_id = fields.Char(string='Isha Issued ID', track_visibility='onchange')
    isha_id_image = fields.Binary(string='Isha ID Image', track_visibility='onchange')
    # medical information
    has_medical_reasons = fields.Boolean('Has Medical Reasons', track_visibility='onchange')
    medical_description = fields.Html('Medical Reasons Description', track_visibility='onchange')

    # Incident Details
    date_created = fields.Datetime('Incident Date', default=fields.Datetime.now, copy=False,
                                   track_visibility='onchange')

    location = fields.Many2one('incident.location', string='Incident Location',
                               index=True, required=True, track_visibility='onchange')
    other_location = fields.Char('Other Incident Location', track_visibility='onchange')
    # incident severity
    severity = fields.Selection([('1', 'Low'), ('2', 'Medium'),
                                 ('3', 'High'), ('4', 'Severe')], string='Severity', track_visibility='onchange')
    department_id = fields.Many2one(
        'incident.department', 'Incident Department', track_visibility='onchange')
    # incident_type = fields.Many2one(
    #     'ims.incident.type', string="Incident Type", index=True, required=True, track_visibility='always')
    incident_types = fields.Many2many('ims.incident.type', relation='incident_incident_type_rel', column1='incidents',
                                      column2='types', string="Incident Type")
    other_incident_type = fields.Char('Other Incident Type', track_visibility='onchange')
    incident_text = fields.Text('Incident Description', required=True, track_visibility='onchange')
    response_text = fields.Html('Action To Be Taken', required=False, track_visibility='onchange')

    is_location_other = fields.Boolean('Is location other', compute='_compute_is_location_other', default=False,
                                       store=False)  # only for ui
    # is_incident_type_other = fields.Boolean(
    #     'Is Incident Type other', compute='_compute_is_incident_type_other', default=False, store=False)  # only for ui

    # Emergency Contact Details
    emergency_contact_name = fields.Char('Emergency Contact Name', track_visibility='onchange')
    emergency_contact_relation = fields.Char('Emergency Contact Relation', track_visibility='onchange')
    emergency_contact_email = fields.Char('Emergency Contact Email ID', track_visibility='onchange')
    emergency_contact_phone_country = fields.Many2one(
        'res.country', string='Emergency Phone Country Code', track_visibility='onchange')
    emergency_contact_phone = fields.Char('Emergency Contact Phone', track_visibility='onchange')

    # Reporter Details
    reporter_name = fields.Char(
        'Reporter Full Name', required=True, default=lambda self: self.env.user.name, track_visibility='onchange')
    reporter_email = fields.Char(
        'Reporter Email ID', required=True, default=lambda self: self.env.user.email, track_visibility='onchange')

    def _get_phone_country_code(self):
        try:
            phone_country_code = int(
                self.env.user.phone_country_code)  # Turning string phone_country_code into integer
            # Looking for country in the res.country table using the phone_code...
            country_code = self.env['res.country'].search([('phone_code', '=', phone_country_code)])[0]
        except:
            # Populating with country_id or nationality_id if phone_code is missing.
            if self.env.user.country_id:
                country_code = self.env.user.country_id
            elif self.env.user.nationality_id:
                country_code = self.env.user.nationality_id
            else:
                country_code = False
        return country_code

    reporter_phone_country = fields.Many2one(
        'res.country', string='Reporter Phone Country Code', required=True,
        default=_get_phone_country_code, track_visibility='onchange')
    reporter_phone = fields.Char(
        'Reporter Phone', required=True, default=lambda self: self.env.user.phone, track_visibility='onchange')

    reporter_phone_valid = fields.Boolean('Reporter Phone Validity', default=False)  # only for ui
    # Other Fields

    name = fields.Char(
        required=False, index=True, default="New",
        copy=False, track_visibility='onchange', string="Incident ID")
    stage_id = fields.Many2one(
        'incident.stage', 'Stage', ondelete='restrict',
        required=True, index=True, track_visibility="onchange",
        copy=False)
    team_ids = fields.Many2many(
        'moderation.team', relation='incident_incident_moderation_team_rel', string='Moderation Teams',
        column1='incident_incident_ids', column2='moderation_team_ids', ondelete='restrict',
        track_visibility='onchange', help="Moderation Teams of this incident")

    created_by_id = fields.Many2one(
        'res.users', 'Raised by', readonly=True, ondelete='restrict',
        default=lambda self: self.env.user,
        help="Incident was created by this user", copy=False, track_visibility='onchange')
    date_moved = fields.Datetime('Moved', readonly=True, copy=False, track_visibility='onchange')
    moved_by_id = fields.Many2one(
        'res.users', 'Moved by', readonly=True, ondelete='restrict',
        copy=False, track_visibility='onchange')
    closed_by_id = fields.Many2one(
        'res.users', 'Closed by', readonly=True, ondelete='restrict',
        copy=False, help="Incident was closed by this user", track_visibility='onchange')

    # user_ids = fields.Many2many('res.users', relation='incident_incident_res_user_rel',
    #                             column1='incident_ids', column2='assigned_user_ids', string='Moderators',
    #                             track_visibility='onchange', ondelete='restrict')

    author_id = fields.Many2one(
        'res.partner', 'Author', index=True, required=False,
        ondelete='restrict',
        track_visibility='onchange',
        domain=[('is_company', '=', False)],
        default=lambda self: self.env.user.partner_id,
        help="Author of this incident")
    user_id = fields.Many2one(
        'res.users', 'Assigned to',
        ondelete='restrict', track_visibility='onchange',
        help="User responsible for next action on this incident.")

    # team_id = fields.Many2one(
    #     'moderation.team', 'Moderation Team',
    #     ondelete='restrict',
    #     track_visibility='onchange',
    #     # default=lambda self: self.partner_id.person_type_id.moderation_team_id,
    #     help="Moderation Team of this incident")

    # help_html = fields.Html(
    #     "Help", related="type_id.help_html", readonly=True)
    category_help_html = fields.Html(related='category_id.help_html',
                                     readonly=True, string="Category help")
    stage_help_html = fields.Html(
        "Stage help", related="stage_id.help_html", readonly=True)
    # instruction_html = fields.Html(related='type_id.instruction_html',
    #                                readonly=True, string='Instruction')
    # note_html = fields.Html(related='type_id.note_html',
    #                         readonly=True, string="Note")

    # Type and stage related fields

    category_id = fields.Many2one(
        'incident.category', 'Star Marking', index=True,
        required=False, ondelete="restrict", track_visibility='onchange')

    stage_type_id = fields.Many2one(
        'incident.stage.type', related="stage_id.type_id", string="Stage Type",
        index=True, readonly=True, store=True)
    stage_type_name = fields.Char(
        related="stage_type_id.name", store=True, readonly=True)
    stage_bg_color = fields.Char(
        compute="_compute_stage_colors", string="Stage Background Color")
    stage_label_color = fields.Char(
        compute="_compute_stage_colors")
    last_route_id = fields.Many2one('incident.stage.route', 'Last Route')
    closed = fields.Boolean(related='stage_id.closed', store=True, index=True, readonly=True)
    can_be_closed = fields.Boolean(compute='_compute_can_be_closed', readonly=True, compute_sudo=False)

    # UI change restriction fields
    # can_change_incident_text = fields.Boolean(
    #     compute='_compute_can_change_incident_text', readonly=True,
    #     compute_sudo=False)
    # can_change_assignee = fields.Boolean(
    #     compute='_compute_can_change_assignee', readonly=True,
    #     compute_sudo=False)
    # can_change_author = fields.Boolean(
    #     compute='_compute_can_change_author', readonly=True,
    #     compute_sudo=False)
    # can_change_category = fields.Boolean(
    #     compute='_compute_can_change_category', readonly=True,
    #     compute_sudo=False)
    next_stage_ids = fields.Many2many(
        'incident.stage', compute="_compute_next_stage_ids", readonly=True)

    incident_text_sample = fields.Text(
        compute="_compute_incident_text_sample", track_visibility="always",
        string='Incident text')

    message_discussion_ids = fields.One2many(
        'mail.message', 'res_id',
        string='Discussion Messages',
        compute="_compute_message_discussion_ids",
        store=False, compute_sudo=False)

    # instruction_visible = fields.Boolean(
    #     compute='_compute_instruction_visible', default=False,
    #     compute_sudo=False)

    activity_date_deadline = fields.Date(compute_sudo=True)
    message_ids = fields.One2many(
        groups="isha_ims.group_ims_adv_reporter, isha_ims.group_ims_moderator")
    message_follower_ids = fields.One2many(
        groups="isha_ims.group_ims_adv_reporter, isha_ims.group_ims_moderator")

    # Events
    incident_event_ids = fields.One2many(
        'incident.event', 'incident_id', 'Events', readonly=True)
    incident_event_count = fields.Integer(
        compute='_compute_incident_event_count', readonly=True)

    # For possible Contact matches
    partner_match_ids = fields.One2many(
        'res.partner', 'incident_match_id', string=' ',
        compute="_compute_partner_match_ids", readonly=True, store=False)

    # To differentiate UI from Import
    is_data_from_import = fields.Boolean(default=False, invisible=True)

    # a few dummy fields to facilitate the Contact Creation/Linking
    # without making delegate=True for partner_id field
    # contact_name_import = fields.Char(invisible=True, required=False)
    # contact_phone_import = fields.Char(invisible=True, required=False)
    # contact_mobile_import = fields.Char(invisible=True, required=False)
    # contact_email_import = fields.Char(invisible=True, required=False)
    contact_sm_import = fields.Char(invisible=True, required=False, string='Star Mark Category')
    contact_sm_date_assign_import = fields.Char(invisible=True, required=False, string='Star Mark Assigned Date')
    contact_sm_date_closed_import = fields.Char(invisible=True, required=False, string='Star Mark Expiry Date')
    contact_donotallow_stayarea_ids_import = fields.Char(
        invisible=True, required=False, string="DNA StayArea")
    contact_dna_stayarea_comments_import = fields.Char(
        invisible=True, required=False, string='DNA Stayarea Comments')
    contact_donotallow_program_ids_import = fields.Char(
        invisible=True, required=False, string="DNA Programs")
    contact_dna_program_comments = fields.Char(invisible=True, required=False, string='DNA Program Comments')
    contact_donotallow_volunteering_ids_import = fields.Char(
        invisible=True, required=False, string="DNA Volunteering")
    contact_dna_volunteering_comments_import = fields.Char(
        invisible=True, required=False, string='DNA Volunteering Comments')
    contact_donotallow_general_ids_import = fields.Char(
        invisible=True, required=False, string="DNA General")
    contact_dna_general_comments_import = fields.Char(
        invisible=True, required=False, string='DNA General Comments')
    contact_partner_severity_import = fields.Char(
        invisible=True, required=False)
    incident_types_import = fields.Char(
        invisible=True, required=False, string='Incident Type')
    team_ids_import = fields.Char(
        invisible=True, required=False, string='Moderation Teams')

    @api.depends('location')
    def _compute_is_location_other(self):
        for rec in self:
            if rec.location.code == 'incident_location_others':
                rec.is_location_other = True
            else:
                rec.is_location_other = False

    # @api.depends('incident_type')
    # def _compute_is_incident_type_other(self):
    #     for rec in self:
    #         if rec.incident_type.code == 'incident_type_other':
    #             rec.is_incident_type_other = True
    #         else:
    #             rec.is_incident_type_other = False

    @api.depends('department_id')
    def _compute_partner_match_ids(self):
        for rec in self:
            ids = []
            if rec.stage_id.type_id.code == 'report' and \
                    self.env.user.has_group('isha_ims.group_ims_adv_reporter'):
                name = rec.partner_name
                phone = rec.partner_phone
                email = rec.partner_email
                contact_results = self.env['res.partner'].getHighMatch(name, phone, email)

                if contact_results:
                    for result in contact_results:
                        ids.append(result.id)

            rec.partner_match_ids = self.env['res.partner'].search([('id', 'in', ids)])

    @api.onchange('stage_id')
    def update_partner_severity(self):
        for rec in self:
            if rec.stage_id.type_id.code == 'approved' or rec.stage_id.type_id.code == 'freeze':
                severity_current = int(rec.severity)
                severity_partner = int(rec.partner_id.partner_severity)
                if severity_current > severity_partner:
                    rec.partner_id.partner_severity = str(severity_current)

    @api.depends('department_id')
    def _compute_partner_status(self):
        for rec in self:
            if rec.stage_id.type_id.code == 'approved' or rec.stage_id.type_id.code == 'rejected' or \
                    rec.stage_id.type_id.code == 'freeze' or rec.stage_id.type_id.code == 'unfreeze':
                rec.is_approved = True
            else:
                rec.is_approved = False

    @api.onchange('partner_id')
    def _onchange_partner(self):
        self.ensure_one()
        # user_is_adv_reporter = self.env.user.has_group(
        #     'isha_ims.group_ims_adv_reporter')
        # user_is_moderator = self.env.user.has_group(
        #     'isha_ims.group_ims_moderator')

        for rec in self:
            if rec.partner_id:
                contact = rec.partner_id
                if contact:
                    if not rec.created_by_id.has_group('isha_ims.group_ims_reporter'):
                        rec.partner_name = contact.name
                        try:
                            country_code = int(
                                contact.phone_country_code)  # Turning string phone_country_code into integer
                            # Looking for country in the res.country table using the phone_code...
                            rec.partner_phone_country = self.env['res.country'].search(
                                [('phone_code', '=', country_code)])[0]
                        except:
                            # Populating with country_id or nationality_id if phone_code is missing.
                            if contact.country_id:
                                rec.partner_phone_country = contact.country_id
                            elif contact.nationality_id:
                                rec.partner_phone_country = contact.nationality_id

                        rec.partner_phone = contact.phone
                        rec.partner_email = contact.email
                        rec.gender = contact.gender
                        rec.dob = contact.dob
                        rec.approx_dob = contact.approx_dob
                        rec.street_1 = contact.street
                        rec.city = contact.city
                        rec.state = contact.state_id
                        rec.zip = contact.zip
                        rec.country = contact.country_id
                        rec.nationality = contact.nationality_id
                    # if self.env.user.has_group('isha_ims.group_ims_moderator') and rec.stage_type_name == 'Open':
                    #     rec.crm_partner_sm = contact.starmark_category_id.sm_color

                    if not rec.partner_photo or rec.last_autoupdated_partner_photo == rec.partner_photo:
                        rec.partner_photo = contact.image_1920
                        rec.last_autoupdated_partner_photo = contact.image_1920

    @api.depends('dob', 'approx_dob')
    def _compute_age(self):
        today = datetime.date.today()
        for rec in self:
            if rec.dob and rec.dob > today:
                raise exceptions.ValidationError(_('Date of Birth can not be future date'))
            valid_dob = rec.dob or rec.approx_dob
            if valid_dob:
                rec.age = today.year - valid_dob.year - ((today.month, today.day) < (valid_dob.month, valid_dob.day))
            else:
                rec.age = None

    # @api.depends('show_agitator_information')
    # def _show_agitator_information(self):
    #     for rec in self:
    #         rec.show_agitator_information = rec.id and not self.env.user.has_group(
    #             'isha_ims.group_ims_moderator')

    # @api.depends('show_incident_information')
    # def _show_incident_information(self):
    #     for rec in self:
    #         rec.show_incident_information = not self.env.user.has_group(
    #             'isha_ims.group_ims_moderator')

    # @api.depends('show_emergency_information')
    # def _show_emergency_information(self):
    #     for rec in self:
    #         rec.show_emergency_information = not self.env.user.has_group(
    #             'isha_ims.group_ims_moderator')

    # @api.depends('show_reporter_information')
    # def _show_reporter_information(self):
    #     for rec in self:
    #         rec.show_reporter_information = not self.env.user.has_group(
    #             'isha_ims.group_ims_moderator')

    @api.depends('department_id')
    def _compute_user_group(self):
        self.ensure_one()
        for rec in self:
            rec.is_adv_user = self.env.user.has_group(
                'isha_ims.group_ims_adv_reporter') or self.env.user.has_group(
                'isha_ims.group_ims_moderator') or self.env.user.has_group('isha_ims.group_ims_admin')

    @api.depends('department_id')
    def _compute_to_show_sm_btn(self):
        for rec in self:
            if rec.partner_id:
                rec.show_sm_btn = self.env.user.has_group(
                    'isha_ims.group_ims_moderator') and rec.stage_id is not None and rec.stage_id.type_id.code in ['draft', 'open', 'approved', 'unfreeze']
            else:
                rec.show_sm_btn = False

    @api.depends('department_id')
    def _compute_show_sm_ribbon(self):
        for rec in self:
            rec.show_sm_ribbon = self.env.user.has_group(
                'isha_ims.group_ims_moderator') and rec.crm_partner_sm is not False and rec.stage_id is not None and rec.stage_id.type_id.code in ['open']

    @api.onchange('person_type_id', 'is_overseas')
    def _compute_team_ids(self):
        for rec in self:
            rec.team_ids = [(5, 0,)]
            if rec.person_type_id:
                if rec.is_overseas:
                    rec.team_ids = rec.person_type_id.overseas_moderation_team_ids
                else:
                    rec.team_ids = rec.person_type_id.india_moderation_team_ids

    @api.onchange('nationality', 'country')
    def _update_isoverseas(self):
        for rec in self:
            if rec.nationality and rec.country:
                if rec.nationality.name != 'India' or rec.country.name != 'India':
                    rec.is_overseas = True
                else:
                    rec.is_overseas = False

    @api.depends('message_ids')
    def _compute_message_discussion_ids(self):
        for incident in self:
            incident.message_discussion_ids = incident.message_ids.filtered(
                lambda r: r.subtype_id == self.env.ref('mail.mt_comment'))

    @api.depends('stage_id', 'stage_id.type_id')
    def _compute_stage_colors(self):
        for rec in self:
            rec.stage_bg_color = rec.stage_id.res_bg_color
            rec.stage_label_color = rec.stage_id.res_label_color

    @api.depends('stage_id.route_out_ids.stage_to_id.closed')
    def _compute_can_be_closed(self):
        for record in self:
            can_be_closed = False
            for route in record.stage_id.route_out_ids:
                if route.close:
                    can_be_closed = True
                    break
            record.can_be_closed = can_be_closed

    @api.depends('incident_event_ids')
    def _compute_incident_event_count(self):
        for record in self:
            record.incident_event_count = len(record.incident_event_ids)

    def _hook_can_change_incident_text(self):
        """ Can be overridden in other addons
        """
        self.ensure_one()
        return not self.closed

    def _hook_can_change_assignee(self):
        """ Can be overridden in other addons
        """
        self.ensure_one()
        return not self.closed

    def _hook_can_change_category(self):
        return
        # self.ensure_one()
        # return self.stage_id == self.sudo().type_id.start_stage_id

    # @api.depends('type_id', 'stage_id', 'user_id',
    #              'partner_id', 'created_by_id')
    # def _compute_can_change_incident_text(self):
    #     for rec in self:
    #         rec.can_change_incident_text = rec._hook_can_change_incident_text()

    # @api.depends('type_id', 'stage_id', 'user_id',
    #              'partner_id', 'created_by_id')
    # def _compute_can_change_assignee(self):
    #     for rec in self:
    #         rec.can_change_assignee = rec._hook_can_change_assignee()

    # @api.depends('type_id', 'type_id.start_stage_id', 'stage_id')
    # def _compute_can_change_author(self):
    #     for record in self:
    #         if not self.env.user.has_group(
    #                 'isha_ims.group_incident_user_can_change_author'):
    #             record.can_change_author = False
    #         elif record.stage_id != record.sudo().type_id.start_stage_id:
    #             record.can_change_author = False
    #         else:
    #             record.can_change_author = True

    # @api.depends('type_id', 'type_id.start_stage_id', 'stage_id')
    # def _compute_can_change_category(self):
    #     for record in self:
    #         record.can_change_category = record._hook_can_change_category()

    def _get_next_stage_route_domain(self):
        self.ensure_one()
        groups = []
        if self.env.user.has_group('isha_ims.group_ims_moderator'):
            groups.append(self.env.ref('isha_ims.group_ims_moderator').id)
        if self.env.user.has_group('isha_ims.group_ims_adv_reporter'):
            groups.append(self.env.ref('isha_ims.group_ims_adv_reporter').id)
        if self.env.user.has_group('isha_ims.group_ims_reporter'):
            groups.append(self.env.ref('isha_ims.group_ims_reporter').id)
        if self.env.user.has_group('isha_ims.group_ims_bulk_uploader'):
            groups.append(self.env.ref('isha_ims.group_ims_bulk_uploader').id)
        return [
            # ('incident_type_id', '=', self.type_id.id),
            ('stage_from_id', '=', self.stage_id.id),
            ('allowed_group_ids', 'in', groups),
            ('close', '=', False),
            ('show_in_ui', '=', True)
        ]

    @api.depends('stage_id')
    def _compute_next_stage_ids(self):
        for record in self:
            routes = self.env['incident.stage.route'].search(record._get_next_stage_route_domain())
            record.next_stage_ids = (record.stage_id + routes.mapped('stage_to_id'))

    @api.depends('incident_text')
    def _compute_incident_text_sample(self):
        for incident in self:
            text_list = html2text(incident.incident_text).splitlines()
            result = []
            while len(result) <= REQUEST_TEXT_SAMPLE_MAX_LINES and text_list:
                line = text_list.pop(0)
                line = line.lstrip('#').strip()
                if not line:
                    continue
                result.append(line)
            incident.incident_text_sample = "\n".join(result)

    # @api.depends('user_id')
    # def _compute_instruction_visible(self):
    #     for rec in self:
    #         rec.instruction_visible = (
    #             (
    #                 self.env.user == rec.user_id or
    #                 self.env.user.id == SUPERUSER_ID or
    #                 self.env.user.has_group(
    #                     'isha_ims.group_incident_manager')
    #             ) and (
    #                 rec.instruction_html
    #             )
    #         )

    def _create_update_from_type(self, r_type, vals):
        # Name update
        if vals.get('name') == "###new###":
            # To set correct name for incident generated from mail aliases
            # See code `mail.models.mail_thread.MailThread.message_new` - it
            # attempts to set name if it is empty. So we pass special name in
            # our method overload, and handle it here, to keep all incident name
            # related logic in one place
            vals['name'] = False
        if not vals.get('name') and r_type.sequence_id:
            vals['name'] = r_type.sudo().sequence_id.next_by_id()
        elif not vals.get('name'):
            vals['name'] = self.env['ir.sequence'].sudo().next_by_code(
                'incident.incident.name')

        # Update stage
        if r_type.start_stage_id:
            vals['stage_id'] = r_type.start_stage_id.id
        else:
            raise exceptions.ValidationError(
                _("Cannot create incident of type '%s':"
                  " This type have no start stage defined!") % r_type.name)
        return vals

    @api.model
    def _add_missing_default_values(self, values):
        res = super(IncidentIncident, self)._add_missing_default_values(values)

        if res.get('author_id') and 'partner_id' not in values:
            author = self.env['res.partner'].browse(res['author_id'])
            if author.commercial_partner_id != author:
                res['partner_id'] = author.commercial_partner_id.id

        return res

    # Validate any given phone number
    def validate_phone_number(self, phone_country_field_name, phone_field_name, vals):
        # Getting the latest values if the fields were updated
        updated_phone = vals.get(phone_field_name)
        updated_phone_country_index = vals.get(phone_country_field_name)
        # Using the index stored in the field to retrieve the actual record from its model
        updated_phone_country = self.env['res.country'].browse(
            updated_phone_country_index)
        # Storing the original values of the fields
        original_phone = getattr(self, phone_field_name)
        original_phone_country = getattr(self, phone_country_field_name)
        # Using the updated value and if it doesn't exist, we're using the original value
        phone = updated_phone if phone_field_name in vals else original_phone
        phone_country = updated_phone_country if phone_country_field_name in vals else original_phone_country
        # Getting the ISO code of the country from the phone country
        country_iso_code = phone_country.code
        try:
            phn_number = phonenumbers.parse(
                number=phone, region=country_iso_code, keep_raw_input=True)
            is_valid = phonenumbers.is_valid_number(phn_number)
        except:
            is_valid = False
        if (country_iso_code or phone) and not is_valid:
            return False
        else:
            return True

    def validate_all_phone_fields(self, vals):
        # checking if phone numbers are valid
        agitator_phone_valid = self.validate_phone_number(
            'partner_phone_country', 'partner_phone', vals)
        emergency_phone_valid = self.validate_phone_number(
            'emergency_contact_phone_country', 'emergency_contact_phone', vals)
        reporter_phone_valid = self.validate_phone_number(
            'reporter_phone_country', 'reporter_phone', vals)
        if reporter_phone_valid == False:
            raise exceptions.ValidationError('Reporter Phone is invalid!')
        if agitator_phone_valid == False:
            raise exceptions.ValidationError('Agitator Phone is invalid!')
        if emergency_phone_valid == False:
            raise exceptions.ValidationError('Emergency Phone is invalid!')

    def validate_email(self, vals):
        email_validator = re.compile(
            r'^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$')
        if vals.get('partner_email'):
            if not email_validator.match(vals.get('partner_email')):
                raise exceptions.ValidationError('Contact Email is invalid!')
        if vals.get('emergency_contact_email'):
            if not email_validator.match(vals.get('emergency_contact_email')):
                raise exceptions.ValidationError(
                    'Emergency Contact Email is invalid!')
        if vals.get('reporter_email'):
            if not email_validator.match(vals.get('reporter_email')):
                raise exceptions.ValidationError('Reporter Email is invalid!')

    def strip_html_content(self, string):
        if string:
            string1 = string.replace("&nbsp;", "")
            string2 = string1.replace(" ", "")
            return string2
        else:
            return ""

    def check_mandatory_fields(self, vals):
        # Checking if mandatory HTML fields are filled
        incident_text = vals.get('incident_text') if vals.get(
            'incident_text') else self.incident_text
        stripped_incident_text = self.strip_html_content(incident_text)
        medical_description = vals.get('medical_description') if vals.get(
            'medical_description') else self.medical_description
        has_medical_reasons = vals.get('has_medical_reasons') if vals.get(
            'has_medical_reasons') else self.has_medical_reasons
        stripped_medical_description = self.strip_html_content(
            medical_description)
        if stripped_incident_text == "<p><br></p>" or stripped_incident_text == "<p></p>":
            raise exceptions.Warning("Incident Description cannot be empty!")
        if has_medical_reasons == True and stripped_medical_description == "<p><br></p>" or stripped_medical_description == "<p></p>":
            raise exceptions.Warning("Medical Description cannot be empty!")

    # Overwriting the default Write function (Save/Update button)
    def write(self, vals):
        # validating all phone fields
        self.validate_all_phone_fields(vals)
        # validating email
        self.validate_email(vals)
        # Check if all mandatory fields are filled
        self.check_mandatory_fields(vals)
        self_ctx = self.with_context(mail_create_nolog=False, mail_create_nosubscribe=True)
        if 'stage_id' in vals:
            new_stage = self.env['incident.stage'].search(
                [('id', '=', vals.get('stage_id'))])
            if new_stage.type_id.name == 'Open':
                if ('partner_id' not in vals or vals.get('partner_id') is None) and (self.partner_id.active is False):
                    # both old and new contact are null
                    raise exceptions.Warning(
                        "Agitator has to be linked before moving the Incident to Open state.")
            elif new_stage.type_id.name == 'Approved':
                if ('partner_id' not in vals or vals.get('partner_id') is None) and (self.partner_id.active is False):
                    # both old and new contact are null
                    raise exceptions.Warning(
                        "Agitator has to be linked before moving the Incident to Open state.")
                # by-passing Approved using Open-Freeze route
                stage_freeze = self.env['incident.stage'].search([('name', '=', 'Freeze')])
                vals.update({'stage_id': stage_freeze.id})

                if 'partner_id' in vals and vals.get('partner_id') is not None:
                    # new partner
                    linked_contact = vals.get('partner_id')
                elif self.partner_id and self.partner_id.active is True:
                    # old partner is present.
                    linked_contact = self.partner_id
                star_mark = linked_contact.starmark_category_id
                dna_stay_area = linked_contact.donotallow_stayarea_ids
                dna_program = linked_contact.donotallow_program_ids
                dna_volunteer = linked_contact.donotallow_volunteering_ids
                dna_general = linked_contact.donotallow_general_ids

                dna_stay_count = len(dna_stay_area.ids)
                dna_prog_count = len(dna_program.ids)
                dna_vol_count = len(dna_volunteer.ids)
                dna_gen_count = len(dna_general.ids)

                if star_mark.active is False or (
                        dna_stay_count == 0 and dna_prog_count == 0 and dna_vol_count == 0 and dna_gen_count == 0):
                    raise exceptions.Warning("Decision has to be marked before approving the incident. Please fill "
                                             "the starMark field and at least one of the DoNotAllow fields. Please use the 'Update Star Marking' button")
        # To Update partner photo
        if vals.get('partner_photo'):
            if self.partner_id:
                self.partner_id.image_1920 = vals.get('partner_photo')

        res = super(IncidentIncident, self_ctx).write(vals)
        return res

    def ims_entries_to_remove(self, the_dict):
        entries = ('contact_sm_import', 'contact_sm_date_assign_import', 'contact_sm_date_closed_import',
                   'contact_donotallow_stayarea_ids_import', 'contact_dna_stayarea_comments_import',
                   'contact_donotallow_program_ids_import', 'contact_dna_program_comments',
                   'contact_donotallow_volunteering_ids_import', 'contact_dna_volunteering_comments_import',
                   'contact_donotallow_general_ids_import', 'contact_dna_general_comments_import',
                   'contact_partner_severity_import', 'incident_types_import', 'team_ids_import')

        # d = {'some': 'data'}
        # entriesToRemove = ('any', 'iterable')
        # for k in entriesToRemove:
        #     d.pop(k, None)

        for key in entries:
            if key in the_dict:
                del the_dict[key]

        return the_dict

    def ims_create_contact_dic(self, vals):
        map_contact = {'name': vals.get('partner_name'), 'email': vals.get('partner_email'),
                       'phone_country_code': vals.get('partner_phone_country'), 'phone': vals.get('partner_phone'),

                       'country_id': vals.get('country'), 'zip': vals.get('zip'), 'state_id': vals.get('state'),
                       'city': vals.get('city'), 'street': vals.get('street_1'), 'street2': vals.get('street_2'),
                       'gender': vals.get('gender'), 'dob': vals.get('dob'), 'nationality_id': vals.get('nationality'),

                       'starmark_category_id': vals.get('contact_sm_import'),
                       'incident_date_assigned': vals.get('contact_sm_date_assign_import'),
                       'incident_date_closed': vals.get('contact_sm_date_closed_import'),
                       # 'partner_severity': vals.get('contact_partner_severity_import'),
                       'donotallow_stayarea_ids': vals.get('contact_donotallow_stayarea_ids_import'),
                       'dna_stayarea_comments': vals.get('contact_dna_stayarea_comments_import'),
                       'donotallow_program_ids': vals.get('contact_donotallow_program_ids_import'),
                       'dna_program_comments': vals.get('contact_dna_program_comments'),
                       'donotallow_volunteering_ids': vals.get('contact_donotallow_volunteering_ids_import'),
                       'dna_volunteering_comments': vals.get('contact_dna_volunteering_comments_import'),
                       'donotallow_general_ids': vals.get('contact_donotallow_general_ids_import'),
                       'dna_general_comments': vals.get('contact_dna_general_comments_import')}

        # since we are doing manual creation, odoo's "search and find the Id" won't work
        # so we have to fetch the Id/Ids for many2one/many2many fields

        # if map_contact.get('nationality'):
        #     nationality = self.env['res.country'].search([
        #         ('id', '=', map_contact.get('nationality'))
        #     ])
        #     if nationality:
        #         map_contact.update({
        #             'nationality': nationality.name
        #         })

        if map_contact.get('phone_country_code'):
            phone_country_code = self.env['res.country'].search([
                ('id', '=', map_contact.get('phone_country_code'))
            ])
            if phone_country_code:
                map_contact.update({
                    'phone_country_code': str(phone_country_code.phone_code)
                })


        if map_contact.get('starmark_category_id'):
            starmark_category_id = self.env['starmark.category'].search([
                ('name', '=', map_contact.get('starmark_category_id'))
            ])
            if starmark_category_id:
                map_contact.update({
                    'starmark_category_id': starmark_category_id.id
                })

        dna_list = []
        donotallow_stayarea_ids = []
        if map_contact.get('donotallow_stayarea_ids'):
            temp_list = map_contact.get('donotallow_stayarea_ids').split(',')
            for temp in temp_list:
                dna_list.append(temp.strip())
            for temp in dna_list:
                donotallow_record = self.env['incident.donotallows'].search(
                    [('name', '=', temp)])
                if donotallow_record:
                    donotallow_stayarea_ids.append(donotallow_record.id)

        dna_list = []
        donotallow_program_ids = []
        if map_contact.get('donotallow_program_ids'):
            temp_list = map_contact.get('donotallow_program_ids').split(',')
            for temp in temp_list:
                dna_list.append(temp.strip())
            for temp in dna_list:
                donotallow_record = self.env['incident.donotallows'].search(
                    [('name', '=', temp)])
                if donotallow_record:
                    donotallow_program_ids.append(donotallow_record.id)

        dna_list = []
        donotallow_volunteering_ids = []
        if map_contact.get('donotallow_volunteering_ids'):
            temp_list = map_contact.get(
                'donotallow_volunteering_ids').split(',')
            for temp in temp_list:
                dna_list.append(temp.strip())
            for temp in dna_list:
                donotallow_record = self.env['incident.donotallows'].search(
                    [('name', '=', temp)])
                if donotallow_record:
                    donotallow_volunteering_ids.append(donotallow_record.id)

        dna_list = []
        donotallow_general_ids = []
        if map_contact.get('donotallow_general_ids'):
            temp_list = map_contact.get('donotallow_general_ids').split(',')
            for temp in temp_list:
                dna_list.append(temp.strip())
            for temp in dna_list:
                donotallow_record = self.env['incident.donotallows'].search(
                    [('name', '=', temp)])
                if donotallow_record:
                    donotallow_general_ids.append(donotallow_record.id)

        if donotallow_stayarea_ids:
            map_contact.update({'donotallow_stayarea_ids': [(6, 0, donotallow_stayarea_ids)]})
        else:
            map_contact.pop('donotallow_stayarea_ids')

        if donotallow_program_ids:
            map_contact.update({'donotallow_program_ids': [(6, 0, donotallow_program_ids)]})
        else:
            map_contact.pop('donotallow_program_ids')

        if donotallow_volunteering_ids:
            map_contact.update({'donotallow_volunteering_ids': [(6, 0, donotallow_volunteering_ids)]})
        else:
            map_contact.pop('donotallow_volunteering_ids')

        if donotallow_general_ids:
            map_contact.update({'donotallow_general_ids': [(6, 0, donotallow_general_ids)]})
        else:
            map_contact.pop('donotallow_general_ids')

        return map_contact

    @api.model
    def create(self, vals):
        # validating all phone fields
        self.validate_all_phone_fields(vals)
        # validating email
        self.validate_email(vals)
        # Check if all mandatory fields are filled
        self.check_mandatory_fields(vals)

        # is_data_from_import = 'is_data_from_import'
        # if is_data_from_import in vals:
        if vals.get('is_data_from_import'):

            # Create the contact dictionary
            map_contact = self.ims_create_contact_dic(vals)

            # Field Validations
            # location
            if 'location' in vals and vals.get('location'):
                location = self.env['incident.location'].search([
                    ('id', '=', vals.get('location'))
                ])
                if location.name != 'Others':
                    vals.update({'other_location': ''})
                elif 'other_location' not in vals:
                    raise exceptions.ValidationError('other_location field is MISSING!')
                elif not vals.get('other_location'):
                    raise exceptions.ValidationError('other_location field is invalid!')
            else:
                raise exceptions.ValidationError('location field is Mandatory!!')

            # phone and email id
            phone = vals.get('partner_phone') if 'partner_phone' in vals and vals.get('partner_phone') else False
            email = vals.get('partner_email') if 'partner_email' in vals and vals.get('partner_email') else False
            if not phone and not email:
                raise exceptions.ValidationError('One of the following should be mandatory; contact phone number or email id')

            # incident_types
            if 'incident_types_import' not in vals or not vals.get('incident_types_import'):
                raise exceptions.ValidationError('incident_types_import field is Mandatory!!')
            else:
                incidents_type_list = []
                temp_list = vals.get('incident_types_import').split(',')  # split the text based on comma
                for temp in temp_list:
                    incidents_type_list.append(temp.strip())  # remove white-spaces
                temp_list = []
                for temp in incidents_type_list:  # check if all the values are present in DB/are valid
                    incident_type = self.env['ims.incident.type'].search(
                        [('name', '=', temp)])
                    if incident_type:
                        temp_list.append(incident_type.id)
                if temp_list:  # if list has some valid values
                    vals.update({'incident_types': temp_list})  # updating the actual incident_types field
                else:
                    raise exceptions.ValidationError('incident_types_import field is invalid!!')

            # team_ids
            if 'team_ids_import' not in vals or not vals.get('team_ids_import'):
                raise exceptions.ValidationError('team_ids_import field is Mandatory!!')
            else:
                team_ids_list = []
                temp_list = vals.get('team_ids_import').split(',')  # split the text based on comma
                for temp in temp_list:
                    team_ids_list.append(temp.strip())  # remove white-spaces
                temp_list = []
                for temp in team_ids_list:  # check if all the values are present in DB/are valid
                    team_id = self.env['moderation.team'].search(
                        [('name', '=', temp)])
                    if team_id:
                        temp_list.append(team_id.id)
                if temp_list:  # if list has some valid values
                    vals.update({'team_ids': temp_list})  # updating the actual team_ids field
                else:
                    raise exceptions.ValidationError('team_ids_import field is invalid!!')

            # severity
            if not vals.get('severity'):
                raise exceptions.ValidationError('severity field is invalid!')

            # is_overseas
            nationality = self.env['res.country'].search([('id', '=', vals.get('nationality'))])
            country = self.env['res.country'].search([('id', '=', vals.get('country'))])
            if nationality.name != "India" or country.name != 'India':
                vals.update({'is_overseas': True})
            else:
                vals.update({'is_overseas': False})

            # has_medical_reasons        medical_description
            if vals.get('has_medical_reasons'):
                if not vals.get('medical_description'):
                    raise exceptions.ValidationError('Medical Description cannot be EMPTY!!')

            # starmark_category_id      incident_date_assigned      incident_date_closed
            if not map_contact.get('starmark_category_id'):
                raise exceptions.ValidationError('Contact Star Mark cannot be EMPTY!!')
            if not map_contact.get('incident_date_assigned'):
                raise exceptions.ValidationError('Contact Star Mark Assign Date cannot be EMPTY!!')
            if not map_contact.get('incident_date_closed'):
                raise exceptions.ValidationError('Contact Star Mark Expiry Date cannot be EMPTY!!')

            # at least one of DNAs
            if not map_contact.get('donotallow_stayarea_ids') and \
                    not map_contact.get('donotallow_program_ids') and \
                    not map_contact.get('donotallow_volunteering_ids') and \
                    not map_contact.get('contact_donotallow_general_ids_import'):
                raise exceptions.ValidationError('at least one of the DoNotAllow fields is Mandatory!!')

            # Search/Create Contact
            # contact_record = self.env['res.partner'].search([
            #     ('name', '=', map_contact.get('name')),
            #     ('phone', '=', map_contact.get('phone')),
            #     ('email', '=', map_contact.get('email'))])
            # write_status = True

            # severity_current = int(vals.get('severity'))
            # severity_partner = int(contact_record.partner_severity)
            # if severity_current > severity_partner:
            #     map_contact.update({'partner_severity': str(severity_current)})

            # if contact_record:
            #     write_status = contact_record.write(map_contact)
            # else:
            contact_record = self.env['res.partner'].sudo().create(
                map_contact)

            if contact_record:
                vals.update({'partner_id': contact_record.id})
            else:
                raise exceptions.ValidationError('Agitator Creation/Updation Error!!')

            # Create Incident
            vals = self.ims_entries_to_remove(vals)
            incident = super(IncidentIncident, self).create(vals)
            incident.trigger_event('created')
            return incident

        else:
            if vals.get('name') == "###new###":
                vals['name'] = False
            if not vals.get('name'):
                vals['name'] = self.env['ir.sequence'].sudo().next_by_code(
                    'incident.incident.name')
            # Update date_assigned
            if vals.get('user_id'):
                vals.update({
                    'date_assigned': fields.Datetime.now(),
                })
            if not vals.get('stage_id'):
                start_stage = self.env['incident.stage'].search(
                    [('start_stage', '=', True)])
                if start_stage:
                    vals.update({
                        'stage_id': start_stage.id
                    })
                else:
                    exceptions.Warning(
                        "Incident stages are not configured, please contact the administrator")
            # if vals.get('type_id', False):
            #     r_type = self.env['incident.type'].browse(vals['type_id'])
            #     vals = self._create_update_from_type(r_type, vals)
            if not vals.get('author_id') and vals.get('created_by_id'):
                create_user = self.env['res.users'].sudo().browse(
                    vals['created_by_id'])
                vals.update({
                    'author_id': create_user.partner_id.id,
                })
            # 2 is the odoo bot.
            # vals['partner_id'] = 2

            # To Update partner photo
            if vals.get('partner_photo'):
                partner_id = self.env['res.partner'].sudo().browse(vals['partner_id'])
                if partner_id:
                    partner_id.image_1920 = vals.get('partner_photo')

            self_ctx = self.with_context(mail_create_nolog=False, mail_create_nosubscribe=True)
            incident = super(IncidentIncident, self_ctx).create(vals)
            # incident.trigger_event('created')
            return incident

    @api.constrains('stage_id')
    def _push_starmark_info_topic(self):
        for incident_id in self:
            partner = incident_id.partner_id
            incident_date_closed = partner['incident_date_closed']
            incident_date_assigned = partner['incident_date_assigned']
            maps = self.env['contact.map'].sudo().search_read(
                domain=[('system_id', '=', 61), ('contact_id_fkey', '=', partner.id)],
                fields=['id', 'local_contact_id', 'system_id'], order='id ASC')
            if incident_id.stage_id.type_id.code == 'freeze' and len(maps) > 0:
                msg = {
                    'name': partner.name, 'phone': partner.phone,
                    'phone_country_code': partner.phone_country_code,
                    'phone2': partner.phone2,
                    'phone2_country_code': partner.phone2_country_code,
                    'phone3': partner.phone3,
                    'phone3_country_code': partner.phone3_country_code,
                    'phone4': partner.phone4,
                    'phone4_country_code': partner.phone4_country_code,
                    'email': partner.email, 'email2': partner.email2, 'email3': partner.email3,
                    'program_tags': ",".join([record.name for record in partner.pgm_tag_ids]),
                    'is_starmarked': True if len(partner.approved_incident_by_partner_ids) > 0 else False,
                    'contact_map': maps,
                    'starmark_category': partner['starmark_category_id'].name,
                    'incident_date_closed': str(incident_date_closed) if incident_date_closed else False,
                    'incident_date_assigned': str(incident_date_assigned) if incident_date_assigned else False,
                    'partner_severity': partner['partner_severity'],
                    'message_type': 'ims'
                }
                crm_config = Configuration('CRM')
                kafka_config = Configuration('KAFKA')
                p = Producer({
                    'bootstrap.servers': kafka_config['KAFKA_BOOTSTRAP_SERVERS'],
                    'security.protocol': kafka_config['KAFKA_SECURITY_PROTOCOL'],
                    'ssl.ca.location': kafka_config['KAFKA_SSL_CA_CERT_PATH'],
                    'ssl.certificate.location': kafka_config['KAFKA_SSL_CERT_PATH']
                })

                sulaba_star_mark_topic = crm_config['CRM_KAFKA_PUSH_SULABA_MERGE_RESPONSE']
                record = json.dumps(msg)
                p.produce(sulaba_star_mark_topic, record.encode('utf-8'))
                p.flush(2)

    def _scheduler_update_partner_photo(self):
        _logger.info('isha_ims : res_partner : _scheduler_update_partner_photo : cron job - start')
        for incident_id in self.env['incident.incident'].sudo().search([('partner_photo', '=', True), ]):
            partner_id = self.env['res.partner'].search([
                ('id', '=', incident_id.partner_id.id),
            ])
            # ('image_1920', '=', False)
            partner_id.sudo().write({
                'image_1920': incident_id.partner_photo
            })

    def _get_generic_tracking_fields(self):
        """ Compute list of fields that have to be tracked
        """
        return super(
            IncidentIncident, self
        )._get_generic_tracking_fields() | TRACK_FIELD_CHANGES

    def _preprocess_write_changes(self, changes):
        """ Called before write

            This method may be overridden by other addons to add
            some preprocessing of changes, before write

            :param dict changes: keys are changed field names,
                                 values are tuples (old_value, new_value)
            :rtype: dict
            :return: values to update incident with.
                     These values will be written just after write
        """
        vals = super(IncidentIncident, self)._preprocess_write_changes(changes)

        Route = self.env['incident.stage.route']
        if 'user_id' in changes:
            new_user = changes['user_id'][1]  # (old_user, new_user)
            if new_user:
                vals['date_assigned'] = fields.Datetime.now()
            else:
                vals['date_assigned'] = False

        if 'stage_id' in changes:
            old_stage, new_stage = changes['stage_id']
            route = Route.ensure_route(self, new_stage.id)
            route.hook_before_stage_change(self)
            vals['last_route_id'] = route.id
            vals['date_moved'] = fields.Datetime.now()
            vals['moved_by_id'] = self.env.user.id

            if not old_stage.closed and new_stage.closed:
                vals['date_closed'] = fields.Datetime.now()
                vals['closed_by_id'] = self.env.user.id
            elif old_stage.closed and not new_stage.closed:
                vals['date_closed'] = False
                vals['closed_by_id'] = False

        # if 'type_id' in changes:
        #     raise exceptions.ValidationError(_(
        #         'It is not allowed to change incident type'))
        return vals

    def _manage_followers(self, changes):
        partners_to_add = set()
        if 'stage_id' in changes:
            if self.stage_type_name == 'Report':
                partners_to_add = self._get_advanced_reporters(self.team_ids)
            if self.stage_type_name == 'Open':
                partners_to_add = self._get_moderators(self.team_ids)

        if 'team_ids' in changes:
            current_followers = self.env['mail.followers'].search([('res_id', '=', self.id), ('res_model', '=', 'incident.incident')])
            self.message_unsubscribe([follower.partner_id.id for follower in current_followers])
            new_teams = changes['team_ids'][1]
            if self.stage_type_name != 'Draft' and self.stage_type_name == 'Report':
                partners_to_add = self._get_advanced_reporters(new_teams)
            else:
                partners_to_add = self._get_advanced_reporters(new_teams).union(self._get_moderators(new_teams))
        self.message_subscribe(list(partners_to_add))

    def _postprocess_write_changes(self, changes):
        """ Called after write

            This method may be overridden by other modules to add
            This method may be overridden by other modules to add
            some postprocessing of write.
            This method does not return any  value.

            :param dict changes: keys are changed field names,
                                 values are tuples (old_value, new_value)
            :return: None

        """
        res = super(IncidentIncident, self)._postprocess_write_changes(changes)

        if 'stage_id' in changes:
            self.last_route_id.hook_after_stage_change(self)

            # Trigger stage-change related events
            old_stage, new_stage = changes['stage_id']
            event_data = {
                'route_id': self.last_route_id.id,
                'old_stage_id': old_stage.id,
                'new_stage_id': new_stage.id,
            }

            if new_stage.closed and not old_stage.closed:
                self.trigger_event('closed', event_data)
            elif old_stage.closed and not new_stage.closed:
                self.trigger_event('reopened', event_data)
            else:
                self.trigger_event('stage-changed', event_data)

        self._manage_followers(changes)
        return res

    def _creation_subtype(self):
        return self.env.ref('isha_ims.mt_incident_created')

    def _track_subtype(self, init_values):
        """ Give the subtypes triggered by the changes on the record according
        to values that have been updated.

        :param init_values: the original values of the record;
                            only modified fields are present in the dict
        :type init_values: dict
        :returns: a subtype xml_id or False if no subtype is triggered
        """
        self.ensure_one()
        if 'stage_id' in init_values:
            init_stage = init_values['stage_id']
            if init_stage and init_stage != self.stage_id and \
                    self.stage_id.closed and not init_stage.closed:
                return self.env.ref('isha_ims.mt_incident_closed')
            if init_stage and init_stage != self.stage_id and \
                    not self.stage_id.closed and init_stage.closed:
                return self.env.ref('isha_ims.mt_incident_reopened')
            if init_stage != self.stage_id:
                return self.env.ref('isha_ims.mt_incident_stage_changed')

        return self.env.ref('isha_ims.mt_incident_updated')

    # @api.onchange('type_id')
    # def onchange_type_id(self):
    #     """ Set default stage_id
    #     """
    #     for incident in self:
    #         if incident.type_id and incident.type_id.start_stage_id:
    #             incident.stage_id = incident.type_id.start_stage_id
    #         else:
    #             incident.stage_id = self.env['incident.stage'].browse([])

    #         if incident.type_id and incident.type_id.default_incident_text:
    #             incident.incident_text = incident.type_id.default_incident_text

    # @api.onchange('type_id')
    # def _onchange_category_type(self):
    #     # TODO: rename to `_onchange_type_category`
    #     if self.category_id:
    #         # Clear category if it does not in allowed categs for selected type
    #         # Or if incident type is not selected
    #         if not self.type_id:
    #             self.category_id = False
    #         elif self.category_id not in self.type_id.category_ids:
    #             self.category_id = False

    #     if self.type_id:
    #         return {
    #             'domain': {
    #                 'category_id': [
    #                     ('incident_type_ids', '=', self.type_id.id),
    #                 ],
    #             },
    #         }

    #     return {
    #         'domain': {partner_id
    #             # When type is not selected category could not be selected too
    #             'category_id': expression.FALSE_DOMAIN,
    #         },
    #     }

    @api.onchange('author_id')
    def _onchange_author_id(self):
        for rec in self:
            if rec.author_id:
                rec.partner_id = self.author_id.parent_id
            else:
                rec.partner_id = False

    # exclude_fields = ['stage_id', 'created_by_id', 'date_created']
    exclude_fields = ['stage_id']

    @api.model
    def fields_view_get(self, view_id=None, view_type='form',
                        toolbar=False, submenu=False):
        result = super(IncidentIncident, self).fields_view_get(
            view_id=view_id, view_type=view_type,
            toolbar=toolbar, submenu=submenu)

        # Disabling the import button for users who are not in bulk upload group
        if view_type == 'tree':
            doc = etree.XML(result['arch'])
            user_is_bulk_uploader = self.env.user.has_group(
                'isha_ims.group_ims_bulk_uploader')
            if not user_is_bulk_uploader:
                # nodes = doc.xpath("//tree[@string='Test Tree']")
                for node in doc.xpath("//tree"):
                    node.set('import', 'false')
            result['arch'] = etree.tostring(doc)

        # Make type_id and stage_id readonly for kanban mode.
        # this is required to disable kanban drag-and-drop features for this
        # fields, because changing incident type or incident stage in this way,
        # may lead to broken workflow for incidents (no routes to move incident
        # to next stage)
        if view_type == 'kanban':
            for rfield in KANBAN_READONLY_FIELDS:
                if rfield in result['fields']:
                    result['fields'][rfield]['readonly'] = True
        if view_type == 'form':
            # doc = etree.XML(result['arch'])
            # for node in doc.xpath("//form"):
            #     node.set('edit', "[('stage_type_name','!=','Freeze')]")
            # result['arch'] = etree.tostring(doc)
            if self.env.user.has_group('isha_ims.group_ims_reporter'):
                # Setting dynamic readonly attribute to all fields with domain filters
                # for when basic reporter tries to update an incident in the report stage
                doc = etree.XML(result['arch'])
                for node in doc.xpath("//field"):
                    name = node.get('name')
                    # exclude_fields = ['stage_id','show_agitator_information', 'show_incident_information',
                    #                   'show_emergency_information', 'show_reporter_information']
                    if name not in self.exclude_fields:
                        domain = [('stage_type_name', '!=', False), ('stage_type_name', '!=', 'Draft')]
                        if node.attrib.get('modifiers'):
                            attr = json.loads(node.attrib.get('modifiers'))
                            if attr.get('readonly'):
                                value_readonly = attr.get('readonly')
                                if str(attr.get('readonly')) != "True":
                                    value_readonly.insert(0, "|")
                                    domain = value_readonly + domain
                            attr['readonly'] = domain
                            node.set('modifiers', json.dumps(attr))
                    else:
                        if node.attrib.get('modifiers'):
                            attr = json.loads(node.attrib.get('modifiers'))
                            attr['readonly'] = 0
                            node.set('modifiers', json.dumps(attr))
            elif self.env.user.has_group('isha_ims.group_ims_adv_reporter') and not self.env.user.has_group(
                    'isha_ims.group_ims_moderator'):
                # Setting dynamic readonly attribute to all fields with domain filters
                # for when basic reporter tries to update an incident in the report stage
                doc = etree.XML(result['arch'])
                for node in doc.xpath("//field"):
                    name = node.get('name')
                    # exclude_fields = ['stage_id','show_agitator_information', 'show_incident_information',
                    #                   'show_emergency_information', 'show_reporter_information']
                    if name not in self.exclude_fields:
                        domain = [('stage_type_name', '!=', False), ('stage_type_name', '!=', 'Draft'),
                                  ('stage_type_name', '!=', 'Report')]
                        if node.attrib.get('modifiers'):
                            attr = json.loads(node.attrib.get('modifiers'))
                            if attr.get('readonly'):
                                value_readonly = attr.get('readonly')
                                if str(attr.get('readonly')) != "True":
                                    value_readonly.insert(0, "|")
                                    domain = value_readonly + domain
                            attr['readonly'] = domain
                            node.set('modifiers', json.dumps(attr))
                    else:
                        if node.attrib.get('modifiers'):
                            attr = json.loads(node.attrib.get('modifiers'))
                            attr['readonly'] = 0
                            node.set('modifiers', json.dumps(attr))
            else:
                # Setting dynamic readonly attribute to all fields with domain filters
                doc = etree.XML(result['arch'])
                for node in doc.xpath("//field"):
                    name = node.get('name')
                    # exclude_fields = ['stage_id','show_agitator_information', 'show_incident_information',
                    #                   'show_emergency_information', 'show_reporter_information']
                    if name not in self.exclude_fields:
                        domain = ['|',('stage_type_name', '=', 'Freeze'), ('stage_type_name', '=', 'Rejected')]
                        if node.attrib.get('modifiers'):
                            attr = json.loads(node.attrib.get('modifiers'))
                            if attr.get('readonly'):
                                value_readonly = attr.get('readonly')
                                if str(attr.get('readonly')) != "True":
                                    value_readonly.insert(0, "|")
                                    domain = value_readonly + domain
                            attr['readonly'] = domain
                            node.set('modifiers', json.dumps(attr))
                    else:
                        if node.attrib.get('modifiers'):
                            attr = json.loads(node.attrib.get('modifiers'))
                            attr['readonly'] = 0
                            node.set('modifiers', json.dumps(attr))
            # Making sure that created_by_id and date_created fields are always readonly
            for node in doc.xpath("//field"):
                name = node.get('name')
                # exclude_fields = ['stage_id','show_agitator_information', 'show_incident_information',
                #                   'show_emergency_information', 'show_reporter_information']
                readonly_fields = ['create_date','created_by_id', 'date_moved', 'moved_by_id']
                if name in readonly_fields:
                    if node.attrib.get('modifiers'):
                        attr = json.loads(node.attrib.get('modifiers'))
                        attr['readonly'] = 1
                        node.set('modifiers', json.dumps(attr))
            result['arch'] = etree.tostring(doc)

            # if 'user_ids' in result['fields']:
            #     moderator_ids = []
            #     if self.env.user.has_group('isha_ims.group_ims_adv_reporter') | self.env.user.has_group(
            #             'isha_ims.group_ims_moderator'):
            #         # team_users_list = self.env['res.users'].search(['|', '|', '|', ('team_id', '=', 'OCO'), (
            #         #     'team_id', '=', 'VRO'), ('team_id', '=', 'VCD'), ('team_id', '=', 'HP')])
            #         team_users_list = self.env['res.users'].search([('team_ids', '!=', False)])
            #         group_ims_moderator_ref = self.env.ref(
            #             'isha_ims.group_ims_moderator')
            #         for team_user in team_users_list:
            #             if self.env.user.id != team_user.id and group_ims_moderator_ref in team_user.groups_id:
            #                 moderator_ids.append(team_user.id)
            #     elif self.env.user.has_group('isha_ims.group_ims_reporter'):
            #         moderator_ids = self.env.user.moderator_ids.ids
            #     result['fields']['user_ids']['domain'] = [
            #         ('id', 'in', moderator_ids)]
        return result

    def ensure_can_assign(self):
        self.ensure_one()
        if self.closed:
            raise exceptions.UserError(_(
                "You can not assign this incident (%s), "
                "because this incident is closed."
            ) % self.display_name)
        if not self.can_change_assignee:
            raise exceptions.UserError(_(
                "You can not assign this (%s) incident"
            ) % self.display_name)

    def action_incident_assign(self):
        self.ensure_one()
        self.ensure_can_assign()
        action = self.env.ref('isha_ims.action_incident_wizard_assign')
        action = action.read()[0]
        action['context'] = {
            'default_incident_id': self.id,
        }
        return action

    # Default notifications
    def _send_default_notification__get_email_from(self, **kw):
        """ To be overloaded to change 'email_from' field for notifications
        """
        return 'ims@ishafoundation.org'

    def _send_default_notification__get_context(self, event):
        """ Compute context for default notification
        """
        values = event.get_context()
        values.update({
            'company': self.env.user.company_id,
        })
        return values

    def _send_default_notification__get_msg_params(self, **kw):
        return dict(
            composition_mode='mass_mail',
            auto_delete=True,
            auto_delete_message=False,
            parent_id=False,  # override accidental context defaults
            # subtype_id=self.env.ref('mail.mt_note').id,
            **kw,
        )

    def _send_default_notification__send(self, template, partners,
                                         event, **kw):
        """ Send default notification

            :param str template: XMLID of template to use for notification
            :param Recordset partners: List of partenrs that have to receive
                                       this notification
            :param Recordset event: Single record of 'incident.event'
            :param function lazy_subject: function (self) that have to return
                                          translated string for subject
                                          for notification
        """
        values_g = self._send_default_notification__get_context(event)
        message_data_g = self._send_default_notification__get_msg_params(**kw)
        try:
            ims_config = isha_base_importer.Configuration('IMS')
            email_from = ims_config['IMS_EMAIL_FROM']
        except:
            email_from = self._send_default_notification__get_email_from(**kw)
        if email_from:
            message_data_g['email_from'] = email_from

        # In order to handle translatable subjects, we use specific argument:
        # lazy_subject, which is function that receives 'self' and returns
        # string.
        lazy_subject = message_data_g.pop('lazy_subject', None)

        # for partner in partners.sudo():
        # Skip partners without emails to avoid errors
        # if not partner.email:
        #     continue
        # values = dict(
        #     values_g,
        #     partner=partner,
        # )
        self_ctx = self.sudo()
        # if partner.lang:
        #     self_ctx = self_ctx.with_context(lang=partner.sudo().lang)
        message_data = dict(
            message_data_g,
            partner_ids=[(4, partner.id) for partner in partners if partner.email],
            values=dict(values_g, partner=self.sudo().author_id))
        if lazy_subject:
            message_data['subject'] = lazy_subject(self_ctx)
        self_ctx.message_post_with_view(
            template,
            **message_data)

    def _send_default_notification_created(self, event):
        # if not self.sudo().type_id.send_default_created_notification:
        #     return

        self._send_default_notification__send(
            'isha_ims.message_incident_created__author',
            self.sudo().author_id,
            event,
            lazy_subject=lambda self: _(
                "Incident %s successfully created!") % self.name,
        )

    def _send_default_notification_assigned(self, event):
        # if not self.sudo().type_id.send_default_assigned_notification:
        #     return

        self._send_default_notification__send(
            'isha_ims.message_incident_assigned__assignee',
            event.sudo().new_user_id.partner_id,
            event,
            lazy_subject=lambda self: _(
                "You have been assigned to incident %s!") % self.name,
        )

    def _send_default_notification_closed(self, event):
        # if not self.sudo().type_id.send_default_closed_notification:
        #     return

        self._send_default_notification__send(
            'isha_ims.message_incident_closed__author',
            self.sudo().author_id,
            event,
            lazy_subject=lambda self: _(
                "Your incident %s has been closed!") % self.name,
        )

    def _send_default_notification_reopened(self, event):
        # if not self.sudo().type_id.send_default_reopened_notification:
        #     return

        self._send_default_notification__send(
            'isha_ims.message_incident_reopened__author',
            self.sudo().author_id,
            event,
            lazy_subject=lambda self: _(
                "Your incident %s has been reopened!") % self.name,
        )

    def _send_default_notification_stage_changed(self, event):
        # if not self.sudo().type_id.send_default_stage_changed_notification:
        #     return

        send_list = set()
        # new_stage = self.env['incident.stage'].search([('id', '=', event.new_stage_id)])
        new_stage = event.new_stage_id
        if new_stage.type_id.name == 'Report':
            # incident = self.env['incident.incident'].search([('id', '=', event.incident_id)])
            send_list.add(self.sudo().author_id)
            incident = event.incident_id
            if incident.team_ids:
                for team in incident.team_ids:
                    if team.users:
                        for user in team.users:
                            if user.has_group('isha_ims.group_ims_adv_reporter'):
                                send_list.add(user.partner_id)
        elif new_stage.type_id.name == 'Open' or new_stage.type_id.name == 'Freeze' or \
                new_stage.type_id.name == 'Unfreeze' or new_stage.type_id.name == 'Rejected':
            incident = event.incident_id
            # collect all moderators of the same team as that of this incident.
            if incident.team_ids:
                for team in incident.team_ids:
                    if team.users:
                        for user in team.users:
                            if user.has_group('isha_ims.group_ims_moderator'):
                                send_list.add(user.partner_id)
            # collect all the moderators directly assigned to this incident.
            # if incident.user_ids:
            #     for moderator in incident.user_ids:
            #         send_list.add(moderator.partner_id)
            if new_stage.type_id.name == 'Open':
                # collect the current user who opened this incident.
                send_list.add(self.env.user.partner_id)

        # send email notification for all the collected users.
        # for partner in send_list:
        if len(send_list) > 0:
            self._send_default_notification__send(
                'isha_ims.message_incident_stage_changed__author',
                send_list,
                event,
                lazy_subject=lambda self: _(
                    "Incident on %s has changed its state") % self.partner_name
            )

    def handle_incident_event(self, event):
        """ Place to handle incident events
        """
        if event.event_type_id.code in ('assigned', 'reassigned'):
            self._send_default_notification_assigned(event)
        elif event.event_type_id.code == 'created':
            self._send_default_notification_created(event)
        elif event.event_type_id.code == 'closed':
            self._send_default_notification_closed(event)
        elif event.event_type_id.code == 'reopened':
            self._send_default_notification_reopened(event)
        elif event.event_type_id.code == 'stage-changed':
            self._send_default_notification_stage_changed(event)

    def trigger_event(self, event_type, event_data=None):
        """ Trigger an event.

            :param str event_type: code of event type
            :param dict event_data: dictionary with data to be written to event
        """
        event_type_id = self.env['incident.event.type'].get_event_type_id(
            event_type)
        event_data = event_data if event_data is not None else {}
        event_data.update({
            'event_type_id': event_type_id,
            'incident_id': self.id,
            'user_id': self.env.user.id,
            'date': fields.Datetime.now(),
        })
        event = self.env['incident.event'].sudo().create(event_data)
        self.handle_incident_event(event)

    def get_mail_url(self):
        """ Get incident URL to be used in mails
        """
        url = "/web#id=%s&model=incident.incident"
        menu = self.env['ir.ui.menu'].search([('name', 'ilike', 'Isha IMS')])
        if menu:
            menu_id = menu[0].id
            url += "&menu_id=%s"
        return url % (self.id, menu_id)

    def _notify_get_groups(self):
        """ Use custom url for *button_access* in notification emails
        """
        self.ensure_one()
        groups = super(IncidentIncident, self)._notify_get_groups()

        view_title = _('View Incident')
        access_link = self.get_mail_url()

        # pylint: disable=unused-variable
        for group_name, group_method, group_data in groups:
            group_data['button_access'] = {
                'title': view_title,
                'url': access_link,
            }

        return groups

    # def _message_auto_subscribe_followers(self, updated_values,
    #                                       default_subtype_ids):
    #     res = super(IncidentIncident, self)._message_auto_subscribe_followers(
    #         updated_values, default_subtype_ids)

    #     followers = set()
    #     stage = updated_values.get('stage_id')
    #     if stage is not None:
    #         if self.stage_type_name == 'Report':
    #             followers = self._get_advanced_reporters(self.team_ids)
    #         if self.stage_type_name == 'Open':
    #             followers = self._get_moderators(self.team_ids)

    #     for follower in followers:
    #         res.append((follower.id, default_subtype_ids, False))
    #     return res

    def _message_auto_subscribe_notify(self, partner_ids, template):
        # Disable sending mail to assigne, when incident was assigned.
        # See _postprocess_write_changes
        return super(
            IncidentIncident,
            self.with_context(mail_auto_subscribe_no_notify=True)
        )._message_auto_subscribe_notify(partner_ids, template)

    @api.model
    def message_new(self, msg, custom_values=None):
        """ Overrides mail_thread message_new that is called by the mailgateway
            through message_process.
            This override updates the document according to the email.
        """
        custom_values = custom_values if custom_values is not None else {}

        # Concatenate subject and body for incident text
        incident_text_tmpl = "<h1>%(subject)s</h1>\n<br/>\n<br/>%(body)s"
        incident_text = incident_text_tmpl % {
            'subject': msg.get('subject', _("No Subject")),
            'body': msg.get('body', '')
        }

        # Compute defaults
        defaults = {
            'incident_text': incident_text,
            'name': "###new###",  # Special name to avoid
            # placing subject to as name
        }

        # Update defaults with partner and created_by_id if possible
        author_id = msg.get('author_id')
        if author_id:
            author = self.env['res.partner'].browse(author_id)
            defaults['partner_id'] = author.commercial_partner_id.id
            if len(author.user_ids) == 1:
                defaults['created_by_id'] = author.user_ids[0].id
            else:
                defaults['author_id'] = author.id
        else:
            author = False

        defaults.update(custom_values)

        incident = super(IncidentIncident, self).message_new(
            msg, custom_values=defaults)

        # Find partners from email and subscribe them
        email_list = tools.email_split(
            (msg.get('to') or '') + ',' + (msg.get('cc') or ''))
        partner_ids = incident._mail_find_partner_from_emails(
            email_list, force_create=False)
        partner_ids = [pid for pid in partner_ids if pid]

        if author:
            partner_ids += [author.id]

        incident.message_subscribe(partner_ids)
        return incident

    def action_show_incident_events(self):
        self.ensure_one()
        action = self.env.ref(
            'isha_ims.action_incident_event_view').read()[0]
        action['domain'] = [('incident_id', '=', self.id)]
        return action

    def contact_info(self):
        self.ensure_one()
        return {
            'name': 'Agitator',  # Label
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'res.partner',  # your model
            'target': 'new',  # if you want popup
            # 'context': ctx, # if you need
            'res_id': self.partner_id.id
        }

    # def _update_category_if_expired(self):
    #     incidents = self.search(
    #         [('stage_type_name', 'in', ['Approved', 'Freeze', 'Unfreeze'])])

    #     if incidents:
    #         for incident in incidents:
    #             if incident.partner_id:
    #                 if incident.partner_id.incident_date_closed < fields.Datetime.now():
    #                     # get all moderators for the contact from all its incidents.
    #                     all_contact_incidents = self.search(
    #                         [('partner_id', '=', incident.partner_id.id)])
    #                     moderators = set()
    #                     if all_contact_incidents:
    #                         for inc in all_contact_incidents:
    #                             moderators |= self._get_moderators(inc)

    #                     current_category = incident.partner_id.starmark_category_id
    #                     next_category = current_category.transition_to_id
    #                     # transition the starmark category
    #                     if next_category:
    #                         incident.partner_id.starmark_category_id = next_category
    #                         template = 'isha_ims.message_contact_star_mark_changed'
    #                         subject = 'The contact %s has transitioned to a new star mark category.'
    #                         event_type_id = self.env['incident.event.type'].get_event_type_id('starmark-updated')
    #                     else:
    #                         template = 'isha_ims.message_contact_star_mark_expired'
    #                         subject = 'The contact %s has star mark category which has expired.'
    #                         event_type_id = self.env['incident.event.type'].get_event_type_id('starmark-expired')
    #                         # send mail saying category has expired.

    #                     event_data = ({
    #                         'event_type_id': event_type_id,
    #                         'partner_id': incident.parter_id.id,
    #                         'user_id': self.env.user.id,
    #                         'date': fields.Datetime.now(),
    #                     })
    #                     event = self.env['res.partner.event'].sudo().create(event_data)
    #                     # send emails to all the moderators.
    #                     for partner in moderators:
    #                         self._send_default_notification__send(
    #                             template,
    #                             partner,
    #                             event,
    #                             lazy_subject=lambda self: _(
    #                                 subject) % incident.partner_id.name
    #                         )

    # def _get_moderators(self, incident):
    #     users_set = set()
    #     users_set = self._get_moderators_from_team_ids(incident.team_ids)
    #     # collect all the moderators directly assigned to this incident.
    #     if incident.user_ids:
    #         for moderator in incident.user_ids:
    #             users_set.add(moderator.partner_id)
    #     return users_set

    def _get_advanced_reporters(self, team_ids):
        users_set = set()
        if team_ids:
            for team in team_ids:
                if team.users:
                    for user in team.users:
                        if user.has_group('isha_ims.group_ims_adv_reporter'):
                            users_set.add(user.partner_id.id)
        return users_set

    def _get_moderators(self, team_ids):
        users_set = set()
        if team_ids:
            for team in team_ids:
                if team.users:
                    for user in team.users:
                        if user.has_group('isha_ims.group_ims_moderator'):
                            users_set.add(user.partner_id.id)
        return users_set

    exportable_fields = ['date_created', 'partner_id', 'partner_name', 'partner_phone', 'partner_email',
                         'person_type_id', 'govt_id_type', 'govt_issued_id',
                         'isha_id_type', 'isha_issued_id', 'has_medical_reasons', 'medical_description', 'location',
                         'department_id', 'incident_type', 'incident_text', 'emergency_contact_name',
                         'emergency_contact_relation', 'emergency_contact_email', 'emergency_contact_phone',
                         'reporter_name', 'reporter_email', 'reporter_phone', 'name',
                         'created_by_id', 'stage_id', 'incident_types', 'severity', 'team_ids']

    @api.model
    def fields_get(self, allfields=None, attributes=None):
        res = super(IncidentIncident, self).fields_get(allfields, attributes=attributes)
        non_exportable_fields = set(res.keys()) - set(self.exportable_fields)
        for field in non_exportable_fields:
            res[field]['exportable'] = False  # to hide in export list
            res[field]['searchable'] = False
            res[field]['sortable'] = False
        return res
