from odoo import models, fields, api


class IncidentCategory(models.Model):
    _name = "incident.category"
    _inherit = [
        'generic.mixin.parent.names',
        'mail.thread',
        'generic.mixin.name_with_code',
    ]
    _description = "Incident Category"
    _order = 'sequence, name'

    _parent_name = "parent_id"
    _parent_store = True
    _parent_order = 'name'

    # Defined in generic.mixin.name_with_code
    name = fields.Char()
    code = fields.Char()
    parent_id = fields.Many2one(
        'incident.category', 'Parent', index=True, ondelete='cascade')
    parent_path = fields.Char(index=True)

    active = fields.Boolean(default=True, index=True)

    description = fields.Text(translate=True)
    help_html = fields.Html(translate=True)

    # Stat
    incident_ids = fields.One2many(
        'incident.incident', 'category_id', 'Incidents', readonly=True)
    incident_count = fields.Integer(
        'All Incidents', compute='_compute_incident_count', readonly=True)
    incident_open_count = fields.Integer(
        compute="_compute_incident_count", readonly=True,
        string="Open Incidents")
    incident_closed_count = fields.Integer(
        compute="_compute_incident_count", readonly=True,
        string="Closed Incidents")

    incident_type_ids = fields.Many2many(
        'incident.type',
        'incident_type_category_rel', 'category_id', 'type_id',
        string="Incident types")
    incident_type_count = fields.Integer(
        compute='_compute_incident_type_count')
    sequence = fields.Integer(index=True, default=5)
    color = fields.Integer()  # for many2many_tags widget
    # below is the custom code added by vijay
    star_mark_category = fields.Selection([('amber', 'Amber'), ('black', 'Black'), ('red', 'Red'), ('white', 'White'),
                                           ('yellow', 'Yellow')], string='Category')
    time_limit = fields.Float(string='Time Limit')
    transition_to = fields.Selection([('amber', 'Amber'), ('black', 'Black'), ('red', 'Red'), ('white', 'White'),
                                           ('yellow', 'Yellow')], string='Transition To')
    can_moderator_assign = fields.Boolean(string="Can Moderator Assign")


    _sql_constraints = [
        ('name_uniq',
         'UNIQUE (parent_id, name)',
         'Category name must be unique.'),
    ]

    @api.depends('incident_ids')
    def _compute_incident_count(self):
        IncidentIncident = self.env['incident.incident']
        for record in self:
            record.incident_count = len(record.incident_ids)
            record.incident_closed_count = IncidentIncident.search_count([
                ('closed', '=', True),
                ('category_id', '=', record.id)
            ])
            record.incident_open_count = IncidentIncident.search_count([
                ('closed', '=', False),
                ('category_id', '=', record.id)
            ])

    @api.depends('incident_type_ids')
    def _compute_incident_type_count(self):
        for record in self:
            record.incident_type_count = len(record.incident_type_ids)

    def name_get(self):
        # This is required to avoid access rights errors when tracking values
        # in chatter. (At least in Odoo 10.0)
        return super(IncidentCategory, self.sudo()).name_get()
