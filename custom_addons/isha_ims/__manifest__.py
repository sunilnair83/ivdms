{
    'name': "Isha IMS",
    'sequence': -1,
    'summary': """
        Incident Management System - logging, recording,
        tracking, addressing, handling and archiving
        issues that occur in daily routine.
    """,

    'author': "Isha IT Team",
    'website': "https://ishafoundation.org",
    'category': 'Isha Incident',
    'version': '13.0.1.25.6',
    'external_dependencies': {
        'python': [
            'html2text',
        ],
    },

    # any module necessary for this one to work correctly
    'depends': [
        'base',
        'mail',
        'generic_mixin',
        'crnd_web_diagram_plus',
        'crnd_web_list_popover_widget',
        'base_setup',
        'backend_theme_v13',
        'isha_crm',
        'muk_rest'
    ],

    # always loaded
    'data': [
        'security/security.xml',
        # 'data/users.xml',
        'security/ir.model.access.csv',
        'views/assets.xml',
        'data/incident_sequence.xml',
        'data/mail_subtype.xml',
        'data/incident_stage_type.xml',
        'data/incident_event_type.xml',
        'data/ir_cron.xml',
        'data/moderation_teams_master_data.xml',
        'data/incident_master_data.xml',
        'data/incident_stage_type.xml',
        'data/stages_master_data.xml',
        'data/stage_route_master_data.xml',
        'views/templates.xml',
        'views/incident_views.xml',
        'views/res_partner_view.xml',
        'views/res_config_settings_view.xml',
        'views/incident_category_view.xml',
        # 'views/incident_type_view.xml',
        'views/incident_stage_route_view.xml',
        'views/incident_stage_view.xml',
        'views/incident_stage_type_view.xml',
        'views/incident_incident_view.xml',
        'views/res_users.xml',
        'views/mail_templates.xml',
        'views/incident_event.xml',
        'views/incident_master.xml',
        'views/access_rights_ims.xml',
        # 'wizard/incident_wizard_close_views.xml',
        'wizard/incident_wizard_assign.xml',
        'views/custom_js_scripts.xml',
        'data/mail_templates.xml',
        'views/partner_import_temp.xml',
    ],

    # 'qweb': [
    #     'static/src/xml/dashboard.xml'],

    'demo': [
        # 'demo/incident_demo_users.xml',
        # 'demo/incident_category_demo.xml',
        # 'demo/incident_type_simple.xml',
        # 'demo/incident_type_seq.xml',
        # 'demo/incident_type_access.xml',
        # 'demo/incident_type_non_ascii.xml',
        # 'demo/incident_mail_activity.xml',
    ],

    'images': ['static/description/banner.gif'],
    'installable': True,
    'application': True,
    'license': 'OPL-1',
}
