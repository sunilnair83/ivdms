def migrate(cr, version):
    cr.execute("""
    INSERT INTO incident_type_category_rel
        (type_id, category_id)
    SELECT id,
           category_id
        FROM incident_type AS RT
        WHERE NOT EXISTS
            (SELECT type_id, category_id
                FROM incident_type_category_rel
                WHERE type_id = RT.id AND category_id = RT.category_id)
    """)
    cr.execute("""
        ALTER TABLE incident_type DROP COLUMN category_id;
    """)
