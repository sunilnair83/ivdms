def migrate(cr, installed_version):
    cr.execute("""
        UPDATE ir_model_data
        SET module = 'isha_ims'
        WHERE module = 'isha_ims_stage_type'
          AND name IN (
            'incident_stage_type_draft',
            'incident_stage_type_sent',
            'incident_stage_type_cancel',
            'incident_stage_type_closed_ok',
            'incident_stage_type_closed_fail',
            'incident_stage_type_pause',
            'model_incident_stage_type');

        UPDATE ir_model_constraint
        SET module = (
               SELECT id FROM ir_module_module
               WHERE name = 'isha_ims')
        WHERE module IN (
               SELECT id FROM ir_module_module
               WHERE name IN (
                'isha_ims_stage_type',
                'isha_ims_stage_type_sla')
        );

        UPDATE ir_model_data
        SET module = 'isha_ims'
        WHERE module = 'isha_ims_stage_type'
          AND model IN (
                 'ir.model.fields',
                 'ir.model.constraint',
                 'ir.model.relation',
                 'ir.ui.menu',
                 'ir.model.access',
                 'ir.actions.act_window');

        -- Migrate isha_ims_sla_log related
        UPDATE ir_model_data
        SET module = 'isha_ims_sla_log'
        WHERE module = 'isha_ims_stage_type_sla'
          AND name = 'field_incident_sla_log_stage_type_id';

        -- Delete views
        DELETE FROM ir_ui_view WHERE id IN (
            SELECT res_id
            FROM ir_model_data
            WHERE model = 'ir.ui.view'
              AND module IN (
                    'isha_ims_stage_type_sla',
                    'isha_ims_stage_type')
        );

        -- DELETE references to ir_model
        DELETE FROM ir_model_data
        WHERE model = 'ir.model'
          AND module IN (
                'isha_ims_stage_type_sla',
                'isha_ims_stage_type');

        -- DELETE removed modules from database
        DELETE FROM ir_module_module WHERE name IN (
                'isha_ims_stage_type_sla',
                'isha_ims_stage_type');
    """)
