# -*- coding: utf-8 -*-

XMLIDS_TO_MIGRATE = (
    'incident_type_incident',
    'incident_stage_type_incident_draft',
    'incident_stage_type_incident_new',
    'incident_stage_type_incident_classification',
    'incident_stage_type_incident_progress',
    'incident_stage_type_incident_done',
    'incident_stage_type_incident_rejected',
    'incident_stage_route_type_incident_draft_to_new',
    'incident_stage_route_type_incident_new_to_classification',
    'incident_stage_route_type_incident_classification_to_rejected',
    'incident_stage_route_type_incident_classification_to_progress',
    'incident_stage_route_type_incident_progress_to_done',
)


def migrate(cr, version):
    cr.execute("""
        SELECT EXISTS (
            SELECT 1 FROM ir_module_module
            WHERE name = 'service_desk'
              AND state IN ('installed', 'to install', 'to upgrade')
        )
    """)
    is_service_desk_installed = cr.fetchone()[0]

    if is_service_desk_installed:
        cr.execute("""
            UPDATE ir_model_data
            SET module = 'service_desk'
            WHERE name IN %(xmlids)s
            AND module = 'isha_ims'
        """, {'xmlids': XMLIDS_TO_MIGRATE})
