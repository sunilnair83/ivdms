// /**
//  * List of constants used in the logic in this file
//  * Make sure that these constants are kept up to date. 
//  * Otherwise the functionalities written here may not function as intended.
//  */
// const constants = {
//     moduleName: "Isha IMS",
//     partnerModelIdentifier: "model=res.partner",
//     incidentModelIdentifier: "model=incident.incident",
//     moduleBrandClassName: '.o_menu_brand',
//     leftControlPanelClassName: '.o_cp_left',
//     editButtonClassName: '.o_form_button_edit'
// }

// /**
//  * This function takes a callback function as argument and triggers the callback function any time that the HTML of the page changes.
//  * @param {Callback Function} callback 
//  */
// const PageChangedTrigger = (callback) => {
//     let target = document.querySelector('body')
//     // create an observer instance
//     let observer = new MutationObserver(function (mutations) {
//         callback()
//     });
//     // configuration of the observer:
//     let config = { attributes: true, childList: true, characterData: true };
//     // pass in the target node, as well as the observer options
//     observer.observe(target, config);       
// }


// /**
//  * This function will execute custom logic to hide/show control panel buttons for specific forms in the IMS app
//  */
// odoo.define('isha_ims.custom_hide_buttons_logic', function (require) {
//     var FormView = require('web.FormView');
//     let mainInterval;
//     let wait = 1;
//     let thisIsIms;
//     let currentUrl = window.location.href
//     let insidePartnerModel = currentUrl.includes(constants.partnerModelIdentifier)
//     const CheckIfIMS = () => {
//         let moduleName = $(constants.moduleBrandClassName).text();
//         if (moduleName) {
//             if (moduleName === constants.moduleName) {
//                 thisIsIms = true;
//                 currentUrl = window.location.href
//                 insidePartnerModel = currentUrl.includes(constants.partnerModelIdentifier)
//             } else {
//                 thisIsIms = false;
//             }
//             if (thisIsIms && insidePartnerModel) {
//                 // If this in IMS module and we're inside the partner model
//                 $(constants.leftControlPanelClassName).css({ 'visibility': 'hidden' });
//             } else {
//                 // If we're not in IMS or not in partner model, resetting the CSS change
//                 $(constants.leftControlPanelClassName).css({ 'visibility': '' });
//             }
//             clearInterval(mainInterval)
//         }
//     }
//     $(document).ready(function () {
//         PageChangedTrigger(setTheInterval)
//     });
//     setTheInterval = () => {
//         clearInterval(mainInterval)
//         mainInterval = setInterval(CheckIfIMS, wait);
//     }

//     setTheInterval();
//     FormView.include({
//         init: function () {
//             this._super.apply(this, arguments);
//             let interval; // To store the setInterval function. Useful if we need to clear that interval
//             let wait = 1; // Wait time for the setInterval.
//             let insideIncidentForm = currentUrl.includes(constants.incidentModelIdentifier) && currentUrl.includes('view_type=form')
//             $(document).ready(function () {
//                 PageChangedTrigger(hashchanged)
         
//             });
//             /**
//              * This will check if thee current page is of the incident form
//              */
//             const runTheCustomScript = () => {
//                 currentUrl = window.location.href
//                 insideIncidentForm = currentUrl.includes(constants.incidentModelIdentifier) && currentUrl.includes('view_type=form')
//                 // If the current page belongs to the incident or res.partner model
//                 return insideIncidentForm
//             }
//             /**
//              * #1 Hiding the Edit button if the incident is in the Freeze stage
//              * This is the actual method that checks if the page contains incident form with an incident in Freeze stage.
//              * If it does, then it will hide the Edit button via CSS manipulation.
//              * ***NOTE***: The user can still see the Edit button by manipulating the CSS manually. So this shouldn't be used as a security measure.
//              */
//             const customShowHideLogic = () => {
//                 if(thisIsIms === true) {
//                     // Running this code only if this is the IMS app
//                     if (insideIncidentForm) {
//                         // If we're inside the incident model...
//                         if (this.fieldsInfo.form) {
//                             if (this.fieldsInfo.form.partner_name.modifiersValue) {
//                                 if (this.fieldsInfo.form.partner_name.modifiersValue.readonly === true) {
//                                     $(constants.editButtonClassName).css({ 'display': 'none' });
//                                 } else {
//                                     $(constants.editButtonClassName).css({ 'display': '' });
//                                 }
//                                 // Clearing the interval as soon as we set the CSS value on the edit button once
//                                 clearInterval(interval)
//                             }
//                         }
//                     } 
//                 } else {
//                     // Clearing the interval if this is not the IMS app
//                     clearInterval(interval)
//                 }
//             }

//             /**
//              * Function to start the setInterval
//              */
//             const setTheInterval = () => {
//                 interval = setInterval(() => {
//                     customShowHideLogic()
//                 }, wait)
//             }

//             /**
//              * This is run for the first time if page is hard refreshed while on the incident form
//              */
//             setTimeout(() => {
//                 customShowHideLogic();
//             }, 1000)

//             /**
//              * If page reloads, we check if the current page is of incident form, if yes, then we set the Interval
//              */
//             function hashchanged() {
//                 clearInterval(interval)
//                 if (runTheCustomScript()) {
//                     setTheInterval()
//                 }
//             }
//         },
//     });
// });


odoo.define('isha_ims.custom_stage_confirm', function (require) {
    "use strict";

    //require the module to modify:
    var relational_fields = require('web.relational_fields');

    var core = require('web.core');
    var Dialog = require('web.Dialog');

    var _t = core._t;

    //------------------------------------------------------------------------------
    // Many2one widgets
    //------------------------------------------------------------------------------

    var M2ODialog = Dialog.extend({
        template: "M2ODialog",
        init: function (parent, name, value) {
            this.name = name;
            this.value = value;
            this._super(parent, {
                title: _.str.sprintf(_t("Create a %s"), this.name),
                size: 'medium',
                buttons: [{
                    text: _t('Create'),
                    classes: 'btn-primary',
                    click: function () {
                        if (this.$("input").val() !== '') {
                            this.trigger_up('quick_create', { value: this.$('input').val() });
                            this.close(true);
                        } else {
                            this.$("input").focus();
                        }
                    },
                }, {
                    text: _t('Create and edit'),
                    classes: 'btn-primary',
                    close: true,
                    click: function () {
                        this.trigger_up('search_create_popup', {
                            view_type: 'form',
                            value: this.$('input').val(),
                        });
                    },
                }, {
                    text: _t('Cancel'),
                    close: true,
                }],
            });
        },
        start: function () {
            this.$("p").text(_.str.sprintf(_t("You are creating a new %s, are you sure it does not exist yet?"), this.name));
            this.$("input").val(this.value);
        },
        /**
         * @override
         * @param {boolean} isSet
         */
        close: function (isSet) {
            this.isSet = isSet;
            this._super.apply(this, arguments);
        },
        /**
         * @override
         */
        destroy: function () {
            if (!this.isSet) {
                this.trigger_up('closed_unset');
            }
            this._super.apply(this, arguments);
        },
    });

    //override the method:
    relational_fields.FieldStatus.include({
        /**
         * Called when on status stage is clicked -> sets the field value.
         * @private
         * @param {MouseEvent} e
         */
        _onClickStage: function (e) {
            var self = this;
            console.log('from onClickStage', e)
            Dialog.confirm(this, _t("Are you sure you want to change the stage to '" + e.currentTarget.innerText + "'?"), {
                confirm_callback: function () {
                    self._setValue($(e.currentTarget).data("value"));
                },
            });

        },
    });

    relational_fields.FieldMany2One.include({
        /**
         * 
         * Overridden the _search function to display 'Search More...' always in Many2One or Many2Many fields.
         * In the form view set search_more option as true
         * e.g. <field name="field_name" options="{'search_more': True}"/>
         * 
         * 
         * Executes a name_search and process its result.
         *
         * @override
         * @private
         * @param {string} search_val
         * @returns {Promise}
         */
        _search: function (search_val) {
            var self = this;
            var def = new Promise(function (resolve, reject) {
                var context = self.record.getContext(self.recordParams);
                var domain = self.record.getDomain(self.recordParams);

                // Add the additionalContext
                _.extend(context, self.additionalContext);

                var blacklisted_ids = self._getSearchBlacklist();
                if (blacklisted_ids.length > 0) {
                    domain.push(['id', 'not in', blacklisted_ids]);
                }

                self._rpc({
                    model: self.field.relation,
                    method: "name_search",
                    kwargs: {
                        name: search_val,
                        args: domain,
                        operator: "ilike",
                        limit: self.limit + 1,
                        context: context,
                    }
                }).then(function (result) {
                    // possible selections for the m2o
                    var values = _.map(result, function (x) {
                        x[1] = self._getDisplayName(x[1]);
                        return {
                            label: _.str.escapeHTML(x[1].trim()) || data.noDisplayContent,
                            value: x[1],
                            name: x[1],
                            id: x[0],
                        };
                    });

                    // search more... if more results than limit
                    if (self.nodeOptions.ext_force_search_more) {
                        values = self._manageSearchMore(values, search_val, domain, context);
                    } else if (values.length > self.limit) {
                        values = self._manageSearchMore(values, search_val, domain, context);
                    }
                    var create_enabled = self.can_create && !self.nodeOptions.no_create;
                    // quick create
                    var raw_result = _.map(result, function (x) { return x[1]; });
                    if (create_enabled && !self.nodeOptions.no_quick_create &&
                        search_val.length > 0 && !_.contains(raw_result, search_val)) {
                        var ext_no_create_opt = self.nodeOptions.ext_no_create_opt
                        if (ext_no_create_opt) {
                            values.push({
                                // label: _.str.sprintf(_t('Create "<strong>%s</strong>"'),
                                //     $('<span />').text(search_val).html()),
                                action: self._quickCreate.bind(self, search_val),
                                classname: 'o_m2o_dropdown_option'
                            });
                        } else {
                            values.push({
                                label: _.str.sprintf(_t('Create "<strong>%s</strong>"'),
                                    $('<span />').text(search_val).html()),
                                action: self._quickCreate.bind(self, search_val),
                                classname: 'o_m2o_dropdown_option'
                            });
                        }
                    }
                    // create and edit ...
                    if (create_enabled && !self.nodeOptions.no_create_edit) {
                        var createAndEditAction = function () {
                            // Clear the value in case the user clicks on discard
                            self.$('input').val('');
                            return self._searchCreatePopup("form", false, self._createContext(search_val));
                        };
                        values.push({
                            label: _t("Create and Edit..."),
                            action: createAndEditAction,
                            classname: 'o_m2o_dropdown_option',
                        });
                    } else if (values.length === 0) {
                        values.push({
                            label: _t("No results to show..."),
                        });
                    }

                    resolve(values);
                });
            });
            this.orderer.add(def);
            return def;
        },

        /**
         * @override
         * @private
         */
        _onInputFocusout: function () {
            var ext_m2o_dialog = this.nodeOptions.ext_m2o_dialog
            if (ext_m2o_dialog && this.can_create && this.floating) {
                new M2ODialog(this, this.string, this.$input.val()).open();
            }
        },
        /**
         * Prepares and returns options for SelectCreateDialog
         *@override
         * @private
         */
        _getSearchCreatePopupOptions: function (view, ids, context, dynamicFilters) {
            var self = this;
            let search_label = "Search: ";
            let custom_string = "";
            // custom title on clicking search more on partner_id in Incident model
            if (view === 'search' && self.model === 'incident.incident' && self.name === 'partner_id') {
                search_label = "";
                if (!dynamicFilters) {
                    let rec = self.recordData;
                    if (rec.partner_name || rec.partner_phone || rec.partner_email) {
                        // dynamicFilters = []
                        if (rec.partner_name) {
                            custom_string = rec.partner_name;
                            // dynamicFilters.push({
                            //     'description': 'Fuzzy Name:' + rec.partner_name
                            // })
                        }
                        if (rec.partner_phone) {
                            custom_string = custom_string ? custom_string += ', ' + rec.partner_phone : rec.partner_phone
                            // dynamicFilters.push({
                            //     'description': 'Phone:' + rec.partner_phone
                            // })
                        }
                        if (rec.partner_email) {
                            custom_string = custom_string ? custom_string += ', ' + rec.partner_email : rec.partner_email
                            // dynamicFilters.push({
                            //     'description': 'Email:' + rec.partner_email
                            // })
                        }
                    }
                }
            }
            let string_to_display = custom_string ? `${this.string}: ${custom_string}` : this.string;
            return {
                res_model: this.field.relation,
                domain: this.record.getDomain({ fieldName: this.name }),
                context: _.extend({}, this.record.getContext(this.recordParams), context || {}),
                dynamicFilters: dynamicFilters || [],
                title: (view === 'search' ? _t(search_label) : _t("Create: ")) + string_to_display,
                initial_ids: ids,
                initial_view: view,
                disable_multiple_selection: true,
                no_create: !self.can_create,
                kanban_view_ref: this.attrs.kanban_view_ref,
                on_selected: function (records) {
                    self.reinitialize(records[0]);
                },
                on_closed: function () {
                    self.activate();
                },
            };
        }
    });
});