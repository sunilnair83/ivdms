# -*- coding: utf-8 -*-
import logging
import socket

from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)


class FieldValidations(http.Controller):

    @http.route('/field_validations/phone_number', auth='public', type='json')
    def validate_phone_number(self, **kw):
        is_valid_number = request.env['res.partner'].sudo().is_phone_valid(
            {'phone': kw.get('phone_number'), 'phone_country_code': kw.get('country_code')},
            'phone_country_code', 'phone')
        return is_valid_number