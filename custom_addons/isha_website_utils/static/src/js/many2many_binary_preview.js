odoo.define('isha_website_utils.field_registry', function (require) {
    "use strict";

    var AbstractField = require('web.AbstractField');
    var basic_fields = require('web.basic_fields');
    var relational_fields = require('web.relational_fields');
    var registry = require('web.field_registry');
    var special_fields = require('web.special_fields');

    var FieldMany2ManyBinaryPreview = relational_fields.FieldMany2ManyBinaryMultiFiles.extend({
        template_files: "FieldBinaryFileUploaderOverride.files",
        fieldsToFetch: {
            name: {type: 'char'},
            mimetype: {type: 'char'},
            description: {type: 'char'},
        },
        /**
         * Compute the URL of an attachment.
         *
         * @private
         * @param {Object} attachment
         * @returns {string} URL of the attachment
         */
        _getFileUrl: function (attachment) {
            return '/web/content/' + attachment.id;
        },
    });
    // Adding new relational fields
    registry.add('many2many_binary_preview', FieldMany2ManyBinaryPreview);
});