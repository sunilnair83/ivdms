odoo.define('isha_website_utils.BaseForm', function (require) {
	"use strict";

	require('web.dom_ready');
	var ajax = require('web.ajax');
	var Class = require('web.Class');
	var rpc = require('web.rpc');
	var API_DATE_FORMAT = 'YYYY-MM-DD';
	var BaseForm = Class.extend({
		init(options = {}) {
			this.setUIComponentDefaults();
			this.isWizardForm = false;
			if (options.isWizardForm) {
				this.initWizardForm();
				this.isWizardForm = true;
			}
			if (options.hasDynamicFieldsRow) {
				this.initDynamicFieldsRow()
			}
			this.initUIComponents();
			this.uniqueId = 0; // Used to assign unique ids for cloned fields
			this.isWizardFormSaved = false;
			this.uploads = {};
			$('.loader').hide();
			$('.container, .container-fluid').removeClass('d-none');
			this.setupEventListeners();
			if (this._isFormSubmitted()) {
				this.disableForm();
				$('.alert-success').removeClass('hide').addClass('show').focus();
				this.setButtonsStateOnFormSubmit();
			}
		},
		setUIComponentDefaults() {
			$.fn.datetimepicker.defaults.icons = {
				time: "fa fa-clock-o",
				date: "fa fa-calendar",
				clear: "fa fa-trash",
				close: "fa fa-remove",
				date: "fa fa-calendar",
				up: "fa fa-chevron-up",
				down: "fa fa-chevron-down",
				next: "fa fa-chevron-right",
				previous: "fa fa-chevron-left",
				today: "fa fa-calendar-check-o"
			};
			$.fn.select2.defaults.set("theme", "bootstrap");
			$.fn.select2.defaults.set("placeholder", {
				'id': '',
				text: ''
			});
			$.fn.select2.defaults.set("minimumResultsForSearch", 7);
			// https://stackoverflow.com/a/49261426/2648297 - to open select on focus.
			// on first focus (bubbles up to document), open the menu
			$(document).on('focus', '.select2-selection.select2-selection--single', function (e) {
				$(this).closest(".select2-container").siblings('select:enabled').select2('open');
			});

			// steal focus during close - only capture once and stop propogation
			$('select.select2').on('select2:closing', function (e) {
				$(e.target).data("select2").$selection.one('focus focusin', function (e) {
					e.stopPropagation();
				});
			});
		},
		setupEventListeners() {
			var self = this;
			/**
			 * Trigger File Input on Button Click
			 */
			$(document).on('click', 'a.file-upload', function () {
				var fileInput = $(this).data('file-input')
				$('#' + fileInput).trigger('click')
			});

			/**
			 * Get Files
			 */
			$(document).on('change', "input[type=file]", function (event) {
				var $fileField = $(event.target);
				var fieldName = $fileField.attr('name');
				var showPreview = $fileField.data('show-preview');
				var mutipleFiles = $fileField.data('multiple');
				$.each(event.target.files, function (index, file) {
					// if a file with same name already exisits, do not attach it.
					if (_.isArray(self.uploads[fieldName])) {
						var file_exists = _.findWhere(self.uploads[fieldName], {'filename': file.name});
						if (file_exists) {
							return
						}
					}
					if (showPreview) {
						// Preview Reader
						var previewReader = new FileReader();
						previewReader.onload = function (event) {
							// Show Preview
							$('img#preview-' + fieldName).attr('src', event.target.result);
						};
						previewReader.readAsDataURL(file);
					}

					if (mutipleFiles) {
						let $multipleUploadsWrap = $fileField.closest('.multiple-uploads-wrap');
						let $lasUploadedFile = $multipleUploadsWrap.find('.uploaded-file').first();
						let $uploadedFileClone = $lasUploadedFile.clone();
						let $attachmentLink =  $uploadedFileClone.find('.js-attachment-link');
						$uploadedFileClone.removeClass('hide');
						$attachmentLink.text(file.name);
						$attachmentLink.attr('href', URL.createObjectURL(file));
						$uploadedFileClone.find('.js-attachment-delete').attr('data-field-name', fieldName).attr('data-file-name', file.name);
						$uploadedFileClone.insertAfter($lasUploadedFile);
					}

					// Upload Reader
					var uploadReader = new FileReader();
					uploadReader.onload = function (event) {
						// Add to upload array for upload
						var obj = {
							// name: fieldName,
							filename: file.name,
							data: btoa(uploadReader.result) // btoa() takes a string and encodes it to Base64.
						};
						if ($fileField.data('description')) {
							obj.description = $fileField.data('description');
						}
						if (!mutipleFiles) {
							self.uploads[fieldName] = obj;
						} else {
							if (_.isArray(self.uploads[fieldName])) {
								self.uploads[fieldName].push(obj);
							} else {
								self.uploads[fieldName] = [obj];
							}
						}

					};
					uploadReader.readAsBinaryString(file);
				});
			});

			$(document).on('click', '.js-add-new-carousel-item', this._cloneCarouselForm.bind(this));
			$(document).on('click', '.js-delete-carousel-item', this._deleteCarouselForm.bind(this));
			$(document).on('click', '.js-attachment-delete', this._deleteAttachment.bind(this));
			$(document).on('blur', '.js-validate-phone input', this._validatePhone.bind(this));
			$(document).on('change', '.js-country-code select', this._validatePhone.bind(this));
		},
		getWizardOptions() {
			let wizardOptions = {
				headerTag: 'h3',
				bodyTag: 'section',
				transitionEffect: 'fade',
				titleTemplate: '#title#',
				startIndex: 0,
				enableAllSteps: false,
				labels: {
					next: 'Save & Next',
					finish: 'Submit'
				},
				onStepChanging: function (event, currentIndex, newIndex, isStepClick) {
					return this._onWizardStepChaning(event, currentIndex, newIndex, isStepClick);
				}.bind(this),
				onStepChanged: function(event, currentIndex, priorIndex) {
					return this._onWizardStepChanged(event, currentIndex, priorIndex);
				}.bind(this),
				onFinishing: function (event, currentIndex) {
					return this._onWizardStepChaning(event, currentIndex);
				}.bind(this)
			};
			if($('input[type=hidden][name="form-section-submitted"]').val()) {
				let startIndex = Number($('input[type=hidden][name="form-section-submitted"]').val());
				// If it is last section, just stay there!
				if (startIndex == $('#wizard > section').length) {
					startIndex = startIndex - 1;
				}
				wizardOptions.startIndex = startIndex;
			}
			return wizardOptions;
		},
		initWizardForm() {
			let wizardOptions = this.getWizardOptions();
			this.wizard = $('#wizard').steps(wizardOptions);
		},
		_onWizardStepChaning(event, currentIndex, newIndex, isStepClick=false) {
			var $form = $('#wizard-p-' + currentIndex).find('form');
			// do not validate the form on step click.
			if (isStepClick) {
				return true;
			}
			// https://github.com/rstaib/jquery-steps/wiki/Q:-How-to-allow-step-back-during-step-errors
			if (currentIndex > newIndex || this.isWizardFormSaved || $form.attr('disabled')) {
				this.isWizardFormSaved = false;
				return true; // Proceed to next step
			}
			this.validateAndSaveForm($form);
		},
		_onWizardStepChanged(event, currentIndex, priorIndex) {
			if (this._isWizardFinalStep() && this._isFormSubmitted()) {
				$('[href="#finish"]').closest('li').hide();
			}
		},
		validateAllForms() {
			let isValid = true;
			let $forms = $('#wizard form');
			$forms.each(function(index, form) {
				if (!form.checkValidity()) {
					isValid = false;
				}
			});
			if (!isValid || $('.is-invalid').length) {
				flash('Please fill all mandatory fields in all sections, fix all validations and then try again.', {type: 'error'});
				return false;
			}
			return isValid;
		},
		validateAndSaveForm($form, isWizardSave = true) {
			if ($form[0].checkValidity() && !$form.find('.is-invalid').length) {
				if (this.customFormValidation && !this.customFormValidation($form)) {
					return false;
				}
				if (isWizardSave && this._isWizardFinalStep()) {
					if (!this.validateAllForms()) {
						return false;
					}
					if (this.showWizardSubmitConfirmation) {
						return this.showWizardSubmitConfirmation();
					}
				}
				return this.saveForm($form, isWizardSave);
			} else {
				$form[0].classList.add('was-validated');
				setTimeout(() => {
					if ($form.find('input:invalid, .is-invalid, textarea:invalid').length) {
						let $error_field = $form.find('input:invalid, .is-invalid, textarea:invalid').eq(0);
						if ($error_field[0].type == 'file') {
							$error_field = $error_field.siblings('.file-upload');
						}
						$error_field.focus();
						$('html, body').animate({scrollTop: $error_field.offset().top - 100}, 500);
					} else if ($form.find('.form-control:invalid').filter('select').length) {
						$form.find('.form-control:invalid').filter('select').eq(0).select2('open');
					}
				}, 100);
			}
		},
		_isWizardFinalStep() {
			let wizardState = $('#wizard').data('state');
			return this.isWizardForm && wizardState.currentIndex + 1 === wizardState.stepCount;
		},
		saveForm($form, isWizardSave = true) {
			$('.loader').show();
			let data = this.getFormData($form);
			let url = $form.data('url');
			return ajax.jsonRpc(url, 'call', data)
				.then((response) => {
					$('.loader').hide();
					if (this.onFormSaveSuccess) {
						this.onFormSaveSuccess(response);
					}
					if (isWizardSave) {
						this.isWizardFormSaved = true;
						$('#wizard').steps("next");
					}
					this.resetFileInputs($form);
					if (!this._isWizardFinalStep()) {
						flash('Details successfully saved 🎉');
					}
					return response;
				})
				.guardedCatch(function (reason) {
					$('.loader').hide();
				});
		},
		resetFileInputs($form) {
			let formElements = $.prop($form[0], 'elements');
			// resetting file uploads local data on save success
			$(formElements).filter('input[type="file"]').each(function (_, fileInput) {
				var fieldName = $(fileInput).attr('name');
				this.uploads[fieldName] = '';
			}.bind(this));
		},
		disableForm() {
			$('form,input,textarea').attr('disabled', 'true');
			$('select').attr('disabled', 'true').trigger('change.select2');
			$('.js-add-icon, .js-delete-icon').addClass('hide');
			$('.btn-modal-save').addClass('hide');
			$('.js-attachment-delete, .js-add-new-carousel-item, .js-delete-carousel-item, .upload-btn-wrap').hide();
		},
		getFormData($form) {
			var data = {};
			var formDataArray = $form.serializeArray();
			var dateFieldNames = [];
			// gets all form elements
			let formElements = $.prop($form[0], 'elements');
			// format dates to API date format YYYY-MM-DD
			$.makeArray(formElements).forEach((field) => {
				if (field.classList.contains('datetimepicker-input') && !field.disabled) {
					// there can be duplicate fields when we have dynamic row fields
					if (!dateFieldNames.includes(field.name)) {
						dateFieldNames.push(field.name);
					}
				}
			});
			// Below code changes the date format in serializeArray data.
			dateFieldNames.forEach((fieldName) => {
				let inputDateFormat = $(`[name="${fieldName}"]`).closest('.date').data('format') || 'DD-MM-YYYY';
				formDataArray.forEach((formData) => {
					if (formData.name == fieldName && formData['value']) {
						formData['value'] = moment(formData['value'], inputDateFormat).format(API_DATE_FORMAT);
					}
				});
			});
			var groupedDynamicFieldsObj = {};
			formDataArray.forEach((field) => {
				// Set non dynamic fields directly to request data
				if (!field.name.includes('dynamic.') && field.value) {
					if (!data[field.name]) {
						data[field.name] = field.value;	
					} else {
						// for multiple checkboxes, name attr will be same and we need to send the value as array
						if (_.isArray(data[field.name])) {
							data[field.name].push(field.value);
						} else {
							data[field.name] = [data[field.name], field.value];
						}
					}
				}
				// Below code is to convert dynamic fields to json array
				// For Example: [{dynamic.edu.name: 'test'}, {dynamic.edu.score: 1}] to {'edu': [{'name': 'test', 'score', 1}]}
				var splits = field.name.split('.')
				if (splits.length === 3 && splits[0] === 'dynamic') {
					if (!(groupedDynamicFieldsObj.hasOwnProperty(field.name))) {
						groupedDynamicFieldsObj[field.name] = [field.value]
					} else {
						groupedDynamicFieldsObj[field.name].push(field.value)
					}
				}
				// groupedDynamicFieldsObj contains {'dynamic.edu.name': ['test'], 'dynamic.edu.score': [1],}
				Object.keys(groupedDynamicFieldsObj).forEach((key) => {
					var splits = key.split('.');
					var dynamicRowName = splits[1];
					if (!(data.hasOwnProperty(dynamicRowName))) {
						data[dynamicRowName] = []
					}
					groupedDynamicFieldsObj[key].forEach((item, index) => {
						var tmpObj = {};
						if (data[dynamicRowName][index]) {
							tmpObj = data[dynamicRowName][index];
						} else {
							data[dynamicRowName].push(tmpObj);
						}
						Object.assign(tmpObj, {
							[splits[2]]: item
						});
					});
				});
			});
			$(formElements).filter('input[type="file"]').each(function (_, fileInput) {
				var fieldName = $(fileInput).attr('name');
				var apiKey = fieldName;
				var splits = apiKey.split('.');
				// if it is a dynamic name like "head-and-neck.medical_report_ids" we change it to medical_report_ids
				if (splits.length > 1) {
					apiKey = splits[1];
				}
				if (this.uploads[fieldName]) {
					data[apiKey] = this.uploads[fieldName]
				}
			}.bind(this));
			return data;
		},
		_initDateField(date_field) {
			let $dateField = $(date_field); 
			let minDate = $dateField.data('min-date');
			let maxDate = $dateField.data('max-date');
			let position = $dateField.data('position');
			let viewMode = $dateField.data('view-mode');
			if (minDate) {
				if (minDate === 'now') {
					minDate = moment();
				} else {
					minDate = moment(minDate, API_DATE_FORMAT);
				}
			} else {
				minDate = moment({y: 1900});
			}
			if (maxDate) {
				if (maxDate === 'now') {
					maxDate = moment();
				} else {
					maxDate = moment(minDate, API_DATE_FORMAT);
				}
			} else {
				maxDate = moment().add(200, "y");
			}
			// Hack: to set the maxdate to next day but disable it so that picker doesn't throw error.
			maxDate = moment(maxDate).add(1, "d");
			let disabledDates = [maxDate]
			var datetimepickerFormat = 'DD-MM-YYYY';
			if ($(date_field).data('format')) {
				datetimepickerFormat = $(date_field).data('format');
			}
			var options = {
				format: datetimepickerFormat,
				minDate,
				maxDate,
				disabledDates,
				useCurrent: false,
				date: null
			};
			if (position) {
				options.widgetPositioning = JSON.parse(position.replace(/'/g, '"'));	
			}
			if (viewMode) {
				options.viewMode = viewMode;
			}
			if ($(date_field).find('input').data('value')) {
				options.date = moment($(date_field).find('input').data('value'), API_DATE_FORMAT);
			}
			$(date_field).datetimepicker(options);
		},
		_initSelectField(selectField) {
			let options = {};
			let $selectField = $(selectField);
			if ($selectField.data('is-country-code')) {
				options.templateResult = function (option) {
					if (!option.id) { return option.text; }
					var $option = $(
						'<span class="flag-icon flag-icon-'+ $(option.element).data('countryCode').toLowerCase() +' flag-icon-squared"></span>' +
						'<span class="flag-text">'+ option.text+"</span>"
					);
					return $option;
				};
				options.templateSelection = options.templateResult
			}
			$selectField.select2(options);
		},
		initUIComponents() {
			_.each($('.input-group.date'), this._initDateField);
			$(document).on('focus', '.datetimepicker-input', function () {
				$(this).closest('.date').data('DateTimePicker').show();
			});
			$(document).on('blur', '.datetimepicker-input', function () {
				var $dateField = $(this).closest('.date');
				let format = $dateField.data('format');
				let units = 'days';
				var setMaxDateTarget = $dateField.data('set-max-date-target');
				var setMinDateTarget = $dateField.data('set-min-date-target');
				var datepicker = $dateField.data('DateTimePicker');
				let selectedDate = datepicker.viewDate();
				if (format == 'MM-YYYY') {
					units = 'months';
				}
				datepicker.hide();
				if (selectedDate && setMaxDateTarget) {
					let $targetDateField = $(setMaxDateTarget).closest('.date');
					let targetFieldDatePicker = $targetDateField.data('DateTimePicker');
					let targetFieldDate = targetFieldDatePicker.viewDate();
					if (targetFieldDate && targetFieldDate.isSameOrAfter(selectedDate, units)) {
						targetFieldDatePicker.date(null);
					}
					targetFieldDatePicker.maxDate(selectedDate.add(1, units));
				}
				if (selectedDate && setMinDateTarget) {
					let $targetDateField = $(setMinDateTarget).closest('.date');
					let targetFieldDatePicker = $targetDateField.data('DateTimePicker');
					let targetFieldDate = targetFieldDatePicker.viewDate();
					if (targetFieldDate && targetFieldDate.isSameOrBefore(selectedDate, units)) {
						targetFieldDatePicker.date(null);
					}
					if (targetFieldDatePicker.maxDate().isSameOrBefore(selectedDate, units)) {
						targetFieldDatePicker.minDate(targetFieldDatePicker.maxDate());
					} else {
						targetFieldDatePicker.minDate(selectedDate.add(1, units));
					}
					
				}
			});
			_.each($('select'), this._initSelectField);
			$('[data-toggle="tooltip"]').tooltip();

			// Hidden fields should be disabled or else they will also be validated
			$('fieldset.hide').attr('disabled', '');
			$('div.hide').find('input,select,textarea').attr('disabled', '');
		},
		initDynamicFieldsRow() {
			$(document).on('click', '.js-add-icon', this._cloneFieldsRow.bind(this));
			$(document).on('click', '.js-delete-icon', this._deleteFieldsRow.bind(this));
		},
		_cloneFieldsRow(event) {
			// get the last row div
			var $fields_row = $(event.target).siblings('.clone-fields-row').last(0);
			var $fields_clone = this._getClonedFields($fields_row);
			// Enable the delete icon
			$fields_clone.find('.js-delete-icon').removeClass('d-none');
			$fields_clone.insertAfter($fields_row);
			if ($fields_clone.find('.input-group.date')) {
				// Init date pickers
				_.each($fields_clone.find('.input-group.date'), this._initDateField);
			}
		},
		_deleteFieldsRow(event) {
			var $parent_row = $(event.target).parent();
			$parent_row.remove();
		},
		_getClonedFields($fields_row) {
			$fields_row.find('.pmd-select2').each(function (index) {
				// This is required when cloning select2 elements. Refer: https://stackoverflow.com/a/17381913/2648297
				$(this).select2('destroy');
			});
			var $fields_clone = $fields_row.clone();
			// Setting unique ids for all the cloned elements. Ex: id="test" after the below code, id will something like test1
			let uniqueId = this.uniqueId++;
			let dynamicAttrs = ['id', 'for', 'data-target', 'data-file-input', 'data-set-min-date-target', 'data-set-max-date-target'];
			dynamicAttrs.forEach(function(attr) {
				$fields_clone.find(`[${attr}]`).addBack(`[${attr}]`).attr(attr, function (index, attr) {
					return attr + uniqueId;
				});
			});
			// Init select 2 elements and reset the value for cloned select2
			if ($fields_clone.find('.pmd-select2')) {
				$fields_clone.find('.pmd-select2').each(function (index) {
					$(this).val('');
					$(this).select2();
					$(this).closest('.pmd-textfield').removeClass('pmd-textfield-floating-label-completed')
				});
				$fields_row.find('.pmd-select2').each(function (index) {
					$(this).select2();
				});
			}
			$fields_clone.find('input, select').val('');
			$fields_clone.find('input').data('value', ''); // for date fields
			$fields_clone.find('.is-dependant').addClass('hide').find('input,select').attr('disabled', '');
			// When the first row is hidden
			if ($fields_clone.hasClass('hide')) {
				$fields_clone.removeClass('hide');
				$fields_clone.find('input,select').removeAttr('disabled');
			}
			return $fields_clone;
		},
		_cloneCarouselForm(event) {
			var $carousel = $(event.target).closest('.carousel');
			var $carousel_item = $(event.target).closest('.carousel-item');
			var $carousel_indicators = $carousel.find('.carousel-indicators');
			var $carousel_item_clone = this._getClonedFields($carousel_item);
			$carousel_item_clone.removeClass('active');
			$carousel_item_clone.find('.js-delete-carousel-item').removeClass('d-none');		
			$carousel_item_clone.insertAfter($carousel.find('.carousel-item').last());
			$carousel_indicators.find('li.active').removeClass('active');
			var $last_indicator = $carousel_indicators.find('li').last();
			var $indicator_clone = $last_indicator.clone();
			// set the clone slide-to last indicator slide to + 1
			var last_slide_index = Number($last_indicator.data('slide-to')) + 1;
			$last_indicator.text(last_slide_index);
			$indicator_clone.text(last_slide_index + 1);
			$indicator_clone.insertAfter($last_indicator);
			$indicator_clone.attr('data-slide-to', last_slide_index).addClass('active');
			$carousel.carousel(last_slide_index);
			if ($carousel_item_clone.find('.input-group.date')) {
				// Init date pickers
				_.each($carousel_item_clone.find('.input-group.date'), this._initDateField);
			}
			if ($carousel_item_clone.find('.uploaded-files')) {
				$carousel_item_clone.find('.uploaded-file').not('.hide').remove();
			}
		},
		_deleteCarouselForm(event) {
			var $carousel = $(event.target).closest('.carousel');
			var $carousel_item = $(event.target).closest('.carousel-item');
			var $next_indicator = $carousel.find('.carousel-indicators li.active').next();
			var $next_carousel_item = $carousel.find('.carousel-item.active').next();
			$carousel.find('.carousel-indicators li.active').remove();
			$carousel.find('.carousel-indicators li').each(function(index, element) {
				$(element).attr('data-slide-to', index);
			});
			// If last record is deleted, go to the first one.
			if (!$next_indicator.length) {
				$next_indicator = $carousel.find('.carousel-indicators li').eq(0);
				$next_carousel_item = $carousel.find('.carousel-item').eq(0);
				$next_indicator.addClass('active');
			}
			$next_indicator.addClass('active');
			$next_carousel_item.addClass('active');
			$next_carousel_item.find('input, select').removeAttr('disabled');
			$carousel_item.addClass('is-deleted');
			$carousel.carousel($next_indicator.data('slide-to'));
			setTimeout(function() {
				$carousel_item.remove();
			}, 1000);
		},
		_deleteAttachment(event) {
			var $deleteAttachment = $(event.target);
			var $uploadedFile = $deleteAttachment.closest('.uploaded-file');
			var attachmentsKeyName = $deleteAttachment.data('field-name');
			var filename = $deleteAttachment.data('file-name');
			if (this.uploads[attachmentsKeyName]) {
				this.uploads[attachmentsKeyName] = _.reject(this.uploads[attachmentsKeyName], function(attachment) {
					return attachment.filename == filename;
				});
			}
			$uploadedFile.remove();
		},
		toggleRadioDependentFields(options) {
			let radioSelector = options.radioSelector;
			let dependantSelector = options.dependantSelector;
			let showOnSelectingNo = options.showOnSelectingNo || false;
			let parentSelector = options.parentSelector || '';
			// For scoping the fields when there are duplicates on the same page
			if (parentSelector) {
				radioSelector = `${parentSelector}  ${radioSelector}`;
				dependantSelector = `${parentSelector}  ${dependantSelector}`;
			}
			
			$(document).on('change', radioSelector, function () {
				let value = $(this).val();
				// When we have dependant fields inside a carousel item.
				if ($(this).closest('.carousel-item.active').length) {
					var $carouselItem = $(this).closest('.carousel-item.active');
					dependantSelector = $carouselItem.find(options.dependantSelector); 
				}
				if (['yes', 'true'].includes(value) && !showOnSelectingNo || showOnSelectingNo && value == 'no') {
					$(dependantSelector).removeClass('hide');
					$(dependantSelector).removeAttr('disabled');
					$(dependantSelector).find('input,select,textarea').removeAttr('disabled');
				} else {
					$(dependantSelector).addClass('hide');
					$(dependantSelector).attr('disabled', '');
					$(dependantSelector).find('input,select,textarea').attr('disabled', '');
				}
			});
		},
		_validatePhone(event) {
			let $targetElement = $(event.target);
			let country_code, phone_number, $phone_input, $country_code, $validate_phone;
			let isValid = true;
			if ($targetElement.hasClass('pmd-select2')) {
				$country_code = $targetElement.closest('.js-country-code');
				$validate_phone = $country_code.siblings('.js-validate-phone');
				$phone_input = $validate_phone.find('input');
			} else {
				$phone_input = $targetElement;
				$validate_phone = $phone_input.closest('.js-validate-phone');
				$country_code = $validate_phone.prev('.js-country-code');
			}
			let $phone_field = $validate_phone.closest('.phone-field');
			country_code = $country_code.find('select').val();
			phone_number = $phone_input.val();
			if (phone_number && country_code) {
                rpc.query({
                    route: '/field_validations/phone_number',
                    params: {phone_number, country_code},
                }).then(function(is_valid) {
                    if (is_valid) {
						$phone_field.removeClass('is-invalid');
                    } else if (!$phone_field.hasClass('is-invalid')) {
						$phone_field.addClass('is-invalid');
                    }
                }).catch(function(error) {
                    console.log(error)
                });
            } else if (phone_number && $phone_field.find('.is-mandatory').length && !$phone_field.hasClass('is-invalid')) {
				$phone_field.addClass('is-invalid');
			}
		},
		_isFormSubmitted() {
			return $('input[type=hidden][name="form-status"]').val() == 'submitted';
		},
		setButtonsStateOnFormSubmit() {
			$('[href="#cancel"]').closest('li').hide();
			$('[href="#finish"]').closest('li').hide();
			$('[href="#next"]').text('Next');
		},
		onFormSaveSuccess(response) {
			if (!this.isWizardForm || this._isWizardFinalStep()) {
				$('#saveConfirmation').modal('hide');			
				$('[name="form-status"]').val('submitted');
				this.disableForm();
				this.setButtonsStateOnFormSubmit();				
				flash('Done! Form submitted successfully 🎉. Thank you. Namaskaram 🙏');
				setTimeout(function() {
					$('.alert-success').removeClass('hide').addClass('show');
					$('html, body').animate({scrollTop: $(".alert-success").offset().top - 150}, 600);
				}, 3500);
			}
		}
	});
	return BaseForm;
});
