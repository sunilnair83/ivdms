
/*!
 * Propeller v1.3.2 (http://propeller.in)
 * Copyright 2016-2019 Digicorp, Inc.
 * Licensed under MIT (http://propeller.in/LICENSE)
 */

"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

// Attach Parent Selector
var commons = function () {
	
	function commons() {}
	commons.attachParentSelector = function (parentSelector, defaultSelector) {
		var customSelector = defaultSelector;
		if (parentSelector && parentSelector !== '' && parentSelector.length > 0) {
			if (parentSelector === defaultSelector) {
				customSelector = defaultSelector;
			} else if ($(parentSelector).hasClass(defaultSelector)) {
				customSelector = parentSelector + "" + defaultSelector;
			} else {
				customSelector = parentSelector + " " + defaultSelector;
			}
		}
		return customSelector;
	};
	return commons;
};

// Inherit one class to another
function _inherits(SubClass, SuperClass) {
	if (typeof SuperClass !== "function" && SuperClass !== null) {
		throw new TypeError("Super expression must either be null or a function, not " + typeof SuperClass);
	}
	SubClass.prototype = new SuperClass();
}

// Propeller components Mapping
var propellerControlMapping = {
	"pmd-checkbox": function () {
		$('.pmd-checkbox').pmdCheckBox();
	},
	"pmd-radio": function () {
		$('.pmd-radio').pmdRadio();
	},
	"pmd-textfield": function () {
		$('.pmd-textfield').pmdTextfield();
	},
	"pmd-dropdown": function () {
		$('.pmd-dropdown').pmdDropdown();
	},
	"pmd-alert-toggle": function () {
		$('.pmd-alert-toggle').pmdAlert();
	},
	"pmd-tabs": function () {
		$('.pmd-tabs').pmdTab();
	},
	"pmd-sidebar": function () {
		$().pmdSidebar();
	},
	"pmd-accordion": function () {
		$('.pmd-accordion').pmdAccordion();
	},
	"pmd-ripple-effect": function () {
		$('.pmd-ripple-effect').pmdButton();
	}
};

// DOM Observer
var observeDOM = (function () {
	var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
		eventListenerSupported = window.addEventListener;
	return function (obj, callback) {
		if (MutationObserver) {
			// define a new observer
			var obs = new MutationObserver(function (mutations, observer) {
				if (mutations[0].addedNodes.length || mutations[0].removedNodes.length) {
					callback(mutations);
				}
			});
			// have the observer observe foo for changes in children
			obs.observe(obj, {
				childList: true,
				subtree: true,
				attributes: true,
				characterData: true
			});
		} else if (eventListenerSupported) {
			obj.addEventListener('DOMNodeInserted', callback, false);
			obj.addEventListener('DOMNodeRemoved', callback, false);
		}
	};
})();

$(document).ready(function () {
	$.propellerkit();
});

$.propellerkit = function() {
	observeDOM(document.querySelector('body'), function (mutations) {
		processMutation(0);
		function processMutation(index) {
			if (index >= mutations.length) {
				return;
			}
			var mutation = mutations[index];
			var nodes = mutation.addedNodes;
			processNodes(nodes, function () {
				processMutation(index + 1);
			});
		}
		
		function processNodes(nodes, callback) {
			if (nodes.length === 0) {
				callback();
				return;
			}
			processNode(nodes, 0, function () {
				callback();
			});
		}

		function processNode(nodes, index, callback) {
			if (index >= nodes.length) {
				callback();
				return;
			}
			var node = nodes[index];
			if (containsPmdClassPrefix(node)) {
				if ($(node).attr("data-toggle") !== undefined && $(node).attr("data-toggle").toLowerCase() === "popover") {
					$().pmdPopover();
				}
				var classes = $(node).attr('class');
				if (classes === undefined) {
					callback();
					return;
				}
				classes = classes.split(' ');
				classes.forEach(function (clazz) {
					if (propellerControlMapping[clazz]) {
						propellerControlMapping[clazz]();
						return true;
					}
					return false;
				});
				processNode(nodes, index+1, function() {
					callback();
				});
			} else {
				try {
					var childNodes = node.childNodes;
					processNodes(childNodes, function() {
						processNode(nodes, index+1, function() {
							callback();
						});
					});
				} catch (e) {
					
				}
			}
		}
		function containsPmdClassPrefix(ele) {
			if ($(ele).attr('class') === undefined) {
				return false;
			}
			var classes = $(ele).attr('class').split(' ');
			for (var i = 0; i < classes.length; i++) {
				
				if (propellerControlMapping.hasOwnProperty(classes[i])) {
					return true;	
				}				
			}
			return false;
		}
	});
};


$( document ).ready(function() {

//	$(".pmd-textfield .select2-selection").after('<span class="pmd-textfield-focused"></span>');

	var $eventSelect = $(".pmd-select2");
	$eventSelect.on("select2:opening", function () { 
		$(this).closest('.pmd-textfield').addClass("pmd-textfield-floating-label-active pmd-textfield-floating-label-completed");
	});
	
	$eventSelect.on("select2:close", function () {
		$(".pmd-textfield").removeClass("pmd-textfield-floating-label-active"); 
		var selected_value = $(this).val();
   		if (selected_value==0 || selected_value=='' || selected_value==undefined) {
			$(this).closest('.pmd-textfield').removeClass("pmd-textfield-floating-label-completed");
   		} else {
			$(this).closest('.pmd-textfield').addClass("pmd-textfield-floating-label-completed");
		}
	});
	$eventSelect.each(function(){
		var selected_value = $(this).val();
   		if (selected_value==0 || selected_value=='' || selected_value==undefined) {
			$(this).closest('.pmd-textfield').removeClass("pmd-textfield-floating-label-completed");
   		} else {
			$(this).closest('.pmd-textfield').addClass("pmd-textfield-floating-label-completed");
		}
	});
	
	var $eventSelectTag = $(".pmd-select2-tags");
	$eventSelectTag.on("select2:opening", function () { 
		$(this).closest('.pmd-textfield').addClass("pmd-textfield-floating-label-active pmd-textfield-floating-label-completed");
	});
	
	$eventSelectTag.on("select2:close", function () {
		$(".pmd-textfield").removeClass("pmd-textfield-floating-label-active");
		var selected_tag = $(this).closest('.pmd-textfield').find('.select2-selection__choice').hasClass('select2-selection__choice');
		if (selected_tag) {
			$(this).closest('.pmd-textfield').addClass("pmd-textfield-floating-label-completed");
		} else {
			$(this).closest('.pmd-textfield').removeClass("pmd-textfield-floating-label-completed");
		}
	});
	
	$eventSelectTag.on("change", function(){
		if ($('.select2-selection__rendered li').hasClass('select2-selection__choice')) {
			$(this).closest('.pmd-textfield').addClass("pmd-textfield-floating-label-completed");
		} else {
			$(this).closest('.pmd-textfield').removeClass("pmd-textfield-floating-label-completed");
		}
	});
	
	$eventSelectTag.each(function(){
		var selected_tag = $(this).find('option').attr('selected')
		if (selected_tag) {
			$(this).closest('.pmd-textfield').addClass("pmd-textfield-floating-label-completed");
		} else {
			$(this).closest('.pmd-textfield').removeClass("pmd-textfield-floating-label-completed");
		}
	});
	
});



/**
 * --------------------------------------------------------------------------
 * Propeller v1.3.2 (http://propeller.in): textfield.js
 * Copyright 2016-2019 Digicorp, Inc.
 * Licensed under MIT (http://propeller.in/LICENSE)
 * --------------------------------------------------------------------------
 */

var pmdTextfield = function ($) {		
	
	
	/**
	 * ------------------------------------------------------------------------
	 * Variables
	 * ------------------------------------------------------------------------
	 */
 
	 var NAME = 'pmdTextfield';
	 var JQUERY_NO_CONFLICT = $.fn[NAME];
 
	 var ClassName = {
		 PMD_TEXTFIELD: 'pmd-textfield',
		 FOCUS: 'pmd-textfield-focused',
		 FLOATING_COMPLETE: 'pmd-textfield-floating-label-completed',
		 FLOATING_ACTIVE: 'pmd-textfield-floating-label-active'
	 };
 
	 var Selector = {
		 PARENT_SELECTOR: '',
		 PMD_TEXTFIELD: '.' + ClassName.PMD_TEXTFIELD,
		 FOCUS: '.' + ClassName.FOCUS,
		 INPUT: '.form-control'
	 };
 
	 var Template = {
		 LABEL: '<span class="pmd-textfield-focused"></span>'
	 };
 
	 var Event = {
		 FOCUS: 'focus',
		 FOCUSOUT: 'focusout',
		 CHANGE: 'change'
	 };	
 
	 /**
	 * ------------------------------------------------------------------------
	 * Functions
	 * ------------------------------------------------------------------------
	 */
 
	 function onFocus(e) {
		 var $this = $(e.target);
		 $this.closest(Selector.PMD_TEXTFIELD).addClass(ClassName.FLOATING_ACTIVE + " " + ClassName.FLOATING_COMPLETE);
	 }
 
	 function onFocusOut(e) {
		 var $this = $(e.target);
		 if ($this.val() === "") {
			 $this.closest(Selector.PMD_TEXTFIELD).removeClass(ClassName.FLOATING_COMPLETE);
		 }
		 $this.closest(Selector.PMD_TEXTFIELD).removeClass(ClassName.FLOATING_ACTIVE);
	 }
 
	 function onChange(e) {
		 var $this = $(e.target);
		 if ($this.val() !== "") {
			 $this.closest(Selector.PMD_TEXTFIELD).addClass(ClassName.FLOATING_COMPLETE);
		 }
	 }
 
	 
	/**
	 * ------------------------------------------------------------------------
	 * Initialization
	 * ------------------------------------------------------------------------
	 */
 
	 var pmdTextfield = function () {
		 _inherits(pmdTextfield, commons);
		 function pmdTextfield() {
			 $(pmdTextfield.prototype.attachParentSelector(Selector.PARENT_SELECTOR, Selector.FOCUS)).remove();
			 $(pmdTextfield.prototype.attachParentSelector(Selector.PARENT_SELECTOR, Selector.PMD_TEXTFIELD)).find(Selector.INPUT).after(Template.LABEL);
			 $(pmdTextfield.prototype.attachParentSelector(Selector.PARENT_SELECTOR, Selector.PMD_TEXTFIELD)).find(Selector.INPUT).each(function () {
				 if ($(this).val() !== "") {
					 $(this).closest(Selector.PMD_TEXTFIELD).addClass(ClassName.FLOATING_COMPLETE);
				 }
			 });
		 }
		 return pmdTextfield;
	 }();
 
	 
	/**
	 * ------------------------------------------------------------------------
	 * jQuery
	 * ------------------------------------------------------------------------
	 */
	 
	 var plugInFunction = function () {
		 if (this.selector !== "") {
		   Selector.PARENT_SELECTOR = this.selector;
		 }
		 new pmdTextfield();
	 };
	 $(document).on(Event.CHANGE, Selector.PMD_TEXTFIELD + " " + Selector.INPUT, onChange);
	 $(document).on(Event.FOCUS, Selector.PMD_TEXTFIELD + " " + Selector.INPUT, onFocus);
	 $(document).on(Event.FOCUSOUT, Selector.PMD_TEXTFIELD + " " + Selector.INPUT, onFocusOut);
	 $.fn[NAME] = plugInFunction;
	 return pmdTextfield;
   
 } (jQuery)();
 