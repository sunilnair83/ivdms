# -*- coding: utf-8 -*-
{
    'name': "Isha - Website Utils",
    'summary': """Reusable website templates and utils""",
    'description': """Reusable website templates and utils""",
    'author': "Nanda",
    'website': "http://www.isha.sadhguru.org",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': [
        'base',
        'website'
    ],
    'data': [
        'views/assets.xml',
        'views/form_templates.xml'
    ],
    'qweb': [
        'static/src/xml/odoo_template_overrides.xml'
    ],
    'application': False
}
