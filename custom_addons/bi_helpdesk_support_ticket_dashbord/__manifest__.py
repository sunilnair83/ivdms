# -*- coding: utf-8 -*-
# Part of Browseinfo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Helpdesk Support Ticket Dashbord in Odoo',
    'version': '13.0.0.2',
    'category' : 'Website',
    'summary': 'helpdesk Ticket Dashboard helpdesk Dashboard Support Dashboard help desk support dashboard support ticket dashboard for support Customer Helpdesk Support Ticket Online ticketing system for customer support service desk  support Issue tracking dashboard',
    'description': """helpdesk Support Ticket Dashbord 
	
	Dashboard for Helpdesk Support Tickets
	support ticket dashboard
	ticket dashboard
	support dashboard
	dashboard for support 
	support ticket dashboard
    help desk dashboard
    helpdesk dashboard
    helpdesk support dashboard
    help desk support dashboard
    help desk ticket dashboard
    helpdesk ticket dashboard
    

	""",
    'author': 'BrowseInfo',
    'website' : 'https://www.browseinfo.in',
    'price': 39,
    'currency': 'EUR',
    'depends': ['base','website',
                # 'website_sale',
                'bi_website_support_ticket','bi_helpdesk_service_level_agreement'],
    'data':[
        'views/helpdesk_ticket_kanban_view.xml',
        'views/helpdesk_ticket_kanban_actions.xml',
        'views/helpdesk_support_ticket_dashbord_views.xml',
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'live_test_url' : 'https://youtu.be/3RHTq67PVSo',
    'images':['static/description/Banner.png'],
}
