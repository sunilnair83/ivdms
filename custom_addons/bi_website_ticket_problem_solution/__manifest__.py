# -*- coding: utf-8 -*-
# Part of Browseinfo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Solution Answer for Helpdesk Support Ticket in odoo',
    'version': '13.0.0.1',
    'category': 'Website',
    'author' : 'BrowseInfo',
    'summary': 'App Helpdesk Support Ticket FAQ Helpdesk Support Ticket Q&A Helpdesk Support Ticket Solution Answer Helpdesk Support Ticket Question Answer ticket Q&A support ticket answer helpdesk support question answer Customer Helpdesk Support Ticket Solution answer',
    'description': '''A helpdesk / support ticket system for your website
    support management from website
    Solution Answer for Helpdesk Support Ticket
    Solution Answer for Helpdesk Support Ticket and ISSUe
    Solution Answer for Helpdesk issue
    Solution Answer for Helpdesk FAQ
    FAQ Solution Answer for Helpdesk Support Ticket
    Solution Answer for issue
    website support ticket reply , service reply , troubleshooting solution , ticketing system , support answers , 
    website helpdesk management
    Support system for your website
    website support management
    submit support request
	 formatted solution formatted answer solution , formatted reply , formatted service reply , formatted helpdesk reply ,formatted support answer , formatted support reply formatted helpdesk reply 
    support form
    Ticket support
    website support ticket
    website issue
    website project issue
    website crm management
    website ticket handling
    support management
    project support, crm support, online support management, online support, support product, 
    support services, issue support, fix issue, raise ticket by website, 
    raise support ticket by website, view support request, display support on website, 
    list support on website, helpdesk system for your website, website helpdesk management, 
    submit helpdesk, helpdesk form, Ticket helpdesk, website support ticket, website issue, 
    website project issue, website crm management, website ticket handling,support management, 
    project support, crm support, online support management, online helpdesk, helpdesk product, 
    helpdesk services, issue helpdesk, fix helpdesk, raise ticket by website, raise issue by website, 
    view helpdesk, display helpdesk on website, list helpdesk on website, website customer support Ticket
    website support Ticket with timesheet, website support Ticket invoice, website helpdesk Ticket invoice, 
    website support helpdesk Ticket invoice, website helpdesk support Ticket with timesheet, 
    website support Ticket, website helpdesk Ticket
    Customer Website Helpdesk Support Ticket for Customer
    client Website Helpdesk Support Ticket for client
    Website portal Helpdesk Support Ticket for Customer
    customer portal helpdesk support, customer portal support management, portal customer support management
    website portal helpdesk support for customer, website portal support, website portal helpdesk
    website portal support Ticket,website portal helpdesk Ticket , website portal support request , website support request
    Submit Support Request, online Support Request, manage Support Request, manage support team
    create task from support request, create task from helpdesk request, online service request, create task from service request
    website portal service request, website portal helpdesk service request, create invoice from timesheet
    invoice from service request, invoice from helpdesk request, invoice from helpdesk timesheet
    timesheet for helpdesk support Ticket, invoice for helpdesk support request, project sub-task management
    Re-assign Helpdesk Support Ticket(Responsible/Team)
    Reassign Helpdesk Support request(Responsible/Team)
    Re-assign Helpdesk request(Responsible/Team)
    Reassign responsible on Helpdesk Support Ticket(Responsible/Team)
    Reassign team on Helpdesk Support Ticket(Responsible/Team)
    Re-assign support team Helpdesk Support Ticket(Responsible/Team)
    Escalate Helpdesk Support Ticket(Responsible/Team)
    Escalate team on helpdesk support request
    Escalate helpdesk support request, Escalate helpdesk support ticket
        

        ''',
    'price': 15,
    'currency': "EUR",
    'website': 'https://www.browseinfo.in',
    'depends': ['base','website',
                # 'website_sale',
                'bi_website_support_ticket'],
    'data': [
    'report/ticket_solution_template_view.xml',
    'report/ticket_solution_report_view.xml',
    'views/support_ticket_website_view.xml',
    'views/ticket_solution_views.xml',
    ],
    'demo': [],
    'css': [],
    'installable': True,
    'auto_install': False,
    'application': False,
    'live_test_url':'https://youtu.be/p5IlxqYSp3o',
    "images":['static/description/Banner.png'],
}
