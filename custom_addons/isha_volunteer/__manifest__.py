# -*- coding: utf-8 -*-
{
    'name': "Isha Volunteer",

    'summary': """
        Isha Volunteer Domain Filter.
        """,

    'description': """
        Isha Volunteer Domain Filter.
    """,

    'author': "Isha IT",
    'website': "http://www.ishait.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'version': '13.0.0.0.1',
    'depends': ['isha_crm', 'hr', 'base'],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/volunteer_view.xml',
        'views/person_type_view.xml',
        'views/proof_type_view.xml',
        'views/work_experience_view.xml',
        'views/programs_view.xml',
        'views/education_qualification_view.xml',
        'views/volunteer_education_qualification_view.xml',
        'views/volunteer_language_view.xml',
        'views/skill_view.xml',
        'views/skill_function_view.xml',
        'views/isha_connect_view.xml',
        'views/volunteer_volunteering_iyc_view.xml',
        'views/volunteer_volunteering_local_view.xml',
        'views/family_medical_history_view.xml',
        'views/emergency_contact_view.xml',
        'views/program_history_view.xml',
        'views/yatra_history_view.xml',
        'views/event_history_view.xml',
        'data/education_qualification_data.xml',
        'data/proof_type_data.xml',
        'data/functions_and_skills_data.xml',
        'data/isha_programs2_data.xml',
        'data/work_industry_data.xml',
        'data/volunteering_activity.xml',
        'data/isha_yatra.xml',
        'data/isha_event.xml',
        'data/isha_connect_data.xml',
        # 'cron/cron_jobs.xml',
        'data/cron.xml'
    ],
    'installable': True,
    'application': True,
    'license': 'OPL-1',
}
