import datetime
from datetime import date
from dateutil.relativedelta import relativedelta
from odoo import models, fields, api
from . import constants


class IshaVolunteer(models.Model):
    _name = "isha.volunteer"
    _description = "Isha Volunteer"
    _rec_name = "partner_id"

    # Personal details
    partner_id = fields.Many2one('res.partner', delegate=True, required=True, auto_join=True)
    mother_tongue = fields.Many2one('res.lang', string='Mother Tongue')
    language_ids = fields.One2many('isha.volunteer.languages', 'volunteer_id', string='Languages')
    family_details_ids = fields.One2many('isha.volunteer.family.details', 'volunteer_id',
                                         string='Direct Family Details')
    work_experience_ids = fields.One2many('isha.volunteer.work.experience', 'volunteer_id', string='Work Experience')
    education_ids = fields.One2many('isha.volunteer.edu.qualification', 'volunteer_id', string='Education')
    isha_connect = fields.Many2many('isha.connect', string='Isha Connect')
    skills = fields.Many2many('isha.volunteer.skill', relation='volunteer_skills_rel')
    additional_skills = fields.Text("Any Additional Skills")

    # def _compute_default_person_type(self):
    #     return self.env.ref('isha_volunteer.isha_person_type_sadhanapada_applicant').id
    # person_type_id = fields.Many2one('isha.person.type' , string='Person Type',default=_compute_default_person_type)

    # Nationality/Address
    id_proof = fields.Image(string='Id Proof')
    id_proof_type = fields.Many2one('proof.type', string='Id Proof Type')
    address_proof = fields.Image(string='Address Proof')
    address_proof_type = fields.Many2one('proof.type', string='Address Proof Type')
    # ID proof and address proof image mixing
    id_proof_512 = fields.Image("ID Proof 512", related="id_proof", max_width=512, max_height=512, store=True)
    address_proof_512 = fields.Image("Address Proof 512", related="address_proof", max_width=512, max_height=512, store=True)

    # Volunteering
    has_volunteered_at_main_center = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    center_volunteering_ids = fields.One2many('isha.volunteer.main.center.volunteering', 'volunteer_id',
                                              string='Volunteering at IYC')
    has_volunteered_at_local_center = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    local_volunteering_ids = fields.One2many('isha.volunteer.local.volunteering', 'volunteer_id',
                                             string='Local Volunteering')

    # Program and events
    has_completed_program = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    program_history_ids = fields.One2many('isha.volunteer.program.history', 'volunteer_id', string='Program History')
    has_attended_event = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    event_history_ids = fields.One2many('isha.volunteer.event.history', 'volunteer_id', string='Event History')
    has_completed_yatra = fields.Selection(list(constants.BOOLEAN_VALUES.items()))
    yatra_history_ids = fields.One2many('isha.volunteer.yatra.history', 'volunteer_id', string='Yatra History')
    other_programs = fields.Text(string='If you have practiced/are practising any other methods like spiritual '
                                        'practices, yoga, aroma therapy, energy systems, massage therapy, pilates etc, '
                                        'please give details')

    # Emergency
    emergency_contact_ids = fields.One2many('isha.volunteer.emergency.contact', 'volunteer_id',
                                            string='Emergency Contact')

    @api.constrains('center_volunteering_ids', 'local_volunteering_ids', 'program_history_ids', 'event_history_ids',
                    'yatra_history_ids')
    def _update_flags(self):
        for record in self:
            if record.center_volunteering_ids:
                record.has_volunteered_at_main_center = 'yes' if len(record.center_volunteering_ids) > 0 else 'no'
            if record.local_volunteering_ids:
                record.has_volunteered_at_local_center = 'yes' if len(record.local_volunteering_ids) > 0 else 'no'
            if record.program_history_ids:
                record.has_completed_program = 'yes' if len(record.program_history_ids) > 0 else 'no'
            if record.event_history_ids:
                record.has_attended_event = 'yes' if len(record.event_history_ids) > 0 else 'no'
            if record.yatra_history_ids:
                record.has_completed_yatra = 'yes' if len(record.yatra_history_ids) > 0 else 'no'

    @api.model
    def create(self, values):
        if 'partner_id' not in values:
            partner_id = self.env['res.partner'].sudo().create(values).id
            values['partner_id'] = partner_id
        else:
            partner_id = int(values['partner_id'])
        volunteer = self.env['isha.volunteer'].sudo().search([('partner_id', '=', partner_id)],
                                                             order='id desc', limit=1)
        if not volunteer:
            return super(IshaVolunteer, self).create(values)
        else:
            volunteer.write(values)
            return volunteer


class ProofType(models.Model):
    _name = 'proof.type'

    _sql_constraints = [('name_uniq', 'UNIQUE (name)', 'You can not have two proof types with the same name !')]

    name = fields.Char(string='Proof Type Name', required=True)
    active = fields.Boolean(string='Active', default=True)


class WorkExperience(models.Model):
    _name = 'isha.volunteer.work.experience'
    _description = 'Work Experience'
    _rec_name = 'company'

    volunteer_id = fields.Many2one('isha.volunteer', string='Name', ondelete='cascade', required=True)
    from_date = fields.Date(string='From Date')
    to_date = fields.Date(string='To Date')
    company = fields.Char(string='Company')
    industry = fields.Many2one('work.industry', string='Industry')
    designation = fields.Char(string='Designation')
    tasks = fields.Text(string='Responsibilities')


class WorkIndustry(models.Model):
    _name = 'work.industry'
    _description = "Work Industry"
    _order = 'name'

    _sql_constraints = [('name_uniq', 'UNIQUE (name,alias_name)', 'You cannot have two industries with same!')]

    name = fields.Char(string='Name')
    alias_name = fields.Char(string='Alias Name')


class IshaPrograms(models.Model):
    _name = 'isha.programs'
    _description = "Isha Programs"
    _order = 'name'

    _sql_constraints = [
        ('name_uniq', 'UNIQUE (name,alias_name)', 'You can not have two same type with the same name !')]

    name = fields.Char(string='Name')
    alias_name = fields.Char(string='Alias Name')


class IshaVolunteerEduQualification(models.Model):
    _name = 'isha.volunteer.edu.qualification'
    _description = 'Education'
    _rec_name = 'education_qualifications'

    volunteer_id = fields.Many2one('isha.volunteer', string='Volunteer', ondelete='cascade', required=True)
    education_qualifications = fields.Many2one('educational.qualification', string='Qualifications')
    institution_name = fields.Char(string="Institution's Name")
    city = fields.Char(string='City')
    specialization = fields.Char(string='Specialization')
    passing_year = fields.Char(string='Year of Passing/Graduation')
    marks_percentage = fields.Char(string='% Marks')


class EducationalQualification(models.Model):
    _name = 'educational.qualification'
    _description = "Educational Qualification"
    _order = 'name'
    _sql_constraints = [
        ('name_uniq', 'UNIQUE (name,alias_name)', 'You can not have two same type with the same name !')]

    name = fields.Char(string='Name')
    alias_name = fields.Char(string='Alias Name')


class Languages(models.Model):
    _name = 'isha.volunteer.languages'
    _description = 'Languages'
    _rec_name = 'language'
    # _sql_constraints = [('isha_volunteer_language_unique', 'UNIQUE(language, volunteer_id)', 'Language already added')]

    LANGUAGE_SKILL_LEVEL = [('dont_know', "Don't Know"), ('beginner', 'Beginner'), ('intermediate', 'Intermediate'),
                            ('medium', 'Medium'), ('fluent', 'Fluent')]

    def name_get(self):
        result = []
        for record in self:
            if record.can_read != 'dont_know':
                can_read = 'R'
            else:
                can_read = ''
            if record.can_write != 'dont_know':
                can_write = 'W'
            else:
                can_write = ''
            if record.can_speak != 'dont_know':
                can_speak = 'S'
            else:
                can_speak = ''
            if record.can_type != 'dont_know':
                can_type = 'T'
            else:
                can_type = ''
            rec_name = "%s (%s %s %s %s)" % (record.language.name, can_read, can_write, can_speak, can_type)
            result.append((record.id, rec_name))
        return result

    volunteer_id = fields.Many2one('isha.volunteer', string='Name', ondelete='cascade', required=True)
    language = fields.Many2one('res.lang', string='Languages')

    can_read = fields.Selection(LANGUAGE_SKILL_LEVEL, string="Can read")
    can_write = fields.Selection(LANGUAGE_SKILL_LEVEL, string="Can write")
    can_speak = fields.Selection(LANGUAGE_SKILL_LEVEL, string="Can speak")
    can_type = fields.Selection([('dont_know', "Don't Know"), ('slow', 'Slow'), ('medium', 'Medium'), ('fast', 'Fast')],
                                string="Can type")
    alias_name = fields.Char(string='Alias Name')


class VolunteeringDetails(models.AbstractModel):
    _name = 'isha.volunteer.volunteering.details'
    _description = 'Volunteering Details'

    volunteer_id = fields.Many2one('isha.volunteer', string='Volunteer', ondelete='cascade', required=True)
    from_date = fields.Date(string='From Date')
    volunteering_duration = fields.Integer(string="Volunteering Duration (No. of Days)")

    coordinator_name = fields.Char(string='Coordinator name')
    activity_description = fields.Text(string='Activity Description')


class VolunteeringMainCenter(models.Model):
    _name = 'isha.volunteer.main.center.volunteering'
    _inherit = 'isha.volunteer.volunteering.details'
    _description = 'Volunteering at IYC'
    _rec_name = 'activity'

    activity = fields.Many2one('main.center.volunteering.activity', string='Center Activity')
    location = fields.Selection([('iyc', 'IYC, Coimbatore'), ('iii', 'III, Tennessee')], string='Location')


class IycVolunteeringActivity(models.Model):
    _name = 'main.center.volunteering.activity'
    _description = "Center Activity"
    _order = 'name'
    _sql_constraints = [('name_uniq', 'UNIQUE (name,alias_name)', 'You cannot have two activities with same!')]

    name = fields.Char(string='Name')
    alias_name = fields.Char(string='Alias Name')


class VolunteeringLocal(models.Model):
    _name = 'isha.volunteer.local.volunteering'
    _inherit = 'isha.volunteer.volunteering.details'
    _description = 'Local Volunteering'
    _rec_name = 'activity'

    activity = fields.Many2one('local.volunteering.activity', string='Local Department')
    location = fields.Char(string='Location')


class LocalVolunteeringActivity(models.Model):
    _name = 'local.volunteering.activity'
    _description = "IYC Department"
    _order = 'name'
    _sql_constraints = [('name_uniq', 'UNIQUE (name,alias_name)', 'You cannot have two activities with same!')]

    name = fields.Char(string='Name')
    alias_name = fields.Char(string='Alias Name')


class IshaVolunteerFamilyDetails(models.Model):
    _name = 'isha.volunteer.family.details'
    _description = "Family Details"

    volunteer_id = fields.Many2one('isha.volunteer', string='Name', ondelete='cascade', required=True)
    name = fields.Char(string='Name')
    relation = fields.Selection([('Father', 'Father'), ('Mother', 'Mother'), ('Brother', 'Brother'),
                                 ('Sister', 'Sister'), ('Son', 'Son'), ('Daughter', 'Daughter'),
                                 ('Grandmother Maternal', 'Grandmother Maternal'),
                                 ('Grandfather Maternal', 'Grandfather Maternal'),
                                 ('Grandmother Paternal', 'Grandmother Paternal'),
                                 ('Grandfather Paternal', 'Grandfather Paternal'),
                                 ('Mother in law', 'Mother in law'),
                                 ('Father in law', 'Father in law'),
                                 ('Sister in law', 'Sister in law'),
                                 ('Brother in law', 'Brother in law'),
                                 ('Friend', 'Friend'),
                                 ('Colleague', 'Colleague'),
                                 ('Neighbour', 'Neighbour')], string='Relation')
    dob = fields.Date(string='DOB')
    age = fields.Integer(compute="_compute_age")
    gender = fields.Selection([('male', 'Male'), ('female', 'Female'), ('other', 'Other')], string='Gender')
    occupation = fields.Char(string='Occupation')
    contact_address = fields.Char(string='Contact address')
    phone_country_code = fields.Char(string='Phone Country Code')
    phone_number = fields.Char(string='Contact no')
    email = fields.Char(string='Email')
    is_meditator = fields.Selection(list(constants.BOOLEAN_VALUES.items()), string='Is Isha meditator')

    @api.depends('dob')
    def _compute_age(self):
        for record in self:
            record.age = None
            if record.dob:
                today = date.today()
                offset = int(record.dob.replace(year=today.year) > today)
                record.age = date.today().year - record.dob.year - offset


class EmergencyContact(models.Model):
    _name = 'isha.volunteer.emergency.contact'
    _description = 'Emergency Contact'

    volunteer_id = fields.Many2one('isha.volunteer', string='Volunteer', ondelete='cascade')
    name = fields.Char(string='Name')
    address = fields.Char(string='Address')
    phone = fields.Char(string='Phone')
    phone_country_code = fields.Char(string='Phone Country Code')
    email = fields.Char(string='Email')
    relation = fields.Char(string='Relation')
    emergency_type = fields.Selection([('primary', 'Primary'), ('secondary', 'Secondary')])


class ProgramHistory(models.Model):
    _name = 'isha.volunteer.program.history'
    _description = 'Program History'
    _rec_name = 'isha_program'
    # _sql_constraints = [('program_history_unique', 'UNIQUE (isha_program, program_date, volunteer_id)',
    #                      'The program is already entered for the participant')]

    volunteer_id = fields.Many2one('isha.volunteer', string='Volunteer', ondelete='cascade')
    isha_program = fields.Many2one('isha.programs', string='Program', required=True, ondelete='cascade')
    program_date = fields.Date(string='Date')
    location = fields.Char(string='Location')
    teacher = fields.Char(string='Teacher')
    frequency_of_practice = fields.Char(string='Frequency Of Practice')

class YatraHistory(models.Model):
    _name = 'isha.volunteer.yatra.history'
    _description = 'Yatra History'
    # _sql_constraints = [('yatra_history_unique', 'UNIQUE (yatra_name, volunteer_id)',
    #                      'The yatra is already entered for the participant')]

    volunteer_id = fields.Many2one('isha.volunteer', string='Volunteer', ondelete='cascade', required=True)
    yatra_name = fields.Many2one('isha.yatra.name', string='Yatra', required=True)
    yatra_date = fields.Date(string='Date', required=True)


class EventHistory(models.Model):
    _name = 'isha.volunteer.event.history'
    _description = 'Event History'
    # _sql_constraints = [('event_history_unique', 'UNIQUE (event_name, volunteer_id)',
    #                      'The event is already entered for the participant')]

    volunteer_id = fields.Many2one('isha.volunteer', string='Volunteer', ondelete='cascade', required=True)
    event_name = fields.Many2one('isha.event.name', string='Event')
    event_location = fields.Char(string='Location')
    event_date = fields.Date(string='Date')


class YatraName(models.Model):
    _name = 'isha.yatra.name'
    _description = 'Yatra Name'
    _order = 'name'
    _sql_constraints = [('name_uniq', 'UNIQUE (name)', 'You can not have two same type with the same name !')]

    name = fields.Char('Yatra Name')
    active = fields.Boolean('Status', default=True)


class EventName(models.Model):
    _name = 'isha.event.name'
    _description = 'Event Name'
    _order = 'name'
    _sql_constraints = [('name_uniq', 'UNIQUE (name)', 'You can not have two same type with the same name !')]

    name = fields.Char('Event Name')
    active = fields.Boolean('Status', default=True)


class IshaVolunteerSkill(models.Model):
    _name = 'isha.volunteer.skill'
    _description = 'Skills'
    _order = 'name'
    _sql_constraints = [
        ('name_uniq', 'UNIQUE (name, skill_function)', 'You can not have duplicate Skills in the same function!')]

    name = fields.Char(string='Skill Name', required=True, translate=True)
    skill_function = fields.Many2one('isha.volunteer.skill.function', string='Function',
                                     ondelete='cascade')
    active = fields.Boolean(default=True, help="The active field allows you to hide the category without removing it.")

    # color = fields.Integer(string='Color Index')
    # parent_id = fields.Many2one('isha.volunteer.skill', string='Skill Categories', index=True, ondelete='cascade')
    # parent_path = fields.Char(index=True)

    def name_get(self):
        result = []
        for record in self:
            rec_name = "%s / %s" % (record.skill_function.function, record.name)
            result.append((record.id, rec_name))
        return result


class IshaVolunteerSkillFunction(models.Model):
    _name = 'isha.volunteer.skill.function'
    _description = 'Functions'
    _rec_name = 'function'
    _sql_constraints = [('skill_function_unique', 'UNIQUE(function)', 'Function already added')]

    function = fields.Char(string='Functions')
