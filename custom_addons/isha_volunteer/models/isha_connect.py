from odoo import models, fields, api


class IshaConnect(models.Model):
    _name = "isha.connect"
    _description = "Isha Connect"

    name = fields.Char()
    active = fields.Boolean(string='Active', default=True)
