from odoo import fields, models


class Partner(models.Model):
    _inherit = 'res.partner'

    nationality_id = fields.Many2one('res.country', 'Nationality Id')
    isha_phone = fields.Char(string="CUG")
    # phone2 = fields.Char(string="Whats-app")
    # phone3 = fields.Char(string="Other")
    # phone4 = fields.Char(string="other")
    isha_volunteer_ids = fields.One2many('isha.volunteer', 'partner_id', string='Isha Volunteers')

