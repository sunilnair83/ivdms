odoo.define('isha_samskriti.batch_selection', function (require) {
    'use strict';

    var publicWidget = require('web.public.widget');

    publicWidget.registry.batchDetails = publicWidget.Widget.extend({
        // defined the selector and the event for the form
        selector: '.batch_selection_div',
        events: {
            'change select[name="programapplied"]': '_onProgramAppliedChange',
        },

        
        // the start function will get called in the very beginning as soon as the page is ready
        start: function () {
            var def = this._super.apply(this, arguments);

            // getting the batch element and the batch options
            this.$batch = this.$('select[name="batchapplied"]');
            this.$batchOptions = this.$batch.find('option:not(:first)');
            
            // the method will do the internal operation of hiding and showing the desired batches
            this._adaptBatchForm();
            
            return def;
        },
        
        _adaptBatchForm: function () {
            // getting the selected schedule and it's value
            var $schd = this.$('select[name="programapplied"]');
            var schdID = ($schd.val() || 0);

            
            this.$batchOptions.detach();
            
            // filtering the batches which only belongs to the selected schedule and appending them in the dropdown
            var $displayedBatches = this.$batchOptions.filter('[pbid=' + schdID + ']');
            var nb = $displayedBatches.appendTo(this.$batch).show().length;            
            this.$batch.parent().toggle(nb >= 1);
        },
        
        _onProgramAppliedChange: function(){
            // the method will do the internal operation of hiding and showing the desired batches
            this._adaptBatchForm();
        },
    });
    
});
