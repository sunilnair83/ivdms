from odoo import fields, models, tools


class ProgramQuickGlance(models.Model):
    _name = "samskriti.program_quick_glance"
    _description = "Samskriti Program Quick Glance"
    _auto = False

    program_name = fields.Many2one("program.type.master", string="Program Name")
    schedule_date_from = fields.Date("Date From")
    schedule_date_to = fields.Date("Date To")
    batch_timing = fields.Char("Batch Timings")
    total_seats = fields.Integer("Total Seats")
    seats_left = fields.Integer("Seats Left")

    def _query(self, with_clause='', fields={}, groupby='', from_clause=''):
        return '''
            SELECT BATCH.id as id,
            SCHD.PROGRAM_TYPE_ID "program_name",
            SCHD.START_DATE "schedule_date_from",
            SCHD.END_DATE "schedule_date_to",
			BATCH.DISPLAY_TEXT "batch_timing",
            BATCH.NO_OF_SEATS "total_seats",
            BATCH.SEATS_LEFT "seats_left"
            FROM PROGRAM_SCHEDULE_BATCH_MASTER BATCH,PROGRAM_SCHEDULE_MASTER SCHD  where SCHD.ID = BATCH.PROGRAM_SCHEDULE_ID
        '''


    def init(self):
        tools.drop_view_if_exists(self._cr, 'samskriti_program_quick_glance')

        self.env.cr.execute("""CREATE or REPLACE VIEW %s AS (%s)""" % (self._table, self._query()))
