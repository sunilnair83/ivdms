import logging
from odoo import api, models, fields

_logger = logging.getLogger(__name__)


class SamaskritiProgramPricingParameters(models.Model):
    _name = "samaskriti.pricing.parameters"
    _description = "Temple Program Pricing Parameters"
    _rec_name = "countrycur"

    programtype = fields.Many2one("program.type.master", string="Program Type")
    countryname = fields.Many2one(
        "res.country",
        string="Pricing Country",
        required=True,
    )
    countrycur = fields.Many2one("res.currency", "Currency", size=150)
    countrycost = fields.Float(string="Cost", required=True)
