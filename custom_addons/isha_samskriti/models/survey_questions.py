from odoo import fields, models, api, _
from odoo.addons.survey.models.survey_user import dict_keys_startswith
from datetime import datetime

import logging

from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)


class SurveyQuestions(models.Model):
    _inherit = "survey.question"

    sm_programType = fields.Many2one("program.type.master", string="Program Type")
    question_type = fields.Selection(
        selection_add=[
            ("terms_conditions", "Terms & Conditions"),
            ("health_disclaimer", "Health Disclaimer"),
        ]
    )
    sm_HtmlText = fields.Html(
        "Html Text",
        help="The field will hold the value needed to show against the t&c checkbox or health disclaimer",
    )
    sm_tcEntity = fields.Many2one("res.country.group", "Entity")

    @api.constrains("sm_tcEntity")
    def _constrains_sm_tcEntity(self):
        for record in self:
            if record.sm_tcEntity:
                conflict = self.search_count(
                    [
                        ("question_type", "=", "terms_conditions"),
                        ("sm_tcEntity.id", "=", record.sm_tcEntity.id),
                        ("sm_programType", "=", record.sm_programType.id),
                        ("id", "!=", record.id),
                    ]
                )

                if conflict:
                    raise UserError(
                        _(
                            "T&C and entity mapping should be unqiue, there can't be duplicates present."
                        )
                    )


class SurveyInputLine(models.Model):
    _inherit = "survey.user_input_line"

    user_input_id = fields.Many2one(
        "survey.user_input", string="User Input", ondelete="cascade", required=False
    )
    sm_programLead = fields.Many2one("program.lead", string="Program Type")
    sm_valueTerms = fields.Datetime(string="User Accepted Terms On")

    answer_type = fields.Selection(
        selection_add=[
            ("terms_conditions", "Terms & Conditions"),
            ("health_disclaimer", "Health Disclaimer"),
        ]
    )

    @api.model
    def sams_save_lines(self, user_input_id, question, post, answer_tag):
        """Save answers to questions, depending on question type

        If an answer already exists for question and user_input_id, it will be
        overwritten (in order to maintain data consistency).
        """
        try:
            saver = getattr(self, "custom_save_line_" + question.question_type)
        except AttributeError:
            _logger.error(
                question.question_type
                + ": This type of question has no saving function"
            )
            return False
        else:
            saver(user_input_id, question, post, answer_tag)

    @api.model
    def custom_save_line_health_disclaimer(
        self, user_input_id, question, post, answer_tag
    ):
        vals = {
            "user_input_id": user_input_id,
            "question_id": question.id,
            "skipped": False,
            "sm_programLead": post.get("program_lead_rec").id,
        }

        if post.get("agreeddate"):
            dt = datetime.fromisoformat(post.get("agreeddate")).strftime(
                "%Y-%m-%d %H:%M:%S"
            )
            vals.update(
                {
                    "answer_type": "health_disclaimer",
                    "sm_valueTerms": dt,
                }
            )

        old_uil = (
            question.survey_id
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("survey_id", "=", question.survey_id.id),
                    ("question_id", "=", question.id),
                ]
            )
            or post.get("program_lead_rec", False)
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("sm_programLead", "=", post.get("program_lead_rec").id),
                    ("question_id", "=", question.id),
                ]
            )
        )

        if old_uil:
            old_uil.write(vals)
        else:
            old_uil.create(vals)
        return True

    @api.model
    def custom_save_line_terms_conditions(
        self, user_input_id, question, post, answer_tag
    ):
        vals = {
            "user_input_id": user_input_id,
            "question_id": question.id,
            "skipped": False,
            "sm_programLead": post.get("program_lead_rec").id,
        }

        if answer_tag in post and post[answer_tag].strip():
            vals.update(
                {"answer_type": "terms_conditions", "sm_valueTerms": datetime.now()}
            )
        else:
            vals.update({"answer_type": None, "skipped": True})

        old_uil = (
            question.survey_id
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("survey_id", "=", question.survey_id.id),
                    ("question_id", "=", question.id),
                ]
            )
            or post.get("program_lead_rec", False)
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("sm_programLead", "=", post.get("program_lead_rec").id),
                    ("question_id", "=", question.id),
                ]
            )
        )

        if not vals.get("skipped"):
            if old_uil:
                old_uil.write(vals)
            else:
                old_uil.create(vals)

        return True

    @api.model
    def custom_save_line_free_text(self, user_input_id, question, post, answer_tag):
        vals = {
            "user_input_id": user_input_id,
            "question_id": question.id,
            "skipped": False,
        }

        # the two if methods below add ref to survey id and program lead to the answer
        if question.survey_id:
            vals.update({"survey_id": question.survey_id.id})

        if post.get("program_lead_rec", False):
            vals.update({"sm_programLead": post.get("program_lead_rec").id})

        if answer_tag in post and post[answer_tag].strip():
            vals.update(
                {"answer_type": "free_text", "value_free_text": post[answer_tag]}
            )
        else:
            vals.update({"answer_type": None, "skipped": True})

        old_uil = (
            question.survey_id
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("survey_id", "=", question.survey_id.id),
                    ("question_id", "=", question.id),
                ]
            )
            or post.get("program_lead_rec", False)
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("sm_programLead", "=", post.get("program_lead_rec").id),
                    ("question_id", "=", question.id),
                ]
            )
        )

        if old_uil:
            old_uil.write(vals)
        else:
            old_uil.create(vals)
        return True

    @api.model
    def custom_save_line_textbox(self, user_input_id, question, post, answer_tag):
        vals = {
            "user_input_id": user_input_id,
            "question_id": question.id,
            "skipped": False,
        }

        # the two if methods below add ref to survey id and program lead to the answer
        if question.survey_id:
            vals.update({"survey_id": question.survey_id.id})

        if post.get("program_lead_rec", False):
            vals.update({"sm_programLead": post.get("program_lead_rec").id})

        if answer_tag in post and post[answer_tag].strip():
            vals.update({"answer_type": "text", "value_text": post[answer_tag]})
        else:
            vals.update({"answer_type": None, "skipped": True})
        old_uil = (
            question.survey_id
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("survey_id", "=", question.survey_id.id),
                    ("question_id", "=", question.id),
                ]
            )
            or post.get("program_lead_rec", False)
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("sm_programLead", "=", post.get("program_lead_rec").id),
                    ("question_id", "=", question.id),
                ]
            )
        )
        if old_uil:
            old_uil.write(vals)
        else:
            old_uil.create(vals)
        return True

    @api.model
    def custom_save_line_numerical_box(self, user_input_id, question, post, answer_tag):
        vals = {
            "user_input_id": user_input_id,
            "question_id": question.id,
            "skipped": False,
        }

        # the two if methods below add ref to survey id and program lead to the answer
        if question.survey_id:
            vals.update({"survey_id": question.survey_id.id})

        if post.get("program_lead_rec", False):
            vals.update({"sm_programLead": post.get("program_lead_rec").id})

        if answer_tag in post and post[answer_tag].strip():
            vals.update(
                {"answer_type": "number", "value_number": float(post[answer_tag])}
            )
        else:
            vals.update({"answer_type": None, "skipped": True})
        old_uil = (
            question.survey_id
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("survey_id", "=", question.survey_id.id),
                    ("question_id", "=", question.id),
                ]
            )
            or post.get("program_lead_rec", False)
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("sm_programLead", "=", post.get("program_lead_rec").id),
                    ("question_id", "=", question.id),
                ]
            )
        )
        if old_uil:
            old_uil.write(vals)
        else:
            old_uil.create(vals)
        return True

    @api.model
    def custom_save_line_date(self, user_input_id, question, post, answer_tag):
        vals = {
            "user_input_id": user_input_id,
            "question_id": question.id,
            "skipped": False,
        }

        # the two if methods below add ref to survey id and program lead to the answer
        if question.survey_id:
            vals.update({"survey_id": question.survey_id.id})

        if post.get("program_lead_rec", False):
            vals.update({"sm_programLead": post.get("program_lead_rec").id})

        if answer_tag in post and post[answer_tag].strip():
            date = datetime.strptime(
                post[answer_tag].split(" ")[0], "%m/%d/%Y"
            ).strftime("%Y-%m-%d")
            vals.update({"answer_type": "date", "value_date": date})
        else:
            vals.update({"answer_type": None, "skipped": True})

        old_uil = (
            question.survey_id
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("survey_id", "=", question.survey_id.id),
                    ("question_id", "=", question.id),
                ]
            )
            or post.get("program_lead_rec", False)
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("sm_programLead", "=", post.get("program_lead_rec").id),
                    ("question_id", "=", question.id),
                ]
            )
        )
        if old_uil:
            old_uil.write(vals)
        else:
            old_uil.create(vals)
        return True

    @api.model
    def custom_save_line_datetime(self, user_input_id, question, post, answer_tag):
        vals = {
            "user_input_id": user_input_id,
            "question_id": question.id,
            "skipped": False,
        }

        # the two if methods below add ref to survey id and program lead to the answer
        if question.survey_id:
            vals.update({"survey_id": question.survey_id.id})

        if post.get("program_lead_rec", False):
            vals.update({"sm_programLead": post.get("program_lead_rec").id})

        if answer_tag in post and post[answer_tag].strip():
            datetim = datetime.strptime(
                post[answer_tag].strip(), "%m/%d/%Y %I:%M %p"
            ).strftime("%Y-%m-%d %H:%M:%S")
            vals.update({"answer_type": "datetime", "value_datetime": datetim})

        else:
            vals.update({"answer_type": None, "skipped": True})
        old_uil = (
            question.survey_id
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("survey_id", "=", question.survey_id.id),
                    ("question_id", "=", question.id),
                ]
            )
            or post.get("program_lead_rec", False)
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("sm_programLead", "=", post.get("program_lead_rec").id),
                    ("question_id", "=", question.id),
                ]
            )
        )
        if old_uil:
            old_uil.write(vals)
        else:
            old_uil.create(vals)
        return True

    @api.model
    def custom_save_line_simple_choice(self, user_input_id, question, post, answer_tag):
        vals = {
            "user_input_id": user_input_id,
            "question_id": question.id,
            "skipped": False,
        }

        # the two if methods below add ref to survey id and program lead to the answer
        if question.survey_id:
            vals.update({"survey_id": question.survey_id.id})

        if post.get("program_lead_rec", False):
            vals.update({"sm_programLead": post.get("program_lead_rec").id})

        old_uil = (
            question.survey_id
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("survey_id", "=", question.survey_id.id),
                    ("question_id", "=", question.id),
                ]
            )
            or post.get("program_lead_rec", False)
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("sm_programLead", "=", post.get("program_lead_rec").id),
                    ("question_id", "=", question.id),
                ]
            )
        )
        old_uil.sudo().unlink()

        if answer_tag in post and post[answer_tag].strip():
            vals.update(
                {"answer_type": "suggestion", "value_suggested": int(post[answer_tag])}
            )
        else:
            vals.update({"answer_type": None, "skipped": True})

        # '-1' indicates 'comment count as an answer' so do not need to record it
        if post.get(answer_tag) and post.get(answer_tag) != "-1":
            self.create(vals)

        comment_answer = post.pop(("%s_%s" % (answer_tag, "comment")), "").strip()
        if comment_answer:
            vals.pop("answer_score", False)
            vals.update(
                {
                    "answer_type": "text",
                    "value_text": comment_answer,
                    "skipped": False,
                    "value_suggested": False,
                }
            )
            self.create(vals)

        return True

    @api.model
    def custom_save_line_multiple_choice(
        self, user_input_id, question, post, answer_tag
    ):
        vals = {
            "user_input_id": user_input_id,
            "question_id": question.id,
            "skipped": False,
        }
        # the two if methods below add ref to survey id and program lead to the answer
        if question.survey_id:
            vals.update({"survey_id": question.survey_id.id})

        if post.get("program_lead_rec", False):
            vals.update({"sm_programLead": post.get("program_lead_rec").id})

        old_uil = (
            question.survey_id
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("survey_id", "=", question.survey_id.id),
                    ("question_id", "=", question.id),
                ]
            )
            or post.get("program_lead_rec", False)
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("sm_programLead", "=", post.get("program_lead_rec").id),
                    ("question_id", "=", question.id),
                ]
            )
        )
        old_uil.sudo().unlink()

        ca_dict = dict_keys_startswith(post, answer_tag + "_")
        comment_answer = ca_dict.pop(("%s_%s" % (answer_tag, "comment")), "").strip()
        if len(ca_dict) > 0:
            for key in ca_dict:
                # '-1' indicates 'comment count as an answer' so do not need to record it
                if key != ("%s_%s" % (answer_tag, "-1")):
                    val = ca_dict[key]
                    vals.update(
                        {
                            "answer_type": "suggestion",
                            "value_suggested": bool(val) and int(val),
                        }
                    )
                    self.create(vals)
        if comment_answer:
            vals.update(
                {
                    "answer_type": "text",
                    "value_text": comment_answer,
                    "value_suggested": False,
                }
            )
            self.create(vals)
        if not ca_dict and not comment_answer:
            vals.update({"answer_type": None, "skipped": True})
            self.create(vals)
        return True

    @api.model
    def custom_save_line_matrix(self, user_input_id, question, post, answer_tag):
        vals = {
            "user_input_id": user_input_id,
            "question_id": question.id,
            "skipped": False,
        }
        # the two if methods below add ref to survey id and program lead to the answer
        if question.survey_id:
            vals.update({"survey_id": question.survey_id.id})

        if post.get("program_lead_rec", False):
            vals.update({"sm_programLead": post.get("program_lead_rec").id})

        old_uil = (
            question.survey_id
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("survey_id", "=", question.survey_id.id),
                    ("question_id", "=", question.id),
                ]
            )
            or post.get("program_lead_rec", False)
            and self.search(
                [
                    ("user_input_id", "=", user_input_id),
                    ("sm_programLead", "=", post.get("program_lead_rec").id),
                    ("question_id", "=", question.id),
                ]
            )
        )
        old_uil.sudo().unlink()

        no_answers = True
        ca_dict = dict_keys_startswith(post, answer_tag + "_")

        comment_answer = ca_dict.pop(("%s_%s" % (answer_tag, "comment")), "").strip()
        if comment_answer:
            vals.update({"answer_type": "text", "value_text": comment_answer})
            self.create(vals)
            no_answers = False

        if question.matrix_subtype == "simple":
            for row in question.labels_ids_2:
                a_tag = "%s_%s" % (answer_tag, row.id)
                if a_tag in ca_dict:
                    no_answers = False
                    vals.update(
                        {
                            "answer_type": "suggestion",
                            "value_suggested": ca_dict[a_tag],
                            "value_suggested_row": row.id,
                        }
                    )
                    self.create(vals)

        elif question.matrix_subtype == "multiple":
            for col in question.labels_ids:
                for row in question.labels_ids_2:
                    a_tag = "%s_%s_%s" % (answer_tag, row.id, col.id)
                    if a_tag in ca_dict:
                        no_answers = False
                        vals.update(
                            {
                                "answer_type": "suggestion",
                                "value_suggested": col.id,
                                "value_suggested_row": row.id,
                            }
                        )
                        self.create(vals)
        if no_answers:
            vals.update({"answer_type": None, "skipped": True})
            self.create(vals)
        return True
