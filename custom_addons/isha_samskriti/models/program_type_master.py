from odoo import models, fields, api


class ProgramTypeMaster(models.Model):
    _inherit = "program.type.master"

    sm_questions = fields.One2many(
        "survey.question", "sm_programType", string="Questions"
    )
    sm_programPricing = fields.One2many(
        "samaskriti.pricing.parameters", "programtype", string="Pricing"
    )
    sm_isSamskriti = fields.Boolean("Is Samskriti", compute="_compute_is_samskriti")
    sm_priceCount = fields.Integer(compute="_compute_prices_count")
    sm_seqPref = fields.Char("Sequence Prefix")

    def _compute_is_samskriti(self):
        for rec in self:
            rec.sm_isSamskriti = bool(self._context.get("default_category", False))

    def _compute_prices_count(self):
        for rec in self:
            rec.sm_priceCount = (
                self.env["samaskriti.pricing.parameters"]
                .sudo()
                .search_count([("programtype", "=", rec.id)])
                or 0
            )

    def get_pricing(self):
        self.ensure_one()
        action = self.env.ref("isha_samskriti.action_samskriti_pricing_params").read()[
            0
        ]

        action["context"] = {"default_programtype": self.id}

        action["domain"] = [("programtype.id", "=", self.id)]

        return action
