import logging

import base64
from odoo import models, fields, _
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)

class SamskritiMoveRegister(models.TransientModel):
    _name = 'samskriti.move.register'
    _description = 'Samskriti Move Register'

    register_id = fields.Char()
    lead_name = fields.Char()
    move_email = fields.Char()
    previous_mail = fields.Char()
    move_registration_link = fields.Char()
    schedule_date = fields.Char()
    schedule_date_time = fields.Char()
    programname = fields.Char()

    # Move Registration Link Send
    def move_register_link_Send(self):
        program_lead_obj = self.env['program.lead']
        register_rec = program_lead_obj.search([('id','=',self.register_id)])

        register_rec.sudo().write({
            "reg_status": "Waiting for approval",
            "transfer_status":'Name transfer',
            "name_transfer_id":self.id
        })

        # if register_rec.reg_status in ["Name Transfer Confirmed"]:
        #     raise ValidationError(_("Name transfer is already done once of this record, you can't request it again."))
        #
        # programname = register_rec.sm_scheduleId.program_type_id.program_type
        #
        # base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        # redirect_url = f"{base_url}/registrations/samskriti/transfer?programname={programname}&rec_id={register_rec.id}&is_name_transfer={True}"
        # # application_data = 'rec_id=' + str(register_rec.id) + '&move_email=' + str(move_email) + '&programname=' + str(programname) +'&is_name_transfer=True'
        # # redirect_url = base_url + "/smaskriti/move/registrationform?" + application_data
        # _logger.info("******* Move Registration Link URL ****"+str(redirect_url))
        #
        # # encode_application_data = base64.b64encode(application_data.encode("UTF-8"))
        # # s1 = encode_application_data.decode("UTF-8")
        # # redirect_url = base_url + "/registrations/samskriti/transfer?" + application_data
        # # redirect_url = base_url + "/kshetragna/registrationform/documentpage?registration_id=" + str(register_rec.id)+'&passport='+str(passport)+'&pan='+str(pan)+'&oca_card='+str(oca_card)
        # self.move_registration_link = redirect_url
        # self.programname = programname
        # self.move_email = self.move_email
        # register_rec.sudo().write({
        #     "reg_status":"Name Transfer Requested",
        #     "transfer_email":self.move_email,
        #     "move_registration_link": redirect_url,
        #     # "move_url":url
        #     # "move_registration_link_sent_date":fields.Date.today()
        # })
        # mail_template_id = self.env.ref('isha_samskriti.move_registration_mail_template')
        #
        # try:
        #     mail_rec = self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(self.id, force_send=True)
        #     _logger.info("***** Mail record ID - Move Register ******" + str(mail_rec))
        #
        # except Exception as e:
        #     print(e)
        #
        # return True
