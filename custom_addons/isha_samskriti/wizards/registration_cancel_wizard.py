from odoo import models, fields, api, exceptions
import base64
import logging


class SamskritiRegisterCancel(models.TransientModel):
    _name = 'samskriti.register.cancel'
    _description = 'Samskriti Register Cancel'

    register_id = fields.Char()
    remarks = fields.Text()

    # Move Registration Link Send
    def cancel_register_form(self):
        program_lead_obj = self.env['program.lead']
        register_rec = program_lead_obj.search([('id','=',self.register_id)])
        register_rec.sudo().write({
            'remarks':self.remarks,
            'reg_status':'Cancelled'
        })
        # Seats value updated for Existing Record while cancel the Program
        sm_batch_id_rec = self.env['program.schedule.batch.master'].search([('id', '=', register_rec.sm_batchId.id)])
        for i in sm_batch_id_rec:
            current_seats_left = i.seats_left
            seats_left = current_seats_left + 1
            i.sudo().write({'seats_left': seats_left})

        return True
