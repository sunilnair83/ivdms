import datetime
import logging

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class ProgramTransfer(models.TransientModel):
    _name = "isha_samskriti.program_transfer"
    _description = "Isha Samskriti Program Transfer Wizard"

    @api.model
    def default_get(self, fields):
        '''
        The method gets the fields and sets their default values

        :param fields: field list
        :return: dict
        '''
        result = super(ProgramTransfer, self).default_get(fields)
        lead_id = self.env.context.get('active_id')
        if lead_id:
            lead_rec = self.env['program.lead'].sudo().browse(lead_id)
            result['name'] = lead_rec.id
            result['current_program'] = lead_rec.sm_scheduleId.program_type_id.local_program_type_id
            result[
                'current_schedule'] = f"{lead_rec.sm_scheduleId.start_date.strftime('%d %B %Y')}-{lead_rec.sm_scheduleId.end_date.strftime('%d %B %Y')}"
            result['current_batch'] = lead_rec.sm_batchId.display_text
            result['target_program'] = lead_rec.sm_scheduleId.program_type_id.id

        return result

    name = fields.Many2one("program.lead", string="Current Record")
    transfer_type = fields.Selection(
        [("InterProgram", "Schedule Transfer Without Fee"), ("IntraProgram", "Program Transfer")],
        string="Transfer Type", default="InterProgram")

    current_program = fields.Char("Current Program", readonly=True)
    current_schedule = fields.Char("Current Schedule", readonly=True)
    current_batch = fields.Char("Current Batch", readonly=True)

    target_program = fields.Many2one("program.type.master", string="Target Program", store=True)
    target_schedule = fields.Many2one("program.schedule.master", string="Target Schedule",
                                      domain="[('program_type_id','=',target_program)]")

    target_schedule_date = fields.Char("Target Schedule Start Date", readonly=True)

    target_batch = fields.Many2one("program.schedule.batch.master", string="Target Batch",
                                   domain="[('program_schedule_id','=',target_schedule)]")

    show_warning = fields.Boolean('Show Warning', default=False)
    reg_count = fields.Char(compute="_compute_reg_count", store=True)

    amount = fields.Float("Amount Delta")
    currency = fields.Char()

    @api.depends('target_batch')
    def _compute_reg_count(self):
        for rec in self:
            rec.reg_count = f"the registration count for this batch will be {(rec.target_batch.no_of_seats - rec.target_batch.seats_left) + 1}"

    @api.onchange('transfer_type')
    def _onchange_transfer_type(self):
        '''
        the onchange method is there to remove any selections in case user changes transfer type

        :return: None
        '''
        for record in self:
            record.show_warning = False
            if record.transfer_type == 'InterProgram':
                record.write({
                    'target_program': record.name.sm_scheduleId.program_type_id.id,
                    'target_schedule': False,
                    'target_batch': False,
                    'target_schedule_date': False

                })
            else:
                record.write({
                    'target_schedule': False,
                    'target_batch': False,
                    'target_schedule_date': False
                })

    def _get_smas_pgm_ids(self):
        '''
        Method returns the list of program type ids

        :return: List
        '''
        return [
            self.env.ref("isha_samskriti.kalaripayattu_prog_type").id,
            self.env.ref("isha_samskriti.nirvana_shatakam_prog_type").id,
            self.env.ref("isha_samskriti.niraive_shakti_prog_type").id,
            self.env.ref("isha_samskriti.nirbhay_nirgun_prog_type").id,
        ]

    @api.onchange('target_schedule')
    def _onchange_target_schedule(self):
        '''
        the onchange method triggers a new domain for the batches

        :return: dict
        '''
        for rec in self:
            rec.show_warning = False
            rec.target_batch = False

            if rec.target_schedule:
                batches = self.get_batches(rec, rec.target_schedule)
                rec.target_schedule_date = f"{rec.target_schedule.start_date.strftime('%d %B %Y')}-{rec.target_schedule.end_date.strftime('%d %B %Y')}"
                res = {
                    'domain': {
                        'target_batch': [('id', 'in', batches.ids),
                                         ('program_schedule_id', '=', rec.target_schedule.id)],
                    }
                }
                payment_transaction_rec = self.env['samaskriti.participant.paymenttransaction'].sudo().search([("orderid", "=", rec.name.local_trans_id),("transactiontype", "=", "Request to Gateway")], limit=1)
                self.currency = payment_transaction_rec.currency
                return res

    @api.onchange('target_program')
    def _onchange_target_program(self):
        '''
        the on change method gets triggered on the change of the program,
        the method sets domain for the schedule
        and the batches based on the selected program

        :return: dict
        '''
        for record in self:
            if record.target_program:
                show_warning = False

                # extracting the schedules which are in the date range of the previously selected schedules
                program_schedules = self.env["program.schedule.master"].sudo().search([
                    ("program_type_id", "=", record.target_program.id),
                    ("start_date", ">", datetime.datetime.now()),
                ])

                # getting the batches based on the schedules that are left after filtering
                batches = self.get_batches(record, program_schedules)

                # finally filtering the schedules for the last time to make sure that
                # no schedules with conflicting or zero seats are there
                program_schedules = program_schedules.filtered(
                    lambda x: x.id in batches.mapped("program_schedule_id.id")
                )

                # showing an error if no schedules or batches are present
                if not program_schedules or not batches:
                    show_warning = True

                # creating the domain dictionary to return
                res = {
                    'domain': {
                        'target_schedule': [('id', 'in', program_schedules.ids),
                                            ('program_type_id', '=', record.target_program.id)],
                        'target_batch': [('id', 'in', batches.ids)],
                    }
                }

                record.write({
                    'show_warning': show_warning,
                    'target_schedule': False,
                    'target_batch': False
                })

                return res

    def get_batches(self, record, program_schedules):
        '''
        the method returns the batches which aren't conflicting to passed schedules

        :param record: the current transient record
        :param program_schedules: the schedule(s) for which the batches will be tested
        :param schd: the currently registered schedule
        :return: recordset of program.schedule.batch.master
        '''

        # checking the batches for which the user has already
        # registered and fetching the conflicting batches from thme
        existing_rec = self.env["program.lead"].sudo().search([
            ("sso_id", "=", record.name.sso_id),
            ("sm_scheduleId.start_date", ">=", datetime.datetime.now()),
            ("pgm_type_master_id", "in", self._get_smas_pgm_ids()),
            ("id", "!=", record.name.id),
            ("reg_status", "not in", ["In Progress", "Registered", "Failed", "Schedule Transferred","Cancelled"])
        ])

        # extracting the batches from the schedules we recieved as an argument
        batches = program_schedules.no_of_batch_ids

        # filtering the batches which doesn't have any seats left
        if batches:
            batches = batches.filtered(lambda x: x.id != record.name.sm_batchId.id)

        # filtering out the conflicting batches
        if existing_rec:
            batches = batches - existing_rec.mapped('sm_batchId') - existing_rec.mapped("sm_batchId.conflicts")

        return batches

    def action_done(self):
        '''
        The action gets called when the user submits the form

        :return: newly created record
        '''
        for rec in self:
            # transfer_type = "InterProgram"
            if rec.transfer_type == "InterProgram":
                # extracting the local trans id
                _logger.info("--------Schedule transfer-------")
                current_local_trans_id = rec.name.local_trans_id

                # fetching the code for program type
                sams_program_type_music = self.env["ir.config_parameter"].sudo().get_param(
                    "samaskriti.programcode.music")
                sams_program_type_kalari = self.env["ir.config_parameter"].sudo().get_param(
                    "samaskriti.programcode.kalari")

                # setting program type based on the program
                if rec.target_program.local_program_type_id == 'Kalaripayattu':
                    sams_program_type = sams_program_type_kalari
                elif rec.target_program.local_program_type_id == 'Nirvana Shatakam':
                    sams_program_type = sams_program_type_music
                elif rec.target_program.local_program_type_id == 'Niraive Shakti':
                    sams_program_type = sams_program_type_music
                else:
                    sams_program_type = sams_program_type_music

                # creating new local transaction id
                new_local_trans_id = f"{'T'}-{rec.name.sso_id}-{sams_program_type}-{rec.target_schedule.id}-{rec.target_batch.id}"

                # fetching the payment transaction record in to get information
                # like previously paid amount billing country etc
                payment_transaction_rec = self.env['samaskriti.participant.paymenttransaction'].sudo().search(
                    [('orderid', '=', current_local_trans_id), ('transactiontype', '=', 'Request to Gateway')])

                # without payment transaction record we won't proceed further
                if not payment_transaction_rec:
                    raise ValidationError(_("No payment transaction record found for current user."))

                # fetching the pricing record for the newly selected program and checking the amount delta
                pricing_rec = self.env['samaskriti.pricing.parameters'].sudo().search([
                    ("countryname.code", "=", payment_transaction_rec.billing_country),
                    ("programtype.id", "=", rec.target_program.id),
                ])

                amount_delta = rec.amount

                # setting status based on the amount delta
                # if amount_delta < 0:
                #     status = "Registered"
                # elif amount_delta > 0:
                #     status = "Refund Needed"
                # else:
                #     status = "Confirmed"

                # creating a new record using sudo to bypass the restrictions on program lead
                new_rec = rec.name.sudo().copy(
                    {
                        'reg_status': "Confirmed",
                        'amount_delta': abs(amount_delta),
                        'transfer_currency': payment_transaction_rec.currency,
                        'transfer_local_trans_id': new_local_trans_id,
                        'local_trans_id': new_local_trans_id,
                        'transfer_rec': rec.name.id,
                        'local_program_type_id': rec.target_program.local_program_type_id,
                        'pgm_type_master_id': rec.target_program.id,
                        'sm_scheduleId': rec.target_schedule.id,
                        'sm_batchId': rec.target_batch.id
                    }
                )

                # updating the batch and schedule in the current payment record if the amount delta is zero
                schedule = new_rec.sm_scheduleId.start_date.strftime(
                    '%d %B %Y') + ' to ' + rec.name.sm_scheduleId.end_date.strftime('%d %B %Y')
                batch = new_rec.sm_batchId.display_text

                payment_transaction_rec.write({
                    "batch_details": batch,
                    "schedule_details": schedule
                })

                # incrementing the seat in the old batch and decrementing the seat in current batch
                rec.name.sm_batchId.seats_left += 1
                rec.target_batch.seats_left -= 1

                # updating the older record
                if rec.transfer_type == 'InterProgram':
                    status = 'Schedule Transferred'
                else:
                    status = 'Program Transferred'
                rec.name.update({
                    'reg_status': status,
                    'transfer_rec': new_rec.id,
                    'trans_rec_id': rec.id
                })

                return new_rec
            else:
                # Record_country value updated to Program Lead table while hitting the program transfer confirm button
                local_trans_id = rec.name.local_trans_id
                payment_Rec = self.env['samaskriti.participant.paymenttransaction'].sudo().search([('orderid', '=', local_trans_id), ('transactiontype', '=', 'Request to Gateway')], limit=1)
                if not rec.name.record_country and payment_Rec:
                    country_rec = self.env['res.country'].sudo().search([('code','=',payment_Rec.billing_country)])
                    rec.name.record_country = str(country_rec.id)

                # checking if the user selected same batch for intra transfer
                if rec.target_program.id == rec.name.pgm_type_master_id.id:
                    pass
                    # commenting the validation because a schedule transfer is needed with extra payment from user
                    # raise ValidationError(_("For intra program transfers, the target program and the current program has to be different"))


                # creating a record in trans program table
                if payment_Rec:
                    currency = payment_Rec.currency

                trans_rec = self.env['samskriti.program.transfers.history'].sudo().create({
                    'name': rec.name.id,
                    'current_schedule': rec.current_schedule,
                    'current_program': rec.current_program,
                    'current_batch': rec.current_batch,
                    'current_program_id': rec.name.pgm_type_master_id.id,
                    'current_schedule_id': rec.name.sm_scheduleId.id,
                    'current_batch_id': rec.name.sm_batchId.id,
                    'target_program': rec.target_program.id,
                    'target_schedule': rec.target_schedule.id,
                    'target_schedule_date': rec.target_schedule_date,
                    'target_batch': rec.target_batch.id,
                    'amount': rec.amount,
                    'currency': currency if currency else 'INR',
                    'record_email': rec.name.record_email,
                    'record_name': rec.name.record_name,
                    "program_reg_id": rec.id,
                })

                # # creating the re-registration url using the user inputs
                # base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
                # url = f"{base_url}/registrations/samskriti/transfer?programname={rec.target_program.local_program_type_id}&schd={rec.target_schedule.id}&batch={rec.target_batch.id}&istransfer={True}&trans_rec={trans_rec.id}"
                # trans_rec.reg_link = url
                # _logger.info("********** Trans rec- Email ************" + str(trans_rec))
                # _logger.info("********** Trans rec- Email ID************" + str(trans_rec.record_email))
                # # fetching the mail template and sending the re-registration email
                # mail_template_id = self.env.ref("isha_samskriti.mail_template_pgm_transfer")
                # mail_rec=self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(trans_rec.id, force_send=True)
                #
                # _logger.info("***** Transac Mail record ID ******" + str(mail_rec))
                # updating the status for the current record
                rec.name.reg_status = 'Waiting for approval'
                rec.name.transfer_status = 'Program transfer'
                rec.name.trans_rec_id = trans_rec.id
                rec.name.transfer_currency = currency if currency else 'INR',
