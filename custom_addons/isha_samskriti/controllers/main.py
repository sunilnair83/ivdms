import hashlib
import hmac
import json
import logging
import traceback
from _datetime import datetime, timedelta

import requests
import werkzeug

from odoo import http
from odoo.http import request
import base64

import odoo.addons.isha_crm.ContactProcessor as ContactProcessor

# import google_auth_oauthlib.flow

_logger = logging.getLogger(__name__)


def hash_hmac(data, secret):
    data_enc = json.dumps(data["session_id"], sort_keys=True, separators=(",", ":"))
    signature = hmac.new(
        bytes(secret, "utf-8"), bytes(data_enc, "utf-8"), digestmod=hashlib.sha256
    ).hexdigest()
    return signature


# Api to get the profile data
def sso_get_user_profile(url, payload):
    dp = {"data": json.dumps(payload)}
    url = (
        request.env["ir.config_parameter"]
        .sudo()
        .get_param("satsang.SSO_LOGIN_INFO_ENDPOINT1")
    )
    return requests.post(url, data=dp)


def get_fullprofileresponse_DTO():
    return {
        "profileId": "",
        "basicProfile": {
            "createdBy": "",
            "createdDate": "",
            "lastModifiedBy": "",
            "lastModifiedDate": "",
            "id": 0,
            "profileId": "",
            "email": "",
            "firstName": "",
            "lastName": "",
            "profilePic": "",
            "phone": {"countryCode": "", "number": ""},
            "gender": "",
            "dob": "",
            "countryOfResidence": "",
        },
        "extendedProfile": {
            "createdBy": "",
            "createdDate": "",
            "lastModifiedBy": "",
            "lastModifiedDate": "",
            "id": 0,
            "passportNum": "",
            "nationality": "",
            "pan": "",
            "profileId": "",
        },
        "documents": [],
        "attendedPrograms": [],
        "profileSettingsConfig": {
            "createdBy": "",
            "createdDate": "",
            "lastModifiedBy": "",
            "lastModifiedDate": "",
            "id": 0,
            "nameLocked": False,
            "isPhoneVerified": False,
            "phoneVerificationDate": None,
            "profileId": "",
        },
        "addresses": [
            {
                "createdBy": "",
                "createdDate": "",
                "lastModifiedBy": "",
                "lastModifiedDate": "",
                "id": 0,
                "addressType": "",
                "addressLine1": "",
                "addressLine2": "",
                "townVillageDistrict": "",
                "city": "",
                "state": "",
                "country": "",
                "pincode": "",
                "profileId": "",
            }
        ],
    }


# Api to get full profile data
def sso_get_full_data(sso_id):
    request_url = (
        request.env["ir.config_parameter"].sudo().get_param("satsang.SSO_PMS_ENDPOINT1")
        + "full-profiles/"
        + sso_id
    )
    authreq = (
        request.env["ir.config_parameter"]
        .sudo()
        .get_param("satsang.SSO_API_SYSTEM_TOKEN1")
    )
    headers = {
        "Authorization": "Bearer " + authreq,
        "X-legal-entity": "IF",
        "x-user": sso_id,
    }
    profile = requests.get(request_url, headers=headers)
    return profile.json()


def _get_smas_pgm_ids():
    return [
        request.env.ref("isha_samskriti.kalaripayattu_prog_type").id,
        request.env.ref("isha_samskriti.nirvana_shatakam_prog_type").id,
        request.env.ref("isha_samskriti.niraive_shakti_prog_type").id,
        request.env.ref("isha_samskriti.nirbhay_nirgun_prog_type").id,
    ]


class Smaskriti(http.Controller):
    system_id = 44

    def _create_partner_rec(self, msg, local_trans_id):
        """
        The method creates the res.partner record
        :param msg: the sso profile dict
        :return: res.partner record
        """
        sso_id = msg["profileId"]
        basic_profile = ContactProcessor.replaceEmptyString(msg["basicProfile"])
        phone_dict = ContactProcessor.replaceEmptyString(msg["basicProfile"]["phone"])
        extended_profile = ContactProcessor.replaceEmptyString(msg["extendedProfile"])

        name = basic_profile["firstName"]
        if basic_profile["lastName"] and len(basic_profile["lastName"]) > 0:
            name = name + " " + basic_profile["lastName"]
        phone = None
        if phone_dict["number"]:
            phone = "+" + phone_dict["countryCode"] if phone_dict["countryCode"] else ""
            phone += phone_dict["number"]
        whatsapp_cc = None
        whatsapp_number = None
        if basic_profile.get("whatsapp", False):
            whatsapp_cc = basic_profile.get("whatsapp").get("countryCode", None)
            whatsapp_number = basic_profile.get("whatsapp").get("number", None)

        email = basic_profile["email"]
        gender = ContactProcessor.getgender(basic_profile["gender"])
        dob = basic_profile["dob"] if basic_profile["dob"] else None
        nationality = extended_profile["nationality"]
        passport = (
            extended_profile["passportNum"] if extended_profile["passportNum"] else None
        )
        pan = extended_profile["pan"] if extended_profile["pan"] else None
        street = None
        street2 = None
        state = None
        city = None
        country = None
        zip = None
        if len(msg["addresses"]) > 0:
            address_dict = ContactProcessor.replaceEmptyString(msg["addresses"][0])
            street = address_dict["addressLine1"]
            street2 = address_dict["addressLine2"]
            if address_dict["townVillageDistrict"]:
                if street2 and type(street2) == str:
                    street2 = street2 + ", " + address_dict["townVillageDistrict"]
                else:
                    street2 = address_dict["townVillageDistrict"]
            city = address_dict["city"]
            state = address_dict["state"]
            country = address_dict["country"]
            zip = address_dict["pincode"]

        odooRec = {
            "system_id": self.system_id,
            "local_contact_id": local_trans_id,
            "active": True,
            "name": name,
            "display_name": name,
            "prof_dr": None,
            "phone": phone,
            "phone2": None,
            "phone3": None,
            "phone4": None,
            "whatsapp_number": whatsapp_number,
            "whatsapp_country_code": whatsapp_cc,
            "email": email,
            "email2": None,
            "email3": None,
            "street": street,
            "street2": street2,
            "city": city,
            "state": state,
            "country": country,
            "zip": zip,
            "street2_1": None,
            "street2_2": None,
            "city2": None,
            "state2": None,
            "country2": None,
            "zip2": None,
            "work_street1": None,
            "work_street2": None,
            "work_city": None,
            "work_state": None,
            "work_country": None,
            "work_zip": None,
            "gender": gender,
            "occupation": None,
            "marital_status": None,
            "dob": dob,
            "nationality": nationality,
            "deceased": None,
            "contact_type": None,
            "companies": None,
            "aadhaar_no": None,
            "pan_no": pan,
            "passport_no": passport,
            "dnd_phone": None,
            "dnd_email": None,
            "dnd_postmail": None,
            "sso_id": sso_id,
        }

        odooRec = ContactProcessor.validateValues(odooRec, {}, {})

        # Switcing to Cron based contact assignment
        # rec = request.env["res.partner"].sudo().create(odooRec)
        # return rec

        return odooRec

    def get_current_country(self, country_code):
        country = request.env["res.country"].sudo().search([("code", "=", country_code)])

        entity = request.env["res.country.group"].sudo().search([("country_ids.id", "=", country.id)])

        return {
            "country": country,
            "entity": entity,
        }

    def get_current_country_pricing(self, country_code, program):
        """
        The method will return the pricing record which will be used to determine the price
        :param country: Country for which we're finding the price
        :param program: The program.type.master record for which we are finding the record
        :return: samaskriti.pricing.parameters rec
        """
        price = request.env["samaskriti.pricing.parameters"].sudo().search([
                    ("countryname.code", "=", country_code),
                    ("programtype.id", "=", program.id),
                ])
        return price

    def get_program_record(self, program_name):
        """
        Returns the system id for the program name
        :param program_name: The programname string
        :return:
        """
        if program_name == "Kalaripayattu":
            rec = request.env.ref("isha_samskriti.kalaripayattu_prog_type").id
        elif program_name == "NirvanaShatakam":
            rec = request.env.ref("isha_samskriti.nirvana_shatakam_prog_type").id
        elif program_name == "NiraiveShakti":
            rec = request.env.ref("isha_samskriti.niraive_shakti_prog_type").id
        elif program_name == "NirbhayNirgun":
            rec = request.env.ref("isha_samskriti.nirbhay_nirgun_prog_type").id
        else:
            rec = 0

        return rec

    def check_return_exis_rec(self, **post):
        """
        The method returns existing record for given transaction id whose transaction has been completed

        :param post: the post param containing the local_trans_id
        :return:
        """
        if post.get("registration_completed", False):
            return request.env["program.lead"].sudo().search([
                        ("local_trans_id", "=", post.get("trans_id")),
                        ("reg_status", "in", ["Confirmed", "Inprogress"]),
                    ])
        else:
            return request.env["program.lead"].sudo().search([
                        ("local_trans_id", "=", post.get("trans_id")),
                        ("reg_status", "in", ["In Progress", "Failed"])
                    ])

    def redirect_Payment_gateway(self,created_rec,local_trans_id):
        """ Redirec to Payment Gateway logic"""
        _logger.info("*** Inner Check on Redirect to Payment GAteway ****")
        try:
            sams_gpms_payurl = (request.env["ir.config_parameter"].sudo().get_param("samaskriti.gpms_payment_url"))
            sams_callbackUrl = (request.env["ir.config_parameter"].sudo().get_param("samaskriti.gpms_call_back_url"))
            sams_program_type_music = request.env["ir.config_parameter"].sudo().get_param(
                "samaskriti.programcode.music")
            sams_program_type_kalari = request.env["ir.config_parameter"].sudo().get_param(
                "samaskriti.programcode.kalari")
            if created_rec.program_type == 'Kalaripayattu':
                sams_program_type = sams_program_type_kalari
                sams_code = 'KALARIPAYATTU'
            elif created_rec.program_type == 'Nirvana Shatakam':
                sams_program_type = sams_program_type_music
                sams_code = 'NIRVANA_SHATAKAM'
            elif created_rec.program_type == 'Niraive Shakti':
                sams_program_type = sams_program_type_music
                sams_code = 'NIRAIVE_SHAKTI'
            else:
                sams_program_type = sams_program_type_music
                sams_code = 'NIRBHAY_NIRGUN'
            # if reg_status is Failed then check any seats are avaibel and then proceed with payment gateway
            if created_rec.reg_status =="Failed":
                if created_rec.sm_batchId.seats_left<=0:
                    values = {
                        "status": "no_seats_available",
                        "message": "There is error in sending mail",
                        "email": "",
                    }
                    return request.render("isha_samskriti.exception", values)
                else:
                    created_rec.sm_batchId.seats_left = created_rec.sm_batchId.seats_left - 1

            created_rec.write({"reg_status": "Payment Initiated"})
            street = ""
            city = ""
            state  = ""
            country  = ""
            zip = ""
            email = ""
            phone  = ""
            name = ""
            if local_trans_id:
                req_payment_rec = request.env['samaskriti.participant.paymenttransaction'].sudo().search(
                    [('orderid', '=', local_trans_id), ("transactiontype", "=", "Request to Gateway")], limit=1)
                if not req_payment_rec:
                    req_payment_rec = request.env['samaskriti.participant.paymenttransaction'].sudo().search(
                        [('orderid', '=', local_trans_id), ('billing_email', '!=', False)], limit=1)
                payment_rec = req_payment_rec
                if payment_rec:
                    amount = payment_rec.amount
                    currency = payment_rec.currency
                    street = (payment_rec.billing_address)[:50] if payment_rec.billing_address else '',
                    city = payment_rec.billing_city
                    #state_rec = request.env["res.country.state"].sudo().search([("id", "=", post.get("bill_state"))])
                    state = payment_rec.billing_state
                    country = payment_rec.billing_country
                    zip = payment_rec.billing_zip
                    email = payment_rec.billing_email
                    phone = payment_rec.billing_tel1
                    name = payment_rec.billing_name
                else:
                    values = {"status": 'Error', 'message': "No Payment Transacition records found",
                              'email': ""}
                    return request.render('isha_samskriti.exception', values)

            if created_rec.record_name:
                first_name = created_rec.record_name.split(' ')[0]
                last_name = created_rec.record_name.split(' ')[1]
            values = {
                "firstName": first_name if first_name else name,
                "lastName": last_name if last_name else name,
                "email": created_rec.record_email if created_rec.record_email else email,
                "mobilePhone": phone if phone else created_rec.record_phone,
                "addressLine1": street if street else created_rec.street,  # GPMS accepts max 50 chars
                "addressLine2": "",  # Not a mandatory field
                "city": city if city else created_rec.record_city,
                "state": state if state else created_rec.record_state,
                "postCode": zip if zip else created_rec.record_zip,
                "country": country if country else "IN",
                "amount": amount if amount else 0,
                "currency": currency,
                "requestId": created_rec.local_trans_id,
                "ssoId": created_rec.sso_id,
                "gpms_payurl": sams_gpms_payurl,
                "callbackUrl": sams_callbackUrl,
                "programType": sams_program_type,
                "programCode": sams_code,
            }
            _logger.info("*** Gpms Dictionary Values - Manually****")
            _logger.info(values)
            return request.render("isha_samskriti.isha_website_payment", values)

        except Exception as e:
            values = {"status": 'Error', 'message': "There is error in sending mail",
                      'email': ""}
            tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
            _logger.info(tb_ex)
            return request.render('isha_samskriti.exception', values)


    def save_batch_data(self, program, **post):

        # sams_code = (
        #     request.env["ir.config_parameter"]
        #     .sudo()
        #     .get_param("samaskriti.programcode")
        # )
        sams_program_type_music = request.env["ir.config_parameter"].sudo().get_param("samaskriti.programcode.music")
        sams_program_type_kalari = request.env["ir.config_parameter"].sudo().get_param("samaskriti.programcode.kalari")

        if program.program_type == 'Kalaripayattu':
            sams_program_type = sams_program_type_kalari
            sams_code = 'KALARIPAYATTU'
        elif program.program_type == 'Nirvana Shatakam':
            sams_program_type = sams_program_type_music
            sams_code = 'NIRVANA_SHATAKAM'
        elif program.program_type == 'Niraive Shakti':
            sams_program_type = sams_program_type_music
            sams_code = 'NIRAIVE_SHAKTI'
        else:
            sams_program_type = sams_program_type_music
            sams_code = 'NIRBHAY_NIRGUN'

        local_trans_id = f"{post.get('sso_profile_id')}-{sams_program_type}-{post.get('programapplied')}-{post.get('batchapplied')}"

        existing_rec = self.check_return_exis_rec(
            trans_id=local_trans_id, registration_completed=True
        )



        if not existing_rec:
            rec = self.check_return_exis_rec(
                trans_id=local_trans_id, registration_completed=False
            )

            odoo_rec = self._create_partner_rec(
                eval(post.get("sso_prof")), local_trans_id
            )

            if not rec:
                rec = request.env["program.lead"].sudo().create(
                        {
                            "local_program_type_id": program.local_program_type_id,
                            "program_name": program.program_type,
                            "sso_id": post.get("sso_profile_id"),
                            "sm_scheduleId": int(post.get("programapplied")),
                            "sm_batchId": int(post.get("batchapplied")),
                            "reg_status": "In Progress",
                            "pgm_type_master_id": program.id,
                            "contact_id_fkey": 2,
                            "program_schedule_info": {"pii": odoo_rec},
                            "local_trans_id": local_trans_id,
                        })
            else:
                rec.write({
                        "local_program_type_id": program.local_program_type_id,
                        "program_name": program.program_type,
                        "sso_id": post.get("sso_profile_id"),
                        "sm_scheduleId": int(post.get("programapplied")),
                        "sm_batchId": int(post.get("batchapplied")),
                        "reg_status": "In Progress",
                        "pgm_type_master_id": program.id,
                        "local_trans_id": local_trans_id,
                    })
            if post.get("hlab"):
                post.update({"health_disclaimer": True})
                self.save_survey_questions(rec, program, **post)

            return rec
        else:
            # ToDo: Redirect the user to the duplicated reigstration or wait for payment page

            return existing_rec

    def save_survey_questions(self, rec, program, **post):
        """
        The method saves survey master questions against program lead record

        :param rec: program.lead record
        :param program: program.type.master record
        :param post: post params
        :return: None
        """

        # getting all the questions
        if not post.get("health_disclaimer", False):
            questions = program.mapped("sm_questions")
        else:
            _logger.info(
                "********** hlab Questions (Isha Samaskriti) ************"
                + str(type(post.get("hlab")))
            )
            questions = program.sm_questions.filtered(
                lambda x: x.id == int(post.get("hlab"))
            )

        # updating the post params with program.lead rec
        post.update({"program_lead_rec": rec})

        # looping through questions and saving them
        for question in questions:
            # the tag helps to fetch the answer from the dict
            answer_tag = "%s_%s" % (program.id, question.id)
            request.env["survey.user_input_line"].sudo().sams_save_lines(
                False, question, post, answer_tag
            )

    def create_payment_trans_rec(self, gpms_dict, bill_add_dict,existing_rec):
        '''
        method creates the fisrt record for payment transaction table
        :param gpms_dict: dict with payment contents
        :param bill_add_dict: dict with bill address contens
        :return: None
        '''
        if not (gpms_dict and bill_add_dict):
            print("Data passed isn't proper")
            return

        trns_var = request.env['samaskriti.participant.paymenttransaction'].sudo()

        country = request.env['res.country'].sudo().search([('id','=',int(bill_add_dict.get('bill_country')))])

        #state = False
        #if country.state_ids:
         #   state = request.env['res.country.state'].sudo().search([('id','=',bill_add_dict.get('bill_state'))])

        schedule = None
        batch = None

        try:
            schedule = existing_rec.sm_scheduleId.start_date.strftime('%d %B %Y') + ' to ' + existing_rec.sm_scheduleId.end_date.strftime('%d %B %Y')
            batch = existing_rec.sm_batchId.display_text
        except:
            pass

        values = {
            'first_name':gpms_dict.get('firstName'),
            'last_name':gpms_dict.get("lastName"),
            'programname':bill_add_dict.get('program_name'),
            'transactiontype':'Request to Gateway',
            # identifier for picking the record
            'orderid':gpms_dict.get('requestId'),
            'billing_name':f"{gpms_dict.get('firstName')} {gpms_dict.get('lastName')}",
            'billing_address':bill_add_dict.get('bill_address'),
            'billing_city':bill_add_dict.get('bill_district'),
            "billing_zip":bill_add_dict.get('pincode'),
            'billing_state':bill_add_dict.get('bill_state'),
            "billing_country":country.code,
            'billing_tel1': gpms_dict.get("mobilePhone"),
            'billing_email': gpms_dict.get("email"),
            "amount": gpms_dict.get("amount"),
            "currency": gpms_dict.get("currency"),
            "batch_details": batch,
            "schedule_details":schedule
        }

        return trns_var.sudo().create(values)



    def save_profile_data(self, program, schd, batch, **post):
        """
        The method saves the profile data inputted by the user

        :param program: The program.type.master record
        :param schd: The schd user signed up for
        :param batch: The batch user signed up for
        :param post: The post param
        :return: The dict to be sent to gpms for payment flow
        """
        sams_gpms_payurl = request.env["ir.config_parameter"].sudo().get_param("samaskriti.gpms_payment_url")
        sams_callbackUrl = request.env["ir.config_parameter"].sudo().get_param("samaskriti.gpms_call_back_url")

        sams_program_type_music = request.env["ir.config_parameter"].sudo().get_param("samaskriti.programcode.music")
        sams_program_type_kalari = request.env["ir.config_parameter"].sudo().get_param("samaskriti.programcode.kalari")

        if program.program_type == 'Kalaripayattu':
            sams_program_type = sams_program_type_kalari
            sams_code = 'KALARIPAYATTU'
        elif program.program_type == 'Nirvana Shatakam':
            sams_program_type = sams_program_type_music
            sams_code = 'NIRVANA_SHATAKAM'
        elif program.program_type == 'Niraive Shakti':
            sams_program_type = sams_program_type_music
            sams_code = 'NIRAIVE_SHAKTI'

        else:
            sams_program_type = sams_program_type_music
            sams_code = 'NIRBHAY_NIRGUN'

        # creating transaction id which acts as a unique identifier
        local_trans_id = f"{post.get('sso_profile_id')}-{sams_program_type}-{schd}-{batch}"
        _logger.info("_____________ Local Transaction ID _____________"+str(local_trans_id))

        # checking if the record exists to update
        existing_rec = self.check_return_exis_rec(
            trans_id=local_trans_id, registration_completed=False
        )

        if existing_rec:
            # updating the record
            countrycode = post.get("country_code")
            phone = post.get("phone_number")
            same_whatappnumber = post.get("issame")
            wtsappcountrycode = post.get("whatsappnumbercode")
            wtsappphone = post.get("whatsappnumber")
            if same_whatappnumber == "YES" or not wtsappcountrycode or not wtsappphone:
                wtsappcountrycode = countrycode
                wtsappphone = phone

            # the following if else fetches state or country from db
            if post.get("bill_state"):
                state_recd = request.env["res.country.state"].sudo().search([("id", "=", post.get("bill_state"))])

                country_rec = state_recd.country_id
                state_rec = state_recd.name
            else:
                country_rec = request.env["res.country"].sudo().search([("id", "=", post.get("bill_country"))])

                state_rec = post.get('bill_state_text')
                state_recd = False

            pg_schd_info = eval(existing_rec.program_schedule_info)
            pg_schd_info["pii"].update(
                {
                    "phone_country_code": countrycode,
                    "phone": phone,
                    "email": post.get("email"),
                    "whatsapp_country_code": wtsappcountrycode,
                    "whatsapp_number": wtsappphone,
                    'street': post.get("bill_address"),
                    'city': post.get("bill_district"),
                    'state': state_rec,
                    'state_id': state_recd and state_recd.id,
                    'country': country_rec.name,
                    'country_id': country_rec.id,
                    'zip': post.get("billl_pincode")
                }
            )

            existing_rec.write(
                {
                    "record_name": f"{post.get('first_name')} {post.get('last_name')}",
                    "reg_status": "Registered",
                    "record_phone": post.get("phone_number"),
                    "phone": post.get("phone_number"),
                    "record_email": post.get("email"),
                    # "record_email":post.get("bill_address"),
                    "record_city":post.get("bill_district"),
                    "record_state":post.get("bill_state"),
                    "record_zip":post.get("billl_pincode"),
                    "record_country":post.get("bill_country"),
                    "local_trans_id": local_trans_id,
                    "program_schedule_info": pg_schd_info,
                }
            )


            # Todo: use this dict to save bill address in payment transaction
            bill_add_dict = {
                "pincode": post.get("billl_pincode"),
                "bill_country": post.get("bill_country"),
                "bill_district": post.get("bill_district"),
                "bill_state": state_rec,
                "bill_address": post.get("bill_address"),
                "program_name":existing_rec.program_name
            }

            # removing the duplicate key to remove duplicacy in arguments
            if 'rec' in post.keys():post.pop('rec')
            # saving questions that are rendered from the survey questions master
            self.save_survey_questions(existing_rec, program, **post)



            # gpms dict creation
            gpms_dict = {
                "firstName": post.get("first_name"),
                "lastName": post.get("last_name"),
                "email": post.get("email"),
                "mobilePhone": post.get("phone_number"),
                "addressLine1": post.get("bill_address"),  # GPMS accepts max 50 chars
                "city": post.get("bill_district"),
                "state": state_rec,
                "postCode": post.get("billl_pincode"),
                "country": country_rec and country_rec.code or "IN",
                "amount": post.get("price"),
                "currency": post.get("price_currency"),
                "requestId": local_trans_id,
                "ssoId": post.get("sso_profile_id"),
                "programCode": sams_code,
                "programType": sams_program_type,
                "gpms_payurl": sams_gpms_payurl,
                "callbackUrl": sams_callbackUrl,
            }

            return gpms_dict,existing_rec,bill_add_dict

    @http.route(
        "/registrations/samskriti",
        type="http",
        auth="public",
        methods=["GET", "POST"],
        website=True,
        csrf=False,
    )
    def index(self, programname="",local="", consent_grant_status="", **post):
        """
        The method gets hit and re-directs the user to the suitable first page based on program type

        :param programname: The name of program
        :param consent_grant_status:
        :param post: post params
        :return: Redirection template or url
        """

        if post.get("sso_redirect") or request.httprequest.method == "GET":
            # Force the Ip country page
            if not post.get("sso_redirect"):
                return request.render(
                    "isha_samskriti.fetch_ip_country",
                    {"action_url": request.httprequest.full_path},
                )

            if post.get("is_logout") == "yes":
                _logger.info("................inside get session")
                post.pop("is_logout")
                return request.render("isha_satsang.logout_sso_auto_redirect")
            try:
                ret = request.env["ir.config_parameter"].sudo().get_param("satsang.SSO_API_SECRET1")

                if "localhost" in request.httprequest.url:
                    cururls = request.httprequest.url
                else:
                    # IP country based redirection Logic
                    ip_country = post.get("ip_country")

                    if ip_country == 'IN':
                        cururls = request.env["ir.config_parameter"].sudo().get_param(
                            "isha_crm.indian_server_domain") + request.httprequest.full_path
                    else:
                        cururls = request.env["ir.config_parameter"].sudo().get_param(
                            "isha_crm.overseas_server_domain") + request.httprequest.full_path

                sso_log = {
                    "request_url": str(cururls) + "&ip=" + post.get("ip_country"),
                    "callback_url": str(cururls) + "&ip=" + post.get("ip_country"),
                    "api_key": request.env["ir.config_parameter"]
                    .sudo()
                    .get_param(
                        "satsang.SSO_API_KEY1"
                    ),  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                    "hash_value": "",
                    "action": "0",
                    "legal_entity": "IF",
                    "force_consent": "1",
                }
                ret_list = {
                    "request_url": str(cururls) + "&ip=" + post.get("ip_country"),
                    "callback_url": str(cururls) + "&ip=" + post.get("ip_country"),
                    "api_key": request.env["ir.config_parameter"]
                    .sudo()
                    .get_param(
                        "satsang.SSO_API_KEY1"
                    ),  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                }
                data_enc = json.dumps(ret_list, sort_keys=True, separators=(",", ":"))
                data_enc = data_enc.replace("/", "\\/")
                signature = hmac.new(
                    bytes(ret, "utf-8"),
                    bytes(data_enc, "utf-8"),
                    digestmod=hashlib.sha256,
                ).hexdigest()
                sso_log["hash_value"] = signature
                sso_log["sso_logurl"] = (
                    request.env["ir.config_parameter"]
                    .sudo()
                    .get_param("satsang.SSO_LOGIN_URL1")
                )
                values = {"sso_log": sso_log}
                # return request.render('isha_livingspaces.livingspacessso', values)
                return request.render("isha_samskriti.onlinesamskritisso", values)
            except Exception as ex:
                tb_ex = "".join(
                    traceback.format_exception(
                        etype=type(ex), value=ex, tb=ex.__traceback__
                    )
                )
                _logger.error(tb_ex)
                _logger.error("Error caught during request creation", exc_info=True)
        # if request.httprequest.method == "GET":
        #     try:
        #         if programname == "Kalaripayattu":
        #             page = 0
        #         else:
        #             page = 1
        #
        #         if page == 1:
        #             access_token = "test"#request.httprequest.form["session_id"]
        #             tmpurl = request.httprequest.url.replace(
        #                 "samskriti", "samskriti/batchselect"
        #             )
        #             batch_select_url = (
        #                 f"{tmpurl}&access_token={access_token}&ip=IN"
        #             )
        #             return werkzeug.utils.redirect(batch_select_url)
        #         elif page == 0:
        #             # access_token = request.httprequest.form["session_id"]
        #             tmpurl = request.httprequest.url.replace(
        #                 "samskriti", "samskriti/healthdisclaimer"
        #             )
        #             health_disclaimer_url = f"{tmpurl}"
        #                                     # f"&access_token={access_token}"
        #             return werkzeug.utils.redirect(health_disclaimer_url)
        #
        #     except Exception as ex:
        #         tb_ex = "".join(
        #             traceback.format_exception(
        #                 etype=type(ex), value=ex, tb=ex.__traceback__
        #             )
        #         )
        #         _logger.error(tb_ex)
        elif request.httprequest.method == "POST" and "session_id" in request.httprequest.form:
            try:
                if programname == "Kalaripayattu":
                    page = 0
                else:
                    page = 1

                if page == 1:
                    access_token = request.httprequest.form["session_id"]
                    tmpurl = request.httprequest.url.replace(
                        "samskriti", "samskriti/batchselect"
                    )
                    batch_select_url = (
                        f"{tmpurl}&access_token={access_token}&ip={post.get('ip')}"
                    )
                    return werkzeug.utils.redirect(batch_select_url)
                elif page == 0:
                    access_token = request.httprequest.form["session_id"]
                    tmpurl = request.httprequest.url.replace(
                        "samskriti", "samskriti/healthdisclaimer"
                    )
                    health_disclaimer_url = (
                        f"{tmpurl}&access_token={access_token}&ip={post.get('ip')}"
                    )
                    return werkzeug.utils.redirect(health_disclaimer_url)

            except Exception as ex:
                tb_ex = "".join(
                    traceback.format_exception(
                        etype=type(ex), value=ex, tb=ex.__traceback__
                    )
                )
                _logger.error(tb_ex)

    @http.route(
        "/registrations/samskriti/healthdisclaimer",
        type="http",
        auth="public",
        methods=["GET", "POST"],
        website=True,
        csrf=False,
    )
    def _samskriti_health_disclaimer(self, programname="", access_token="", **post):
        """
        The controller renders the health disclaimer form and asks the user to agree before proceeding

        :param programname: The program for which user is registering
        :param access_token: The access token based on which the user's will data will be retrieved
        :param post: The post params to save in db
        :return: The template or url to redirect the user to
        """
        if request.httprequest.method == "GET":
            try:
                program_id = self.get_program_record(programname)

                if not program_id:
                    _logger.info(
                        "********** Program Type Sent is Wrong (Isha Samaskriti) ************"
                    )

                program = (
                    request.env["program.type.master"]
                    .sudo()
                    .search([("id", "=", program_id)])
                )

                disclaimers = (
                    program.sm_questions
                    and program.sm_questions.filtered(
                        lambda x: x.question_type == "health_disclaimer"
                    )
                    or False
                )

                if disclaimers:
                    disclaimer = disclaimers[0]
                else:
                    # Todo: Render a template saying no health disclaimers
                    return

                values = {
                    "ptm": program,
                    "disclaimer": disclaimer,
                    "program_name": programname,
                    "ipcountry": post.get("ip"),
                    "access_token": access_token,
                    "url_string":"healthdisclaimer"
                }

                return request.render("isha_samskriti.questionpage", values)
            except Exception as ex:
                _logger.error(
                    "-------Issue in health disclaimer get method-------------"
                )
                tb_ex = "".join(
                    traceback.format_exception(
                        etype=type(ex), value=ex, tb=ex.__traceback__
                    )
                )
                _logger.error(tb_ex)
                values = {
                    "status": "Error",
                    "message": "",
                    "email": "",
                }
                return request.render("isha_samskriti.exception", values)

        elif request.httprequest.method == "POST":
            try:
                tmpurl = request.httprequest.url.replace(
                    "healthdisclaimer", "batchselect"
                )

                batch_select_url = f"{tmpurl}?programname={post.get('program_name')}&access_token={access_token}&hlab={post.get('question_label')}&agreeddate={datetime.now()}&ip={post.get('ipcountry')}"

                return werkzeug.utils.redirect(batch_select_url)
            except Exception as ex:
                _logger.error(
                    "-------Issue in health disclaimer post method-------------"
                )
                tb_ex = "".join(
                    traceback.format_exception(
                        etype=type(ex), value=ex, tb=ex.__traceback__
                    )
                )
                _logger.error(tb_ex)
                values = {
                    "status": "Error",
                    "message": "",
                    "email": "",
                }
                return request.render("isha_samskriti.exception", values)

    @http.route(
        "/registrations/samskriti/batchselect",
        type="http",
        auth="public",
        methods=["GET", "POST"],
        website=True,
        csrf=False,
    )
    def _samskriti_batch_select(self, programname="", access_token="", **post):
        """
        The controller renders the form for batch selection and saves user input

        :param programname: The program for which user is registering
        :param access_token: The access token based on which the user's will data will be retrieved
        :param post: The post params to save in db
        :return: The template or url to redirect the user to
        """
        if request.httprequest.method == "GET":
            try:
                program_id = self.get_program_record(programname)

                if not program_id:
                    _logger.info(
                        "********** Program Type Sent is Wrong (Isha Samaskriti) ************"
                    )
                    values = {"status": "program_type_wrong"}
                    return request.render("isha_samskriti.exception", values)

                program = (
                    request.env["program.type.master"]
                    .sudo()
                    .search([("id", "=", program_id)])
                )

                ret = (
                    request.env["ir.config_parameter"]
                    .sudo()
                    .get_param("satsang.SSO_API_SECRET1")
                )

                api_key = (
                    request.env["ir.config_parameter"]
                    .sudo()
                    .get_param("satsang.SSO_API_KEY1")
                )  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                session_id = access_token
                hash_val = hash_hmac({"session_id": session_id}, ret)
                data = {
                    "api_key": api_key,
                    "session_id": session_id,
                    "hash_value": hash_val,
                }
                request_url = (
                    request.env["ir.config_parameter"]
                    .sudo()
                    .get_param("satsang.SSO_LOGIN_INFO_ENDPOINT1")
                )
                sso_user_profile = eval(sso_get_user_profile(request_url, data).text)

                fullprofileresponsedatVar = get_fullprofileresponse_DTO()

                if "autologin_email" in sso_user_profile:
                    fullprofileresponsedatVar["basicProfile"]["email"] = sso_user_profile["autologin_email"]
                    fullprofileresponsedatVar["profileId"] = sso_user_profile["autologin_profile_id"]
                    full_name = sso_user_profile['autologin_name']
                    fullprofileresponsedatVar = sso_get_full_data(sso_user_profile["autologin_profile_id"])

                # fetching the schedules for the current program
                N_DAYS_AGO = 2
                today = datetime.now()
                schedule_close_date = today + timedelta(days=N_DAYS_AGO)
                _logger.info('Schedule close date 2 days Before '+str(schedule_close_date))
                program_schedules = request.env["program.schedule.master"].sudo().search([
                    ("program_type_id", "=", program.id),
                    ("start_date", ">", schedule_close_date)
                ])


                # Remove the schedules which are already confirmed
                registered_schedules = request.env['program.lead'].sudo().search(
                    [('sso_id', '=', fullprofileresponsedatVar["profileId"]),
                     ('reg_status','in',['Confirmed',"Inprogress"]),
                     ('sm_scheduleId','in',program_schedules.ids)]
                ).mapped('sm_scheduleId.id')

                program_schedules = program_schedules.filtered(lambda x: x.id not in registered_schedules)

                # if no schedules redirect user to no schedules page
                if not program_schedules:
                    # Todo: Redirect user to no schedule page
                    _logger.info(
                        "********** No Program schedules for current programs (Isha Samaskriti) ************"
                    )

                    values = {
                        "status": "no_program_schedule",
                        "sso_id": fullprofileresponsedatVar["profileId"],
                        "program_name": program.program_type,
                        "full_name":full_name if full_name else '',
                        "country":fullprofileresponsedatVar["addresses"]["country"],
                        # "timeslot": timeslot,
                        "email": fullprofileresponsedatVar["basicProfile"]["email"],
                    }
                    return request.render("isha_samskriti.exception", values)


                # checking if the user has already registered and getting the record whose batch date is in future
                existing_conflicts = (
                    request.env["program.lead"]
                    .sudo()
                    .search(
                        [
                            ("sso_id", "=", fullprofileresponsedatVar["profileId"]),
                            ("sm_scheduleId.end_date", ">=", datetime.now()),
                            ("pgm_type_master_id", "in", _get_smas_pgm_ids()),
                            (
                                "reg_status",
                                "not in",
                                ["In Progress", "Registered", "Failed","Schedule Transferred"],
                            ),
                        ],
                    )
                ).mapped("sm_batchId.conflicts")

                batches = program_schedules.no_of_batch_ids

                if batches:
                    batches = batches.filtered(lambda x: x.seats_left > 0)

                if existing_conflicts:
                    batches = batches - existing_conflicts

                program_schedules = program_schedules.filtered(
                    lambda x: x.id in batches.mapped("program_schedule_id.id")
                )

                # if existing_regs:
                #     conflicting_batches = request.env["program.schedule.batch.master"]
                #     for existing_reg in existing_regs:
                #         conflicting_batches += program_schedules.no_of_batch_ids.filtered(
                #             lambda x: x.start_date == existing_reg.sm_batchId.start_date
                #             or x.end_date == existing_reg.sm_batchId.end_date
                #             or x.start_date
                #             < existing_reg.sm_batchId.start_date
                #             < x.end_date
                #             < existing_reg.sm_batchId.end_date
                #             or existing_reg.sm_batchId.start_date
                #             < x.start_date
                #             < existing_reg.sm_batchId.end_date
                #             or x.seats_left <= 0
                #             or x.program_schedule_id.id
                #             == existing_reg.sm_batchId.program_schedule_id.id
                #         )
                #     if conflicting_batches:
                #         batch_changed = True
                #         batches = batches - conflicting_batches


                # if we don't have any batches present then we'd redirect the user to a different template
                if not batches:
                    # Todo: render the no date available template
                    _logger.info(
                        "********** No Batches for current programs (Isha Samaskriti) ************"
                    )

                    values = {
                        "status": "no_program_schedule",
                        "sso_id": fullprofileresponsedatVar["profileId"],
                        "email": fullprofileresponsedatVar["basicProfile"]["email"],
                        "program_name": program.program_type,
                        "full_name": full_name if full_name else '',
                        "country": post.get('ip'),
                        # "timeslot": timeslot,

                    }
                    return request.render("isha_samskriti.exception", values)

                # get the pricing for the current country based on the entity of current country
                price = False
                if post.get("ip"):
                    price = self.get_current_country_pricing(post.get("ip"), program)

                _logger.info(
                    "********** hlab (Isha Samaskriti) ************"
                    + str(type(post.get("hlab")))
                )
                values = {
                    "ptm": program,
                    "batches": batches,
                    "program_schedules": program_schedules,
                    "price": price,
                    "program_name": programname,
                    "sso_prof": fullprofileresponsedatVar,
                    "access_token": access_token,
                    "ipcountry": post.get("ip"),
                    "hlab": post.get("hlab"),
                    "agreeddate": post.get("agreeddate"),
                }
                _logger.info(
                    "********** Batch Selection Values (Isha Samaskriti) ************"
                    + str(values)
                )

                return request.render("isha_samskriti.batch_selection", values)
            except Exception as ex:
                _logger.error("-------Issue in batch select get method-------------")
                tb_ex = "".join(
                    traceback.format_exception(
                        etype=type(ex), value=ex, tb=ex.__traceback__
                    )
                )
                _logger.error(tb_ex)
                values = {
                    "status": "Error",
                    "message": "",
                    "email": "",
                }
                return request.render("isha_samskriti.exception", values)

        elif request.httprequest.method == "POST":
            try:
                program_id = self.get_program_record(post.get("program_name"))
                program = (
                    request.env["program.type.master"]
                    .sudo()
                    .search([("id", "=", program_id)])
                )

                # No Batch Record Notify logic
                if post.get('status') == 'no_bacth':
                    if post.get('sso_id'):
                        sso_id = post.get('sso_id')
                    if post.get('program_name'):
                        program_name = post.get('program_name')
                    if post.get('full_name'):
                        full_name = post.get('full_name')
                    if post.get('email'):
                        email = post.get('email')
                    if post.get('country'):
                        country = post.get('country')

                    values = {
                        "sso_id": sso_id,
                        "pgm_name": program_name,
                        "email": email,
                        "full_name":full_name,
                        "country":country
                    }
                    rec = request.env['samaskriti.notify.me'].search([('sso_id', '=', sso_id)], limit=1)
                    if rec:
                        request.env['samaskriti.notify.me'].sudo().write(values)
                    else:
                        request.env['samaskriti.notify.me'].sudo().create(values)
                    return request.render("isha_samskriti.success_batch_message")

                # No Batch Schedule Record Notify logic
                if post.get('status') == 'no_program_schedule':
                    if post.get('sso_id'):
                        sso_id = post.get('sso_id')
                    if post.get('program_name'):
                        program_name = post.get('program_name')
                    if post.get('full_name'):
                        full_name = post.get('full_name')
                    if post.get('email'):
                        email = post.get('email')
                    if post.get('country'):
                        country = post.get('country')

                    values = {
                        "sso_id": sso_id,
                        "pgm_name": program_name,
                        "email": email,
                        "full_name":full_name,
                        "country":country
                    }
                    rec = request.env['samaskriti.notify.me'].search([('sso_id', '=', sso_id)], limit=1)
                    if rec:
                        request.env['samaskriti.notify.me'].sudo().write(values)
                    else:
                        request.env['samaskriti.notify.me'].sudo().create(values)

                    return request.render("isha_samskriti.success_program_schedule_message")

                # If alreddy register for this program with same batch number , then auto redirect to payment gateway
                is_payment_pending_rec = request.env["program.lead"].sudo().search([
                        ("sso_id", "=", post.get("sso_profile_id")),
                        ("sm_scheduleId", "=", int(post.get("programapplied"))),
                        ("sm_batchId", "=", int(post.get("batchapplied"))),
                        ("reg_status", "in", ("Payment Initiated","Failed"))], limit=1)
                _logger.info(
                        "_If alreddy register for this program with same batch number then auto redirect to payment gateway " + str(
                            is_payment_pending_rec))

                if is_payment_pending_rec:
                    local_trans_id = is_payment_pending_rec.local_trans_id
                    return self.redirect_Payment_gateway(is_payment_pending_rec, local_trans_id)
                else:
                    self.save_batch_data(program, **post)
                    tmpurl = request.httprequest.url.replace(
                        "batchselect", "profilecompletion"
                    )
                    batch_select_url = f"{tmpurl}?programname={post.get('program_name')}&access_token={access_token}&schd={post.get('programapplied')}&batch={post.get('batchapplied')}&ip={post.get('ipcountry')}"
                    _logger.info(
                        "********** Batch Selection (Isha Samaskriti) ************"
                        + str(batch_select_url)
                    )
                    return werkzeug.utils.redirect(batch_select_url)
            except Exception as ex:
                _logger.error("-------Issue in batch select post method-------------")
                tb_ex = "".join(
                    traceback.format_exception(
                        etype=type(ex), value=ex, tb=ex.__traceback__
                    )
                )
                _logger.error(tb_ex)
                values = {
                    "status": "Error",
                    "message": "",
                    "email": "",
                }
                return request.render("isha_samskriti.exception", values)

    @http.route(
        "/registrations/samskriti/profilecompletion",
        type="http",
        auth="public",
        methods=["GET", "POST"],
        website=True,
        csrf=False,
    )
    def _samskriti_profile_completion(self, programname="", access_token="", schd="", batch="", **post):
        """
        The controller is used to render the profile detail page and save it in user db

        :param programname: The program name
        :param access_token: The access token based on which we'll retrieve sso user profile
        :param schd: The schedule user signed up for
        :param batch: The batch user signed up for
        :param post: The post params dict
        :return: Return the url or the template to render
        """
        if request.httprequest.method == "GET":
            try:
                program_id = self.get_program_record(programname)

                if not program_id:
                    print("Program Type Sent is Wrong")
                    return False

                program = request.env["program.type.master"].sudo().search([("id", "=", program_id)])

                ret = request.env["ir.config_parameter"].sudo().get_param("satsang.SSO_API_SECRET1")

                api_key = request.env["ir.config_parameter"].sudo().get_param("satsang.SSO_API_KEY1")

                 # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                session_id = access_token
                hash_val = hash_hmac({"session_id": session_id}, ret)
                data = {
                    "api_key": api_key,
                    "session_id": session_id,
                    "hash_value": hash_val,
                }
                request_url = (
                    request.env["ir.config_parameter"]
                    .sudo()
                    .get_param("satsang.SSO_LOGIN_INFO_ENDPOINT1")
                )
                sso_user_profile = eval(sso_get_user_profile(request_url, data).text)

                fullprofileresponsedatVar = get_fullprofileresponse_DTO()

                if "autologin_email" in sso_user_profile:
                    fullprofileresponsedatVar["basicProfile"]["email"] = sso_user_profile["autologin_email"]
                    fullprofileresponsedatVar["profileId"] = sso_user_profile["autologin_profile_id"]

                    fullprofileresponsedatVar = sso_get_full_data(
                        sso_user_profile["autologin_profile_id"]
                    )

                # get the pricing for the current country based on the entity of current country
                price = False
                if post.get("ip"):
                    price = self.get_current_country_pricing(post.get("ip"), program)

                # the current country fetched via the ip
                current_country = self.get_current_country(post.get("ip"))

                # country list to render the country dropdown
                countries = request.env["res.country"].sudo().search([])

                # the terms and condition to render for the current entity
                terms_conds = program.sm_questions.filtered(
                    lambda x: x.sm_tcEntity == current_country.get("entity")
                    and x.question_type == "terms_conditions"
                )

                values = {
                    "ptm": program,
                    "object": program,
                    "profile": fullprofileresponsedatVar,
                    "price": price,
                    "program_name": programname,
                    "sso_prof": fullprofileresponsedatVar,
                    "countr": current_country["country"],
                    "access_token": access_token,
                    "countries": countries,
                    "term_cond": terms_conds,
                    "schd": schd,
                    "batch": batch,
                    "url_string":"profilecompletion"
                }

                return request.render("isha_samskriti.personaldetails", values)
            except Exception as ex:
                _logger.error(
                    "-------Issue in profile completion get method-------------"
                )
                tb_ex = "".join(
                    traceback.format_exception(
                        etype=type(ex), value=ex, tb=ex.__traceback__
                    )
                )
                _logger.error(tb_ex)
                values = {
                    "status": "Error",
                    "message": "",
                    "email": "",
                }
                return request.render("isha_samskriti.exception", values)

        if request.httprequest.method == "POST":
            try:
                # get the program type master record
                program_id = self.get_program_record(post.get("program_name"))
                program = request.env["program.type.master"].sudo().search([("id", "=", program_id)])

                # fetching the batch record in order to check seats left
                batch_rec = request.env["program.schedule.batch.master"].sudo().search([("id", "=", int(batch))])

                if batch_rec:
                    # stopping the registration process as soon if number of seats has ran out
                    if batch_rec.seats_left <= 0:
                        values = {
                            "status": "no_seats_available",
                            "message": "There is error in sending mail",
                            "email": "",
                        }

                        return request.render("isha_samskriti.exception", values)

                # batch_rec.seats_left -= 1

                # the method call saves the data in the database and returns the dict to be passed to gpms
                gpms_dict,existing_rec,bill_add_dict = self.save_profile_data(program, schd, batch, **post)
                _logger.info("inner post methos ----------- Samaskriti Program Lead")
            except Exception as ex:
                _logger.error(
                    "-------Issue in profile completion post method-------------"
                )
                tb_ex = "".join(
                    traceback.format_exception(
                        etype=type(ex), value=ex, tb=ex.__traceback__
                    )
                )
                _logger.error(tb_ex)
                values = {
                    "status": "Error",
                    "message": "",
                    "email": "",
                }
                return request.render("isha_samskriti.exception", values)

            # if registration_id is exist on post method, allow them to redirect to directly to payment gateway
            if post.get("sso_profile_id"):
                _logger.info("inside registration id on POST method")
                try:
                    if existing_rec:
                        existing_rec.write({
                            "reg_status": "Payment Initiated",
                        })
                        # Seats value updated for Existing Record
                        sm_batch_id_rec = request.env['program.schedule.batch.master'].search([('id', '=', existing_rec.sm_batchId.id)], limit=1)
                        if sm_batch_id_rec:
                            current_seats_left = sm_batch_id_rec.seats_left
                            seats_left = current_seats_left - 1
                            sm_batch_id_rec.write({'seats_left': seats_left})

                        # created the payment transaction record
                        payment_trans_rec = self.create_payment_trans_rec(gpms_dict, bill_add_dict,existing_rec)
                        _logger.info("*** Gpms Dictionary Values****")
                        _logger.info(gpms_dict)

                        return request.render(
                            "isha_samskriti.isha_website_payment", gpms_dict
                        )
                        # return self.redirect_to_paymentpage(rec, gpms_dict)

                except Exception as e:
                    values = {
                        "status": "Error",
                        "message": "There is error in sending mail",
                        "email": "",
                    }
                    tb_ex = "".join(
                        traceback.format_exception(
                            etype=type(e), value=e, tb=e.__traceback__
                        )
                    )
                    _logger.info(tb_ex)
                    return request.render("isha_samskriti.exception", values)

    @http.route('/smaskriti/move/registrationform', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def move_registrationform_page(self, **post):
        if request.httprequest.method == "GET":
            _logger.info("***** Move Registration Form Controller ******")

            validation_errors = []
            try:
                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                cururls = str(request.httprequest.url).replace("http://", "https://", 1)
                sso_log = {'request_url': str(cururls),
                           "callback_url": str(cururls),
                           "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                           "hash_value": "",
                           "action": "0",
                           "legal_entity": "IF",
                           "force_consent": "1"
                           }
                ret_list = {'request_url': str(cururls),
                            "callback_url": str(cururls),
                            "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                            }
                data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
                data_enc = data_enc.replace("/", "\\/")
                signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                                     digestmod=hashlib.sha256).hexdigest()
                sso_log["hash_value"] = signature
                sso_log["sso_logurl"] = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_URL1')
                values = {
                    'sso_log': sso_log
                }
                return request.render('isha_livingspaces.livingspacessso', values)
            except Exception as ex:
                _logger.error("Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)
                # values = {'language': language}

        if request.httprequest.method == "POST" and "session_id" in request.httprequest.form:
            # if request.httprequest.method == "GET":
            _logger.info("Loading view  into Move Registration form view")

            ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
            api_key = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.SSO_API_KEY1')  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
            session_id = request.httprequest.form['session_id']
            hash_val = hash_hmac({'session_id': session_id}, ret)
            data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
            request_url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
            sso_user_profile = eval(sso_get_user_profile(request_url, data).text)
            auto_mail = sso_user_profile["autologin_email"]
            profile_id = sso_user_profile["autologin_profile_id"]

            application_data = post.get('reg_values')
            decode_application_data = base64.b64decode(application_data)
            s1 = decode_application_data.decode("UTF-8")
            registration_id = s1.split('&')[0].split('=')[1]
            move_email = s1.split('&')[1].split('=')[1]
            programname = s1.split('&')[2].split('=')[1]
            is_name_transfer = s1.split('&')[3].split('=')[1]

            # rec = request.env['program.lead'].sudo().search(
            #     [("id", "=", registration_id)], limit=1)
            rec = request.env['program.lead'].sudo().search(
                [("id", "=", registration_id),
                 ("sso_id", "=", profile_id)], limit=1)

            # auto_mail = 't4samskriti@mailinator.com'
            # profile_id = ''
            if rec and rec.email == auto_mail:
                values = {
                    "reg_id": registration_id,
                    "move_email": move_email,
                    "programname":programname,
                    "is_name_transfer":is_name_transfer,
                    "login_email": auto_mail,
                    "sso_profile_id": profile_id
                }
                return request.render('isha_samskriti.move_registration_rediret_page', values)
            else:
                if rec.email:
                    reg_email = rec.email
                else:
                    reg_email = ''

                auto_mail = ' ' +str(auto_mail)
                _logger.info("Record mismatch, login email and registration email is different")
                values = {"msg": 'mismatch_log', 'login_email': auto_mail, 'reg_email': reg_email}
                return request.render('isha_samskriti.exception', values)

        if request.httprequest.method == "POST":
            vals = {"reg_status": "Name Transfer Confirmed"}
            program_lead_obj = request.env['program.lead']
            rec = program_lead_obj.sudo().search([("id", "=", post.get("reg_id"))], limit=1)
            rec.sudo().write(vals)
            values = {"person_name": rec.record_name}
            # New Record Created
            # if rec:
            #     if rec.contact_id_fkey:
            #         street = rec.contact_id_fkey.street
            #         city = rec.contact_id_fkey.city
            #         state = rec.contact_id_fkey.state
            #         country = rec.contact_id_fkey.country
            #         zip = rec.contact_id_fkey.zip
            #         phone = rec.contact_id_fkey.phone
            #         rec_name = rec.contact_id_fkey.name
            #
            #     program_id = rec.sm_scheduleId.program_type_id.id
            #     program = request.env["program.type.master"].sudo().search([("id", "=", program_id)])
            #
            #     create_vals = {
            #         "contact_id_fkey":rec.contact_id_fkey.id,
            #         "record_name": rec.record_name if rec.record_name else rec_name,
            #         "record_phone": rec.record_phone if rec.record_phone else phone,
            #         # "phone": rec.phone if rec.phone else phone,
            #         "record_email": post.get("move_email"),
            #         "street":rec.street if rec.street else street,
            #         "record_city":rec.record_city if rec.record_city else city,
            #         "record_state":rec.record_state if rec.record_state else state,
            #         "record_country": rec.record_country if rec.record_country else country,
            #         "record_zip":rec.record_zip if rec.record_zip else zip,
            #         "local_trans_id": rec.local_trans_id,
            #         "local_program_type_id": program.local_program_type_id,
            #         # "local_program_type_id": rec.sm_scheduleId.program_type_id.local_program_type_id,
            #         "program_name": rec.program_type,
            #         "sso_id": post.get("sso_profile_id"),
            #         "sm_scheduleId": rec.sm_scheduleId.id,
            #         "sm_batchId": rec.sm_batchId.id,
            #         "reg_status": "In Progress"
            #         # "program_schedule_info": pg_schd_info,
            #     }
            #     _logger.info("**** Created new record ******"+str(create_vals))
            #     program_lead_obj.sudo().create(create_vals)

            # inherited the model to pass the values to the model from the form#
            return request.render("isha_samskriti.samaskriti_move_registration_success_form", values)
