from odoo.addons.portal.controllers.portal import CustomerPortal

import odoo.http as http
from odoo import _
from odoo.http import request
from odoo.osv.expression import OR

from .main import _get_smas_pgm_ids

class LeadPortal(CustomerPortal):

    def _prepare_portal_layout_values(self):
        values = super(LeadPortal, self)._prepare_portal_layout_values()
        return values

    @http.route(['/samskriti/registrations'], type='http', auth="user", website=True)
    def query_lead_data(self, page=1, date_begin=None, date_end=None, sortby=None, search_in='name', search='', **kw):
        if not request.env.user.has_group('base.group_user'):
            return request.redirect('/my/home')

        values = self._prepare_portal_layout_values()

        partner = request.env.user.partner_id

        searchbar_sortings = {
            'create_date_asc': {'label': _('Created Date (Asc)'), 'order': 'create_date'},
            'create_date_desc': {'label': _('Created Date (Desc)'), 'order': 'create_date desc'},
            'write_date_asc': {'label': _('Updated On (Asc)'), 'order': 'write_date'},
            'write_date_desc': {'label': _('Updated On (Desc)'), 'order': 'write_date desc'},
        }
        searchbar_inputs = {
            'name': {'input': 'name', 'label': _('Search in Contact Name')},
            'email': {'input': 'email', 'label': _('Search in Email')},
            'phone': {'input': 'phone', 'label': _('Search in Phone')},
         
        }

        domain = [('active','=',True),('contact_id_fkey','!=',2),('pgm_type_master_id','in',_get_smas_pgm_ids())] if search else []
        # search
        if search and search_in:
            search_domain = []
            if search_in in ('email', 'all'):
                search_domain = OR([search_domain, [('record_email', 'ilike', search)]])
            if search_in in ('phone', 'all'):
                search_domain = OR([search_domain, [('phone', 'ilike', search)]])
            if search_in in ('name', 'all'):
                search_domain = OR([search_domain, [('record_name', 'ilike', search)]])

            domain += search_domain

        # default sort by value
        if not sortby:
            sortby = 'create_date_desc'
        order = searchbar_sortings[sortby]['order']


        lead_model_sudo = request.env['program.lead'].sudo()

        iecso_recs = lead_model_sudo.search(domain, order=order, limit=10)

        values.update({
            'lead_recs': iecso_recs,
            'page_name': 'iecso',
            # 'pager': pager,
            'searchbar_sortings': searchbar_sortings,
            'searchbar_inputs': searchbar_inputs,
            'search_in': search_in,
            'sortby': sortby,
            # 'archive_groups': archive_groups,
            'default_url': '/samskriti/registrations',
        })
        return request.render("isha_samskriti.portal_sams_reg_view", values)
