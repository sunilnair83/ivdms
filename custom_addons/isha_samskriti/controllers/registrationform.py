import hashlib
import hmac
import json

try:
    from odoo.http import request
except:
    pass
import logging
import traceback
from datetime import timedelta, datetime, date

import requests
import werkzeug
import werkzeug.utils
import werkzeug.wrappers

from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)
null = None
false = False
true = True
from . import main
# Api to get the profile data
def sso_get_user_profile(url, payload):
    dp = {"data": json.dumps(payload)}
    url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
    # request.env['satsang.tempdebuglog'].sudo().create({
    #     'logtext': "sso_get_user_profile config " + url
    # })
    return requests.post(url, data=dp)


def hash_hmac(data, secret):
    data_enc = json.dumps(data['session_id'], sort_keys=True, separators=(',', ':'))
    signature = hmac.new(bytes(secret, 'utf-8'), bytes(data_enc, 'utf-8'), digestmod=hashlib.sha256).hexdigest()
    return signature

class SamaskritiRegistration(http.Controller):

    @http.route("/samaskriti/paymentsuccess",type="http",methods=["GET", "POST"],auth="public",csrf=False,)
    def samaskriti_paymentsuccess(self,status="",amount="",transactionId="",requestId="",currency="",errorCode="",errorReason="",errorMsg="",errCode="",errReason="",errMsg="",**post):
        _logger.info("********** Payment Success (Isha Samaskriti) ************")
        c_value_pr = {
            "payment_status": status,
            "amount": amount,
            "paymenttrackingid": transactionId,
            "orderid": requestId,
            "currency": currency,
            "errorcode": errCode,
            "errorreason": errReason,
            "errormsg": errMsg,
        }
        return self.samaskriti_paymentresponse_returntemplate(c_value_pr)

    @http.route("/samaskriti/paystatus",type="http",methods=["GET", "POST"],auth="public",csrf=False)
    def samaskriti_paystatus(self,status="",amount="",transactionId="",requestId="",currency="",errorCode="",errorReason="",errorMsg="",errCode="",errReason="",errMsg="",**post):
        _logger.info("********** Payment Status (Isha Samaskriti) ************")
        c_value_pr = {
            "payment_status": status,
            "amount": amount,
            "paymenttrackingid": transactionId,
            "orderid": requestId,
            "currency": currency,
            "errorcode": errCode,
            "errorreason": errReason,
            "errormsg": errMsg,
        }
        return self.samaskriti_paymentresponse_returntemplate(c_value_pr)

    def samaskriti_paymentresponse_returntemplate(self, c_value):
        """Update the Payment Transaction on every hits
        :param:@array of values
        @:return: display the msg in HTML template"""
        _logger.info(
            "********** samaskriti_paymentresponse_returntemplate (Isha Samaskriti) ************"
        )
        _logger.info(
            "startinng execution paymentresponse_returntemplate c_value" + str(c_value)
        )
        # errorlog("startinng execution paymentresponse_returntemplate c_value" + str(c_value))
        orderid = str(c_value["orderid"])
        # below line need to removed - enable only for testing
        # self.put_user_sso_info(rec_id)
        err_msg = ""
        person_name = ""
        support_email = "samaskriti.online@ishafoundation.org"
        p_registration_rec = request.env["program.lead"].sudo().search([("local_trans_id", "=", orderid)], limit=1)

        if p_registration_rec:
            person_name = p_registration_rec.record_name
        msg = "Transaction_Failure"
        mail_template_id = None
        if c_value.get("payment_status") == "ERROR":
            err_msg = c_value.get("errMsg")
            if err_msg == 'ERR_VALIDATION':
                values = {
                    'status':'ERROR',
                    'err_msg':err_msg
                }
                return request.render("isha_samskriti.payumoney_status_template", values)
        c_value["transactiontype"] = "synccallback"

        sams_program_type_music = request.env["ir.config_parameter"].sudo().get_param("samaskriti.programcode.music")
        sams_program_type_kalari = request.env["ir.config_parameter"].sudo().get_param("samaskriti.programcode.kalari")
        if p_registration_rec.pgm_type_master_id.program_type == 'Kalaripayattu':
            sams_program_type = sams_program_type_kalari
            sams_code = 'KALARIPAYATTU'
        elif p_registration_rec.pgm_type_master_id.program_type == 'Nirvana Shatakam':
            sams_program_type = sams_program_type_music
            sams_code = 'NIRVANA_SHATAKAM'
        elif p_registration_rec.pgm_type_master_id.program_type == 'Niraive Shakti':
            sams_program_type = sams_program_type_music
            sams_code = 'NIRAIVE_SHAKTI'
        else:
            sams_program_type = sams_program_type_music
            sams_code = 'NIRBHAY_NIRGUN'
        c_value["programtype"] = str(sams_program_type) + '-' + str(sams_code)
        # if len(p_registration_rec) > 0:
        #     try:
        #         c_value["programname"] = p_registration_rec.pgm_type_master_id.pgm_code
        #         c_value["billing_name"] = p_registration_rec.record_name
        #         c_value["billing_email"] = p_registration_rec.record_email
        #         c_value["billing_tel"] = p_registration_rec.record_phone
        #         c_value["billing_city"] = p_registration_rec.record_city
        #         c_value["billing_state"] = p_registration_rec.record_state
        #         c_value["billing_country"] = p_registration_rec.record_country
        #         c_value["programtype"] = str(sams_program_type)+'-'+str(sams_code)
        #     except:
        #         pass
        payment_transa_obj = request.env["samaskriti.participant.paymenttransaction"]
        payment_rec_check = payment_transa_obj.search([('orderid', '=', orderid)], limit=1)
        _logger.info("****** Samaskrit Module Payment Rec - Check **********" + str(payment_rec_check))
        if payment_rec_check:
            c_value['programregistration'] = p_registration_rec.id
            # payment_rec_check.sudo().write(c_value)
            # payment_transaction = payment_rec_check
        # else:
        #     _logger.warning("-.-.-.--Sams-->--The code should never be executed----------")
            try:
                country_rec = request.env['res.country'].sudo().search([('id','=',p_registration_rec.record_country)],limit=1)
                state_rec = request.env['res.country.state'].sudo().search([('id','=',p_registration_rec.record_state)],limit=1)
                c_value['programregistration'] = p_registration_rec.id
                c_value["programname"] = p_registration_rec.pgm_type_master_id.pgm_code
                c_value["programtype"] = str(sams_program_type) + '-' + str(sams_code)
                c_value["billing_name"] = p_registration_rec.record_name
                c_value["billing_email"] = p_registration_rec.record_email
                c_value["billing_tel"] = p_registration_rec.record_phone
                c_value["billing_city"] = p_registration_rec.record_city
                c_value["billing_state"] = state_rec.name
                c_value["billing_country"] = country_rec.code

                _logger.info("****** Inside Samaskriti Module C['Value']  **********" + str(c_value))
                payment_transaction = payment_transa_obj.sudo().create(c_value)
                # payment_transaction = payment_rec_check
                _logger.info("****** Samaskrit Module Payment Transaction **********" + str(payment_transaction))
            except:
                pass

        values = {
            "support_email": support_email,
            "status": c_value.get("payment_status"),
            "transactionId": c_value.get("paymenttrackingid"),
            "err_msg": c_value.get("errormsg") if c_value.get("errormsg") else err_msg,
            "order_id": c_value.get("orderid") if c_value.get("orderid") else "",
            "error_code": c_value.get("errorcode") if c_value.get("errorcode") else "",
            "person_name": "   " + person_name,
            "object": p_registration_rec,
            "trans_rec":payment_rec_check,
            "login_email": "  "+p_registration_rec.record_email,
            "region_support_email_id": payment_transaction.email,
            "is_ishangamone": request.env["ir.config_parameter"].sudo().get_param("tp.goy_is_ishangamone"),
        }
        return request.render("isha_samskriti.payumoney_status_template", values)

    # Payment Check EXpired or not
    def check_payment_link(self, rec):
        is_valid = True
        if rec:
            lead_rec = request.env['program.lead'].sudo().search([('id','=',int(rec))],limit=1)

            # If the program_lead is confirmed the given payment link should be invalidated
            if lead_rec.reg_status == 'Confirmed':
                return False

            req_payment_rec = request.env['samaskriti.participant.paymenttransaction'].sudo().search(
                [('orderid', '=', lead_rec.local_trans_id), ("transactiontype", "=", "Request to Gateway")], limit=1)
            if not req_payment_rec:
                req_payment_rec = request.env['samaskriti.participant.paymenttransaction'].sudo().search(
                    [('orderid', '=', lead_rec.local_trans_id), ('billing_email', '!=', False)], limit=1)
            reg_record =req_payment_rec
                # = request.env['samaskriti.participant.paymenttransaction'].sudo().search([("orderid", "=", lead_rec.local_trans_id)], limit=1)
            if reg_record:
                config_rec = request.env['samaskriti.payment.configuration'].sudo().search([], limit=1)
            today = datetime.today().date()
            if config_rec.invalidate_all_payment_links_on <= today:
                return False
            if config_rec.payment_link_validity and reg_record.payment_link_sent_date:
                Begindatestring = str(reg_record.payment_link_sent_date)
                Begindate = datetime.strptime(Begindatestring, "%Y-%m-%d")
                expire_date = Begindate + timedelta(days=config_rec.payment_link_validity)
                if today > expire_date.date():
                    is_valid = False
        return is_valid

    # Check checkExistingTxn_Samaskriti
    def checkExisting_txn_samaskriti(self, requestidvar, ssoprofileid, **post):
        _logger.info("starting exection checkExistingTxn requestidvar" + str(requestidvar))
        _logger.info("ssoprofileid" + str(ssoprofileid))
        _logger.info("**post" + str(post))
        program_type_ids = request.env['program.type.master'].search([('category','=','Samskriti')])
        ssorecord_exist = request.env['program.lead'].sudo().search([("sso_id", "=", ssoprofileid), ("reg_status", "=", 'Confirmed'),('pgm_type_master_id','in',program_type_ids.ids)])
        record_exist = request.env['program.lead'].sudo().search([("local_trans_id", "=", requestidvar)],order ="id desc",limit=1)
        valid =True
        err_msg = ""
        status = ""
        if len(ssorecord_exist) > 0:
            err_msg = "ERR_COMPLETED"
        if len(record_exist) > 0:
            err_msg = "ERR_VALIDATION"
            status="ERROR"
            if record_exist.reg_status == "Confirmed":
                err_msg = "ERR_COMPLETED"
                valid= False
            if record_exist.reg_status == "INPROGRESS" or record_exist.reg_status == "Inprogress":
                err_msg = "ERR_AWAITED"
                valid = False
            if record_exist.reg_status == "Payment Initiated":
                valid = True
            if record_exist.reg_status == "Failed":
                valid = True
            if record_exist.reg_status == "Cancelled":
                valid = False
                status = "Cancelled"
                err_msg = "user_cancelled"
            return {"valid":valid,"errmsg":err_msg,"status":status}

        else:
            return {"valid":True,"errmsg":err_msg,"status":status}

    @http.route('/samaskriti/registrationform/paymentpage', type='http', auth="public", methods=["GET", "POST"],
                website=True,
                csrf=False)
    def online_smaskriti_registration_payment_status(self, registration_id="", **post):
        if post.get("registration_id"):
            registration_id = post.get("registration_id")
        else:
            registration_id = registration_id
        _logger.info("Payment view - Reg ID "+str(registration_id))
        if request.httprequest.method == "GET":
            is_link_vaild = self.check_payment_link(registration_id)
            _logger.info("Loop into Payment view"+str(is_link_vaild))
            # payment link expired
            if not is_link_vaild:
                _logger.info("link expired")
                values = {"msg": 'link_expired'}
                return request.render('isha_samskriti.exception', values)

            # program_type_ids = request.env['program.type.master'].search([('category', '=', 'Samskriti')])
            # lead_rec = request.env['program.lead'].sudo().search([("id", "=", int(registration_id)),('pgm_type_master_id', 'in', program_type_ids.ids)], limit=1)
            # _logger.info("****** Lead ID(Isha Samaskriti) *******" + str(lead_rec))
            #
            # # Call the Resend payment Link Email template
            # payment_transaction_rec = request.env['samaskriti.participant.paymenttransaction'].sudo().search(
            #     [("orderid", "=", lead_rec.local_trans_id)], limit=1)
            #
            # _logger.info("****** Payment Page information (Isha Samaskriti) ******" + str(payment_transaction_rec))
            # if payment_transaction_rec:
            #     values = {
            #         "reg_id": registration_id,
            #         "payment_lead_id": lead_rec.id,
            #         "amt": payment_transaction_rec.amount,
            #         "currency": payment_transaction_rec.currency
            #     }
            #     return request.render('isha_samskriti.smaskriti_payment_page', values)

            validation_errors = []
            try:
                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                cururls = str(request.httprequest.url).replace("http://", "https://", 1)
                sso_log = {'request_url': str(cururls),
                           "callback_url": str(cururls),
                           "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                           "hash_value": "",
                           "action": "0",
                           "legal_entity": "IF",
                           "force_consent": "1"
                           }
                ret_list = {'request_url': str(cururls),
                            "callback_url": str(cururls),
                            "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                            }
                data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
                data_enc = data_enc.replace("/", "\\/")
                signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                                     digestmod=hashlib.sha256).hexdigest()
                sso_log["hash_value"] = signature
                sso_log["sso_logurl"] = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_URL1')
                values = {
                    'sso_log': sso_log
                }
                return request.render('isha_livingspaces.livingspacessso', values)
            except Exception as ex:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)

        if request.httprequest.method == "POST" and "session_id" in request.httprequest.form:
            _logger.info("Loading view  into POST view")

            ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
            api_key = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.SSO_API_KEY1')  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
            session_id = request.httprequest.form['session_id']
            hash_val = hash_hmac({'session_id': session_id}, ret)
            data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
            request_url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
            sso_user_profile = eval(sso_get_user_profile(request_url, data).text)
            auto_mail = sso_user_profile["autologin_email"]
            profile_id = sso_user_profile["autologin_profile_id"]
            program_type_ids = request.env['program.type.master'].search([('category', '=', 'Samskriti')])
            _logger.info("post-registration" + str(post.get("registration_id")))
            _logger.info("direct registration_id" + str(registration_id))
            lead_rec = request.env['program.lead'].sudo().search(
                [("id", "=", registration_id)], limit=1)
            _logger.info("lead_rec Email" + str(lead_rec.record_email))
            _logger.info("lead_rec" + str(lead_rec))
            is_link_vaild = self.check_payment_link(registration_id)
            _logger.info("Loop into Payment view" + str(is_link_vaild))
            # payment link expired
            if not is_link_vaild:
                _logger.info("link not expired")
                values = {"msg": 'link_expired'}
                return request.render('isha_samskriti.exception', values)
            if lead_rec.record_email != auto_mail:
                _logger.info("Record mismatch, login email and Registration email is different")
                _logger.info("Login Email" + str(auto_mail))
                auto_mail = ' ' + str(auto_mail)
                # return request.render('isha_livingspaces.kshetragna_error_template')
                values = {
                    "msg": 'mismatch_log',
                    "reg_email":lead_rec.record_email,
                    'login_email': "  "+str(auto_mail)
                }
                return request.render('isha_samskriti.exception', values)
            else:
                _logger.info("********** Login Succes, Record *******"+str(lead_rec))
                responsecheckexisting = self.checkExisting_txn_samaskriti(lead_rec.local_trans_id, profile_id, **post)
                _logger.info("********** Exisiting Check *******" + str(responsecheckexisting.get("valid")))
                if responsecheckexisting.get("valid") == False:
                    values = {"support_email": 'info.samaskriti@ishafoundation.org',
                              "status": responsecheckexisting.get("status"),
                              "program_name": "SKT",
                              "login_email": "  "+str(auto_mail),
                              "transactionId": str(lead_rec.local_trans_id) if lead_rec.local_trans_id else '',
                              "region_support_email_id": "",
                              "err_msg": responsecheckexisting.get("errmsg"), }
                    return request.render("isha_samskriti.payumoney_status_template", values)

                # Payment Link Call logic
                program_type_ids = request.env['program.type.master'].search([('category', '=', 'Samskriti')])
                lead_rec = request.env['program.lead'].sudo().search(
                    [("id", "=", int(registration_id)), ('pgm_type_master_id', 'in', program_type_ids.ids)], limit=1)
                _logger.info("****** Lead ID(Isha Samaskriti) *******" + str(lead_rec))

                # Call the Resend payment Link Email template
                # payment_transaction_rec = request.env['samaskriti.participant.paymenttransaction'].sudo().search(
                #     [("orderid", "=", lead_rec.local_trans_id)], limit=1)

                req_payment_rec = request.env['samaskriti.participant.paymenttransaction'].search(
                    [('orderid', '=', lead_rec.local_trans_id), ("transactiontype", "=", "Request to Gateway")], limit=1)
                if not req_payment_rec:
                    req_payment_rec = request.env['samaskriti.participant.paymenttransaction'].search(
                        [('orderid', '=', lead_rec.local_trans_id), ('billing_email', '!=', False)], limit=1)
                payment_transaction_rec = req_payment_rec

                _logger.info("****** Payment Page information (Isha Samaskriti) ******" + str(payment_transaction_rec))
                if payment_transaction_rec:
                    values = {
                        "reg_id": registration_id,
                        "payment_lead_id": lead_rec.id,
                        "amt": payment_transaction_rec.amount,
                        "currency": payment_transaction_rec.currency
                    }
                    return request.render('isha_samskriti.smaskriti_payment_page', values)



            # Call the Resend payment Link Email template
            payment_transaction_rec = request.env['samaskriti.participant.paymenttransaction'].sudo().search([("orderid", "=", lead_rec.local_trans_id)], limit=1)

            _logger.info("********** Lead ID(Isha Samaskriti) ************" + str(lead_rec))
            _logger.info("********** Payment Page information (Isha Samaskriti) ************" + str(payment_transaction_rec))
            if payment_transaction_rec:
                values = {
                    "reg_id": registration_id,
                    "payment_lead_id":lead_rec.id,
                    "amt": payment_transaction_rec.amount,
                    "currency": payment_transaction_rec.currency
                }
                return request.render('isha_samskriti.smaskriti_payment_page', values)

        elif request.httprequest.method == "POST":
            _logger.info("Loading view  POST Form for payment")
            program_lead_rec = request.env['program.lead'].sudo().search([("id", "=", int(registration_id))], limit=1)
            amt = post.get('amt')
            currency = post.get('currency')
            local_trans_id = program_lead_rec.local_trans_id
            return main.Smaskriti.redirect_Payment_gateway(self, program_lead_rec,local_trans_id)

    # Payment Redirect Code
    def redirect_Payment_gatewayss(self,created_rec,local_trans_id):
        """ Redirec to Payment Gateway logic"""
        try:
            sams_gpms_payurl = (request.env["ir.config_parameter"].sudo().get_param("samaskriti.gpms_payment_url"))
            sams_callbackUrl = (request.env["ir.config_parameter"].sudo().get_param("samaskriti.gpms_call_back_url"))
            sams_program_type_music = request.env["ir.config_parameter"].sudo().get_param(
                "samaskriti.programcode.music")
            sams_program_type_kalari = request.env["ir.config_parameter"].sudo().get_param(
                "samaskriti.programcode.kalari")
            if created_rec.program_type == 'Kalaripayattu':
                sams_program_type = sams_program_type_kalari
                sams_code = 'KALARIPAYATTU'
            elif created_rec.program_type == 'Nirvana Shatakam':
                sams_program_type = sams_program_type_music
                sams_code = 'NIRVANA_SHATAKAM'
            elif created_rec.program_type == 'Niraive Shakti':
                sams_program_type = sams_program_type_music
                sams_code = 'NIRAIVE_SHAKTI'
            else:
                sams_program_type = sams_program_type_music
                sams_code = 'NIRBHAY_NIRGUN'
            if created_rec.reg_status == "Failed":
                if created_rec.sm_batchId.seats_left <= 0:
                    values = {
                        "status": "no_seats_available",
                        "message": "There is error in sending mail",
                        "email": "",
                    }
                    return request.render("isha_samskriti.exception", values)
                else:
                    created_rec.sm_batchId.seats_left = created_rec.sm_batchId.seats_left - 1
            created_rec.write({"reg_status": "Payment Initiated"})
            street = ""
            city = ""
            state = ""
            country = ""
            zip = ""
            email = ""
            phone = ""
            name = ""
            if local_trans_id:
                # payment_rec = request.env['samaskriti.participant.paymenttransaction'].search([('orderid','=',local_trans_id)],limit=1)
                req_payment_rec = request.env['samaskriti.participant.paymenttransaction'].search(
                    [('orderid', '=', local_trans_id), ("transactiontype", "=", "Request to Gateway")], limit=1)
                if not req_payment_rec:
                    req_payment_rec = request.env['samaskriti.participant.paymenttransaction'].search(
                        [('orderid', '=', local_trans_id), ('billing_email', '!=', False)], limit=1)
                payment_rec = req_payment_rec
                if payment_rec:
                    amount = payment_rec.amount
                    currency = payment_rec.currency
                    street = (payment_rec.billing_address)[:50] if payment_rec.billing_address else '',
                    city = payment_rec.billing_city
                    state = payment_rec.billing_state
                    country = payment_rec.contact_id_fkey.country
                    zip = payment_rec.billing_zip
                    email = payment_rec.billing_email
                    phone = payment_rec.billing_tel1
                    name = payment_rec.billing_name

            # if created_rec.contact_id_fkey:
            #     street = created_rec.contact_id_fkey.street
            #     city = created_rec.contact_id_fkey.city
            #     state = created_rec.contact_id_fkey.state
            #     country = created_rec.contact_id_fkey.country
            #     zip = created_rec.contact_id_fkey.zip
            #     email = created_rec.contact_id_fkey.email
            #     phone = created_rec.contact_id_fkey.phone
            #     name = created_rec.contact_id_fkey.name

            if created_rec.record_name:
                first_name = created_rec.record_name.split(' ')[0]
                last_name = created_rec.record_name.split(' ')[1]
            values = {
                "firstName": first_name if first_name else name,
                "lastName": last_name if last_name else name,
                "email": created_rec.record_email if created_rec.record_email else email,
                "mobilePhone": phone if phone else created_rec.record_phone,
                "addressLine1": street if street else created_rec.street,  # GPMS accepts max 50 chars
                "addressLine2": "",  # Not a mandatory field
                "city": city if city else created_rec.record_city,
                "state": state if state else created_rec.record_state,
                "postCode": zip if zip else created_rec.record_zip,
                "country": country if country else "IN",
                "amount": amount if amount else 0,
                "currency": currency,
                "requestId": created_rec.local_trans_id,
                "ssoId": created_rec.sso_id,
                "gpms_payurl": sams_gpms_payurl,
                "callbackUrl": sams_callbackUrl,
                "programType": sams_program_type,
                "programCode": sams_code,
            }
            _logger.info("*** Gpms Dictionary Values - Manually****")
            _logger.info(values)
            return request.render("isha_samskriti.isha_website_payment", values)

        except Exception as e:
            values = {"status": 'Error', 'message': "There is error in sending mail",
                      'email': ""}
            tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
            _logger.info(tb_ex)
            return request.render('isha_samskriti.exception', values)
