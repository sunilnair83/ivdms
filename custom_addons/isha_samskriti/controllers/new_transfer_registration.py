import hashlib
import hmac
import json
from datetime import datetime

import werkzeug

try:
    from odoo.http import request
except:
    pass
import logging
import traceback

from odoo import http
from odoo.http import request
from .main import sso_get_user_profile, hash_hmac, get_fullprofileresponse_DTO, sso_get_full_data
import odoo.addons.isha_crm.ContactProcessor as ContactProcessor

_logger = logging.getLogger(__name__)

null = None
false = False
true = True


class SamaskritiTransferRegistration(http.Controller):
    system_id = 44

    def _create_partner_rec(self, msg, local_trans_id):
        """
        The method creates the res.partner record
        :param msg: the sso profile dict
        :return: res.partner record
        """
        sso_id = msg["profileId"]
        basic_profile = ContactProcessor.replaceEmptyString(msg["basicProfile"])
        phone_dict = ContactProcessor.replaceEmptyString(msg["basicProfile"]["phone"])
        extended_profile = ContactProcessor.replaceEmptyString(msg["extendedProfile"])

        name = basic_profile["firstName"]
        if basic_profile["lastName"] and len(basic_profile["lastName"]) > 0:
            name = name + " " + basic_profile["lastName"]
        phone = None
        if phone_dict["number"]:
            phone = "+" + phone_dict["countryCode"] if phone_dict["countryCode"] else ""
            phone += phone_dict["number"]
        whatsapp_cc = None
        whatsapp_number = None
        if basic_profile.get("whatsapp", False):
            whatsapp_cc = basic_profile.get("whatsapp").get("countryCode", None)
            whatsapp_number = basic_profile.get("whatsapp").get("number", None)

        email = basic_profile["email"]
        gender = ContactProcessor.getgender(basic_profile["gender"])
        dob = basic_profile["dob"] if basic_profile["dob"] else None
        nationality = extended_profile["nationality"]
        passport = (
            extended_profile["passportNum"] if extended_profile["passportNum"] else None
        )
        pan = extended_profile["pan"] if extended_profile["pan"] else None
        street = None
        street2 = None
        state = None
        city = None
        country = None
        zip = None
        if len(msg["addresses"]) > 0:
            address_dict = ContactProcessor.replaceEmptyString(msg["addresses"][0])
            street = address_dict["addressLine1"]
            street2 = address_dict["addressLine2"]
            if address_dict["townVillageDistrict"]:
                if street2 and type(street2) == str:
                    street2 = street2 + ", " + address_dict["townVillageDistrict"]
                else:
                    street2 = address_dict["townVillageDistrict"]
            city = address_dict["city"]
            state = address_dict["state"]
            country = address_dict["country"]
            zip = address_dict["pincode"]

        odooRec = {
            "system_id": self.system_id,
            "local_contact_id": local_trans_id,
            "active": True,
            "name": name,
            "display_name": name,
            "prof_dr": None,
            "phone": phone,
            "phone2": None,
            "phone3": None,
            "phone4": None,
            "whatsapp_number": whatsapp_number,
            "whatsapp_country_code": whatsapp_cc,
            "email": email,
            "email2": None,
            "email3": None,
            "street": street,
            "street2": street2,
            "city": city,
            "state": state,
            "country": country,
            "zip": zip,
            "street2_1": None,
            "street2_2": None,
            "city2": None,
            "state2": None,
            "country2": None,
            "zip2": None,
            "work_street1": None,
            "work_street2": None,
            "work_city": None,
            "work_state": None,
            "work_country": None,
            "work_zip": None,
            "gender": gender,
            "occupation": None,
            "marital_status": None,
            "dob": dob,
            "nationality": nationality,
            "deceased": None,
            "contact_type": None,
            "companies": None,
            "aadhaar_no": None,
            "pan_no": pan,
            "passport_no": passport,
            "dnd_phone": None,
            "dnd_email": None,
            "dnd_postmail": None,
            "sso_id": sso_id,
            "phone_country_code": phone_dict.get('countryCode', "")
        }

        odooRec = ContactProcessor.validateValues(odooRec, {}, {})

        # Switcing to Cron based contact assignment
        # rec = request.env["res.partner"].sudo().create(odooRec)
        # return rec

        return odooRec

    def check_link_expiry(self, trans_rec):
        '''
        the method will check if the transfer link has expired or not

        :param trans_rec: id of the transfer record (samskriti.program_transfer)
        :return: bool, request render
        '''
        program_transfer_rec = request.env['samskriti.program.transfers.history'].sudo().search([('id', '=', trans_rec)],
                                                                                        limit=1)
        valid = True
        if program_transfer_rec.has_expired:
            valid = False
            values = {"msg": 'transfer_link_expired'}
            return valid, request.render('isha_samskriti.exception', values)

        return valid, False

    def get_current_country(self, country_id):
        country = request.env["res.country"].sudo().search([("id", "=", country_id)])

        entity = request.env["res.country.group"].sudo().search([("country_ids.id", "=", country.id)])

        return {
            "country": country,
            "entity": entity,
        }

    def save_survey_questions(self, rec, program, **post):
        """
        The method saves survey master questions against program lead record

        :param rec: program.lead record
        :param program: program.type.master record
        :param post: post params
        :return: None
        """

        # getting all the questions
        if not post.get("health_disclaimer", False):
            questions = program.mapped("sm_questions")
        else:
            _logger.info(
                "********** hlab Questions (Isha Samaskriti) ************"
                + str(type(post.get("hlab")))
            )
            questions = program.sm_questions.filtered(
                lambda x: x.id == int(post.get("hlab"))
            )

        # updating the post params with program.lead rec
        post.update({"program_lead_rec": rec})

        # looping through questions and saving them
        for question in questions:
            # the tag helps to fetch the answer from the dict
            answer_tag = "%s_%s" % (program.id, question.id)
            request.env["survey.user_input_line"].sudo().sams_save_lines(
                False, question, post, answer_tag
            )

    def save_profile_data(self, existing_rec, **post):
        '''
        the method saves the program lead data for profile completion

        :param existing_rec: the program.lead record to update
        :param post: the dict with misc extra params
        :return: dicts
        '''

        sams_gpms_payurl = request.env["ir.config_parameter"].sudo().get_param("samaskriti.gpms_payment_url")
        sams_callbackUrl = request.env["ir.config_parameter"].sudo().get_param("samaskriti.gpms_call_back_url")

        sams_program_type_music = request.env["ir.config_parameter"].sudo().get_param(
            "samaskriti.programcode.music")
        sams_program_type_kalari = request.env["ir.config_parameter"].sudo().get_param(
            "samaskriti.programcode.kalari")

        program_name = existing_rec.local_program_type_id

        if program_name == 'Kalaripayattu':
            sams_program_type = sams_program_type_kalari
            sams_code = 'KALARIPAYATTU'
        elif program_name == 'Nirvana Shatakam':
            sams_program_type = sams_program_type_music
            sams_code = 'NIRVANA_SHATAKAM'
        elif program_name == 'Niraive Shakti':
            sams_program_type = sams_program_type_music
            sams_code = 'NIRAIVE_SHAKTI'
        else:
            sams_program_type = sams_program_type_music
            sams_code = 'NIRBHAY_NIRGUN'

        if existing_rec:
            # the following if else fetches state or country from db
            if post.get("bill_state"):
                state_recd = request.env["res.country.state"].sudo().search(
                    [("id", "=", post.get("bill_state"))])

                country_rec = state_recd.country_id
                state_rec = state_recd.name
            else:
                country_rec = request.env["res.country"].sudo().search([("id", "=", post.get("bill_country"))])

                state_rec = post.get('bill_state_text')
                state_recd = False

            if post.get("is_name_transfer", False):
                # updating the record
                countrycode = post.get("country_code")
                phone = post.get("phone_number")
                same_whatappnumber = post.get("issame")
                wtsappcountrycode = post.get("whatsappnumbercode")
                wtsappphone = post.get("whatsappnumber")
                if same_whatappnumber == "YES" or not wtsappcountrycode or not wtsappphone:
                    wtsappcountrycode = countrycode
                    wtsappphone = phone


                pg_schd_info = eval(existing_rec.program_schedule_info)

                pg_schd_info["pii"].update(
                    {
                        "phone_country_code": countrycode,
                        "phone": phone,
                        "email": post.get("email"),
                        "whatsapp_country_code": wtsappcountrycode,
                        "whatsapp_number": wtsappphone,
                        'street': post.get("bill_address"),
                        'city': post.get("bill_district"),
                        'state': state_rec,
                        'state_id': state_recd and state_recd.id,
                        'country': country_rec.name,
                        'country_id': country_rec.id,
                        'zip': post.get("billl_pincode"),
                    }
                )

                existing_rec.write(
                    {
                        "record_name": f"{post.get('first_name')} {post.get('last_name')}",
                        "reg_status": "Registered",
                        "record_phone": post.get("phone_number"),
                        "phone": post.get("phone_number"),
                        "record_email": post.get("email"),
                        # "record_email":post.get("bill_address"),
                        "record_city": post.get("bill_district"),
                        "record_state": post.get("bill_state"),
                        "record_zip": post.get("billl_pincode"),
                        "record_country": post.get("bill_country"),
                        "program_schedule_info": pg_schd_info,
                    }
                )
            else:
                whatsapp_number = post.get("whatsappnumber", False)
                if not whatsapp_number:
                    whatsapp_number = post.get("phone_number")
                existing_rec.write(
                    {
                        "record_name": f"{post.get('first_name')} {post.get('last_name')}",
                        "reg_status": "Registered",
                        "record_phone": post.get("phone_number"),
                        "phone": post.get("phone_number"),
                        "record_email": post.get("email"),
                        # "record_email":post.get("bill_address"),
                        "record_city": post.get("bill_district"),
                        "record_state": post.get("bill_state"),
                        "record_zip": post.get("billl_pincode"),
                        "record_country": post.get("bill_country"),
                        "whatsapp_number": whatsapp_number
                    }
                )

            # Todo: use this dict to save bill address in payment transaction
            bill_add_dict = {
                "pincode": post.get("billl_pincode"),
                "bill_country": post.get("bill_country"),
                "bill_district": post.get("bill_district"),
                "bill_state": state_rec,
                "bill_address": post.get("bill_address"),
                "program_name": existing_rec.program_name
            }

            # saving questions that are rendered from the survey questions master
            self.save_survey_questions(existing_rec, existing_rec.pgm_type_master_id, **post)

            # gpms dict creation
            if post.get("price_currency"):
                price_currency = post.get("price_currency")
            elif country_rec:
                price_currency = country_rec.currency_id.name

            if price_currency:
                price_currency = price_currency
            else:
                price_currency = 'INR'

            if post.get("sso_profile_id"):
                profile_id = post.get("sso_profile_id")
            else:
                profile_id = existing_rec.sso_id

            re_registration = 'ereceipt.reregistration=1'
            gpms_dict = {
                "firstName": post.get("first_name"),
                "lastName": post.get("last_name"),
                "email": post.get("email"),
                "mobilePhone": post.get("phone_number"),
                "addressLine1": post.get("bill_address"),  # GPMS accepts max 50 chars
                "city": post.get("bill_district"),
                "state": state_rec,
                "postCode": post.get("billl_pincode"),
                "country": country_rec and country_rec.code or "IN",
                "amount": post.get("delta_amount"),
                "currency": price_currency,
                "requestId": existing_rec.local_trans_id,
                "ssoId": profile_id,
                "programCode": sams_code,
                "programType": sams_program_type,
                "params": str(re_registration),
                "gpms_payurl": sams_gpms_payurl,
                "callbackUrl": sams_callbackUrl,
            }

            return gpms_dict, existing_rec, bill_add_dict

    def create_validate_rec(self, trans_rec):
        '''
        the method creates a new program_lead rec or updates an existing one

        :param trans_rec: transfer record
        :return: program.lead record
        '''

        # fetching the param which creates local trans id
        sams_program_type_music = request.env["ir.config_parameter"].sudo().get_param("samaskriti.programcode.music")
        sams_program_type_kalari = request.env["ir.config_parameter"].sudo().get_param("samaskriti.programcode.kalari")

        program = trans_rec.target_program.local_program_type_id

        if program == 'Kalaripayattu':
            sams_program_type = sams_program_type_kalari
        elif program == 'Nirvana Shatakam':
            sams_program_type = sams_program_type_music
        elif program == 'Niraive Shakti':
            sams_program_type = sams_program_type_music
        else:
            sams_program_type = sams_program_type_music

        # creating new local trans id
        local_trans_id = f"{'T'}-{trans_rec.name.sso_id}-{sams_program_type}-{trans_rec.target_schedule.id}-{trans_rec.target_batch.id}"

        lead_rec = request.env['program.lead'].sudo().search(
            [('local_trans_id', '=', local_trans_id), ('transfer_rec', '=', trans_rec.name.id)],
            limit=1)

        if not lead_rec:
            new_rec_dict = dict(
                reg_status="Transfer In Progress",
                amount_delta=trans_rec.amount,
                transfer_local_trans_id=False,
                transfer_rec=trans_rec.name.id,
                local_program_type_id=trans_rec.target_program.local_program_type_id,
                program_name=trans_rec.target_program.local_program_type_id,
                pgm_type_master_id=trans_rec.target_program.id,
                sm_scheduleId=trans_rec.target_schedule.id,
                sm_batchId=trans_rec.target_batch.id,
                sm_answers=False,
                local_trans_id=local_trans_id,
                is_confirmation_email=False,
                confirmation_email_send_date=False,
                payment_email_send_date=False,
                is_payment_email=False,
                contact_id_fkey=2

            )

            lead_rec = trans_rec.name.sudo().copy(new_rec_dict)

        # increment seat in the old batch
        trans_rec.name.sm_batchId.seats_left += 1

        # decrement seats in the newly selected batch
        trans_rec.target_batch.seats_left -= 1

        return lead_rec

    def create_validate_name_rec(self, rec, **post):
        # fetching the param which creates local trans id
        sams_program_type_music = request.env["ir.config_parameter"].sudo().get_param("samaskriti.programcode.music")
        sams_program_type_kalari = request.env["ir.config_parameter"].sudo().get_param("samaskriti.programcode.kalari")

        program = rec.local_program_type_id

        if program == 'Kalaripayattu':
            sams_program_type = sams_program_type_kalari
        elif program == 'Nirvana Shatakam':
            sams_program_type = sams_program_type_music
        elif program == 'Niraive Shakti':
            sams_program_type = sams_program_type_music
        else:
            sams_program_type = sams_program_type_music

        # creating new local trans id
        local_trans_id = f"{'T'}-{post.get('sso_profile_id')}-{sams_program_type}-{rec.sm_scheduleId.id}-{rec.sm_batchId.id}"

        lead_rec = request.env['program.lead'].sudo().search(
            [('local_trans_id', '=', local_trans_id), ('transfer_rec', '=', rec.id)],
            limit=1)

        if not lead_rec:

            ret = request.env["ir.config_parameter"].sudo().get_param("satsang.SSO_API_SECRET1")

            api_key = request.env["ir.config_parameter"].sudo().get_param("satsang.SSO_API_KEY1")

            session_id = post.get('access_token')
            hash_val = hash_hmac({"session_id": session_id}, ret)
            data = {
                "api_key": api_key,
                "session_id": session_id,
                "hash_value": hash_val,
            }
            request_url = (
                request.env["ir.config_parameter"]
                    .sudo()
                    .get_param("satsang.SSO_LOGIN_INFO_ENDPOINT1")
            )
            sso_user_profile = eval(sso_get_user_profile(request_url, data).text)

            fullprofileresponsedatVar = get_fullprofileresponse_DTO()

            if "autologin_email" in sso_user_profile:
                fullprofileresponsedatVar["basicProfile"]["email"] = sso_user_profile["autologin_email"]
                fullprofileresponsedatVar["profileId"] = sso_user_profile["autologin_profile_id"]

                fullprofileresponsedatVar = sso_get_full_data(
                    sso_user_profile["autologin_profile_id"]
                )

            odoo_rec = self._create_partner_rec(
                fullprofileresponsedatVar, local_trans_id
            )

            lead_rec = request.env["program.lead"].sudo().create(
                {
                    "local_program_type_id": rec.local_program_type_id,
                    "program_name": rec.local_program_type_id,
                    "sso_id": post.get("sso_profile_id"),
                    "sm_scheduleId": rec.sm_scheduleId.id,
                    "sm_batchId": rec.sm_batchId.id,
                    "reg_status": "Transfer In Progress",
                    "pgm_type_master_id": rec.pgm_type_master_id.id,
                    "contact_id_fkey": 2,
                    "program_schedule_info": {"pii": odoo_rec},
                    "local_trans_id": local_trans_id,
                    "transfer_rec": rec.id,
                })
        else:
            lead_rec.write({
                "local_program_type_id": rec.local_program_type_id,
                "program_name": rec.local_program_type_id,
                "sso_id": post.get("sso_profile_id"),
                "sm_scheduleId": rec.sm_scheduleId.id,
                "sm_batchId": rec.sm_batchId.id,
                "reg_status": "Transfer In Progress",
                "pgm_type_master_id": rec.pgm_type_master_id.id,
                "local_trans_id": local_trans_id,
                "transfer_rec": rec.id,
            })

        return lead_rec

    def get_program_transfer_vals(self, sso_user_profile, session_id, **post):
        '''
        the method creates the values needed for the program transfer template renderring

        :param sso_user_profile: the dict containing the sso details of the user
        :param session_id: the sso session id of the current user
        :param post: the dict containing the misc data
        :return: dict
        '''
        # fetching the transfer record
        trans_rec = post.get('trans_rec')
        program_transfer_rec = request.env['samskriti.program.transfers.history'].sudo().search([('id', '=', trans_rec)],
                                                                            limit=1)
        _logger.info("SSO profile ID "+str(sso_user_profile["autologin_profile_id"]))
        _logger.info("SSO ID "+str(post.get('test_sso_id')))

        if not post.get('test_sso_id'):
        # profile_id = 'zxcO9WvAsaPy90VEBtr6EsO3H0e2'
        # auto_mail = 'testsamishaas1@mailinator.com'
        # checking if logged in user and the registered user is one and same or not
            if sso_user_profile["autologin_profile_id"] != program_transfer_rec.name.sso_id:
                autologin_email = sso_user_profile["autologin_email"]
                auto_mail = ' ' + str(autologin_email)
                values = {"msg": 'mismatch_log',
                          'login_email': auto_mail,
                          'reg_email': program_transfer_rec.record_email}
                return request.render('isha_samskriti.exception', values)

        # params to fill the confirmation form
        pgm_id = program_transfer_rec.current_program
        pgm_amt = program_transfer_rec.amount or "0.0"
        current_schedule = program_transfer_rec.current_schedule
        batch_id = program_transfer_rec.current_batch
        transfer_pgm_id = program_transfer_rec.target_program.local_program_type_id
        program_schedule_name = program_transfer_rec.target_schedule_date
        program_batch_name = program_transfer_rec.target_batch.display_text

        return {
            "pgm_id": pgm_id,
            "batch_id": batch_id,
            "current_schedule": current_schedule,
            "pgm_amt": pgm_amt,
            "transfer_pgm_id": transfer_pgm_id,
            'transfer_schedule_id': program_schedule_name,
            "transfer_batch_id": program_batch_name,
            "transfer_record": program_transfer_rec.id,
            "access_token": session_id,
            "sso_profile_id": sso_user_profile["autologin_profile_id"],
            "is_name_transfer": post.get("is_name_transfer", False) and True or False
        }

    def get_name_transfer_vals(self, sso_profile_id, session_id, **post):
        '''
        the method creates the values needed for the program transfer template rendering

        :param session_id: the sso session id of the current user
        :param post: the dict containing the misc data
        :return: dict
        '''
        # fetching the reg record
        reg_id = post.get('rec_id')
        reg_rec = request.env['program.lead'].sudo().search([('id', '=', reg_id)])

        if reg_rec.transfer_email:
            if reg_rec.transfer_email != sso_profile_id['autologin_email']:
                values = {"msg": 'name_trans_mismatch',
                      'login_email': sso_profile_id["autologin_email"],
                      'reg_email': reg_rec.transfer_email}
                return request.render('isha_samskriti.exception', values)


        # params to fill the confirmation form
        pgm_id = reg_rec.pgm_type_master_id.local_program_type_id

        current_schedule = f"{reg_rec.sm_scheduleId.start_date.strftime('%d %B %Y')}-{reg_rec.sm_scheduleId.end_date.strftime('%d %B %Y')}"
        batch_id = reg_rec.sm_batchId.display_text

        return {
            "pgm_id": pgm_id,
            "batch_id": batch_id,
            "current_schedule": current_schedule,
            "reg_id": reg_rec,
            "access_token": session_id,
            "sso_profile_id": sso_profile_id['autologin_profile_id'],
            "is_name_transfer": post.get("is_name_transfer", False) and True or False
        }

    @http.route('/registrations/samskriti/transfer', type='http', auth="public", methods=["GET", "POST"],
                website=True, csrf=False)
    def index(self, **post):
        if request.httprequest.method == "GET":
            # checking if the link has expired or not
            valid, temp = self.check_link_expiry(post.get('trans_rec'))

            if not valid:
                return temp

            if post.get("is_logout") == "yes":
                _logger.info("................inside get session")
                post.pop("is_logout")
                return request.render("isha_satsang.logout_sso_auto_redirect")
            try:
                ret = request.env["ir.config_parameter"].sudo().get_param("satsang.SSO_API_SECRET1")

                if "localhost" in request.httprequest.url:
                    cururls = request.httprequest.url
                else:
                    cururls = str(request.httprequest.url).replace("http://", "https://", 1)

                sso_log = {
                    "request_url": str(cururls),
                    "callback_url": str(cururls),
                    "api_key": request.env["ir.config_parameter"]
                        .sudo()
                        .get_param(
                        "satsang.SSO_API_KEY1"
                    ),  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                    "hash_value": "",
                    "action": "0",
                    "legal_entity": "IF",
                    "force_consent": "1",
                }
                ret_list = {
                    "request_url": str(cururls),
                    "callback_url": str(cururls),
                    "api_key": request.env["ir.config_parameter"]
                        .sudo()
                        .get_param(
                        "satsang.SSO_API_KEY1"
                    ),  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                }
                data_enc = json.dumps(ret_list, sort_keys=True, separators=(",", ":"))
                data_enc = data_enc.replace("/", "\\/")
                signature = hmac.new(
                    bytes(ret, "utf-8"),
                    bytes(data_enc, "utf-8"),
                    digestmod=hashlib.sha256,
                ).hexdigest()
                sso_log["hash_value"] = signature
                sso_log["sso_logurl"] = (
                    request.env["ir.config_parameter"]
                        .sudo()
                        .get_param("satsang.SSO_LOGIN_URL1")
                )
                values = {"sso_log": sso_log}
                # return request.render('isha_livingspaces.livingspacessso', values)
                return request.render("isha_samskriti.onlinesamskritisso", values)
            except Exception as ex:
                tb_ex = "".join(
                    traceback.format_exception(
                        etype=type(ex), value=ex, tb=ex.__traceback__
                    )
                )
                _logger.error(tb_ex)
                _logger.error("Error caught during request creation", exc_info=True)

        if request.httprequest.method == "POST" and "session_id" in request.httprequest.form:
            # getting the session id
            session_id = request.httprequest.form['session_id']

            ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
            api_key = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.SSO_API_KEY1')
            hash_val = hash_hmac({'session_id': session_id}, ret)
            data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
            request_url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
            sso_user_profile = eval(sso_get_user_profile(request_url, data).text)

            # get relevant dicts based on transfer type in order to render the template
            if not post.get("is_name_transfer", False):
                values = self.get_program_transfer_vals(sso_user_profile, session_id, **post)

            else:
                values = self.get_name_transfer_vals(sso_user_profile, session_id, **post)

            # returning the error template if the values isn't dict but response object
            if not isinstance(values, dict):
                return values

            _logger.info('******** Program transfer record' + str(values))
            return request.render('isha_samskriti.transfer_registration_rediret_page', values)

        if request.httprequest.method == "POST":
            # initializing the variables to be used for program identification
            trans_rec = False

            if not post.get("is_name_transfer", False):
                # fetching the transfer record
                trans_rec = request.env['samskriti.program.transfers.history'].sudo().search(
                    [('id', '=', int(post.get('transfer_record')))],
                    limit=1)

                # updating the status for program_lead record of old one
                trans_rec.name.write({"reg_status": "Program Transfer Confirmed"})

                # creating a new record for the transfer
                lead_rec = self.create_validate_rec(trans_rec)
            else:
                # getting the required program lead record
                reg_id = post.get('rec_id')
                reg_rec = request.env['program.lead'].sudo().search([('id', '=', reg_id)])

                # creating a new record for the transfer
                lead_rec = self.create_validate_name_rec(reg_rec, **post)

            # checking if the transfer request is already under progress
            if lead_rec.reg_status in ['Confirmed', 'Payment Initiated', 'Registered']:
                values = {"msg": 'transfer_in_progress'}
                return request.render('isha_samskriti.exception', values)

            # checking what should be the first page of the user
            # since for kalari we've an extra step of taking user's
            # health consent
            if lead_rec.local_program_type_id == "Kalaripayattu":
                page = 0
            else:
                page = 1

            if page == 0:
                access_token = post.get('access_token')
                tmpurl = request.httprequest.url.replace(
                    "/transfer", "/transfer/healthdisclaimer"
                )
                health_disclaimer_url = (
                    f"{tmpurl}&access_token={access_token}&trans_rec={trans_rec and trans_rec.id or False}&rec={lead_rec.id}"
                )
                return werkzeug.utils.redirect(health_disclaimer_url)
            else:
                access_token = post.get('access_token')
                tmpurl = request.httprequest.url.replace(
                    "/transfer", "/transfer/profilecompletion"
                )
                profile_complete_url = (
                    f"{tmpurl}&access_token={access_token}&rec={lead_rec.id}"
                )
                return werkzeug.utils.redirect(profile_complete_url)

    @http.route(
        "/registrations/samskriti/transfer/healthdisclaimer",
        type="http",
        auth="public",
        methods=["GET", "POST"],
        website=True,
        csrf=False,
    )
    def _samskriti_transfer_health_disclaimer(self, access_token="", rec="", trans_rec="", **post):
        """
        The controller renders the health disclaimer form and asks the user to agree before proceeding

        :param programname: The program for which user is registering
        :param access_token: The access token based on which the user's will data will be retrieved
        :param post: The post params to save in db
        :return: The template or url to redirect the user to
        """
        if request.httprequest.method == "GET":
            try:

                lead_rec = request.env['program.lead'].sudo().search([('id', '=', rec)])

                program = lead_rec.pgm_type_master_id

                disclaimers = (
                    program.sm_questions
                    and program.sm_questions.filtered(
                    lambda x: x.question_type == "health_disclaimer"
                )
                    or False
                )

                if disclaimers:
                    disclaimer = disclaimers[0]
                else:
                    # Todo: Render a template saying no health disclaimers
                    return

                values = {
                    "ptm": program,
                    "disclaimer": disclaimer,
                    "program_name": lead_rec.local_program_type_id,
                    "access_token": access_token,
                    "trans_rec": trans_rec or False,
                    "rec": rec,
                    "rec_id": post.get("rec_id", False),
                    "url_string": "transfer/healthdisclaimer"
                }

                return request.render("isha_samskriti.questionpage", values)
            except Exception as ex:
                _logger.error(
                    "-------Issue in re-reg health disclaimer get method-------------"
                )
                tb_ex = "".join(
                    traceback.format_exception(
                        etype=type(ex), value=ex, tb=ex.__traceback__
                    )
                )
                _logger.error(tb_ex)
                values = {
                    "status": "Error",
                    "message": "",
                    "email": "",
                }
                return request.render("isha_samskriti.exception", values)

        elif request.httprequest.method == "POST":
            try:
                rec = request.env['program.lead'].sudo().search([('id', '=', rec)])

                question = request.env['survey.question'].sudo().search([('id', '=', post.get('question_label'))],
                                                                        limit=1)

                post.update({"program_lead_rec": rec, 'agreeddate': str(datetime.now())})

                request.env["survey.user_input_line"].sudo().custom_save_line_health_disclaimer(
                    False, question, post, False
                )

                tmpurl = request.httprequest.url.replace(
                    "healthdisclaimer", "profilecompletion"
                )

                if post.get('rec_id', False) and post.get('rec_id') != None:
                    profile_complete_url = (
                        f"{tmpurl}?access_token={access_token}&rec={rec.id}&rec_id={post.get('rec_id')}"
                    )
                else:
                    profile_complete_url = (
                        f"{tmpurl}?access_token={access_token}&rec={rec.id}&trans_rec={trans_rec}"
                    )

                return werkzeug.utils.redirect(profile_complete_url)

            except Exception as ex:
                _logger.error(
                    "-------Issue in health disclaimer post method-------------"
                )
                tb_ex = "".join(
                    traceback.format_exception(
                        etype=type(ex), value=ex, tb=ex.__traceback__
                    )
                )
                _logger.error(tb_ex)
                values = {
                    "status": "Error",
                    "message": "",
                    "email": "",
                }
                return request.render("isha_samskriti.exception", values)

    @http.route(
        "/registrations/samskriti/transfer/profilecompletion",
        type="http",
        auth="public",
        methods=["GET", "POST"],
        website=True,
        csrf=False,
    )
    def _samskriti_transfer_profile_completion(self, trans_rec="", access_token="", rec="", **post):
        """
        The controller is used to render the profile detail page and save it in user db

        :param programname: The program name
        :param access_token: The access token based on which we'll retrieve sso user profile
        :param schd: The schedule user signed up for
        :param batch: The batch user signed up for
        :param post: The post params dict
        :return: Return the url or the template to render
        """
        if request.httprequest.method == "GET":
            try:
                trans_rec = trans_rec and request.env['samskriti.program.transfers.history'].sudo().search([('id', '=', trans_rec)],
                                                                                         limit=1) or False

                # rec_id is the id of the old record in case of name transfer
                # we're using it to fetch the record country, this is assuming
                # that we won't allow intercountry migrations
                if post.get('rec_id', False):
                    lead_rec = request.env['program.lead'].sudo().search([('id', '=', post.get('rec_id'))],
                                                                         limit=1)
                else:
                    # in the else part we're fetching the new record that was created for program tranfer
                    # since in program transfer the new record would have all the mandatory information
                    # such as country and stuff, we can use the newly created record unlike in case of
                    # name transfers
                    lead_rec = request.env['program.lead'].sudo().search([('id', '=', rec)],
                                                                         limit=1)

                program = lead_rec.pgm_type_master_id

                ret = request.env["ir.config_parameter"].sudo().get_param("satsang.SSO_API_SECRET1")

                api_key = request.env["ir.config_parameter"].sudo().get_param("satsang.SSO_API_KEY1")

                # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                session_id = access_token
                hash_val = hash_hmac({"session_id": session_id}, ret)
                data = {
                    "api_key": api_key,
                    "session_id": session_id,
                    "hash_value": hash_val,
                }
                request_url = (
                    request.env["ir.config_parameter"]
                        .sudo()
                        .get_param("satsang.SSO_LOGIN_INFO_ENDPOINT1")
                )
                sso_user_profile = eval(sso_get_user_profile(request_url, data).text)

                fullprofileresponsedatVar = get_fullprofileresponse_DTO()

                if "autologin_email" in sso_user_profile:
                    fullprofileresponsedatVar["basicProfile"]["email"] = sso_user_profile["autologin_email"]
                    fullprofileresponsedatVar["profileId"] = sso_user_profile["autologin_profile_id"]

                    fullprofileresponsedatVar = sso_get_full_data(
                        sso_user_profile["autologin_profile_id"]
                    )

                # get the pricing for the current country based on the entity of current country
                if trans_rec:
                    price = trans_rec.amount
                else:
                    price = 0.0

                # the current country fetched via the ip
                current_country = self.get_current_country(lead_rec.record_country)

                # country list to render the country dropdown
                countries = request.env["res.country"].sudo().search([])

                # the terms and condition to render for the current entity
                terms_conds = program.sm_questions.filtered(
                    lambda x: x.sm_tcEntity == current_country.get("entity")
                              and x.question_type == "terms_conditions"
                )

                values = {
                    "ptm": lead_rec.pgm_type_master_id,
                    "object": program,
                    "profile": fullprofileresponsedatVar,
                    "delta_amount": price,
                    "program_name": lead_rec.pgm_type_master_id.local_program_type_id,
                    "sso_prof": fullprofileresponsedatVar,
                    "countr": current_country["country"],
                    "access_token": access_token,
                    "countries": countries,
                    "term_cond": terms_conds,
                    "trans_rec": trans_rec and trans_rec.id or False,
                    "sso_profile_id": fullprofileresponsedatVar["profileId"],
                    "rec": rec,
                    "url_string": "transfer/profilecompletion",
                    "is_name_transfer":post.get("is_name_transfer",False)
                }
                _logger.error(
                    "------- Personal Details -------------"+str(values)
                )
                return request.render("isha_samskriti.personaldetails", values)
            except Exception as ex:
                _logger.error(
                    "-------Issue in profile completion get method-------------"
                )
                tb_ex = "".join(
                    traceback.format_exception(
                        etype=type(ex), value=ex, tb=ex.__traceback__
                    )
                )
                _logger.error(tb_ex)
                values = {
                    "status": "Error",
                    "message": "",
                    "email": "",
                }
                return request.render("isha_samskriti.exception", values)

        if request.httprequest.method == "POST":
            try:
                # logging the inputs
                _logger.info("-----Program transfer post is %s--------------" % (post))

                if trans_rec:
                    trans_rec = request.env['samskriti.program.transfers.history'].sudo().search(
                        [('id', '=', trans_rec)],
                        limit=1)
                    post.update({'delta_amount': trans_rec.amount})

                lead_rec = request.env['program.lead'].sudo().search(
                    [('id', '=', rec)],
                    limit=1)

                # fetching the batch record in order to check seats left
                batch_rec = lead_rec.sm_batchId

                # the method call saves the data in the database and returns the dict to be passed to gpms
                gpms_dict, existing_rec, bill_add_dict = self.save_profile_data(lead_rec, **post)

                if lead_rec.amount_delta == 0:
                    lead_rec.reg_status = "Confirmed"

                    trans_rec = self.create_payment_trans_rec(gpms_dict, bill_add_dict, existing_rec)

                    values = {
                        'status': 'TRANSFER_COMPLETED',
                        'trans_rec': trans_rec
                    }

                    return request.render("isha_samskriti.payumoney_status_template", values)


                else:
                    # ToDo: please add the gpms flow
                    # 1. sending the user to payment gateway with amount delta
                    # 2. don't decrement the seats
                    # 3. create a new "Request to Gateway" transaction record
                    # 4. add a new field in transaction table to hold value of old local tnx id
                    payment_trans_rec = self.create_payment_trans_rec(gpms_dict, bill_add_dict, existing_rec)
                    _logger.info(payment_trans_rec)
                    _logger.info("*** Gpms Dictionary Values - Re- Registration ****")
                    _logger.info(gpms_dict)
                    return request.render("isha_samskriti.isha_website_payment", gpms_dict)

            except Exception as ex:
                _logger.error(
                    "-------Issue in profile completion post method-------------"
                )
                tb_ex = "".join(
                    traceback.format_exception(
                        etype=type(ex), value=ex, tb=ex.__traceback__
                    )
                )
                _logger.error(tb_ex)
                values = {
                    "status": "Error",
                    "message": "",
                    "email": "",
                }
                return request.render("isha_samskriti.exception", values)

    def create_payment_trans_rec(self, gpms_dict, bill_add_dict, existing_rec):
        '''
        method creates the fisrt record for payment transaction table
        :param gpms_dict: dict with payment contents
        :param bill_add_dict: dict with bill address contens
        :return: None
        '''
        if not (gpms_dict and bill_add_dict):
            print("Data passed isn't proper")
            return

        trns_var = request.env['samaskriti.participant.paymenttransaction'].sudo()

        # checking if the payment trans record already exists
        trans_rec = trns_var.search([('orderid', '=', gpms_dict.get('requestId'))], limit=1)

        if trans_rec:
            _logger.info("--------The payment transaction record for the local trnx id already exists----------")
            return trans_rec

        country = request.env['res.country'].sudo().search([('id', '=', int(bill_add_dict.get('bill_country')))])

        # state = False
        # if country.state_ids:
        #   state = request.env['res.country.state'].sudo().search([('id','=',bill_add_dict.get('bill_state'))])

        schedule = None
        batch = None

        try:
            schedule = existing_rec.sm_scheduleId.start_date.strftime(
                '%d %B %Y') + ' to ' + existing_rec.sm_scheduleId.end_date.strftime('%d %B %Y')
            batch = existing_rec.sm_batchId.display_text
        except:
            pass

        values = {
            'first_name': gpms_dict.get('firstName'),
            'last_name': gpms_dict.get("lastName"),
            'programname': bill_add_dict.get('program_name'),
            'transactiontype': 'Request to Gateway',
            # identifier for picking the record
            'orderid': gpms_dict.get('requestId'),
            'billing_name': f"{gpms_dict.get('firstName')} {gpms_dict.get('lastName')}",
            'billing_address': bill_add_dict.get('bill_address'),
            'billing_city': bill_add_dict.get('bill_district'),
            "billing_zip": bill_add_dict.get('pincode'),
            'billing_state': bill_add_dict.get('bill_state'),
            "billing_country": country.code,
            'billing_tel1': gpms_dict.get("mobilePhone"),
            'billing_email': gpms_dict.get("email"),
            "amount": gpms_dict.get("amount"),
            "currency": gpms_dict.get("currency"),
            "batch_details": batch,
            "schedule_details": schedule,
            "re_registration":'ereceipt.reregistration=1',
            "old_local_trans_id": existing_rec.transfer_rec and existing_rec.transfer_rec.local_trans_id or False
        }

        return trns_var.sudo().create(values)
