import datetime
import json
import logging
from odoo.addons.muk_rest import tools
import odoo
from odoo.http import request, Response
from .main import _get_smas_pgm_ids
_logger = logging.getLogger(__name__)
null = None


def converter(o):
    if isinstance(o, datetime.datetime):
        return o.__str__()

def get_session_count(program_name):
    if program_name == 'Nirvana Shatakam':
        return 6
    if program_name == 'Nirbhay Nirgun':
        return 4
    if program_name == 'Niraive Shakti':
        return 5
    if program_name == 'Kalaripayattu':
        return 12

class ContactApiController(odoo.http.Controller):

    def construct_api_response(self, params):
        res = {"jsonrpc": "2.0", "id": null, "result": params}
        _logger.info(str(res))
        return Response(json.dumps(res, default=converter), content_type='application/json;charset=utf-8',
                        status=200)

    @odoo.http.route([
        '/api/schedules/samskriti'
    ], auth="none", type='http', methods=['GET'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def search_contacts(self, **kw):
        _logger.info("Samaskriti Schedules [GET]")

        schedules = request.env['program.schedule.master'].sudo().search(
            [
                ('end_date', '>=', datetime.datetime.now().date()),
                ('program_type_id','in', _get_smas_pgm_ids())
             ]
        )

        res = []
        for schedule in schedules:
            res.append({
                'program_type': schedule.program_type_id.program_type,
                'schedule_id': schedule.id,
                'schedule_name': schedule.name,
                'start_date': schedule.start_date.strftime('%Y-%m-%d') if schedule.start_date else None,
                'end_date': schedule.end_date.strftime('%Y-%m-%d') if schedule.end_date else None,
                'batches': [
                    {
                        'batch_name': batch.display_text,
                        'batch_id': batch.id,
                    } for batch in schedule.no_of_batch_ids
                ],
                'session_count': get_session_count(schedule.program_type_id.program_type)
            })

        return self.construct_api_response(res)
