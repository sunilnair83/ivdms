# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

import logging
_logger = logging.getLogger(__name__)


class ReassignTicket(models.TransientModel):
	_name = 'reassign.ticket'
	_description = 'Description'

	assign = fields.Selection([
		# ('team','Team Leader'),
		# ('user','User'),
		('support','Helpdesk Team')
		],string="Assign",required=True, default='support')
	
	team_id = fields.Many2one('res.users',string="Team Leader")
	user_id = fields.Many2one('res.users',string="Responsible")
	support_id = fields.Many2one('support.team',string="Helpdesk Team")  
	email_to = fields.Char(string='Email to',)
	ticket_id = fields.Many2one('support.ticket')
	user_email_id = fields.Many2one('res.users',string="Responsible",default=lambda self: self.env.user)
	
	@api.onchange('assign','support_id','team_id','user_id')
	def _onchange_email_to(self):
		for reassign in self:
			if reassign.assign == 'support':
				reassign.email_to = reassign.support_id.team_leader.login
			elif reassign.assign == 'team':
				reassign.email_to = reassign.team_id.login
			elif reassign.assign == 'user':
				reassign.email_to = reassign.user_id.login

	def assign_ticket(self):
		for reassign in self:
			ticket = self.env['support.ticket'].browse(self._context.get('active_id'))
			stage_obj = self.env['support.stage'].search([('name','=','Work In Progress')])
			support_team = self.env['support.team'].search([('name','=',ticket.support_team_id.name)])
			if reassign.assign == 'support':
				ticket.write({
					'support_team_id' : reassign.support_id.id,
					'stage_id' : stage_obj.id,
					})
				reassign.ticket_id = ticket.id
				template_id = self.env.ref('bi_website_re_assign_ticket.email_template_edi_ticket_reassign')
				send = template_id.send_mail(self.id, force_send=True)
			elif reassign.assign == 'user':
				support_team.write({
						'user_id':reassign.user_id.id
					})
				ticket.write({
					'user_id' : reassign.user_id.id,
					'stage_id' : stage_obj.id,
					})
				reassign.ticket_id = ticket.id
				template_id = self.env.ref('bi_website_re_assign_ticket.email_template_edi_ticket_reassign')
				send = template_id.send_mail(self.id, force_send=True)
			elif reassign.assign == 'team':
				support_team.write({
						'team_leader':reassign.team_id.id
					})
				ticket.write({
					'team_leader_id' : reassign.team_id.id,
					'stage_id' : stage_obj.id,
					})
				reassign.ticket_id = ticket.id
				template_id = self.env.ref('bi_website_re_assign_ticket.email_template_edi_ticket_reassign')
				send = template_id.send_mail(self.id, force_send=True)
			else:
				raise(_("You can not assign ticket..........."))