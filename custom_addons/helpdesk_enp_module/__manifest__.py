# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Helpdesk Features',
    'category': 'Social',
    'summary': 'Helpdesk Features',
    'version': '1.0',
    'description': """Helpdesk Features""",
    'depends': [
        # 'crm',
        'bi_website_support_ticket',
        'bi_helpdesk_service_level_agreement',
        'bi_helpdesk_support_ticket_dashbord',
        'bi_helpdesk_ticket_knowledge_base',
        'bi_support_helpdesk_ticket_merge',
        'bi_support_ticket_by_team',
        'bi_ticket_auto_response',
        'bi_website_lock_ticket',
        'bi_website_re_assign_ticket',
        'mail_forward'
        # 'bi_website_ticket_problem_solution'
        ],
    'data': [
        # 'wizard/ticket_from_crm_view.xml',
        # 'views/helpdesk_ticket_view.xml',
    ],
    'qweb': [],
    'auto_install': True,
}
