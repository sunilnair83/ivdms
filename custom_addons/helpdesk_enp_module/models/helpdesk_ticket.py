# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import dateutil.parser
import requests
import urllib.parse

from odoo import api, models, fields
from werkzeug.urls import url_join


class HelpdeskSupport(models.Model):
    _inherit = 'support.ticket'
