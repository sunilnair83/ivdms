# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import base64
import json

import requests
import werkzeug
import urllib.parse
from werkzeug.urls import url_encode, url_join

from odoo import http
from odoo.http import request
