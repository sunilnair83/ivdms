# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

{
    "name" : "Website Helpdesk Support Ticket Bundle",
    "version" : "13.0.0.0",
    "category" : "Website",
    'summary': 'Customer Helpdesk Support Ticket website support ticket Website Help Desk Support Ticket Online ticketing system for customer support service desk customer supporting Support Ticketing website all in one support ticket all in one bundle of support ticket',
    "description": """ 
    Bundle of Website Helpdesk Support Ticket
	helpdesk bundle 
	helpdesk support bundle
	all in helpdesk support
	helpdesk ticket bundle 
    all helpdesk apps

	
	
	""",
    "author": "BrowseInfo",
    "website" : "https://www.browseinfo.in",
    "price": 10,
    "currency": 'EUR',
    "depends" : ['bi_subtask',
                'bi_website_support_ticket',
                'bi_material_purchase_requisitions',
                'bi_contract_recurring_invoice_analytic',
                'bi_odoo_job_costing_management',
                'bi_support_helpdesk_ticket_merge',
                'bi_support_ticket_by_team',
                'bi_helpdesk_ticket_knowledge_base',
                'bi_ticket_auto_response',
                'bi_website_re_assign_ticket',
                'bi_website_lock_ticket', 
                'bi_website_ticket_problem_solution',
                'bi_helpdesk_service_level_agreement',
                'bi_website_job_workorder',
                'bi_helpdesk_domain_restriction',
                'bi_job_costing_dashboard',
                'bi_job_order_link_cost_sheet',
                'bi_asset_mro_maintenance_management',
                'bi_helpdesk_support_ticket_dashbord',
                'bi_customer_sla_contract_helpdesk',
                'bi_helpdesk_support_ticket_maintenance',],
    "data": [
       ],
    'qweb': [
    ],
    "auto_install": False,
    "installable": True,
    "live_test_url":'https://youtu.be/X3wIgoqp-dY',
    "images":["static/description/Banner.png"],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
