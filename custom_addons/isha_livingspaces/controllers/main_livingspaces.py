import base64
import json
from datetime import date, timedelta, datetime
from odoo import http
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.http import request
from odoo.tools import ustr
import logging
import traceback
import hashlib
import hmac
import requests
import werkzeug
import werkzeug.utils
import werkzeug.wrappers
_logger = logging.getLogger(__name__)


# Api to get full profile data
def sso_get_full_data(sso_id):
    # request_url = "https://uat-profile.isha.in/services/pms/api/full-profiles/" + sso_id
    request_url = request.env['ir.config_parameter'].sudo().get_param(
        'satsang.SSO_PMS_ENDPOINT1') + "full-profiles/" + sso_id
    authreq = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SYSTEM_TOKEN1')
    headers = {
        "Authorization": "Bearer " + authreq,
        'X-legal-entity': 'IF',
        'x-user': sso_id
    }
    profile = requests.get(request_url, headers=headers)
    return profile.json()


def get_fullprofileresponse_DTO():
    return {
        "profileId": "",
        "basicProfile": {
            "createdBy": "",
            "createdDate": "",
            "lastModifiedBy": "",
            "lastModifiedDate": "",
            "id": 0,
            "profileId": "",
            "email": "",
            "firstName": "",
            "lastName": "",
            "phone": {
                "countryCode": "",
                "number": ""
            },
            "gender": "",
            "dob": "",
            "countryOfResidence": ""
        },
        "extendedProfile": {
            "createdBy": "",
            "createdDate": "",
            "lastModifiedBy": "",
            "lastModifiedDate": "",
            "id": 0,
            "nationality": "",
            "profileId": ""
        },
        "documents": [],
        "attendedPrograms": [],
        "profileSettingsConfig": {
            "createdBy": "",
            "createdDate": "",
            "lastModifiedBy": "",
            "lastModifiedDate": "",
            "id": 0,
            "nameLocked": False,
            "isPhoneVerified": False,
            "phoneVerificationDate": None,
            "profileId": ""
        },
        "addresses": [
            {
                "createdBy": "",
                "createdDate": "",
                "lastModifiedBy": "",
                "lastModifiedDate": "",
                "id": 0,
                "townVillageDistrict": "",
                "city": "",
                "state": "",
                "country": "",
                "pincode": "",
                "profileId": ""
            }
        ]
    }


# Api to get the profile data
def sso_get_user_profile(url, payload):
    dp = {"data": json.dumps(payload)}
    url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
    # request.env['satsang.tempdebuglog'].sudo().create({
    #     'logtext': "sso_get_user_profile config " + url
    # })
    return requests.post(url, data=dp)


def hash_hmac(data, secret):
    data_enc = json.dumps(data['session_id'], sort_keys=True, separators=(',', ':'))
    signature = hmac.new(bytes(secret, 'utf-8'), bytes(data_enc, 'utf-8'), digestmod=hashlib.sha256).hexdigest()
    return signature


def get_is_meditator_or_ieo(name=None, phone=None, email=None, check_meditator=False, check_ieo=False,
                            check_shoonya=False, check_samyama=False):
    try:
        if check_meditator:
            check_meditator = eval(check_meditator)
        if check_ieo:
            check_ieo = eval(check_ieo)
        if check_shoonya:
            check_shoonya = eval(check_shoonya)
        if check_samyama:
            check_samyama = eval(check_samyama)
    except:
        pass
    high_match = request.env['res.partner'].sudo().getHighMatch(name, phone, email)
    ishangam_id = None
    is_meditator = False
    shoonya_completed = False
    samyama_completed = False
    ieo_completed = False
    rec_id = None

    if check_shoonya:
        _logger.info('check Shoonya ' + str(high_match))
        for rec in high_match:
            if rec.shoonya_date:
                shoonya_completed = True
                rec_id = rec.id
                break
        return {"ishangam_id": rec_id, "shoonya_completed": shoonya_completed}

    if check_samyama:
        _logger.info('check Samyama ' + str(high_match))
        for rec in high_match:
            if rec.samyama_date:
                samyama_completed = True
                rec_id = rec.id
                break
        return {"ishangam_id": rec_id, "samyama_completed": samyama_completed}

    if check_meditator:
        _logger.info('check meditator ' + str(high_match))
        for rec in high_match:
            if rec.is_meditator:
                is_meditator = True
                ieo_completed = 'NA'
                rec_id = rec.id
                break
    if check_ieo and not is_meditator:
        _logger.info('check ieo ' + str(high_match))
        for rec in high_match:
            if rec.ieo_date:
                ieo_completed = True
                is_meditator = rec.is_meditator
                rec_id = rec.id
                break
    if len(high_match) > 0 and not rec_id:
        recent_contact = high_match[0]
        for rec in high_match:
            if rec.write_date > recent_contact.write_date:
                recent_contact = rec
        rec_id = recent_contact.id

    res = {
        'ishangam_id': rec_id,
        'is_meditator': is_meditator,
        'ieo_completed': ieo_completed,
        'matched_id': rec_id,
        'matched_rec': request.env['res.partner'].sudo().search([('id', '=', rec_id)])
    }
    return res


def get_duplicaterecord(name=None, email=None):
    retval = False
    try:
        recs = request.env['livingspaces.registration'].sudo().search(
            ['&', ('first_name', '=', name),
             ('participant_email', '=', email)])

        if (len(recs) > 0):
            retval = True
        else:
            retval = False

    except Exception as ex:
        tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        _logger.error(tb_ex)
        retval = False
    finally:
        return retval


def sendemailandsms(transprogramlead_rec):
    try:
        request.env['livingspaces.notification'].sudo().sendRegistrationEmailSMSCore(transprogramlead_rec)
    except Exception as ex:
        tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))


class livingspacesRegistration(http.Controller):

    @http.route(['/generate_otp'], type='http', auth="public", website=True)
    def generate_otp(self, **post):
        import random
        otp = ""
        print(post)
        if post.get("mobile"):
            # smstemplate = request.env['sms.template'].sudo().browse(templateid)
            # subject = request.env['sms.template'].sudo()._render_template(smstemplate.body,
            #                                                     'tp.program.registration', rec.id)
            # print(subject)
            otp = random.randrange(100000, 999999)
            phone_number = post.get('ccode')+post.get('mobile')
            print(phone_number)
            print("otp")
            print(otp)
            request.env['kshetragna.registration'].sudo()._send_smcountry_sms({
                'number': phone_number,
                'message': "OTP Received"+str(otp)
            })
        dd ={"otp": otp, "status": "success"}
        print(otp)
        return json.dumps(dd)

    def checkExistingTxn_livingspace(self, requestidvar, ssoprofileid, **post):
        _logger.info("starting exection checkExistingTxn requestidvar" + str(requestidvar))
        _logger.info("ssoprofileid" + str(ssoprofileid))
        _logger.info("**post" + str(post))
        ssorecord_exist = request.env['kshetragna.registration'].sudo().search(
            [("sso_id", "=", ssoprofileid), ("payment_status", "=", 'Paid')])
        record_exist = request.env['kshetragna.registration'].sudo().search([("order_id", "=", requestidvar)],order ="id desc",limit=1)
        valid =True
        err_msg = ""
        status = ""
        if len(ssorecord_exist) > 0:
            err_msg = "ERR_COMPLETED"
        if len(record_exist) > 0:
            err_msg = "ERR_VALIDATION"
            status="ERROR"
            # if record_exist.payment_status == "Payment Link Sent":
            #     valid= False
            if record_exist.payment_status == "Paid" and record_exist.payment_status == "Pending":
                err_msg = "ERR_Payment_Pending_approved_failed"
                valid= False
            if record_exist.payment_status == "Paid" and record_exist.payment_status == "Approved":
                err_msg = "ERR_Payment_Pending_approved_failed"
                valid= False
            if record_exist.payment_status == "Paid":
                err_msg = "ERR_COMPLETED"
                valid= False
            if record_exist.payment_status == "payment_inprogress":
                err_msg = "ERR_AWAITED"
                valid = False
            if record_exist.payment_status == "payment_pending":
                err_msg = "ERR_AWAITED"
                valid = False
            if record_exist.payment_status == "payment_initiated":
                valid = True
            if record_exist.payment_status == "payment_failed" or record_exist.registration_status in ['dd_failed']:
                err_msg = "ERR_Payment_Pending_approved_failed"
                valid= False
            if record_exist.registration_status in ['awating_neft_approval',"Waiting for approve",
                                                    'awating_dd_approval', 'neft_requested', 'neft_success',
                                                    'dd_requested', 'dd_success']:
                err_msg = "ERR_GENERIC_ERROR"
                valid = False
            if record_exist.registration_status == 'Payment Link Sent' and record_exist.payment_status == 'Approved':
                valid = True

            return {"valid":valid,"errmsg":err_msg,"status":status}

        else:
            return {"valid":True,"errmsg":err_msg,"status":status}

    def redirect_to_paymentpage(self,record_id,sso_id,unq_id):
        """
        :param record_id: rec id
        :return: redirect to payment page
        """
        _logger.info("redirect to Payment page method")
        if record_id:
            created_rec = record_id
            if '_' in created_rec.mobile:
                _logger.info("Mobile is having '_'")
                values = {"msg": 'invalid_registration'}
                return request.render('isha_livingspaces.kshetragna_user_message', values)
            mail_template_id = request.env.ref('isha_livingspaces.mail_template_kshetragna_registration')
            try:
                callbackUrl = request.env['ir.config_parameter'].sudo().get_param('kshetragna.gpms_call_back_url')
                gpms_payurl = request.env['ir.config_parameter'].sudo().get_param('kshetragna.gpms_payment_url')
                amount = request.env['ir.config_parameter'].sudo().get_param('kshetragna.reg_amount')
                currency_value = request.env['ir.config_parameter'].sudo().get_param('kshetragna.payment_currency')
                # unq_id = post.get('sso_id') +"-"+ str(created_rec.id)
                created_rec.write({"payment_status": "payment_initiated", "registration_status": "transfer_to_payment_gateway","payment_mode":"online"})

                values = {
                    "firstName": created_rec.first_name,
                    "lastName": created_rec.last_name,
                    "email": created_rec.email,
                    "mobilePhone": str(created_rec.mobile) if created_rec.mobile else '',
                    "addressLine1": (created_rec.receipt_addressline)[:50] if created_rec.receipt_addressline else '',  # GPMS accepts max 50 chars
                    "addressLine2": "",  # Not a mandatory field
                    "city": created_rec.receipt_city,
                    "state": created_rec.receipt_state,
                    "postCode": created_rec.receipt_pincode,
                    "country": created_rec.receipt_country.code if created_rec.receipt_country else "IN",
                    "amount": amount,
                    "currency": currency_value,
                    "callbackUrl": callbackUrl,
                    "requestId": str(unq_id),
                    "ssoId":sso_id,
                    "gpms_payurl": gpms_payurl,
                    "program_type": request.env['ir.config_parameter'].sudo().get_param('kshetragna.programtype'),
                    "program_code": request.env['ir.config_parameter'].sudo().get_param('kshetragna.programcode'),
                }
                _logger.info(str(values))
                return request.render("isha_livingspaces.kshetragna_website_payment", values)
            except Exception as e:
                values = {"status": 'Error', 'message': "There is error in sending mail",
                          'email': ""}
                tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                _logger.info(tb_ex)
                return request.render('isha_livingspaces.exception', values)

    def check_payment_link(self, rec):
        is_valid = True
        if rec:
            reg_record = request.env['kshetragna.registration'].sudo().search(
                [("id", "=", rec)], limit=1)
            if reg_record:
                config_rec = request.env['kshetragna.configuration'].sudo().search([],limit=1)
            today = date.today()
            if config_rec.invalidate_all_payment_links_on<=today:
                return False
            if config_rec.payment_link_validity and reg_record.payment_link_sent_date:
                Begindatestring = str(reg_record.payment_link_sent_date)
                Begindate = datetime.strptime(Begindatestring, "%Y-%m-%d")
                expire_date = Begindate + timedelta(days=config_rec.payment_link_validity)
                if today>expire_date.date():
                   is_valid = False
        return is_valid

    @http.route('/kshetragna/message', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def  onlinekshetragna_registration_status(self,**post):
        """
        :param post: pass first name in templates
        :return:
        """
        if post.get('isha_residential_space')=="isha_residential_space":
            isha_residential_space = True
        else:
            isha_residential_space = False

        values ={
            "person_name": post.get("info"),
            "login_email":post.get("login_email"),
            "isha_residential_space":isha_residential_space
        }

        return request.render('isha_livingspaces.online_kshetragna_status', values)

    @http.route('/kshetragna/update_address', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def update_address_onregistration(self, **post):
        """
        :param post: pass the registration id
        :return:
        """
        # if post.get("id") or post.get("registration_id"):
        if request.httprequest.method == "GET":
            if post.get("id") != 'False':
                countries = request.env['res.country'].sudo().search([('code', '!=', '')], order="name asc")
                existing_rec = request.env['kshetragna.registration'].sudo().search([('id', '=', post.get("id"))],
                                                                                    order="id asc")
                state_id = request.env['res.country.state'].sudo().search([('name', '=', existing_rec.state)])
                if existing_rec.country.id ==104 and  existing_rec.nationality_id.id==104:
                    is_india = "True"
                else:
                    is_india = "False"

                values = {'countries': countries,
                          'states': request.env['res.country.state'].sudo().search(
                              [("country_id", "in", (countries.ids))]),
                          'registration_id': post.get("id"),
                          'login_email': post.get("login_email"),
                          'addressline': existing_rec.addressline if existing_rec else '',
                          'city': existing_rec.city if existing_rec else '',
                          'state': state_id.id if existing_rec and state_id else '',
                          'pincode': existing_rec.pincode if existing_rec else '',
                          'country': existing_rec.country.id if existing_rec else '',
                          'is_india':is_india,
                          }
                return request.render('isha_livingspaces.Kshetragna_receipt_address_page', values)
            else:
                values = {"msg": 'invalid_registration', 'login_email': post.get("login_email")}
                return request.render('isha_livingspaces.kshetragna_user_message', values)

        elif request.httprequest.method == "POST":
            rec = request.env['kshetragna.registration'].sudo().search(
                [("id", "=", post.get("registration_id"))], limit=1)
            if rec:
                if post.get('payment_mode_text') == 'Online':
                    payment_mode = 'online'
                elif post.get('payment_mode_text') == 'NEFT / RTGS':
                    payment_mode = 'neft'
                elif post.get('payment_mode_text') == 'DD / Cheque':
                    payment_mode = 'dd'
                else:
                    payment_mode = 'online'

                if post.get("user_action") != 'is_skip':
                    state_vlue = False
                    if post.get("receipt_state") != "" and post.get("receipt_state") != None:
                        state_rec = request.env['res.country.state'].sudo().search(
                            [("id", "=", int(post.get("receipt_state")))])
                        state_vlue = state_rec.name
                    # rec.write({"payment_status": "payment_initiated"})
                    country_id = False
                    if post.get("receipt_country"):
                        country_id = int(post.get("receipt_country"))
                    else:
                        country_id = rec.country.id

                    update_values = {
                        "receipt_addressline": post.get("receipt_addressline") if post.get("receipt_addressline") else rec.addressline,
                        "receipt_city": post.get("receipt_city") if post.get("receipt_city") else rec.city,
                        "receipt_state": state_vlue if state_vlue else rec.state,
                        "registration_status": "waiting_for_approve",
                        "receipt_country": country_id,
                        "receipt_pincode": post.get("receipt_pincode") if post.get("receipt_pincode") else rec.pincode,
                        "account_number": post.get("account_number"),
                        "account_holder_name": post.get("account_holder_name"),
                        "bank_name": post.get("bank_name"),
                        "bank_branch_name": post.get("bank_branch_name"),
                        "account_type": post.get("account_type"),
                        "ifsc_code": post.get("ifsc_code"),
                        "payment_mode":payment_mode
                    }
                else:
                    update_values = {
                        "account_number": post.get("account_number"),
                        "account_holder_name": post.get("account_holder_name"),
                        "bank_name": post.get("bank_name"),
                        "bank_branch_name": post.get("bank_branch_name"),
                        "account_type": post.get("account_type"),
                        "ifsc_code": post.get("ifsc_code"),
                        "registration_status": "waiting_for_approve",
                        "payment_status":"Pending",
                        "payment_mode": payment_mode
                    }

                rec.write(update_values)

                # For india ppl allow to them to redirect to payment view
                if rec.country.id == 104 and rec.nationality_id.id == 104:
                    if payment_mode == 'online':
                        return self.redirect_to_paymentpage(rec, rec.sso_id, rec.order_id)
                    elif payment_mode == 'neft':
                        rec.write({"registration_status": "awating_neft_approval", "payment_status": "Pending"})
                        values = {"person_name": rec.first_name, "login_email": rec.email}
                        tmpurl = request.httprequest.url.replace('update_address', 'message')
                        return werkzeug.utils.redirect(tmpurl)
                    elif payment_mode == 'dd':
                        rec.write({"registration_status": "awating_dd_approval", "payment_status": "Pending"})
                        values = {"person_name": rec.first_name, "login_email": rec.email}
                        tmpurl = request.httprequest.url.replace('update_address', 'message')
                        return werkzeug.utils.redirect(tmpurl)
                else:
                    values = {"person_name": rec.first_name, "login_email": rec.email}
                    tmpurl = request.httprequest.url.replace('update_address', 'message')
                    return werkzeug.utils.redirect(tmpurl)
            else:
                values = {"msg": 'invalid_registration', 'login_email': post.get("login_email")}
                return request.render('isha_livingspaces.kshetragna_user_message', values)



    @http.route('/kshetragna/registrationform/paymentpage', type='http', auth="public", methods=["GET", "POST"],
                website=True,
                csrf=False)
    def onlinekshetragna_registration_payment_status(self, **post):
        if request.httprequest.method == "GET":
            _logger.info("Loofer into Paymetn view")
            is_link_vaild = self.check_payment_link(post.get("registration_id"))
            _logger.info("Loading view  into Paymetn view")
            _logger.info(post.get("registration_id"))
            if not is_link_vaild:
                _logger.info("link not expired")
                values = {"msg": 'link_expried'}
                return request.render('isha_livingspaces.kshetragna_user_message', values)
            # values = {"reg_id": post.get("registration_id")}
            # return request.render('isha_livingspaces.Kshetragna_payment_page', values)
            validation_errors = []
            try:
                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                cururls = str(request.httprequest.url).replace("http://", "https://", 1)
                sso_log = {'request_url': str(cururls),
                           "callback_url": str(cururls),
                           "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                           "hash_value": "",
                           "action": "0",
                           "legal_entity": "IF",
                           "force_consent": "1"
                           }
                ret_list = {'request_url': str(cururls),
                            "callback_url": str(cururls),
                            "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                            }
                data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
                data_enc = data_enc.replace("/", "\\/")
                signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                                     digestmod=hashlib.sha256).hexdigest()
                sso_log["hash_value"] = signature
                sso_log["sso_logurl"] = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_URL1')
                values = {
                    'sso_log': sso_log
                }
                return request.render('isha_livingspaces.livingspacessso', values)
            except Exception as ex:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)
                # values = {'language': language}
        # values = {"reg_id": post.get("registration_id")}

        if request.httprequest.method == "POST" and "session_id" in request.httprequest.form:
            _logger.info("Loading view  into Paymetn view")

            ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
            api_key = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.SSO_API_KEY1')  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
            session_id = request.httprequest.form['session_id']
            hash_val = hash_hmac({'session_id': session_id}, ret)
            data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
            request_url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
            sso_user_profile = eval(sso_get_user_profile(request_url, data).text)
            auto_mail =sso_user_profile["autologin_email"]
            profile_id =sso_user_profile["autologin_profile_id"]
            rec = request.env['kshetragna.registration'].sudo().search(
                [("id", "=", post.get("registration_id")),
                  ("email","=",auto_mail),
                  ("sso_id","=",profile_id)], limit=1)
            if rec:
                responsecheckexisting = self.checkExistingTxn_livingspace(rec.order_id, profile_id, **post)
                if responsecheckexisting.get("valid") == False:
                    values = {"support_email": 'info.kshetragna@ishafoundation.org',
                              "status": responsecheckexisting.get("status"),
                              "program_name": "KTG",
                              "login_email":auto_mail,
                              "transactionId": "",
                              "region_support_email_id": "",
                              "err_msg": responsecheckexisting.get("errmsg"), }
                    return request.render("isha_livingspaces.payumoney_status_template", values)
                amount = request.env['ir.config_parameter'].sudo().get_param('kshetragna.reg_amount')
                currency_value = request.env['ir.config_parameter'].sudo().get_param('kshetragna.payment_currency')
                values = {"reg_id": post.get("registration_id"), "amount": float(amount), "currency": currency_value,"login_email":auto_mail}
                return request.render('isha_livingspaces.Kshetragna_payment_page', values)
            else:
                _logger.info("Record mismatch, login email and registration email is different")
                # return request.render('isha_livingspaces.kshetragna_error_template')
                values = {"msg": 'mismatch','login_email':auto_mail}
                return request.render('isha_livingspaces.kshetragna_user_message', values)

        elif request.httprequest.method == "POST":
            _logger.info("Loading view  iPOSt for payment")
            rec = request.env['kshetragna.registration'].sudo().search(
                [("id", "=", post.get("registration_id"))], limit=1)
            return self.redirect_to_paymentpage(rec, rec.sso_id, rec.order_id)

    @http.route('/kshetragna/sync_indiadata',type='http', auth="public", methods=["GET","POST"], website=True,csrf=False)
    def sync_india_data_server_one(self, **post):
        _logger.info('**** India Data sync logic - started *****')
        regist_rec = request.env['kshetragna.registration']
        _logger.info("Post call -" + str(post))
        _logger.info("Registration_status " + str(post.get("registration_status")))

        rec_values = {
            "registration_status": post.get("registration_status"),
            "payment_status": post.get("payment_status"),
            "payment_mode": post.get("payment_mode"),
            "first_name": post.get("first_name"),
            "last_name": post.get("last_name"),
            # "dob": post.get("dob"),
            "gender": post.get("gender"),
            "marital_status": post.get("marital_status"),
            "countrycode": post.get("countrycode"),
            "mobile": post.get("mobile"),
            "email": post.get("email"),
            "sso_id": post.get('sso_id'),
            "country": post.get('country'),
            "nationality_id": post.get("nationality_id"),
            "addressline": post.get("addressline"),
            "city": post.get("city"),
            "state": post.get("state"),
            "pincode": post.get("pincode"),
            "receipt_addressline": post.get("addressline"),
            "receipt_city": post.get("city"),
            "receipt_state": post.get("receipt_state"),
            "receipt_country": post.get("receipt_country"),
            "receipt_pincode": post.get("receipt_pincode"),
            "accommodation_type": post.get("accommodation_type"),
            "furnish_type": post.get("furnish_type"),
            "passport_number": post.get("passport_number"),
            "passport_image": post.get("passport_image"),
            "is_oci_card_holder": post.get("is_oci_card_holder"),
            "is_oci_card": post.get("is_oci_card"),
            "pan_no": post.get("pan_no"),
            "pan_card_file": post.get("pan_card_file"),
            "aadhar_card": post.get("aadhar_card"),
            "aadhar_card_file": post.get("aadhar_card_file"),
            "account_number": post.get("account_number"),
            "account_holder_name": post.get("account_holder_name"),
            "bank_name": post.get("bank_name"),
            "bank_branch_name": post.get("bank_branch_name"),
            "account_type": post.get("account_type"),
            "ifsc_code": post.get("ifsc_code"),
        }
        res = regist_rec.sudo().create(rec_values)
        res.write({'is_india_Data_check': True})
        _logger.info('India Record Created to Server One'+str(res))
        return "True"

    @http.route('/kshetragna/registrationform', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def onlinekshetragna_registration(self, **post):
        _logger.info("------------------In the 1 first step reg form----------------")

        fullprofileresponsedatVar = request.env['livingspaces.ssoapi'].sudo().get_fullprofileresponse_DTO()

        consentgrantStatusflagvar = True
        if request.httprequest.method == "GET":
            _logger.info("------------------In the Get first step reg form----------------")
            # Logout Redirection template
            # if post.get("is_logout"):
            #     post.pop('is_logout')
            #     return request.render('isha_livingspaces.kshetragna_sso_auto_redirect')

            validation_errors = []
            try:
                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                cururls = str(request.httprequest.url).replace("http://", "https://", 1)
                sso_log = {'request_url': str(cururls),
                           "callback_url": str(cururls),
                           "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                           "hash_value": "",
                           "action": "0",
                           "legal_entity": "IF",
                           "force_consent": "1"
                           }
                ret_list = {'request_url': str(cururls),
                            "callback_url": str(cururls),
                            "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                            }
                data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
                data_enc = data_enc.replace("/", "\\/")
                signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                                     digestmod=hashlib.sha256).hexdigest()
                sso_log["hash_value"] = signature
                sso_log["sso_logurl"] = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_URL1')
                values = {
                    'sso_log': sso_log
                }
                return request.render('isha_livingspaces.livingspacessso', values)
            except Exception as ex:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)
                # values = {'language': language}
                return request.render('isha_livingspaces.exception')
        # if request.httprequest.method == "GET":
        #     return request.render('isha_livingspaces.livingspace_registrationform', values)


        elif request.httprequest.method == "POST" and "session_id" in request.httprequest.form:
            _logger.info("------------------In the 3 first step reg form----------------")
            _logger.info("innser session post")
            _logger.info("inner session post methos ----------- kshetragna registration")

            # if registration_id is exist on post method, allow them to redirect to directly to payment gateway
            if post.get("registration_id"):
                _logger.info("inside registration id on 2 POST method")
                rec = request.env['kshetragna.registration'].sudo().search(
                    [("id", "=", post.get("registration_id"))], limit=1)
                rec.write({"payment_status": "payment_initiated"})
                return self.redirect_to_paymentpage(rec, rec.sso_id, rec.order_id)
            validation_errors = []
            values = {}
            try:
                # qsurl = str(request.httprequest.url).split('?')[1]
                # countries_scl = request.env['res.country'].sudo().search()
                # countries = request.env['res.country'].sudo().search()
                # result_countries =  countries
                result_countries = request.env['res.country'].sudo().search(
                    [('name', '!=', ' ')],order="name asc")

                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                api_key = request.env['ir.config_parameter'].sudo().get_param(
                    'satsang.SSO_API_KEY1')  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                session_id = request.httprequest.form['session_id']
                hash_val = hash_hmac({'session_id': session_id}, ret)
                data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
                request_url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
                sso_user_profile = eval(sso_get_user_profile(request_url, data).text)
                amounttobepaid = request.env['ir.config_parameter'].sudo().get_param('kshetragna.reg_amount')
                currencytobepaid = request.env['ir.config_parameter'].sudo().get_param('kshetragna.payment_currency')

                fullprofileresponsedatVar = get_fullprofileresponse_DTO()
                # fullprofileresponsedatVar = {
                #     "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #     "basicProfile": {
                #         "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "createdDate": "2020-05-05T10:54:05Z",
                #         "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "lastModifiedDate": "2020-06-02T11:37:57Z",
                #         "id": 43083,
                #         "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "email": "krishna.t@ishafoundation.org",
                #         "firstName": "K",
                #         "lastName": "T",
                #         "profilePic": "https://lh3.googleusercontent.com/-I2gNoP1r5kg/AAAAAAAAAAI/AAAAAAAAAAA/AAKWJJOfNXvcjzTF3XhKfjkoCRnjfDrP3Q/photo.jpg?size=250",
                #         "phone": {
                #             "countryCode": "91",
                #             "number": "7867765697"
                #         },
                #         "gender": "MALE",
                #         "dob": "1950-06-01",
                #         "countryOfResidence": "IN"
                #     },
                #     "extendedProfile": {
                #         "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x2",
                #         "createdDate": "2020-05-05T10:54:05Z",
                #         "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "lastModifiedDate": "2020-06-02T11:37:57Z",
                #         "id": 43081,
                #         "passportNum": "sfswe",
                #         "nationality": "IN",
                #         "pan": "11",
                #         "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3"
                #     },
                #     "documents": [],
                #     "attendedPrograms": [],
                #     "profileSettingsConfig": {
                #         "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "createdDate": "2020-05-05T10:54:05Z",
                #         "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "lastModifiedDate": "2020-05-05T10:54:05Z",
                #         "id": 43086,
                #         "nameLocked": False,
                #         "isPhoneVerified": False,
                #         "phoneVerificationDate": None,
                #         "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3"
                #     },
                #     "addresses": [
                #         {
                #             "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #             "createdDate": "2020-05-22T06:36:08Z",
                #             "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #             "lastModifiedDate": "2020-06-02T11:37:57Z",
                #             "id": 18750,
                #             "addressType": "RESIDENCE",
                #             "addressLine1": "a1",
                #             "addressLine2": "a2",
                #             "townVillageDistrict": "a",
                #             "city": "Secunderabad",
                #             "state": "Telangana",
                #             "country": "IN",
                #             "pincode": "500055",
                #             "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3"
                #         }
                #     ]
                # }
                if "autologin_email" in sso_user_profile:
                    fullprofileresponsedatVar["basicProfile"]["email"] = sso_user_profile["autologin_email"]
                    fullprofileresponsedatVar["profileId"] = sso_user_profile["autologin_profile_id"]
                fullprofileresponsedatVar = sso_get_full_data(sso_user_profile['autologin_profile_id'])
                fetch_prescreened_participant =""
                if fullprofileresponsedatVar["basicProfile"]["email"]:
                    # prescreened_participant=false
                    fetch_prescreened_participant= request.env['kshetragna.prescreenedregistrants'].sudo().search([('email', '=',fullprofileresponsedatVar["basicProfile"]["email"].lower())])
                    if len(fetch_prescreened_participant) == 0:
                        return request.render('isha_livingspaces.invalidrequest')

                fullprofileresponsedatVar['basicProfile']['phone']['number'] = fetch_prescreened_participant.mobile
                profresponse = json.dumps(fullprofileresponsedatVar)
                uniq_request_id = request.env['ir.config_parameter'].sudo().get_param('kshetragna.uniq_request_id')
                if uniq_request_id:
                    unq_id = fullprofileresponsedatVar["profileId"] + "-" + str(uniq_request_id)
                else:
                    unq_id = fullprofileresponsedatVar["profileId"] + "-" + str("KTG")
                    # print(unq_id)
                check_nri_rec = request.env['kshetragna.registration'].sudo().search([("order_id", "=", unq_id)],
                                                                                     order="id desc", limit=1)
                _logger.info("***** Kshetragna Registration - Check NRI record ***** " + str(check_nri_rec))
                list_Status = ['transfer_to_payment_gateway', 'neft_requested', 'dd_requested', 'awating_neft_approval',
                               'awating_dd_approval', 'Payment Link Sent', 'Pending', 'waiting_for_approve',
                               'document_pending', 'document_corrected']
                if check_nri_rec:
                    if check_nri_rec.registration_status in list_Status and check_nri_rec.payment_status == 'Paid':
                        values = {"msg": 'nri_message', 'person_name': check_nri_rec.first_name,
                                  'login_email': fullprofileresponsedatVar["basicProfile"]["email"]
                                  }
                        return request.render('isha_livingspaces.kshetragna_user_message', values)
                    elif check_nri_rec.registration_status in list_Status and check_nri_rec.payment_status != 'Paid':
                        values = {"msg": 'nri_message1', 'person_name': check_nri_rec.first_name,
                                  'login_email': fullprofileresponsedatVar["basicProfile"]["email"]
                                  }
                        return request.render('isha_livingspaces.kshetragna_user_message', values)
                    elif check_nri_rec.registration_status in ('neft_success', 'dd_success',
                                                               'transfer_to_payment_gateway') and check_nri_rec.payment_status == 'Paid':
                        values = {"msg": 'nri_message2', 'person_name': check_nri_rec.first_name,
                                  'login_email': fullprofileresponsedatVar["basicProfile"]["email"]
                                  }
                        return request.render('isha_livingspaces.kshetragna_user_message', values)
                    
                responsecheckexisting = self.checkExistingTxn_livingspace(unq_id, post.get('sso_id'), **post)
                _logger.info("-----CHECK %s-----"%(responsecheckexisting))
                if responsecheckexisting.get("valid") == False:
                    values = {"support_email": 'info.kshetragna@ishafoundation.org',
                              "status": responsecheckexisting.get("status"),
                              "program_name": "KTG",
                              "login_email": fullprofileresponsedatVar["basicProfile"]["email"],
                              "transactionId": "",
                              "region_support_email_id":'info.kshetragna@ishafoundation.org',
                              "err_msg": responsecheckexisting.get("errmsg"), }
                    return request.render("isha_livingspaces.payumoney_status_template", values)

                # print(responsecheckexisting)
                # print(responsecheckexisting.get("status"))

                # checking if enquiry record exists to pre-fill data in advance payment form
                record_exist = request.env['kshetragna.registration'].sudo().search(
                    [("order_id", "=", unq_id)], order="id desc", limit=1)

                # if record exists, update the profile dicts in order to pre-fill data
                if record_exist:
                    # extracting the profresponse json to add city and pincode to populate them based on enquiry form
                    profresponse = json.loads(profresponse)

                    # adding the addresses list in sso dict if it's not present so we can auto-populate data
                    # if we don't get the addresses list in sso response
                    if not 'addresses' in profresponse.keys() or ('addresses' in profresponse.keys() and not profresponse['addresses']):
                        profresponse['addresses'] = []
                        profresponse['addresses'].append({'pincode':''})

                    _logger.info("--------------Prof Resp %s"%(profresponse))

                    # pre-populating the general fields of form advance payment form
                    if record_exist.gender: fullprofileresponsedatVar['basicProfile']['gender'] = record_exist.gender.upper()
                    if record_exist.country: fullprofileresponsedatVar['basicProfile']['countryOfResidence'] = record_exist.country.code
                    if record_exist.nationality_id: fullprofileresponsedatVar['extendedProfile']['nationality'] = record_exist.nationality_id.code
                    if record_exist.city and 'addresses' in profresponse.keys() and profresponse['addresses'] and profresponse['addresses'][0]: profresponse['addresses'][0]['townVillageDistrict'] = record_exist.city
                    if record_exist.pincode and 'addresses' in profresponse.keys() and profresponse['addresses'][0]: profresponse['addresses'][0]['pincode'] = record_exist.pincode

                    # adding data to pre-populate state field, since we've both dropdown and text field so in if cond we are passing id
                    # and in else condition we are passing text
                    if record_exist.country and record_exist.country.state_ids and record_exist.state and 'addresses' in profresponse.keys() and profresponse['addresses'][0]:
                        profresponse['addresses'][0]['stateId'] = request.env['res.country.state'].sudo().search([('name', '=', record_exist.state)], limit=1).exists().id
                    if record_exist.country and not record_exist.country.state_ids and record_exist.state and 'addresses' in profresponse.keys() and profresponse['addresses'][0]:
                        profresponse['addresses'][0]['state'] = record_exist.state

                    # turning json into string again
                    profresponse = json.dumps(profresponse)

                values = {}

                record_exist = request.env['kshetragna.registration'].sudo().search(
                    [("order_id", "=", unq_id)], order="id desc", limit=1)
                if record_exist and record_exist.gender:
                    fullprofileresponsedatVar['basicProfile']['gender'] = record_exist.gender.upper()

                countries = request.env['res.country'].sudo().search([('name', '!=', ' ')], order="name asc")

                # if record exists and their status as 'partially_complete', Go to the Update address page.
                # For india ppl allow to them to redirect to payment view
                if record_exist.registration_status == 'partially_complete' and record_exist.payment_status == 'Pending':
                    if record_exist.country.id != 0:
                        add = "update_address?id=" + str(record_exist.id) + "&login_email=" + str(post.get("email"))
                        tmpurl = request.httprequest.url.replace('registrationform', add)
                        return werkzeug.utils.redirect(tmpurl)
                    else:
                        tmpurl = request.httprequest.url.replace('update_address', 'message')
                        return werkzeug.utils.redirect(tmpurl)

                    # oci_card_holder = 'yes' if record_exist.is_oci_card_holder else 'no'
                    # values = {
                    #     'countries': countries,
                    #     'states': request.env['res.country.state'].sudo().search([]),
                    #     'isofname': fullprofileresponsedatVar,
                    #     'inputobj': profresponse,
                    #     'prescreenedrecord': fetch_prescreened_participant,
                    #     "amounttobepaid": amounttobepaid,
                    #     "currencytobepaid": currencytobepaid,
                    #     'marital_status': record_exist.marital_status,
                    #     'accommodation_type': record_exist.accommodation_type,
                    #     'furnish_type': record_exist.furnish_type,
                    #     'passport_number': record_exist.passport_number,
                    #     'oci_card_holder': oci_card_holder,
                    #     'pan_no': record_exist.pan_no,
                    #     'aadhar_card': record_exist.aadhar_card,
                    #     'login_email': fullprofileresponsedatVar["basicProfile"]["email"]
                    # }
                    # return request.render('isha_livingspaces.livingspace_registrationform', values)

                # countrycodep = fullprofileresponsedatVar["basicProfile"]["countryOfResidence"]
                # if countrycodep == "IN":
                #     countries = request.env['res.country'].sudo().search([('code', '=', "IN")])
                values = {
                    'countries': countries,
                    'states': request.env['res.country.state'].sudo().search([]),
                    'isofname': fullprofileresponsedatVar,
                    'inputobj': profresponse,
                    'prescreenedrecord':fetch_prescreened_participant,
                    "amounttobepaid": amounttobepaid,
                    "currencytobepaid": currencytobepaid,
                    'login_email': fullprofileresponsedatVar["basicProfile"]["email"]
                }
                return request.render('isha_livingspaces.livingspace_registrationform', values)
                # else:
                #     return request.render('isha_livingspaces.nonindian_page', values)


            except Exception as ex:
                _logger.error("Error caught during request creation", exc_info=True)
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                request.env['livingspaces.tempdebuglog'].sudo().create({
                    'logtext': tb_ex
                })
        elif request.httprequest.method == "POST":
            _logger.info("------------------In the 4 first step reg form----------------")
            _logger.info("inner post methos ----------- kshetragna registration")

            # if registration_id is exist on post method, allow them to redirect to directly to payment gateway
            if post.get("registration_id"):
                _logger.info("inside registration id on POST method")
                rec = request.env['kshetragna.registration'].sudo().search(
                    [("id", "=", post.get("registration_id"))], limit=1)
                # rec.write({"payment_status": "payment_initiated", "registration_status": "transfer_to_payment_gateway"})
                return self.redirect_to_paymentpage(rec, rec.sso_id, rec.order_id)
            _logger.info("inside post")
            # status = "payment_initiated"
            # allow all of them allow to pay after approvals done
            payment_status = "Pending"
            registration_status = "partially_complete"
            if post.get("pan_card_file"):
                pan_cardattached_filephoto = request.httprequest.files.getlist('pan_card_file')
                read1 = pan_cardattached_filephoto[0].read()
                pan_card_datas1 = base64.b64encode(read1).replace(b'\n', b'')
            #
            if post.get("is_oci_card"):
                is_oci_card_filephoto = request.httprequest.files.getlist('is_oci_card')
                is_oci_card_read = is_oci_card_filephoto[0].read()
                is_oci_carddatas1 = base64.b64encode(is_oci_card_read).replace(b'\n', b'')
            # passport
            if post.get("passport_image_file"):
                passport_image_fileephoto = request.httprequest.files.getlist('passport_image_file')
                passport = passport_image_fileephoto[0].read()
                passportdatas1 = base64.b64encode(passport).replace(b'\n', b'')

            # aadhar card
            if post.get("aadhar_card_file"):
                aadhar_image_fileephoto = request.httprequest.files.getlist('aadhar_card_file')
                aadhar = aadhar_image_fileephoto[0].read()
                aadhardatas1 = base64.b64encode(aadhar).replace(b'\n', b'')
                # status = "Waiting for approve"
            state_vlue = False
            if post.get("state") != "" and post.get("state")!=None:
                state_vlue = request.env['res.country.state'].sudo().search([("id", "=", int(post.get("state")))])
            country_id = post.get("country")
            if country_id:
                country_id = request.env['res.country'].sudo().search([("id", "=", int(post.get("country")))])
            is_oci_card_holder = False
            if post.get("jsociyesno") == "yes":
                is_oci_card_holder = True
                # status = "Waiting for approve"
            if country_id and country_id.id !=104:
                payment_status ="Pending"
            try:
                rec_values = {
                        "first_name": post.get("first_name"),
                        "last_name": post.get("last_name"),
                        "countrycode": post.get("countrycode"),
                        "mobile": post.get("phone2"),
                        "gender": post.get("gender"),
                        "dob": post.get("dob"),
                        "email": post.get("email"),
                        "marital_status": post.get("marital_status"),
                        "nationality_id": post.get("nationality_id"),
                        "city": post.get("city"),
                        "state": state_vlue and state_vlue.name or post.get('state_text_field'),
                        "country": country_id.id if country_id else 104,
                        "pincode": post.get("pincode"),
                        "receipt_addressline":post.get("addressline"),
                        "receipt_city":post.get("city"),
                        "receipt_state":state_vlue and state_vlue.name or post.get('state_text_field'),
                        "receipt_country":country_id.id if country_id else 104,
                        "receipt_pincode":post.get("pincode"),
                        # "approximate_date": post.get("approximate_date"),
                        "furnish_type": post.get("furnish_type"),
                        "accommodation_type": post.get("accommodation_type"),
                        "account_number": post.get("account_number"),
                        "account_holder_name": post.get("account_holder_name"),
                        "bank_name": post.get("bank_name"),
                        "bank_branch_name": post.get("bank_branch_name"),
                        "account_type": post.get("account_type"),
                        "ifsc_code": post.get("ifsc_code"),
                        "is_oci_card_holder": is_oci_card_holder,
                        "is_oci_card": is_oci_carddatas1 if post.get("is_oci_card") else False,
                        "passport_number": post.get("passport_number"),
                        "passport_image": passportdatas1 if post.get("passport_image_file") else False,
                        "pan_no": post.get("pan_no"),
                        "pan_card_file": pan_card_datas1 if post.get("pan_card_file") else False,
                        "aadhar_card": post.get("aadhar_no"),
                        "aadhar_card_file":aadhardatas1 if post.get("aadhar_card_file") else False,
                        "sso_id":post.get('sso_id'),
                        "addressline":post.get("addressline"),
                        "payment_status":payment_status,
                        "registration_status":registration_status,
                    }

                uniq_request_id = request.env['ir.config_parameter'].sudo().get_param('kshetragna.uniq_request_id')
                if uniq_request_id:
                    unq_id = post.get('sso_id') + "-" + str(uniq_request_id)
                recdyn = request.env['kshetragna.registration'].sudo().search([("order_id", "=", unq_id)],order ="id desc",limit=1)
                if not recdyn:
                    _logger.info(str(rec_values))
                    recdyn = request.env['kshetragna.registration'].sudo().create(rec_values)
                    unq_id = post.get('sso_id') + "-" + str(uniq_request_id)
                    recdyn.write({"order_id": unq_id})
                else:
                    recdyn.sudo().write(rec_values)
                    recdyn.sudo().write({"payment_status": payment_status, "registration_status": registration_status})

                # May 29 - Allow both NRI and INdian Ppll able to redirect to Update address page
                # For india ppl allow to them to redirect to payment view
                if country_id.id != 0:
                    add = "update_address?id="+str(recdyn.id)+"&login_email="+str(post.get("email"))
                    tmpurl =request.httprequest.url.replace('registrationform',add)
                else:
                    values = {"person_name": recdyn.first_name, "login_email": recdyn.email}
                    tmpurl = request.httprequest.url.replace('update_address', 'message')
                return werkzeug.utils.redirect(tmpurl)
                # else:
                # # """ For all registration,allow to make registration and get the passport copy,number
                # #  and restrict the payment flow for non india.
                # #  For Non indian ppl, once registation is done ,
                # #  then that records has sent to approval from isha backend..
                # #  once it approval is done from backend, email has been to sent to them to make payment"""
                #     if country_id.id != 104:
                #         add = "update_address?id="+str(recdyn.id)+"&login_email="+str(post.get("email"))
                #         tmpurl =request.httprequest.url.replace('registrationform',add)
                #     else:
                #         values={"person_name":post.get("first_name"),"login_email":post.get("email")}
                #         tmpurl = request.httprequest.url.replace('registrationform', 'message?login_email=%s'%(post.get("email")))
                #     return werkzeug.utils.redirect(tmpurl)

                    # return request.render('isha_livingspaces.online_kshetragna_status',values)

            except Exception as e:
                values = {"status": 'Error', 'message': "There is error in sending mail",
                        }
                tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                _logger.info(tb_ex)
                return request.render('isha_livingspaces.exception', values)


    @http.route('/livingspaces/static_page', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def onlinekshetragna_static_page_test(self, page, **post):
        '''
        controller to test static page redirection

        :param page: page number
        :param post: value dict
        :return: action dict or string
        '''
        if page in ['one','two','three','four']:
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
            file_url = '/isha_livingspaces/static/description/page_%s.html'%(page)

            page_url = base_url+file_url

            from werkzeug.utils import redirect
            response = redirect(page_url)
            return response

        else:
            return "enter valid page number between 1-4 in words"

    # Enquiry form controller
    @http.route('/livingspaces/enquiryformin', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def onlinekshetragna_enquiry(self,**post):

        fullprofileresponsedatVar = request.env['livingspaces.ssoapi'].sudo().get_fullprofileresponse_DTO()
        #
        consentgrantStatusflagvar = True
        if request.httprequest.method == "GET":
            validation_errors = []
            try:
                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                cururls = str(request.httprequest.url).replace("http://", "https://", 1)
                sso_log = {'request_url': str(cururls),
                           "callback_url": str(cururls),
                           "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                           "hash_value": "",
                           "action": "0",
                           "legal_entity": "IF",
                           "force_consent": "1"
                           }
                ret_list = {'request_url': str(cururls),
                            "callback_url": str(cururls),
                            "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                            }
                data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
                data_enc = data_enc.replace("/", "\\/")
                signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                                     digestmod=hashlib.sha256).hexdigest()
                sso_log["hash_value"] = signature
                sso_log["sso_logurl"] = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_URL1')
                values = {
                    'sso_log': sso_log
                }
                return request.render('isha_livingspaces.livingspacessso', values)
            except Exception as ex:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)
                # values = {'language': language}
                return request.render('isha_livingspaces.exception', values)
        # if request.httprequest.method == "GET":
        #     return request.render('isha_livingspaces.livingspace_registrationform', values)

        # Post Method SSO Session Id Logic
        elif request.httprequest.method == "POST" and "session_id" in request.httprequest.form:

            validation_errors = []
            values = {}
            try:

                result_countries = request.env['res.country'].sudo().search(
                    [('name', '!=', ' ')], order="name asc")

                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                api_key = request.env['ir.config_parameter'].sudo().get_param(
                    'satsang.SSO_API_KEY1')  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                session_id = request.httprequest.form['session_id']
                hash_val = hash_hmac({'session_id': session_id}, ret)
                data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
                request_url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
                sso_user_profile = eval(sso_get_user_profile(request_url, data).text)

                # fullprofileresponsedatVar = get_fullprofileresponse_DTO()
                # fullprofileresponsedatVar = {
                #     "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #     "basicProfile": {
                #         "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "createdDate": "2020-05-05T10:54:05Z",
                #         "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "lastModifiedDate": "2020-06-02T11:37:57Z",
                #         "id": 43083,
                #         "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "email": "krishna.t@ishafoundation.org",
                #         "firstName": "K",
                #         "lastName": "T",
                #         "profilePic": "https://lh3.googleusercontent.com/-I2gNoP1r5kg/AAAAAAAAAAI/AAAAAAAAAAA/AAKWJJOfNXvcjzTF3XhKfjkoCRnjfDrP3Q/photo.jpg?size=250",
                #         "phone": {
                #             "countryCode": "91",
                #             "number": "7867765697"
                #         },
                #         "gender": "MALE",
                #         "dob": "1950-06-01",
                #         "countryOfResidence": "IN"
                #     },
                #     "extendedProfile": {
                #         "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x2",
                #         "createdDate": "2020-05-05T10:54:05Z",
                #         "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "lastModifiedDate": "2020-06-02T11:37:57Z",
                #         "id": 43081,
                #         "passportNum": "sfswe",
                #         "nationality": "IN",
                #         "pan": "11",
                #         "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3"
                #     },
                #     "documents": [],
                #     "attendedPrograms": [],
                #     "profileSettingsConfig": {
                #         "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "createdDate": "2020-05-05T10:54:05Z",
                #         "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "lastModifiedDate": "2020-05-05T10:54:05Z",
                #         "id": 43086,
                #         "nameLocked": False,
                #         "isPhoneVerified": False,
                #         "phoneVerificationDate": None,
                #         "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3"
                #     },
                #     "addresses": [
                #         {
                #             "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #             "createdDate": "2020-05-22T06:36:08Z",
                #             "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #             "lastModifiedDate": "2020-06-02T11:37:57Z",
                #             "id": 18750,
                #             "addressType": "RESIDENCE",
                #             "addressLine1": "a1",
                #             "addressLine2": "a2",
                #             "townVillageDistrict": "a",
                #             "city": "Secunderabad",
                #             "state": "Telangana",
                #             "country": "IN",
                #             "pincode": "500055",
                #             "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3"
                #         }
                #     ]
                # }
                if "autologin_email" in sso_user_profile:
                    fullprofileresponsedatVar["basicProfile"]["email"] = sso_user_profile["autologin_email"]
                    fullprofileresponsedatVar["profileId"] = sso_user_profile["autologin_profile_id"]
                fullprofileresponsedatVar = sso_get_full_data(sso_user_profile['autologin_profile_id'])


                # fullprofileresponsedatVar['basicProfile']['phone']['number'] = fetch_prescreened_participant.mobile

                profresponse = json.dumps(fullprofileresponsedatVar)
                uniq_request_id = request.env['ir.config_parameter'].sudo().get_param('kshetragna.uniq_request_id')
                if uniq_request_id:
                    unq_id = fullprofileresponsedatVar["profileId"] + "-" + str(uniq_request_id)
                else:
                    unq_id = fullprofileresponsedatVar["profileId"] + "-" + str("KTG")

                check_nri_rec = request.env['kshetragna.registration'].sudo().search([("order_id", "=", unq_id), (
                'registration_status', "in", ('waiting_for_approve', 'document_pending', 'document_corrected'))],
                                                                                     order="id desc", limit=1)
                if check_nri_rec:
                    if check_nri_rec.nationality_id.id == 104 or check_nri_rec.country.id == 104:
                        values = {"msg": 'nri_message', 'person_name': check_nri_rec.first_name,
                                  'login_email': fullprofileresponsedatVar["basicProfile"]["email"]}
                        return request.render('isha_livingspaces.kshetragna_user_message', values)

                # searching to see if the current user already enquired or not
                record_exist = request.env['kshetragna.registration'].sudo().search(
                    [("order_id", "=", unq_id)], order="id desc", limit=1)

                if record_exist:
                    if record_exist.registration_status != 'payment_failed':
                        values = {"msg": 'duplicate_enquiry',
                                  'login_email': fullprofileresponsedatVar["basicProfile"]["email"]}
                        return request.render('isha_livingspaces.kshetragna_user_message', values)
                    else:
                        amount = request.env['ir.config_parameter'].sudo().get_param('kshetragna.reg_amount')
                        currency_value = request.env['ir.config_parameter'].sudo().get_param('kshetragna.payment_currency')
                        values = {"reg_id": record_exist.id,
                                  "amount": float(amount),
                                  "currency": currency_value,
                                  "login_email": sso_user_profile["autologin_email"]
                                }
                        return request.render('isha_livingspaces.Kshetragna_payment_page', values)

                values = {}
                countries = request.env['res.country'].sudo().search([('name', '!=', ' ')], order="name asc")
                # countrycodep = fullprofileresponsedatVar["basicProfile"]["countryOfResidence"]
                # if countrycodep == "IN":
                #     countries = request.env['res.country'].sudo().search([('code', '=', "IN")])
                isishangamonev = request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')
                values = {
                    'countries': countries,
                    'states': request.env['res.country.state'].sudo().search([]),
                    'isofname': fullprofileresponsedatVar,
                    'inputobj': profresponse,
                    'isishangamonev': isishangamonev,
                    'is_ishangam': 'True',
                    'login_email': fullprofileresponsedatVar["basicProfile"]["email"]
                }
                return request.render('isha_livingspaces.livingspace_registration_enquiry_from', values)

            except Exception as ex:
                _logger.error("Error caught during request creation", exc_info=True)
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                request.env['livingspaces.tempdebuglog'].sudo().create({
                    'logtext': tb_ex
                })

        elif request.httprequest.method == "POST":
            _logger.info("inner post ----------- kshetragna enquiry registration")

            # allow all of them allow to pay after approvals done
            country_id = post.get("country")
            if country_id:
                country_id = request.env['res.country'].sudo().search([("id", "=", int(post.get("country")))])

            # Mobile Duplicate
            if post.get("phone2"):
                mobile = post.get("phone2")
                reg_kshetrag_obj = request.env['kshetragna.registration'].sudo().search([("mobile", "=", mobile)],
                                                                                        limit=1)
                if reg_kshetrag_obj:
                    values = {"msg": 'duplicate_mobile', 'mobile': mobile}
                    return request.render('isha_livingspaces.kshetragna_user_message', values)
            else:
                mobile = ''

            state_vlue = False
            if post.get("state") != "" and post.get("state") != None:
                state_vlue = request.env['res.country.state'].sudo().search([("id", "=", int(post.get("state")))])

            try:
                rec_values = {
                    "first_name": post.get("first_name"),
                    "last_name": post.get("last_name"),
                    "countrycode": post.get("countrycode"),
                    "mobile":mobile,
                    "gender": post.get("gender"),
                    "email": post.get("email"),
                    "pincode": post.get("pincode"),
                    "city": post.get("city"),
                    "state": state_vlue and state_vlue.name or post.get('state_text_field'),
                    "country": country_id.id if country_id else 104,
                    "nationality_id": post.get("nationality_id"),
                    'sso_id': post.get('sso_id'),
                    'payment_status': "Pending",
                    'registration_status': "Enquiry",
                    "ip_Address": post.get("amazon_ip"),
                    "ip_country": post.get("amazon_country"),

                }
                isishangamonev = request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')
                rec_values['isha_residential_space_m1'] = post.get('isha_residential_space')

                # creation record
                uniq_request_id = request.env['ir.config_parameter'].sudo().get_param('kshetragna.uniq_request_id')
                if uniq_request_id:
                    unq_id = post.get('sso_id') + "-" + str(uniq_request_id)
                recdyn = request.env['kshetragna.registration'].sudo().search([("order_id", "=", unq_id)],
                                                                              order="id desc", limit=1)
                if not recdyn:
                    _logger.info(str(rec_values))
                    created_rec = request.env['kshetragna.registration'].sudo().create(rec_values)
                    unq_id = post.get('sso_id') + "-" + str(uniq_request_id)
                    created_rec.write({"order_id": unq_id})
                else:
                    # regstatus = "payment_initiated"
                    recdyn.sudo().write(rec_values)
                    created_rec = recdyn

                if post.get("email"):
                    login_email = post.get("email")
                else:
                    login_email = fullprofileresponsedatVar["basicProfile"]["email"]


                tmpurl = request.httprequest.url.replace('enquiryformin', 'message?login_email=%s'%(login_email)).replace('livingspaces','kshetragna')
                return werkzeug.utils.redirect(tmpurl)

            except Exception as e:
                values = {"status": 'Error', 'message': "There is error in sending mail",
                          }
                tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                _logger.info(tb_ex)
                return request.render('isha_livingspaces.exception', values)

    # Enquiry form controller - NRI
    @http.route('/livingspaces/expressionofinterest', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def livingspaces_expressionofinterest(self, **post):

        fullprofileresponsedatVar = request.env['livingspaces.ssoapi'].sudo().get_fullprofileresponse_DTO()
        #
        consentgrantStatusflagvar = True
        if request.httprequest.method == "GET":
            validation_errors = []
            try:
                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                cururls = str(request.httprequest.url).replace("http://", "https://", 1)
                sso_log = {'request_url': str(cururls),
                           "callback_url": str(cururls),
                           "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                           "hash_value": "",
                           "action": "0",
                           "legal_entity": "IF",
                           "force_consent": "1"
                           }
                ret_list = {'request_url': str(cururls),
                            "callback_url": str(cururls),
                            "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                            }
                data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
                data_enc = data_enc.replace("/", "\\/")
                signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                                     digestmod=hashlib.sha256).hexdigest()
                sso_log["hash_value"] = signature
                sso_log["sso_logurl"] = request.env['ir.config_parameter'].sudo().get_param(
                    'satsang.SSO_LOGIN_URL1')
                values = {
                    'sso_log': sso_log
                }
                return request.render('isha_livingspaces.livingspacessso', values)
            except Exception as ex:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)
                # values = {'language': language}
                return request.render('isha_livingspaces.exception', values)
        # if request.httprequest.method == "GET":
        #     return request.render('isha_livingspaces.livingspace_registrationform', values)

        # Post Method SSO Session Id Logic
        elif request.httprequest.method == "POST" and "session_id" in request.httprequest.form:

            validation_errors = []
            values = {}
            try:

                result_countries = request.env['res.country'].sudo().search(
                    [('name', '!=', ' ')], order="name asc")

                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                api_key = request.env['ir.config_parameter'].sudo().get_param(
                    'satsang.SSO_API_KEY1')  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                session_id = request.httprequest.form['session_id']
                hash_val = hash_hmac({'session_id': session_id}, ret)
                data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
                request_url = request.env['ir.config_parameter'].sudo().get_param(
                    'satsang.SSO_LOGIN_INFO_ENDPOINT1')
                sso_user_profile = eval(sso_get_user_profile(request_url, data).text)

                # fullprofileresponsedatVar = get_fullprofileresponse_DTO()
                # fullprofileresponsedatVar = {
                #     "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #     "basicProfile": {
                #         "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "createdDate": "2020-05-05T10:54:05Z",
                #         "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "lastModifiedDate": "2020-06-02T11:37:57Z",
                #         "id": 43083,
                #         "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "email": "krishna.t@ishafoundation.org",
                #         "firstName": "K",
                #         "lastName": "T",
                #         "profilePic": "https://lh3.googleusercontent.com/-I2gNoP1r5kg/AAAAAAAAAAI/AAAAAAAAAAA/AAKWJJOfNXvcjzTF3XhKfjkoCRnjfDrP3Q/photo.jpg?size=250",
                #         "phone": {
                #             "countryCode": "91",
                #             "number": "7867765697"
                #         },
                #         "gender": "MALE",
                #         "dob": "1950-06-01",
                #         "countryOfResidence": "IN"
                #     },
                #     "extendedProfile": {
                #         "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "createdDate": "2020-05-05T10:54:05Z",
                #         "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "lastModifiedDate": "2020-06-02T11:37:57Z",
                #         "id": 43081,
                #         "passportNum": "sfswe",
                #         "nationality": "IN",
                #         "pan": "11",
                #         "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3"
                #     },
                #     "documents": [],
                #     "attendedPrograms": [],
                #     "profileSettingsConfig": {
                #         "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "createdDate": "2020-05-05T10:54:05Z",
                #         "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #         "lastModifiedDate": "2020-05-05T10:54:05Z",
                #         "id": 43086,
                #         "nameLocked": False,
                #         "isPhoneVerified": False,
                #         "phoneVerificationDate": None,
                #         "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3"
                #     },
                #     "addresses": [
                #         {
                #             "createdBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #             "createdDate": "2020-05-22T06:36:08Z",
                #             "lastModifiedBy": "BS5uh8qd73QzRGv8BnLlvmsuW2x3",
                #             "lastModifiedDate": "2020-06-02T11:37:57Z",
                #             "id": 18750,
                #             "addressType": "RESIDENCE",
                #             "addressLine1": "a1",
                #             "addressLine2": "a2",
                #             "townVillageDistrict": "a",
                #             "city": "Secunderabad",
                #             "state": "Telangana",
                #             "country": "IN",
                #             "pincode": "500055",
                #             "profileId": "BS5uh8qd73QzRGv8BnLlvmsuW2x3"
                #         }
                #     ]
                # }
                if "autologin_email" in sso_user_profile:
                    fullprofileresponsedatVar["basicProfile"]["email"] = sso_user_profile["autologin_email"]
                    fullprofileresponsedatVar["profileId"] = sso_user_profile["autologin_profile_id"]
                fullprofileresponsedatVar = sso_get_full_data(sso_user_profile['autologin_profile_id'])

                # fullprofileresponsedatVar['basicProfile']['phone']['number'] = fetch_prescreened_participant.mobile

                profresponse = json.dumps(fullprofileresponsedatVar)
                uniq_request_id = request.env['ir.config_parameter'].sudo().get_param('kshetragna.uniq_request_id')
                if uniq_request_id:
                    unq_id = fullprofileresponsedatVar["profileId"] + "-" + str(uniq_request_id)
                else:
                    unq_id = fullprofileresponsedatVar["profileId"] + "-" + str("KTG")

                check_nri_rec = request.env['kshetragna.registration'].sudo().search([("order_id", "=", unq_id), (
                    'registration_status', "in",
                    ('waiting_for_approve', 'document_pending', 'document_corrected'))],
                                                                                     order="id desc", limit=1)
                if check_nri_rec:
                    if check_nri_rec.nationality_id.id == 104 or check_nri_rec.country.id == 104:
                        values = {"msg": 'nri_message', 'person_name': check_nri_rec.first_name,
                                  'login_email': fullprofileresponsedatVar["basicProfile"]["email"]}
                        return request.render('isha_livingspaces.kshetragna_user_message', values)

                # searching to see if the current user already enquired or not
                record_exist = request.env['kshetragna.registration'].sudo().search(
                    [("order_id", "=", unq_id)], order="id desc", limit=1)

                if record_exist:
                    if record_exist.registration_status != 'payment_failed':
                        values = {"msg": 'duplicate_enquiry',
                                  'login_email': fullprofileresponsedatVar["basicProfile"]["email"]}
                        return request.render('isha_livingspaces.kshetragna_user_message', values)
                    else:
                        amount = request.env['ir.config_parameter'].sudo().get_param('kshetragna.reg_amount')
                        currency_value = request.env['ir.config_parameter'].sudo().get_param(
                            'kshetragna.payment_currency')
                        values = {"reg_id": record_exist.id,
                                  "amount": float(amount),
                                  "currency": currency_value,
                                  "login_email": sso_user_profile["autologin_email"]
                                  }
                        return request.render('isha_livingspaces.Kshetragna_payment_page', values)

                values = {}
                countries = request.env['res.country'].sudo().search([('name', '!=', ' ')], order="name asc")
                # countrycodep = fullprofileresponsedatVar["basicProfile"]["countryOfResidence"]
                # if countrycodep == "IN":
                #     countries = request.env['res.country'].sudo().search([('code', '=', "IN")])
                isishangamonev = request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')

                values = {
                    'countries': countries,
                    'states': request.env['res.country.state'].sudo().search([]),
                    'isofname': fullprofileresponsedatVar,
                    'inputobj': profresponse,
                    'isishangamonev': isishangamonev,
                    'is_ishangam': 'False',
                    'login_email': fullprofileresponsedatVar["basicProfile"]["email"],
                }
                return request.render('isha_livingspaces.livingspace_registration_enquiry_from', values)

            except Exception as ex:
                _logger.error("Error caught during request creation", exc_info=True)
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                request.env['livingspaces.tempdebuglog'].sudo().create({
                    'logtext': tb_ex
                })

        elif request.httprequest.method == "POST":
            _logger.info("inner post ----------- kshetragna enquiry registration")

            # allow all of them allow to pay after approvals done
            country_id = post.get("country")
            if country_id:
                country_id = request.env['res.country'].sudo().search([("id", "=", int(post.get("country")))])

            # Mobile Duplicate
            if post.get("phone2"):
                mobile = post.get("phone2")
                reg_kshetrag_obj = request.env['kshetragna.registration'].sudo().search([("mobile", "=", mobile)],
                                                                                        limit=1)
                if reg_kshetrag_obj:
                    values = {"msg": 'duplicate_mobile', 'mobile': mobile}
                    return request.render('isha_livingspaces.kshetragna_user_message', values)
            else:
                mobile = ''

            state_vlue = False
            if post.get("state") != "" and post.get("state") != None:
                state_vlue = request.env['res.country.state'].sudo().search([("id", "=", int(post.get("state")))])

            # if post.get('isha_residential_space') == 'isha_institute':
            #     is_usa_bool = True
            # else:
            #     is_usa_bool = False

            try:
                rec_values = {
                    "first_name": post.get("first_name"),
                    "last_name": post.get("last_name"),
                    "countrycode": post.get("countrycode"),
                    "mobile": mobile,
                    "gender": post.get("gender"),
                    "email": post.get("email"),
                    "pincode": post.get("pincode"),
                    "city": post.get("city"),
                    "state": state_vlue and state_vlue.name or post.get('state_text_field'),
                    "country": country_id.id if country_id else 104,
                    "nationality_id": post.get("nationality_id"),
                    "sso_id": post.get('sso_id'),
                    "payment_status": "Pending",
                    "registration_status": "Enquiry",
                    "ip_Address": post.get("amazon_ip"),
                    "ip_country": post.get("amazon_country"),
                    "isha_residential_space_m2":post.get('isha_residential_space'),
                }
                isishangamonev = request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')
                rec_values['isha_residential_space_m2'] = post.get('isha_residential_space')

                # creation record
                uniq_request_id = request.env['ir.config_parameter'].sudo().get_param('kshetragna.uniq_request_id')
                if uniq_request_id:
                    unq_id = post.get('sso_id') + "-" + str(uniq_request_id)

                recdyn = request.env['kshetragna.registration'].sudo().search([("order_id", "=", unq_id)],
                                                                              order="id desc", limit=1)
                if not recdyn:
                    created_rec = request.env['kshetragna.registration'].sudo().create(rec_values)
                    unq_id = post.get('sso_id') + "-" + str(uniq_request_id)
                    created_rec.write({"order_id": unq_id})

                    # Api call to push data to US system
                    nationality_id = request.env['res.country'].sudo().search([('id', '=', created_rec.nationality_id.id)])
                    if created_rec.isha_residential_space_m2 == 'isha_institute':
                        api_info = {
                            'fname': created_rec.first_name,
                            'lname': created_rec.last_name,
                            'email': created_rec.email,
                            'phone_country_code': created_rec.countrycode,
                            'phone': created_rec.mobile,
                            'gender': created_rec.gender,
                            'nationality': nationality_id.name,
                            'city': created_rec.city,
                            'state': created_rec.state,
                            'country': created_rec.country.name,
                            'sso_cor': created_rec.country.name,
                            'ssoId': created_rec.sso_id,
                            'zip': created_rec.pincode,
                            'createdDate': created_rec.create_date,
                            'housing_at': "2",
                        }
                        self.kshetragna_api_async_push(api_info, created_rec)

                    if created_rec.isha_residential_space_m2 == 'isha_yoga_center':
                        s1_url = request.env['ir.config_parameter'].sudo().get_param('kshetragna.kshetragna_india_url')
                        s2_url = request.env['ir.config_parameter'].sudo().get_param('kshetragna.kshetragna_us_url')

                        # redirect_url = s1_url + "/kshetragna/sync_indiadata"
                        base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
                        url = s1_url + "/kshetragna/sync_indiadata"

                        _logger.info("kshetragna - New registration Pass data = " + str(rec_values))
                        headersinfo = {
                            "content-Type": "application/x-www-form-urlencoded",
                            "Accept": "application/json",
                            "Cookie": ""
                        }
                        r = requests.post(url, data=rec_values, headers=headersinfo)
                        _logger.info(r.text)
                        _logger.info(r.status_code)
                        if r.status_code == 200:
                            created_rec.write({'is_sync': True})
                            _logger.info('Successfully - Record Moved to Server')
                else:
                    # regstatus = "payment_initiated"
                    recdyn.sudo().write(rec_values)
                    _logger.info('Successfully - Record Updated')

                if post.get("email"):
                    login_email = post.get("email")
                else:
                    login_email = fullprofileresponsedatVar["basicProfile"]["email"]

                if post.get('isha_residential_space') == "isha_institute":
                    isha_residential_space = 'True'
                else:
                    isha_residential_space = 'False'

                values = {
                    "person_name": post.get("info"),
                    "login_email": post.get("login_email"),
                    "isha_residential_space": isha_residential_space
                }

                return request.render('isha_livingspaces.online_kshetragna_status', values)

                # tmpurl = request.httprequest.url.replace('expressionofinterest','message_nri?login_email=%s' % (login_email)).replace('livingspaces', 'kshetragna')
                # return werkzeug.utils.redirect(tmpurl)

            except Exception as e:
                values = {"status": 'Error', 'message': "There is error in sending mail",
                          }
                tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                _logger.info(tb_ex)
                return request.render('isha_livingspaces.exception', values)

    @http.route('/livingspaces/enquiryform2/', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def registrationform(self, legal_entity="", consent_grant_status="", id_token="", cbnm="", testsso="0",
                         program_id="0", language="english", **post):
        fullprofileresponsedatVar = request.env['livingspaces.ssoapi'].sudo().get_fullprofileresponse_DTO()
        consentgrantStatusflagvar = True
        if request.httprequest.method == "GET":
            validation_errors = []
            try:
                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                cururls = str(request.httprequest.url).replace("http://", "https://", 1)
                sso_log = {'request_url': str(cururls),
                           "callback_url": str(cururls),
                           "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                           "hash_value": "",
                           "action": "0",
                           "legal_entity": "IF",
                           "force_consent": "1"
                           }
                ret_list = {'request_url': str(cururls),
                            "callback_url": str(cururls),
                            "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                            }
                data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
                data_enc = data_enc.replace("/", "\\/")
                signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                                     digestmod=hashlib.sha256).hexdigest()
                sso_log["hash_value"] = signature
                sso_log["sso_logurl"] = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_URL1')
                values = {
                    'sso_log': sso_log
                }
                return request.render('isha_livingspaces.livingspacessso', values)
            except Exception as ex:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)
                values = {'language': language}
                return request.render('isha_livingspaces.exception', values)
        # if request.httprequest.method == "GET":
        #     validation_errors = []
        #     diseases_list = self.get_disease_list()
        #     values = {}
        #     try:
        #         result_countries = request.env['res.country'].sudo().search(
        #             [('name', '!=', ' ')])
        #
        #         profresponse = json.dumps(fullprofileresponsedatVar)
        #         values = {
        #             'programregn': request.env['livingspaces.registration'].sudo().search([]),
        #             'countries': result_countries,
        #             'doctypes': request.env['documenttype'].sudo().search([]),
        #             'states': request.env['res.country.state'].sudo().search([]),
        #             'submitted': post.get('submitted', False),
        #             'current_page': 1,
        #             'filled_pages': 1,
        #             'isofname': fullprofileresponsedatVar,
        #             'ischangeofparticipant': post.get('iscop', False),
        #             # 'changeofparticipant_ref': post.get('cop', ''),
        #             'ssoadproofuri': '',
        #             'isoidproofuri': '',
        #             'inputobj': profresponse,
        #             'enableishaautofill': consentgrantStatusflagvar,
        #             'consent_grant_status': '1',  # consent_grant_status,
        #             'program_id': program_id,
        #             'diseases': diseases_list,
        #         }
        #         values['validation_errors'] = validation_errors
        #         return request.render('isha_livingspaces.pgmregn', values)
        #     except (UserError, AccessError, ValidationError) as exc:
        #         validation_errors.append(ustr(exc))
        #     except Exception:
        #         _logger.error(
        #             "Error caught during request creation", exc_info=True)
        elif request.httprequest.method == "POST" and "session_id" in request.httprequest.form:
            validation_errors = []
            diseases_list = self.get_disease_list()
            values = {}
            try:
                # qsurl = str(request.httprequest.url).split('?')[1]
                # countries_scl = request.env['res.country'].sudo().search()
                # countries = request.env['res.country'].sudo().search()
                # result_countries =  countries
                result_countries = request.env['res.country'].sudo().search(
                    [('name', '!=', ' ')])

                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                api_key = request.env['ir.config_parameter'].sudo().get_param(
                    'satsang.SSO_API_KEY1')  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                session_id = request.httprequest.form['session_id']
                hash_val = hash_hmac({'session_id': session_id}, ret)
                data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
                request_url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
                sso_user_profile = eval(sso_get_user_profile(request_url, data).text)

                fullprofileresponsedatVar = get_fullprofileresponse_DTO()
                if "autologin_email" in sso_user_profile:
                    fullprofileresponsedatVar["basicProfile"]["email"] = sso_user_profile["autologin_email"]
                    fullprofileresponsedatVar["profileId"] = sso_user_profile["autologin_profile_id"]
                if consent_grant_status == "1":
                    fullprofileresponsedatVar = sso_get_full_data(sso_user_profile['autologin_profile_id']);

                profresponse = json.dumps(fullprofileresponsedatVar)
                values = {
                    'programregn': request.env['livingspaces.registration'].sudo().search([]),
                    'countries': result_countries,
                    'doctypes': request.env['documenttype'].sudo().search([]),
                    'states': request.env['res.country.state'].sudo().search([]),
                    'submitted': post.get('submitted', False),
                    'current_page': 1,
                    'filled_pages': 1,
                    'isofname': fullprofileresponsedatVar,
                    'ischangeofparticipant': '',  # post.get('iscop', False),
                    'changeofparticipant_ref': '',  # post.get('cop', ''),
                    'ssoadproofuri': '',
                    'isoidproofuri': '',
                    'inputobj': profresponse,
                    'enableishaautofill': consentgrantStatusflagvar,
                    'consent_grant_status': '1',  # consent_grant_status,
                    'program_id': program_id,
                    'diseases': diseases_list,
                }

                values['validation_errors'] = validation_errors
                return request.render('isha_livingspaces.pgmregn', values)

            # except (UserError, AccessError, ValidationError) as exc:
            #     validation_errors.append(ustr(exc))

            except Exception as ex:
                _logger.error("Error caught during request creation", exc_info=True)
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                request.env['livingspaces.tempdebuglog'].sudo().create({
                    'logtext': tb_ex
                })

        elif request.httprequest.method == "POST":
            validation_errors = []
            first_data = []
            try:
                first_name = post.get('first_name')
                last_name = post.get('last_name')
                participant_email = post.get('participant_email')
                gender = post.get('gender')
                dob = post.get('dob')
                occupation = post.get('occupation')
                country = post.get('country')
                nationality_id = post.get('nationality_id')
                state = post.get('state')
                if (state):
                    state = int(state)
                else:
                    state = ''
                pincode = post.get('pincode')
                city = post.get('city')
                countrycode = post.get('countrycode')
                phone = post.get('phone')
                phone2 = post.get('phone2', None)
                phone2_cc = post.get('countrycodep2', None)
                marital_status = post.get('marital_status')
                tmpjspgmregfmwayesno = post.get('jspgmregfmwayesno')
                if (tmpjspgmregfmwayesno == 'YES'):
                    wacountrycode = countrycode
                    waphone = phone
                else:
                    wacountrycode = post.get('wacountrycode')
                    waphone = post.get('waphone')

                tmpjslsdualcity = post.get('jsdualcityyesno')
                if (tmpjslsdualcity == 'YES'):
                    dualcityname = post.get('dual_nationality_id')
                else:
                    dualcityname = ''

                tmpjsocistatus = post.get('jsociyesno')
                if (tmpjsocistatus):
                    ocistatus = True
                else:
                    ocistatus = False

                # education_qualification = post.get('education_qualification')
                # spoken_languages = post.get('spoken_languages')

                spoken_languages = request.httprequest.form.getlist('spoken_languages')
                for sl in spoken_languages:
                    print(sl)

                spokenlanguagescollec = [(0, 0, {
                    'languagesspoken': spoken_language,
                }) for spoken_language in
                                         spoken_languages]

                other_spoken_lang = ''
                if 'OTHERS' in spoken_languages:
                    other_spoken_lang = post.get('other_lang')

                ischangeofparticipant = False
                copapplied_emailsms_pending = False

                if consentgrantStatusflagvar == True:
                    fullprofileresponsedatVar['basicProfile']['firstName'] = first_name
                    fullprofileresponsedatVar['basicProfile']['lastName'] = last_name
                    fullprofileresponsedatVar['basicProfile']['gender'] = gender.upper()
                    fullprofileresponsedatVar['basicProfile']['email'] = participant_email
                reg_dict = {
                    'first_name': first_name,
                    'last_name': last_name,
                    'participant_email': participant_email,
                    'gender': gender,
                    'dob': dob,
                    'occupation': occupation,
                    'marital_status': marital_status,
                    'city': city,
                    'country': int(country),
                    'nationality_id': int(nationality_id),
                    'state': state,
                    'pincode': pincode,
                    'countrycode': countrycode,
                    'phone': phone,
                    'phone2': phone2,
                    'countrycode2': phone2_cc,
                    'wacountrycode': wacountrycode,
                    'waphone': waphone,
                    'dualcityname': dualcityname,
                    'ocistatus': ocistatus,
                    # 'education_qualification': education_qualification,
                    'spoken_languages': spokenlanguagescollec,
                    'other_spoken_lang': other_spoken_lang,
                    'registration_status': 'Incomplete',
                    'incompleteregistration_pagescompleted': 1,
                    'incompleteregistration_email_send': False,
                    'incompleteregistration_sms_send': False,
                    'registration_email_send': False,
                    'registration_sms_send': False,
                }
                disease_list = [x for x in post if 'is_disease' in x]
                if len(disease_list) > 0:
                    for disease in disease_list:
                        reg_dict.update({disease: 'YES'})

                res = get_duplicaterecord(first_name, participant_email)
                incompleteflage = False;
                if (res == True):
                    recs = request.env['livingspaces.registration'].sudo().search(
                        ['&', ('first_name', '=', first_name), ('participant_email', '=', participant_email)])
                    values = {'first_name': first_name}
                    if (recs[0].incompleteregistration_pagescompleted != 1):
                        return request.render("isha_livingspaces.alreadyregistered", values)
                    else:
                        incompleteflage = True;

                resofismed = get_is_meditator_or_ieo(first_name, phone, participant_email, True, False, False, False)
                is_meditator = resofismed['is_meditator']
                if (incompleteflage != True):
                    recdyn = request.env['livingspaces.registration'].sudo().create(reg_dict)
                else:
                    recdyn = request.env['livingspaces.registration'].sudo().search([('first_name', '=', first_name)])

                first_data = request.env['livingspaces.registration'].sudo().search(
                    [('access_token', '=', recdyn[0].access_token)])
                contact_id = first_data.contact_id_fkey.id

                program_category = request.env['master.program.category'].sudo().search(
                    [('keyprogram', '=', True)])
                ishangamcategories = []
                i = 0
                for val in program_category:
                    ishangamcategories.insert(i, val.programcategory)
                    i = i + 1

                print('contact_id_fkey:', contact_id)
                program_category = ['Inner Engineering', 'Bhava Spandana', 'Shoonya', 'Hatha Yoga', 'Samyama']
                pgmrecs = first_data.getProgramAttendance(contact_id, ishangamcategories)
            # if (spoken_languages[0] != ''):
            #     first_data.spoken_languages = spokenlanguagescollec

            except Exception as ex:
                # stacktrace = traceback.print_exc()
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                request.env['livingspaces.tempdebuglog'].sudo().create({
                    'logtext': 'inside 1 page post' + tb_ex
                })
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)

            if not validation_errors:
                if (False):  # progtype == 'Medical Consultation Review' or progtype == 'IECO - Preliminary class'
                    first_data.incompleteregistration_pagescompleted = 1
                    first_data.registration_status = "Incomplete"
                    first_data.registration_email_send = False
                    first_data.registration_sms_send = False
                    values = {'object': first_data}
                    return request.render("isha_livingspaces.pgmregn2", values)
                else:
                    state_onlyindia = request.env['res.country.state'].sudo().search([])
                    values = {
                        'pgmrecs': pgmrecs,
                        'programregn': first_data,
                        'countries': request.env['res.country'].sudo().search([]),
                        'states': state_onlyindia,
                        'prgregncenters': request.env['isha.center'].sudo().search([]),
                        'prgregnprogramnamecategories': request.env['master.program.category'].sudo().search(
                            [('keyprogram', '=', True)]),
                        'submitted': post.get('submitted', False),
                        'ismeditator': is_meditator,
                        'current_page': 2,
                        'filled_pages': 2,
                        'firstnamevar': first_name,
                    }
                    values['validation_errors'] = validation_errors
                    if is_meditator:
                        return request.render("isha_livingspaces.registrationsuccessnew2", values)
                    else:
                        return request.render("isha_livingspaces.livingregn2", values)
            else:
                _logger.log(25, validation_errors)
                logtext = 'Date:' + datetime.now().strftime("%m/%d/%Y, %H:%M:%S") + ': ' + ' '.join(
                    [str(elem) for elem in validation_errors])
                request.env['livingspaces.tempdebuglog'].sudo().create({
                    'logtext': logtext
                })
                if (logtext.__contains__("Invalid phone number or country code for Phone")):
                    return request.render('isha_livingspaces.phone_exception')
                else:
                    return request.render('isha_livingspaces.exception')

    @http.route('/livingspaces/registrationform2', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def registrationform2(self, access_token, **post):
        first_data = request.env['livingspaces.registration'].sudo().search([('access_token', '=', access_token)])
        if (first_data.incompleteregistration_pagescompleted == 5):
            values = {'object': first_data}
            return request.render("isha_livingspaces.registrationcompletion", values)

        if first_data.access_token != None and request.httprequest.method == "POST":
            values = {}
            validation_errors = []
            try:
                ismedichoice = post.get('ismedichoice')
                if ismedichoice == "YES":
                    fprogramnames = request.httprequest.form.getlist('fprogramname')
                    pgmdates = request.httprequest.form.getlist('pgmdate')
                    teachernames = request.httprequest.form.getlist('teachername')
                    pgmcountrys = request.httprequest.form.getlist('pgmcountry')
                    pgmstates = request.httprequest.form.getlist('pgmstate1')
                    pgmlocations = request.httprequest.form.getlist('pgmlocation1')
                    pgmhistorycollec = [(0, 0, {
                        'programname': fprogramname,
                        'pgmdate': pgmdate,
                        'teachername': teachername,
                        'pgmcountry': pgmcountry,
                        'pgmstate': pgmstate,
                        'pgmlocation': pgmlocation
                    }) for fprogramname, pgmdate, teachername, pgmcountry, pgmstate, pgmlocation in
                                        zip(fprogramnames, pgmdates, teachernames, pgmcountrys, pgmstates,
                                            pgmlocations)]
                    otherpractices = post.get('otherpractices')

                    first_data.otherpractices = otherpractices
                    if (pgmdates[0] != ''):
                        first_data.programhistory = pgmhistorycollec
                else:
                    ytfollower = post.get('op1')
                    emfollower = post.get('op2')
                    otfollower = post.get('op3')
                    first_data.ytfollower = ytfollower
                    first_data.emfollower = emfollower
                    first_data.otfollower = otfollower

                first_data = request.env['livingspaces.registration'].sudo().search(
                    [('access_token', '=', access_token)])
                first_data.incompleteregistration_pagescompleted = 2

                values = {
                    'programregn': first_data,
                    'submitted': post.get('submitted', False),
                    'countries': request.env['res.country'].sudo().search([]),
                    'current_page': 3,
                    'filled_pages': 3,
                }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception as ex:
                _logger.error(
                    "Error caught during request creation", ex, exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")

            if not validation_errors:
                values['validation_errors'] = validation_errors
                values['first_name'] = first_data.first_name
                if ismedichoice:
                    if ismedichoice == 'YES':
                        request.env['mail.template'].sudo().search(
                            [('name', '=', 'livingspaces: Program enquiry')]).send_mail(first_data.id, force_send=True)
                        return request.render("isha_livingspaces.registrationsuccessnew2", values)
                    elif ismedichoice == 'NO':
                        request.env['mail.template'].sudo().search(
                            [('name', '=', 'livingspaces: Program enquiry')]).send_mail(first_data.id, force_send=True)
                        return request.render("isha_livingspaces.registrationsuccessnewnotmedi2", values)
                else:
                    request.env['mail.template'].sudo().search(
                        [('name', '=', 'livingspaces: Program enquiry')]).send_mail(first_data.id, force_send=True)
                    return request.render("isha_livingspaces.registrationsuccessnew2", values)
            else:
                _logger.log(25, validation_errors)
                logtext = 'Date:' + datetime.now().strftime("%m/%d/%Y, %H:%M:%S") + ': ' + ' '.join(
                    [str(elem) for elem in validation_errors])
                request.env['livingspaces.tempdebuglog'].sudo().create({
                    'logtext': logtext
                })
                return request.render('isha_livingspaces.exception')
        elif first_data.access_token != None and request.httprequest.method == "GET":
            validation_errors = []
            values = {}
            contact_id = first_data.contact_id_fkey.id
            program_category = request.env['master.program.category'].sudo().search(
                [('keyprogram', '=', True)])
            ishangamcategories = []
            i = 0
            for val in program_category:
                ishangamcategories.insert(i, val.programcategory)
                i = i + 1

            #    program_category = ['Inner Engineering', 'Bhava Spandana', 'Shoonya', 'Hatha Yoga', 'Samyama']
            pgmrecs = first_data.getProgramAttendance(contact_id, ishangamcategories)
            state_onlyindia = request.env['res.country.state'].sudo().search([('country_id', '=', 104)])

            try:
                values = {
                    'pgmrecs': pgmrecs,
                    'programregn': first_data,
                    'countries': request.env['res.country'].sudo().search([]),
                    'states': state_onlyindia,
                    'prgregncenters': request.env['isha.center'].sudo().search([]),
                    'prgregnprogramnamecategories': request.env['master.program.category'].sudo().search(
                        [('keyprogram', '=', True)]),
                    # 'programschedules': request.env['livingspaces.program.schedule'].sudo().search([]),
                    # 'programscheduleroomdatas': request.env['livingspaces.program.schedule.roomdata'].sudo().search([]),
                    'submitted': post.get('submitted', False),
                    'current_page': 2,
                    'filled_pages': 2,
                    'firstnamevar': first_data.first_name,
                }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception as ex:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                values['validation_errors'] = validation_errors
            # return request.render("isha_livingspaces.registrationsuccess2", values)

    @http.route('/livingspaces/get_center', type='http', auth="public", methods=['GET'], website=True, sitemap=False)
    def scope_location_data(self, country_id=None, state_id=None, **post):
        state_ids = {}
        center_ids = {}
        if country_id:
            # For india return the possible states
            if int(country_id) == 104:
                state_data = request.env['res.country.state'].sudo().search_read(
                    domain=[('country_id', '=', int(country_id))],
                    fields=['id', 'name']
                )
                for state in state_data:
                    state_ids[str(state['id'])] = state['name']


            else:
                # For non india get the list of centers from i_p_c and r_c
                center_data = request.env['isha.pincode.center'].sudo().search_read(
                    domain=[('country_id', '=', int(country_id))],
                    fields=['center_id']
                )

                center_data += request.env['res.country'].sudo().search_read(
                    domain=[('id', '=', int(country_id))],
                    fields=['center_id']
                )

                for center in center_data:
                    if center['center_id']:
                        center_ids[str(center['center_id'][0])] = True
        elif state_id:
            # For direct state selection get the center from r_c_s directly
            center_data = request.env['res.country.state'].sudo().search_read(
                domain=[('id', '=', int(state_id))],
                fields=['center_ids']
            )
            for state in center_data:
                for center in state['center_ids']:
                    center_ids[str(center)] = True

        result = {'state_ids': state_ids, 'center_ids': center_ids}
        return json.dumps(result)

    def get_disease_list(self):
        mp_model = request.env['livingspaces.registration']

        field_list = [f for f in mp_model._fields if f.startswith("is_disease")]

        result = [
            {
                'name': mp_model._fields[f].string,
                'key': f,
                'selected': 'Yes'
            }

            for f in field_list
        ]

        return result

    @http.route('/kshetragna/paystatus', type='http', methods=['GET', 'POST'],
                auth='public', csrf=False)
    def kshetragnapaystatus(self, status="", amount="", transactionId="", requestId="", currency="", errorCode="",
                  errorReason="", errorMsg="", errCode="", errReason="", errMsg="", **post):
        c_value_pr = {'payment_status': status,
                      'amount': amount,
                      'paymenttrackingid': transactionId,
                      'orderid': requestId,
                      'currency': currency,
                      'errorcode': errCode,
                      'errorreason': errReason,
                      'errormsg': errMsg
                      }
        return self.kshetragnapaymentresponse_returntemplate(c_value_pr)

    @http.route('/kshetragna/registrationform/documentpage', type='http', auth="public", methods=["GET", "POST"],
                website=True,
                csrf=False)
    def onlinekshetragna_registration_document_page(self, **post):
        if request.httprequest.method == "GET":
            _logger.info("Loofer into Paymetn view")
            # values = {"reg_id": post.get("registration_id")}
            # return request.render('isha_livingspaces.Kshetragna_payment_page', values)
            validation_errors = []
            try:
                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                cururls = str(request.httprequest.url).replace("http://", "https://", 1)
                sso_log = {'request_url': str(cururls),
                           "callback_url": str(cururls),
                           "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                           "hash_value": "",
                           "action": "0",
                           "legal_entity": "IF",
                           "force_consent": "1"
                           }
                ret_list = {'request_url': str(cururls),
                            "callback_url": str(cururls),
                            "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                            }
                data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
                data_enc = data_enc.replace("/", "\\/")
                signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                                     digestmod=hashlib.sha256).hexdigest()
                sso_log["hash_value"] = signature
                sso_log["sso_logurl"] = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_URL1')
                values = {
                    'sso_log': sso_log
                }
                return request.render('isha_livingspaces.livingspacessso', values)
            except Exception as ex:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)
                # values = {'language': language}

        if request.httprequest.method == "POST" and "session_id" in request.httprequest.form:
        # if request.httprequest.method == "GET":
            _logger.info("Loading view  into Document Pending view")

            ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
            api_key = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.SSO_API_KEY1')  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
            session_id = request.httprequest.form['session_id']
            hash_val = hash_hmac({'session_id': session_id}, ret)
            data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
            request_url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
            sso_user_profile = eval(sso_get_user_profile(request_url, data).text)
            auto_mail = sso_user_profile["autologin_email"]
            profile_id = sso_user_profile["autologin_profile_id"]

            application_data = post.get('reg_values')
            decode_application_data = base64.b64decode(application_data)
            s1 = decode_application_data.decode("UTF-8")
            registration_id = s1.split('&')[0].split('=')[1]
            passport = s1.split('&')[1].split('=')[1]
            pan = s1.split('&')[2].split('=')[1]
            oca_card = s1.split('&')[3].split('=')[1]
            aadhar = s1.split('&')[4].split('=')[1]
            pan_aadhar = s1.split('&')[5].split('=')[1]
            # rec = request.env['kshetragna.registration'].sudo().search(
            #     [("id", "=", registration_id)], limit=1)
            rec = request.env['kshetragna.registration'].sudo().search(
                [("id", "=", registration_id),
                 ("sso_id", "=", profile_id)], limit=1)
            # email_id = auto_mail
            # rec = request.env['kshetragna.registration'].sudo().search(
            #     [("id", "=", registration_id),
            #      ("email", "=", auto_mail),
            #      ("sso_id", "=", profile_id)], limit=1)
            # auto_mail = 'krishna.t@ishafoundation.org'
            if rec and rec.email == auto_mail:
                # responsecheckexisting = self.checkExistingTxn_livingspace(rec.order_id, profile_id, **post)
                # if responsecheckexisting.get("valid") == False:
                #     values = {"support_email": 'info.kshetragna@ishafoundation.org',
                #               "status": responsecheckexisting.get("status"),
                #               "program_name": "KTG",
                #               "login_email": auto_mail,
                #               "transactionId": "",
                #               "region_support_email_id": "",
                #               "err_msg": responsecheckexisting.get("errmsg"), }
                #     return request.render("isha_livingspaces.payumoney_status_template", values)

                values = {
                    "reg_id": registration_id,
                    "passport": passport,
                    "pan": pan,
                    "aadhar":aadhar,
                    "oca_card": oca_card,
                    "pan_aadhar": pan_aadhar,
                    "finance_comments":rec.upload_doc_comments,
                    "login_email": auto_mail,
                }
                return request.render('isha_livingspaces.Kshetragna_document_rediret_page', values)
            else:
                if rec.email :
                    reg_email = rec.email
                else:
                    reg_email = ''
                _logger.info("Record mismatch, login email and registration email is different")
                # return request.render('isha_livingspaces.kshetragna_error_template')
                values = {"msg": 'mismatch_log', 'login_email': auto_mail,'reg_email':reg_email}
                return request.render('isha_livingspaces.kshetragna_user_message', values)

        if request.httprequest.method == "POST":
            user_comments = ''
            if post.get('user_comments'):
                user_comments = post.get('user_comments')

            finance_comments = ''
            if post.get('finance_comments'):
                finance_comments = post.get('finance_comments')

            vals = {
                "registration_status": "document_corrected",
                # "user_comments":user_comments
            }
            # Pan
            if post.get("pan_card_file"):
                pan_cardattached_filephoto = request.httprequest.files.getlist('pan_card_file')
                read1 = pan_cardattached_filephoto[0].read()
                pan_card_datas1 = base64.b64encode(read1).replace(b'\n', b'')
                vals['pan_card_file'] = pan_card_datas1
                vals['pan_no'] = post.get("pan_no")
            # Aadhar
            if post.get("aadhar_card_file"):
                aadhar_cardattached_filephoto = request.httprequest.files.getlist('aadhar_card_file')
                read1 = aadhar_cardattached_filephoto[0].read()
                aadhar_card_datas1 = base64.b64encode(read1).replace(b'\n', b'')
                vals['aadhar_card_file'] = aadhar_card_datas1
                vals['aadhar_card'] = post.get("aadhar_no")
            #OCI Card
            if post.get("is_oci_card"):
                is_oci_card_filephoto = request.httprequest.files.getlist('is_oci_card')
                is_oci_card_read = is_oci_card_filephoto[0].read()
                is_oci_carddatas1 = base64.b64encode(is_oci_card_read).replace(b'\n', b'')
                vals['is_oci_card'] = is_oci_carddatas1
            # passport
            if post.get("passport_image_file"):
                passport_image_fileephoto = request.httprequest.files.getlist('passport_image_file')
                passport = passport_image_fileephoto[0].read()
                passportdatas1 = base64.b64encode(passport).replace(b'\n', b'')
                vals['passport_image'] = passportdatas1
                vals['passport_number'] = post.get("passport_number")

            rec = request.env['kshetragna.registration'].sudo().search([
                ("id", "=", post.get("registration_id"))],limit=1)

            rec.write(vals)
            finance_comments_list = []
            finance_comments_list.append([0, 0, {
                'registration_id': post.get("registration_id"),
                'upload_doc_comments':finance_comments,
                'user_comments':user_comments,
            }])

            rec.write({'finance_comments_ids': finance_comments_list})

            # inherited the model to pass the values to the model from the form#
            return request.render("isha_livingspaces.Kshetragna_document_success_form")

    @http.route('/kshetragna/paymentsuccess', type='http', methods=['GET', 'POST'],
                auth='public', csrf=False)
    def kshetragnapaymentsuccess(self, status="", amount="", transactionId="", requestId="", currency="", errorCode="",
                       errorReason="", errorMsg="", errCode="", errReason="", errMsg="", **post):
        c_value_pr = {'payment_status': status,
                      'amount': amount,
                      'paymenttrackingid': transactionId,
                      'orderid': requestId,
                      'currency': currency,
                      'errorcode': errCode,
                      'errorreason': errReason,
                      'errormsg': errMsg
                      }
        return self.kshetragnapaymentresponse_returntemplate(c_value_pr)


    def kshetragnapaymentresponse_returntemplate(self, c_value):
        """ Update the Payment Transaction on every hits
        :param:@array of values
        @:return: display the msg in HTML template"""
        _logger.info("startinng execution paymentresponse_returntemplate c_value" + str(c_value))
        # errorlog("startinng execution paymentresponse_returntemplate c_value" + str(c_value))
        # request.render("isha_livingspaces.payumoney_status_template", values)
        rec_id = str(c_value['orderid'])
        # below line need to removed - enable only for testing
        # self.put_user_sso_info(rec_id)
        err_msg = ""
        person_name = ""
        support_email = 'info.kshetragna@ishafoundation.org'
        p_registration_rec = request.env['kshetragna.registration'].sudo().search([("order_id", "=", rec_id)])
        if p_registration_rec:
            person_name = p_registration_rec.first_name
        msg = "Transaction_Failure"
        mail_template_id = None
        if c_value.get("payment_status") == "ERROR":
            err_msg = c_value.get("errMsg")

        c_value['transactiontype'] = 'synccallback'
        progreg = request.env['kshetragna.registration'].sudo().search([("order_id", "=", rec_id)])

        if len(progreg) > 0:
            try:
                c_value['programname'] =request.env['ir.config_parameter'].sudo().get_param('kshetragna.programname')
                c_value['first_name'] = progreg.first_name
                c_value['last_name'] = progreg.last_name
                c_value['email'] = progreg.email
                c_value['programtype'] = request.env['ir.config_parameter'].sudo().get_param('kshetragna.programtype')
            except Exception as e:
                pass

        payment_transaction = request.env['kshetragna.participant.paymenttransaction'].sudo().create(c_value)
        values = {"support_email": support_email,
                  "status": c_value.get("payment_status"),
                  "transactionId": c_value.get("paymenttrackingid"),
                  "err_msg": c_value.get("errormsg") if c_value.get("errormsg") else err_msg,
                  "order_id": c_value.get("orderid") if c_value.get("orderid") else "",
                  "error_code": c_value.get("errorcode") if c_value.get("errorcode") else "",
                  "person_name": "   " + person_name,
                  "object": p_registration_rec,
                  "login_email":p_registration_rec.email,
                  "region_support_email_id": "",
                  "is_ishangamone": request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')
                  }
        return request.render("isha_livingspaces.payumoney_status_template", values)

    def kshetragna_api_async_push(self,rec,recdynReg):
        try:
            _logger.info(str(rec))
            rec['createdDate'] = rec['createdDate'].strftime("%Y-%m-%d %H:%M:%S")
            if len(rec) > 0:
                jsnresponse = json.dumps(rec)
                _logger.info('JSON Response : ' + jsnresponse)
            else:
                _logger.info('No data for id - ' + rec)
                raise
            try:
                headersinfo = {
                    "content-Type": "application/x-www-form-urlencoded",
                    "Accept":"application/json",
                    "Cookie":""
                }
                requestconsent_url = request.env['ir.config_parameter'].sudo().get_param('kshetragna.prs_innerengineering_com')
                reqvar = requests.post(requestconsent_url, jsnresponse, headers=headersinfo)
                _logger.info('API triggered to push data')
                _logger.info(reqvar.text)
                if reqvar.status_code == 200:
                    recdynReg.write({'is_usa_bool':True})
            except Exception as ex2:
                _logger.info('In exception kshetragna_api_async_push')
                tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
                _logger.error(tb_ex)

        except Exception as ex2:
            _logger.info('error in kshetragna_api_async_push')
            tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
            _logger.error(tb_ex)
