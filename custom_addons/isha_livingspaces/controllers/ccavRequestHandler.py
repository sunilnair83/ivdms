import json
import logging
import time
import traceback
import sys
import werkzeug

from datetime import datetime
from dateutil.relativedelta import relativedelta
from math import ceil

from odoo import fields, http, _
from odoo.addons.base.models.ir_ui_view import keep_query
from odoo.exceptions import UserError
from odoo.http import request, content_disposition, Response
from odoo.tools import ustr

from Crypto.Cipher import AES
import hashlib
import traceback

from werkzeug import urls
from string import Template

_logger = logging.getLogger(__name__)

class ccavHandler(http.Controller):

	def pad(self, data):
		length = 16 - (len(data) % 16)
		data += chr(length)*length
		return data

	def _unpad(self, data):
		data = data[:-data[-1]]
		return data

	def encrypt(self, plainText,workingKey):
		iv = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f'
		plainText = self.pad(plainText)
		encDigest = hashlib.md5()
		encDigest.update(workingKey.encode('utf-8'))
		enc_cipher = AES.new(encDigest.digest(), AES.MODE_CBC, iv)
		encryptedText = enc_cipher.encrypt(plainText).hex()
		return encryptedText

	def decrypt(self, cipherText, workingKey):
		iv = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f'
		decDigest = hashlib.md5()
		decDigest.update(workingKey.encode('utf-8'))
		encryptedText = bytes.fromhex(cipherText)
		dec_cipher = AES.new(decDigest.digest(), AES.MODE_CBC, iv)
		decryptedText = self._unpad(dec_cipher.decrypt(encryptedText)).decode('utf-8')
		return decryptedText

	def res(self, encResp):
		workingKey = request.env['ir.config_parameter'].sudo().get_param('livingspaces.workingkey')
		decResp = self.decrypt(encResp,workingKey)
		return decResp

	def resdecbyworkingkey(self, encResp, workingKey):
		decResp = self.decrypt(encResp,workingKey)
		return decResp

	def templog(self, logtext):
		request.env['livingspaces.tempdebuglog'].create({
			'logtext': logtext
		})

	@http.route('/livingspaces/registration/ccavResponseHandler', type='http', methods=['GET', 'POST'], auth='public', website=True, csrf=False)
	def ccavResponseHandler(self, **post):
		print('inside ccavResponseHandler')
		_logger.info('inside ccavResponseHandler')
		try:

			self.templog(post['encResp'])
			orderNo = post['orderNo']
			access_databyorder = request.env['livingspaces.registration'].search([('orderid', '=', orderNo)])

			# plainText = self.res(post['encResp'])
			workingKey = request.env['ir.config_parameter'].sudo().get_param('livingspaces.workingkey')
			progtypevar = access_databyorder.programapplied.pgmschedule_programtype
			consultationprog = progtypevar.description.upper().__contains__("ONLINE")
			rejuvenationtype = progtypevar.programcategory.programcategory.upper() == 'REJUVENATION'
			if consultationprog and rejuvenationtype:
				workingKey = request.env['ir.config_parameter'].sudo().get_param('livingspaces.workingkey2')

			plainText = self.resdecbyworkingkey(post['encResp'], workingKey)
			print('encryption completed')
			self.templog(workingKey)
			self.templog(plainText)

			response = plainText.split('&')

			orderstatus = response[3].split('=')[1]
			paymenttrackingid = response[1].split('=')[1]
			orderid = response[0].split('=')[1]
			registrationid = response[26].split('=')[1]
			request_type = response[27].split('=')[1]
			upgrade_access_token = response[28].split('=')[1] if request_type == 'upgrade' else ''
		
			amountpaid = float(response[10].split('=')[1])

			print('response split completed')
			# self.templog('orderstatus: ' + orderstatus)
			# self.templog('orderid: ' + orderid)
			# self.templog('registrationid: ' + registrationid)
			# self.templog('amountpaid: ' + str(amountpaid))

			access_data = request.env['livingspaces.registration'].browse(int(registrationid))

			if (access_data.exists()):
				pass
			else:
				self.templog('record not exists hence calling invalid request')
				return request.render("isha_livingspaces.invalidrequest")

			if (access_data.payment_status != 'Initiated'):
				self.templog('Registration status not initiated hence calling invalid request - ' + str(
					access_data.payment_status))
				return request.render("isha_livingspaces.invalidrequest")

			if (access_data.orderid != orderid):
				self.templog('order id not matched hence calling invalid request - ' + str(access_data.orderid))
				return request.render("isha_livingspaces.invalidrequest")

			if (orderstatus == 'Success' or orderstatus == 'Failure' or orderstatus == 'Aborted'):

				request.env['livingspaces.paymenttransaction'].create({
					'programregistration': access_data.id,
					'orderid': orderid,
					'payment_status': orderstatus,
					'transactiontype': 'Response',
					'amount': amountpaid,
					'paymenttrackingid': paymenttrackingid,
					'billing_name': access_data.billing_name,
					'billing_address': access_data.billing_address,
					'billing_city': access_data.billing_city,
					'billing_state': access_data.billing_state,
					'billing_zip': access_data.billing_zip,
					'billing_country': access_data.billing_country,
					'billing_tel': access_data.billing_tel,
					'billing_email': access_data.billing_email
				})

				if (request_type == 'upgrade'):
					print('upgrade request')
					if (orderstatus == 'Failure' or orderstatus == 'Aborted'):
						access_data.write({
							'payment_status': ''
						})

						count = request.env['livingspaces.registration.transactions'].search_count([('upgrade_access_token','=', upgrade_access_token)])
						if (count != 1):
							return request.render("isha_livingspaces.403")

						access_data = request.env['livingspaces.registration.transactions'].search([('upgrade_access_token','=', upgrade_access_token)])

						if (access_data.isactive == False):
							return request.render("isha_livingspaces.expired")
						
						result = request.env['livingspaces.commonvalidations'].checkPackageUpgradeEligiblity(access_data.programregistration)
						if (result == False):
							return request.render("isha_livingspaces.expired")

						print('get balance amount')
						balanceamount = request.env['livingspaces.payments']._getBalanceAmountForPackageUpgrade(access_data)

						if balanceamount > 0:
							print('1')
							return request.render("isha_livingspaces.packageupgrade", {'object': access_data, 'balanceamount': balanceamount, 'showfailuremessage': True })
						else:
							return request.render("isha_livingspaces.exception")

					elif (orderstatus == 'Success'):
						access_data = request.env['livingspaces.registration.transactions'].search([('upgrade_access_token','=', upgrade_access_token)])
						request.env['livingspaces.payments']._upgradePackage(access_data, amountpaid)
						return request.render("isha_livingspaces.packageupgrade_confirmation", {'object': access_data })

				elif (request_type == 'normal'):
				
					if (orderstatus == 'Failure' or orderstatus == 'Aborted'):
						access_data.write({
							'payment_status': ''
						})
						bal_seats = 0
						mode = 'START'           
						selected_package = request.env['livingspaces.program.schedule.roomdata'].browse(access_data.packageselection.id)
						if selected_package:
							if (access_data.use_emrg_quota == True):
								bal_seats = selected_package.pgmschedule_packageemergencyseats - selected_package.pgmschedule_emergencyseatspaid
							else:
								bal_seats = selected_package.pgmschedule_packagenoofseats - selected_package.pgmschedule_regularseatspaid
						newpackages = request.env['livingspaces.program.schedule.roomdata'].search([('pgmschedule_regularseatsbalance','>',0),('pgmschedule_programname','=',access_data.programapplied.id),('id','!=',selected_package.id)])
						if (bal_seats <= 0):
							print('seats not available')                               
							if newpackages:
								print('Package available')
								mode = 'CHANGE PACKAGE'
							else:
								mode = 'NO PACKAGE'
						termlist = request.env['livingspaces.terms_and_conditions'].search([])
						balanceamount = request.env['livingspaces.payments']._getBalanceAmount(access_data, selected_package)
						return request.render("isha_livingspaces.registration_general", {'object': access_data, 'terms':termlist[0],'countries': request.env['res.country'].search([]), 'packages':newpackages, 'selectedpack':selected_package, 'mode':mode, 'showfailuremessage': True, 'balanceamount': balanceamount})
					elif (orderstatus == 'Success'):
						request.env['livingspaces.payments']._updatePaidStatus(access_data, amountpaid)
						accompanywith = request.env['livingspaces.registration'].search([('programapplied','=',access_data.programapplied.id),('id','!=',access_data.id)])
						return request.render("isha_livingspaces.registration_confirmation", {'object': access_data,'acc_with':accompanywith})
			else:
				self.templog('order status invalid hence calling invalid request - ' + str(orderstatus))
				return request.render("isha_livingspaces.invalidrequest")
		except Exception as e:
			self.templog(str(post))
			self.templog('error in ccavResponseHandler')
			_logger.info('error in ccavResponseHandler')
			tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
			self.templog(tb_ex)
			_logger.info(tb_ex)
			return request.render("isha_livingspaces.exception")

	#@http.route('/livingspaces/registration/ccavRequestHandler/<string:access_token>', type='http', methods=['GET', 'POST'], auth='public', website=True)
	def login(self, request_type, access_token, post, upgrade_access_data):

		try:

			print('inside ccavRequestHandler login')
			_logger.info('inside ccavRequestHandler login')
			self.templog('inside ccavRequestHandler login')
			count = request.env['livingspaces.registration'].search_count([('access_token','=', access_token)])
			if (count != 1):
				return request.render("isha_livingspaces.403")

			access_data = request.env['livingspaces.registration'].search([('access_token','=', access_token)])
			
			print('get balance amount')

			balanceamount = 0
			if (request_type == 'normal'):
				if (access_data.registration_status != 'Payment Link Sent') :
					return request.render("isha_livingspaces.invalidrequest")
				else:
					balanceamount = request.env['livingspaces.payments']._getBalanceAmount(access_data, access_data.packageselection)
			elif (request_type == 'upgrade'):
				print('upgrade request')
				if (access_data.registration_status != 'Paid' and access_data.registration_status != 'Confirmed'):
					return request.render("isha_livingspaces.invalidrequest")
				else:
					balanceamount = request.env['livingspaces.payments']._getBalanceAmountForPackageUpgrade(upgrade_access_data)

			accessCode = request.env['ir.config_parameter'].sudo().get_param('livingspaces.accesscode')
			workingKey = request.env['ir.config_parameter'].sudo().get_param('livingspaces.workingkey')
			p_merchant_id = request.env['ir.config_parameter'].sudo().get_param('livingspaces.merchantid')
			progtypevar = access_data.programapplied.pgmschedule_programtype
			consultationprog = progtypevar.description.upper().__contains__("ONLINE")
			rejuvenationtype = progtypevar.programcategory.programcategory.upper() == 'REJUVENATION'
			if post['billing_country'].upper() == 'INDIA' and consultationprog and rejuvenationtype:
				accessCode = request.env['ir.config_parameter'].sudo().get_param('livingspaces.accesscode2')
				workingKey = request.env['ir.config_parameter'].sudo().get_param('livingspaces.workingkey2')
				p_merchant_id = request.env['ir.config_parameter'].sudo().get_param('livingspaces.merchantid2')

			if post['billing_country'].upper() != 'INDIA' and consultationprog and rejuvenationtype:
				return request.render("isha_livingspaces.invalidrequest")

			p_order_id = ""
			_logger.info('AC , WK ')
			_logger.info(accessCode)
			_logger.info(workingKey)
			self.templog(str(accessCode))
			self.templog(str(workingKey))
			if (access_data.orderid == None or access_data.orderid == "" or access_data.orderid == False):
				# p_order_id = request.env['ir.sequence'].next_by_code('livingspaces.payment.orderid')
				p_order_id =request.env['ir.sequence'].sudo().next_by_code('livingspaces.payment.orderid')
				p_order_id = 'REG-' + str(int(time.time())) + '-' + p_order_id
				print('new order id generated: ' + p_order_id)
				access_data.write({ 
					'orderid': p_order_id
				})
			else:
				p_order_id = access_data.orderid

			p_currency = request.env['ir.config_parameter'].sudo().get_param('livingspaces.currencycode')
			p_amount = str(balanceamount)
			base_url = request.env['ir.config_parameter'].sudo().get_param('livingspaces.ccavcallbackurl')
			p_redirect_url = urls.url_join(base_url, "livingspaces/registration/ccavResponseHandler")
			p_cancel_url = urls.url_join(base_url, "livingspaces/registration/ccavResponseHandler")

			p_language = request.env['ir.config_parameter'].sudo().get_param('livingspaces.language')

			p_billing_name = post['billing_name']
			p_billing_address = post['billing_address']
			p_billing_city = post['billing_city']
			p_billing_state = post['billing_state']
			p_billing_zip = post['billing_zip']
			p_billing_country = post['billing_country']
			p_billing_tel = post['billing_tel']
			p_billing_email = post['billing_email']

			p_delivery_name = ""
			p_delivery_address = ""
			p_delivery_city = ""
			p_delivery_state = ""
			p_delivery_zip = ""
			p_delivery_country = ""
			p_delivery_tel = ""
			p_merchant_param1 = str(access_data.id)
			p_merchant_param2 = request_type
			p_merchant_param3 = upgrade_access_data.upgrade_access_token if request_type == 'upgrade' else ''
			p_merchant_param4 = ""
			p_merchant_param5 = ""
			p_promo_code = ""
			p_customer_identifier = ""

			print ('data collected')

			merchant_data='merchant_id='+p_merchant_id+'&'+'order_id='+p_order_id + '&' + "currency=" + p_currency + '&' + 'amount=' + p_amount+'&'+'redirect_url='+p_redirect_url+'&'+'cancel_url='+p_cancel_url+'&'+'language='+p_language+'&'+'billing_name='+p_billing_name+'&'+'billing_address='+p_billing_address+'&'+'billing_city='+p_billing_city+'&'+'billing_state='+p_billing_state+'&'+'billing_zip='+p_billing_zip+'&'+'billing_country='+p_billing_country+'&'+'billing_tel='+p_billing_tel+'&'+'billing_email='+p_billing_email+'&'+'delivery_name='+p_delivery_name+'&'+'delivery_address='+p_delivery_address+'&'+'delivery_city='+p_delivery_city+'&'+'delivery_state='+p_delivery_state+'&'+'delivery_zip='+p_delivery_zip+'&'+'delivery_country='+p_delivery_country+'&'+'delivery_tel='+p_delivery_tel+'&'+'merchant_param1='+p_merchant_param1+'&'+'merchant_param2='+p_merchant_param2+'&'+'merchant_param3='+p_merchant_param3+'&'+'merchant_param4='+p_merchant_param4+'&'+'merchant_param5='+p_merchant_param5+'&'+'promo_code='+p_promo_code+'&'+'customer_identifier='+p_customer_identifier+'&'
			_logger.info('merchant_data')
			_logger.info(merchant_data)
			self.templog(merchant_data)

			print(merchant_data.encode('utf-8'))

			encryption = self.encrypt(merchant_data,workingKey)

			html = '''\
		<html>
		<head>
			<title>Sub-merchant checkout page</title>
			<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		</head>
		<body>
		<form id="nonseamless" method="post" name="redirect" action="$ccavurl" > 
				<input type="hidden" id="encRequest" name="encRequest" value=$encReq>
				<input type="hidden" name="access_code" id="access_code" value=$xscode>
				<script language='javascript'>document.redirect.submit();</script>
		</form>    
		</body>
		</html>
		'''

			ccavurl = request.env['ir.config_parameter'].sudo().get_param('livingspaces.ccavurl')
			fin = Template(html).safe_substitute(encReq=encryption,xscode=accessCode,ccavurl=ccavurl)
			
			print ('template ready, update the transaction status to db and initiate the payment process')

			access_data.write({ 'registration_agreed': True,
				'billinginfoconfirmation': post.get('billinginfoconfirmation'),
				'billing_name': p_billing_name,
				'billing_address': p_billing_address,
				'billing_city': p_billing_city,
				'billing_state': p_billing_state,
				'billing_zip': p_billing_zip,
				'billing_country': p_billing_country,
				'billing_tel': p_billing_tel,
				'billing_email': p_billing_email,
				'payment_status': 'Initiated',
				'payment_initiated_dt': datetime.now()
			})
			
			if (request_type == 'normal'):
				request.env['livingspaces.payments']._updateSeat(access_data)

			print('billing info updated into registration table')

			request.env['livingspaces.paymenttransaction'].create({
				'programregistration': access_data.id,
				'orderid': p_order_id,
				'billing_name': p_billing_name,
				'billing_address': p_billing_address,
				'billing_city': p_billing_city,
				'billing_state': p_billing_state,
				'billing_zip': p_billing_zip,
				'billing_country': p_billing_country,
				'billing_tel': p_billing_tel,
				'billing_email': p_billing_email,
				'payment_status': 'Initiated',
				'transactiontype': 'Request',
				'amount': p_amount
			})

			print('entry created in payment transaction table')
			
			# for testing
			'''
			html = 
			<html>
			<head>
				<title>Sub-merchant checkout page</title>
				<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
			</head>
			<body>
			<form id="nonseamless" method="post" name="redirect" action="$ccavurl" > 
					<input type="hidden" id="encRequest" name="encRequest" value=$encReq>
					<input type="hidden" name="access_code" id="access_code" value=$xscode>
					<input type="hidden" id="orderid" name="orderid" value=$orderid>
					<input type="hidden" name="orderstatus" id="orderstatus" value=$orderstatus>
					<input type="hidden" name="registrationid" id="registrationid" value=$registrationid>
					<script language='javascript'>document.redirect.submit();</script>
			</form>    
			</body>
			</html>
			'''
			#fin = Template(html).safe_substitute(ccavurl=p_cancel_url, orderid = p_order_id, orderstatus= 'Test', registrationid= str(access_data.id))
			
			return fin
			#return 'ok1'
		except Exception as e:
			self.templog(str(post))
			tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
			_logger.info('error in fin')
			self.templog(tb_ex)
			_logger.info(tb_ex)
			self.templog('exception inside ccavenue request handler fin')
			print('exception inside ccavenue request handler')
			raise e

	@http.route('/livingspaces/registration/process/<string:access_token>', type='http', methods=['GET', 'POST'], auth='public', website=True, csrf=False)
	def registration_process(self, access_token, **post):
		print('inside registration process')
		print(access_token)
		print(self)
		print(post)
		try:
			count = request.env['livingspaces.registration'].search_count([('access_token','=', access_token)])

			if (count != 1):
				return request.render("isha_livingspaces.403")

			access_data = request.env['livingspaces.registration'].search([('access_token','=', access_token)])

			btn_string = 'None'
			new_package = 0

			for rec in post:
				if (rec == 'selected_pack'):
					new_package = post[rec]
					continue
				if (rec == 'button_cancel' and post[rec] == 'cancel'):
					btn_string = 'Cancel'
					continue
				elif (rec == 'button_pay' and post[rec] == 'payment'):
					btn_string = 'Pay'
					continue      
				elif (rec == 'button_onchange' and post[rec] == 'change'):
					btn_string = 'Change'   
					continue

			if btn_string == 'Cancel':
				access_data = self.cancelRegistration(access_token)
				if isinstance(access_data, Response):
					return access_data
			elif btn_string == 'Change':
				print('pack selected  clicked')
				mode = 'CHANGE PACKAGE'
				selected_package = request.env['livingspaces.program.schedule.roomdata'].browse(int(new_package))
				#newpackages = request.env['livingspaces.program.schedule.roomdata'].search([('pgmschedule_regularseatsbalance','>',0),('pgmschedule_programname','=',access_data.programapplied.id)])
				newpackages = request.env['livingspaces.program.schedule.roomdata'].search([('pgmschedule_regularseatsbalance','>',0),('pgmschedule_programname','=',access_data.programapplied.id),('id','!=',selected_package.id)])
				print(selected_package)
				if newpackages:
					print(newpackages)
				else:
					bal_seats = 0
					if selected_package:
						print('selected pack available')
						if (access_data.use_emrg_quota == True):
							bal_seats = selected_package.pgmschedule_packageemergencyseats - selected_package.pgmschedule_emergencyseatspaid
						else:
							bal_seats = selected_package.pgmschedule_packagenoofseats - selected_package.pgmschedule_regularseatspaid

					if (bal_seats <= 0):
						print('seats not available')  
						mode = 'NO PACKAGE'                             
					else:
						mode = 'CHANGE PACKAGE'
				
				balanceamount = request.env['livingspaces.payments']._getBalanceAmount(access_data, selected_package)
				termlist = request.env['livingspaces.terms_and_conditions'].search([])
				return request.render("isha_livingspaces.registration_general", {'object': access_data, 'terms':termlist[0],'countries': request.env['res.country'].search([]), 'packages':newpackages, 'selectedpack':selected_package,'mode':mode, 'balanceamount': balanceamount})
			elif btn_string == 'Pay':
				print('pay')
				mode = 'START'
				if (new_package == 0):
					new_package = access_data.packageselection.id
				#selected_package = request.env['livingspaces.program.schedule.roomdata'].browse(access_data.packageselection.id)
				selected_package = request.env['livingspaces.program.schedule.roomdata'].browse(int(new_package))
				bal_seats = 0
				if selected_package:
					print('selected pack available')
					if (access_data.use_emrg_quota == True):
						bal_seats = selected_package.pgmschedule_packageemergencyseats - selected_package.pgmschedule_emergencyseatspaid
					else:
						bal_seats = selected_package.pgmschedule_packagenoofseats - selected_package.pgmschedule_regularseatspaid

				#newpackages = request.env['livingspaces.program.schedule.roomdata'].search([('pgmschedule_regularseatsbalance','>',0),('pgmschedule_programname','=',access_data.programapplied.id)])
				newpackages = request.env['livingspaces.program.schedule.roomdata'].search([('pgmschedule_regularseatsbalance','>',0),('pgmschedule_programname','=',access_data.programapplied.id),('id','!=',selected_package.id)])
				if (bal_seats <= 0):
					print('seats not available')                               
					if newpackages:
						print('Package available')
						mode = 'CHANGE PACKAGE'
					else:
						mode = 'NO PACKAGE'

					termlist = request.env['livingspaces.terms_and_conditions'].search([])
					balanceamount = request.env['livingspaces.payments']._getBalanceAmount(access_data, selected_package)
					return request.render("isha_livingspaces.registration_general", {'object': access_data, 'terms':termlist[0],'countries': request.env['res.country'].search([]), 'packages':newpackages, 'selectedpack':selected_package,'mode':mode, 'balanceamount': balanceamount})
				else:
					access_data.write({'packageselection': selected_package})
					print('Proceed to pay')
					print(post)

					balanceamount = request.env['livingspaces.payments']._getBalanceAmount(access_data, selected_package)
					if (balanceamount > 0):
						result = self.login('normal', access_token, post, None)
						#return request.redirect('/livingspaces/dummy/%s' %(access_data.access_token))
						return result
					else:
						# no balance to pay incase of change of participant
						# block seat update payment status and redirect to confirmation page
						print('no balance to pay, redirect confirmation')
						request.env['livingspaces.payments']._updateSeat(access_data)
						access_data.write({'registration_agreed': True})
						request.env['livingspaces.payments']._updatePaidStatus(access_data, 0)
						accompanywith = request.env['livingspaces.registration'].search([('programapplied','=',access_data.programapplied.id),('id','!=',access_data.id)])
						return request.render("isha_livingspaces.registration_confirmation", {'object': access_data,'acc_with':accompanywith})

			else:
				return request.render("isha_livingspaces.expired")

		except Exception as e:
			print('inside exception handle of rejuvenation process controller hanlder')
			print(e)
			return request.render("isha_livingspaces.exception")


	@http.route('/livingspaces/registration/packageupgrade/apply/<string:access_token>', type='http', auth='public', website=True, csrf=False)
	def registration_packageupgrade_apply(self, access_token, **post):
		print('inside registration_packageupgrade_apply')
		try:
			count = request.env['livingspaces.registration.transactions'].search_count([('upgrade_access_token','=', access_token)])
			if (count != 1):
				return request.render("isha_livingspaces.403")

			access_data = request.env['livingspaces.registration.transactions'].search([('upgrade_access_token','=', access_token)])

			if (access_data.isactive == False):
				return request.render("isha_livingspaces.expired")
			
			result = request.env['livingspaces.commonvalidations'].checkPackageUpgradeEligiblity(access_data.programregistration)
			if (result == False):
				return request.render("isha_livingspaces.expired")

			#raise payment request

			balanceamount = request.env['livingspaces.payments']._getBalanceAmountForPackageUpgrade(access_data)
			if (balanceamount > 0):
				result = self.login('upgrade', access_data.programregistration.access_token, post, access_data)
				#return request.redirect('/livingspaces/dummy/%s' %(access_data.programregistration.access_token))
				return result
			else:
				return request.render("isha_livingspaces.exception")

		except Exception as e:
			print(e)
			return request.render("isha_livingspaces.exception")


	@http.route('/livingspaces/dummy/<string:access_token>', type='http', methods=['GET', 'POST'], auth='public', website=True, csrf=False)
	def _dummyRedirect(self, access_token):
		
		try:

			access_data = request.env['livingspaces.registration'].search([('access_token','=', access_token)])

			orderid = access_data.orderid
			orderstatus = 'Success'
			amountpaid = request.env['livingspaces.payments']._getBalanceAmount(access_data, access_data.packageselection)
			request_type = 'upgrade'
			upgrade_access_token = ''
			list = request.env['livingspaces.registration.transactions'].search([])
			for rec in list:
				upgrade_access_token = rec.upgrade_access_token
			
			request.env['livingspaces.paymenttransaction'].create({
				'programregistration': access_data.id,
				'orderid': access_data.orderid,
				'payment_status': orderstatus,
				'transactiontype': 'Response',
				'amount': amountpaid
			})
			
			if (access_data.exists()):
				pass
			else:
				self.templog('record not exists hence calling invalid request')
				return request.render("isha_livingspaces.invalidrequest")

			if (access_data.payment_status != 'Initiated'):
				self.templog('registration status not initiated hence calling invalid request - ' + str(access_data.payment_status))
				return request.render("isha_livingspaces.invalidrequest")

			if (access_data.orderid != orderid):
				self.templog('order id not matched hence calling invalid request - ' + str(access_data.orderid))
				return request.render("isha_livingspaces.invalidrequest")

			if (orderstatus == 'Success' or orderstatus == 'Failure' or orderstatus == 'Aborted'):
				if (request_type == 'upgrade'):
					print('upgrade request')
					if (orderstatus == 'Failure' or orderstatus == 'Aborted'):
						access_data.write({
							'payment_status': ''
						})
						count = request.env['livingspaces.registration.transactions'].search_count([('upgrade_access_token','=', upgrade_access_token)])
						if (count != 1):
							return request.render("isha_livingspaces.403")

						access_data = request.env['livingspaces.registration.transactions'].search([('upgrade_access_token','=', upgrade_access_token)])

						if (access_data.isactive == False):
							return request.render("isha_livingspaces.expired")
						
						result = request.env['livingspaces.commonvalidations'].checkPackageUpgradeEligiblity(access_data.programregistration)
						if (result == False):
							return request.render("isha_livingspaces.expired")

						print('get balance amount')
						balanceamount = request.env['livingspaces.payments']._getBalanceAmountForPackageUpgrade(access_data)

						if balanceamount > 0:
							print('1')
							return request.render("isha_livingspaces.packageupgrade", {'object': access_data, 'balanceamount': balanceamount, 'showfailuremessage': True })
						else:
							return request.render("isha_livingspaces.exception")

					elif (orderstatus == 'Success'):
						access_data = request.env['livingspaces.registration.transactions'].search([('upgrade_access_token','=', upgrade_access_token)])
						request.env['livingspaces.payments']._upgradePackage(access_data, amountpaid)
						return request.render("isha_livingspaces.packageupgrade_confirmation", {'object': access_data })
				elif (request_type == 'normal'):
					if (orderstatus == 'Failure' or orderstatus == 'Aborted'):
						access_data.write({
							'payment_status': ''
						})
						bal_seats = 0
						mode = 'START'           
						selected_package = request.env['livingspaces.program.schedule.roomdata'].browse(access_data.packageselection.id)
						if selected_package:
							if (access_data.use_emrg_quota == True):
								bal_seats = selected_package.pgmschedule_packageemergencyseats - selected_package.pgmschedule_emergencyseatspaid
							else:
								bal_seats = selected_package.pgmschedule_packagenoofseats - selected_package.pgmschedule_regularseatspaid
						newpackages = request.env['livingspaces.program.schedule.roomdata'].search([('pgmschedule_regularseatsbalance','>',0),('pgmschedule_programname','=',access_data.programapplied.id),('id','!=',selected_package.id)])
						if (bal_seats <= 0):
							print('seats not available')                               
							if newpackages:
								print('Package available')
								mode = 'CHANGE PACKAGE'
							else:
								mode = 'NO PACKAGE'
						termlist = request.env['livingspaces.terms_and_conditions'].search([])
						balanceamount = request.env['livingspaces.payments']._getBalanceAmount(access_data, selected_package)
						return request.render("isha_livingspaces.registration_general", {'object': access_data, 'terms':termlist[0],'countries': request.env['res.country'].search([]), 'packages':newpackages, 'selectedpack':selected_package, 'mode':mode, 'showfailuremessage': True, 'balanceamount': balanceamount})
					elif (orderstatus == 'Success'):
						request.env['livingspaces.payments']._updatePaidStatus(access_data, amountpaid)
						accompanywith = request.env['livingspaces.registration'].search([('programapplied','=',access_data.programapplied.id),('id','!=',access_data.id)])
						return request.render("isha_livingspaces.registration_confirmation", {'object': access_data,'acc_with':accompanywith})
			else:
				self.templog('order status invalid hence calling invalid request - ' + str(orderstatus))
				return request.render("isha_livingspaces.invalidrequest")
		except Exception as e:
			self.templog(str(e))
			return request.render("isha_livingspaces.exception")

	def cancelRegistration(self, access_token):
		try:
			count = request.env['livingspaces.registration'].search_count([('access_token','=', access_token)])

			if (count != 1):
				return request.render("isha_livingspaces.403")

			access_data = request.env['livingspaces.registration'].search([('access_token','=', access_token)])

			if (access_data.registration_status != 'Pending' and access_data.registration_status != 'Payment Link Sent' and access_data.registration_status != 'Paid' and access_data.registration_status != 'Confirmed') :
				return request.render("isha_livingspaces.invalidrequest")
			
			if (access_data.registration_status == 'Pending' or access_data.registration_status == 'Payment Link Sent'):
				if (access_data.payment_status == 'Initiated'):
					return request.render("isha_livingspaces.paymentinitiated")
				else:
					access_data.write({'registration_status': 'Withdrawn','date_withdrawn':fields.Date.today()})
					return request.render("isha_livingspaces.cancelsuccess")
			elif (access_data.registration_status == 'Paid' or access_data.registration_status == 'Confirmed'):
				print('Cancel with refund')
				access_data = self.cancelRegistrationWithRefund(access_token)
				if isinstance(access_data, Response):
					return access_data

		except Exception as e:
			return request.render("isha_livingspaces.exception")

	def cancelRegistrationWithRefund(self, access_token):
		print('inside Cancel with refund')
		try:
			count = request.env['livingspaces.registration'].search_count([('access_token','=', access_token)])

			if (count != 1):
				return request.render("isha_livingspaces.403")

			access_data = request.env['livingspaces.registration'].search([('access_token','=', access_token)])
			refund_data = request.env['livingspaces.payments']._calculateRefundAmount(access_data, datetime.now())
			
			if (refund_data['days_before'] <= 0):
				return request.render("isha_livingspaces.invalidrequest")
			
			access_data.write({'refund_eligible_ondaysbefore': refund_data['days_before'], 
			'refund_eligible_onpercentage': refund_data['elg_percent'], 'refund_eligible': refund_data['elg_amt']})

			return request.render("isha_livingspaces.registrationcancelwithrefund", {'object': access_data })

		except Exception as e:
			print(e)
			return request.render("isha_livingspaces.exception")
