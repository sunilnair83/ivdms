from collections import namedtuple

from odoo import models, fields, api, exceptions
from datetime import datetime
import datetime
import _datetime
from . import commonmodels
from . import livingspacestype
import time
import datetime

class MegaProgramSchedule(models.Model):
	_name = 'livingspaces.program.schedule'
	_description = 'living spaces Schedule'
	_order = 'pgmschedule_programtype'
	_rec_name = 'pgmschedule_programname'
	_sql_constraints = [('unique_pgmschedule_programname','unique(pgmschedule_programname)','Cannot have duplicate program name, give different name')]

	pgmschedule_programtype = fields.Many2one('living.spaces.type',string = 'Living Spaces Type', required=True)
	pgmschedule_programname = fields.Char(string = 'livingspaces Name', size=200, required=True)
	pgmschedule_startdate = fields.Date(string = 'Start Date', required=True)
	pgmschedule_enddate = fields.Date(string = 'End Date', required=True)
	pgmschedule_registrationopen = fields.Boolean('Registration Open', default=False)
	pgmschedule_publishinwebsite = fields.Boolean('Publish In Website', default=False)

	pgmschedule_session = fields.Many2many('livingspaces.session.timings', relation="livingspaces_relation_program_session", string='Session')

	pgmschedule_roomdata = fields.One2many('livingspaces.program.schedule.roomdata',
	'pgmschedule_programname', string = 'Manage Room', required=True)

	def _getdomain(self):
		return [('lob', '=', self.env.ref('isha_livingspaces.lob_data_rejuvenation').lobname)]
	pgmlocation = fields.Many2one('master.centers', string='Program Center', required=True, domain= _getdomain)

	numberofdays = fields.Integer(string='Number Of Days', size=50)

	pgmschedule_entity = fields.Many2one('livingspaces.entity', string='Entity', store=True)
	pgmschedule_entityisoverridable = fields.Boolean('Is Overridable', store=True)
	pgmschedule_pgmpurpose = fields.Char(string = 'Program Purpose', size=100, store=True)
	pgmschedule_pgmpurposeisoverridable = fields.Boolean('Is Overridable', store=True)
	pgmschedule_paymentgateway =  fields.Many2one('livingspaces.paymentgateway', string='Payment Gateway', store=True)
	pgmschedule_paymentgatewayisoverridable = fields.Boolean('Is Overridable', store=True)

	programtypeflag = fields.Boolean(string="Program Type Flag",default=False)

	# eligibility parameters
	pgmschedule_minimumage = fields.Integer(string='Minimum Age', store=True)
	pgmschedule_minimumageisoverridable = fields.Boolean('Is Overridable', store=True)
	pgmschedule_gender = fields.Selection([('Male','Male'), ('Female','Female'), ('All','All')], string='Gender', store=True)
	pgmschedule_genderisoverridable = fields.Boolean('Is Overridable', store=True)
	pgmschedule_language1 =  fields.Many2one('livingspaces.language1', string='Language', store=True)
	pgmschedule_languageisoverridable = fields.Boolean('Is Overridable', store=True)
#	pgmschedule_prerequisitepractice = fields.Many2many('rejuvenation.practice', relation="ralation_practice_prgschd", string='Prerequisite Practice', store=True)
#	pgmschedule_prerequisitepracticeisoverridable = fields.Boolean('Is Overridable', relation="ralation_practice_prgschd1", store=True)
	pgmschedule_prerequisiteprogram = fields.Many2many('master.livingspaces.category', relation="livingspaces_relation_program_prgschd", string='Prerequisite Practice', store=True)
	pgmschedule_prerequisiteprogramisoverridable = fields.Boolean('Is Overridable', store=True)
	pgmschedule_repetitionallowed = fields.Integer(string='Repetition Allowed', store=True)
	pgmschedule_repetitionallowedisoverridable = fields.Boolean('Is Overridable', store=True)
	pgmschedule_gapbetweenprgm = fields.Integer(string='Gap Between Two Programs', store=True)
	pgmschedule_gapbetweenprgmisoverridable = fields.Boolean('Is Overridable', store=True)

	pgmschedule_practicetaught = fields.Many2many('livingspaces.practice',  relation="livingspaces_relation_practice_taught_schd", string='Practices Thaught', store=True)
	pgmschedule_practicetaughtisoverridable = fields.Boolean('Is Overridable', store=True)

	pgmschedule_totalseats = fields.Integer(string = 'Total Seats',  compute="_compute_totalseats", store=True)
	pgmschedule_totalseatspaid = fields.Integer(string = 'Booked Seats',  compute="_compute_totalpaidseats", store=True)
	pgmschedule_totalbalanceseats = fields.Integer(string = 'Available Seats',  compute="_compute_balanceseats", store=True)

	show_pgm_att = fields.Boolean(compute="_compute_programattflag")
	show_room_allocate = fields.Boolean(compute="_compute_roomallocateflag")

	attcompletionflag = fields.Boolean(string="Attendance Completion Flag",default=False)
	pushtocico = fields.Boolean(string="Push To CICO",default=False)

	pgmschedule_online_code = fields.Char(string = 'Online Registration Code', size=10, required=False)


	@api.model
	def fields_get(self, fields=None):
		fields_to_show = ['pgmschedule_programtype','pgmschedule_programname','pgmschedule_startdate','pgmschedule_enddate','attcompletionflag','pushtocico','pgmschedule_online_code']
		res = super(MegaProgramSchedule, self).fields_get()
		for field in res:
			if (field in fields_to_show):
				res[field]['selectable'] = True	
				res[field]['sortable'] = True
			else:
				res[field]['selectable'] = False
				res[field]['sortable'] = False
		return res

	def open_room_allocate_act(self):
		print('call method')
		return {
            'name':'Allocate Rooms',  
			'context':{'default_active_id': self.id, 'default_programapplied': self.pgmschedule_programname },
            'view_type': 'form',
            'res_model': 'livingspaces.allocaterooms',
            'view_id': False,
            'view_mode': 'form',
            'type': 'ir.actions.act_window',
			'target':'new'
        }

	def open_mark_attendance_act(self):
		return {
            'name':'Program Attendance',  
			'context':{'default_programtype':self.pgmschedule_programtype.id, 'default_active_id': self.id, 'default_programapplied': self.pgmschedule_programname},
            'view_type': 'form',
            'res_model': 'livingspacesattendance',
            'view_id': False,
            'view_mode': 'form',
            'type': 'ir.actions.act_window',
			'target':'new'
        }

	def show_participants(self):
		print('program')
		print(self.id)
		return {
            'name':'Program Participants',  			
            'view_type': 'form',
            'res_model': 'livingspaces.registration',
            'view_id': False,
            'view_mode': 'tree',
            'type': 'ir.actions.act_window',
			'context':{'search_default_program_search':1,'search_default_programapplied':self.id},
        }

	def load_markattendancecompleted(self):
		print('att mark completed')
		isConfirm = True
		#confirmation alert need to be added

		if isConfirm == True:
			nrecs = self.env["livingspaces.registration"].search([('programapplied','=',self.pgmschedule_programname),('attendance_status','!=','Present'),('attendance_status','!=','Absent'),'|',('registration_status','=','Paid'),('registration_status','=','Confirmed')])
			if nrecs.exists():
				print(nrecs)
				for id in nrecs:
					id.write({'attendance_status':'Present'})

			id = self.env['livingspaces.program.schedule'].browse(self.id)
			if id:				
				id.write({'attcompletionflag':True})


	def start_pushtocico(self):
		print('start cico')
		self.env['rejuvenation.cicoapi']._pushRegistrationsToCICO()


	def _compute_programattflag(self):
		for rec in self:
			rec.show_pgm_att = False
			if rec.create_date == False:
				rec.show_pgm_att = False
			else:
				if (rec.pgmschedule_startdate):
					if (rec.pgmschedule_startdate < fields.Date.today()):
						print('less')
						print(rec.pgmschedule_startdate)
						print(fields.Date.today())
						rec.show_pgm_att = True
					elif (rec.pgmschedule_startdate == fields.Date.today()):
						print('equal')
						print(rec.pgmschedule_startdate)
						print(fields.Date.today())
						rec.show_pgm_att = True
					else:
						print('more')
						print(rec.pgmschedule_startdate)
						print(fields.Date.today())
						rec.show_pgm_att = False

					if (rec.show_pgm_att == True):
						parties = self.env["livingspaces.registration"].search([('programapplied.id','=',rec.id),'|',('registration_status','=','Paid'),('registration_status','=','Confirmed')])
						if parties:
							print('yes')
							rec.show_pgm_att = True
						else:
							print('no')
							rec.show_pgm_att = False


	
	def _compute_roomallocateflag(self):
		for rec in self:
			rec.show_room_allocate = False
			if rec.create_date == False:
				rec.show_room_allocate = False
			else:
				if rec.id:
					parties = self.env["livingspaces.registration"].search([('programapplied.id','=',rec.id),'|','|',('registration_status','=','Paid'),('registration_status','=','Confirmed'),('registration_status','=','Cancel Applied')])
					if parties:
						rec.show_room_allocate = True

	def get_core_parameters(self):
		ProgramSchedule = namedtuple('MegaProgramSchedule', [
			'program_title',
			'program_name',
			'start_date',
			'end_date',
			'cat_id',
			'contact_email',
			'contact_phone1',
			'contact_phone2',
			'program_zone',
			'program_venue_name',
			'program_city',
			'program_google_map_link',
			'program_fee',
			'program_address',
		])

		# Notes:
		# cat_id is currently localized in Joomla creation
		# change it to return from this method once the
		# joomla cat id model is in place
		# method below returns blanks

		program_schedule = ProgramSchedule(
			program_title = "{} - Moksha {}".\
				format(self.pgmschedule_programtype.programtype, self.id),
			program_name = self.pgmschedule_programtype.programtype,
			start_date = self.pgmschedule_startdate.isoformat(),
			end_date = self.pgmschedule_enddate.isoformat(),
			cat_id = "",
			contact_email = "ipc.itsupport@ishafoundation.org",
			contact_phone1 = "+914222525345",
			contact_phone2 = "",
			program_zone = "IYC",
			program_venue_name = "Isha Yoga Center",
			program_city = "Coimbatore",
			# map of IYC, Coimbatore
			program_google_map_link = "https://goo.gl/maps/2mkH3jrcPw8Rjuxy9",
			program_fee = "1000",
			# note, this can be changed to get from the schedule model if possible
			program_address = "Isha Yoga Center, Coimbatore"
		)

		return program_schedule

	def compare_pgmschedule_startdate_with_today(self):
		pgmschedule_startdateAfterToday = False
		try:
			past = self.pgmschedule_startdate
			present = _datetime.date.today()
			print (past < present)
			if past < present:
				pgmschedule_startdateAfterToday = False
			else:
				pgmschedule_startdateAfterToday = True
		except Exception as e:
			pgmschedule_startdateAfterToday = False
		return pgmschedule_startdateAfterToday

	@api.constrains('pgmschedule_minimumage')
	def	validate_minimumage(self):
		if (self.pgmschedule_minimumage < 0 or self.pgmschedule_minimumage > 120):
			raise exceptions.ValidationError("Field 'Minimum Age' should be between 0 to 120")

	@api.constrains('pgmschedule_repetitionallowed')
	def	validate_repetitionallowed(self):
		if (self.pgmschedule_repetitionallowed < 0 or self.pgmschedule_repetitionallowed > 999):
			raise exceptions.ValidationError("Field 'Repetition Allowed' should be between 1 to 999")

	@api.constrains('pgmschedule_gapbetweenprgm')
	def	validate_gapbetweenprgm(self):
		if (self.pgmschedule_gapbetweenprgm < 1 or self.pgmschedule_gapbetweenprgm > 12):
			raise exceptions.ValidationError("Field 'Gap Between Two Programs' should be between 1 to 12")

	@api.constrains('pgmschedule_startdate')
	def	validate_pgmschedule_startdate(self):
		diff = self.pgmschedule_enddate - self.pgmschedule_startdate
		if (self.pgmschedule_startdate > self.pgmschedule_enddate):
			raise exceptions.ValidationError("Start date should be lesser then End date")
		elif (self.pgmschedule_programtype.numberofdays > (diff.days + 1)):
			raise exceptions.ValidationError("Difference between dates should be greater than or equal to program type number of days")

	@api.constrains('pgmschedule_enddate')
	def	validate_pgmschedule_enddate(self):
		diff = self.pgmschedule_enddate - self.pgmschedule_startdate
		if (self.pgmschedule_startdate > self.pgmschedule_enddate):
			raise exceptions.ValidationError("End date should be greater then Start date")
		elif (self.pgmschedule_programtype.numberofdays > (diff.days + 1)):
			raise exceptions.ValidationError("Difference between dates should be greater than or equal to program type number of days")

	@api.onchange('pgmschedule_startdate','pgmschedule_enddate')
	def set_program_name(self):
		if (self.pgmschedule_startdate != False and self.pgmschedule_enddate != False):
			self.pgmschedule_programname = self.pgmschedule_programtype.programtype +" - "+ self.pgmschedule_startdate.strftime("%d %b, %Y")+ " - "+ self.pgmschedule_enddate.strftime("%d %b, %Y")
		else:
			self.pgmschedule_programname = ""

	@api.onchange('pgmschedule_programtype')
	def pgmschedule_programtype_change(self):
		if (self.pgmschedule_programtype):
			self.programtypeflag = True
		for rec in self:
			rec.pgmschedule_entity = rec.pgmschedule_programtype.entity
			rec.pgmschedule_pgmpurpose = rec.pgmschedule_programtype.pgmpurpose
			rec.pgmschedule_paymentgateway = rec.pgmschedule_programtype.paymentgateway
			rec.pgmschedule_minimumage = rec.pgmschedule_programtype.minimumage
			rec.pgmschedule_gender = rec.pgmschedule_programtype.gender
			rec.pgmschedule_language1 = rec.pgmschedule_programtype.language1
			rec.pgmschedule_prerequisiteprogram = rec.pgmschedule_programtype.prerequisiteprogram
			rec.pgmschedule_repetitionallowed = rec.pgmschedule_programtype.repetitionallowed
			rec.pgmschedule_gapbetweenprgm = rec.pgmschedule_programtype.gapbetweenprgm
			rec.pgmschedule_entityisoverridable = rec.pgmschedule_programtype.entityisoverridable
			rec.pgmschedule_pgmpurposeisoverridable = rec.pgmschedule_programtype.pgmpurposeisoverridable
			rec.pgmschedule_paymentgatewayisoverridable = rec.pgmschedule_programtype.paymentgatewayisoverridable
			rec.pgmschedule_minimumageisoverridable = rec.pgmschedule_programtype.minimumageisoverridable
			rec.pgmschedule_genderisoverridable = rec.pgmschedule_programtype.genderisoverridable
			rec.pgmschedule_languageisoverridable = rec.pgmschedule_programtype.languageisoverridable
			rec.pgmschedule_prerequisiteprogramisoverridable = rec.pgmschedule_programtype.prerequisiteprogramisoverridable
			rec.pgmschedule_repetitionallowedisoverridable = rec.pgmschedule_programtype.repetitionallowedisoverridable
			rec.pgmschedule_gapbetweenprgmisoverridable = rec.pgmschedule_programtype.gapbetweenprgmisoverridable
			rec.numberofdays = rec.pgmschedule_programtype.numberofdays
			rec.pgmschedule_practicetaught = rec.pgmschedule_programtype.practicetaught
			rec.pgmschedule_practicetaughtisoverridable = rec.pgmschedule_programtype.practicetaughtisoverridable

			packinfo = self.env['livingspaces.package.parameters'].search([('programtype.id','=',rec.pgmschedule_programtype.id)])
			rec.pgmschedule_roomdata = [(5,0,0)]
			pgmschedule_roomdata = []
			for prec in packinfo:
				line = (0, 0, {
					'pgmschedule_packagetype': prec,
					'pgmschedule_noofrooms': 0,
					'pgmschedule_roomnofrom1': 0,
					'pgmschedule_roomnoto': 0,
					'pgmschedule_packagenoofseats': prec.packagenoofseats,
					'pgmschedule_packageemergencyseats': prec.packageemergencyseats
				})
				pgmschedule_roomdata.append(line)
			rec.pgmschedule_roomdata = pgmschedule_roomdata

	@api.depends('pgmschedule_roomdata')
	def _compute_totalseats(self):
		for rec in self:
			tseats = 0
			for x in rec.pgmschedule_roomdata:
				tseats = tseats + x.pgmschedule_packagetotalseats

			rec.pgmschedule_totalseats = tseats

	@api.depends('pgmschedule_roomdata')
	def _compute_totalpaidseats(self):
		for rec in self:
			pseats = 0
			for x in rec.pgmschedule_roomdata:
				pseats = pseats + (x.pgmschedule_regularseatspaid + x.pgmschedule_emergencyseatspaid)

			rec.pgmschedule_totalseatspaid = pseats

	@api.depends('pgmschedule_totalseats','pgmschedule_totalseatspaid')
	def _compute_balanceseats(self):
		for rec in self:
			rec.pgmschedule_totalbalanceseats = rec.pgmschedule_totalseats - rec.pgmschedule_totalseatspaid

	@api.model
	def create(self, vals):
		rec = super(MegaProgramSchedule, self).create(vals)
		if (not rec.pgmschedule_roomdata.exists()):
			raise exceptions.ValidationError("Atleast one package information is required")
		return rec

	def write(self, vals):
		res = super(MegaProgramSchedule, self).write(vals)
		for rec in self:
			if (not rec.pgmschedule_roomdata.exists()):
				raise exceptions.ValidationError("Atleast one package information is required")
		return res

class livingspacesSessions(models.Model):
	_name = 'livingspaces.session.timings'
	_description = 'Mega programs sessions in each schedule'
	_rec_name = 'session_timing'

	session_timing = fields.Char(string = 'Session timing', size=200, required=True)
	pgmschedule_programname = fields.Many2many('livingspaces.program.schedule', string = 'Program schedule')

class ProgramScheduleRoomDetail(models.Model):
	_name = 'livingspaces.program.schedule.roomdata'
	_description = 'Mega Program Schedule Room Detail'
	_rec_name = 'pgmschedule_packagetype'
	_sql_constraints = [('unique_pgmschedule_roomd','unique(pgmschedule_programname,pgmschedule_packagetype)','Cannot have duplicate program schedule room data, give different data')]

	pgmschedule_programname = fields.Many2one('livingspaces.program.schedule',string = 'Program Name')
	pgmschedule_packagetype = fields.Many2one('livingspaces.package.parameters', string = 'Package Name' ,
	domain="[('programtype','=',context.get('default_pgmschedule_programtype'))]", required=True)
	pgmschedule_roomtype = fields.Many2one('livingspaces.roomtype',string = 'Room Type')
	pgmschedule_noofrooms = fields.Integer(string='No. Of Rooms', required=True, default=0)
	pgmschedule_roomnofrom1 = fields.Integer(string='Starting Room No.', required=True, default=0)
	pgmschedule_roomnoto = fields.Integer(string='Ending Room No.', required=True, default=0)

	pgmschedule_packagenoofseats = fields.Integer(string = 'No of Seats',  required=True, default=0)
	pgmschedule_packageemergencyseats = fields.Integer(string = 'Emergency Seats', required=True, default=0)
	pgmschedule_packagetotalseats = fields.Integer(string = 'Total Seats',  compute="_compute_packagetotalseats", store=True, default=0)

	pgmschedule_regularseatspaid = fields.Integer(string = 'Booked', store=True, default=0)
	pgmschedule_regularseatsblocked = fields.Integer(string = 'Blocked', store=True, default=0)
	pgmschedule_regularseatsbalance = fields.Integer(string = 'Available',  compute="_compute_regularseatsbalance", store=True)

	pgmschedule_emergencyseatspaid = fields.Integer(string = 'Emergency Quota Booked', store=True, default=0)
	pgmschedule_emergencyseatsblocked = fields.Integer(string = 'Emergency Quota Blocked', store=True, default=0)
	pgmschedule_emergencyseatsbalance = fields.Integer(string = 'Emergency Quota Available',  compute="_compute_emergencyseatsbalance", store=True)

	# pgmschedule_paidrooms = fields.Integer(string='Booked Rooms', store=True)
	# pgmschedule_balancerooms = fields.Integer(string='Balance Available Rooms', compute="_compute_packagebalancerooms", store=True)

	@api.constrains('pgmschedule_roomnofrom1')
	def	validate_pgmschedule_roomnofrom(self):
		for rec in self:
			if rec.pgmschedule_roomtype.exists():
				if (rec.pgmschedule_roomnofrom1 <= 0 or rec.pgmschedule_roomnofrom1 > 999999999):
					raise exceptions.ValidationError("Field 'Starting Room No.' should be between 1 to 999999999")
			else:
				if (rec.pgmschedule_roomnofrom1 != 0):
					raise exceptions.ValidationError("Select room type, since you have entered room no")

	@api.constrains('pgmschedule_roomnoto')
	def	validate_pgmschedule_roomnoto(self):
		for rec in self:
			if rec.pgmschedule_roomtype.exists():
				if (rec.pgmschedule_roomnoto <= 0 or rec.pgmschedule_roomnoto > 999999999):
					raise exceptions.ValidationError("Field 'Ending Room No.' should be between 1 to 999999999")
			else:
				if (rec.pgmschedule_roomnoto != 0):
					raise exceptions.ValidationError("Select room type, since you have entered room no")

	@api.constrains('pgmschedule_noofrooms')
	def	validate_pgmschedule_noofrooms(self):
		for rec in self:
			if rec.pgmschedule_roomtype.exists():
				diff = ((rec.pgmschedule_roomnoto + 1) - rec.pgmschedule_roomnofrom1)
				if (rec.pgmschedule_noofrooms <= 0 or rec.pgmschedule_noofrooms > 999):
					raise exceptions.ValidationError("Field 'Number of rooms' should be between 1 to 999")
				if (rec.pgmschedule_noofrooms > diff):
					raise exceptions.ValidationError("Difference between Starting Room No. and Ending Room No, should be equal or more the No. of Rooms")
			else:
				if (rec.pgmschedule_noofrooms != 0):
					raise exceptions.ValidationError("Select room type, since you have entered total rooms")


	@api.onchange('pgmschedule_packagetype')
	def pgmschedule_packagetype_change(self):
		for rec in self:
				rec.pgmschedule_packagenoofseats = rec.pgmschedule_packagetype.packagenoofseats
				rec.pgmschedule_packageemergencyseats = rec.pgmschedule_packagetype.packageemergencyseats
				rec.pgmschedule_packagetotalseats = rec.pgmschedule_packagetype.packagetotalseats

	@api.depends('pgmschedule_packagenoofseats','pgmschedule_packageemergencyseats')
	def _compute_packagetotalseats(self):
		for rec in self:
			rec.pgmschedule_packagetotalseats = (rec.pgmschedule_packagenoofseats + rec.pgmschedule_packageemergencyseats)

	@api.depends('pgmschedule_regularseatspaid','pgmschedule_packagenoofseats')
	def _compute_regularseatsbalance(self):
		for rec in self:
			rec.pgmschedule_regularseatsbalance = (rec.pgmschedule_packagenoofseats - (rec.pgmschedule_regularseatspaid + rec.pgmschedule_regularseatsblocked))

	@api.depends('pgmschedule_emergencyseatspaid','pgmschedule_packageemergencyseats')
	def _compute_emergencyseatsbalance(self):
		for rec in self:
			rec.pgmschedule_emergencyseatsbalance = (rec.pgmschedule_packageemergencyseats - (rec.pgmschedule_emergencyseatspaid + rec.pgmschedule_emergencyseatsblocked))

	# @api.depends('pgmschedule_paidrooms','pgmschedule_noofrooms')
	# def _compute_packagebalancerooms(self):
	# 	for rec in self:
	# 		rec.pgmschedule_balancerooms = (rec.pgmschedule_noofrooms - rec.pgmschedule_paidrooms)
	