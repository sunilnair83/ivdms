from odoo import models, fields, api,  _

class livingspacesettings(models.TransientModel):
    _inherit = 'res.config.settings'

    sms_provider_base_url = fields.Char(string='SMS Gateway URL', size=300, default="https://api.smscountry.com/SMSCwebservice_bulk.aspx")
    sms_provider_username = fields.Char(string='SMS Provider Username', size=50, default="eastrco")
    sms_provider_password = fields.Char(string='SMS Provider Password', size=50, default="", password="True")
    sms_provider_sender_id = fields.Char(string='SMS Provider Sender ID', size=50, default="ISHATD")
    # waitinglist_remindercount = fields.Char(string='Reminder Count', size=50)
    # waitinglist_remindermonths = fields.Char(string='Reminder Months', size=50)
    #
    # cico_url = fields.Char(string='CICO URL', size=300)
    # cico_apikey = fields.Char(string='CICO API Key', size=100)
    # cico_src = fields.Char(string='CICO Source', size=100)
    # cico_dest = fields.Char(string='CICO Destination', size=100)
    # cico_method = fields.Char(string='CICO Method', size=100)
    #
    # ccavurl = fields.Char(string='CCAV URL', size=200)
    # merchantid = fields.Char(string='Merchant ID', size=50)
    # accesscode = fields.Char(string='Access Code', size=50)
    # workingkey = fields.Char(string='Working Key', size=50)
    # merchantid2 = fields.Char(string='Merchant ID2', size=50)
    # accesscode2 = fields.Char(string='Access Code2', size=50)
    # workingkey2 = fields.Char(string='Working Key2', size=50)
    # language = fields.Char(string='Language', size=10)
    # currencycode = fields.Char(string='Currency Code', size=10)
    # ccavstatuscheckurl = fields.Char(string='CCAV Status Check URL', size=200)
    # statusaccesscode = fields.Char(string='Access Code', size=50)
    # statusworkingkey = fields.Char(string='Working Key', size=50)
    # ccavcallbackurl = fields.Char(string='Call back URL', size=200)
    #
    # ereceipturl = fields.Char(string='E-Receipt URL', size=200)
    # ereceipttreasurer = fields.Char(string='E-Receipt Treasurer', size=100)
    # ereceiptsecretkey = fields.Char(string='E-Receipt Secret Key', size=300)
    # ereceiptwho = fields.Char(string='E-Receipt Who', size=100)
    # ereceiptcenter = fields.Char(string='E-Receipt center', size=100)
    # ereceiptmode = fields.Char(string='E-Receipt Mode', size=100)
    #
    # templogdays = fields.Char(string='Temp Log Days')
    #
    # joomlaurl = fields.Char(string='Joomla URL', size=200)
    # joomlauid = fields.Char(string='Joomla UID', size=100)
    # joomlakey = fields.Char(string='Joomla  Key', size=300)
    #
    def set_values(self):
        res = super(livingspacesettings, self).set_values()
        self.env['ir.config_parameter'].set_param('livingspaces.sms_provider_base_url', self.sms_provider_base_url)
        self.env['ir.config_parameter'].set_param('livingspaces.sms_provider_username', self.sms_provider_username)
        self.env['ir.config_parameter'].set_param('livingspaces.sms_provider_password', self.sms_provider_password)
        self.env['ir.config_parameter'].set_param('livingspaces.sms_provider_sender_id', self.sms_provider_sender_id)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.waitinglist_remindercount', self.waitinglist_remindercount)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.waitinglist_remindermonths', self.waitinglist_remindermonths)
    #
    #     self.env['ir.config_parameter'].set_param('rejuvenation.cico_url', self.cico_url)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.cico_apikey', self.cico_apikey)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.cico_src', self.cico_src)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.cico_dest', self.cico_dest)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.cico_method', self.cico_method)
    #
    #     self.env['ir.config_parameter'].set_param('rejuvenation.ccavurl', self.ccavurl)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.merchantid', self.merchantid)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.accesscode', self.accesscode)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.workingkey', self.workingkey)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.merchantid2', self.merchantid2)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.accesscode2', self.accesscode2)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.workingkey2', self.workingkey2)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.language', self.language)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.currencycode', self.currencycode)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.ccavstatuscheckurl', self.ccavstatuscheckurl)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.statusaccesscode', self.statusaccesscode)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.statusworkingkey', self.statusworkingkey)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.ccavcallbackurl', self.ccavcallbackurl)
    #
    #     self.env['ir.config_parameter'].set_param('rejuvenation.ereceipturl', self.ereceipturl)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.ereceipttreasurer', self.ereceipttreasurer)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.ereceiptsecretkey', self.ereceiptsecretkey)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.ereceiptwho', self.ereceiptwho)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.ereceiptcenter', self.ereceiptcenter)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.ereceiptmode', self.ereceiptmode)
    #
    #     self.env['ir.config_parameter'].set_param('rejuvenation.templogdays', self.templogdays)
    #
    #     self.env['ir.config_parameter'].set_param('rejuvenation.joomlaurl', self.joomlaurl)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.joomlauid', self.joomlauid)
    #     self.env['ir.config_parameter'].set_param('rejuvenation.joomlakey', self.joomlakey)
    #
        return res
    #
    # @api.model
    def get_values(self):
         res = super(livingspacesettings, self).get_values()
         rejsudo = self.env['ir.config_parameter'].sudo()
         sms_provider_base_url = rejsudo.get_param('livingspaces.sms_provider_base_url')
         sms_provider_username = rejsudo.get_param('livingspaces.sms_provider_username')
         sms_provider_password = rejsudo.get_param('livingspaces.sms_provider_password')
         sms_provider_sender_id = rejsudo.get_param('livingspaces.sms_provider_sender_id')
    #     waitinglist_remindercount = rejsudo.get_param('rejuvenation.waitinglist_remindercount')
    #     waitinglist_remindermonths = rejsudo.get_param('rejuvenation.waitinglist_remindermonths')
    #
    #     cico_url = rejsudo.get_param('rejuvenation.cico_url')
    #     cico_apikey = rejsudo.get_param('rejuvenation.cico_apikey')
    #     cico_src = rejsudo.get_param('rejuvenation.cico_src')
    #     cico_dest = rejsudo.get_param('rejuvenation.cico_dest')
    #     cico_method = rejsudo.get_param('rejuvenation.cico_method')
    #
    #     ccavurl = rejsudo.get_param('rejuvenation.ccavurl')
    #     merchantid = rejsudo.get_param('rejuvenation.merchantid')
    #     accesscode = rejsudo.get_param('rejuvenation.accesscode')
    #     workingkey = rejsudo.get_param('rejuvenation.workingkey')
    #     merchantid2 = rejsudo.get_param('rejuvenation.merchantid2')
    #     accesscode2 = rejsudo.get_param('rejuvenation.accesscode2')
    #     workingkey2 = rejsudo.get_param('rejuvenation.workingkey2')
    #     language = rejsudo.get_param('rejuvenation.language')
    #     currencycode = rejsudo.get_param('rejuvenation.currencycode')
    #     ccavstatuscheckurl = rejsudo.get_param('rejuvenation.ccavstatuscheckurl')
    #     statusaccesscode = rejsudo.get_param('rejuvenation.statusaccesscode')
    #     statusworkingkey = rejsudo.get_param('rejuvenation.statusworkingkey')
    #     ccavcallbackurl = rejsudo.get_param('rejuvenation.ccavcallbackurl')
    #
    #     ereceipturl = rejsudo.get_param('rejuvenation.ereceipturl')
    #     ereceipttreasurer = rejsudo.get_param('rejuvenation.ereceipttreasurer')
    #     ereceiptsecretkey = rejsudo.get_param('rejuvenation.ereceiptsecretkey')
    #     ereceiptwho = rejsudo.get_param('rejuvenation.ereceiptwho')
    #     ereceiptcenter = rejsudo.get_param('rejuvenation.ereceiptcenter')
    #     ereceiptmode = rejsudo.get_param('rejuvenation.ereceiptmode')
    #
    #     templogdays = rejsudo.get_param('rejuvenation.templogdays')
    #
    #     joomlaurl = rejsudo.get_param('rejuvenation.joomlaurl')
    #     joomlauid = rejsudo.get_param('rejuvenation.joomlauid')
    #     joomlakey = rejsudo.get_param('rejuvenation.joomlakey')
    #
         res.update(
             sms_provider_base_url=sms_provider_base_url,
             sms_provider_username=sms_provider_username,
             sms_provider_password=sms_provider_password,
             sms_provider_sender_id=sms_provider_sender_id
    #         waitinglist_remindercount=waitinglist_remindercount,
    #         waitinglist_remindermonths=waitinglist_remindermonths,
    #         cico_url=cico_url,
    #         cico_apikey=cico_apikey,
    #         cico_src=cico_src,
    #         cico_dest=cico_dest,
    #         cico_method=cico_method,
    #         ccavurl=ccavurl,
    #         merchantid=merchantid,
    #         accesscode=accesscode,
    #         workingkey=workingkey,
    #         merchantid2=merchantid2,
    #         accesscode2=accesscode2,
    #         workingkey2=workingkey2,
    #         language=language,
    #         currencycode=currencycode,
    #         ccavstatuscheckurl = ccavstatuscheckurl,
    #         statusaccesscode=statusaccesscode,
    #         statusworkingkey=statusworkingkey,
    #         ccavcallbackurl=ccavcallbackurl,
    #         ereceipturl=ereceipturl,
    #         ereceipttreasurer=ereceipttreasurer,
    #         ereceiptsecretkey=ereceiptsecretkey,
    #         ereceiptwho=ereceiptwho,
    #         ereceiptcenter=ereceiptcenter,
    #         ereceiptmode=ereceiptmode,
    #         templogdays=templogdays,
    #         joomlaurl=joomlaurl,
    #         joomlauid=joomlauid,
    #         joomlakey=joomlakey
         )
    #
         return res
