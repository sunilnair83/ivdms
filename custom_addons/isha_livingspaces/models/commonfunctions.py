import base64
import json
from datetime import date, timedelta, datetime

import requests
import cryptography
import cffi
import _cffi_backend
import jwt
import logging

from odoo import api, models
from odoo.exceptions import UserError

#from ..services.joomla import sync_joomla

DEFAULT_ENDPOINT = 'http://api.smscountry.com'
_logger = logging.getLogger(__name__)


class SMS(models.AbstractModel):
    _name = 'livingspaces.smsapi'
    _description = 'Isha Livingspaces SMS API'

    @api.model
    def _send_smcountry_sms(self, data):

        url = self.env['ir.config_parameter'].sudo().get_param('livingspaces.sms_provider_base_url')
        params = {'User': self.env['ir.config_parameter'].sudo().get_param('livingspaces.sms_provider_username'),
                  'passwd': self.env['ir.config_parameter'].sudo().get_param('livingspaces.sms_provider_password'),
                  'mobilenumber': data['number'],
                  'message': data['message'],
                  'sid': self.env['ir.config_parameter'].sudo().get_param('livingspaces.sms_provider_sender_id'),
                  'mtype': 'N', 'DR': 'Y'}
        try:
            result = requests.get(url, params)
        except Exception as e:
            self._raise_query_error(e)
        if 'Invalid Password' in result.text:
            self._raise_query_error('Unexpected issue. Please contact Administrator.')
        if 'ERROR:' in result.text:
            self._raise_query_error(result.text)
        return True

    def _raise_query_error(self, error):
        print('Error with sending sms: %s' % error)
        # raise ValueError('Error with sending sms: %s' % error)


class Notification(models.AbstractModel):
    _name = 'livingspaces.notification'
    _description = 'Isha Livingspaces Notification'

    @api.model
    def _sendRegistrationEmailSMSCore(self, rec):
        eventdetail = self.env['livingspaces.notificationtemplate'].search(
            [('notificationevent.notificationevent', '=', 'Registration')])
        if (eventdetail.sendemail == True and rec.registration_email_send == False):
            print('sending email enabled')
            templateid = eventdetail.emailtemplateid.id
            self.env['mail.template'].browse(templateid).send_mail(rec.id, force_send=True)
            rec.write({
                'registration_email_send': True
            })
            print('sending email completed')
        if (eventdetail.sendsms == True and rec.registration_sms_send == False and rec.country.name == 'India'):
            print('sending sms enabled')
            templateid = eventdetail.smstemplateid.id
            smstemplate = self.env['sms.template'].browse(templateid)
            subject = self.env['sms.template']._render_template(smstemplate.body,
                                                                'program.registration', rec.id)
            print(subject)
            self.env['livingspaces.smsapi']._send_smcountry_sms({
                'number': rec.phone,
                'message': subject
            })
            rec.write({
                'registration_sms_send': True
            })
            print('sending sms completed')

    @api.model
    def _sendRegistrationPartiallyCompletedFormEmailSMS(self, rec):
        try:
            _logger.info("Inside _sendRegistrationPartiallyCompletedFormEmailSMS")
            eventdetail = self.env['livingspaces.notificationtemplate'].search(
                [('programtype.id', '=', rec.participant_programtype.id),
                 ('notificationevent.notificationevent', '=', 'PartialRegistration')])
            _logger.info(str(eventdetail))
            _logger.info(str(rec))
            if (eventdetail.sendemail == True and rec.incompleteregistration_email_send == False):
                print('sending email enabled')
                _logger.info('sending email enabled')
                templateid = eventdetail.emailtemplateid.id
                _logger.info('sending email templateid')
                _logger.info(str(templateid))
                self.env['mail.template'].browse(templateid).send_mail(rec.id, force_send=True)
                rec.write({
                    'incompleteregistration_email_send': True
                })
                print('sending email completed')
                _logger.info('sending email completed')
            if (
                    eventdetail.sendsms == True and rec.incompleteregistration_sms_send == False and rec.country.name == 'India'):
                print('sending sms enabled')
                _logger.info('sending sms enabled')
                templateid = eventdetail.smstemplateid.id
                _logger.info('sending sms templateid')
                _logger.info(str(templateid))
                smstemplate = self.env['sms.template'].browse(templateid)
                subject = self.env['sms.template']._render_template(smstemplate.body,
                                                                    'program.registration', rec.id)
                print(subject)
                _logger.info(str(subject))
                self.env['livingspaces.smsapi']._send_smcountry_sms({
                    'number': rec.phone,
                    'message': subject
                })
                rec.write({
                    'incompleteregistration_sms_send': True
                })
                print('sending sms completed')
                _logger.info('sending sms completed')
        except Exception:
            _logger.error(
                "Error caught during request creation", exc_info=True)

    @api.model
    def _sendRegistrationRejectionEmailCore(self, rec):
        eventdetail = self.env['livingspaces.notificationtemplate'].search(
            [('programtype.id', '=', rec.participant_programtype.id),
             ('notificationevent.notificationevent', '=', 'Rejection')])
        if (eventdetail.sendemail == True and rec.rejection_email_send == False):
            print('sending email enabled')
            templateid = eventdetail.emailtemplateid.id
            self.env['mail.template'].browse(templateid).send_mail(rec.id, force_send=True)
            rec.write({
                'rejection_email_send': True
            })
            print('sending email completed')
        if (eventdetail.sendsms == True and rec.rejection_sms_send == False and rec.country.name == 'India'):
            print('sending sms enabled')
            templateid = eventdetail.smstemplateid.id
            smstemplate = self.env['sms.template'].browse(templateid)
            subject = self.env['sms.template']._render_template(smstemplate.body,
                                                                'livingspaces.registration', rec.id)
            print(subject)
            self.env['livingspaces.smsapi']._send_smcountry_sms({
                'number': rec.phone,
                'message': subject
            })
            rec.write({
                'rejection_sms_send': True
            })
            print('sending sms completed')

    @api.model
    def _sendRegistrationHAAdditionalInfoEmailCore(self, rec):
        eventdetail = self.env['livingspaces.notificationtemplate'].search(
            [('notificationevent.notificationevent', '=', 'HA Additional Info')])
        if (eventdetail.sendemail == True and rec.harequest_email_send == False):
            print('sending email enabled')
            templateid = eventdetail.emailtemplateid.id
            self.env['mail.template'].browse(templateid).send_mail(rec.id, force_send=True)
            rec.write({
                'harequest_email_send': True
            })
            print('sending email completed')
        if (eventdetail.sendsms == True and rec.harequest_sms_send == False and rec.country.name == 'India'):
            print('sending sms enabled')
            templateid = eventdetail.smstemplateid.id
            smstemplate = self.env['sms.template'].browse(templateid)
            subject = self.env['sms.template']._render_template(smstemplate.body,
                                                                'livingspaces.registration', rec.id)
            print(subject)
            self.env['livingspaces.smsapi']._send_smcountry_sms({
                'number': rec.phone,
                'message': subject
            })
            rec.write({
                'harequest_sms_send': True
            })
            print('sending sms completed')

    @api.model
    def _sendPaymentLinkEmailCore(self, rec):
        eventdetail = self.env['livingspaces.notificationtemplate'].search(
            [('programtype.id', '=', rec.participant_programtype.id),
             ('notificationevent.notificationevent', '=', 'Payment Link Send')])
        if (eventdetail.sendemail == True and rec.paymentlink_email_send == False):
            print('sending email enabled')
            templateid = eventdetail.emailtemplateid.id
            self.env['mail.template'].browse(templateid).send_mail(rec.id, force_send=True)
            rec.write({
                'paymentlink_email_send': True,
                'registration_status': 'Payment Link Sent'
            })
            print('sending email completed')
        if (eventdetail.sendsms == True and rec.paymentlink_sms_send == False and rec.country.name == 'India'):
            print('sending sms enabled')
            templateid = eventdetail.smstemplateid.id
            smstemplate = self.env['sms.template'].browse(templateid)
            subject = self.env['sms.template']._render_template(smstemplate.body,
                                                                'livingspaces.registration', rec.id)
            print(subject)
            self.env['livingspaces.smsapi']._send_smcountry_sms({
                'number': rec.phone,
                'message': subject
            })
            rec.write({
                'paymentlink_sms_send': True
            })
            print('sending sms completed')

    @api.model
    def _sendChangeOfParticipantEmailCore(self, rec):
        eventdetail = self.env['livingspaces.notificationtemplate'].search(
            [('programtype.id', '=', rec.participant_programtype.id),
             ('notificationevent.notificationevent', '=', 'Change Of Participant Request')])
        if (eventdetail.sendemail == True):
            print('sending email enabled')
            templateid = eventdetail.emailtemplateid.id
            self.env['mail.template'].browse(templateid).send_mail(rec.id, force_send=True)
            print('sending email completed')
        if (eventdetail.sendsms == True and rec.country.name == 'India'):
            print('sending sms enabled')
            templateid = eventdetail.smstemplateid.id
            smstemplate = self.env['sms.template'].browse(templateid)
            subject = self.env['sms.template']._render_template(smstemplate.body,
                                                                'livingspaces.registration', rec.id)
            print(subject)
            self.env['livingspaces.smsapi']._send_smcountry_sms({
                'number': rec.phone,
                'message': subject
            })
            print('sending sms completed')

    #
    @api.model
    def _sendWaitingListScheduledProgram(self):

        print('inside _sendWaitingListScheduledProgram')

        foundrec = 0

        waitinglistcount = int(
            self.env['ir.config_parameter'].sudo().get_param('livingspaces.waitinglist_remindercount'))
        waitinglistmonths = int(
            self.env['ir.config_parameter'].sudo().get_param('livingspaces.waitinglist_remindermonths'))

        if (waitinglistcount == 0):
            waitinglistcount = 3

        if (waitinglistmonths == 0):
            waitinglistmonths = 18

        print('waitinglistcount')
        print(waitinglistcount)

        print('waitinglistmonths')
        print(waitinglistmonths)

        rejlist = self.env['livingspaces.registration'].search([('registration_status', '=', 'Add To Waiting List'),
                                                           ('waitinglist_email_count', '<', waitinglistcount),
                                                           ('waitinglist_period_inmonth', '<=', waitinglistmonths)])

        print(rejlist)

        for rec in rejlist:

            foundrec = 1
            waitinglist_period_inmonth = 0

            if (rec.waitinglist_date):
                end_date = date.today()
                start_date = rec.waitinglist_date
                waitinglist_period_inmonth = (((end_date.year - start_date.year) * 12) +
                                              (end_date.month - start_date.month))
            else:
                waitinglist_period_inmonth = 0

            update = 0

            if (waitinglist_period_inmonth <= waitinglistmonths):
                if rec.waitinglist_last_communicate_sch.exists():
                    eventdetail = self.env['livingspaces.program.schedule'].search([
                        ('pgmschedule_programtype.id', '=', rec.participant_programtype.id),
                        ('pgmschedule_startdate', '>', date.today()),
                        ('id', '>', rec.waitinglist_last_communicate_sch.id),
                        ('pgmschedule_registrationopen', '=', True)])
                else:
                    eventdetail = self.env['livingspaces.program.schedule'].search([
                        ('pgmschedule_programtype.id', '=', rec.participant_programtype.id),
                        ('pgmschedule_startdate', '>', date.today()),
                        ('id', '!=', rec.programapplied.id),
                        ('pgmschedule_registrationopen', '=', True)])

                for eventd in eventdetail:
                    update = 1
                    rec.write({
                        'waitinglist_email_count': (rec.waitinglist_email_count + 1),
                        'waitinglist_last_communicate_sch': eventd,
                        'waitinglist_period_inmonth': waitinglist_period_inmonth,
                        'waitinglist_last_communicate_dt': date.today(),
                        'waitinglist_email_send': False,
                        'waitinglist_sms_send': False
                    })

                    eventdetail1 = self.env['livingspaces.notificationtemplate'].search(
                        [('programtype.id', '=', rec.participant_programtype.id),
                         ('notificationevent.notificationevent', '=', 'Future Programs')])

                    if (eventdetail1.sendemail == True):
                        print('sending email enabled')
                        templateid = eventdetail1.emailtemplateid.id
                        self.env['mail.template'].browse(templateid).send_mail(rec.id, force_send=True)
                        rec.write({
                            'waitinglist_email_send': True
                        })
                        print('sending email completed')
                    if (eventdetail1.sendsms == True and rec.country.name == 'India'):
                        print('sending sms enabled')
                        templateid = eventdetail1.smstemplateid.id
                        smstemplate = self.env['sms.template'].browse(templateid)
                        subject = self.env['sms.template']._render_template(smstemplate.body,
                                                                            'livingspaces.registration', rec.id)
                        print(subject)
                        self.env['livingspaces.smsapi']._send_smcountry_sms({
                            'number': rec.phone,
                            'message': subject
                        })
                        rec.write({
                            'waitinglist_sms_send': True
                        })
                        print('sending sms completed')

                    break

            if (update == 0):
                rec.write({
                    'waitinglist_period_inmonth': waitinglist_period_inmonth
                })

        if foundrec == 1:
            print('futureprogram: completed sending email and sms completed for all request')
        else:
            print('futureprogram: no records found, completed..')

    #
    @api.model
    def _updateAndSendProgramConfirmation(self):

        print('inside _updateAndSendProgramConfirmation')

        foundrec = 0

        plist1 = self.env['livingspaces.registration'].search(
            [('registration_status', '=', 'Paid'), ('ereceiptgenerated', '=', True)])
        plist2 = self.env['livingspaces.registration'].search(
            [('registration_status', '=', 'Paid'), ('amount_paid', '=', 0), ('ereceiptgenerated', '=', False)])

        plist = plist1 | plist2

        for rec in plist:

            foundrec = 1

            eventdetail = self.env['livingspaces.notificationtemplate'].search(
                [('programtype.id', '=', rec.participant_programtype.id),
                 ('notificationevent.notificationevent', '=', 'Confirmation')])

            if (eventdetail.sendemail == True):
                print('sending email enabled')
                templateid = eventdetail.emailtemplateid.id
                self.env['mail.template'].browse(templateid).send_mail(rec.id, force_send=True)
                rec.write({
                    'confirmation_email_send': True
                })
                print('sending email completed')
            if (eventdetail.sendsms == True and rec.country.name == 'India'):
                print('sending sms enabled')
                templateid = eventdetail.smstemplateid.id
                smstemplate = self.env['sms.template'].browse(templateid)
                subject = self.env['sms.template']._render_template(smstemplate.body,
                                                                    'livingspaces.registration', rec.id)
                print(subject)
                self.env['livingspaces.smsapi']._send_smcountry_sms({
                    'number': rec.phone,
                    'message': subject
                })
                rec.write({
                    'confirmation_sms_send': True
                })
                print('sending sms completed')

            rec.write({
                'registration_status': 'Confirmed',
                'last_modified_dt': datetime.now()
            })

        if foundrec == 1:
            print('confirmation update: completed sending email and sms completed for all request')
        else:
            print('confirmation update: no records found, completed..')

    #
    @api.model
    def _sendCancelAppliedConfirmation(self):

        print('inside _sendCancelAppliedConfirmation')
        foundrec = 0
        plist = self.env['livingspaces.registration'].search([('cancelapplied_emailsms_pending', '=', True)])

        for rec in plist:
            foundrec = 1
            self.env['livingspaces.notification']._sendCancellationMailAndSMS(rec.id, 'Cancel Applied')
            rec.write({
                'cancelapplied_emailsms_pending': False
            })

        if foundrec == 1:
            print('_sendCancelAppliedConfirmation : completed sending email and sms completed for all request')
        else:
            print('_sendCancelAppliedConfirmation : no records found, completed..')

    #
    @api.model
    def _sendCOPAppliedConfirmation(self):

        print('inside _sendCOPAppliedConfirmation')
        foundrec = 0
        plist = self.env['livingspaces.registration'].search([('copapplied_emailsms_pending', '=', True)])

        for rec in plist:
            foundrec = 1
            self.env['livingspaces.notification']._sendGlobalNotification(rec, 'Change Of Participant')
            rec.write({
                'copapplied_emailsms_pending': False
            })

        if foundrec == 1:
            print('_sendCOPAppliedConfirmation : completed sending email and sms completed for all request')
        else:
            print('_sendCOPAppliedConfirmation : no records found, completed..')

    #
    @api.model
    def _sendAddToWaitListEmailCore(self, rec):
        eventdetail = self.env['livingspaces.notificationtemplate'].search(
            [('programtype.id', '=', rec.participant_programtype.id),
             ('notificationevent.notificationevent', '=', 'Add to Waitlist')])
        if (eventdetail.sendemail == True and rec.addtowaitlist_email_send == False):
            print('sending email enabled')
            templateid = eventdetail.emailtemplateid.id
            self.env['mail.template'].browse(templateid).send_mail(rec.id, force_send=True)
            rec.write({
                'addtowaitlist_email_send': True
            })
            print('sending email completed')
        if (eventdetail.sendsms == True and rec.addtowaitlist_sms_send == False and rec.country.name == 'India'):
            print('sending sms enabled')
            templateid = eventdetail.smstemplateid.id
            smstemplate = self.env['sms.template'].browse(templateid)
            subject = self.env['sms.template']._render_template(smstemplate.body,
                                                                'livingspaces.registration', rec.id)
            print(subject)
            self.env['livingspaces.smsapi']._send_smcountry_sms({
                'number': rec.phone,
                'message': subject
            })
            rec.write({
                'addtowaitlist_sms_send': True
            })
            print('sending sms completed')

    @api.model
    def _sendInquiryAcknowledgement(self, rec):

        print('inside _sendInquiryAcknowledgement')

        foundrec = 0

        if rec:

            foundrec = 1

            eventdetail = self.env['livingspaces.notificationtemplate'].search(
                [('programtype.id', '=', rec.selected_programtype.id),
                 ('notificationevent.notificationevent', '=', 'Inquiry')])

            if (eventdetail.sendemail == True and rec.ack_email_send == False):
                print('sending email enabled')
                templateid = eventdetail.emailtemplateid.id
                self.env['mail.template'].browse(templateid).send_mail(rec.id, force_send=True)
                rec.write({
                    'ack_email_send': True
                })
                print('sending email completed')
            if (eventdetail.sendsms == True and rec.ack_sms_send == False and rec.mobile and rec.countrycode == '91'):
                print('sending sms enabled')
                templateid = eventdetail.smstemplateid.id
                smstemplate = self.env['sms.template'].browse(templateid)
                subject = self.env['sms.template']._render_template(smstemplate.body,
                                                                    'livingspaces.inquiry', rec.id)
                print(subject)
                self.env['livingspaces.smsapi']._send_smcountry_sms({
                    'number': rec.mobile,
                    'message': subject
                })
                rec.write({
                    'ack_sms_send': True
                })
                print('sending sms completed')

        if foundrec == 1:
            print('acknowledgement update: completed sending email and sms completed for all request')
        else:
            print('acknowledgement update: no records found, completed..')

    #
    @api.model
    def _sendCancellationMailAndSMS(self, party_id, party_status):
        print('inside _sendCancellationAppliedMailAndSMS')
        print(party_id)

        foundrec = 0

        # plist = self.env['livingspaces.registration'].search([('registration_status','=','Cancel Approved'),'|',('cancelapproved_email_send','=',False),('cancelapplied_sms_send','=',False)])
        rec = self.env['livingspaces.registration'].browse(party_id)
        # for rec in plist:
        if rec:
            print('rec found')
            print(rec.first_name)

            eventdetail = self.env['livingspaces.notificationtemplate'].search(
                [('programtype.id', '=', rec.participant_programtype.id),
                 ('notificationevent.notificationevent', '=', party_status)])

            trigger_email = False
            trigger_sms = False

            if (
                    eventdetail.sendemail == True and rec.cancelapplied_email_send == False and party_status == 'Cancel Applied'):
                trigger_email = True
            elif (
                    eventdetail.sendsms == True and rec.cancelapplied_sms_send == False and party_status == 'Cancel Applied' and rec.country.name == 'India' and rec.phone and len(
                    rec.phone) == 10):
                trigger_sms = True
            elif (
                    eventdetail.sendemail == True and rec.cancelapproved_email_send == False and party_status == 'Cancel Approved'):
                trigger_email = True
            elif (
                    eventdetail.sendsms == True and rec.cancelapproved_sms_send == False and party_status == 'Cancel Approved' and rec.country.name == 'India' and rec.phone and len(
                    rec.phone) == 10):
                trigger_sms = True
            # if (eventdetail.sendemail == True and rec.cancelrefund_email_send == False and party_status == 'Cancel Refunded'):
            #     trigger_email = True
            # elif (eventdetail.sendsms == True and rec.cancelrefund_sms_send == False and party_status == 'Cancel Refunded' and rec.country.name == 'India' and rec.phone and len(rec.phone) == 10):
            #     trigger_sms = True

            print(party_status)
            if trigger_email == True:
                print('sending email enabled')
                templateid = eventdetail.emailtemplateid.id
                self.env['mail.template'].browse(templateid).send_mail(rec.id, force_send=True)

                if (party_status == 'Cancel Applied'):
                    rec.write({'cancelapplied_email_send': True})
                elif (party_status == 'Cancel Approved'):
                    rec.write({'cancelapproved_email_send': True})
                elif (party_status == 'Cancel Refunded'):
                    rec.write({'cancelrefund_email_send': True})

                foundrec = foundrec + 1

                print('sending email completed')

            if trigger_sms == True:
                print('sending sms enabled')
                templateid = eventdetail.smstemplateid.id
                smstemplate = self.env['sms.template'].browse(templateid)
                subject = self.env['sms.template']._render_template(smstemplate.body,
                                                                    'livingspaces.registration', rec.id)
                print(subject)
                self.env['livingspaces.smsapi']._send_smcountry_sms({
                    'number': rec.phone,
                    'message': subject
                })

                if (party_status == 'Cancel Applied'):
                    rec.write({'cancelapplied_sms_send': True})
                elif (party_status == 'Cancel Approved'):
                    rec.write({'cancelapproved_sms_send': True})
                elif (party_status == 'Cancel Refunded'):
                    rec.write({'cancelrefund_sms_send': True})

                foundrec = foundrec + 1

                print('sending sms completed')

        if foundrec > 0:
            print('cancellation update: completed sending email and sms completed for all request')
        else:
            print('cancellation update: no records found, completed..')

    #
    @api.model
    def _sendGlobalNotification(self, rec, notificationevent):
        eventdetaillist = self.env['livingspaces.globalnotificationtemplate'].search(
            [('notificationevent.notificationevent', '=', 'Change Of Participant')])
        for eventdetail in eventdetaillist:
            if (eventdetail.sendemail == True):
                print('sending email enabled')
                templateid = eventdetail.emailtemplateid.id
                self.env['mail.template'].browse(templateid).send_mail(rec.id, force_send=True)
                print('sending email completed')
            if (eventdetail.sendsms == True and rec.country.name == 'India'):
                print('sending sms enabled')
                templateid = eventdetail.smstemplateid.id
                smstemplate = self.env['sms.template'].browse(templateid)
                subject = self.env['sms.template']._render_template(smstemplate.body,
                                                                    'livingspaces.registration', rec.id)
                print(subject)
                self.env['livingspaces.smsapi']._send_smcountry_sms({
                    'number': rec.phone,
                    'message': subject
                })
                print('sending sms completed')


#
class CICO(models.AbstractModel):
    _name = 'livingspaces.cicoapi'
    _description = 'Isha Rejuvenation CICO'

    def templog(self, logtext):
        self.env['livingspaces.tempdebuglog'].create({
            'logtext': logtext
        })

    @api.model
    def _pushToCICO(self, rec):
        try:
            print('inside _pushToCICO')

            lage = 0
            if (rec.dob != None and rec.dob != False):
                today = date.today()
                lage = today.year - rec.dob.year

            lstatus = ''
            if (rec.registration_status == 'Confirmed'):
                lstatus = 'Confirmed'
            elif (
                    rec.registration_status == 'Cancel Approved' or rec.registration_status == 'Refunded' or rec.registration_status == 'Change Of Participant'):
                lstatus = 'Cancelled'

            city = ""
            if (rec.city != None and rec.city != False):
                city = rec.city

            phone = ""
            if (rec.phone != None and rec.phone != False):
                phone = rec.phone

            phone1 = ""
            if (rec.phone1 != None and rec.phone1 != False):
                phone1 = rec.phone1

            name1 = ""
            if (rec.name1 != None and rec.name1 != False):
                name1 = rec.name1

            sdate = ""
            if (rec.programapplied.pgmschedule_startdate != None and rec.programapplied.pgmschedule_startdate != False):
                sdate = rec.programapplied.pgmschedule_startdate.strftime('%Y-%m-%d %H:%M')

            edate = ""
            if (rec.programapplied.pgmschedule_enddate != None and rec.programapplied.pgmschedule_enddate != False):
                edate = rec.programapplied.pgmschedule_enddate.strftime('%Y-%m-%d %H:%M')

            oco_id = ""
            if (rec.oco_id != None and rec.oco_id != False):
                oco_id = str(rec.oco_id)

            relationship1 = ""
            if (rec.relationship1 != None and rec.relationship1 != False):
                relationship1 = rec.relationship1

            gender = ""
            if (rec.gender != None and rec.gender != False):
                if (rec.gender == "Male"):
                    gender = "M"
                elif (rec.gender == "Female"):
                    gender = "F"
                elif (rec.gender == "Others"):
                    gender = "O"

            occupation = ""
            if (rec.occupation != None and rec.occupation != False):
                occupation = rec.occupation

            postData = {"name": rec.first_name, "mobile": phone, "email": rec.participant_email, "city": city,
                        "country": rec.country.name, "nationality": rec.nationality_id.name, "status": lstatus,
                        "programName": rec.programapplied.pgmschedule_programname,
                        "programId": str(rec.programapplied.id),
                        "pvolId": str(rec.id), "receivedFromRemote": "", "occupation": occupation, "bayName": "",
                        "barcode": "", "firstname": rec.first_name, "lastname": rec.last_name, "category": "",
                        "emergencyPhone": phone1, "age": lage, "gender": gender, "emergencyContact": name1,
                        "emergencyRelation": relationship1,
                        "arrivalDate": sdate, "departureDate": edate, "ocoId": oco_id}

            imagedata = []
            if (rec.photo_file != None and rec.photo_file != False):
                print('photo available')
                photo_image = base64.b64encode(rec.photo_file)
                ilen = len(photo_image)
                photo_image = photo_image.decode('ascii')
                photo_image = 'data:image/jpeg;base64,' + photo_image
                imagedata.append({"name": "addressImage", "size": str(ilen), "type": "image/jpeg", "data": photo_image})
                postData["photo"] = imagedata
            else:
                imagedata.append({"name": "addressImage", "size": "0", "type": "image/jpeg", "data": ""})
                postData["photo"] = imagedata

            imagedata = []
            if (rec.addrproof_file != None and rec.addrproof_file != False):
                print('address proof available')
                addrproof_file = base64.b64encode(rec.addrproof_file)
                ilen = len(addrproof_file)
                addrproof_file = addrproof_file.decode('ascii')
                addrproof_file = 'data:image/jpeg;base64,' + addrproof_file
                imagedata.append(
                    {"name": "addressImage", "size": str(ilen), "type": "image/jpeg", "data": addrproof_file})
                postData["addressImage"] = imagedata
            else:
                imagedata.append({"name": "addressImage", "size": "0", "type": "image/jpeg", "data": ""})
                postData["addressImage"] = imagedata

            if rec.idproof_type.exists():
                postData["idcardType"] = rec.idproof_type.idproof_type
            else:
                postData["idcardType"] = "NA";

            imagedata = []
            if (rec.idproof_file != None and rec.idproof_file != False):
                print('id proof available')
                idproof_file = base64.b64encode(rec.idproof_file)
                ilen = len(idproof_file)
                idproof_file = idproof_file.decode('ascii')
                idproof_file = 'data:image/jpeg;base64,' + idproof_file
                imagedata.append({"name": "idcard", "size": str(ilen), "type": "image/jpeg", "data": idproof_file})
                postData["idcardImage"] = imagedata
            else:
                imagedata.append({"name": "idcard", "size": "0", "type": "image/jpeg", "data": ""})
                postData["idcardImage"] = imagedata

            _postData = []
            _postData.append(postData);

            # print (_postData)
            print('data preparation completed..')

            cico_url = self.env['ir.config_parameter'].sudo().get_param('livingspaces.cico_url')
            cico_apikey = self.env['ir.config_parameter'].sudo().get_param('livingspaces.cico_apikey')
            cico_src = self.env['ir.config_parameter'].sudo().get_param('livingspaces.cico_src')
            cico_dest = self.env['ir.config_parameter'].sudo().get_param('livingspaces.cico_dest')
            cico_method = self.env['ir.config_parameter'].sudo().get_param('livingspaces.cico_method')

            print('cico_url: ' + cico_url)
            # print('cico_apikey: ' + cico_apikey)
            # print('cico_src: ' + cico_src)
            # print('cico_dest: ' + cico_dest)
            # print('cico_method: ' + cico_method)

            _data = {
                'apikey': cico_apikey,
                'src': cico_src,
                'dest': cico_dest,
                'method': cico_method,
                'records': json.dumps(_postData)
            }

            # _data = json.dumps(_data)

            # f = open('/home/umapathy/cicotest.txt','w')
            # f.write(str(_data))
            # f.close()

            # print(_data)

            print('api request data preparation completed, going to call api..')

            self.templog(json.dumps(_data))

            try:
                callresult = requests.post(url=cico_url, data=_data)
            except Exception as e:
                print('error in http call..')
                print(e)
                return 0

            print(str(callresult))
            print(str(callresult.text))

            self.templog(str(callresult.text))

            result = json.loads(callresult.text)

            print(result['status'])
            print(result['status'].casefold())

            sc = 0
            if (result != None):
                if (result['status'] == None or result['status'] == ""):
                    sc = 0
                    print("Response status not received from CICO system")
                elif (result['status'].casefold() == "error"):
                    sc = 0
                    print(result['error'])
                elif (result['status'].casefold() == "ok"):
                    if (result['records'] != None and len(result['records']) >= 1):
                        if (result['records'][0]['status'] == None or result['records'][0]['status'] == ""):
                            sc = 0
                            print("Response - record status not received from CICO system")
                        elif (result['records'][0]['status'].casefold() == "error"):
                            sc = 0
                            print(result['records'][0]['error'])
                        elif (result['records'][0]['status'].casefold() == "ok"):
                            sc = 1
                            print('cico update success')
                        else:
                            sc = 0
                            print("Invalid response - record status received from CICO system")
                    else:
                        sc = 0
                        print("Response - record status not received from CICO system")
                else:
                    sc = 0
                    print("Invalid response status received from CICO system")
            else:
                sc = 0
                print("Response not received from CICO system")

            return sc
        except Exception as e:
            print(e)
            return 0

    #
    @api.model
    def _pushRegistrationsToCICO(self):

        print('inside _pushToCICO')

        foundrec = 0

        plist1 = self.env['program.registration'].search(
            [('registration_status', '=', 'Confirmed'), ('pushedtocico', '=', False),
             ('programapplied.pushtocico', '=', True),
             ('programapplied.pgmschedule_programtype.isresidential', '=', 'Yes')])

        # print(plist1)

        plist2 = self.env['program.registration'].search(
            ['|', ('registration_status', '=', 'Cancel Approved'), ('registration_status', '=', 'Refunded'),
             ('pushedtocico', '=', True)])

        plist3 = self.env['program.registration'].search(
            [('registration_status', '=', 'Change Of Participant'), ('pushedtocico', '=', True)])

        # print(plist2)

        plist = plist1 | plist2
        plist = plist | plist3

        print(plist)

        for rec in plist:

            foundrec = 1

            updatestatus = self.env['livingspaces.cicoapi']._pushToCICO(rec)

            if (updatestatus == 1):
                if (rec.registration_status == 'Confirmed'):
                    rec.write({
                        'pushedtocico': True
                    })
                    print('pushed to cico - ' + str(rec.id))
                else:
                    rec.write({
                        'pushedtocico': False
                    })
                    print('reversed cico entry - ' + str(rec.id))
            else:
                print('error response received..')

        if foundrec == 1:
            print('cico push: completed cico push for all request')
        else:
            print('cico push: no records found, completed..')


# class Payments(models.AbstractModel):
#     _name = 'livingspaces.payments'
#     _description = 'Isha Living Spaces Payments'
#
#     @api.model
#     def _sync_joomla(self):
#         sync_joomla(self.env)
#
#     #
#     @api.model
#     def _calculateRefundAmount(self, access_data, canceldate):
#
#         refundrule = {}
#
#         refundrule[
#             access_data.programapplied.pgmschedule_programtype.cancellationnoofdays1] = access_data.programapplied.pgmschedule_programtype.cancellationnoofdays1percent
#         refundrule[
#             access_data.programapplied.pgmschedule_programtype.cancellationnoofdays2] = access_data.programapplied.pgmschedule_programtype.cancellationnoofdays2percent
#         refundrule[
#             access_data.programapplied.pgmschedule_programtype.cancellationnoofdays3] = access_data.programapplied.pgmschedule_programtype.cancellationnoofdays3percent
#
#         print(access_data.programapplied.pgmschedule_startdate)
#         print(canceldate.date())
#
#         refundpercentage = 0
#         days = (access_data.programapplied.pgmschedule_startdate - (canceldate.date())).days
#
#         print('days: ' + str(days))
#
#         for i in sorted(refundrule.keys(), reverse=True):
#             if days >= i:
#                 refundpercentage = refundrule[i]
#                 break
#
#         print('refund percentage: ' + str(refundpercentage))
#
#         refundamount = 0
#         if (refundpercentage > 0 and access_data.amount_paid > 0):
#             refundamount = round(((access_data.amount_paid / 100) * refundpercentage), 2)
#
#         transfercharge = 0
#         if (access_data.programapplied.pgmschedule_programtype.participantreplacement > 0
#                 and access_data.amount_paid > 0):
#             transfercharge = round(((
#                                                 access_data.amount_paid / 100) * access_data.programapplied.pgmschedule_programtype.participantreplacement),
#                                    2)
#
#         return {'days_before': days, 'elg_percent': refundpercentage, 'elg_amt': refundamount,
#                 'transfercharge': transfercharge}
#
#     #
#     @api.model
#     def _getBalanceAmount(self, access_data, selected_package):
#         balanceamount = 0
#         if (access_data.ischangeofparticipant == True):
#             if (access_data.transferred_amount < selected_package.pgmschedule_packagetype.packagecost):
#                 balanceamount = (selected_package.pgmschedule_packagetype.packagecost - access_data.transferred_amount)
#         else:
#             balanceamount = selected_package.pgmschedule_packagetype.packagecost
#         return balanceamount
#
#     @api.model
#     def _updateSeat(self, access_data):
#         # schedule package data
#         sel_pack = self.env['livingspaces.program.schedule.roomdata'].browse(access_data.packageselection.id)
#         paid_seats = 0
#         if sel_pack:
#             if (access_data.use_emrg_quota):
#                 paid_seats = sel_pack.pgmschedule_emergencyseatspaid
#             else:
#                 paid_seats = sel_pack.pgmschedule_regularseatspaid
#
#         paid_seats = paid_seats + 1
#
#         # package update
#         if (access_data.use_emrg_quota):
#             sel_pack.write({'pgmschedule_emergencyseatspaid': paid_seats})
#         else:
#             sel_pack.write({'pgmschedule_regularseatspaid': paid_seats})
#
#     @api.model
#     def _reverseSeat(self, access_data):
#         # schedule package data
#         sel_pack = self.env['livingspaces.program.schedule.roomdata'].browse(access_data.packageselection.id)
#         paid_seats = 0
#         if sel_pack:
#             if (access_data.use_emrg_quota):
#                 paid_seats = sel_pack.pgmschedule_emergencyseatspaid
#             else:
#                 paid_seats = sel_pack.pgmschedule_regularseatspaid
#
#         if paid_seats > 0:
#             paid_seats = paid_seats - 1
#
#         # package update
#         if (access_data.use_emrg_quota):
#             sel_pack.write({'pgmschedule_emergencyseatspaid': paid_seats})
#         else:
#             sel_pack.write({'pgmschedule_regularseatspaid': paid_seats})
#
#     @api.model
#     def _updatePaidStatus(self, access_data, amountpaid):
#         access_data.write({'registration_status': 'Paid', 'payment_status': 'Success',
#                            'amount_paid': amountpaid, 'date_paid': datetime.now()})
#         if (amountpaid > 0 and access_data.ereceiptgenerated == False):
#             self.env['livingspaces.paymentstatus'].generateEreceipt(access_data)


class SSO(models.AbstractModel):
    _name = 'livingspaces.ssoapi'
    _description = 'Isha Living Spaces SSO API'



    @api.model
    def get_consent_info(self, data):
        consentgrantStatusflag = False
        profidvarforconsent = data["profileidvar"]
        requestconsent_url = "https://uat-profile.isha.in/services/pms/api/consents?legalEntity=" + data[
            "legal_entity"] + "&profileId=" + profidvarforconsent
        headersconsent = {
            "x-user": profidvarforconsent,
            "X-legal-entity": "IBPL",
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJlbnYiOiJVQVQiLCJraWQiOiJlMmYwZWRjYjU0YWQ0NzVhODZmNGI4YmNmNGE5MzQxZCIsImFsZyI6IlJTMjU2In0.eyJpYXQiOjE1OTYwMjQ5MTQsImlzcyI6InBtcyIsInN1YiI6IlJlanV2ZW5hdGlvbiIsIl90dCI6InN5c3RlbSIsInJvbGVzIjpbXX0.Bq-2grTvpaq1T7KS4VjkbEE_30ECBTBcusrxkvohmgXIIPKkQsm_mnXslqmuZbI9DEQAVENqK48jSuINwPM7qpbVR52qUVtWndhkzoTBQ9DzbiAQeratwVzl2ahgvlT54D0WfR8mKvN2ZDYatAAowL76jiSVHU6R0BU2PIy26tVOqJNdeWDtvi6Pbx1cMpyko7TT6mclXuYSImPI7KzoCKpmE3pRgRRB2nW8TJlU0olEiFJCmUNFtVveuIi3dOzGS9pzQLjCbHYyRF4pECmoSGtRYBn_gkWMk_ai8YRRhrRSaboiWdFEGkYpDhjgsjjsGeRUMs7oBEu_DPjMB5WBqQ"
        }
        try:
            if profidvarforconsent != "0":
                result = requests.get(requestconsent_url, headers=headersconsent, verify=False)
                result.raise_for_status()
                consentreq_dict = result.json()
                consentgrantStatusflag = consentreq_dict[0]["grantStatus"]

        except Exception as e:
            self._raise_query_error(e)
        return consentgrantStatusflag

    def get_profileid_fromtoken(self, data):

        profidvar = "0"
        try:
            decodedtoken = jwt.decode(data, verify=False, algorithms=['RS256'])
            profidvar = decodedtoken['sub']
        except Exception as e:
            self._raise_query_error(e)
        return profidvar

    def get_fullprofile_info(self, data):
        profidvar = data["profileidvar"]
        legal_entity = data["legalEntity"]
        fullprofileresponsedat = {}
        request_url = "https://uat-profile.isha.in/services/pms/api/full-profiles/" + profidvar
        headers = {
            "x-user": profidvar,
            "X-legal-entity": legal_entity,
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJlbnYiOiJVQVQiLCJraWQiOiJlMmYwZWRjYjU0YWQ0NzVhODZmNGI4YmNmNGE5MzQxZCIsImFsZyI6IlJTMjU2In0.eyJpYXQiOjE1OTYwMjQ5MTQsImlzcyI6InBtcyIsInN1YiI6IlJlanV2ZW5hdGlvbiIsIl90dCI6InN5c3RlbSIsInJvbGVzIjpbXX0.Bq-2grTvpaq1T7KS4VjkbEE_30ECBTBcusrxkvohmgXIIPKkQsm_mnXslqmuZbI9DEQAVENqK48jSuINwPM7qpbVR52qUVtWndhkzoTBQ9DzbiAQeratwVzl2ahgvlT54D0WfR8mKvN2ZDYatAAowL76jiSVHU6R0BU2PIy26tVOqJNdeWDtvi6Pbx1cMpyko7TT6mclXuYSImPI7KzoCKpmE3pRgRRB2nW8TJlU0olEiFJCmUNFtVveuIi3dOzGS9pzQLjCbHYyRF4pECmoSGtRYBn_gkWMk_ai8YRRhrRSaboiWdFEGkYpDhjgsjjsGeRUMs7oBEu_DPjMB5WBqQ"
        }
        try:
            req = requests.get(request_url, headers=headers, verify=False)
            req.raise_for_status()
            fullprofileresponsedat = req.json()

        except Exception as e:
            self._raise_query_error(e)

        return fullprofileresponsedat

    def post_fullprofile_info(self, fullprofiledat, profidvar, legal_entity):
        responsecode = 0
        headers2 = {
            "x-user": profidvar,
            "X-legal-entity": legal_entity,
            "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJlbnYiOiJVQVQiLCJraWQiOiJlMmYwZWRjYjU0YWQ0NzVhODZmNGI4YmNmNGE5MzQxZCIsImFsZyI6IlJTMjU2In0.eyJpYXQiOjE1OTYwMjQ5MTQsImlzcyI6InBtcyIsInN1YiI6IlJlanV2ZW5hdGlvbiIsIl90dCI6InN5c3RlbSIsInJvbGVzIjpbXX0.Bq-2grTvpaq1T7KS4VjkbEE_30ECBTBcusrxkvohmgXIIPKkQsm_mnXslqmuZbI9DEQAVENqK48jSuINwPM7qpbVR52qUVtWndhkzoTBQ9DzbiAQeratwVzl2ahgvlT54D0WfR8mKvN2ZDYatAAowL76jiSVHU6R0BU2PIy26tVOqJNdeWDtvi6Pbx1cMpyko7TT6mclXuYSImPI7KzoCKpmE3pRgRRB2nW8TJlU0olEiFJCmUNFtVveuIi3dOzGS9pzQLjCbHYyRF4pECmoSGtRYBn_gkWMk_ai8YRRhrRSaboiWdFEGkYpDhjgsjjsGeRUMs7oBEu_DPjMB5WBqQ"
        }
        request_url = "https://uat-profile.isha.in/services/pms/api/full-profiles/" + profidvar
        try:
            # datavarpost = json.dumps(fullprofiledat)
            r = requests.post(request_url, files={'profile': (None, fullprofiledat)}, headers=headers2)
            responsecode = r.status_code
        except Exception as e:
            self._raise_query_error(e)

        return responsecode

    def get_fullprofileresponse_DTO(self):
        return {
            "profileId": "",
            "basicProfile": {
                "createdBy": "",
                "createdDate": "",
                "lastModifiedBy": "",
                "lastModifiedDate": "",
                "id": 0,
                "profileId": "",
                "email": "",
                "firstName": "",
                "lastName": "",
                "profilePic": "",
                "phone": {
                    "countryCode": "",
                    "number": ""
                },
                "gender": "",
                "dob": "",
                "spoken_languages":"",
                "countryOfResidence": ""
            },
            "extendedProfile": {
                "createdBy": "",
                "createdDate": "",
                "lastModifiedBy": "",
                "lastModifiedDate": "",
                "id": 0,
                "passportNum": "",
                "nationality": "",
                "pan": "",
                "profileId": ""
            },
            "documents": [],
            "attendedPrograms": [],
            "profileSettingsConfig": {
                "createdBy": "",
                "createdDate": "",
                "lastModifiedBy": "",
                "lastModifiedDate": "",
                "id": 0,
                "nameLocked": False,
                "isPhoneVerified": False,
                "phoneVerificationDate": None,
                "profileId": ""
            },
            "addresses": [
                {
                    "createdBy": "",
                    "createdDate": "",
                    "lastModifiedBy": "",
                    "lastModifiedDate": "",
                    "id": 0,
                    "addressType": "",
                    "addressLine1": "",
                    "addressLine2": "",
                    "townVillageDistrict": "",
                    "city": "",
                    "state": "",
                    "country": "",
                    "pincode": "",
                    "profileId": ""
                }
            ]
        }

    def _raise_query_error(self, error):
        print('Error with SSO api: %s' % error)
        # raise ValueError('Error with sending sms: %s' % error)

    def get_ssoimage_froms3(self, ssoidproofuri):
        req = requests.get(ssoidproofuri, headers={}, verify=False)
        req.raise_for_status()
        return req._content