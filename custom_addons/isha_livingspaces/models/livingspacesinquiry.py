from odoo import models, fields, api, exceptions
from . import commonmodels
from . import livingspacestype
from . import livingspacesschedule
import time
import datetime

class CandidateInquiry(models.Model): 
	_name = 'livingspaces.inquiry'
	_description = 'Living Spaces Inquiry'
	_rec_name = 'first_name'

	first_name = fields.Char(string = 'First Name')
	last_name = fields.Char(string = 'Last Name')
	countrycode = fields.Char(string='Country Tel code')
	mobile = fields.Char(string='Mobile')
	email = fields.Char(string='Email')
	nationality = fields.Char(string='Nationality', required=False)
	city = fields.Char(string='City', required=False)
	state = fields.Char(string='State', required=False)
	country = fields.Many2one('res.country', 'Country of residence')
	pincode = fields.Char(string='Pin Code', required=False)
	eduqualify = fields.Char(string='Education Qualification', required=False)
	occupation = fields.Char(string='Occupation/Work Experience', required=False)
	langknown= fields.Char(string='Languages Known', required=False)
	iepstatus=fields.Selection([('YES', 'Yes'), ('NO', 'No')], default='YES', string='Have you done Isha Inner Engineering Program?', required=False)
	center = fields.Char(string='Center Name', required=False)
	comp_date = fields.Char(string='Date', required=False)
	place = fields.Char(string='Place', required=False)
	teacher_name = fields.Char(string='Teacher Name', required=False)
	sham_status = fields.Selection([('YES', 'Yes'), ('NO', 'No')], default='YES',	string='Have you completed Online Shambavi Initiation?', required=False)
	sham_center = fields.Char(string='Center Name', required=False)
	sham_comp_date = fields.Char(string='Date', required=False)
	sham_place = fields.Char(string='Place', required=False)
	sham_teacher_name = fields.Char(string='Teacher Name', required=False)
	comments =  fields.Text(string = 'Comments')
	selected_program = fields.Many2one('livingspaces.program.schedule',string = 'Selected Program')
	selected_programtype = fields.Many2one(related='selected_program.pgmschedule_programtype')
	inquiry_status =  fields.Selection([('Inquiry','Inquiry'),('Registered','Registered')], string='Status')

	ack_email_send = fields.Boolean(string="Acknowledge Email Send", default=False)
	ack_sms_send = fields.Boolean(string="Acknowledge SMS Send", default=False)

	@api.model
	def fields_get(self, fields=None):
		fields_to_show = ['first_name','last_name','mobile','email','comments','selected_program']
		res = super(CandidateInquiry, self).fields_get()
		for field in res:
			if (field in fields_to_show):
				res[field]['selectable'] = True	
				res[field]['sortable'] = True
			else:
				res[field]['selectable'] = False
				res[field]['sortable'] = False
		return res
		
	def update_registered(self):
		print('update triggered')
		nlist = self.env['livingspaces.inquiry'].search([('inquiry_status','=','Inquiry')])
		if nlist:
			print('list found')
			for rec in nlist:
				print('rec found')
				print(rec)
				ids = self.env['livingspaces.registration'].search([('participant_email','=',rec.email),('participant_programtype','=',rec.selected_programtype.id),('create_date','>',rec.create_date)])
				if ids.exists():
					print('found party')
					rec.write({'inquiry_status':'Registered'})

	@api.model
	def create(self, vals):   
		rec = super(CandidateInquiry, self).create(vals)
		#self.env['rejuvenation.notification']._sendInquiryAcknowledgement(rec)
		return rec

	@api.model
	def sendInquiryAcknowledgement(self):
		reclist = self.env['livingspaces.inquiry'].search([('inquiry_status','=','Inquiry'),
		('ack_email_send','=', False), ('ack_sms_send','=', False)])
		for rec in reclist:
			self.env['livingspaces.notification']._sendInquiryAcknowledgement(rec)
		print('completed sending inquiry acknowledgement')
