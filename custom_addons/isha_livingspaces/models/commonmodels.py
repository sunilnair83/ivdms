from odoo import models, fields, api, exceptions


class livingspacesEntity(models.Model):
    _name = 'livingspaces.entity'
    _description = 'Entity'
    _order = 'entityname'
    _rec_name = 'entityname'
    _sql_constraints = [
        ('unique_entityname', 'unique(entityname)', 'Cannot have duplicate entity name, give different name')]

    entityname = fields.Char(string='Entity Name', size=100, required=True)


class livingspacesPaymentGateway(models.Model):
    _name = 'livingspaces.paymentgateway'
    _description = 'Payment Gateway'
    _order = 'paymentgatewayname'
    _rec_name = 'paymentgatewayname'
    _sql_constraints = [('unique_paymentgatewayname', 'unique(paymentgatewayname)',
                         'Cannot have duplicate payment gateway, give different gateway')]

    paymentgatewayname = fields.Char(string='Payment Gateway', size=100, required=True)


class livingspacesLanguage(models.Model):
    _name = 'livingspaces.language1'
    _description = 'Language Description'
    _order = 'languagedescription'
    _rec_name = 'languagedescription'
    _sql_constraints = [('unique_languagedescription', 'unique(languagedescription)',
                         'Cannot have duplicate language, give different name')]

    languagedescription = fields.Char(string='Language Description', size=100, required=True)


class livingspacesPracticeMaster(models.Model):
    _name = 'livingspaces.practice'
    _description = 'Practice Master'
    _order = 'practicename'
    _rec_name = 'practicename'
    _sql_constraints = [('unique_practicename', 'unique(practicename)',
                         'Cannot have duplicate practice name, give different practice name')]

    practicename = fields.Char(string='Practice Name', size=100, required=True)


class livingspacesHealthReportsMaster(models.Model):
    _name = 'livingspaces.healthreports'
    _description = 'Health Report Master'
    _order = 'healthreportname'
    _rec_name = 'healthreportname'
    _sql_constraints = [('unique_healthreportname', 'unique(healthreportname)',
                         'Cannot have duplicate health reportname, give different name')]

    healthreportname = fields.Char(string='Health Report Name', size=100, required=True)


class livingspacesRoomType(models.Model):
    _name = 'livingspaces.roomtype'
    _description = 'Room Type'
    # _order = roomtype'
    _rec_name = 'roomtypename'
    _sql_constraints = [
        ('unique_roomtypename', 'unique(roomtypename)', 'Cannot have duplicate room type name, give different name')]

    roomtypename = fields.Char(string='Room Type Name', size=100, required=True)
    noofpersons = fields.Integer(string='No Of Persons', required=True, default=1)

    @api.constrains('noofpersons')
    def validate_noofpersons(self):
        for rec in self:
            if (rec.noofpersons <= 0 or rec.noofpersons > 2):
                raise exceptions.ValidationError("No of persons should be 1 or 2")


class livingspacesNotificationMaster(models.Model):
    _name = 'livingspaces.notificationevent'
    _description = 'Notification Event'
    # _order = 'lob, programtype'
    _rec_name = 'notificationevent'
    _sql_constraints = [('unique_notificationevent', 'unique(notificationevent)',
                         'Cannot have duplicate notification event, give different name')]

    notificationevent = fields.Char(string='Notification Event Code', size=100, required=True)


class GlobalNotificationMaster(models.Model):
    _name = 'livingspaces.globalnotificationevent'
    _description = 'Global Notification Event'
    # _order = 'lob, programtype'
    _rec_name = 'notificationevent'
    _sql_constraints = [('unique_globalnotificationevent', 'unique(notificationevent)',
                         'Cannot have duplicate notification event, give different name')]

    notificationevent = fields.Char(string='Notification Event Code', size=100, required=True)


class GlobalNotificationTemplateMaster(models.Model):
    _name = 'livingspaces.globalnotificationtemplate'
    _description = 'Global Notification Template'
    # _order = 'lob, programtype'
    _rec_name = 'notificationevent'
    _sql_constraints = [
        ('unique_globalnotificationevent1', 'unique(notificationevent)', 'Cannot have duplicate notification event')]

    notificationevent = fields.Many2one('livingspaces.globalnotificationevent', string='Notification Event')
    sendemail = fields.Boolean(string='Send Email', default=False)
    emailtemplateid = fields.Many2one('mail.template', string='Email Template', track_visibility='onchange')
    sendsms = fields.Boolean(string='Send SMS', default=False)
    smstemplateid = fields.Many2one('sms.template', string='SMS Template', track_visibility='onchange')


class livingspacesNotificationTemplateMaster(models.Model):
    _name = 'livingspaces.notificationtemplate'
    _description = 'Notification Template'
    # _order = 'lob, programtype'
    _rec_name = 'notificationevent'
    _sql_constraints = [('unique_notificationevent1', 'unique(programtype, notificationevent)',
                         'Cannot have duplicate notification event')]

    programtype = fields.Many2one('living.spaces.type', string='Program Type')
    notificationevent = fields.Many2one('livingspaces.notificationevent', string='Notification Event')
    sendemail = fields.Boolean(string='Send Email', default=False)
    emailtemplateid = fields.Many2one('mail.template', string='Email Template', track_visibility='onchange')
    sendsms = fields.Boolean(string='Send SMS', default=False)
    smstemplateid = fields.Many2one('sms.template', string='SMS Template', track_visibility='onchange')


class livingspacesMedicalQuestionBank(models.Model):
    _name = 'livingspaces.qbnk'
    _description = 'Question Bank'
    # _order = 'paymentgatewayname'
    _rec_name = 'questiondescription'
    _sql_constraints = [('unique_questiondescription', 'unique(questiondescription)',
                         'Cannot have duplicate question description, give different question')]

    questiondescription = fields.Text(string='Question', required=True)


class livingspacesRejectionReason(models.Model):
    _name = 'livingspaces.rejection.reason'
    _description = 'Rejection Reason'
    # _order = 'lob, programtype'
    _rec_name = 'reason_description'
    _sql_constraints = [('unique_reason_description', 'unique(reason_description)',
                         'Cannot have duplicate reason description, give different reason description')]

    reason_description = fields.Char(string='Reason Description', size=100, required=True)


class livingspacesTermsAndConditions(models.Model):
    _name = 'livingspaces.terms_and_conditions'
    _description = 'Terms And Conditions'
    _rec_name = 'tc_description'

    tc_description = fields.Html(string='Terms and conditions')


class livingspacesTempDebugLog(models.Model):
    _name = 'livingspaces.tempdebuglog'
    _description = 'Temp debug log'

    logtext = fields.Text(string='logtext')


class MasterProgramCategory(models.Model):
    _name = 'master.livingspaces.category'
    _description = "Master list of livingspaces categories"
    _rec_name = 'programcategory'

    programcategory = fields.Char('Program Category')
    keyprogram = fields.Boolean('Key Program', default=False)