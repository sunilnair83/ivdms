import json
from odoo import api, models
from odoo.exceptions import UserError
from datetime import date, timedelta, datetime

class Cleanup(models.AbstractModel):
    _name = 'livingspaces.cleanup'
    _description = 'Isha Living Spaces DB Cleanup'

    @api.model
    def cleanup(self):
        templogdays = self.env['ir.config_parameter'].sudo().get_param('livingspaces.templogdays')
        templogdays = int(templogdays)
        if (templogdays == 0 or templogdays == False):
            templogdays = 90
        diff = (datetime.now() - timedelta(days=templogdays))
        print (diff)
        self.env['livingspaces.tempdebuglog'].search([('create_date','<', diff)]).unlink()
        print('cleanup completed..')
