from odoo import models, fields, api, exceptions
import time
import datetime

import requests


class KshetragnaPrescreened(models.Model):
    _name = 'kshetragna.prescreenedregistrants'
    _description = 'Kshetragn Prescreened Registrans'
    _rec_name = 'first_name'

    first_name = fields.Char(string='First Name')
    last_name = fields.Char(string='Last Name')
    countrycode = fields.Char(string='Country Tel code',required=True)
    mobile = fields.Char(string='Mobile',required=True)
    email = fields.Char(string='Email',required=True)
    payment_status = fields.Selection(
        [('Pending', 'Pending'), ('Rejected', 'Rejected'), ('Rejection Review', 'Rejection Review'),
         ('Payment Link Sent', 'Payment Link Sent'), ('Add To Waiting List', 'Add To Waiting List'),
         ('Cancel Applied', 'Cancel Applied'), ('Cancel Approved', 'Cancel Approved'), ('Paid', 'Paid'),
         ('Confirmed', 'Confirmed'), ('Withdrawn', 'Withdrawn'), ('Refunded', 'Refunded'),
         ('Incomplete', 'Incomplete'), ('payment_pending', 'Payment Pending'), ('payment_upgraded', 'Payment Upgraded'),
         ('Enquiry', 'Enquiry'), ('payment_failed', 'Payment Failed'), ('payment_inprogress', 'Payment Progress'),
         ('payment_initiated', 'Payment Initiated')],
        string='Payment Status', size=100, default='Pending')

    @api.constrains('mobile', 'email')
    def _check_values(self):
        rec = self.env['kshetragna.prescreenedregistrants'].search([("email","=",self.email)])
        if len(rec) > 1:
            raise exceptions.ValidationError("Registered Email-Id Already Exists")

        rec = self.env['kshetragna.prescreenedregistrants'].search([("mobile","=",self.mobile)])
        if len(rec) > 1:
            raise exceptions.ValidationError("Registered mobile number Already Exists. Please Change the mobile number")
