from odoo import api, models, fields

class LivingspaceOfflinePayment(models.Model):
    _name = 'livingspaces.offline.payment'
    _description = 'Livingspaces User Transacion Reference'
    _rec_name = 'remitter_name'

    date_of_transfer = fields.Date('Date Of Transfer')
    remitter_name = fields.Char(string='Remitter’s Name')
    remitter_account_number = fields.Char(string='Remitter’s Account Number')
    remitter_address = fields.Char(string='Remitter’s Complete Address')
    amount_transferred = fields.Char(string='Amount Transferred')
    UTR_number = fields.Char(string='UTR Number')
    registration_id = fields.Many2one('kshetragna.registration')

class LivingspaceOfflinePayment(models.Model):
    _name = 'livingspaces.bank.user.payment'
    _description = 'Livingspaces Bank Transaction Reference'
    _rec_name = 'remitter_name'

    receipt_no = fields.Char(string='Receipt No')
    receipt_date = fields.Date('Receipt Date')
    payment_mode = fields.Selection([('cash', 'Cash'), ('cheque', 'Cheque'),
                                     ('card', 'Credit/Debit Card'),('demand_draft', 'Demand Draft'),
                                     ('neft', 'NEFT/RTGS')],string='Payment Mode')
    net_amount = fields.Char(string='Net Amount')
    Currency = fields.Many2one('res.currency',string="Currency")
    remitter_name = fields.Char(string='Remitter')
    remitter_email = fields.Char(string='Remitter Email')
    remitter_mobile = fields.Char(string='Remitter Mobile')
    remitter_address = fields.Char(string='Remitter Address')
    remitter_city = fields.Char(string='Remitter City')
    remitter_state = fields.Char(string='Remitter State')
    remitter_pincode = fields.Char(string='Remitter Pincode')
    remitter_country = fields.Many2one('res.country', 'Remitter Country')
    nationality_id = fields.Many2one('res.country', 'Remitter Nationality')
    ereceipt_url = fields.Text()
    date_of_transfer = fields.Date('Transaction Received Date')
    trans_id = fields.Char(string='Transaction ID')
    remitter_ac_no = fields.Char(string='Remitter Account No.')
    remitter_ifsc = fields.Char(string='Remitter IFSC')
    cheque_no = fields.Char(string='Instrument / DD/Cheque No')
    cheque_date = fields.Char(string='Instrument / DD/ Cheque Date')
    bank_name = fields.Char(string='Bank Name')
    branch_name = fields.Char(string='Branch Name')
    cc_approval_code = fields.Char(string='CC Approval Code')
    cc_terminal_id = fields.Char(string='CC Terminal Id')
    benificiary_ac_no = fields.Char(string='Beneficiary Account')
    Purpose = fields.Char()
    participants_name = fields.Char(string='Participants Name')
    registration_id = fields.Many2one('kshetragna.registration',string="Registration Number")

class LivingspaceFinanceComments(models.Model):
    _name = 'livingspaces.finance.comments'
    _description = 'Tracking Finance Comments'
    _rec_name = 'registration_id'

    upload_doc_comments = fields.Text('Finance Comments')
    user_comments = fields.Char('User Comments')
    registration_id = fields.Many2one('kshetragna.registration')



