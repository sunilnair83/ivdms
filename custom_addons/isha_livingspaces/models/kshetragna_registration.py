from odoo import models, fields, api, _
from odoo.exceptions import ValidationError
import re
import time
import datetime
import logging
_logger = logging.getLogger(__name__)
import requests
import json
import logging
import traceback

class KshetragnaRegistration(models.Model):
    _name = 'kshetragna.registration'
    _description = 'Kshetragna Registration'
    _rec_name = 'first_name'

    def _get_isha_env(self):
        isha_ev = self.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')
        if isha_ev =='ishangamone':
            return True
        return False


    first_name = fields.Char(string='First Name')
    last_name = fields.Char(string='Last Name')
    countrycode = fields.Char(string='Country Tel code')
    mobile = fields.Char(string='Mobile')
    gender = fields.Selection([('Male', 'Male'), ('Female', 'Female'), ('Others', 'Others')], string='Gender',
                              required=False)
    dob = fields.Date(string='Date Of Birth', required=False)
    email = fields.Char(string='Email')
    last_email_sent_status = fields.Char(string='Last Email sent')
    last_email_sent_ts = fields.Datetime(string='Last Email sent timestamp')
    date_paid = fields.Datetime()
    amount_paid = fields.Float(string='Amount Paid', default=0)
    marital_status = fields.Selection(
        [('Unmarried', 'Unmarried'),('Married', 'Married'), ('Divorced', 'Divorced'),
         ('Widowed', 'Widowed'),('Separated','Separated')], string='Marital Status')
    nationality_id = fields.Many2one('res.country', 'Nationality')
    addressline = fields.Char(string="Address")
    city = fields.Char(string='City', required=False)
    state = fields.Char(string='State', required=False)
    country = fields.Many2one('res.country', 'Country of residence')
    pincode = fields.Char(string='Pin Code', required=False)
    is_oci_card_holder = fields.Boolean(string="OCI Card holder")
    is_oci_card = fields.Binary(string="")
    approximate_date = fields.Date(string='Approximate date when you want to move in', required=False)
    furnish_type = fields.Selection(
        [('unfurnished', 'Unfurnished'), ('furnished', 'Furnished'),('semi_furnished', 'Semi-Furnished')],string='Furnish Needed')
    accommodation_type=fields.Selection(
            [('studio', 'Studio'), ('studio_with_balcony', 'Studio with Balcony'),('1bhk', '1BHK'), ('2bhk', '2BHK'),
             ('others', 'Others')],
            string='Accommodation Type')
    pan_no = fields.Char(string="PAN Number")
    pan_card_file = fields.Binary(string="Upload Pan Card")
    passport_number = fields.Char(string="Passport")
    passport_image = fields.Binary(string="Passport Copy")
    aadhar_card = fields.Char(string="Aadhar Card")
    aadhar_card_file = fields.Binary(string="Upload Aadhar Card")

    #Bank Account Details
    account_number = fields.Char(string="Account Number")
    account_holder_name = fields.Char(string="Account Holder Name")
    bank_name = fields.Char(string="Bank Name")
    bank_branch_name = fields.Char(string="Branch Name")
    # account_type = fields.Char(string="Account Type")
    account_type = fields.Selection([('sb', 'Savings'),('current', 'Current')],string='Account Type')
    ifsc_code  = fields.Char(string="IFSC Code")
    #sso_id
    sso_id = fields.Char(string="SSO Id", required=False, index=True)
    order_id = fields.Char(string="Order Id", required=False, index=True)
    online_transaction_id = fields.Char(string="Payment transaction id")


    registration_status = fields.Selection(
        [('Pending', 'Pending'), ('Rejected', 'Rejected'), ('Rejection Review', 'Rejection Review'),
         ('Payment Link Sent', 'Payment Link Sent'), ('Add To Waiting List', 'Add To Waiting List'),
         ('Approved','Approved'),('prescreened','Pre-Screened'),
         ('Cancel Applied', 'Cancel Applied'), ('Cancel Approved', 'Cancel Approved'), ('Paid', 'Paid'),
         ('Confirmed', 'Confirmed'), ('Withdrawn', 'Withdrawn'), ('Refunded', 'Refunded'),
         ('Incomplete', 'Incomplete'), ('payment_pending', 'Payment Pending'), ('payment_upgraded', 'Payment Upgraded'),
         ('Enquiry', 'Enquiry'), ('payment_failed', 'Payment Failed'), ('payment_inprogress', 'Payment Progress'),
         ('payment_initiated', 'Payment Initiated'),('payment_link_sent_neft', 'Payment Link Sent to NEFT'),('payment_link_sent_dd', 'Payment Link Sent to DD'),
         ('document_pending','Document Pending'),('document_corrected','Document Reuploaded'),
         ('waiting_for_approve','Waiting for Approval'),('transfer_to_payment_gateway','Transferring to Payment Gateway'),
         ('awating_neft_approval', 'Awaiting NEFT Approval'), ('awating_dd_approval', 'Awaiting DD/Cheque Approval'),
         ('neft_requested', 'NEFT Requested'),('neft_success', 'NEFT Success'), ('dd_requested', 'DD/Cheque Requested'),
         ('dd_success', 'DD/Cheque Success'),('dd_failed', 'DD/Cheque Failed'),('partially_complete','Partially Completed')
         ],
        string='Registration Status', size=100, default='Pending')
    payment_status = fields.Selection(
        [('Pending', 'Pending'), ('Rejected', 'Rejected'), ('Rejection Review', 'Rejection Review'),
         ('Payment Link Sent', 'Payment Link Sent'), ('Add To Waiting List', 'Add To Waiting List'),
         ('Waiting for approve','Waiting for Approval'),('Approved','Approved'),
         ('Cancel Applied', 'Cancel Applied'), ('Cancel Approved', 'Cancel Approved'), ('Paid', 'Paid'),
         ('Confirmed', 'Confirmed'), ('Withdrawn', 'Withdrawn'), ('Refunded', 'Refunded'),
         ('Incomplete', 'Incomplete'), ('payment_pending', 'Payment Pending'), ('payment_upgraded', 'Payment Upgraded'),
         ('Enquiry', 'Enquiry'), ('payment_failed', 'Payment Failed'), ('payment_inprogress', 'Payment Progress'),
         ('payment_initiated', 'Payment Initiated')],
        string='Payment Status', size=100, default='Pending')
    payment_mode = fields.Selection([('online', 'Online'),('neft', 'NEFT / RTGS'), ('dd', 'DD/ Cheque')],string='Payment Mode')
    payment_link = fields.Char(string="payment link")
    payment_link_sent_date = fields.Date(string="Link Sent Date")
    register_link = fields.Char(string="Registered link")
    register_link_sent_date = fields.Date(string="Link Sent Date")
    document_pending_link = fields.Char(string="Document Pending link")
    document_pending_link_sent_date = fields.Char(string="Document Link Sent Date")

    receipt_addressline = fields.Char(string="Address")
    receipt_city = fields.Char(string='City', required=False)
    receipt_state = fields.Char(string='State', required=False)
    receipt_country = fields.Many2one('res.country', 'Country')
    receipt_pincode = fields.Char(string='Pin Code', required=False)
    is_interview_mail_sent = fields.Boolean(string="Interview schedule Notification Sent")
    # payment_status = fields.Char(string='Payment Status', size=70)

    isha_residential_space_m1 = fields.Selection([('sadhana_aspects', 'Lifelong Sadhana'), ('lifelong_living_space', 'License for a lifelong living space (includes lifelong sadhana)')], string='Im interested in')
    isha_residential_space_m2 = fields.Selection([('lifelong_sadhana_only', 'Lifelong Sadhana Only'), ('isha_yoga_center', 'Living Isha Residential spaces at Isha Yoga Center, India'), ('isha_institute', 'Living Isha Residential spaces at Isha Institute of inner sciences, USA')], string='Im interested in')
    isishangamonev = fields.Boolean(default=_get_isha_env)
    ip_Address = fields.Char()
    ip_country = fields.Char()
    is_enquiry_email = fields.Boolean()
    is_sync = fields.Boolean('Synced')
    is_india_Data_check = fields.Boolean()

    upload_doc_comments = fields.Text()
    user_comments = fields.Char()

    user_transaction_ids = fields.One2many('livingspaces.offline.payment','registration_id',string="User Transaction Reference")
    bank_transaction_ids = fields.One2many('livingspaces.bank.user.payment', 'registration_id',string="User Transaction Reference")
    is_usa_bool = fields.Boolean()
    finance_comments_ids = fields.One2many('livingspaces.finance.comments', 'registration_id',string="Tracking Finance Comments")

    def set_access_for_record(self):
        self.able_to_modify_record = self.env['res.users'].has_group('isha_livingspaces.group_livingspaces_edit_access')
        # self.able_to_modify_record = True
    able_to_modify_record = fields.Boolean(compute=set_access_for_record, string='Is user able to modify record?')

    # Cron US Server India Data sync to INDIA Server
    def us_server_india_data_sync(self):
        s1_url = self.env['ir.config_parameter'].sudo().get_param('kshetragna.kshetragna_india_url')
        s2_url = self.env['ir.config_parameter'].sudo().get_param('kshetragna.kshetragna_us_url')

        # redirect_url = s1_url + "/kshetragna/sync_indiadata"
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        url = s1_url + "/kshetragna/sync_indiadata"

        try:
            kshetragna_rec = self.env['kshetragna.registration'].sudo().search([('isha_residential_space_m2', '=', 'isha_yoga_center'),('is_sync','=',False)], limit=100)
            _logger.info('Kshetragna record fetch '+str(kshetragna_rec))
            if kshetragna_rec:
                for i in kshetragna_rec:
                    datas = {
                        "registration_status": i.registration_status,
                        "payment_status": i.payment_status,
                        "payment_mode": i.payment_mode if i.payment_mode else '',
                        "first_name": i.first_name,
                        "last_name": i.last_name,
                        # "dob": i.dob if i.dob else '2004-07-01',
                        "gender": i.gender,
                        "marital_status": i.marital_status if i.marital_status else '',
                        "countrycode": i.countrycode,
                        "mobile": i.mobile,
                        "email": i.email,
                        "sso_id": i.sso_id,
                        "country": i.country.id if i.country.id else 104,
                        "nationality_id": i.nationality_id.id if i.nationality_id.id else 104,
                        "addressline": i.addressline if i.addressline else '',
                        "city": i.city if i.city else '',
                        "state": i.state if i.state else '',
                        "pincode": i.pincode if i.pincode else '',
                        "receipt_addressline":  i.receipt_addressline if i.receipt_addressline else '',
                        "receipt_city": i.receipt_city if i.receipt_city else '',
                        "receipt_state": i.receipt_state if i.receipt_state else '',
                        "receipt_country": i.receipt_country.id if i.receipt_country.id else 104,
                        "receipt_pincode": i.receipt_pincode if i.receipt_pincode else '',
                        "accommodation_type": i.accommodation_type if i.accommodation_type else '',
                        "furnish_type": i.furnish_type if i.furnish_type else '',
                        "passport_number": i.passport_number if i.passport_number else '',
                        "passport_image": i.passport_image if i.passport_image else '',
                        "is_oci_card_holder": i.is_oci_card_holder,
                        "is_oci_card": i.is_oci_card if i.is_oci_card else '',
                        "pan_no": i.pan_no if i.pan_no else '',
                        "pan_card_file": i.pan_card_file if i.pan_card_file else '',
                        "aadhar_card": i.aadhar_card if i.aadhar_card else '',
                        "aadhar_card_file": i.aadhar_card_file if i.aadhar_card_file else '',
                        "account_number": i.account_number if i.account_number else '',
                        "account_holder_name": i.account_holder_name if i.account_holder_name else '',
                        "bank_name": i.bank_name if i.bank_name else '',
                        "bank_branch_name": i.bank_branch_name if i.bank_branch_name else '',
                        "account_type": i.account_type if i.account_type else '',
                        "ifsc_code": i.ifsc_code if i.ifsc_code else '',
                    }
                    _logger.info("Pass data = "+str(datas))
                    _logger.info("kshetragna - Cron call")
                    headersinfo = {
                        "content-Type": "application/x-www-form-urlencoded",
                        "Accept": "application/json",
                        "Cookie": ""
                    }
                    r = requests.post(url, data=datas, headers=headersinfo)
                    _logger.info(r.text)
                    _logger.info(r.status_code)
                    if r.status_code == 200:
                        i.write({'is_sync': True})
                        _logger.info('Successfully - Record Moved to Server')
            else:
                raise ValidationError("There is no matching Records")

        except Exception as e:
            _logger.info(e)

    # Cron US Data Push Kshetragna Registration
    def us_data_kshetragna_registration(self):
        kshetragna_rec = self.env['kshetragna.registration'].sudo().search([('isha_residential_space_m2','=','isha_institute'),('is_usa_bool','=',False)],limit=10)
        # kshetragna_rec = self.env['kshetragna.registration'].sudo().search([('id', 'in', (11, 17, 18, 22, 25, 26, 28, 47, 54))])

        # Api call to push data to US system
        _logger.info(str(kshetragna_rec))
        for p in kshetragna_rec:
            api_info = {
                'fname': p.first_name,
                'lname': p.last_name,
                'email': p.email,
                'phone_country_code': p.countrycode,
                'phone': p.mobile,
                'gender': p.gender,
                'nationality': p.nationality_id.name,
                'city': p.city,
                'state': p.state,
                'country': p.country.name,
                'sso_cor': p.country.name,
                'ssoId': p.sso_id,
                'zip': p.pincode,
                'createdDate': p.create_date,
                'housing_at': "2",
            }
            self.kshetragna_api_async_push(api_info, p)

    def kshetragna_api_async_push(self,rec,recdynReg):
        try:
            _logger.info(str(rec))
            rec['createdDate'] = rec['createdDate'].strftime("%Y-%m-%d %H:%M:%S")
            if len(rec) > 0:
                jsnresponse = json.dumps(rec)
                _logger.info('JSON Response : ' + jsnresponse)
            else:
                _logger.info('No data for id - ' + rec)
                raise
            try:
                headersinfo = {
                    "content-Type": "application/x-www-form-urlencoded",
                    "Accept":"application/json",
                    "Cookie":""
                }
                requestconsent_url = self.env['ir.config_parameter'].sudo().get_param('kshetragna.prs_innerengineering_com')
                reqvar = requests.post(requestconsent_url, jsnresponse, headers=headersinfo)
                _logger.info('API triggered to push data')
                _logger.info(reqvar.text)
                if reqvar.status_code == 200:
                    recdynReg.write({'is_usa_bool': True})
            except Exception as ex2:
                _logger.info('In exception kshetragna_api_async_push')
                tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
                _logger.error(tb_ex)

        except Exception as ex2:
            _logger.info('error in kshetragna_api_async_push')
            tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
            _logger.error(tb_ex)

    def send_interview_link(self):
        '''
        Send Interview Link to  participant if status is paid
        '''
        active_ids = self._context.get('active_ids')
        paid_active_reg_records = self.env['kshetragna.registration'].sudo().search([("id", "in", active_ids),("payment_status", "=", 'Paid')])
        un_paid_records = self.env['kshetragna.registration'].sudo().search([("id", "in", active_ids),("payment_status", "!=", 'Paid')])
        already_sent_records = self.env['kshetragna.registration'].sudo().search([("id", "in", active_ids),("is_interview_mail_sent", "=", True)])
        if (len(un_paid_records))>0:
            raise ValidationError("Link can be sent only to Paid records.")
        if (len(already_sent_records))>0:
            raise ValidationError("Link already sent to this particpant email")
        mail_template_id = self.env.ref('isha_livingspaces.ishaliving_interview_schedule_template')
        for reg_records in paid_active_reg_records:
            if reg_records:
                try:
                    mail_send_rec = self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(reg_records.id,
                                                                                                           force_send=True)
                    print(mail_send_rec)
                    _logger.info("mail sent ")
                    reg_records.write({"is_interview_mail_sent": True})
                except Exception as e:
                    print(e)
    # Re-Send Registration Link Logic
    def send_registration_link(self):
        '''
        Send Registration Link to  participant if registration_status is neft_requested
        '''
        active_ids = self._context.get('active_ids')
        register_mail_send_active_records = self.env['kshetragna.registration'].sudo().search([("id", "in", active_ids),("registration_status", "=", "prescreened")])
        already_mail_send_rec = self.env['kshetragna.registration'].sudo().search([("id", "in", register_mail_send_active_records.ids),("is_enquiry_email", "=", True)])
        mail_not_sent_rec = self.env['kshetragna.registration'].sudo().search([("id", "in", register_mail_send_active_records.ids),("is_enquiry_email", "=", False)])
        mail_template_id = self.env.ref('isha_livingspaces.ishaliving_Prescreened_AdvancePaymentForm_Template')
        mail_Send_bool = False
        for reg_records in register_mail_send_active_records:
            if reg_records:
                try:
                    base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
                    redirect_url = base_url + "/kshetragna/registrationform"
                    reg_records.sudo().write({
                        "register_link": redirect_url,
                        "register_link_sent_date": fields.Date.today(),
                        "is_enquiry_email": True})
                    mail_send_rec = self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(reg_records.id,force_send=True)
                    reg_records.write({"is_enquiry_email": True})
                    mail_Send_bool = True
                    print('Registration Email has been sent record id: %s ' %str(mail_send_rec))
                except Exception as e:
                    print(e)

        # for mail_rec in mail_not_sent_rec:
        #     raise exceptions.ValidationError('Mail has not send to this Record: %s' %mail_rec.id)

        if not mail_Send_bool:
            raise ValidationError('Registration Email has not been sent')

    # Re-Send Payment Link Logic
    def email_resend_payment_logic(self):
        '''
        Re-Send Payment Link to  participant if registration_status is neft_requested
        '''
        active_ids = self._context.get('active_ids')
        payment_sent_active_records = self.env['kshetragna.registration'].sudo().search([("id", "in", active_ids), (
            "registration_status", "in",("neft_requested", "dd_requested", "transfer_to_payment_gateway", "Payment Link Sent","Approved"))])
        payment_not_sent_active_records = self.env['kshetragna.registration'].sudo().search([("id", "in", active_ids), (
            "registration_status", "not in",("neft_requested", "dd_requested", "transfer_to_payment_gateway", "Payment Link Sent","Approved"))])

        if payment_not_sent_active_records:
            raise ValidationError('Payment Email has not been sent this record')

        for i in payment_sent_active_records:
            if i.registration_status == 'neft_requested' and i.payment_mode == 'neft' and i.payment_status != 'Paid':
                try:
                    mail_template_id = self.env.ref('isha_livingspaces.ishaliving_NEFT_email_send_Template')
                    self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(i.id, force_send=True)
                except Exception as e:
                    print(e)

            elif i.registration_status == 'dd_requested' and i.payment_mode == 'dd' and i.payment_status != 'Paid':
                try:
                    mail_template_id = self.env.ref('isha_livingspaces.ishaliving_dd_email_send_Template')
                    self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(i.id, force_send=True)
                except Exception as e:
                    print(e)

            elif i.registration_status in ['transfer_to_payment_gateway','Payment Link Sent','Approved'] and i.payment_mode == 'online' and i.payment_status != 'Paid':
                base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
                redirect_url = base_url + "/kshetragna/registrationform/paymentpage?registration_id=" + str(i.id)
                mail_template_id = self.env.ref('isha_livingspaces.ishaliving_space_mail_payment_template')
                i.sudo().write({"payment_link": redirect_url, "payment_link_sent_date": fields.Date.today()})
                try:
                    self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(i.id, force_send=True)
                except Exception as e:
                    print(e)

    @api.onchange('registration_status')
    def onchange_registration_status(self):
        if self.registration_status == 'neft_success':
            self.payment_status = 'Paid'
        if self.registration_status == 'dd_success':
            self.payment_status = 'Paid'
        if self.registration_status == 'dd_failed':
            self.payment_status = 'payment_failed'

    @api.onchange('email')
    def validate_mail(self):
        if self.email:
            match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', self.email)
            if match == None:
                raise ValidationError('E-mail ID is Invalid. Please Enter valid email')

    @api.onchange('mobile')
    def onchange_mobile(self):
        if self.mobile:
            if not self.mobile.isdigit():
                raise ValidationError('Integer only allowed for Mobile Number')

    def Approve(self):
        # """ For all registration,allow to make registration and get the passport copy,number
        #  and restrict the payment flow for non india.
        #  For Non indian ppl, once registation is done ,
        #  then that records has sent to approval from isha backend..
        #  once it approval is done from backend, email has been to sent to them to make payment"""
        if self.payment_mode == 'neft':
            message_id = self.env['kshetragna.confirm.wizard'].with_context(payment_mode=self.payment_mode).sudo().create(
                {'message': _("You are approving the documents. Do you wish to continue?")})
            return {
                'name': _('Confirmation'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'kshetragna.confirm.wizard',
                # pass the id
                'res_id': message_id.id,
                'context': {
                    'default_payment_mode': self.payment_mode,
                    # for passing Many2One field context value in Wizard form view
                },
                'target': 'new'
            }
        if self.payment_mode == 'dd':
            message_id = self.env['kshetragna.confirm.wizard'].create(
                {'message': _(
                    "You are approving the documents. Do you wish to continue?")})
            return {
                'name': _('Confirmation'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'kshetragna.confirm.wizard',
                # pass the id
                'res_id': message_id.id,
                'target': 'new'
            }

        if self.payment_mode == 'online':
            message_id = self.env['kshetragna.confirm.wizard'].create(
                {'message': _(
                    "You are approving the documents. Do you wish to continue?")})
            return {
                'name': _('Confirmation'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'kshetragna.confirm.wizard',
                # pass the id
                'res_id': message_id.id,
                'target': 'new'
            }
        # if self.payment_mode == 'neft':
        #     registration_status = "neft_requested"
        #     payment_status = "Pending"
        #     self.write({"payment_status": payment_status, "registration_status": registration_status})
        #     mail_template_id = self.env.ref('isha_livingspaces.ishaliving_NEFT_email_send_Template')
        #     try:
        #         self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(self.id, force_send=True)
        #     except Exception as e:
        #         print(e)
        #     return True
        #
        # elif self.payment_mode == 'dd':
        #     registration_status = "dd_requested"
        #     payment_status = "Pending"
        #     self.write({"payment_status": payment_status, "registration_status": registration_status})
        #     mail_template_id = self.env.ref('isha_livingspaces.ishaliving_dd_email_send_Template')
        #     try:
        #         self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(self.id, force_send=True)
        #     except Exception as e:
        #         print(e)
        #         return True
        # else:
        #     registration_status = "Payment Link Sent"
        #     payment_status = "Approved"
        #     self.write({"payment_status": payment_status, "registration_status": registration_status})
        #     base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        #     redirect_url = base_url + "/kshetragna/registrationform/paymentpage?registration_id=" + str(self.id)
        #     mail_template_id = self.env.ref('isha_livingspaces.ishaliving_space_mail_payment_template')
        #     self.sudo().write({"payment_link": redirect_url, "payment_link_sent_date": fields.Date.today()})
        #     try:
        #         self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(self.id, force_send=True)
        #     except Exception as e:
        #         print(e)
        #     return True

    def document_pending(self):
        view = self.env.ref('isha_livingspaces.kshetragna_document_submit_view_wissz')
        wiz = self.env['kshetragna.document.submit'].create({
            'register_id': self.id,
            'residence_country_code':self.country.code if self.country.code == 'IN' and self.nationality_id.code == 'IN' else "NONE"
        })

        # TDE FIXME: a return in a loop, what a good idea. Really.
        return {
            'name': _('Kshetragna Document Submit Form '),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'kshetragna.document.submit',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'res_id': wiz.id,
            'context': self.env.context,
        }

    # Registration to prescreened record creation process
    def prescreened(self):
        if self.registration_status == 'Enquiry':
                message_id = self.env['kshetragna.confirm.wizard'].create({'message': _("The record will be marked as pre-screened. Do you wish to continue?")})
                return {
                    'name': _('Confirmation'),
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'kshetragna.confirm.wizard',
                    # pass the id
                    'res_id': message_id.id,
                    'target': 'new'
                }

        if self.registration_status == 'payment_failed':
            view = self.env.ref('isha_livingspaces.kshetragna_neft_view_wiz')
            wiz = self.env['kshetragna.payment.mode'].create({
                'register_id': self.id
            })

            # TDE FIXME: a return in a loop, what a good idea. Really.
            return {
                'name': _('Kshetragna Payment Mode Form '),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'kshetragna.payment.mode',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': wiz.id,
                'context': self.env.context,
            }

    # Payment Method Process
    def alternate_payment(self):
        if self.payment_status == 'payment_failed':
            view = self.env.ref('isha_livingspaces.kshetragna_neft_view_wiz')
            wiz = self.env['kshetragna.payment.mode'].create({
                'register_id': self.id
            })

            # TDE FIXME: a return in a loop, what a good idea. Really.
            return {
                'name': _('Kshetragna Payment Mode Form '),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'kshetragna.payment.mode',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': wiz.id,
                'context': self.env.context,
            }

    # Payment Approval Process
    def payment_approval(self):
        if self.payment_mode == 'neft':
            message_id = self.env['kshetragna.confirm.wizard'].create(
                {'message': _("You are approving the payment method as 'NEFT'. Do you wish to continue?")})
            return {
                'name': _('Confirmation'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'kshetragna.confirm.wizard',
                # pass the id
                'res_id': message_id.id,
                'target': 'new'
            }
        if self.payment_mode == 'dd':
            message_id = self.env['kshetragna.confirm.wizard'].create(
                {'message': _(
                    "You are approving the payment method as 'DD'. Do you wish to continue?")})
            return {
                'name': _('Confirmation'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'kshetragna.confirm.wizard',
                # pass the id
                'res_id': message_id.id,
                'target': 'new'
            }

    def _raise_query_error(self, error):
        raise ValueError('Error with sending sms: %s' % error)

    @api.model
    def _send_smcountry_sms(self, data):

        url = self.env['ir.config_parameter'].sudo().get_param('kshetragna.sms_provider_base_url')
        params = {'User': self.env['ir.config_parameter'].sudo().get_param('kshetragna.sms_provider_username'),
                  'passwd': self.env['ir.config_parameter'].sudo().get_param('kshetragna.sms_provider_password'),
                  'mobilenumber': data['number'],
                  'message': data['message'],
                  'sid': self.env['ir.config_parameter'].sudo().get_param('kshetragna.sms_provider_sender_id'),
                  'mtype': 'N', 'DR': 'Y'}
        try:
            result = requests.get(url, params)
        except Exception as e:
            self._raise_query_error(e)
        if 'Invalid Password' in result.text:
            self._raise_query_error('Unexpected issue. Please contact Administrator.')
        if 'ERROR:' in result.text:
            self._raise_query_error(result.text)
        return True

class KshetragnaRegistrationConfiguration(models.Model):
    _name = 'kshetragna.configuration'
    _description = 'Kshetragna configuration'

    invalidate_all_payment_links_on = fields.Date(string="Invalidate all payment links on ")
    payment_link_validity = fields.Integer(string="Validity Number of days for individual payment links")
