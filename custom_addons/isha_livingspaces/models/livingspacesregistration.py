from lxml import etree
import json

from odoo import models, fields, api, exceptions, _
from odoo.exceptions import except_orm
from . import commonmodels
from . import livingspacestype
from . import livingspacesschedule
from datetime import date, timedelta, datetime
import base64
import logging, traceback
from copy import deepcopy

from odoo import models, fields, api, tools, _, exceptions, SUPERUSER_ID
from odoo.osv import expression
from odoo.exceptions import UserError, AccessError

from .. import isha_crm_importer
import uuid
from werkzeug import urls


_logger = logging.getLogger(__name__)
try:
    from html2text import HTML2Text
except ImportError as error:
    _logger.debug(error)

def html2text(html):
    """ covert html to text, ignoring images and tables
    """
    if not html:
        return ""

    ht = HTML2Text()
    ht.ignore_images = True
    ht.ignore_tables = True
    ht.ignore_emphasis = True
    ht.ignore_links = True
    return ht.handle(html)

class ValidationError(except_orm):
    """Violation of python constraints.

                 .. admonition:: Example

                     When you try to create a new user with a login which already exist in the db.
                 """

    def __init__(self, msg):
        super(ValidationError, self).__init__(msg)

class livingspacesspokenlanguage(models.Model):
    _name="living.spaces.spokenlanguage"

    livingspaceslanguages=fields.Many2one("livingspaces.registration", string="Contact Name")
    languagesspoken=fields.Char(String="Spoken Languages")

class livingspacesRegistration(models.Model):
    _name = 'livingspaces.registration'
    _description = 'Living Spaces Registration Forms'
    _rec_name = 'first_name'
    #_order = 'last_modified_dt'
    # participant_id = fields.Integer(string="ParticipantId", default=lambda self: self.env['ir.sequence'].next_by_code('increment_your_field'))

    def _get_default_access_token(self):
        return str(uuid.uuid4())

    #basic profile info
    first_name = fields.Char(string="First Name", required=True)
    last_name = fields.Char(string="Last Name", required=True)
    gender = fields.Selection([('Male', 'Male'), ('Female', 'Female'), ('Others', 'Others')], string='Gender', required=False)
    dob = fields.Date(string='Date Of Birth', required=False)

    # contact details
    countrycode = fields.Char(string='Country Tel code', required=False, index=False)
    phone = fields.Char(string='Mobile Number', required=False, index=True)
    countrycode2 = fields.Char(string='Country Tel code 2', required=False, index=False)
    phone2 = fields.Char(string='Mobile Number 2', required=False, index=True)
    wacountrycode = fields.Char(string='Whatsapp Country Tel code', required=False, index=False)
    waphone = fields.Char(string='Whatsapp Mobile Number', required=False, index=True)
    country = fields.Many2one('res.country', 'Country of residence')
    state = fields.Many2one('res.country.state', 'State/province')
    pincode = fields.Char(string="Pincode/Zipcode", required=False, index=True)
    city = fields.Char(string='City/Town/District', required=False)
    nationality_id = fields.Many2one('res.country', 'Nationality Id')
    occupation = fields.Char(string='Occupation', required=False)
    marital_status=fields.Char(string="Marital Status",required=False)
    spoken_languages=fields.One2many("living.spaces.spokenlanguage","livingspaceslanguages", string="Spoken Languages",required=False)
    #fields.Selection([('TAMIL','Tamil'),('KANNADA','Kannada'),('MALAYALAM','Malayalam'),
                                  #     ('TELUGU','Telugu'),('ENGLISH','English'),('OTHERS','Others')], String='Spoken Languages',
                                   #   default='ENGLISH', required=False)
    other_spoken_lang=fields.Char(string='Other Languages', required=False)
    dualcityname = fields.Many2one('res.country', 'Dual Nationality Id')
    education_qualification = fields.Char(String="Educational Qualification")
    lead_interest=fields.Selection([('COLD','Cold'),('WARM','Warm'),('HOT','Hot')], String='Lead Interest',
                                      default='COLD', required=False)
    lead_status_map = fields.Many2one('living.spaces.lead.status', string='Lead Status')
    lead_status = fields.Char(related='lead_status_map.lead_status')
    admin_comment=fields.Text(String='Admin Comment')
    ytfollower = fields.Boolean(String='Youtube Follower')
    emfollower = fields.Boolean(String='E-Media Follower')
    otfollower = fields.Boolean(String="Others")
    # Program history
    programhistorynote = fields.Char(string=' ', size=500, readonly="1",
                                     default='Pls provide the history of Isha Yoga Programs you have undergone')
    programhistory = fields.One2many('living.spaces.history','programregistration', string="Program History", required=False)
    programapplied = fields.Many2one('livingspaces.program.schedule',string='Select program to register')

    accommodation_type = fields.Selection([('studio','Studio'),('ibhk','1BHK'),('2bhk','2BHK'),('others','Others')], string='Accommodation Type')
    custom_accommodation = fields.Char(string='Custom Accommodation Type')

    applicant_budget = fields.Selection([('standard','Standard'),('others','Others')],string='Applicant\'s Budget')
    custom_applicant_budget = fields.Char(string='Custom Budget')

    member_count = fields.Integer(string='Family members count')


    def leadstatusbut(self):
        print("Changing lead status")

    def leadinterestbut(self):
        print("Changing lead interest")

    @api.depends('compute_field')
    def get_user(self):
        retval = True;
        try:
            res_user = self.env['res.users'].search([('id', '=', self._uid)])
            if res_user.has_group('isha_livingspaces.group_livingspaces_backofficeuser'):
                retval = False
            else:
                retval = True
        except Exception as e:
            retval = True
            _logger.debug(error)
        finally:
            self.compute_field = retval

    registration_status = fields.Selection([('Pending','Pending'),('Rejected','Rejected'),('Rejection Review','Rejection Review'),('Payment Link Sent','Payment Link Sent'),('Add To Waiting List','Add To Waiting List'),('Cancel Applied','Cancel Applied'),('Cancel Approved','Cancel Approved'),('Paid','Paid'),('Confirmed','Confirmed'),('Approved','Approved'),('Withdrawn','Withdrawn'),('Refunded','Refunded'),('Incomplete','Incomplete'),('Change Of Participant','Change Of Participant')], string='Registration Status',  size=100, default='Pending')
    ischangeofparticipant = fields.Boolean(string="Change of Participant Applied", default=False)
    participant_comments =  fields.Char(string = 'Comments', size=300)
    participant_email = fields.Char(string = 'Email', size=100)
    status_comments =  fields.Text(string = 'Comments', size=500)
    ocistatus = fields.Boolean(string="oci status")
    # other status fields
    incompleteregistration_pagescompleted = fields.Integer(string='Registration pages completed', default=1)
    incompleteregistration_email_send = fields.Boolean(string="Incomplete Registration Email Send", default=False)
    incompleteregistration_sms_send = fields.Boolean(string="Incomplete Registration SMS Send", default=False)
    registration_email_send = fields.Boolean(string="Registration Email Send", default=False)
    registration_sms_send = fields.Boolean(string="Registration SMS Send", default=False)
    rejection_email_send = fields.Boolean(string="Rejection Email Send", default=False)
    rejection_sms_send = fields.Boolean(string="Rejection SMS Send", default=False)
    harequest_email_send = fields.Boolean(string="HA Addinfo Email Send", default=False)
    harequest_sms_send = fields.Boolean(string="HA Addinfo SMS Send", default=False)
    cancel_email_send = fields.Boolean(string="cancel_email_send")
    access_token = fields.Char('Access Token', default=lambda self: self._get_default_access_token(), copy=False)
    public_url = fields.Char("Public link", compute="_compute_call_public_url")
    #
    addtowaitlist_email_send = fields.Boolean(string="Add To WaitList Email Send", default=False)
    addtowaitlist_sms_send = fields.Boolean(string="Add To WaitList SMS Send", default=False)
    waitinglist_date = fields.Date()
    waitinglist_email_count = fields.Integer(default=0)
    waitinglist_period_inmonth = fields.Integer(default=0)
    waitinglist_last_communicate_sch = fields.Many2one('livingspaces.program.schedule')
    waitinglist_last_communicate_dt = fields.Date(string="Future Programs Last Comm Date")
    waitinglist_email_send = fields.Boolean(string="Future Programs Email Send", default=False)
    waitinglist_sms_send = fields.Boolean(string="Future Programs SMS Send", default=False)
    #
    paymentlink_email_send = fields.Boolean(string="Payment Link Email Send", default=False)
    paymentlink_sms_send = fields.Boolean(string="Payment Link SMS Send", default=False)
    auto_approval_comments = fields.Char("Auto Approval Comments", size=100)
    #
    registration_agreed = fields.Boolean(string="I Agree", default=False)    
    # amount_paid  = fields.Float(string = 'Amount Paid')
    # date_paid =  fields.Date()
    date_confirm  =  fields.Date()

    #
    date_withdrawn = fields.Date()
    date_cancelapproved = fields.Datetime()
    refund_eligible_ondaysbefore = fields.Integer(string = 'Days Before')
    refund_eligible_onpercentage = fields.Float(string = 'Eligible Percentage') 
    refund_eligible = fields.Float(string = 'Refund Amount Eligible')    

    #
    ha_resubmit_date = fields.Date()
    ha_resubmitted = fields.Boolean(string="HA Resubmitted", default=False)    

    #
    date_refund = fields.Datetime(string="Refund Done On")
    refund_amount = fields.Float(string = 'Actual Refunded Amount')
    refund_comments = fields.Text(string = 'Comments')

    contact_id_fkey = fields.Many2one(comodel_name='res.partner',string='Contact')
    # Related fields
    pgm_tag_ids = fields.Many2many(related='contact_id_fkey.pgm_tag_ids',string='Ishangam Prgoram Tags')
    tag_ids = fields.Many2many(related='contact_id_fkey.category_id',string='Contact Tags')

    confirmation_email_send = fields.Boolean(string="Confirmation Email Send", default=False)
    confirmation_sms_send = fields.Boolean(string="Confirmation SMS Send", default=False)

    pushedtocico = fields.Boolean(string="Pushed to CICO", default=False)
    
    last_modified_dt = fields.Datetime(string="Last Modified DT")
    checkindatetime  = fields.Datetime(string="Check-in Date Time from CICO")
	
    cancelapplied_email_send = fields.Boolean(string="Cancellation Applied Email Send", default=False)
    cancelapproved_email_send = fields.Boolean(string="Cancellation Approved Email Send", default=False)
    cancelrefund_email_send = fields.Boolean(string="Cancellation Refunded Email Send", default=False)

    cancelapplied_sms_send = fields.Boolean(string="Cancellation Applied Email Send", default=False)
    cancelapproved_sms_send = fields.Boolean(string="Cancellation Approved Email Send", default=False)
    cancelrefund_sms_send = fields.Boolean(string="Cancellation Refunded Email Send", default=False)

    # payment info
    orderid = fields.Char(string='Order ID', size=80)
    billinginfoconfirmation = fields.Boolean(string="Billing Info Confirmation", default=False)
    billing_name = fields.Char(string='Billing Name', size=60)
    billing_address = fields.Char(string='Billing Address', size=150)
    billing_city = fields.Char(string='Billing City', size=30)
    billing_state = fields.Char(string='Billing State', size=30)
    billing_zip = fields.Char(string='Billing Zip', size=15)
    billing_country = fields.Char(string='Billing Country', size=50)
    billing_tel = fields.Char(string='Billing Tel', size=20)
    billing_email = fields.Char(string='Billing Email', size=70)
    payment_status = fields.Char(string='Payment Status', size=70)
    total_paid  = fields.Float(string = 'Total Amount Paid', default=0)
    amount_paid  = fields.Float(string = 'Amount Paid', default=0)
    date_paid = fields.Datetime()
    ereceiptgenerated = fields.Boolean(string="eReceipt Generated", default=False)
    ereceiptreference = fields.Char(string="eReceipt Reference", size=50)
    ereceipturl = fields.Char(string="eReceipt URL", size=500)
    paymenttransactions = fields.One2many('livingspaces.paymenttransaction', 'programregistration', string="Payment Transaction", required=False)
    payment_initiated_dt = fields.Datetime()


    cancelapplied_emailsms_pending = fields.Boolean(default=False)
    copapplied_emailsms_pending = fields.Boolean(default=False)
    active = fields.Boolean(string='active', default=True)

    show_upgrade = fields.Boolean(compute="_compute_show_upgrade")
    othertransactions = fields.One2many('livingspaces.registration.transactions', 'programregistration', string="Transactions", required=False)

    @api.model
    def fields_get(self, fields=None):
        fields_to_show = ['first_name','last_name','phone','country','state','pincode','city','nationality_id', 'programapplied', 'participant_email', 'oco_id', 'access_token', 'pushedtocico', 'checkindatetime', 'ereceiptgenerated', 'ereceiptreference', 'ereceipturl', 'registration_status', 'id']
        res = super(livingspacesRegistration, self).fields_get()
        for field in res:
            if (field in fields_to_show):
                res[field]['selectable'] = True	
                res[field]['sortable'] = True
            else:
                res[field]['selectable'] = False
                res[field]['sortable'] = False
        return res

    @api.constrains('programapplied')
    def validate_repititionallowed(self):
        repititionallowed=self.programapplied.pgmschedule_repetitionallowed;

        type = self.programapplied.pgmschedule_programtype.id;
        sql = "SELECT prg.participant_email, rps.pgmschedule_enddate  FROM public.living_spaces_type  as ptp  inner join public.livingspaces_program_schedule rps on rps.pgmschedule_programtype = ptp.id   and  ptp.id =  %s   inner join public.livingspaces_registration prg  on prg.programapplied = rps.id and prg.id != %s where prg.participant_email = '%s' and attendance_status ='Present' order  by  rps.pgmschedule_enddate desc;"% (type, self.id,self.participant_email)
        self.env.cr.execute(sql)
        res_all = self.env.cr.fetchall()
        reccount = len(res_all)
        if(reccount >= repititionallowed):
            if(reccount > 0 and res_all is not None and res_all[0] is not None):
                if (res_all[0][1] is not None):
                    raise exceptions.ValidationError("You have already gone through the program %s, %s times which is the max times allowed and last time you attended is on  %s " % (self.programapplied.pgmschedule_programtype.programtype, reccount, res_all[0][1]))
            elif(reccount > 0):
                raise exceptions.ValidationError(
                    "You have already gone through the program %s, %s times which is the max times allowed" % (
                    self.programapplied.pgmschedule_programtype.programtype, reccount))
        else:
            num_months =0;
            if (reccount > 0 and res_all is not None and res_all[0] is not None):
                if (res_all[0][1] is not None):
                    enddate = res_all[0][1]
                    startdate = self.programapplied.pgmschedule_startdate
                    if(enddate < startdate):
                        temp = (startdate.year - enddate.year) * 12 + (startdate.month - enddate.month)
                        num_months = temp
                        print('months: ', num_months)

        if(self.programapplied.pgmschedule_gapbetweenprgm >= num_months ):
            if (reccount > 0 and res_all is not None and res_all[0] is not None):
                if(res_all[0][1] is not None):
                    raise exceptions.ValidationError("You have already gone through the program %s "% self.programapplied.pgmschedule_programtype.programtype+" on %s " %  res_all[0][1] +". "
                    "This program requires a gap of %s " % self.programapplied.pgmschedule_gapbetweenprgm + "months before re-applying,"
                    " Pls re-apply on or after %s " % self.programapplied.pgmschedule_gapbetweenprgm + "months from previous attendance")
            elif (reccount > 0):
                raise exceptions.ValidationError(
                    "You have already gone through the program %s " % self.programapplied.pgmschedule_programtype.programtype +
                    ".This program requires a gap of %s " % self.programapplied.pgmschedule_gapbetweenprgm + "months before re-applying,"
                    " Pls re-apply on or after %s " % self.programapplied.pgmschedule_gapbetweenprgm + "months from previous attendance")

    def checkCOP1(rec):
        try:
            if rec.create_date == False:
                return False
            else:
                if (rec.programapplied.exists()):
                    if (rec.programapplied.pgmschedule_startdate < fields.Date.today()):
                        return False
                    else:
                        if (rec.ischangeofparticipant == False):
                            if (rec.registration_status == 'Paid' or rec.registration_status == 'Confirmed'):
                                return True
                            else:
                                return False
                        else:
                            return False
                else:
                    return False
        except Exception as e:
            print(e)
            return False

    #
    def _compute_call_public_url(self):
        """ Computes a public URL  """
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        for rec in self:
            rec.public_url = urls.url_join(base_url, "livingspaces/registration/%s" % (rec.access_token))

    #def callReportRegistration(self):
    #    return self.env.ref('isha_livingspaces.report_registration').report_action(self)

    def open_changeprogramdate(self):
        print('Open Change Program')
        print(self.id)
        if (self.payment_status == 'Initiated'):
            raise exceptions.ValidationError('Payment have been initiated by the participant. Please try after some time')
        else:
            return {
                'name':'Change Program Date',
                'context':{'default_active_id':self.id,'default_participant_name':self.first_name,'default_programapplied':self.programapplied.id,'default_participant_programtype':self.participant_programtype.id},
                'view_type': 'form',
                'res_model': 'livingspaces.registration.changeprogramdate',
                'view_id': False,
                'view_mode': 'form',
                'type': 'ir.actions.act_window',
                'target':'new'
            }

    def load_resend_mail(self):
        print('call method')
        return {
			'name':'Resend Email/SMS To Participant',  
			'context':{'default_active_id': self.id, 'default_participant_name': self.first_name },
			'view_type': 'form',
			'res_model': 'livingspaces.registration.resendmail',
			'view_id': False,
			'view_mode': 'form',
			'type': 'ir.actions.act_window',
			'target':'new'
        }

    def initiate_upgrade(self):
        print('inside initiate_upgrade')
        if (self.payment_status == 'Initiated'):
            raise exceptions.ValidationError('Payment have been initiated by the participant. Please try after some time')
        else:
            return {
                'name':'Package Upgrade',
                'context':{'default_active_id':self.id,'default_participant_name':self.first_name,'default_programapplied':self.programapplied.id,'default_package_cost': self.packageselection.pgmschedule_packagetype.packagecost},
                'view_type': 'form',
                'res_model': 'livingspaces.registration.packageupgrade',
                'view_id': False,
                'view_mode': 'form',
                'type': 'ir.actions.act_window',
                'target':'new'
            }

    def initiate_cop(self):
        print('inside initiate_cop')
        reclist = self.env['livingspaces.registration'].sudo().search([('id','=',self.id)])
        for rec in reclist:
            if (rec.show_cop):
                base_url = self.env['ir.config_parameter'].sudo().get_param('livingspaces.ccavcallbackurl')
                cop_token = self._get_default_access_token()
                public_url = urls.url_join(base_url, "livingspaces/registration/changeofparticipant/%s" % (cop_token))
                rec.write({'cop_access_token':cop_token, 'cop_url': public_url})
                self.env['livingspaces.notification'].sudo()._sendChangeOfParticipantEmailCore(rec)
                message = {
                        'type':'ir.actions.client',
                        'tag':'display_notification',
                        'params':
                        {
                            'title':_('Information!'),
                            'message': 'Change of participant request sent to participant',
                            'sticky':False
                        }
                    }
                return message
            else:
                raise exceptions.ValidationError("Change of participant is not allowed, pre-requisite not matched")


    def get_contact_dict(self, vals):

        return  {
            'name': vals['first_name']+ ' '+ vals['last_name'],
            'phone_country_code':vals['countrycode'] if 'countrycode' in vals else None,
            'phone': vals['phone'] if 'phone' in vals else None,
            'email':vals['participant_email'] if 'participant_email' in vals else None,
            'gender': isha_crm_importer.isha_crm.ContactProcessor.getgender(vals['gender']) if 'gender' in vals else None,
            'marital_status': isha_crm_importer.isha_crm.ContactProcessor.getMaritalStatus(vals['marital_status']) if 'marital_status' in vals else None,
            'dob': vals['dob'] if 'dob' in vals else None,
            'occupation': vals['occupation'] if 'occupation' in vals else None,
            'street': vals['address_line'] if 'address_line' in vals else None,
            'city':vals['city'] if 'city' in vals else None,
            'state_id': vals['state'] if 'state' in vals else None,
            'state': self.env['res.country.state'].sudo().browse(vals['state']).name if 'state' in vals and vals['state'] else None,
            'zip': vals['pincode'] if 'pincode' in vals else None,
            'country_id': vals['country'] if 'country' in vals else None,
            'country':self.env['res.country'].sudo().browse(vals['country']).code if 'country' in vals and vals['country'] else None,
            'nationality_id':vals['nationality_id'] if 'nationality_id' in vals else None,
            'nationality': self.env['res.country'].sudo().browse(vals['nationality_id']).code if 'nationality_id' in vals and vals['nationality_id'] else None,
        }



    @api.model
    def create(self, vals):
        _logger.info(str(self.env.uid))
        # try:
        #     contact_rec = self.env['res.partner'].sudo().create( self.get_contact_dict(vals) )
        #     vals['contact_id_fkey'] = contact_rec.id
        # except Exception as ex:
        #     tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        #     _logger.error(tb_ex)
        #     pass
        vals['last_modified_dt'] = datetime.now()
        reference = self.env['ir.config_parameter'].sudo().get_param('livingspaces.referenceid')
        reference = int(reference)

        #vals['reference_id'] = datetime.now().strftime("%d-%m-%Y") + "-00" + str(reference);
        #reference = reference + 1;
        #self.env['ir.config_parameter'].sudo().set_param('livingspaces.referenceid', reference)

        rec = super(livingspacesRegistration, self).create(vals)
        self.env['livingspaces.notification'].sudo()._sendRegistrationEmailSMSCore(rec)
        # print( vals['reference_id'])

        ha_preapprove_status = True

        for each_field in vals:
            field_name = each_field
            if field_name.startswith("is_disease"):
                field_value = vals[field_name]
                if field_value:
                    ha_preapprove_status = False
                    break

        #rec.ha_status = ha_preapprove_status
        print('Preapprove Status',ha_preapprove_status)
        return rec

    def load_print(self):
        self.sendPaymentLinkEmail()
       
    def load_print1(self):
        _logger.info('************************ load print is called ************************ ')
        print('report')
        #return self.env.ref('isha_livingspaces.report_registration').report_action(self)
        #self.statusUpdateandSendPaymentLinkEmail()
        #self.env['livingspaces.cleanup'].sudo().cleanup()
        '''rec = self.env['livingspaces.registration'].sudo().search([('id','=',5)])
        source = self.env['livingspaces.registration'].sudo().search([('id','=',4)])
        
        refund_data = self.env['livingspaces.payments']._calculateRefundAmount(source, rec.changeofparticipant_applied_dt)

        print(refund_data)'''

        list = self.env['livingspaces.registration'].sudo().search([('id','=',4)])

        for rec in list:
            rec.write({ 'paymentlink_email_send': False,
					'paymentlink_sms_send' : False
                    })
        
        '''rec.write({'ereceiptgenerated': False})
        rec.write({'registration_status': 'Confirmed'})
        rec.write({'amount_paid': 10})
        '''
        #rec.write({'registration_status': 'Confirmed', 'payment_status': 'Success', 'pushedtocico': True })
        #'amount_paid': 100, 'date_paid': datetime.now()})
        #rec.write({'registration_status': 'Change Of Participant'})
        # rec.write({ 'ischangeofparticipant': True,
        #             'changeofparticipant_applied_dt': datetime.now(),
        #             'changeofparticipant_ref': 1 })
        #rec.write({'payment_status': ''})
        #return self.env.ref('isha_livingspaces.report_registration').report_action(self)

    def call_all_jobs(self):
        for rec in self:
            print(rec.public_url)
        self.checkPaymentStatusForPendingRecords()
        #self.checkAndGenerateEreceipt()
        #self.updateAndSendProgramConfirmation()
        #self.env['livingspaces.cicoapi']._pushRegistrationsToCICO()
        #self.sendWaitingListScheduledProgram()
        #self.sendRegistrationEmail()
        #self.statusUpdateandSendPaymentLinkEmail()
        #self.sendRegistrationHAAdditionalInfoEmail()
        #self.sendRegistrationRejectionEmail()
	
    def changetrigger(self):
        print('trigger')


    #
    def checkPaymentStatusForPendingRecords(self):
        self.env['livingspaces.paymentstatus'].sudo()._checkPendingPaymentStatus()
	
    #
    
    def checkAndGenerateEreceipt(self):
        self.env['livingspaces.paymentstatus'].sudo()._checkAndGenerateEreceipt()
	
    #

    #    
    def updateAndSendProgramConfirmation(self):
        self.env['livingspaces.notification'].sudo()._updateAndSendProgramConfirmation()
	
    def sendCancelAppliedConfirmation(self):
        self.env['livingspaces.notification'].sudo()._sendCancelAppliedConfirmation()
	
    def sendCOPAppliedConfirmation(self):
        self.env['livingspaces.notification'].sudo()._sendCOPAppliedConfirmation()
	
    #
    def statusUpdateandSendPaymentLinkEmail(self):
        foundrec = 0
        rejlist = self.env['livingspaces.registration'].sudo().search([('registration_status','=','Pending')])
        print(rejlist)
        for rec in rejlist:
            foundrec = 1
            try:
                self.statusUpdateandSendPaymentLinkEmailCore(rec)
            except Exception as e:
                print(e)                

        if (foundrec == 1):
            print('registration status update completed, calling email and sms program')
        else:
            print('no records found to update registration status, calling email and sms program')
        self.sendPaymentLinkEmail()

    #
    def statusUpdateandSendPaymentLinkEmailCore(self, rec):
        print('inside statusUpdateandSendPaymentLinkEmailCore')
        auto_approval_comments = ''

        #
					
        if (rec.country.name != 'India' or rec.nationality_id.name != 'India'):
            print('checking overseas')
            if (rec.oco_status == 'Pending' and rec.oco_approval_overide == False):
                print('oco status is pending and override is not selected')
                return
            elif (rec.oco_status != 'Approved' and rec.oco_approval_overide == False):
                auto_approval_comments = 'Rejected due to oco rejection'
                rec.write({
                    'registration_status': 'Rejection Review',
                    'auto_approval_comments': auto_approval_comments,
                    'last_modified_dt': datetime.now()
                })
                print('oco rejection review marked')
                return
        
        #   

        if (rec.bl_status == 'Pending' and rec.blacklisted_overide == False):
            print('waiting for bl status')
            return
        elif (rec.bl_status != 'Verified' and rec.blacklisted_overide == False):
            auto_approval_comments = 'Rejected due to bl rejection'
            rec.write({
                'registration_status': 'Rejection Review',
                'auto_approval_comments': auto_approval_comments,
                'last_modified_dt': datetime.now()
            })
            print('bl rejection review marked')
            return

        #

        if (rec.prerequisite_pgm != 'Matched' and rec.prerequisite_pgm_overide == False):
            print('pre request not matched marking rejection review')
            auto_approval_comments = 'Rejected due to pre-requisite not matched'
            rec.write({
                'registration_status':  'Rejection Review',
                'auto_approval_comments': auto_approval_comments,
                'last_modified_dt': datetime.now()
            })
            return

        #
        print('checking for seat avail')

        if (rec.use_emrg_quota == True):
            emergency_seatscount = (rec.packageselection.pgmschedule_packageemergencyseats - rec.packageselection.pgmschedule_emergencyseatspaid)
            if (emergency_seatscount > 0):
                rec.write({
                    'registration_status': 'Approved',
                    'last_modified_dt': datetime.now(),
                    'auto_approval_comments': 'Seats approved from emergency quota',
                    'paymentlink_email_send': False,
					'paymentlink_sms_send' : False
                })
                return
            else:
                rec.write({
                    'registration_status': 'Rejection Review',
                    'auto_approval_comments': 'Rejected due to seats not available',
                    'last_modified_dt': datetime.now()
                })
                print('seats not available rejection review marked')
                return
        else:
            seatcount = rec.packageselection.pgmschedule_packagenoofseats - rec.packageselection.pgmschedule_regularseatspaid
            if (seatcount > 0):
                rec.write({
                    'registration_status': 'Approved',
                    'last_modified_dt': datetime.now(),
                    'paymentlink_email_send': False,
					'paymentlink_sms_send' : False
                })
                return
            else:
                rec.write({
                    'registration_status': 'Rejection Review',
                    'auto_approval_comments': 'Rejected due to seats not available',
                    'last_modified_dt': datetime.now()
                })
                print('seats not available rejection review marked')
                return
        
        #

    #
    def sendPaymentLinkEmail(self):
        foundrec = 0
        rejlist = self.env['livingspaces.registration'].sudo().search([('registration_status','=','Approved'),
        '|',('paymentlink_email_send','=',False),('paymentlink_sms_send','=',False),('country.name','=','India')])
        print(rejlist)
        for rec in rejlist:
            foundrec = 1
            self.env['livingspaces.notification'].sudo()._sendPaymentLinkEmailCore(rec)
        if foundrec == 1:
            print('paymentlink: sending email and sms completed for all request')
        else:
            print('paymentlink: no records found for sending email and sms..')

    #
    def sendRegistrationEmail(self):
        foundrec = 0
        rejlist = self.env['livingspaces.registration'].sudo().search([('registration_status','=','Pending'),
        '|',('registration_email_send','=',False),('registration_sms_send','=',False),('country.name','=','India')])
        print(rejlist)
        for rec in rejlist:
            foundrec = 1
            self.env['livingspaces.notification'].sudo()._sendRegistrationEmailSMSCore(rec)
        if foundrec == 1:
            print('registrations: sending email and sms completed for all request')
        else:
            print('registrations: no records found for sending email and sms..')

    #
    def sendPartialRegistrationEmail(self):
        foundrec = 0
        rejlist = self.env['livingspaces.registration'].sudo().search([('registration_status','=','Incomplete'),
        '|',('incompleteregistration_email_send','=',False),('incompleteregistration_sms_send','=',False),('country.name','=','India')])
        print(rejlist)
        _logger.info("Inside sendPartialRegistrationEmail")
        _logger.info(str(rejlist))
        for rec in rejlist:
            foundrec = 1
            self.env['livingspaces.notification'].sudo()._sendRegistrationPartiallyCompletedFormEmailSMS(rec)
        if foundrec == 1:
            print('partial registrations: sending email and sms completed for all request')
            _logger.info('partial registrations: sending email and sms completed for all request')
        else:
            print('partial registrations: no records found for sending email and sms..')
            _logger.info('partial registrations: no records found for sending email and sms..')

    def sendWaitingListScheduledProgram(self):
        self.env['livingspaces.notification'].sudo()._sendWaitingListScheduledProgram()
    #    
    def sendRegistrationRejectionEmail(self):
        foundrec = 0
        rejlist = self.env['livingspaces.registration'].sudo().search([('registration_status','=','Rejected'),
         '|',('rejection_email_send','=',False),('rejection_sms_send','=',False),('country.name','=','India')])
        print(rejlist)
        for rec in rejlist:
            foundrec = 1
            self.env['livingspaces.notification'].sudo()._sendRegistrationRejectionEmailCore(rec)
        if foundrec == 1:
            print('rejections: sending email and sms completed for all request')
        else:
            print('rejections: no records found for sending email and sms..')

    #
    def sendRegistrationHAAdditionalInfoEmail(self):
        foundrec = 0
        rejlist = self.env['livingspaces.registration'].sudo().search(['|',('harequest_email_send','=',False),('harequest_sms_send','=',False),('country.name','=','India')])
        print(rejlist)
        for rec in rejlist:
            foundrec = 1
            self.env['livingspaces.notification'].sudo()._sendRegistrationHAAdditionalInfoEmailCore(rec)
        if foundrec == 1:
            print('HA addinfo: sending email and sms completed for all request')
        else:
            print('HA addinfo: no records found for sending email and sms..')

    def onchange_package(self,val):
        print('package')
        print(self.id)

    def getProgramAttendance(self, contact_id, pgm_category = None):
        pgm_recs = self.env['program.attendance'].getProgramAttendance(contact_id, pgm_category)
        print("pgm-recs", pgm_recs)
        return pgm_recs


    # @api.onchange('programapplied')
    # def programapplied_onchange(self):
    #     print('on change program')
    #     recs = self.env['livingspaces.program.schedule.roo                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        mdata'].search([('pgmschedule_programname','=',self.programapplied.id)])
    #     print(recs)

class LeadStatusMapping(models.Model):
    _name = 'living.spaces.lead.status'
    _rec_name = 'lead_status'

    lead_status = fields.Char(sting='Status')
    sub_status = fields.Char(sting='Sub Status')
    level = fields.Char(sting='Level')
    remark = fields.Char(sting='Remark')

class ProgramHistory(models.Model):
    _name = 'living.spaces.history'
    _description = 'Program History'
    _rec_name = 'programname'
    #_order = 'packagecode'
    #_rec_name = 'packagecode'
    #_sql_constraints = [('unique_pkgparameter_packagecode','unique(programtype, packagecode)','Cannot have duplicate package code, give different code')]
    # program history

    programregistration = fields.Many2one('livingspaces.registration', string='program registration')
  #   programname = fields.Many2one('living.spaces.type', string='Program Type', required=True, domain=['&',('lob','=','IPC'),('displaypgmhistory','=',True)])
    programname = fields.Many2one('master.program.category', string='Program Type', required=True)
    pgmdate = fields.Date(string='Program Date',required=True)
    teachername = fields.Char(string='Teacher name', size=100, required=False)
    pgmcountry = fields.Many2one('res.country', string = 'Program Country', required=True)
    pgmstate = fields.Many2one('res.country.state', 'State/province')
    pgmlocation = fields.Many2one('isha.center', string = 'Program Location', required=True)
    # domain=['lob','=','IPC'])


class IdproofList(models.Model):
    _name = "idproof.list"
    _description = "List of Id proofs accepted by IYC"
    _rec_name = 'idproof_type'
#    _sql_constraints = ['unique_idproof','unique(idproof_type)','Id proof type given already exists']

    idproof_type = fields.Char(string='Id proof name')

class MasterLob(models.Model):
    _name = 'master.lob'
    _description = "Master list of line of businesses"
    _rec_name = 'lobname'

    lobname = fields.Char(string='Lob Name', required=False)

class TeacherNames(models.Model):
    _name = 'living.spaces.teachernames'
    _description = 'Program teacher names'
    _rec_name = 'teachername'
    _sql_constraints = [('unique_teacher_name','unique(teacherlob,teachername)','Cannot have duplicate teacher name in same LOB pls modify the name')]

    teachername = fields.Char(string='Teacher name', size=100, required=True)
    teacherlob = fields.Many2one('master.lob')
    pgms_trained = fields.Many2one('living.spaces.type',string = 'Program Type')
#
# class MasterRegions(models.Model):
#     _name = 'master.regions'
#     _description = "Master list of regions"
#     _rec_name = 'regionname'
#
#     regionname = fields.Char(string='Region Name', required=True)
#     countryname = fields.Many2one('res.country')
#     lobname = fields.Many2one('master.lob')
#
# class MasterCenters(models.Model):
#     _name = 'master.centers'
#     _description = "Master list of program centers"
#     _rec_name = 'centername'
#
#     centername = fields.Char(string='Center Name',required=True)
#     regionname = fields.Many2one('master.regions','regionname')
#     countryname = fields.Many2one('res.country')
#     statename = fields.Many2one('res.country.state')
#     lob = fields.Many2one('master.lob')
#

class idproof_attach(models.Model):
    _name = "idproof.attachment"
    _description = "ID Proof Attachment"
    idproof_id = fields.Many2one('res.partner', string='idproof_attach')
    attached_file = fields.Many2many(comodel_name="ir.attachment",
                                     relation="m2m_ir_attached_file_rel",
                                     column1="m2m_id", column2="attachment_id", string="File Attachment",
                                     required=True)
    idproof_document_type = fields.Many2one('documenttype', string="ID Type", required=True)
    # Removing as per the UAT feedback
    idproof_file = fields.Char(string="File Name", required=True)
    active = fields.Boolean('Active', default=True)

    state = fields.Boolean('State', default=True)

    @api.onchange('attached_file')
    def limit_attached_file(self):
        attachement = len(self.attached_file)
        if attachement > 1:
            raise ValidationError(_('You can add only 2 files per ID Proof'))


    # self.packagetotalseats = (self.packagenoofseats + self.packageemergencyseats)

class DocumentType(models.Model):
    _name = "documenttype"
    _description = "document_type"

    name = fields.Char(string="Document Type Name", required=True)
    active = fields.Boolean('Active', default=True)


class RequestAttachment(models.Model):
    _name = "livingspaces.request.attachment"
    _description = "Generic Request Attachment"

    generic_request_id = fields.Many2one('livingspaces.registration', string='attachment')
    attached_file = fields.Many2many(comodel_name="ir.attachment",
                                     relation="livingspaces_m2m_ir_attached_file_rel",
                                     column1="request_m2m_id", column2="attachment_id", string="Attachment(s)", required=True)
    attachment_type_id = fields.Many2one('documenttype', string="Attachment Type", required=True)
    active = fields.Boolean('Active', default=True)

    # @api.onchange('attached_file')
    # def limit_attached_file(self):
    #     attachement = len(self.attached_file)
    #     if attachement > 1:
    #         raise ValidationError(_('You can add only 2 files per ID Proof'))

class livingspacesParticipantQuestionBank(models.Model):
    _name = "livingspaces.qa"
    _description = "Participant HA Question and Answers"
    _rec_name = 'question_bank'
    _sql_constraints = [('unique_participant_qa','unique(first_name,question_bank)','Cannot have duplicate question, give different data')]


    first_name = fields.Many2one('livingspaces.registration',string = 'Participant Name')
    question_bank = fields.Many2one('livingspaces.qbnk', string='Questions')
    answer_text = fields.Text(string='Answer')

class livingspacesParticipantHAReports(models.Model):
    _name = 'livingspaces.hadocs'
    description = 'Living Spaces Participant HA Reports'
    _rec_name = 'ha_attachment'

    participant = fields.Many2one('livingspaces.registration',string = 'Participant Name')
    ha_attachment = fields.Binary(string="HA Report",)
    ha_filename = fields.Char(string='HA Report')

class PaymentTransaction(models.Model):
    _name = "livingspaces.paymenttransaction"
    _description = "Living Spaces Payment Transaction"
    
    programregistration = fields.Many2one('livingspaces.registration', string='program registration')
    first_name = fields.Char(related="programregistration.first_name",store=False)
    last_name = fields.Char(related="programregistration.last_name",store=False)
    programtype = fields.Char(related="programregistration.programapplied.pgmschedule_programtype.programtype",store=False)
    programname = fields.Char(related="programregistration.programapplied.pgmschedule_programname",store=False)
    # ereceiptgenerated = fields.Boolean(related="programregistration.ereceiptgenerated",store=False)
    # ereceiptreference = fields.Char(related="programregistration.ereceiptreference",store=False)
    # ereceipturl = fields.Char(related="programregistration.ereceipturl",store=False)
    ereceiptgenerated = fields.Boolean(default=False)
    ereceiptreference = fields.Char(string='Receipt No', size=50)
    ereceipturl = fields.Char(string="eReceipt URL", size=500)
    ereceiptgenerated_dt = fields.Datetime(string="Receipt No Raised Date")
    orderid = fields.Char(string='Transaction ID', size=80)
    billing_name = fields.Char(string='Participant Name', size=60)
    billing_address = fields.Char(string='Address', size=150)
    billing_city = fields.Char(string='City', size=30)
    billing_state = fields.Char(string='State', size=30)
    billing_zip = fields.Char(string='Zip', size=15)
    billing_country = fields.Char(string='Country', size=50)
    billing_tel = fields.Char(string='Mobile Number', size=20)
    billing_email = fields.Char(string='Email', size=70)
    payment_status = fields.Char(string='Payment Status', size=100)
    amount = fields.Float(string = 'Amount')
    comments = fields.Char(string='Comments', size=200)
    transactiontype = fields.Char(string='Transaction Type', size=50)
    paymenttrackingid = fields.Char(string='PaymentTracking', size=80, default='0')
    transaction_dt = fields.Datetime(string="Transaction Date", compute="_compute_transaction_dt", store=True)
    ebook = fields.Char(string='E-Book', compute="_compute_ebook", store=True)
    status = fields.Char(string='Status', compute="_compute_status", store=True)
    program_purpose = fields.Char(string='Purpose_StatDate_EndDate_ProgramPlace', compute="_compute_programpurpose", store=True)
    gateway = fields.Char(string='Gateway', compute="_compute_gateway", store=True)
    billing_tel1 = fields.Char(string='Phone No1', size=20)
    billing_tel2 = fields.Char(string='Phone No2', size=20)
    billing_pan = fields.Char(string='PAN Number', size=20)
    pre_regid = fields.Char(string='Pre Reg Id', size=20)
    #nooftrees = fields.Char(string='noOfTrees', size=20)
    #recurringdonations = fields.Char(string='recurringDonation', size=20)

    @api.depends('payment_status')
    def _compute_transaction_dt(self):
        for rec in self:
            if rec.create_date != False:
                rec.transaction_dt = rec.create_date
            else:
                rec.transaction_dt = False
    
    @api.depends('ereceiptreference')
    def _compute_ebook(self):
        for rec in self:
            if rec.ereceiptreference != None and rec.ereceiptreference != '' and rec.ereceiptreference != False:
                rec.ebook = rec.ereceiptreference[5:8]
            else:
                rec.ebook = ''
    
    @api.depends('payment_status')
    def _compute_status(self):
        for rec in self:
            if rec.payment_status != None and rec.payment_status != False and rec.payment_status == 'Success':
                rec.status = 'Confirmed'
            else:
                rec.status = ''
    
    @api.depends('payment_status')
    def _compute_programpurpose(self):
        try:
            for rec in self:
                if rec.payment_status != None and rec.payment_status != False and rec.payment_status == 'Success':
                    rec.program_purpose = rec.programregistration.programapplied.pgmschedule_programname + ' - ' + rec.programregistration.programapplied.pgmlocation.centername
                else:
                    rec.program_purpose = ''
        except:
            rec.program_purpose = ''

    @api.depends('payment_status')
    def _compute_gateway(self):
        for rec in self:
            if rec.payment_status != None and rec.payment_status != False and rec.payment_status == 'Success':
                rec.gateway = 'ccavenue'
            else:
                rec.gateway = ''

    
    @api.model
    def fields_get(self, fields=None):
        fields_notto_show = ['programregistration']
        res = super(PaymentTransaction, self).fields_get()
        for field in res:
            if (field in fields_notto_show):
                res[field]['selectable'] = False
                res[field]['sortable'] = False
            else:
                res[field]['selectable'] = True	
                res[field]['sortable'] = True
                
        return res

class livingspacesRegistrationTransactions(models.Model):
    _name = "livingspaces.registration.transactions"
    _description = "Living Spaces Registration Transactions"

    programregistration = fields.Many2one('livingspaces.registration', string='program registration')
    transactiontype = fields.Char(string='Transaction Type', size=50)
    upgrade_url = fields.Char(string='Package Upgrade URL')
    upgrade_access_token = fields.Char(string='Package Upgrade Access Token')
    upgrade_packageselection = fields.Many2one('livingspaces.program.schedule.roomdata',string='Upgrade Package Selected', required=False)
    upgrade_applied = fields.Boolean(default=False)
    upgrade_applied_dt = fields.Datetime(string="Package Upgrade Applied DT")
    upgrade_email_send = fields.Boolean(default=False)
    upgrade_sms_send = fields.Boolean(default=False)
    isactive = fields.Boolean(default=True)
    upgrade_use_emrg_quota = fields.Boolean(default=False)
    comments = fields.Char(string='Comments')
    last_modified_dt = fields.Datetime(string="Last Modified DT")
    old_packageselection = fields.Many2one('livingspaces.program.schedule.roomdata',string='Old Package Selected', required=False)
    upgrade_completed = fields.Boolean(default=False)
    upgrade_completed_email_send = fields.Boolean(default=False)
    upgrade_completed_sms_send = fields.Boolean(default=False)
    seats_block_days =  fields.Integer(compute="_compute_seats_block_days")

    def sendPackageUpgradeRequestSMSEmail(self):
        plist = self.env['livingspaces.registration.transactions'].sudo().search([('isactive','=',True),('upgrade_email_send','=',False)])
        for rec in plist:
            self.env['livingspaces.notification'].sudo()._sendRegistrationPackageUpgradeEmailSMSSend(rec)

    def sendPackageUpgradeConfirmation(self):
        plist = self.env['livingspaces.registration.transactions'].sudo().search([('upgrade_completed','=',True),('upgrade_completed_email_send','=',False)])
        for rec in plist:
            self.env['livingspaces.notification'].sudo()._sendPackageUpgradeConfirmation(rec)

    def cancelPackageUpgradeRequest(self):
        print('cancelPackageUpgradeRequest')
        #self.env['livingspaces.paymentstatus'].sudo()._checkPackageUpgradePendingAndUnblockSeats()

    @api.depends('upgrade_applied')
    def _compute_seats_block_days(self):
        try:
            seats_block_days = int(self.env['ir.config_parameter'].sudo().get_param('livingspaces.seats_block_days'))
            if (seats_block_days == False or seats_block_days == None or seats_block_days == '' or seats_block_days == 0):
                seats_block_days = 15
            for rec in self:
                rec.seats_block_days=seats_block_days
        except Exception as e:
            print(e)
