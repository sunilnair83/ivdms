function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function () {
            defer(method)
        }, 50);
    }
}
defer(jspgmregfmdcoumentready);
function jspgmregfmdcoumentready() {
$(document).ready(function () {

  // Initialize select2
  $(".selUser").select2();

  // Read selected option
  $('#but_read').click(function () {
    var username = $('.selUser option:selected').text();
    var userid = $('.selUser').val();

    $('#result').html("id : " + userid + ", name : " + username);
  });

  $(".selUser").select2({
    placeholder: "None",
    allowClear: true
  });


  $('b[role="presentation"]').hide();
  $('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>');

  displayPopup(".program-history-line", "#program-history-popup", ".close-program-history-popup")
  displayPopup(".isha-life-treatments-line", "#isha-life-treatments-popup", ".isha-life-treatments-close-popup")
  displayPopup(".current-medical-details-line", "#current-medical-details-popup", ".current-medical-details-close-popup")

  displayTextInputOnCheck("#aggravating-factors-food-details-checkbox", "#aggravating-factors-food-details")
  displayTextInputOnCheck("#aggravating-factors-family-details-checkbox", "#aggravating-factors-family-details")
  displayTextInputOnCheck("#aggravating-factors-work-details-checkbox", "#aggravating-factors-work-details")
  displayTextInputOnCheck("#aggravating-factors-climate-details-checkbox", "#aggravating-factors-climate-details")

  displayPopup(".allopathy-medication-line","#allopathy-medication-popup",".allopathy-medication-close-popup")
  displayPopup(".alternate-medication-line","#alternate-medication-popup",".alternate-medication-close-popup")
  displayPopup(".allergies-to-medications-line","#allergies-to-medications-popup",".allergies-to-medications-close-popup")

  add_table_delete_behaviour('treatment-details-table-add','treatment-details-table');
  add_table_delete_behaviour('family-health-history-table-add','family-health-history-table');

  displayTextBoxforDetails("mood-yes-no", "#mood-yes","#mood-details" )
  displayTextBoxforDetails("energy-level-yes-no", "#energy-level-yes","#energy-level-details" )
  displayTextBoxforDetails("appetite-yes-no", "#appetite-yes","#appetite-details" )
  displayTextBoxforDetails("sleep-yes-no", "#sleep-yes","#sleep-details" )
  displayTextBoxforDetails("weight-yes-no", "#weight-yes","#weight-details" )
  displayTextBoxforDetails("sexual-function-yes-no", "#sexual-function-yes","#sexual-function-details" )
  displayTextBoxforDetails("bowel-yes-no", "#bowel-yes","#bowel-details" )

  displayTextInputOnCheck("#personal-safety-mobility-difficulty-details-checkbox", "#personal-safety-mobility-difficulty-details")
  displayTextInputOnCheck("#personal-safety-falling-down-details-checkbox", "#personal-safety-falling-down-details")
  displayTextInputOnCheck("#personal-safety-memory-problems-details-checkbox", "#personal-safety-memory-problems-details")
  displayTextInputOnCheck("#personal-safety-vision-loss-details-checkbox", "#personal-safety-vision-loss-details")
  displayTextInputOnCheck("#personal-safety-hearing-loss-details-checkbox", "#personal-safety-hearing-loss-details")
  displayTextInputOnCheck("#personal-safety-requirement-of-assistance-details-checkbox", "#personal-safety-requirement-of-assistance-details")


//  displayTextInputOnCheck("#display-diabetes-details-popup-checkbox","#diabetes-details-popup")
  displayTreatmentTableOnCheckbox("div.treatment-details-wrapper","#jpsprmregistrauma","#jpsprmregissurgery","#jpsprmregisbloodtransfusion","#pgmtreatmentyear", "#pgmtreatmentcondition","#pgmtreatmentdetails")

  makeTextDetailsMandt("#mood_details","#mood-yes","#mood-no")
  makeTextDetailsMandt("#energylevel_details","#energy-level-yes","#energy-level-no")
  makeTextDetailsMandt("#appetite_details","#appetite-yes","#appetite-no")
  makeTextDetailsMandt("#sleep_details","#sleep-yes","#sleep-no")
  makeTextDetailsMandt("#weight_details","#weight-yes","#weight-no")
  makeTextDetailsMandt("#sexualfunction_details","#sexual-function-yes","#sexual-function-no")
  makeTextDetailsMandt("#bowel_details","#bowel-yes","#bowel-no")

  makeOtherLifestyleDisabled('#jspgmregsedentary','#jspgmregmildexercise','#jspgmregvigorousexercise','#jspgmregregularexercise')
jsprgreghideaddbuttonfunction("#family-health-history-table","#family-health-history-table-add")
jsprgreghideaddbuttonfunction("#treatment-details-table","#treatment-details-table-add")

  maketextareamandtchildhoodillness('#jspgmregfmmmrcheck','.jspgmregfmmmrdivclass')
  maketextareamandtchildhoodillness('#jspgmregfmchickenpoxcheck','.jspgmregfmchickenpoxdivclass')
  maketextareamandtchildhoodillness('#jspgmregfmpneumoniacheck','.jspgmregfmpneumoniadivclass')
  maketextareamandtchildhoodillness('#jspgmregfminfluenzacheck','.jspgmregfminfluenzadivclass')
  maketextareamandtchildhoodillness('#jspgmregfmtetanuscheck','.jspgmregfmtetanusdivclass')
  maketextareamandtchildhoodillness('#jspgmregfmhepatitischeck','.jspgmregfmhepatitisdivclass')

  makedivmenelementsrequired('#jspgmregmenonlygender')
  //makedietingelementsrequired()
    makefemaenotapplicable()

  setCurrentConditions('#current-physical-condition-good','Good')
  setCurrentConditions('#current-physical-condition-fair','Fair')
  setCurrentConditions('#current-physical-condition-poor','Poor')
  setCurrentConditions('#current-psychological-condition-good','Good')
  setCurrentConditions('#current-psychological-condition-fair','Fair')
  setCurrentConditions('#current-psychological-condition-poor','Poor')
});
}
function makeOtherLifestyleDisabled(noexercise, mildexercise, vigorousexercise, regularexercise)
{
  $(noexercise).change(function () {
  if($(this).is(":checked"))
   { $(mildexercise).prop('disabled',true)
    $(vigorousexercise).prop('disabled',true)
    $(regularexercise).prop('disabled',true)}
  else
      {$(mildexercise).prop('disabled',false)
    $(vigorousexercise).prop('disabled',false)
    $(regularexercise).prop('disabled',false)}
  });
    $(mildexercise).change(function () {
  if($(this).is(":checked"))
    {$(noexercise).prop('disabled',true)
    $(vigorousexercise).prop('disabled',true)
    $(regularexercise).prop('disabled',true)}
  else
     {$(noexercise).prop('disabled',false)
    $(vigorousexercise).prop('disabled',false)
    $(regularexercise).prop('disabled',false)}
  });
    $(vigorousexercise).change(function () {
  if($(this).is(":checked"))
    {$(noexercise).prop('disabled',true)
    $(mildexercise).prop('disabled',true)
    $(regularexercise).prop('disabled',true)}
  else
    {$(noexercise).prop('disabled',false)
    $(mildexercise).prop('disabled',false)
    $(regularexercise).prop('disabled',false)}
  });
    $(regularexercise).change(function () {
  if($(this).is(":checked"))
    {$(noexercise).prop('disabled',true)
    $(mildexercise).prop('disabled',true)
    $(vigorousexercise).prop('disabled',true)}
  else
      {$(noexercise).prop('disabled',false)
    $(mildexercise).prop('disabled',false)
    $(vigorousexercise).prop('disabled',false)}
  });

}

function setCurrentConditions(radiobutton,value){
$(radiobutton).change(function () {
 if ($(radiobutton).is(":checked"))
    $(radiobutton).val(value)
  })
}


function makefemaenotapplicable(){
 if($('#jspgmregwomenonlygender') && $('#jspgmregwomenonlygender').val()=='Female')
     {
makedivelementsrequired('#jspgmregwomenonlygender',true)
    }
    $('#jspgmregwomennotapplicable').change(function (){
     if($(this).is(":checked")){
        $('.jspgmregfmgenderclass').css("display","none")
        makedivelementsrequired('#jspgmregwomenonlygender',false)
    }
  else{
        $('.jspgmregfmgenderclass').css("display","block")
        makedivelementsrequired('#jspgmregwomenonlygender',true)

  }
  });
}
function makedivmenelementsrequired(  element){
   if($(element) && $(element).val()=='Male')
     {
              //$('#menprostate').prop('required', true);
     }
}

function makedivelementsrequired(  element,bool){
    if($(element) && $(element).val()=='Female')
     {
         $('#menstruationage').prop('required', bool);
         $('#menstruationlastdate').prop('required', bool);
         $('#menstruationperiod').prop('required', bool);
         $('#menstuationflow').prop('required', bool);
         $('#isheavyperiod').prop('required', bool);
         $('#sexuallyactive').prop('required', bool);
         $('#tryingpregnancy').prop('required', bool);
         $('#normalpregnancies').prop('required', bool);
         $('#caeseareans').prop('required', bool);
         $('#diabetesinpregnancy').prop('required', bool);
         $('#deliverycomplications').prop('required', bool);
         $('#urinationProblem').prop('required', bool);
         $('#sweating').prop('required', bool);
         $('#menstrualtension').prop('required', bool);
         $('#breastlumps').prop('required', bool);
         $('#womendisorders').prop('required', bool);
         if(!bool)
         {
          $('#menstruationage').val("");
         $('#menstruationlastdate').val("");
         $('#menstruationperiod').val("");
         $('#menstuationflow').val("");
         $('#isheavyperiod').val('').trigger("change");
         $('#sexuallyactive').val('').trigger("change");
         $('#tryingpregnancy').val('').trigger("change");
         $('#normalpregnancies').val("");
         $('#caeseareans').val("");
         $('#diabetesinpregnancy').val('').trigger("change");
         $('#deliverycomplications').val('').trigger("change");
         $('#urinationProblem').val('').trigger("change");
         $('#sweating').val('').trigger("change");
         $('#menstrualtension').val('').trigger("change");
         $('#breastlumps').val('').trigger("change");
         $('#lastrectalwomen').val("");
         $('#womendisorders').val("");
         }
      }
}

function maketextareamandtchildhoodillness(checkbox, immunizationdate){
  $(checkbox).change(function () {
  if($(this).is(":checked"))
        {
        $(this).val("YES")
        $(immunizationdate).css("display","inline-block")
        }
      else
        {
        $(this).val("NO")
        $(immunizationdate).css("display", "none")

        }
    })
}

function makeTextDetailsMandt(textelement, radioyes, radiono){
$(radioyes).change(function () {
    if($(this).val() =='YES')
        $(textelement).prop('required',true)
})
$(radiono).change(function () {
    if($(radioyes).val() =='NO')
        $(textelement).prop('required',false)
})
}

function isothercheckboxeschecked(checkbox1,checkbox2){

return $(checkbox1).is(":checked") ||  $(checkbox2).is(":checked");
}

function displayTreatmentTableOnCheckbox(divtable,checkboxtrauma, checkboxsurgery, checkboxbloodtransfusion,element1,element2,element3){
$(checkboxtrauma).change(function () {
    if ($(this).is(":checked"))
      {$(divtable).css("display", "block");
      $(element1).prop('required',true)
      $(element2).prop('required',true)
      $(element3).prop('required',true)
      }
    else
         {
      if(isothercheckboxeschecked(checkboxsurgery,checkboxbloodtransfusion))
        return;
      $(divtable).css("display", "none");
      $(element1).prop('required',false);
      $(element2).prop('required',false);
      $(element3).prop('required',false);
      }
  })
  $(checkboxsurgery).change(function () {
    if ($(this).is(":checked"))
        {$(divtable).css("display", "block");
      $(element1).prop('required',true)
      $(element2).prop('required',true)
      $(element3).prop('required',true)
      }
    else
     {
      if(isothercheckboxeschecked(checkboxtrauma,checkboxbloodtransfusion))
        return;

      $(divtable).css("display", "none");
      $(element1).prop('required',false);
      $(element2).prop('required',false);
      $(element3).prop('required',false);
      }
  })
  $(checkboxbloodtransfusion).change(function () {
    if ($(this).is(":checked"))
       {$(divtable).css("display", "block");
       $(element1).prop('required',true)
      $(element2).prop('required',true)
      $(element3).prop('required',true)
      }
    else
        {
       if(isothercheckboxeschecked(checkboxtrauma,checkboxsurgery))
        return;

      $(divtable).css("display", "none");
      $(element1).prop('required',false);
      $(element2).prop('required',false);
      $(element3).prop('required',false);
      }
  })
}
function displayTextInputOnCheck(checkbox, Element) {
  $(checkbox).change(function () {
    if ($(this).is(":checked"))
      {
      $(Element).css("display", "block");
      $(Element).prop('required',true);
      }
    else
      {$(Element).css("display", "none");
      $(Element).prop('required',false);
      }
  })
}

function displayTextBoxforDetails(name, clickElement, toggleElement) {
  $("input[name="+name+"]").click(function() {
    if ($(clickElement).is(":checked")) {
    $(clickElement).val("YES");
      $(toggleElement).css("display", "block");
    } else {
      $(toggleElement).css("display", "none");
    $(clickElement).val("NO");
    }
  });
}

function displayPopup (clickElement, displayElement, closeButon) {
  $(clickElement).click(function () {
    $(displayElement).show();
  })
  $(closeButon).click(function () {
    $(displayElement).hide();
  })
}

function jspgmregfm4validateForm()
{
return jspgmvalidatetreatmentdetailstable() &&  jspgmvalidatecurrentandfamilyhistory() && jspgmvalidaterecentchanges();

}

function jspgmvalidatetreatmentdetailstable()
{
var rows = document.getElementById("treatment-details-table").rows;
for(i = 1; i <rows.length; i++)
{
          if(i==0) continue;

          var pgmtreatmentyear = $("td input[name='pgmtreatmentyear']",rows[i])[0].value;
          var pgmtreatmentcondition = $("td input[name='pgmtreatmentcondition']",rows[i])[0].value;
          var pgmtreatmentdetails = $("td input[name='pgmtreatmentdetails']",rows[i])[0].value;

          var atleastoneotherisfilled = pgmtreatmentcondition!="" ||  pgmtreatmentdetails!="" ;

          var atleastoneotherisnotfilled =  pgmtreatmentcondition=="" ||  pgmtreatmentdetails == "" ;

          if(pgmtreatmentyear=="" && atleastoneotherisfilled)
           {
            alert("please, fill all the details of treatment details");
            return false;
           }
          else if (pgmtreatmentyear!="" && atleastoneotherisnotfilled)
            {
             alert("please, fill all the details of treatment details");
             return false;

            }

            if(i!=1 && pgmtreatmentyear=="" && atleastoneotherisnotfilled )
                {
                alert("please, delete the empty details record");
                return false;
                }

}
    return true;
}

function jspgmvalidatecurrentandfamilyhistory()
{
        var currentphysicalgood=$('#current-physical-condition-good').val();
        var currentphysicalfair=$('#current-physical-condition-fair').val();
        var currentphysicalpoor=$('#current-physical-condition-poor').val();

        if(currentphysicalfair=='on'&&currentphysicalgood=='on'&&currentphysicalpoor=='on')
        {    alert('Please, select current physical condition');
                    $('#current-physical-condition-good').focus();
            return false;
        }

        var currentpsychologicalgood=$('#current-psychological-condition-good').val();
        var currentpsychologicalfair=$('#current-psychological-condition-fair').val();
        var currentpsychologicalpoor=$('#current-psychological-condition-poor').val();
        if(currentpsychologicalgood=='on'&&currentpsychologicalfair=='on'&&currentpsychologicalpoor=='on')
        {    alert('Please, select current psychological condition');
                               $('#current-psychological-condition-good').focus();
                     return false;
        }

    var sednetary = $('#jspgmregsedentary')[0].checked;
    var mild = $('#jspgmregmildexercise')[0].checked;
    var vigorous = $('#jspgmregvigorousexercise')[0].checked;
    var regular = $('#jspgmregregularexercise')[0].checked;
    if(!sednetary && !mild && !vigorous && !regular)
        {    alert('Please, select one lifestyle');
                               $('#jspgmregsedentary').focus();
                     return false;
        }

var rows = document.getElementById("family-health-history-table").rows;
for(i = 1; i <rows.length; i++)
{
          if(i==0) continue;

         var pgmfamilyrelation = $("td select[name='pgmfamilyrelation']",rows[i])[0].value;
              var pgmfamilygender = $("td select[name='pgmfamilygender']",rows[i])[0].value;
          var pgmfamilyage = $("td input[name='pgmfamilyage']",rows[i])[0].value;
          var pgmfamilyhealth = $("td input[name='pgmfamilyhealth']",rows[i])[0].value;

          var atleastoneotherisfilled =  pgmfamilygender!="" ||   pgmfamilyage!=""  ||  pgmfamilyhealth!=""  ;
          var atleastoneotherisnotfilled = pgmfamilygender=="" ||  pgmfamilyage == "" ||  pgmfamilyhealth == "" ;

          if(pgmfamilyrelation=="" && atleastoneotherisfilled)
           {
            alert("please, fill all the details of family history");
            return false;
           }
          else if (pgmfamilyrelation!="" && atleastoneotherisnotfilled)
            {
             alert("please, fill all the details of family history");
             return false;
            }

}
     return true;
}

function  jspgmvalidaterecentchanges(){

if(!jspgmrecentchanges('#mood-yes','#mood-no'))
    {
    alert("Select valid choice in the Recent changes in the following: Mood");
   $('#mood-yes').focus();
   return false;
    }
else if(!jspgmrecentchanges('#energy-level-yes','#energy-level-no'))
    {
    alert("Select valid choice in the Recent changes in the following: Energy Level");
   $('#energy-level-yes').focus();
   return false;
    }
else if(!jspgmrecentchanges('#appetite-yes','#appetite-no'))
    {
    alert("Select valid choice in the Recent changes in the following: Appetite");
   $('#appetite-yes').focus();
   return false;
    }
else if(!jspgmrecentchanges('#sleep-yes','#sleep-no'))
    {
    alert("Select valid choice in the Recent changes in the following: Sleep");
   $('#sleep-yes').focus();
   return false;
    }
else if(!jspgmrecentchanges('#weight-yes','#weight-no'))
    {
    alert("Select valid choice in the Recent changes in the following: Weight");
   $('#weight-yes').focus();
   return false;
    }
else if(!jspgmrecentchanges('#sexual-function-yes','#sexual-function-no'))
    {
    alert("Select valid choice in the Recent changes in the following: Sexual Function");
   $('#sexual-function-yes').focus();
   return false;
    }
else if(!jspgmrecentchanges('#bowel-yes','#bowel-no'))
    {
    alert("Select valid choice in the Recent changes in the following: Bowel");
   $('#bowel-yes').focus();
   return false;
    }
else
    return true;

}

function jspgmrecentchanges(changeyes, changeno){
if(!$(changeyes).is(":checked"))
  {  if( !$(changeno).is(":checked"))
        return false;
     else
        return true;
        }
else
    return true;
}
