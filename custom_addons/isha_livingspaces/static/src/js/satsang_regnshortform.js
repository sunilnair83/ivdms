function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function () {
            defer(method)
        }, 50);
    }
}
defer(jspgmsatsangdocumentready);

function jspgmsatsangdocumentready(){
  $(document).ready(function () {
        jspgmregsetlangpref();
});
}

function jspgmregsetlangpref() {
    try {
        var paramsvars = new URLSearchParams(location.search);
        var langvar = paramsvars.get('language').toUpperCase()
        $("select#jsgstngfmpreflang option").each(function() {
            if ($(this).text().toUpperCase() == langvar) {
                $("select#jsgstngfmpreflang").val($(this).val());
                return false;
            }

        });
    } catch (e) {}
}

function jspgmsatsangshortfmvalidateform(){

  if(validateemptystring("#jsgstngfmfirstname")){
         alert($('#jsgstngfmfirstnamewarning').val())
         return false;
         }
    if(validateemptystring("#jsgstngfmlastname")){
         alert($('#jsgstngfmlastnamewarning').val())
         return false;
         }
         return true;
}

function validateemptystring(element){
return $.trim($(element).val()) == "";
}

