// while load the page when pincode is entred
$(window).on('load', function(e) {
    $('#receipt_pincode').attr('maxlength', '6');
    $('#receipt_pincode').attr('minlength', '6');
    $('#receipt_pincode').attr('pattern', '[1-9]{1}[0-9]+');
    $('#receipt_pincode').trigger("blur");
});

$(document).ready(function() {
    var jspgremstatecoll =[];
    $(".submit_button_hide").attr("disabled", true);
    $(".submit_button_hide").css("background-color", "#808080");

    $('#receipt_state option').each(function(ivar,jvar){
        var objvar ={
            "valueattr":$(jvar).attr("value"),
            "statenameattr":$(jvar).attr("statename")
        };
        jspgremstatecoll.push(objvar);
    });
    if ($('#is_india').val() == 'True'){
        $('select[name="receipt_country"]').val('104');
        $('#receipt_country').attr("disabled", true);
    }
    $("#receipt_country").trigger("click");
    $('#receipt_country').change(function() {
        jspgmregfmselectstates();
    }).change();

    function jspgmregfmselectstates() {
        var state = document.getElementById("receipt_state");
        var countryname = document.getElementById("receipt_country");
        var countryid = countryname.value;
        $('#receipt_state').attr("required",false);
        $('.receipt_state').removeClass("required");

        var idvar = 0;
        var sd =false;
        var cc=0;
        Array.from(state.options).forEach(item => {
            var trend = item.getAttribute("countryid");
            if (trend == countryid || trend==0) {
                item.style.display = "block";
                item.disabled = false;
                sd =true;
                cc=cc+1;
            } else
            {
                item.style.display = "none";
                item.disabled = true;
            }
        });
        if (idvar != 0) {
            state.options.selectedIndex = idvar;
            state.disabled = false;
            state.height = 35;
        }
        //                 else if (idvar == 0) {
        //                   state.options.selectedIndex = 0;
        //                    state.disabled = true;
        //                }

//        if (fixState == false){
//            $('#receipt_state').val("");
//            $('#state_text_field').val('');
//        }
        if(cc>1){
            $('#receipt_state').attr("required",true);
//            $('#state_text_field').attr("required",false);
            $('.receipt_state').addClass("required");
//            $('.state_text').removeClass("required");
//            $('.state_dropdown').css({"display":"block"});
//            $('.state_txt').css({"display":"none"});
        }else{
            $('#receipt_state').attr("required",false);
//            $('#state_text_field').attr("required",true);
            $('.receipt_state').removeClass("required");
//            $('.state_text').addClass("required");
//            $('.state_txt').css({"display":"block"});
//            $('.state_dropdown').css({"display":"none"});
        }
        fixState = false;
    }


    $(".receipt_text").blur(function(){
        var cc = 0;
        $('#receipt_details .receipt_text').each(function() {
            if ($(this).val()!= null && $(this).val().length == 0) {
                cc = cc + 1;
            }
        });
        if (cc != 0 && !$('.registration_agreed').is(":checked")) {
            $(".submit_button_hide").attr("disabled", true);
            $(".submit_button_hide").css("background-color", "#808080");
        }
        else{
            $(".submit_button_hide").attr("disabled", false);
            $(".submit_button_hide").css("background-color", "#cf4520");
        }
    });
    
    $("#receipt_state").change(function(){
        $(".receipt_text").trigger('blur');
    });

    $("#receipt_addressline").change(function(){
        $(".receipt_text").trigger('blur');
    });

    $(".use_default_address").click(function() {
        india_check_bool = $('#is_india').val();
        if ($(this).is(":checked")) {
            $('#receipt_details .receipt_text').each(function() {
                var ss = "selected_" + "" + $(this).attr("name");
                console.log(ss);
                var selected_attr_value = $(this).attr(ss);

                if (india_check_bool == 'True'){
                    $('#receipt_country').attr("disabled", true);
                }

                $(this).val(selected_attr_value);
            });
             $(".submit_button_hide").attr("disabled", false);
            $(".submit_button_hide").css("background-color", "#cf4520");
            $('#user_action').val("is_skip");
            $(".non_indian_address").show();
        } else {
            $('#receipt_details input[type=text],select').each(function() {
                $(this).val("");
            });
            if (india_check_bool == 'True'){
                $('select[name="receipt_country"]').val('104');
                $('#receipt_country').attr("disabled", true);
            }
            $('select[name="payment_mode"]').val('online');
            $('select[name="account_type"]').val('sb');
            $(".submit_button_hide").attr("disabled", true);
            $(".submit_button_hide").css("background-color", "#808080");
            $('#user_action').val("");

        }
//        $(".receipt_text").trigger('blur');
//        $('#receipt_country').trigger('change');
    });

     $(".registration_agreed").click(function() {
        if($(this).is(":checked")) {
            $(".submit_button_hide").attr("disabled", false);
            $(".receipt_text").attr("disabled", true);
            $(".receipt_text").val('');
            $(".submit_button_hide").css("background-color", "#cf4520");
            $('#user_action').val("is_skip");
        } else {
            $(".submit_button_hide").attr("disabled", true);
            $(".receipt_text").attr("disabled", false);
            $(".submit_button_hide").css("background-color", "#808080");
            $('#user_action').val("");
        }
    });

    $('.payment_mode').change(function(e) {
        var payment_mode_Val = $('.payment_mode').find(":selected").text();
        $('#payment_mode_text').val(payment_mode_Val);
        $('.bank_details input').each(function(){
             $(this).attr("required",false);
        });
        if(payment_mode_Val == 'NEFT / RTGS'){
             $('.bank_details_wrapper').css({
                     "display": "block"
                });
             $('.bank_details input').each(function(){
                    $(this).attr("required",true);
              });
        } else{
            $('.bank_details_wrapper').css({
                    "display": "none"
            });
        }
     });

    $('#receipt_country').change(function(e) {
        console.log("ini" + $(this).val());
        if ($(this).val() == 104) {
            $(".non_indian_address").hide();
            $('#receipt_addressline').attr('minlength', '15');
            $("#receipt_addressline").keyup(function() {
                $('.add_err').css({
                    "display": "none"
                });
            });
            $('#receipt_pincode').attr('minlength', '6');
            $('#receipt_pincode').keypress(function(event) {
                $('.zip_err').css({
                    "display": "none"
                });
                console.log(event.which);
                if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
                    event.preventDefault();
                }
            });
        } else {
            $(".non_indian_address").show();
        }
    });
    $(".bnk_txt").keyup(function() {
                $('.bnk_err').css({
                    "display": "none"
                });
            });
    $('.proceed_receipt_address').click(function(e) {
        e.preventDefault();
        var cc = 0;
        $('#receipt_details .receipt_text').each(function() {
            if ($(this).attr('class') == 'receipt_text' && $(this).val().length == 0) {
                cc = cc + 1;
            }
        });
        if (cc != 4 && cc != 0) {
            return swal({
                backdrop: false,
                title: 'Fields Missing',
                text: "Address is partial. Please fill all the address fields. Or you can skip all the address fields",
                type: 'error'
            });
        }
        if (cc != 4) {
            var selected_receipt_country = $('#receipt_country').val();
            var zipcode = $('#receipt_pincode').val();
            if ($('#receipt_addressline').val().length <= 15 && selected_receipt_country==104) {
                $('.add_err').css({
                    "display": "block"
                });
                return false;
            }
            if (zipcode.length <6 && selected_receipt_country==104) {
                
                $('.zip_err').css({
                    "display": "block"
                });
                return false;
            }
        }

        check_bool = $('#program_terms_checkbox').is(':checked');
        var payment_mode = $('.payment_mode').val();
        if(payment_mode=="neft"){
        var x =0;

        $('.bank_details_wrapper .bnk_txt').each(function() {
            if ($(this).val().length == 0) {
               x =x+1;
            }
        });
        if (x>0){
         $('.bnk_err').css({
                    "display": "block"
             });
            $(".submit_button_hide").css("background-color", "#cf4520");
            return false;
        }
        }


        if(check_bool == false){
             $('.checkbox_err').css({
                    "display": "block"
             });
            $(".submit_button_hide").css("background-color", "#cf4520");
            return false;
        }



        return $('#receipt_details').submit();

    });

    $('.skip_receipt_address').click(function(e) {
        e.preventDefault();
        $('#user_action').val("is_skip");
        return $('#receipt_details').submit();
    });

    
    $('#receipt_pincode').focusout(async () => {
        let selected_country = $('#receipt_country option:selected').text();
        let selected_country_iso2_code = $('#receipt_country option:selected').attr("ccode");
        let pincode = $('#receipt_pincode').val();
        
        pincode = pincode.trim();
        if (pincode != "") {
            let address_details = await fetch(`https://cdi-gateway.isha.in/contactinfovalidation/api/countries/${selected_country_iso2_code}/pincodes/${pincode}`)
            let address_details_json = await address_details.json();
//            console.log("address_details_json");
//            console.log(address_details_json);
//            console.log("er" + address_details_json.state);
            
            if (!_.isEmpty(address_details_json)) {
                let statevar = _.filter(jspgremstatecoll, (a) => a.statenameattr == address_details_json.state)[0];
                $('#receipt_state').val(statevar["valueattr"]);
                $('#receipt_city').val(address_details_json.defaultcity);
                if(!$('#receipt_addressline').val()){
                    $(".submit_button_hide").attr("disabled", true);
                    $(".submit_button_hide").css("background-color", "#808080");
                }else{
                    $(".submit_button_hide").attr("disabled", false);
                    $(".submit_button_hide").css("background-color", "#cf4520");
                }
            }
        }
    });
});
