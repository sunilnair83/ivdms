
function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function () {
            defer(method)
        }, 50);
    }
}

defer(jspgmregfmdcoumentready);

function jspgmregfmdcoumentready() {
  $(document).ready(function () {
jsprgreghideprgaddbuttonfunction("#program-history-table","#jsprgregaddprghistid")
jspgmregaddprghist();
jspgmregsetpatternforphone("#countrycode1","#phone1");
jspgmregsetpatternforphone("#countrycode2","#phone2");
jsismedi();
});
}

function jsismedi(){
    debugger;
    $('#jsmediyes').change(function () {
    if ($(this).is(":checked"))
    {
        $('.program-history-wrapper-2').show();
        $('.isnotmedi').hide();
        $(this).val('YES')
    }
    });
    $('#jsmedino').change(function () {
    if ($(this).is(":checked"))
    {
        $('.program-history-wrapper-2').hide();
        $('.isnotmedi').show();
        $(this).val('NO')
    }
    });
}

function jspgmregsetpatternforphone(countrycodeelement, phoneelement) {
$(countrycodeelement).change(function () {
    var countrycode1 = $(countrycodeelement).val();
    if ( countrycode1 != '91')
        $(phoneelement).prop('pattern', '[0-9]+')
})
}

function jspgmregaddprghist() {

var clonetrvar =  $($("#program-history-table tr")[1]).clone();
    $("#jsprgregaddprghistid").click(function () {

        var appendedelevar =$(clonetrvar).clone().appendTo("#program-history-table");
        $("td a i.fa.fa-trash", appendedelevar).show();
    });

 // delete row handler
   $('#program-history-table').on("click", "tr td a i.fa.fa-trash", function () {
        $($(this).parents('#program-history-table tr')[0]).remove();
    });

//    // call select states based on country selected row handler
    $('#program-history-table').on("change", "tr td  select[name='pgmcountry']",  function () {
        console.log("country changed");
        jspgmregfmselectstates(this);
    });

      $('#program-history-table').on("change", "tr td  select[name='pgmstate1']",  function () {
          console.log("state changed");
          var self = this;
        var trref = this.parentElement.parentElement;
        var state = $("td select[name='pgmstate1']",trref)[0];
        var stateid = state.value;
        $.ajax({
            url: '/livingspaces/get_center',
            dataType: 'json',
            data: {'state_id':stateid},
            success: function (data) {
                var center_ids = data['center_ids'];
                jspgmregfmselectcenters(self,center_ids);

            }
        });

    });


}

function jspgmregfmvalidateForm() {
return true;
//var result = jspgmregprghistvalidate();
    //return result;
}
function jspgmregfmphonevalidation() {
    var p1 = document.getElementById('phone1');
    var p2 = document.getElementById('phone2');
    // Comparison
     if (p1.value == '' && p2.value == '') {
        alert('missing phone number');
        return false;
        }
     else if (p1.value != '' && p2.value != '' ) {
        if (p1.value == p2.value) {
            alert('Alternate emergency contact number should be different');
            return false;
        }
        else
        {
            var phonefromstep1 = document.getElementById("phonenumber");
            var countrycode1 = document.getElementById("countrycode1");
            var phone1 = document.getElementById("phone1");
            var countrycode2 = document.getElementById("countrycode2");
            var phone2 = document.getElementById("phone2");
            var textcode = phonefromstep1.options[1].value
            var textphone = phonefromstep1.options[0].value

            if(textcode == countrycode1.value && textphone == phone1.value)
            {
                alert("emergency phone number1 should not be same as personal mobile number");
                return false;}
            else if(textcode == countrycode2.value && textphone == phone2.value)
            {
                alert("emergency phone number2 should not be same as personal mobile number");
                return false;}
            else
                return true;


        }

    }
}

function jspgmregprghistvalidate(){

var rows = document.getElementById("program-history-table").rows;
for(i = 1; i <rows.length; i++)
{
//   for(j = 1; j < rows[i].getElementsByTagName("td").length; j++)
//     {
          if(i==0) continue;

          var programname = $("td select[name='programname']",rows[i])[0].value;

          var pgmteachername = $("td input[name='teachername']",rows[i])[0].value;
          var pgmcountry = $("td select[name='pgmcountry']",rows[i])[0].value;
          var pgmstate = $("td select[name='pgmstate1']",rows[i])[0];
          var pgmlocation = $("td select[name='pgmlocation1']",rows[i])[0];
          var pgmdate = $("td input[name='pgmdate']",rows[i])[0].value;
          var pgmstatevalue = $("td input[name='pgmstate']",rows[i])[0];
          var pgmlocationvalue = $("td input[name='pgmlocation']",rows[i])[0];

          var statecondition1 = pgmstate.selectedIndex == 0 && pgmstate.disabled ? true : pgmstate.value!="" ; // check based on state if there are no states in the country
          var locationcondition1 = pgmlocation.selectedIndex == 0 && pgmlocation.disabled ? true : pgmlocation.value!="" ; // check based on center if there are no centers in the state
          var atleastoneotherisfilled = pgmteachername!="" || pgmcountry!="" || statecondition1 ||
                      locationcondition1 || pgmdate!="";
           // Case1: if program name is not filled, but atleast one of other fields is filled
          if(programname=="" && atleastoneotherisfilled)
           {
            alert("please, fill all the details of program history");
            return false;
           }
          var statecondition2 =pgmstate.selectedIndex == 0 && pgmstate.disabled ? false : pgmstate.value == ""; // check based on state  if there are no states in the country
          var locationcondition2 =pgmlocation.selectedIndex == 0 && pgmlocation.disabled  ? false : pgmlocation.value =="" ; // check based on center if there are no centers in the state
          var atleastoneotherisnotfilled = pgmteachername=="" || pgmcountry=="" || statecondition2 ||
                      locationcondition2 || pgmdate=="";

           // Case2: if program name is filled, but atleast one of other fields is not filled
          if (programname!="" && atleastoneotherisnotfilled)
            {
             alert("please, fill all the details of program history");
             return false;
            }

           // Delete Case: if program name is not filled, but atleast one of other fields is not filled
          if(i!=1 && programname=="" && atleastoneotherisnotfilled )
            {
            alert("please, delete the empty history record");
            return false;
            }
            pgmstatevalue.value= pgmstate.value;
            pgmlocationvalue.value= pgmlocation.value;
}
    return true;
}

function jspgmregfmselectstates(jspgmtargetele) {

    var trref = jspgmtargetele.parentElement.parentElement;
    var state = $("td select[name='pgmstate1']",trref)[0];
    var pgmstatevalue = $("td input[name='pgmstate']",trref)[0];

    var countryname = $("td select[name='pgmcountry']",trref)[0];
    var countryid = countryname.value;

    var idvar = 0;
    console.log('test');
    $.ajax({
        url: '/megaprograms/get_center',
        dataType: 'json',
        data: {'country_id':countryid},
        success: function (data) {
            var state_ids = data['state_ids'];
            var center_ids = data['center_ids'];

            Array.from(state.options).forEach(item => {
            var trend = item.value;
                if (state_ids.hasOwnProperty(trend)) {
                    item.style.display = "block";
                    if (idvar == 0)
                        idvar = item.index;
                } else
                    item.style.display = "none";
            });
            if (idvar != 0) {
                state.options.selectedIndex = 0;
                state.disabled = false;
                state.required = true;
                state.height = 35;
            } else if (idvar == 0) {
                state.options.selectedIndex = 0;
                state.disabled = true;
            }
            pgmstatevalue.value=state.value;
            jspgmregfmselectcenters(jspgmtargetele,center_ids);

        }
    });

        //jspgmregfmselectcenters(jspgmtargetele);

}


function jspgmregfmselectcenters(jspgmtargetele,center_ids){

    var trref = jspgmtargetele.parentElement.parentElement;
    var state = $("td select[name='pgmstate1']",trref)[0];
    var pgmstatevalue = $("td input[name='pgmstate']",trref)[0];
    var center = $("td select[name='pgmlocation1']",trref)[0];
    var pgmlocationvalue = $("td input[name='pgmlocation']",trref)[0];

    var stateid = pgmstatevalue.value;
    var idvar = 0;
    Array.from(center.options).forEach(item => {
        var trend = item.value;
        if (center_ids.hasOwnProperty(trend)) {
            item.style.display = "block";
            if (idvar == 0)
                idvar = item.index;
        } else
            item.style.display = "none";
    });
    if (idvar != 0) {
        center.options.selectedIndex = 0;
        center.disabled = false;
        center.required = true;
        center.height = 35;
    } else if (idvar == 0) {
        center.options.selectedIndex = 0;
        center.disabled = true;
    }
    pgmlocationvalue.value = center.value;
}

function jsprgreghideprgaddbuttonfunction(tableselectorvar,buttonselectorvar){
        $(buttonselectorvar).hidden = true;
$(tableselectorvar).on('change', 'tr select,tr input', function() {
    var emptyflg = false;
console.log(tableselectorvar+ " input"+", "+tableselectorvar+ " select");
    $(tableselectorvar+ " input"+", "+tableselectorvar+ " select").each(function() {
        if ($(this)[0].name !='pgmstate1' && $(this)[0].name !='pgmstate' && $(this)[0].name !='pgmlocation' && $(this)[0].name !='pgmlocation1' &&
         $(this).val() == "") {
            emptyflg = true;
        }
        else if (jspgmregcheckifloaded($(this)[0]))
        {
            emptyflg = false;
        }

        if(!$(buttonselectorvar)[0].hidden)
            emptyflg = false;
    });
    if (emptyflg) {
        $(buttonselectorvar)[0].hidden = true;
    } else {
        $(buttonselectorvar)[0].hidden = false;
    }
 });
}

function jspgmregcheckifloaded(element){

    if(element.name == 'programname' && element.value != "")
        if(element.name == 'pgmdate' && element.value != "")
            if(element.name == 'teachername' && element.value != "")
                if(element.name == 'pgmcountry' && element.value != "")
                    if(element.name == 'pgmstate1' && !element.disabled && element.value != "")
                        if(element.name == 'pgmlocation1' && !element.disabled && element.value != "")
                            return true;

    return false;

}
