from odoo import models, fields, api, exceptions
import base64

class KshetragnaDocumentSubmit(models.TransientModel):
    _name = 'kshetragna.document.submit'
    _description = 'Kshetragna Document Submit'

    passport_checkbox = fields.Boolean()
    oca_card_checkbox = fields.Boolean()
    pancard_checkbox = fields.Boolean()
    aadhar_checkbox = fields.Boolean()
    pan_aadhar_checkbox = fields.Boolean()
    comments = fields.Text('User Comments')
    register_id = fields.Char()
    residence_country_code = fields.Char()

    # Docuemnt Submit button Logic
    def document_submit(self):
        # """ For all registration,allow to make registration and get the passport copy,number
        #  and restrict the payment flow for non india.
        #  For Non indian ppl, once registation is done ,
        #  then that records has sent to approval from isha backend..
        #  once it approval is done from backend, email has been to sent to them to make payment"""
        kshetragna_reg_obj = self.env['kshetragna.registration']
        register_rec = kshetragna_reg_obj.search([('id','=',self.register_id)])
        passport = 'True' if self.passport_checkbox else 'False'
        pan = 'True' if self.pancard_checkbox else 'False'
        aadhar = 'True' if self.aadhar_checkbox else 'False'
        oca_card = 'True' if self.oca_card_checkbox else 'False'
        pan_aadhar = 'True' if self.pan_aadhar_checkbox else 'False'
        # email_id = register_rec.create_uid.email
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        application_data = "registration_id=" + str(register_rec.id) + '&passport=' + str(passport) + '&pan=' + str(pan) + '&oca_card=' + str(oca_card) + '&aadhar=' + str(aadhar) + '&pan_aadhar=' + str(pan_aadhar)
        encode_application_data = base64.b64encode(application_data.encode("UTF-8"))
        s1 = encode_application_data.decode("UTF-8")
        redirect_url = base_url + "/kshetragna/registrationform/documentpage?reg_values=" + s1
        # redirect_url = base_url + "/kshetragna/registrationform/documentpage?registration_id=" + str(register_rec.id)+'&passport='+str(passport)+'&pan='+str(pan)+'&oca_card='+str(oca_card)

        register_rec.sudo().write({"registration_status":"document_pending",
                                   "upload_doc_comments":self.comments,
                                   "document_pending_link": redirect_url,
                                   "document_pending_link_sent_date":fields.Date.today()})
        mail_template_id = self.env.ref('isha_livingspaces.ishaliving_space_mail_document_pending_template')

        try:
            self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(register_rec.id, force_send=True)
        except Exception as e:
            print(e)
        return True

class KshetragnaPaymentMode(models.TransientModel):
    _name = 'kshetragna.payment.mode'
    _description = 'Kshetragna Payment Modue'

    payment_mode = fields.Selection([('neft', 'NEFT'), ('dd', 'DD/ Cheque')],string='Payment Mode', size=100, default='neft')
    register_id = fields.Char()

    # Payment Mode button Logic
    def payment_mode_confirm(self):
        kshetragna_reg_obj = self.env['kshetragna.registration']
        register_rec = kshetragna_reg_obj.search([('id', '=', self.register_id)])
        # passport = 'True' if self.passport_checkbox else 'False'
        # pan = 'True' if self.pancard_checkbox else 'False'
        # oca_card = 'True' if self.oca_card_checkbox else 'False'
        # email_id = register_rec.create_uid.email
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        # application_data = "registration_id=" + str(register_rec.id) + '&passport=' + str(passport) + '&pan=' + str(
        #     pan) + '&oca_card=' + str(oca_card)
        # encode_application_data = base64.b64encode(application_data.encode("UTF-8"))
        # s1 = encode_application_data.decode("UTF-8")
        # redirect_url = base_url + "/kshetragna/registrationform/documentpage?reg_values=" + s1
        # redirect_url = base_url + "/kshetragna/registrationform/documentpage?registration_id=" + str(register_rec.id)+'&passport='+str(passport)+'&pan='+str(pan)+'&oca_card='+str(oca_card)
        if self.payment_mode == 'neft':
            register_rec.sudo().write({"registration_status": "payment_link_sent_neft","payment_mode":self.payment_mode})
            mail_template_id = self.env.ref('isha_livingspaces.ishaliving_NEFT_email_send_Template')
            try:
                self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(register_rec.id, force_send=True)
            except Exception as e:
                print(e)
            return True

        else:
            register_rec.sudo().write({"registration_status": "payment_link_sent_dd", "payment_mode": self.payment_mode})
            mail_template_id = self.env.ref('isha_livingspaces.ishaliving_dd_email_send_Template')
            try:
                self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(register_rec.id, force_send=True)
            except Exception as e:
                print(e)
            return True

