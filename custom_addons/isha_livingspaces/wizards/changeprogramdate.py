from odoo import models, fields, api, exceptions
from odoo.exceptions import except_orm
from datetime import date, timedelta, datetime
import json

class ChangeProgramDate(models.TransientModel):
	_name = 'livingspaces.registration.changeprogramdate'
	_description = 'Change Program Date'

	#participant_id = fields.Integer(string = 'ID')
	participant_name = fields.Char(string = 'Participant Name', required=True)
	programapplied = fields.Many2one('livingspaces.program.schedule',string = 'Applied Program', required=True)
	prg_date_now = fields.Date()
	selected_program = fields.Many2one('livingspaces.program.schedule',string = 'Available Programs', required=True ,
	domain="[('id','!=',programapplied),('pgmschedule_registrationopen','=',True),('pgmschedule_totalbalanceseats','>',0),('pgmschedule_startdate','>',context_today().strftime('%Y-%m-%d'))]")
	selected_package = fields.Many2one('livingspaces.program.schedule.roomdata', string='Requested Package',
	domain="[('pgmschedule_programname','=',selected_program)]", required=True)

	@api.constrains('selected_program')
	def	validate_selected_program(self):
		if (self.selected_program == self.programapplied):
			raise exceptions.ValidationError("Applied program and selected program should not be the same")
	
	@api.onchange('programapplied')
	def participant_name_onchange(self):
		print('onchange')
		print(self.programapplied.pgmschedule_programtype.id)
		id = self.programapplied.pgmschedule_programtype.id
		print(id)
		recs = self.env['livingspaces.program.schedule'].search([('pgmschedule_programtype', '=', id)])
		print(recs)			
		# self.selected_program = recs

	@api.onchange('selected_program')
	def selected_program_onchange(self):
		print('prog change')
	
	def submit_changeprogramdate(self):
		print('field generate')
		#fields = self.env['livingspaces.registration'].fields_get()

		active_id = self.env.context.get('default_active_id')
		ids = self.env["livingspaces.registration"].browse(active_id)
		for id in ids:

			if (id.ischangeofparticipant == True):
				if (id.registration_status == 'Paid' or id.registration_status == 'Confirmed'):
					raise exceptions.ValidationError('Schedule change not allowed, this registration have done through change of participant.')

			if (id.payment_status == 'Initiated'):
				raise exceptions.ValidationError('Payment has been initiated by the participant. Please try after some time')
			elif (id.registration_status != 'Paid' and id.registration_status != 'Confirmed'):
				print('change initiated')
				id.write({'programapplied':self.selected_program, 'packageselection':self.selected_package})
			else:

				#

				balanceseatcount = self.selected_package.pgmschedule_packagenoofseats - self.selected_package.pgmschedule_regularseatspaid
				balanceemergencyseatcount = self.selected_package.pgmschedule_packageemergencyseats - self.selected_package.pgmschedule_emergencyseatspaid
			
				if (balanceseatcount <= 0 and balanceemergencyseatcount <= 0):
					raise exceptions.ValidationError('Schedule change not allowed, no seats are available in the selected package.')
				
				#

				print('initiate cancellation')

				self.env['livingspaces.payments']._reverseSeat(id)

				refund_data = self.env['livingspaces.payments']._calculateRefundAmount(id, datetime.now())
				
				id.write({
					'cancelapproved_email_send':False,
					'cancelapproved_sms_send':False,
					'date_cancelapproved':datetime.now(),
					'registration_status':'Cancel Approved',
					'refund_eligible_ondaysbefore': refund_data['days_before'],
					'refund_eligible_onpercentage': refund_data['elg_percent'],
					'refund_eligible': refund_data['elg_amt'],
					'last_modified_dt': datetime.now()				
				})

				#email and sms trigger
				self.env['livingspaces.notification']._sendCancellationMailAndSMS(id.id,'Cancel Approved')

				print('do the new registration')

				nrec = self.env["livingspaces.registration"].create({'first_name':id.first_name,
																'last_name':id.last_name,
																'name_called':id.name_called,
																'gender':id.gender,
																'marital_status':id.marital_status,
																'dob':id.dob,
																'countrycode':id.countrycode,
																'phone':id.phone,
																'pincode':id.pincode,
																'address_line':id.address_line,
																'city':id.city,
																'education_qualification':id.education_qualification,
																'occupation':id.occupation,
																'name1':id.name1,
																'relationship1':id.relationship1,
																'phone1':id.phone1,
																'name2':id.name2,
																'relationship2':id.relationship2,
																'phone2':id.phone2,
																'file_name':id.file_name,
																'idfile_name':id.idfile_name,
																'addrfile_name':id.addrfile_name,
																'programhistorynote':id.programhistorynote,
																'passportnumber':id.passportnumber,
																'passportexpirydate':id.passportexpirydate,
																'visanumber':id.visanumber,
																'visaexpirydate':id.visaexpirydate,
																'show_change_pgm':id.show_change_pgm,
																'ha_status':'Pending',
																'registration_status':'Pending',
																'programapplied':self.selected_program.id,
																'packageselection':self.selected_package.id,
																'participant_comments':id.participant_comments,
																'participant_email':id.participant_email,
																'prerequisite_pgm':id.prerequisite_pgm,
																'display_name':id.display_name
																})
								
				if nrec:

					nrec.write({'country':id.country,
								'state':id.state,
								'photo_file':id.photo_file,
								'idproof_file':id.idproof_file,
								'addrproof_file':id.addrproof_file,
								'bloodreports_file':id.bloodreports_file,
								'echoreports_file':id.echoreports_file,
								'nationality_id':id.nationality_id,
								'idproof_type':id.idproof_type
								})

					print('Program History Update')
					ntmp = self.env['living.spaces.history'].search([('programregistration','=',id.id)])
					if ntmp:
						print(ntmp)
						for n in ntmp:
							x = self.env['living.spaces.history'].create({'programregistration':nrec.id,'programname':n.programname.id,'pgmdate':n.pgmdate,'teachername':n.teachername,'pgmcountry':n.pgmcountry.id,'pgmstate':n.pgmstate.id,'pgmlocation':n.pgmlocation.id})

					# #idproof.attachment not updated due to not clear about the attributes
					#
					# print('ishalifetreatments - ishalife.treatments')
					# ntmp = self.env['ishalife.treatments'].search([('programregistration','=',id.id)])
					# if ntmp:
					# 	print(ntmp)
					# 	for n in ntmp:
					# 		x = self.env['ishalife.treatments'].create({'programregistration':nrec.id,
					# 												'treatmentdate':n.treatmentdate,
					# 												'treatmentplace':n.treatmentplace,
					# 												'condition':n.condition,
					# 												'treatmentname':n.treatmentname
					# 												})
					#
					# print('current.medicalhistory')
					# ntmp = self.env['current.medicalhistory'].search([('programregistration','=',id.id)])
					# if ntmp:
					# 	print(ntmp)
					# 	for n in ntmp:
					# 		x = self.env['current.medicalhistory'].create({'programregistration':nrec.id,
					# 												'current_nameofcomplaint':n.current_nameofcomplaint,
					# 												'current_duration':n.current_duration,
					# 												'current_condition':n.current_condition
					# 												})
					#
					# print('current.allopathymedications')
					# ntmp = self.env['current.allopathymedications'].search([('programregistration','=',id.id)])
					# if ntmp:
					# 	print(ntmp)
					# 	for n in ntmp:
					# 		x = self.env['current.allopathymedications'].create({'programregistration':nrec.id,
					# 												'current_allopathyname':n.current_allopathyname,
					# 												'current_allopathydose':n.current_allopathydose,
					# 												'current_allopathyduration':n.current_allopathyduration
					# 												})
					#
					# print('current.alternatemedications')
					# ntmp = self.env['current.alternatemedications'].search([('programregistration','=',id.id)])
					# if ntmp:
					# 	print(ntmp)
					# 	for n in ntmp:
					# 		x = self.env['current.alternatemedications'].create({'programregistration':nrec.id,
					# 												'current_alternatename':n.current_alternatename,
					# 												'current_alternatedose':n.current_alternatedose,
					# 												'current_alternateduration':n.current_alternateduration
					# 												})
					#
					# print('medication.allergies')
					# ntmp = self.env['medication.allergies'].search([('programregistration','=',id.id)])
					# if ntmp:
					# 	print(ntmp)
					# 	for n in ntmp:
					# 		x = self.env['medication.allergies'].create({'programregistration':nrec.id,
					# 												'allergydrugname':n.allergydrugname,
					# 												'allergyreaction':n.allergyreaction
					# 												})
					#
					# print('treatment.details')
					# ntmp = self.env['treatment.details'].search([('programregistration','=',id.id)])
					# if ntmp:
					# 	print(ntmp)
					# 	for n in ntmp:
					# 		x = self.env['treatment.details'].create({'programregistration':nrec.id,
					# 												'treatment_year':n.treatment_year,
					# 												'treatment_condition':n.treatment_condition,
					# 												'treatment_details':n.treatment_details
					# 												})
					#
					# print('family.history')
					# ntmp = self.env['family.history'].search([('programregistration','=',id.id)])
					# if ntmp:
					# 	print(ntmp)
					# 	for n in ntmp:
					# 		x = self.env['family.history'].create({'programregistration':nrec.id,
					# 												'family_relation':n.family_relation,
					# 												'family_gender':n.family_gender,
					# 												'family_age':n.family_age,
					# 												'family_health':n.family_health
					# 												})
					#
					# print('refractive.error')
					# ntmp = self.env['refractive.error'].search([('programregistration','=',id.id)])
					# if ntmp:
					# 	print(ntmp)
					# 	for n in ntmp:
					# 		x = self.env['refractive.error'].create({'programregistration':nrec.id,
					# 												'select_eye':n.select_eye,
					# 												'dvsph':n.dvsph,
					# 												'dvcyl':n.dvcyl,
					# 												'dvaxis':n.dvaxis,
					# 												'dvva':n.dvva,
					# 												'nvsph':n.nvsph,
					# 												'nvcyl':n.nvcyl,
					# 												'nvaxis':n.nvaxis,
					# 												'nvva':n.nvva
					# 												})
					#
					# print('duration.sensation')
					# ntmp = self.env['duration.sensation'].search([('programregistration','=',id.id)])
					# if ntmp:
					# 	print(ntmp)
					# 	for n in ntmp:
					# 		x = self.env['duration.sensation'].create({'programregistration':nrec.id,
					# 												'doespainincrease':n.doespainincrease,
					# 												'durationofincreasedsensations':n.durationofincreasedsensations
					# 												})
					#
					# print('joints.affected')
					# ntmp = self.env['joints.affected'].search([('programregistration','=',id.id)])
					# if ntmp:
					# 	print(ntmp)
					# 	for n in ntmp:
					# 		x = self.env['joints.affected'].create({'programregistration':nrec.id,
					# 												'joints':n.joints,
					# 												'paindegree':n.paindegree,
					# 												'right':n.right,
					# 												'left':n.left
					# 												})
