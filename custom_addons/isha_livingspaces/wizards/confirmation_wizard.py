from odoo import models, fields, api, exceptions, _
import logging
_logger = logging.getLogger(__name__)
import traceback

class KshetragnaConfirmationWizard(models.TransientModel):
    _name = "kshetragna.confirm.wizard"

    message = fields.Text()

    def yes(self):

        if self.message=="The record will be marked as pre-screened. Do you wish to continue?":
            active_ids = self._context.get('active_ids')
            reg_records = self.env['kshetragna.registration'].sudo().search([("id", "in", active_ids), ("registration_status", "=", "Enquiry")])
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
            redirect_url = base_url + "/kshetragna/registrationform"
            mail_template_id = self.env.ref('isha_livingspaces.ishaliving_Prescreened_AdvancePaymentForm_Template')
            values = {
                'first_name': reg_records.first_name if reg_records.first_name else '',
                'last_name': reg_records.last_name if reg_records.last_name else '',
                'countrycode': reg_records.countrycode if reg_records.countrycode else '',
                'mobile': reg_records.mobile if reg_records.mobile else '',
                'email': reg_records.email if reg_records.email else '',
            }
            prescreen_obj = self.env['kshetragna.prescreenedregistrants']
            prescreen_rec = prescreen_obj.sudo().search([('email','=',reg_records.email),("mobile","=",reg_records.mobile),('payment_status','=','Pending')])
            if prescreen_rec:
                raise exceptions.ValidationError("Record Already added in Prescreened")
            else:
                prescreen_rec = prescreen_obj.create(values)

            reg_records.sudo().write({
                "registration_status": "prescreened",
                "register_link": redirect_url,
                "register_link_sent_date": fields.Date.today(),
                "is_enquiry_email":True,
            })
            try:
                mail_send_rec = reg_records.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(reg_records.id, force_send=True)
            except Exception as ex:
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                _logger.error("ERROR: " + tb_ex)

            return True

        if self.message == "You are approving the payment method as 'NEFT'. Do you wish to continue?":
            active_ids = self._context.get('active_ids')
            kshetragna_reg_rec = self.env['kshetragna.registration'].search([('id', 'in', active_ids)])
            if kshetragna_reg_rec and kshetragna_reg_rec.payment_mode == 'neft':
                kshetragna_reg_rec.sudo().write({
                    "registration_status": "neft_requested",
                    "payment_mode": kshetragna_reg_rec.payment_mode
                })
                mail_template_id = self.env.ref('isha_livingspaces.ishaliving_NEFT_email_send_Template')
                try:
                    self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(kshetragna_reg_rec.id,
                                                                                           force_send=True)
                except Exception as ex:
                    tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                    _logger.error("ERROR: " + tb_ex)
                    return True

        if self.message == "You are approving the payment method as 'DD'. Do you wish to continue?":
            active_ids = self._context.get('active_ids')
            kshetragna_reg_rec = self.env['kshetragna.registration'].search([('id', 'in', active_ids)])
            kshetragna_reg_rec.sudo().write({
                "registration_status": "dd_requested",
                "payment_mode": kshetragna_reg_rec.payment_mode
            })
            mail_template_id = self.env.ref('isha_livingspaces.ishaliving_dd_email_send_Template')
            try:
                self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(kshetragna_reg_rec.id, force_send=True)
            except Exception as ex:
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                _logger.error("ERROR: " + tb_ex)
                return True

        if self.message == 'You are approving the documents. Do you wish to continue?':
            active_ids = self._context.get('active_ids')
            kshetragna_reg_rec = self.env['kshetragna.registration'].search([('id', 'in', active_ids)])
            if kshetragna_reg_rec and kshetragna_reg_rec.payment_mode == 'neft' and kshetragna_reg_rec.registration_status in ['waiting_for_approve', 'document_corrected']:
                kshetragna_reg_rec.sudo().write({
                    "registration_status": "neft_requested",
                    "payment_status": "Pending",
                    "payment_mode": kshetragna_reg_rec.payment_mode
                })
                mail_template_id = self.env.ref('isha_livingspaces.ishaliving_NEFT_email_send_Template')
                try:
                    self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(kshetragna_reg_rec.id,
                                                                                           force_send=True)
                except Exception as ex:
                    tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                    _logger.error("ERROR: " + tb_ex)
                    return True

            if kshetragna_reg_rec and kshetragna_reg_rec.payment_mode == 'dd' and kshetragna_reg_rec.registration_status in ['waiting_for_approve', 'document_corrected']:
                kshetragna_reg_rec.write({"payment_status": "Pending", "registration_status": "dd_requested"})
                mail_template_id = self.env.ref('isha_livingspaces.ishaliving_dd_email_send_Template')
                try:
                    self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(kshetragna_reg_rec.id, force_send=True)
                except Exception as ex:
                    tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                    _logger.error("ERROR: " + tb_ex)
                    return True

            if kshetragna_reg_rec and kshetragna_reg_rec.payment_mode == 'online' and kshetragna_reg_rec.registration_status in ['waiting_for_approve', 'document_corrected']:
                registration_status = "Payment Link Sent"
                # payment_status = "Pending"
                kshetragna_reg_rec.write({"registration_status": registration_status})
                base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
                redirect_url = base_url + "/kshetragna/registrationform/paymentpage?registration_id=" + str(kshetragna_reg_rec.id)
                mail_template_id = self.env.ref('isha_livingspaces.ishaliving_space_mail_payment_template')
                kshetragna_reg_rec.sudo().write({"payment_link": redirect_url, "payment_link_sent_date": fields.Date.today()})
                try:
                    self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(kshetragna_reg_rec.id, force_send=True)
                except Exception as ex:
                    tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                    _logger.error("ERROR: " + tb_ex)
                return True

        # if self.message == 'You are requesting the following documents to be reuploaded. You are approving the documents. Do you wish to continue?':
        #     pass

    def no(self):
        """ cancel the popup """
        return {'type': 'ir.actions.act_window_close'}

