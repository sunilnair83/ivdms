from odoo import models, fields


class keyvalueconfig(models.Model):
    _name = 'satsang.key.value.config'
    _description = 'key value config for satsang program'
    _rec_name = 'key'

    # region_name = fields.Many2one('isha.region',required=True, string='Region Name')
    regionname = fields.Many2many('isha.region',relation="satsang_relation_region", required=True, string='Region Name')
    key = fields.Selection([('email','Email'),('phone','Phone'),('tnc','Terms & Conditions and Cancelation Policy'),('res_add_text','Residential Address Text'),('acct_terms_text','Acceptance Terms Text'),('thankyou_addtional_text','ThankYou Page Additional Text'),('confirmation_addtional_text','Confirmation Page Additional Text')], string='Key', required=True)
    value = fields.Text(string="Value", required=True)


class SatsangLangmailTemplate(models.Model):
    _name = 'satsang.lang.mailtempate'
    _description = '  confirm mail for FMF program'
    _rec_name = 'mail_template_id'

    # region_name = fields.Many2one('isha.region',required=True, string='Region Name')
    tz_lang_id = fields.Many2one('res.lang',
                                      string='Languages ', ondelete='cascade')
    mail_template_id = fields.Many2one("mail.template")
    key = fields.Char(string="Key")
