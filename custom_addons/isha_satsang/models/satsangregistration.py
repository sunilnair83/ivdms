import logging
import uuid

from odoo import models, fields
from odoo.exceptions import except_orm

_logger = logging.getLogger(__name__)
try:
    from html2text import HTML2Text
except ImportError as error:
    _logger.debug(error)

def html2text(html):
    """ covert html to text, ignoring images and tables
    """
    if not html:
        return ""

    ht = HTML2Text()
    ht.ignore_images = True
    ht.ignore_tables = True
    ht.ignore_emphasis = True
    ht.ignore_links = True
    return ht.handle(html)

class ValidationError(except_orm):
    """Violation of python constraints.

                 .. admonition:: Example

                     When you try to create a new user with a login which already exist in the db.
                 """

    def __init__(self, msg):
        super(ValidationError, self).__init__(msg)

class SatsangRegistration(models.Model):
    _name = 'satsang.registration'
    _description = 'Satsang Registration Forms'
    _rec_name = 'first_name'

    def _get_default_access_token(self):
        return str(uuid.uuid4())

    #basic profile info
    first_name = fields.Char(string="First Name", required=True)
    last_name = fields.Char(string="Last Name", required=True)
    countrycode = fields.Char(string='Country Tel code', required=False, index=False)
    phone = fields.Char(string='Mobile Number', required=False, index=True)
    wtsappyesno = fields.Selection([('YES', 'YES'), ('NO', 'NO')], string='Use Whatsapp number as mobile number?')
    wtsappcountrycode = fields.Char(string='WhatsApp Country Tel code', required=False, index=False)
    wtsappphone = fields.Char(string='WhatsApp Mobile Number', required=False, index=True)
    participantemail = fields.Char(string='Email', required=False)
    country = fields.Many2one('res.country', 'Country of residence')
    nationality_id = fields.Many2one('res.country', 'Nationality Id')
    pincode = fields.Char(string="Pincode/Zipcode", required=False, index=True)
    city = fields.Char(string='City/Town/District', required=False)
    state = fields.Many2one('res.country.state', 'State/province')
    anyishayesno = fields.Boolean(String="Any other isha program?")
    preflang = fields.Char(string='Preferred Language', required=False)
    preflang1 = fields.Char(string='Selected Preferred Language', required=False)
