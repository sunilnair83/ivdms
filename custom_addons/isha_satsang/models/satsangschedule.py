import datetime

import pytz

from odoo import models, fields, api
from odoo.exceptions import UserError

_tzs = [(tz, tz) for tz in sorted(pytz.all_timezones, key=lambda tz: tz if not tz.startswith('Etc/') else tz)]
def _tz_get(self):
    return _tzs



class pournami_tz_country(models.Model):
    _name = 'satsang.tzregion'

    sch_langregion= fields.Many2one('satsang.schedule',string='Region')
    tzlang_tzregion = fields.One2many('satsang.tzlang', 'tzlangregion',
                                          string='Languages in Timezone', ondelete='cascade')
    tzlang_region = fields.Many2one('isha.region', String='Regions')
    # tzlang_support_country=fields.Many2many('res.country', string="Countries in Timezone",store=True)


class Pournami_tz_lang(models.Model):
    _name = 'satsang.tzlang'
    _description = 'Satsang Timezone Lang Mapping'

    tzlangregion = fields.Many2one('satsang.tzregion', string='Satsang Schedule')
    tzlang_timezone = fields.Selection(_tz_get, string='Timezone', default=lambda self: self._context.get('tz'))
    tzlang_displaytext=fields.Char(string="Display Text", translate=True)
    tzlang_support_language = fields.Many2many('res.lang', string='Transaled into', store=True)
    time_ist = fields.Char('IST Time')
    time_local = fields.Char('Local time ')
    date = fields.Date()
    is_active = fields.Boolean(string='Active', default=True)

class SatsangSchedule(models.Model):
    _name = 'satsang.schedule'
    _description = 'Satsang Schedule'
    _order = 'pgmschedule_programname'
    # _rec_name = 'pgmschedule_programname'

    def name_get(self):
        result = []
        for rec in self:
            name = '-'.join(filter(None, [rec.pgmschedule_programname, rec.pgmschedule_time]))
            result.append((rec.id, name))
        return result

    def _getdomain(self):
        return [('category', '=', 'Online Offering')]
    pgmschedule_programtype = fields.Many2one('program.type.master', string='Program Type', required=True, domain= _getdomain)
    pgmschedule_programname = fields.Char(string='Program Name', size=200, required=True)
    pgmschedule_startdate = fields.Date(string='Start Date', required=True)
    pgmschedulejoomla_onlinecode = fields.Char(string='joomla ')
    pgmschedule_enddate = fields.Date(string='End Date', required=True)
    pgmschedule_registrationopen = fields.Boolean('Registration Open', default=True)
    pgmschedule_publishinwebsite = fields.Boolean('Publish In Website', default=True)
    pgmschedule_language = fields.Selection(
        [('English', 'English'), ('Tamil', 'Tamil'), ('Telugu', 'Telugu'), ('Hindi', 'Hindi'), ('Kannada', 'Kannada')],
        string='Program Language')
    pgmschedule_time=fields.Char(string="Display Date and Time")
    pgmschedule_start_time = fields.Float(string='Start Time')
    pgmschedule_end_time = fields.Float(string='End Time')
    pgmschedule_tz_region= fields.One2many('satsang.tzregion', 'sch_langregion', string='Region timezone Mappings', ondelete='cascade')
    schedulenotificationtemplates = fields.One2many('satsang.notificationtemplate', 'programtype',
                                                    string='Notification Templates', ondelete='cascade')

    @api.depends('pgmschedule_timezone')
    def _compute_tz_offset(self):
        for partner in self:
            partner.tz_offset = datetime.datetime.now(pytz.timezone(self.pgmschedule_timezone or 'GMT')).strftime('%z')

    # // add publish in website and regitrations open bool
    # @api.constrains('pgmschedule_enddate')
    # def validate_pgmschedule_enddate(self):
    # 	diff = self.pgmschedule_enddate - self.pgmschedule_startdate
    # 	if (self.pgmschedule_startdate > self.pgmschedule_enddate):
    # 		raise exceptions.ValidationError("End date should be greater then Start date")
    # 	elif (self.pgmschedule_programtype.numberofdays > (diff.days + 1)):
    # 		raise exceptions.ValidationError(
    # 			"Difference between dates should be greater than or equal to program type number of days")


    @api.onchange('pgmschedule_programtype')
    def setvisiblelang(self):
        if self.pgmschedule_programtype.program_type == 'Online Pournami Satsang':
            self.pgmschedule_language='English'
            self.displayvalue = True
        else:
            self.displayvalue = False

    displayvalue = fields.Boolean(computed=setvisiblelang)

    @api.onchange('pgmschedule_startdate', 'pgmschedule_enddate', 'pgmschedule_language')
    def set_program_name(self):
        if (self.pgmschedule_startdate != False and self.pgmschedule_language != '' and self.pgmschedule_language):
            self.pgmschedule_programname = self.pgmschedule_programtype.program_type + " - " + self.pgmschedule_language + " - " + self.pgmschedule_startdate.strftime(
                "%d %b, %Y")
        else:
            self.pgmschedule_programname = ""

class AnandaAlaiUpdate(models.TransientModel):
    _inherit = 'ananda.alai.update'

    def _getDomain(self):
        pgm_typ_master = self.env['program.type.master'].search([('program_type', '=', 'Online Ananda Alai')])
        return [('pgmschedule_programtype', '=', pgm_typ_master.id)]
    bay_name = fields.Many2one('satsang.schedule', string="Batch", domain=_getDomain)

    def update_ananda_alai_batch(self):
        program_lead_recs = self.env[self._context.get('active_model')].sudo().browse(self._context.get('active_ids'))
        satsang_id = self.bay_name.id
        satsang_record = self.env['satsang.schedule'].sudo().browse(satsang_id)
        for rec in program_lead_recs:
            if rec.program_name != 'Online Ananda Alai':
                raise UserError('Plz select records only from Ananda Alai view.')
            rec.write({'bay_name': satsang_record.pgmschedule_time})
            transprogramlead_rec = self.env['transprogramlead'].sudo().create({
                'record_name': rec.record_name,
                'record_phone': rec.record_phone,
                'record_email': rec.record_email,
                'program_name': 'Online Ananda Alai',
                'tzlangregion': rec.bay_name
            })
            self.env['satsang.notification'].sudo().sendRegistrationEmailSMSCore(transprogramlead_rec)