from odoo import models, fields, _
from datetime import datetime


class ProgramLead(models.Model):
    _inherit = "program.lead"

    def _send_fmf_conf_mail(self):
        mail_template_id = self.env.ref(
            "isha_satsang.satsang_porumani_sucess_mail_template_new"
        )

        gcalurl = "https://isha.co/fmf-gcal-"
        ical = "https://isha.co/fmf-ical-"

        fmfemailnextsatsangdate = (
            self.env["ir.config_parameter"]
            .sudo()
            .get_param("satsang.fmfemailnextsatsangdate")
        )
        d = datetime.strptime(fmfemailnextsatsangdate, "%Y-%m-%d")

        context = {}

        # updated the context with current context so that no context data is lost
        context.update(self.env.context)

        for rec in self:
            schd = (
                self.env["satsang.tzlang"]
                .sudo()
                .search([("tzlang_timezone", "=", rec.bay_name)], limit=1)
            )

            lang = (
                self.env["res.lang"].sudo().search([("name", "ilike", rec.trans_lang)])
            )

            key_value_rec = (
                schd.tzlangregion.tzlang_region
                and self.env["satsang.key.value.config"]
                .sudo()
                .search(
                    [
                        ("key", "=", "confirmation_addtional_text"),
                        ("regionname.id", "=", schd.tzlangregion.tzlang_region.id),
                    ],
                    limit=1,
                )
                or False
            )

            context.update(
                dict(
                    dt_val_tz=False,
                    datetime_text=str(d.strftime("%d-%b-%Y")),
                    time_zone=schd.tzlang_displaytext,
                    gl_rd_url=f"{gcalurl}{schd.tzlang_displaytext}",
                    ic_rd_url=f"{ical}{schd.tzlang_displaytext}",
                    lang=lang.code
                )
            )

            if key_value_rec: context.update({"contact_text":key_value_rec.value})

            self.env["mail.template"].with_context(context).sudo().browse(
                mail_template_id.id
            ).send_mail(rec.id, force_send=True)

    def _send_confirmation_email(self):
        from itertools import groupby

        sort_key = lambda x: x.local_program_type_id

        for key, recs in groupby(
            sorted(
                self.filtered(lambda x: x.start_date > datetime.now().date()),
                key=sort_key,
            ),
            key=sort_key,
        ):
            recs = self.env["program.lead"].concat(*recs)

            if key == "Online Pournami Satsang":
                recs._send_fmf_conf_mail()
