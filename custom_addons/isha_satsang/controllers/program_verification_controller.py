import logging, traceback, json, hmac, hashlib, datetime

import werkzeug

from odoo import http
from odoo.http import request
from .main import hash_hmac, sso_get_user_profile, get_fullprofileresponse_DTO, sso_get_full_data
import requests
_logger = logging.getLogger(__name__)


program_map = {
    'ie': {
        'verification_for': 'Inner Engineering / Shambavi Mahamudra',
        'pgm_master_entry': 'isha_satsang.ie_pgm_verify',
        'txn_pgm_name': 'IE mandala'
    }
}


def update_sso_phone(sso_id, phone, phone_cc):
    request_url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_PMS_ENDPOINT1')
    authreq = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SYSTEM_TOKEN1')
    url = request_url + 'basic-profiles/phone'
    headers = {
        'authorization': 'Bearer ' + authreq,
        'Content-Type': 'application/json',
        'X-User': sso_id
    }

    post_data = {
        "phone": {
            "countryCode": phone_cc,
            "number": phone
        },
        "profileId": sso_id
    }
    profile = requests.post(url, json.dumps(post_data), headers=headers)
    if profile.status_code == 200:
        _logger.info('SSO update successful for ' + str(sso_id) + ' '+str(phone)+' '+str(phone_cc))
    else:
        _logger.error('SSO update failed '+profile.text + ' for ' + str(sso_id) + ' '+str(phone)+' '+str(phone_cc))


def global_api_call(fName,lName,email,phone,sso_id):

    null = None
    false = False
    true = True
    url = request.env['ir.config_parameter'].sudo().get_param('isha_crm.global_meditator_api')
    token = request.env['ir.config_parameter'].sudo().get_param('isha_crm.global_meditator_api_token')
    url = werkzeug.urls.url_join(
        url, '?%(params)s' % {
            'params': werkzeug.urls.url_encode({
                'firstName':fName,
                'lastName':lName,
                'phone':phone,
                'email':email,
                'ssoId':sso_id,
                'bypassCache':1
            }),
        }
    )
    payload = {}
    headers = {
        'Authorization': 'Bearer '+token
    }
    response = requests.request("GET", url, headers=headers, data=payload)
    res_dict = eval(response.text)
    if res_dict.get('payload',None):
        return res_dict.get('payload',None).get('isMeditator',False)
    else:
        return False


class ProgramVerifiaction(http.Controller):

    @http.route('/verifyprogram/registrationform', type='http', auth="public", methods=["GET"], website=True,
                csrf=False)
    def verify_program_get_form(self,language='',pgm_verify='',**kwargs):
        try:
            ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
            if "localhost" in request.httprequest.url:
                cururls = request.httprequest.url
            else:
                cururls = str(request.httprequest.url).replace("http://", "https://", 1)

            sso_log = {'request_url': str(cururls),
                       "callback_url": str(cururls),
                       "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                       "hash_value": "",
                       "action": "0",
                       "legal_entity": "IF",
                       "force_consent": "1"
                       }
            ret_list = {'request_url': str(cururls),
                        "callback_url": str(cururls),
                        "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                        }
            data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
            data_enc = data_enc.replace("/", "\\/")
            signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                                 digestmod=hashlib.sha256).hexdigest()
            sso_log["hash_value"] = signature
            sso_log["sso_logurl"] = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_URL1')
            values = {
                'sso_log': sso_log
            }
            return request.render('isha_satsang.onlinesatsangsso', values)
        except Exception as ex:
            tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
            _logger.error(tb_ex)
            return request.render('isha_satsang.verify_pgm_exception', {'language':language})

    @http.route('/verifyprogram/registrationform', type='http', auth="public", methods=["POST"], website=True,
                csrf=False)
    def verify_program_post_form(self,language='',consent_grant_status='',pgm_verify='',**kwargs):
        # SSO callback
        if "session_id" in request.httprequest.form:
            try:
                result_countries = request.env['res.country'].sudo()
                countries_scl = request.env['res.country'].sudo().search([])
                for x in ['IN', 'US', 'CA', 'GB']:
                    result_countries |= countries_scl.filtered(lambda c:c.code == x)
                result_countries |= countries_scl.filtered(lambda x:x.code not in ['IN', 'US', 'CA', 'GB'])

                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                api_key = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1')  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                session_id = request.httprequest.form['session_id']
                hash_val = hash_hmac({'session_id': session_id}, ret)
                data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
                request_url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
                sso_user_profile = eval(sso_get_user_profile(request_url, data).text)

                fullprofileresponsedatVar = get_fullprofileresponse_DTO()
                if "autologin_email" in sso_user_profile:
                    fullprofileresponsedatVar["basicProfile"]["email"] = sso_user_profile["autologin_email"]
                    fullprofileresponsedatVar["profileId"] = sso_user_profile["autologin_profile_id"]
                if consent_grant_status == "1":
                    fullprofileresponsedatVar = sso_get_full_data(sso_user_profile['autologin_profile_id']);

                caption_list = request.env['satsang.uicaptionlist'].sudo().search([('name', '=', language.title())])
                if not caption_list:
                    caption_list = request.env['satsang.uicaptionlist'].sudo().search([('name', '=', 'English')])

                pgm_verify_mapped = program_map.get('ie') if not pgm_verify else program_map.get(pgm_verify)

                year_picker = []
                for x in reversed(range(1950,datetime.datetime.now().today().year+1)):
                    year_picker.append((x,x))

                values = {
                    'countries': result_countries,
                    'labelvalues': caption_list,
                    'isofname': fullprofileresponsedatVar,
                    'pgm_verify':pgm_verify,
                    'pgm_year':year_picker,
                    'program_verification':pgm_verify_mapped['verification_for']
                }

                # Already registered for the same program verification
                if request.env['program.lead'].sudo().search([('sso_id','=',sso_user_profile["autologin_profile_id"]),
                                                       ('program_name','=',pgm_verify_mapped['txn_pgm_name'])],count=True) > 0:
                    return request.render('isha_satsang.verify_pgm_success', {'status':'already_reg'})

                return request.render('isha_satsang.verify_pgm_reg_from', values)

            except Exception as ex:
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                _logger.error(tb_ex)
                return request.render('isha_satsang.verify_pgm_exception', {'language': language})
        else:
            try:
                pgm_verify_mapped = program_map.get('ie') if not pgm_verify else program_map.get(pgm_verify)
                # Already registered for the same program verification
                if request.env['program.lead'].sudo().search([('sso_id','=',kwargs['sso_id']),
                                                       ('program_name','=',pgm_verify_mapped['txn_pgm_name'])],count=True) > 0:
                    return request.render('isha_satsang.verify_pgm_success', {'status':'already_reg'})

                contact_dict = {
                    'sso_id':kwargs['sso_id'],
                    'name': ' '.join([kwargs.get('first_name'), kwargs.get('last_name')]),
                    'phone_country_code':kwargs.get('phone_cc'),
                    'phone':kwargs.get('phone'),
                    'phone2_country_code':kwargs.get('phone2_cc') if kwargs.get('phone2') else None,
                    'phone2':kwargs.get('phone2'),
                    'email':kwargs.get('email'),
                    'email2':kwargs.get('email2'),
                    'zip':kwargs.get('pincode')
                }
                if kwargs.get('country_of_residence').isnumeric():
                    contact_dict.update({'country_id':int(kwargs.get('country_of_residence'))})
                else:
                    contact_dict.update({'country': kwargs.get('country_of_residence')})

                res_partner = request.env['res.partner'].sudo().create(contact_dict)
                pgm_lead_dict = {
                    'sso_id':kwargs['sso_id'],
                    'record_name': ' '.join([kwargs.get('first_name'),kwargs.get('last_name')]),
                    'record_phone': kwargs.get('phone'),
                    'record_email': kwargs.get('email'),
                    'contact_id_fkey': res_partner.id,
                    'reg_status': 'PENDING',
                    'reg_date_time':datetime.datetime.now(),
                    'program_name': pgm_verify_mapped['txn_pgm_name'],
                    'center_name': kwargs.get('program_country'),
                    'pgm_remark': kwargs.get('program_year'),
                    'pgm_type_master_id':request.env.ref(pgm_verify_mapped['pgm_master_entry']).id
                }

                # Commit is needed as we call the global api for auto verification of the contact
                request.env.cr.commit()
                med_check = False
                if res_partner.is_meditator:
                    pgm_lead_dict['reg_status'] = 'Auto Verified'
                else:
                    # try global api and see if they are already meditator
                    try:
                        med_check = global_api_call(kwargs.get('first_name'),kwargs.get('last_name'),kwargs.get('email'),kwargs.get('phone'),kwargs['sso_id'])
                        if med_check:
                            pgm_lead_dict['reg_status'] = 'Auto Verified Global API'

                    except Exception as ex:
                        _logger.error('Global api call failed for '+kwargs['sso_id'])
                        pass

                lead = request.env['program.lead'].sudo().create(pgm_lead_dict)

                if med_check is True or res_partner.is_meditator:
                    request.env['isha.online.sathsang.api.wizard'].sudo().mark_meditator(lead)
                    request.env['isha.online.sathsang.api.wizard'].sudo().send_email_notif(lead)

                    # If the mapped res_partner is not a meditator then create the program_attendance entry
                    if not res_partner.is_meditator:
                        pgm_att_dict = {
                            'system_id': 0,
                            'local_trans_id': lead.id,
                            'active': True,
                            'reg_status': 'COMPLETED',
                            'local_program_type_id': request.env.ref(
                                'isha_crm.ie_manually_added_master').local_program_type_id,
                            'program_name': 'IE verified Global API',
                            'pgm_type_master_id': request.env.ref('isha_crm.ie_manually_added_master').id,
                            'contact_id_fkey': res_partner.id,
                            'start_date': '1970-01-01',
                            'reg_date_time': '1970-01-01 00:00:00'
                        }
                        request.env['program.attendance'].sudo().create(pgm_att_dict)

                # SSO update for phone
                if kwargs.get('sso_phone_number') != kwargs.get('phone') or kwargs.get('sso_phone_cc') != kwargs.get('phone_cc'):
                    update_sso_phone(kwargs['sso_id'],kwargs.get('phone'),kwargs.get('phone_cc'))
                return request.render('isha_satsang.verify_pgm_success', {'status':'success'})
            except Exception as ex:
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                _logger.error(tb_ex)
                return request.render('isha_satsang.verify_pgm_exception', {'language': language})