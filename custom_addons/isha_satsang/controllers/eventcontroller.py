import hashlib
import hmac
import json
import logging
import traceback
# from datetime import date, timedelta, timezone
import requests
import werkzeug
import werkzeug.utils
import odoo.http
from odoo import http
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.http import request
from odoo.tools import ustr
# import flask
import datetime
from datetime import timedelta
# import pickle
import base64
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow, Flow
from google.auth.transport.requests import Request
from google.auth.credentials import Credentials
import google.oauth2.credentials
import google_auth_oauthlib
from odoo.addons.web.controllers.main import serialize_exception,content_disposition

_logger = logging.getLogger(__name__)

class InsertCalendarEvents(http.Controller):
    SCOPES = ['https://www.googleapis.com/auth/calendar']
    # realroot = "https://registrations.sushumna.isha.in"
    # realroot = "https://localhost:8069"
    odoo.http.HttpRequest.session.__setattr__('credentials', {'token': 'None',
                'refresh_token': 'None',
                'token_uri': "None",
                'client_id': "None",
                'client_secret': "None",
                'scopes':SCOPES})

    def get_client_config(self):
        # _logger.info("get client config...................." + ptvalue)
        baseroot = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
        return { "web": {"client_id": "934587100339-7dsqeauk1chu2m34r30ousej5fm3e4mt.apps.googleusercontent.com",
                         "project_id": "quickstart-1614772637771",
                         "auth_uri": "https://accounts.google.com/o/oauth2/auth",
                         "token_uri": "https://oauth2.googleapis.com/token",
                         "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
                         "client_secret": "GkB2cftuS7QKZ4NiidbqvGhQ",
                         "redirect_uris": [baseroot.replace('http', 'https') + "/onlinesatsang/calendarevent", baseroot.replace('http', 'https') + "/onlinesatsang/oauth2callback"],
                         },
                 }

    def initiate_credentials(self):
        return {'token': 'None',
                'refresh_token': 'None',
                'token_uri': "None",
                'client_id': 'None',
                'client_secret': "None",
                'scopes': self.SCOPES}

    def credentials_to_dict(self, credentials):
        return {'token': credentials.token,
                'refresh_token': credentials.refresh_token,
                'token_uri': "https://oauth2.googleapis.com/token",
                'client_id': "934587100339-7dsqeauk1chu2m34r30ousej5fm3e4mt.apps.googleusercontent.com",
                'client_secret': "GkB2cftuS7QKZ4NiidbqvGhQ",
                'scopes': self.SCOPES}

    def createevent_ical(self, pstartdate, ptimezone, penddate, precurrence, pattendees, peventtitle, plocation, pdescription):
        cal_string='''BEGIN:VEVENT
DTSTART;TZID=Asia/Kolkata:{}
DTEND;TZID=Asia/Kolkata:{}
RRULE:FREQ=DAILY;COUNT=1
DTSTAMP:{}
UID:ishassevent
CREATED:20210422T035437Z
DESCRIPTION:Online Pournami Satsang
LAST-MODIFIED:20210422T035437Z
LOCATION:
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:Isha Pournami Event
TRANSP:OPAQUE
BEGIN:VALARM
ACTION:DISPLAY
DESCRIPTION:This is an event reminder
TRIGGER:-P0DT0H10M0S
END:VALARM
BEGIN:VALARM
ACTION:EMAIL
DESCRIPTION:This is an event reminder
SUMMARY:Alarm notification
ATTENDEE:mailto:
TRIGGER:-P1D
END:VALARM
END:VEVENT'''.format(pstartdate.replace('-',''),penddate.replace('-',''),pstartdate.replace('-',''))
        return cal_string

    def createevent(self, pstartdate, ptimezone, penddate, precurrence, pattendees, peventtitle, plocation, pdescription):
        event = {
            'summary': peventtitle,
            # 'location': plocation,
            'description': pdescription,
            'start': {
                'dateTime': pstartdate,
                'timeZone': ptimezone,
            },
            'end': {
                'dateTime': penddate,
                'timeZone': ptimezone,
            },
            'recurrence': precurrence,
            'attendees': pattendees,
            'reminders': {
                'useDefault': False,
                'overrides': [
                    {'method': 'email', 'minutes': 24 * 60},
                    {'method': 'popup', 'minutes': 10},
                ],
            },
        }
        return (event)

    def get_event_date_time(self):
        dt_col = ["28/01/21 0:0:0", "27/02/21 0:0:0", "28/03/21 0:0:0", "26/04/21 0:0:0", "26/05/21 0:0:0", "24/06/21 0:0:0", "23/07/21 0:0:0",
                 "22/08/21 0:0:0", "20/09/21 0:0:0", "20/10/21 0:0:0", "18/11/21 0:0:0", "18/12/21 0:0:0"]
        return(dt_col)

    def calculate_end_date(self,p_st_date,region_time):
        manupulated_dates={}
        mp_st_date=datetime.datetime.strptime(p_st_date, '%d/%m/%y %H:%M:%S')
        mp_en_date=datetime.datetime.strptime(p_st_date, '%d/%m/%y %H:%M:%S')
        if 'CET' in region_time or 'CEST' in region_time or 'BST' in region_time or 'GMT' in region_time :
            mp_st_date = datetime.datetime.strptime(p_st_date, '%d/%m/%y %H:%M:%S')
            mp_en_date =mp_st_date + timedelta(days=1)
        elif 'ET' in region_time or 'PT' in region_time:
            mp_st_date = datetime.datetime.strptime(p_st_date, '%d/%m/%y %H:%M:%S') +timedelta(days=1)
            mp_en_date=mp_st_date
        elif 'IST' in region_time:
            mp_st_date=datetime.datetime.strptime(p_st_date, '%d/%m/%y %H:%M:%S')
            mp_en_date=mp_st_date

        manupulated_dates={
            'start_date':mp_st_date,
            'end_date':mp_en_date,
        }
        return(manupulated_dates)

    def get_time_details(self,region_time,cal_type):
        time_dict={}
        if 'IST' in region_time:
            time_dict={
                'st_tm_col' : 'T19:00:00' if (cal_type=="G") else 'T190000',
                'en_tm_col' : 'T20:30:00' if (cal_type=="G") else 'T203000',
                'reg_name' : 'Asia/Calcutta',
            }
        elif 'CET' in region_time or 'CEST' in region_time or 'BST' in region_time or 'GMT' in region_time :
            time_dict={
                'st_tm_col' : 'T23:30:00' if (cal_type=="G") else 'T233000',
                'en_tm_col' : 'T01:00:00' if (cal_type=="G") else 'T010000',
                'reg_name' : 'Asia/Calcutta',
            }
        elif 'ET' in region_time:
            time_dict = {
                'st_tm_col': 'T04:30:00' if (cal_type=="G") else 'T043000',
                'en_tm_col': 'T06:00:00' if (cal_type=="G") else 'T060000',
                'reg_name': 'Asia/Calcutta',
            }
        elif 'PT' in region_time:
            time_dict = {
                'st_tm_col': 'T07:30:00' if (cal_type=="G") else 'T073000',
                'en_tm_col': 'T09:00:00' if (cal_type=="G") else 'T090000',
                'reg_name': 'Asia/Calcutta',
            }
        return(time_dict)


    @http.route('/onlinesatsang/downloadicsfile', type='http', auth="public", methods=['GET', 'POST'], website=True, csrf=False)
    def createicsfile(self,**post):
        recursrule = [
            'RRULE:FREQ=DAILY;COUNT=1'
        ]
        attendees = []

        timezonevalue = post.get('tvalue')
        initialdate=self.get_event_date_time()
        tmp_time_details=self.get_time_details(timezonevalue,"A")
        print(datetime.date.today())

        some_data='''BEGIN:VCALENDAR
PRODID:-//Isha Foundataion//isha.in v1.7//EN
VERSION:2.0
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-WR-CALNAME:
X-WR-TIMEZONE:Asia/Kolkata
BEGIN:VTIMEZONE
TZID:Asia/Kolkata
X-LIC-LOCATION:Asia/Kolkata
BEGIN:STANDARD
TZOFFSETFROM:+0530
TZOFFSETTO:+0530
TZNAME:IST
DTSTART:19700101T000000
END:STANDARD
END:VTIMEZONE'''
# END:VTIMEZONE'''.format(datetime.date.today().strftime('%Y%m%d'))

        # for i in range(12):
        tmpdate=self.calculate_end_date(initialdate[3],timezonevalue)
        tmpevent = self.createevent_ical(tmpdate['start_date'].strftime('%Y-%m-%d') + tmp_time_details['st_tm_col'] , tmp_time_details['reg_name'], tmpdate['end_date'].strftime('%Y-%m-%d') + tmp_time_details['en_tm_col'], recursrule, attendees, "Isha Pournami Event", "@ Online", "Online Pournami Satsang")
        some_data=some_data+"\n"+tmpevent

        some_data=some_data+"\nEND:VCALENDAR"

        rec = request.env['satsang.calevent'].sudo().create({
            'event_content': some_data,
        })
        record = request.env['satsang.calevent'].sudo().browse(int(rec.id))
        binary_file = record.event_content
        filecontent=binary_file or ''
        content_type, disposition_content = False, False
        filename=''
        if not filecontent:
            return request.not_found()
        else:
            if filename=='':
                filename = '%s_%s.ics' % ('ssevent', rec.id)
            content_type = ('Content-Type', 'application/octet-stream')
            disposition_content = ('Content-Disposition', content_disposition(filename))
        return request.make_response(filecontent, [content_type, disposition_content])

    # @http.route('/onlinesatsang/createcalendarevent',type='http',auth="public",methods=['GET','POST'],website=True,csrf=False)
    # def createcreatecalendar(self, **post):
    #     if request.httprequest.method == "GET":
    #         values = {
    #             'submitted': post.get('submitted', False),
    #             'email': "None",
    #             'temp_tz_value':post.get('timezoneparam'),
    #         }
    #         odoo.http.HttpRequest.session.__setattr__('credentials', self.initiate_credentials())
    #         return request.render('isha_satsang.pournamiregistrationsuccess', values)
    #         # return request.render('isha_satsang.pournamicalendarevent', values)

    @http.route('/onlinesatsang/calendarevent', type='http', auth="public", methods=['GET','POST'], website=True, csrf=False)
    def createcalendar(self, **post):
        odoo.http.HttpRequest.session.__setattr__('timezonevalue_demo', post.get('tvalue'))
        # request.session['timezonevalue']=post.get('tvalue')
        timezonevalue=post.get('tvalue')
        # _logger.info("session timezonevalue................."+timezonevalue)
        # _logger.info("session timezonevalue...................." + odoo.http.HttpRequest.session.__getattribute__('timezonevalue'))
        credentials = google.oauth2.credentials.Credentials(**odoo.http.HttpRequest.session.__getattribute__('credentials'))
        if (credentials.client_id != "934587100339-7dsqeauk1chu2m34r30ousej5fm3e4mt.apps.googleusercontent.com" and credentials.client_secret!="GkB2cftuS7QKZ4NiidbqvGhQ"):
            return werkzeug.utils.redirect('/onlinesatang/authorize?tvalue='+timezonevalue)

        service = build('calendar', 'v3', credentials=credentials)
        recursrule = [
            'RRULE:FREQ=DAILY;COUNT=1'
        ]
        attendees = []
        # initialdate =self.get_event_date_time()
        # timezonevalue =request.session.pop('timezonevalue',None)
        # odoo.http.HttpRequest.session.__getattribute__('timezonevalue')
        # _logger.info("create calendar timezonevalue...................."+timezonevalue)
        initialdate = self.get_event_date_time()
        tmp_time_details = self.get_time_details(timezonevalue,"G")

        for i in range(12):
            tmpdate = self.calculate_end_date(initialdate[i], timezonevalue)
            tmpevent = self.createevent(tmpdate['start_date'].strftime('%Y-%m-%d') + tmp_time_details['st_tm_col'] , tmp_time_details['reg_name'], tmpdate['end_date'].strftime('%Y-%m-%d') + tmp_time_details['en_tm_col'], recursrule, attendees, "Isha Pournami Event", "@ Online", "Online Pournami Satsang")
            event = service.events().insert(calendarId='primary', body=tmpevent).execute()
        odoo.http.HttpRequest.session.__setattr__('credentials',self.initiate_credentials())
        return request.render('isha_satsang.eventaddedsuccessfully')

    def encode_dict(self, source_dict):
        json_encode = json.dumps(source_dict)
        encoded_bytes = base64.urlsafe_b64encode(json_encode.encode("utf-8"))
        return str(encoded_bytes, "utf-8")

    def decode_dict(self,encoded_string):
        decoded_bytes = base64.urlsafe_b64decode(encoded_string)
        decoded_string = str(decoded_bytes, "utf-8")
        return eval(decoded_string)

    @http.route('/onlinesatang/authorize',type='http',auth="public",methods=['GET','POST'],website=True,csrf=False)
    def authorize(self,**post):
        if request.httprequest.method == "GET":
            _logger.info("authorize timezonevalue...................." + post.get('tvalue'))
            flow = google_auth_oauthlib.flow.Flow.from_client_config(self.get_client_config(), scopes=self.SCOPES)
            baseroot=request.env['ir.config_parameter'].sudo().get_param('web.base.url')
            state=self.encode_dict({'tvalue':post.get('tvalue')})
                # base64.urlsafe_b64encode(json.dumps({'tvalue':post.get('tvalue')}).encode())
            flow.redirect_uri = baseroot.replace('http','https') + "/onlinesatsang/oauth2callback"
            _logger.info("state values encoded........"+state)
            authorization_url, state = flow.authorization_url(
                state=self.encode_dict({'tvalue': post.get('tvalue')}),
                access_type='offline',
                include_granted_scopes='true')
            # requests.session.state = state
            _logger.info("auth url   " + authorization_url)
            _logger.info("state values after authorized url........" + state)
            _logger.info("authorize timezonevalue 2...................." + post.get('tvalue'))
            return werkzeug.utils.redirect(authorization_url)

    @http.route('/onlinesatsang/oauth2callback', type='http',auth="public",methods=['GET','POST'],website=True,csrf=False)
    def oauth2callback(self,**post):
        state = post.get('state')
        _logger.info("state values ib call back........" + state)
        param_values= self.decode_dict(state)

        _logger.info("oauth calback timezonevalue..starting.................." + param_values['tvalue'])
        flow = google_auth_oauthlib.flow.Flow.from_client_config(self.get_client_config(), scopes=self.SCOPES)
        baseroot = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
        flow.redirect_uri = baseroot.replace('http','https') + "/onlinesatsang/oauth2callback"
        authorization_response = request.httprequest.url.replace('http://','https://')
        flow.fetch_token(authorization_response=authorization_response)
        credentials = flow.credentials
        odoo.http.HttpRequest.session.__setattr__('credentials', self.credentials_to_dict(credentials))
        _logger.info("oauth calback timezonevalue..before.................." + param_values['tvalue'])
        tvalue=param_values['tvalue']
        _logger.info("oauth calback timezonevalue..after.................." + param_values['tvalue'])
        return werkzeug.utils.redirect(baseroot.replace('http','https')+"/onlinesatsang/calendarevent?tvalue="+tvalue)

