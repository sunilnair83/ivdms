import hashlib
import hmac
import json
import logging
import traceback
# from datetime import date, timedelta, timezone
import requests
import werkzeug
import werkzeug.utils
from odoo import http
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.http import request
from odoo.tools import ustr
# import flask
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow, Flow
from google.auth.transport.requests import Request
from google.auth.credentials import Credentials
import google.oauth2.credentials

# import google_auth_oauthlib.flow

_logger = logging.getLogger(__name__)


def hash_hmac(data, secret):
    data_enc = json.dumps(data['session_id'], sort_keys=True, separators=(',', ':'))
    signature = hmac.new(bytes(secret, 'utf-8'), bytes(data_enc, 'utf-8'), digestmod=hashlib.sha256).hexdigest()
    return signature


def get_fullprofileresponse_DTO():
    return {
        "profileId": "",
        "basicProfile": {
            "createdBy": "",
            "createdDate": "",
            "lastModifiedBy": "",
            "lastModifiedDate": "",
            "id": 0,
            "profileId": "",
            "email": "",
            "firstName": "",
            "lastName": "",
            "profilePic": "",
            "phone": {
                "countryCode": "",
                "number": ""
            },
            "gender": "",
            "dob": "",
            "countryOfResidence": ""
        },
        "extendedProfile": {
            "createdBy": "",
            "createdDate": "",
            "lastModifiedBy": "",
            "lastModifiedDate": "",
            "id": 0,
            "passportNum": "",
            "nationality": "",
            "pan": "",
            "profileId": ""
        },
        "documents": [],
        "attendedPrograms": [],
        "profileSettingsConfig": {
            "createdBy": "",
            "createdDate": "",
            "lastModifiedBy": "",
            "lastModifiedDate": "",
            "id": 0,
            "nameLocked": False,
            "isPhoneVerified": False,
            "phoneVerificationDate": None,
            "profileId": ""
        },
        "addresses": [
            {
                "createdBy": "",
                "createdDate": "",
                "lastModifiedBy": "",
                "lastModifiedDate": "",
                "id": 0,
                "addressType": "",
                "addressLine1": "",
                "addressLine2": "",
                "townVillageDistrict": "",
                "city": "",
                "state": "",
                "country": "",
                "pincode": "",
                "profileId": ""
            }
        ]
    }


# Api to get the profile data
def sso_get_user_profile(url, payload):
    dp = {"data": json.dumps(payload)}
    url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
    # request.env['satsang.tempdebuglog'].sudo().create({
    #     'logtext': "sso_get_user_profile config " + url
    # })
    return requests.post(url, data=dp)


# Api to get full profile data
def sso_get_full_data(sso_id):
    # request_url = "https://uat-profile.isha.in/services/pms/api/full-profiles/" + sso_id
    request_url = request.env['ir.config_parameter'].sudo().get_param(
        'satsang.SSO_PMS_ENDPOINT1') + "full-profiles/" + sso_id
    authreq = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SYSTEM_TOKEN1')
    headers = {
        "Authorization": "Bearer " + authreq,
        'X-legal-entity': 'IF',
        'x-user': sso_id
    }
    profile = requests.get(request_url, headers=headers)
    return profile.json()


def sso_get_entitlements(sso_id):
    # gentitlement_url = "https://uat-profile.isha.in/services/pms/api/entitlements/" + sso_id
    gentitlement_url = request.env['ir.config_parameter'].sudo().get_param(
        'satsang.SSO_PMS_ENDPOINT1') + "entitlements/" + sso_id
    authreq = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SYSTEM_TOKEN1')
    headers = {
        "x-user": sso_id,
        "X-legal-entity": "IF",
        "Authorization": "Bearer " + authreq
    }
    req = requests.get(gentitlement_url, headers=headers, verify=True)
    # req.raise_for_status()
    return req.json()


def sso_has_entitlement(sso_id, entitlementid):
    retflag = False
    try:
        # testentitlement_url = "https://uat-profile.isha.in/services/pms/api/entitlements/" + sso_id + "/" + entitlementid
        testentitlement_url = request.env['ir.config_parameter'].sudo().get_param(
            'satsang.SSO_PMS_ENDPOINT1') + "entitlements/" + sso_id + "/" + entitlementid
        authreq = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SYSTEM_TOKEN1')
        headers = {
            "x-user": sso_id,
            "X-legal-entity": "IF",
            "Authorization": "Bearer " + authreq
        }
        req = requests.get(testentitlement_url, headers=headers, verify=False)
        if req.text != '':
            responseobj = req.json();
            if 'id' in responseobj:
                if responseobj["id"] == entitlementid:
                    retflag = True
    except Exception as ex2:
        tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
        _logger.error(tb_ex)
        # request.env['satsang.tempdebuglog'].sudo().create({
        #     'logtext': "has_entitlement"
        # })
        # request.env['satsang.tempdebuglog'].sudo().create({
        #     'logtext': tb_ex
        # })
    finally:
        return retflag

def global_api_call(fName,lName,email,phone,sso_id):

    null = None
    false = False
    true = True
    url = request.env['ir.config_parameter'].sudo().get_param('isha_crm.global_meditator_api')
    token = request.env['ir.config_parameter'].sudo().get_param('isha_crm.global_meditator_api_token')
    url = werkzeug.urls.url_join(
        url, '?%(params)s' % {
            'params': werkzeug.urls.url_encode({
                'firstName':fName,
                'lastName':lName,
                'phone':phone,
                'email':email,
                'ssoId':sso_id,
                'bypassCache':1
            }),
        }
    )
    payload = {}
    headers = {
        'Authorization': 'Bearer '+token
    }
    response = requests.request("GET", url, headers=headers, data=payload)
    res_dict = eval(response.text)
    if res_dict.get('payload',None):
        return res_dict.get('payload',None).get('isMeditator',False)
    else:
        return False

def sso_set_entitlement(entitlementid, profidvar):
    try:
        startDate = datetime.datetime.now(datetime.timezone.utc)
        # endDate = date(startDate.year + 4, startDate.month, startDate.day)
        endDate = startDate.replace(startDate.year + 1)
        parents_dict = {
            "entitlements": [
                {
                    "id": entitlementid,
                    "validity": {
                        "endTime": int(endDate.timestamp() * 1000),
                        "startTime": int(startDate.timestamp() * 1000)
                    }
                }
            ]
        }
        datavar2 = json.dumps(parents_dict)
        authreq = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SYSTEM_TOKEN1')
        headers2 = {
            "content-type": "application/json",
            "x-user": profidvar,
            "X-legal-entity": "IF",
            "Authorization": "Bearer " + authreq
        }
        # requestconsent_url = "https://uat-profile.isha.in/services/pms/api/entitlements/" + profidvar
        requestconsent_url = request.env['ir.config_parameter'].sudo().get_param(
            'satsang.SSO_PMS_ENDPOINT1') + "entitlements/" + profidvar
        reqvar = requests.post(requestconsent_url, datavar2, headers=headers2)
        reqvar.raise_for_status()
        # request.env['satsang.tempdebuglog'].sudo().create({
        #     'logtext': "set_entitlement" + str(reqvar.text)
        # })
        # request.env['satsang.tempdebuglog'].sudo().create({
        #     'logtext': str(reqvar.request.headers) + str( reqvar.request.path_url)
        # })
    except Exception as ex2:
        tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
        _logger.error(tb_ex)
        # request.env['satsang.tempdebuglog'].sudo().create({
        #     'logtext': "set_entitlement" + str(reqvar.text)
        # })
        # request.env['satsang.tempdebuglog'].sudo().create({
        #     'logtext': tb_ex
        # })
        # request.env['satsang.tempdebuglog'].sudo().create({
        #     'logtext': str(reqvar.request.headers) + str( reqvar.request.path_url)
        # })


def get_is_meditator_or_ieo(name=None, phone=None, email=None, check_meditator=False, check_ieo=False,
                            check_shoonya=False, check_samyama=False, sso_id=None):
    try:
        if check_meditator:
            check_meditator = eval(check_meditator)
        if check_ieo:
            check_ieo = eval(check_ieo)
        if check_shoonya:
            check_shoonya = eval(check_shoonya)
        if check_samyama:
            check_samyama = eval(check_samyama)
    except:
        pass

    ishangam_id = None
    is_meditator = False
    shoonya_completed = False
    samyama_completed = False
    ieo_completed = False
    rec_id = None
    matches = []
    if sso_id:
        sso_matches = request.env['contact.map'].sudo().search([('sso_id', '=', sso_id)]).mapped('contact_id_fkey')
        for rec in sso_matches:
            matches.append(rec)

    if not sso_id or not matches:
        high_match = request.env['res.partner'].sudo().getHighMatch(name, phone, email, False, True)
        for rec in high_match:
            matches.append(rec)

    if check_shoonya:
        for rec in matches:
            if rec.shoonya_date or rec.samyama_date:
                shoonya_completed = True
                rec_id = rec.id
                break
        _logger.info('Get Contact Info ' + str({'name': name, 'phone': phone, 'email': email, 'sso_id': sso_id})
                     + ' check Shoonya ' + str(matches)+' output ' +
                     str({"ishangam_id": rec_id, "shoonya_completed": shoonya_completed}))
        return {"ishangam_id": rec_id, "shoonya_completed": shoonya_completed}

    if check_samyama:
        for rec in matches:
            if rec.samyama_date:
                samyama_completed = True
                rec_id = rec.id
                break
        _logger.info('Get Contact Info ' + str({'name': name, 'phone': phone, 'email': email, 'sso_id': sso_id})
                     + ' check Samyama ' + str(matches) + ' output ' +
                     str({"ishangam_id": rec_id, "samyama_completed": samyama_completed}))
        return {"ishangam_id": rec_id, "samyama_completed": samyama_completed}

    if check_meditator:
        _logger.info('check meditator ' + str(matches))
        for rec in matches:
            if rec.is_meditator:
                is_meditator = True
                ieo_completed = 'NA'
                rec_id = rec.id
                break
    if check_ieo and not is_meditator:
        _logger.info('check ieo ' + str(matches))
        for rec in matches:
            if rec.ieo_date:
                ieo_completed = True
                is_meditator = rec.is_meditator
                rec_id = rec.id
                break
    if len(matches) > 0 and not rec_id:
        recent_contact = matches[0]
        for rec in matches:
            if rec.write_date > recent_contact.write_date:
                recent_contact = rec
        rec_id = recent_contact.id

    _logger.info('Get Contact Info ' + str({'name': name, 'phone': phone, 'email': email, 'sso_id': sso_id})
                 + ' check meditator/ieo ' + str(matches) + ' output ' +
                 str({'ishangam_id': rec_id,'is_meditator': is_meditator,'ieo_completed': ieo_completed,'matched_id': rec_id}))
    res = {
        'ishangam_id': rec_id,
        'is_meditator': is_meditator,
        'ieo_completed': ieo_completed,
        'matched_id': rec_id,
        'matched_rec': request.env['res.partner'].sudo().search([('id', '=', rec_id)])
    }
    return res


def checkIsNullOrEmpyt(string):
    retval = False
    if (string == None):
        retval = True
    strtrimvalue = string.strip()
    if (strtrimvalue == None or strtrimvalue == ''):
        retval = True

    return retval


def get_duplicaterecord(name=None, phone=None, email=None, startdate=None, language=None, event_type=None, sso_id=None):
    retval = False
    try:
        if event_type == "pournami":
            recs = request.env['program.lead'].sudo().search_count(
                [('sso_id', '=', sso_id), ('local_program_type_id', '=', "Online Pournami Satsang")])
        else:
            local_program_type_id = returneventprogramname(event_type)
            recs = request.env['program.lead'].sudo().search_count(
                [('sso_id', '=', sso_id), ('start_date', '=', startdate), ('trans_lang', '=', language),
                 ('local_program_type_id', '=', local_program_type_id)])

        retval = bool(recs)

    except Exception as ex:
        tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        _logger.error(tb_ex)
        retval = False
    finally:
        return retval


def check_region_mappign(selected_country_region_name, selected_timezone_region, selected_lang_region_name,
                         pournami_timezone):
    is_match = False
    if selected_country_region_name == selected_timezone_region and selected_lang_region_name in pournami_timezone.tzlang_support_language:
        is_match = True
    return is_match


def sendemailandsms(transprogramlead_rec):
    try:
        request.env['satsang.notification'].sudo().sendRegistrationEmailSMSCore(transprogramlead_rec)
    except Exception as ex:
        tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        _logger.error(tb_ex)


def returneventprogramname(eventtype):
    try:
        event_program_type = ""
        if eventtype == "shoonya":
            event_program_type = "Online SCK Satsang"
        elif eventtype == "shambhavi":
            event_program_type = "Online Satsang"
        elif eventtype == "ieo":
            event_program_type = "Online IEO Satsang"
        elif eventtype == "samyama":
            event_program_type = "Online Samyama Satsang"
        elif eventtype == "pournami":
            event_program_type = "Online Pournami Satsang"
        elif eventtype == "anandaalai":
            event_program_type = "Online Ananda Alai"
    except Exception as ex:
        tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        _logger.error(tb_ex)
    finally:
        return event_program_type


def returnevententtitlement(eventtype):
    try:
        evergreenentitlment = ""
        if eventtype == "shoonya":
            evergreenentitlment = "evergreen_shoonya_2020"
        elif eventtype == "shambhavi":
            evergreenentitlment = "evergreen_satsang_2020"
        elif eventtype == "ieo":
            evergreenentitlment = "evergreen_ieo_2020"
        elif eventtype == "samyama":
            evergreenentitlment = "evergreen_samyama_2020"

    except Exception as ex:
        tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        _logger.error(tb_ex)
    finally:
        return evergreenentitlment


def fmf_api_async_push(rec, recdynlead):
    """ API to push satsang_registration info """
    varprogramid = None
    _logger.info('Inside function fmf_api_async_push ' + str(rec))
    request.env['satsang.tempdebuglog'].sudo().create({
        'logtext': str(rec)
    })
    try:
        _logger.info(str(rec))
        if len(rec) > 0:
            jsnresponse = json.dumps(rec)
            _logger.info('Result : ' + jsnresponse)
        else:
            _logger.info('No data for id - ' + rec)
            raise
        try:
            headersinfo = {
                "content-Type": "application/x-www-form-urlencoded",
                "Accept": "application/json",
                "Cookie": ""
            }
            requestconsent_url = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.onlinessoapiurl')
            # "?key=" + request.env['ir.config_parameter'].sudo().get_param(
            #     'satsang.onlinessoapiurlkey')
            reqvar = requests.post(requestconsent_url, jsnresponse, headers=headersinfo)
            reqvar.raise_for_status()
            # progreginfo.sudo().write( {"pushedtoapi": 'True'})
            _logger.info('api triggered to push data')
            _logger.info(reqvar.text)
            recdynlead.teacher_name = 'fmf_api_async_pushed'
            request.env['satsang.tempdebuglog'].sudo().create({
                'logtext': 'API triggered' + reqvar.text
            })
        except Exception as ex2:
            _logger.info('In exception fmf_api_async_push')
            tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
            _logger.error(tb_ex)
            # progreginfo.sudo().write({"pushedtoapi": 'Error'})
            pass
        request.env['satsang.tempdebuglog'].sudo().create({
            'logtext': 'In exception at API trigger'
        })
    except Exception as ex2:
        _logger.info('error in fmf_api_async_push')
        tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
        _logger.error(tb_ex)
        print(tb_ex)
        request.env['satsang.tempdebuglog'].sudo().create({
            'logtext': tb_ex
        })


def fmf_api_platform_push(rec, recdynlead):
    varprogramid = None
    _logger.info('Inside function fmf_api_platform_push ' + str(rec))
    try:
        arrobj = []
        isishangamonev = request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')
        rec["rollNumber"] = "M2"
        if isishangamonev == 'ishangamone':
            rec["rollNumber"] = "M1"
        arrobj.append(rec)
        jsnresponse = json.dumps(arrobj)
        urlverifyflag = False
        try:
            headersinfo = {
                "content-Type": "application/json"
            }
            requestconsent_url = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.ssoapi_platform_url')
            ssoapi_platform_verify_flagvar = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.ssoapi_platform_verify_flag')
            if ssoapi_platform_verify_flagvar == "ssoapi_platform_url_verify_true":
                urlverifyflag = True
            # requestconsent_url = "https://goy-api.isha.in/admin/moksha/studentSessions?key=nZr4t7w!z%C*F-JaNdRgUkXp2s5v8x/A"
            _logger.info('fmf_api_platform_push url' + requestconsent_url)
            _logger.info('fmf_api_platform_push json string' + str(jsnresponse))
            _logger.info('fmf_api_platform_push url verifyflag' + str(urlverifyflag))
            reqvar = requests.post(requestconsent_url, jsnresponse, headers=headersinfo, verify=urlverifyflag)
            _logger.info(reqvar.text)
            reqvar.raise_for_status()
            # progreginfo.sudo().write( {"pushedtoapi": 'True'})
            _logger.info('fmf_api_platform_push completed')
            _logger.info(reqvar.text)
            if recdynlead.teacher_name:
                recdynlead.teacher_name = recdynlead.teacher_name + 'fmf_api_platform_pushed'
            else:
                recdynlead.teacher_name = 'fmf_api_platform_pushed'
        except Exception as ex2:
            _logger.info('In exception fmf_api_platform_push')
            tb_ex2 = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
            _logger.error(tb_ex2)
            pass
    except Exception as ex2:
        _logger.info('error in fmf_api_async_push')
        tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
        _logger.error(tb_ex)


class SatsangRegistration(http.Controller):
    @http.route('/onlinesatsang/registrationform/failure', type='http', auth="public", methods=["GET", "POST"],
                website=True,
                csrf=False)
    def failureonreg(self, tzvalues=""):
        values = {
            'temp_tz_value': tzvalues
        }
        return request.render('isha_satsang.pournamiregistrationfailure', values)

    @http.route('/onlinesatsang/success', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def successonreg(self, tzvalues="", gp=False):
        fmfthankyou_platform_joinsatsang = request.env['ir.config_parameter'].sudo().get_param(
            'satsang.fmfthankyou_platform_joinsatsang')
        gl_rd_url = f"https://isha.co/fmf-gcal-{tzvalues.split(' ')[::-1][0]}"
        ic_rd_url = f"https://isha.co/fmf-ical-{tzvalues.split(' ')[::-1][0]}"

        values = {
            'pournamithankyoujoinsaturl': fmfthankyou_platform_joinsatsang,
            'temp_tz_value': tzvalues,
            'gl_rd_url' : gl_rd_url,
            'ic_rd_url' : ic_rd_url,
            'gp': gp
        }

        if (request.httprequest.method == "GET"):
            return request.render('isha_satsang.pournamiregistrationsuccess', values)

    def _anandaalai_registration_process(self,language,event_type,utm_source="",utm_medium="",utm_campaign="",utm_term="",utm_content="",utm_referer="",**post):
        validation_errors = []
        values = {}
        event_program_type = returneventprogramname(event_type)
        try:
            _logger.info('Post Data ' + str(post))
            qsurl = str(request.httprequest.url).split('?')[1]
            profileid = post.get('jsgstngfmprofileid')

            first_name = post.get('jsgstngfmfirstname')
            last_name = post.get('jsgstngfmlastname')
            countrycode = post.get('jsgstngfmcountrycode')
            phone = post.get('jsgstngfmphone')
            wtsappyesno = post.get('jsgstngfmwtsappyesno')
            wtsappcountrycode = post.get('jsgstngfmwtsappcountrycode')
            wtsappphone = post.get('jsgstngfmwtsappphone')
            if (wtsappyesno == 'YES' or not wtsappcountrycode or not wtsappphone):
                wtsappcountrycode = countrycode
                wtsappphone = phone

            participantemail = post.get('jsgstngfmparticipantemail')

            ccodetmp = post.get('jsgstngfmcountry')
            if (ccodetmp and type(ccodetmp) == str and ccodetmp.isnumeric()):
                country = int(ccodetmp)
            else:
                country = ccodetmp

            pincode = post.get('jsgstngfmpincode')
            city = post.get('jsgstngfmcity')
            state = post.get('jsgstngfmstate')
            question = post.get('pquestion')
            selected_sso_country = post.get('selected_sso_country')
            # event_type_id = post.get('event_id')
            isishangamonev = request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')
            if (state):
                state = int(state)
            else:
                state = 0
            # preflang = post.get('jsgstngfmpreflang')
            if (isishangamonev == "ishangamone"):
                preftime = post.get('jsgstngfmpreftime')
            else:
                tpreftime = post.get('jsgstngfmpreftime')
                tschedule = request.env['satsang.schedule'].sudo().search([('pgmschedule_time', '=', tpreftime)])
                preftime = tschedule["id"]
                tcheck = post.get("program_terms_checkbox")
                if (tcheck):
                    termscheck = "True"
                else:
                    termscheck = "False"

            name = first_name + ' ' + last_name
            pournami_trans_lang = ""
            pournami_timezone = ""

            prgapplied = request.env['satsang.schedule'].sudo().search([('id', '=', preftime)])
            recdyn = {
                'profileid': profileid,
                'programapplied': preftime,
                'name': name,
                'countrycode': countrycode,
                'phone': phone,
                'wtsappyesno': wtsappyesno,
                'wtsappcountrycode': wtsappcountrycode,
                'wtsappphone': wtsappphone,
                'participantemail': participantemail,
                'country': country,
                'pincode': pincode,
                'city': city,
                'state': state,
                'preflang': prgapplied.pgmschedule_language,
                'preflang1': pournami_trans_lang,
                'preftime': prgapplied.pgmschedule_time
            }

            res = get_duplicaterecord(name, phone, participantemail, prgapplied.pgmschedule_startdate,
                                    prgapplied.pgmschedule_language, event_type, profileid)

            alreadyregprgdate = prgapplied.pgmschedule_startdate.strftime('%d-%m-%Y')
            if (res == True):
                values = {'object': name, 'date': alreadyregprgdate, 'language': language}
                if (isishangamonev == "ishangamone"):
                    return request.render("isha_satsang.anandaalaialreadyregistered", values)
                else:
                    return request.render("isha_satsang.anandaalaialreadyregistered_m2", values)

            # resofismed = get_is_meditator_or_ieo(name, phone, participantemail, True, False, False, False, profileid)
            # is_meditator = resofismed['is_meditator']
            # rec_id = resofismed['ishangam_id']


            is_meditator = global_api_call(first_name,last_name,participantemail,phone,profileid)
            _logger.info('Global API Call ' + str({'first_name': first_name, 'last_name': last_name,
                                                   'participantemail': participantemail,
                                                   'phone': phone, 'profileid': profileid})
                         + ' output ' +
                         str({"is_meditator": is_meditator}))
            resofismed = {
                'ishangam_id': 2,
                'is_meditator': is_meditator,
                'ieo_completed': False,
                'matched_id': 2,
            }
            rec_id = resofismed['ishangam_id']
            countryrec = request.env['res.country'].sudo().search([('id', '=', country)], order="code asc")

            countries_filtered = ["India", "Anguilla", "Antigua and Barbuda", "Argentina", "Aruba", "Bahamas",
                                  "Barbados", "Belize", "Bermuda",
                                  "Bolivia", "Bonaire", "Bouveil Island", "Brazil", "Canada", "Cayman Islands", "Chile",
                                  "Colombia", "Costa Rica",
                                  "Cuba", "Curaao", "Dominica", "Dominican Republic", "Ecuador", "El Salvador",
                                  "Falkland Islands", "French Guiana",
                                  "Greenland", "Grenada", "Guadeloupe", "Guatemala", "Guyana", "Haiti", "Honduras",
                                  "Jamaica", "Martinique", "Mexico",
                                  "Montserrat", "Nicaragua", "Panama", "Paraguay", "Peru", "Puerto Rico",
                                  "Saint Barthlemy", "Saint Kitts and Nevis",
                                  "Saint Lucia", "Saint Martin (French Part)", "Saint Pierre and Miqueion",
                                  "Saint Vincent and the Grenadines",
                                  "Sint Maarten (Dutch Part)", "South Georgia and the South Sandwich", "Suriname",
                                  "Trinidad and Tobago",
                                  "Turks and Caicos Islands", "United States of America", "Uruguay",
                                  "Venezuela (Bolivarian Republic of)",
                                  "Virgin Islands (British)", "Virgin Islands (U.S)"]

            if isishangamonev == "ishangamone":
                countries_scl = countries = request.env['res.country'].sudo().search(
                    [('name', '=', "India")])
                # countries = request.env['res.country'].sudo().search(
                #     [('name', '=', "India")])
            else:
                countries_scl = countries = request.env['res.country'].sudo().search([], order="name asc")
                # countries = request.env['res.country'].sudo().search([], order="name asc")

            result_countries = countries_scl | countries
            values = {
                'object': recdyn,
                'first_name': first_name,
                'last_name': last_name,
                'countries': result_countries,
                'submitted': post.get('submitted', False),
                'isoqs': qsurl,
                'email': participantemail
            }
            if is_meditator == True and rec_id != None:
                print('record found ', rec_id)
                # update respartner record
                staterec = request.env['res.country.state'].sudo().search([('id', '=', state)])
                rec_dict = {
                    'name': name,
                    'phone_country_code': countrycode,
                    'phone': phone,
                    'email': participantemail,
                    'city': city,
                    'state_id': state,
                    'state': staterec.name,
                    'zip': pincode,
                    'country_id': country,
                    'country': countryrec.code,
                    'whatsapp_country_code': wtsappcountrycode,
                    'whatsapp_number': wtsappphone,
                    'sso_id': profileid
                }
                pgm_schedule_info = {
                    'teacher_id': None,
                    'teacher_name': None,
                    'center_id': None,
                    'center_name': None,
                    'location': None,
                    'country': None,
                    'remarks': None,
                    'schedule_name': prgapplied.pgmschedule_programname,
                    'start_date': prgapplied.pgmschedule_startdate,
                    'end_date': prgapplied.pgmschedule_enddate,
                    'program_schedule_guid': None,
                    'localScheduleId': None,
                    'pii': rec_dict
                }
                request.env['program.lead'].sudo().create({
                    'program_schedule_info': pgm_schedule_info,
                    'program_name': prgapplied.pgmschedule_programtype.program_type,
                    'start_date': prgapplied.pgmschedule_startdate,
                    'reg_status': 'CONFIRMED',
                    'reg_date_time': datetime.datetime.now(),
                    'active': True,
                    'utm_medium': utm_medium,
                    'utm_campaign': utm_campaign,
                    'is_ie_done': True,
                    'record_name': name,
                    'record_phone': phone,
                    'record_email': participantemail,
                    'record_city': city,
                    'record_state': staterec.name,
                    'record_zip': pincode,
                    'record_country': countryrec.code,
                    'trans_lang': prgapplied.pgmschedule_language,
                    'bay_name': prgapplied.pgmschedule_time,
                    'pgm_type_master_id': prgapplied.pgmschedule_programtype.id,
                    'local_program_type_id': returneventprogramname(event_type),
                    'contact_id_fkey': 2,  # res_rec.id,
                    'pgm_remark': 'First timer',
                    'sso_id': profileid
                })
                evententtitlmenttobeset = returnevententtitlement(event_type)

                transprogramlead_rec = request.env['transprogramlead'].sudo().create({
                    'record_name': name,
                    'record_phone': phone,
                    'record_email': participantemail,
                    'trans_lang': prgapplied.pgmschedule_language,
                    'program_name': event_program_type,
                    'tzlangregion':prgapplied.pgmschedule_time
                })
                sendemailandsms(transprogramlead_rec)
                print('program lead confirmed record created')
                values = {'object': name, 'language': prgapplied.pgmschedule_language, 'isoqs': qsurl}
                if (isishangamonev == "ishangamone"):
                    return request.render('isha_satsang.anandaalairegistrationsuccess', values)
                else:
                    return request.render('isha_satsang.anandaalairegistrationsuccess_m2', values)
            if (is_meditator == False and event_type == "anandaalai"):

                if (isishangamonev == "ishangamone"):
                    return request.render('isha_satsang.onlinesatsangregnalternate', values)
                else:
                    if (termscheck == "True"):
                        return request.render('isha_satsang.onlineanandaalairegnalternate', values)
                    else:
                        return request.render('isha_satsang.anandaalairegistrationfailure_m2', values)
        except (UserError, AccessError, ValidationError) as exc:
            validation_errors.append(ustr(exc))
            request.env['satsang.tempdebuglog'].sudo().create({
                'logtext': ustr(exc)
            })
        except Exception as ex:
            tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
            _logger.error(tb_ex)
            stacktrace = traceback.print_exc()
            _logger.error(
                "Error caught during request creation", exc_info=True)
            validation_errors.append("Unknown server error. See server logs.")
            validation_errors.append(ex)

        if validation_errors:
            values['validation_errors'] = validation_errors
            _logger.log(25, validation_errors)
            logtext = 'Date:' + datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S") + ': ' + ' '.join(
                [str(elem) for elem in validation_errors])
            return request.render('isha_satsang.exception', values)

    def _pournami_registration_process(self, language, event_type, utm_source="", utm_medium="", utm_campaign="",utm_term="", utm_content="",utm_referer="", **post):
        try:
            pournami_trans_lang = post.get('language_preferred')
            pournami_timezone = post.get('timezone_preferred')

            name = f"{post.get('jsgstngfmfirstname')} {post.get('jsgstngfmlastname')}"

            event_program_type = returneventprogramname(event_type)


            country_rec = request.env['res.country'].sudo().search([('id', '=', post.get('jsgstngfmcountry'))],
                                                                   order="code asc")

            pournami_trans_lang = request.env['res.lang'].sudo().search([('id', '=', int(pournami_trans_lang))])

            pournami_timezone = request.env['satsang.tzlang'].with_context({'lang':pournami_trans_lang.code}).sudo().search([('id', '=', int(pournami_timezone))])

            prgapplied = request.env['satsang.schedule'].sudo().search([('id', '=', post.get('jsgstngfmpreflang'))])

            # checking duplicates
            res = get_duplicaterecord(name, post.get('jsgstngfmphone'), post.get('jsgstngfmparticipantemail'),
                                      prgapplied.pgmschedule_startdate,
                                      prgapplied.pgmschedule_language, event_type, post.get('jsgstngfmprofileid'))

            if res:
                return request.render("isha_satsang.pournamialreadyregistered")

            countrycode = post.get('jsgstngfmcountrycode')
            phone = post.get('jsgstngfmphone')
            wtsappyesno = post.get('jsgstngfmwtsappyesno')
            wtsappcountrycode = post.get('jsgstngfmwtsappcountrycode')
            wtsappphone = post.get('jsgstngfmwtsappphone')
            if (wtsappyesno == 'YES' or not wtsappcountrycode or not wtsappphone):
                wtsappcountrycode = countrycode
                wtsappphone = phone

            rec_dict = {
                'name': name,
                'phone_country_code': countrycode,
                'phone': phone,
                'email': post.get('jsgstngfmparticipantemail'),
                'zip': post.get('jsgstngfmpincode'),
                'country_id': post.get('jsgstngfmcountry'),
                'country': country_rec and country_rec.code or False,
                'whatsapp_country_code': wtsappcountrycode,
                'whatsapp_number': wtsappphone,
                'sso_id': post.get('jsgstngfmprofileid')
            }
            pgm_schedule_info = {
                'teacher_id': None,
                'teacher_name': None,
                'center_id': None,
                'center_name': None,
                'location': None,
                'country': None,
                'remarks': None,
                'schedule_name': prgapplied.pgmschedule_programname,
                'start_date': prgapplied.pgmschedule_startdate,
                'end_date': prgapplied.pgmschedule_enddate,
                'program_schedule_guid': None,
                'localScheduleId': None,
                'pii': rec_dict,
                "gp":post.get('gp',False)
            }
            # ToDo: use webinar_event_name for storing the lang
            recdynlead = request.env['program.lead'].sudo().create({
                'program_schedule_info': pgm_schedule_info,
                'program_name': prgapplied.pgmschedule_programtype.program_type,
                'start_date': prgapplied.pgmschedule_startdate,
                'reg_status': 'CONFIRMED',
                'reg_date_time': datetime.datetime.now(),
                'active': True,
                'utm_medium': utm_medium,
                'utm_campaign': utm_campaign,
                'utm_term': utm_term,
                'utm_source': utm_source,
                'utm_content': utm_content,
                'utm_referer': utm_referer,
                'is_ie_done': False,
                'record_name': name,
                'record_phone': phone,
                'record_email': post.get('jsgstngfmparticipantemail'),
                'record_zip': post.get('jsgstngfmpincode'),
                'record_country': country_rec and country_rec.code or False,
                'trans_lang': pournami_trans_lang.name.split('/')[0].strip(),
                'bay_name': pournami_timezone.tzlang_timezone,
                'pgm_type_master_id': prgapplied.pgmschedule_programtype.id,
                'contact_id_fkey': 2,  # rec.id,
                'pgm_remark': 'First timer',
                'query_from_user': post.get('pquestion', ''),
                'local_program_type_id': event_program_type,
                'sso_id': post.get('jsgstngfmprofileid'),
                'form_language': request.lang.code
            })

            date_time_vl = pournami_timezone and pournami_timezone.tzlang_displaytext and str(
                pournami_timezone.tzlang_displaytext) or ""

            fmfemailnextsatsangdate = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.fmfemailnextsatsangdate')
            d = datetime.datetime.strptime(fmfemailnextsatsangdate, '%Y-%m-%d')

            gcalurl = "https://isha.co/fmf-gcal-"
            ical = "https://isha.co/fmf-ical-"

            time_zone = pournami_timezone.tzlang_displaytext or 'IST',

            # created mail dict to be passed as context in mail template rendering
            mail_dict = {}

            # updated the context with current context so that no context data is lost
            mail_dict.update(request.env.context)

            mail_dict.update(dict(
                dt_val_tz=date_time_vl,
                datetime_text=str(d.strftime('%d-%b-%Y')),
                time_zone=time_zone,
                contact_text='',
                gl_rd_url=f"{gcalurl}{pournami_timezone.with_context({}).tzlang_displaytext.split(' ')[::-1][0]}",
                ic_rd_url=f"{ical}{pournami_timezone.with_context({}).tzlang_displaytext.split(' ')[::-1][0]}",
                lang=pournami_trans_lang.code
            ))

            selected_tzlangregion = pournami_timezone.tzlangregion and pournami_timezone.tzlangregion.tzlang_region.id or False

            key_value_rec = request.env['satsang.key.value.config'].sudo().search(
                [("key", "=", 'confirmation_addtional_text')])

            if key_value_rec and selected_tzlangregion:
                for r in key_value_rec:
                    if selected_tzlangregion in r.regionname.ids:
                        mail_dict.update({"contact_text": r.value})
                        break

            if post.get('gp', False) and post['gp'][:4:].lower() == 'true':
                mail_template_id = request.env.ref('isha_satsang.satsang_porumani_sucess_mail_template_gp')
            else:
                mail_template_id = request.env.ref('isha_satsang.satsang_porumani_sucess_mail_template_new')

            request.env['mail.template'].with_context(mail_dict).sudo().browse(mail_template_id.id).send_mail(
                recdynlead.id,
                force_send=True)
            _logger.info("..........After Mail.................")

            selected_sso_country_rec = request.env['res.country'].sudo().search(
                [('code', '=', post.get('selected_sso_country'))], order="code asc")

            api_info = {
                "fname": post.get('jsgstngfmfirstname', ''),
                "lname": post.get('jsgstngfmlastname', ''),
                "participantemail": post.get('jsgstngfmparticipantemail', ''),
                "phoneno": post.get('jsgstngfmphone', ''),
                "country": country_rec and country_rec.code or False,
                "schedule_name": prgapplied.pgmschedule_programname,
                "start_date": datetime.datetime.strftime(prgapplied.pgmschedule_startdate, '%Y-%m-%d'),
                "reg_status": "CONFIRMED",
                "sso_id": post.get('jsgstngfmprofileid', ''),
                "language": pournami_trans_lang and pournami_trans_lang.iso_code or 'en',
                "createdate": datetime.datetime.utcnow().strftime("%Y-%m-%d"),
                "preferredtimezone": pournami_timezone.tzlang_timezone,
                'zip': post.get('jsgstngfmpincode', '')
            }

            if selected_sso_country_rec.center_id.region_id.id in (40, 41, 47, 49):
                fmf_api_async_push(api_info, recdynlead)

            if request.env['ir.config_parameter'].sudo().get_param(
                'satsang.ssoapi_platform_push_flag') == 'ssoapi_platform_url_push_true':
                fmf_api_platform_push(api_info, recdynlead)

            tmpurl = request.httprequest.url.replace('registrationform', 'success')

            gp_value = "True" if post.get('gp', False) and post['gp'][:4:].lower() == 'true' else "False"
            return werkzeug.utils.redirect(tmpurl + "&tzvalues=" + pournami_timezone.with_context(
                {}).tzlang_displaytext.upper() + "&gp=" + gp_value)

        except Exception as e:
            _logger.info("--------Error in processing pournami form------")
            tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
            _logger.error(tb_ex)

            # check the translations
            return request.render('isha_satsang.exception')

    @http.route('/onlinesatsang/registrationform', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def onlinesatsangregnform(self, legal_entity="", consent_grant_status="", id_token="", language="", cbm="", cbnm="",
                              utm_source="", utm_medium="", utm_campaign="", utm_term="", utm_content="",
                              utm_referer="", eventtype="shambhavi", iscorredirect=False, **post):
        isevergreen = False
        eventtype = eventtype.lower()
        event_type = eventtype
        event_program_type = returneventprogramname(event_type)
        if (request.httprequest.method == "GET"):
            if post.get("is_logout") == 'yes':
                _logger.info("................inside get session")
                post.pop('is_logout')
                return request.render('isha_satsang.logout_sso_auto_redirect')
            elif (cbm != "" or event_type == "pournami" or event_type == "anandaalai"):
                ret = request.env['ir.config_parameter'].sudo().get_param('anandaalai_allow')
                if (ret == 'False' and event_type == 'anandaalai'):
                    return request.render('isha_satsang.pause_registration')

                _logger.info("................inside get session ssologin")
                validation_errors = []
                try:

                    ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                    cururls = str(request.httprequest.url).replace("http://", "https://", 1)
                    sso_log = {'request_url': str(cururls),
                               "callback_url": str(cururls),
                               "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                               # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                               "hash_value": "",
                               "action": "0",
                               "legal_entity": "IF",
                               "force_consent": "1"
                               }

                    # added the language in dict to render the sso portal based on that
                    if request.lang and request.lang.iso_code:
                        sso_log.update({'lang': request.lang.url_code})

                    ret_list = {'request_url': str(cururls),
                                "callback_url": str(cururls),
                                "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                                # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                                }
                    data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
                    data_enc = data_enc.replace("/", "\\/")
                    signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                                         digestmod=hashlib.sha256).hexdigest()
                    sso_log["hash_value"] = signature
                    sso_log["sso_logurl"] = request.env['ir.config_parameter'].sudo().get_param(
                        'satsang.SSO_LOGIN_URL1')
                    values = {
                        'sso_log': sso_log
                    }
                    return request.render('isha_satsang.onlinesatsangsso', values)
                except Exception as ex:
                    tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                    _logger.error(tb_ex)
                    _logger.error(
                        "Error caught during request creation", exc_info=True)
                    validation_errors.append("Unknown server error. See server logs.")
                    validation_errors.append(ex)
                    values = {'language': language}
                    if event_type == "pournami":
                        tmpurl = request.httprequest.url.replace('registrationform', 'registrationform/failure')
                        return werkzeug.utils.redirect(tmpurl + "&tzvalue=" + str(language))
                    else:
                        return request.render('isha_satsang.exception', values)
        # if request.httprequest.method == "GET":
        #     if post.get("is_logout") == 'yes':
        #       _logger.info("................inside post session")
        #       post.pop('is_logout')
        #       return request.render('isha_satsang.logout_sso_auto_redirect')
        #     validation_errors = []
        #     values = {}
        #     selected_region = ""
        #     selected_regioncoll = []
        #     ist_timezone = False
        #     lang_vals = []
        #     timezone_list = []
        #     isishangamonev = request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')
        #     try:
        #         qsurl = str(request.httprequest.url).split('?')[1]
        #         if event_type == 'anandaalai':
        #             countries_filtered=["India","Anguilla","Antigua and Barbuda","Argentina","Aruba","Bahamas","Barbados","Belize","Bermuda",
        #                                 "Bolivia","Bonaire","Bouveil Island","Brazil","Canada","Cayman Islands","Chile","Colombia","Costa Rica",
        #                                 "Cuba","Curaao","Dominica","Dominican Republic","Ecuador","El Salvador","Falkland Islands","French Guiana",
        #                                 "Greenland","Grenada","Guadeloupe","Guatemala","Guyana","Haiti","Honduras","Jamaica","Martinique","Mexico",
        #                                 "Montserrat","Nicaragua","Panama","Paraguay","Peru","Puerto Rico","Saint Barthlemy","Saint Kitts and Nevis",
        #                                 "Saint Lucia","Saint Martin (French Part)","Saint Pierre and Miqueion","Saint Vincent and the Grenadines",
        #                                 "Sint Maarten (Dutch Part)","South Georgia and the South Sandwich","Suriname","Trinidad and Tobago",
        #                                 "Turks and Caicos Islands","United States of America","Uruguay","Venezuela (Bolivarian Republic of)",
        #                                 "Virgin Islands (British)","Virgin Islands (U.S)"]
        #             if isishangamonev == "ishangamone":
        #                 countries_scl = request.env['res.country'].sudo().search(
        #                     [('name', '=', "India")])
        #                 countries = request.env['res.country'].sudo().search(
        #                     [('name', '=', "India")])
        #             else:
        #                 countries_scl = request.env['res.country'].sudo().search(
        #                     [('name', 'not in', countries_filtered)])
        #                 countries = request.env['res.country'].sudo().search(
        #                     [('name', 'not in', countries_filtered)])
        #         elif event_type == 'pournami':
        #             countries = countries_scl = False
        #         else:
        #             if isishangamonev == "ishangamone":
        #                 countries_scl = request.env['res.country'].sudo().search(
        #                     ['|', ('name', '=', 'India'), ('name', '=', 'Nepal')])
        #                 countries = request.env['res.country'].sudo().search(
        #                     ['|', '|', '|', ('name', '=', 'Sri Lanka'), ('name', '=', 'Bangladesh'),
        #                         ('name', '=', 'Pakistan'), ('name', '=', 'Afghanistan')])
        #             else:
        #                 countries_scl = request.env['res.country'].sudo().search(
        #                     [('name', '!=', 'India'), ('name', '!=', 'Sri Lanka'), ('name', '!=', 'Nepal')],
        #                         order="name asc")
        #                 countries = request.env['res.country'].sudo().search(
        #                     [('name', '!=', 'Sri Lanka'), ('name', '!=', 'Bangladesh'), ('name', '!=', 'Pakistan'),
        #                         ('name', '!=', 'Afghanistan')], order="name asc")
        #
        #         result_countries = countries_scl and countries and countries_scl | countries or False
        #
        #         # ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
		# 		#
        #         # api_key = request.env['ir.config_parameter'].sudo().get_param(
        #         #      'satsang.SSO_API_KEY1')  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
        #         # session_id = request.httprequest.form['session_id']
        #         # hash_val = hash_hmac({'session_id': session_id}, ret)
        #         # data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
        #         # request_url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
        #         # sso_user_profile = eval(sso_get_user_profile(request_url, data).text)
        #
        #         fullprofileresponsedatVar = get_fullprofileresponse_DTO()
        #         # if "autologin_email" in sso_user_profile:
        #         #     fullprofileresponsedatVar["basicProfile"]["email"] = sso_user_profile["autologin_email"]
        #         #     fullprofileresponsedatVar["profileId"] = sso_user_profile["autologin_profile_id"]
        #         # if consent_grant_status == "1":
        #         #     fullprofileresponsedatVar = sso_get_full_data(sso_user_profile['autologin_profile_id'])
        #
        #         if (event_type == "anandaalai"):
        #             countrycodep = fullprofileresponsedatVar["basicProfile"]["countryOfResidence"]
        #             if (isishangamonev=="ishangamone"):
        #                 fetched_program_schedules_morning = request.env['satsang.schedule'].sudo().search(
        #                     ['&', ('pgmschedule_startdate', '>=', datetime.datetime.now()),
        #                     ('pgmschedule_registrationopen', '=', True),
        #                     ('pgmschedule_programtype.local_program_type_id', '=', event_program_type),
        #                     ('pgmschedule_time', 'ilike', '%Morning%')])
        #                 fetched_program_schedules_noon = request.env['satsang.schedule'].sudo().search(
        #                     ['&', ('pgmschedule_startdate', '>=', datetime.datetime.now()),
        #                     ('pgmschedule_registrationopen', '=', True),
        #                     ('pgmschedule_programtype.local_program_type_id', '=', event_program_type),
        #                     ('pgmschedule_time', 'ilike', '%Noon%')])
        #                 fetched_program_schedules_evening = request.env['satsang.schedule'].sudo().search(
        #                     ['&', ('pgmschedule_startdate', '>=', datetime.datetime.now()),
        #                     ('pgmschedule_registrationopen', '=', True),
        #                     ('pgmschedule_programtype.local_program_type_id', '=', event_program_type),
        #                     ('pgmschedule_time', 'ilike', '%Evening%')])
        #                 fetched_program_schedules = fetched_program_schedules_morning | fetched_program_schedules_noon | fetched_program_schedules_evening
        #             else:
        #                 fetched_program_schedules = request.env['satsang.schedule'].sudo().search(
        #                   ['&', ('pgmschedule_startdate', '>=', datetime.datetime.now()),
        #                    ('pgmschedule_registrationopen', '=', True),
        #                    ('pgmschedule_programtype.local_program_type_id', '=', event_program_type)])
        #         else:
        #             fetched_program_schedules = request.env['satsang.schedule'].sudo().search(
        #                 ['&', ('pgmschedule_startdate', '>=', datetime.datetime.now()),
        #                 ('pgmschedule_registrationopen', '=', True),
        #                 ('pgmschedule_programtype.local_program_type_id', '=', event_program_type)])
        #
        #         selected_region = ""
        #         selected_sso_country = ""
        #         if event_type == 'pournami':
        #             countrycodep = fullprofileresponsedatVar["basicProfile"]["countryOfResidence"]
        #             selected_sso_country = countrycodep
        #
        #             for s in fetched_program_schedules.mapped('pgmschedule_tz_region'):
        #                 selected_region = s.tzlang_tzregion
        #
        #             tz_lang_recs = request.env['satsang.tzlang'].sudo().search([('is_active', '=', True)])
        #
        #             lang_vals = [{"time_zone_id": t.id,
        #                         "time_zone": t.tzlang_timezone,
        #                         "lang_id": l.id,
        #                         "lang_name": l.name}
        #                         for t in
        #                             tz_lang_recs
        #                         for l in
        #                             t.tzlang_support_language]
        #
        #             timezone_list = [{"time_zone_id": t.id,
        #                            "time_zone": t.tzlang_timezone,
        #                            "tz_displaytext": t.tzlang_displaytext}
        #                           for t
        #                               in tz_lang_recs]
        #
        #             if post.get('gp', False) and post['gp'][:4:].lower() == 'true':
        #                 ist_timezone = tz_lang_recs.filtered(lambda t: t.tzlang_timezone == "Asia/Kolkata")
        #                 ist_timezone = ist_timezone and ist_timezone[0].id or False
        #
        #         values = {
        #             'countries': result_countries,
        #             'programschedules': fetched_program_schedules,
        #             'selected_region': selected_region,
        #             'states': request.env['res.country.state'].sudo().search([]),
        #             'submitted': post.get('submitted', False),
        #             'isofname': fullprofileresponsedatVar,
        #             'isoqs': qsurl,
        #             'timezone_list': timezone_list,
        #             'lang_vals': lang_vals,
        #             'ist_timezone': ist_timezone,
        #             'form_lang_code': request.lang.code,
        #             'eventtype': eventtype,
        #             'is_cor_redirect': iscorredirect,
        #             'is_ishagam_one': isishangamonev or "",
        #             'is_ishagam_one_url': request.env['ir.config_parameter'].sudo().get_param('isha_satsang.ishangamone_base_url') or "",
        #             'is_ishagam_two_url': request.env['ir.config_parameter'].sudo().get_param('isha_satsang.ishangamtwo_base_url') or ""
        #         }
        #
        #         evententtitlmenttobechecked = returnevententtitlement(event_type)
        #         isevergreen = sso_has_entitlement(sso_user_profile['autologin_profile_id'],
        #                                         evententtitlmenttobechecked) if evententtitlmenttobechecked else False
        #
        #         values['validation_errors'] = validation_errors
        #         values['selected_sso_country'] = selected_sso_country
        #         jspgremstatecollpour = []
        #         if event_type == 'anandaalai':
        #             for st in values['states']:
        #                 jspgremstatecollpour.append(
        #                     {"valueattr": st.id, "statenameattr": st.name, "countryidattr": st.country_id.id})
        #
        #             # values['jspgremstatecollpour'] = jspgremstatecollpour
        #             jspgremstatecollpour = json.dumps(jspgremstatecollpour)
        #             values['jspgremstatecollpour'] = jspgremstatecollpour
        #             _logger.info("....full path ....")
        #
        #             _logger.info(http.request.httprequest.full_path)
        #
        #             if not iscorredirect:
        #                 if isishangamonev == "ishangamone" and countrycodep != "IN":
        #                     tow_url = request.env['ir.config_parameter'].sudo().get_param(
        #                         'isha_satsang.ishangamtwo_base_url') if request.env[
        #                         'ir.config_parameter'].sudo().get_param(
        #                         'isha_satsang.ishangamtwo_base_url') else ""
        #
        #                     re_url = str(tow_url) + http.request.httprequest.full_path
        #
        #                     _logger.info("....redirec to ....")
        #
        #                     _logger.info(re_url)
        #
        #                     return request.redirect(re_url)
        #
        #                 if isishangamonev != "ishangamone" and countrycodep == "IN":
        #                     one_url = request.env['ir.config_parameter'].sudo().get_param(
        #                         'isha_satsang.ishangamone_base_url') if request.env[
        #                         'ir.config_parameter'].sudo().get_param(
        #                         'isha_satsang.ishangamone_base_url') else ""
        #
        #                     re_url = str(one_url) + http.request.httprequest.full_path
        #
        #                     _logger.info("....redirec to ....")
        #
        #                     _logger.info(re_url)
        #
        #                     return request.redirect(re_url)
        #
        #             if isishangamonev == "ishangamone":
        #                 return request.render('isha_satsang.onlineanandaalairegn', values)
        #             else:
        #                 return request.render('isha_satsang.onlineanandaalairegnm2', values)
        #         if event_type == 'pournami':
        #             for st in values['states']:
        #                 jspgremstatecollpour.append(
        #                     {"valueattr": st.id, "statenameattr": st.name, "countryidattr": st.country_id.id})
        #
        #             # values['jspgremstatecollpour'] = jspgremstatecollpour
        #             jspgremstatecollpour = json.dumps(jspgremstatecollpour)
        #             values['jspgremstatecollpour'] = jspgremstatecollpour
        #             _logger.info("....full path ....")
        #
        #             _logger.info(http.request.httprequest.full_path)
        #
        #             if not iscorredirect:
        #                 if isishangamonev == "ishangamone" and countrycodep != "IN":
        #                     tow_url = request.env['ir.config_parameter'].sudo().get_param(
        #                         'isha_satsang.ishangamtwo_base_url') if request.env[
        #                         'ir.config_parameter'].sudo().get_param(
        #                         'isha_satsang.ishangamtwo_base_url') else ""
        #
        #                     re_url = str(tow_url) + http.request.httprequest.full_path
        #
        #                     _logger.info("....redirec to ....")
        #
        #                     _logger.info(re_url)
        #
        #                     return request.redirect(re_url)
        #
        #                 if isishangamonev != "ishangamone" and countrycodep == "IN":
        #                     one_url = request.env['ir.config_parameter'].sudo().get_param(
        #                         'isha_satsang.ishangamone_base_url') if request.env[
        #                         'ir.config_parameter'].sudo().get_param(
        #                         'isha_satsang.ishangamone_base_url') else ""
        #
        #                     re_url = str(one_url) + http.request.httprequest.full_path
        #
        #                     _logger.info("....redirec to ....")
        #
        #                     _logger.info(re_url)
        #
        #                     return request.redirect(re_url)
        #
        #                 return request.render('isha_satsang.onlinesatpournamisangregn', values)
        #
        #         if (isevergreen == True):
        #             if (language == 'english'):
        #                 return request.render('isha_satsang.onlinesatsangregnshortform', values)
        #             elif (language == 'tamil'):
        #                 return request.render('isha_satsang.onlinesatsangregnshortformtamil', values)
        #             elif (language == 'hindi'):
        #                 return request.render('isha_satsang.onlinesatsangregnshortformhindi', values)
        #             elif (language == 'telugu'):
        #                 return request.render('isha_satsang.onlinesatsangregnshortformtelugu', values)
        #             elif (language == 'kannada'):
        #                 return request.render('isha_satsang.onlinesatsangregnshortformkannada', values)
        #             else:
        #                 return request.render('isha_satsang.onlinesatsangregnshortform', values)
        #         else:
        #             if (language == 'english'):
        #                 return request.render('isha_satsang.onlinesatsangregn', values)
        #             elif (language == 'tamil'):
        #                 return request.render('isha_satsang.onlinesatsangregntamil', values)
        #             elif (language == 'hindi'):
        #                 return request.render('isha_satsang.onlinesatsangregnhindi', values)
        #             elif (language == 'telugu'):
        #                 return request.render('isha_satsang.onlinesatsangregntelugu', values)
        #             elif (language == 'kannada'):
        #                 return request.render('isha_satsang.onlinesatsangregnkannada', values)
        #             else:
        #                 return request.render('isha_satsang.onlinesatsangregn', values)
        #
        #     except Exception as ex:
        #         tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        #         _logger.error(tb_ex)
        #         _logger.error(
        #              "online satsang cb2", exc_info=True)
        #         _logger.exception("postbacks: %s" % str(ex))
        elif request.httprequest.method == "POST" and "session_id" in request.httprequest.form:
            if post.get("is_logout") == 'yes':
                _logger.info("................inside post session")
                post.pop('is_logout')
                return request.render('isha_satsang.logout_sso_auto_redirect')
            validation_errors = []
            values = {}
            selected_region = ""
            selected_regioncoll = []
            ist_timezone = False
            lang_vals = []
            timezone_list = []
            isishangamonev = request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')
            try:
                qsurl = str(request.httprequest.url).split('?')[1]
                if event_type == 'anandaalai':
                    countries_filtered = ["India", "Anguilla", "Antigua and Barbuda", "Argentina", "Aruba", "Bahamas", "Barbados", "Belize", "Bermuda",
                                         "Bolivia", "Bonaire", "Bouveil Island", "Brazil", "Canada", "Cayman Islands", "Chile", "Colombia", "Costa Rica",
                                         "Cuba", "Curaao", "Dominica", "Dominican Republic", "Ecuador", "El Salvador", "Falkland Islands", "French Guiana",
                                         "Greenland", "Grenada", "Guadeloupe", "Guatemala", "Guyana", "Haiti", "Honduras", "Jamaica", "Martinique", "Mexico",
                                         "Montserrat", "Nicaragua", "Panama", "Paraguay", "Peru", "Puerto Rico", "Saint Barthlemy", "Saint Kitts and Nevis",
                                         "Saint Lucia", "Saint Martin (French Part)", "Saint Pierre and Miqueion", "Saint Vincent and the Grenadines",
                                         "Sint Maarten (Dutch Part)", "South Georgia and the South Sandwich", "Suriname", "Trinidad and Tobago",
                                         "Turks and Caicos Islands", "United States of America", "Uruguay", "Venezuela (Bolivarian Republic of)",
                                         "Virgin Islands (British)", "Virgin Islands (U.S)"]
                    if isishangamonev == "ishangamone":
                        countries_scl = countries = request.env['res.country'].sudo().search(
                            [('name', '=', "India")])
                        # countries = request.env['res.country'].sudo().search(
                        #     [('name', '=', "India")])
                    else:
                        countries_scl = countries = request.env['res.country'].sudo().search(
                            [('name', 'not in', countries_filtered)],order="name asc")
                        # countries = request.env['res.country'].sudo().search(
                        #     [('name', 'not in', countries_filtered)],order="name asc")
                        mobile_countries = request.env['res.country'].sudo().search([], order="name asc")
                elif event_type == 'pournami':
                    countries = countries_scl = False
                else:
                    if isishangamonev == "ishangamone":
                        countries_scl = request.env['res.country'].sudo().search(
                            ['|', ('name', '=', 'India'), ('name', '=', 'Nepal')])
                        countries = request.env['res.country'].sudo().search(
                            ['|', '|', '|', ('name', '=', 'Sri Lanka'), ('name', '=', 'Bangladesh'),
                             ('name', '=', 'Pakistan'), ('name', '=', 'Afghanistan')])
                    else:
                        countries_scl = request.env['res.country'].sudo().search(
                            [('name', '!=', 'India'), ('name', '!=', 'Sri Lanka'), ('name', '!=', 'Nepal')],
                            order="name asc")
                        countries = request.env['res.country'].sudo().search(
                            [('name', '!=', 'Sri Lanka'), ('name', '!=', 'Bangladesh'), ('name', '!=', 'Pakistan'),
                             ('name', '!=', 'Afghanistan')], order="name asc")

                result_countries =  countries_scl and countries and countries_scl | countries or False


                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')

                api_key = request.env['ir.config_parameter'].sudo().get_param(
                    'satsang.SSO_API_KEY1')  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                session_id = request.httprequest.form['session_id']
                hash_val = hash_hmac({'session_id': session_id}, ret)
                data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
                request_url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
                sso_user_profile = eval(sso_get_user_profile(request_url, data).text)

                fullprofileresponsedatVar = get_fullprofileresponse_DTO()
                if "autologin_email" in sso_user_profile:
                    fullprofileresponsedatVar["basicProfile"]["email"] = sso_user_profile["autologin_email"]
                    fullprofileresponsedatVar["profileId"] = sso_user_profile["autologin_profile_id"]
                if consent_grant_status == "1":
                    fullprofileresponsedatVar = sso_get_full_data(sso_user_profile['autologin_profile_id'])

                if (event_type=="anandaalai"):
                    if (isishangamonev == "ishangamone"):
                        fetched_program_schedules_morning = request.env['satsang.schedule'].sudo().search(
                            ['&', ('pgmschedule_startdate', '>=', datetime.datetime.now()),
                            ('pgmschedule_registrationopen', '=', True),
                            ('pgmschedule_programtype.local_program_type_id', '=', event_program_type),
                            ('pgmschedule_time', 'ilike', '%Morning%')])
                        fetched_program_schedules_noon = request.env['satsang.schedule'].sudo().search(
                            ['&', ('pgmschedule_startdate', '>=', datetime.datetime.now()),
                            ('pgmschedule_registrationopen', '=', True),
                            ('pgmschedule_programtype.local_program_type_id', '=', event_program_type),
                            ('pgmschedule_time', 'ilike', '%Noon%')])
                        fetched_program_schedules_evening = request.env['satsang.schedule'].sudo().search(
                            ['&', ('pgmschedule_startdate', '>=', datetime.datetime.now()),
                            ('pgmschedule_registrationopen', '=', True),
                            ('pgmschedule_programtype.local_program_type_id', '=', event_program_type),
                            ('pgmschedule_time', 'ilike', '%Evening%')])
                        fetched_program_schedules = fetched_program_schedules_morning | fetched_program_schedules_noon | fetched_program_schedules_evening
                    else:
                        fetched_program_schedules = request.env['satsang.schedule'].sudo().search(
                            ['&', ('pgmschedule_startdate', '>=', datetime.datetime.now()),
                            ('pgmschedule_registrationopen', '=', True),
                            ('pgmschedule_programtype.local_program_type_id', '=', event_program_type)])
                else:
                    fetched_program_schedules = request.env['satsang.schedule'].sudo().search(
                        ['&', ('pgmschedule_startdate', '>=', datetime.datetime.now()),
                        ('pgmschedule_registrationopen', '=', True),
                        ('pgmschedule_programtype.local_program_type_id', '=', event_program_type)])

                selected_region = ""
                selected_sso_country = ""
                # if event_type == "anandaalai":
                #     countrycodep = fullprofileresponsedatVar["basicProfile"]["countryOfResidence"]
                #     selected_sso_country = countrycodep

                if event_type == 'pournami':
                    countrycodep = fullprofileresponsedatVar["basicProfile"]["countryOfResidence"]
                    selected_sso_country = countrycodep

                    for s in fetched_program_schedules.mapped('pgmschedule_tz_region'):
                        selected_region = s.tzlang_tzregion

                    tz_lang_recs = request.env['satsang.tzlang'].sudo().search([('is_active', '=', True)])

                    lang_vals = [{"time_zone_id": t.id,
                                  "time_zone": t.tzlang_timezone,
                                  "lang_id": l.id,
                                  "lang_name": l.name}
                                 for t in
                                 tz_lang_recs
                                 for l in
                                 t.tzlang_support_language]


                    timezone_list = [{"time_zone_id": t.id,
                                      "time_zone": t.tzlang_timezone,
                                      "tz_displaytext": t.tzlang_displaytext}
                                     for t
                                     in tz_lang_recs]

                    if post.get('gp', False) and post['gp'][:4:].lower() == 'true':
                        ist_timezone = tz_lang_recs.filtered(lambda t: t.tzlang_timezone == "Asia/Kolkata")
                        ist_timezone = ist_timezone and ist_timezone[0].id or False

                values = {
                    'countries': result_countries,
                    'phonecountries':mobile_countries if (isishangamonev != "ishangamone") else None,
                    'programschedules': fetched_program_schedules,
                    'selected_region': selected_region,
                    'states': request.env['res.country.state'].sudo().search([]),
                    'submitted': post.get('submitted', False),
                    'isofname': fullprofileresponsedatVar,
                    'isoqs': qsurl,
                    'timezone_list': timezone_list,
                    'lang_vals': lang_vals,
                    'ist_timezone':ist_timezone,
                    'form_lang_code': request.lang.code,
                    'eventtype': eventtype,
                    'is_cor_redirect': iscorredirect,
                    'is_ishagam_one': isishangamonev or "",
                    'is_ishagam_one_url': request.env['ir.config_parameter'].sudo().get_param('isha_satsang.ishangamone_base_url') or "",
                    'is_ishagam_two_url': request.env['ir.config_parameter'].sudo().get_param('isha_satsang.ishangamtwo_base_url') or ""
                }

                evententtitlmenttobechecked = returnevententtitlement(event_type)
                isevergreen = sso_has_entitlement(sso_user_profile['autologin_profile_id'],
                                                  evententtitlmenttobechecked) if evententtitlmenttobechecked else False

                values['validation_errors'] = validation_errors
                values['selected_sso_country'] = selected_sso_country
                jspgremstatecollpour = []
                if event_type == 'anandaalai':
                    # for st in values['states']:
                    #     jspgremstatecollpour.append(
                    #         {"valueattr": st.id, "statenameattr": st.name, "countryidattr": st.country_id.id})
                    #
                    # # values['jspgremstatecollpour'] = jspgremstatecollpour
                    # jspgremstatecollpour = json.dumps(jspgremstatecollpour)
                    # values['jspgremstatecollpour'] = jspgremstatecollpour
                    # _logger.info("....full path ....")
                    #
                    # _logger.info(http.request.httprequest.full_path)
                    #
                    # if not iscorredirect:
                    #     if isishangamonev == "ishangamone" and countrycodep != "IN":
                    #         tow_url = request.env['ir.config_parameter'].sudo().get_param(
                    #             'isha_satsang.ishangamtwo_base_url') if request.env[
                    #             'ir.config_parameter'].sudo().get_param(
                    #             'isha_satsang.ishangamtwo_base_url') else ""
                    #
                    #         re_url = str(tow_url) + http.request.httprequest.full_path
                    #
                    #         _logger.info("....redirec to ....")
                    #
                    #         _logger.info(re_url)
                    #
                    #         return request.redirect(re_url)
                    #
                    #     if isishangamonev != "ishangamone" and countrycodep == "IN":
                    #         one_url = request.env['ir.config_parameter'].sudo().get_param(
                    #             'isha_satsang.ishangamone_base_url') if request.env[
                    #             'ir.config_parameter'].sudo().get_param(
                    #
                    #             'isha_satsang.ishangamone_base_url') else ""
                    #
                    #         re_url = str(one_url) + http.request.httprequest.full_path
                    #
                    #         _logger.info("....redirec to ....")
                    #
                    #         _logger.info(re_url)
                    #
                    #         return request.redirect(re_url)

                    if isishangamonev == "ishangamone":
                        return request.render('isha_satsang.onlineanandaalairegn', values)
                    else:
                        return request.render('isha_satsang.onlineanandaalairegnm2', values)
                if event_type == 'pournami':
                    for st in values['states']:
                        jspgremstatecollpour.append(
                            {"valueattr": st.id, "statenameattr": st.name, "countryidattr": st.country_id.id})

                    # values['jspgremstatecollpour'] = jspgremstatecollpour
                    jspgremstatecollpour = json.dumps(jspgremstatecollpour)
                    values['jspgremstatecollpour'] = jspgremstatecollpour
                    _logger.info("....full path ....")

                    _logger.info(http.request.httprequest.full_path)

                    if not iscorredirect:
                        if isishangamonev == "ishangamone" and countrycodep != "IN":
                            tow_url = request.env['ir.config_parameter'].sudo().get_param(
                                'isha_satsang.ishangamtwo_base_url') if request.env[
                                'ir.config_parameter'].sudo().get_param(
                                'isha_satsang.ishangamtwo_base_url') else ""

                            re_url = str(tow_url) + http.request.httprequest.full_path

                            _logger.info("....redirec to ....")

                            _logger.info(re_url)

                            return request.redirect(re_url)

                        if isishangamonev != "ishangamone" and countrycodep == "IN":
                            one_url = request.env['ir.config_parameter'].sudo().get_param(
                                'isha_satsang.ishangamone_base_url') if request.env[
                                'ir.config_parameter'].sudo().get_param(

                                'isha_satsang.ishangamone_base_url') else ""

                            re_url = str(one_url) + http.request.httprequest.full_path

                            _logger.info("....redirec to ....")

                            _logger.info(re_url)

                            return request.redirect(re_url)

                    return request.render('isha_satsang.onlinesatpournamisangregn', values)
                if (isevergreen == True):
                    if (language == 'english'):
                        return request.render('isha_satsang.onlinesatsangregnshortform', values)
                    elif (language == 'tamil'):
                        return request.render('isha_satsang.onlinesatsangregnshortformtamil', values)
                    elif (language == 'hindi'):
                        return request.render('isha_satsang.onlinesatsangregnshortformhindi', values)
                    elif (language == 'telugu'):
                        return request.render('isha_satsang.onlinesatsangregnshortformtelugu', values)
                    elif (language == 'kannada'):
                        return request.render('isha_satsang.onlinesatsangregnshortformkannada', values)
                    else:
                        return request.render('isha_satsang.onlinesatsangregnshortform', values)
                else:
                    if (language == 'english'):
                        return request.render('isha_satsang.onlinesatsangregn', values)
                    elif (language == 'tamil'):
                        return request.render('isha_satsang.onlinesatsangregntamil', values)
                    elif (language == 'hindi'):
                        return request.render('isha_satsang.onlinesatsangregnhindi', values)
                    elif (language == 'telugu'):
                        return request.render('isha_satsang.onlinesatsangregntelugu', values)
                    elif (language == 'kannada'):
                        return request.render('isha_satsang.onlinesatsangregnkannada', values)
                    else:
                        return request.render('isha_satsang.onlinesatsangregn', values)

            except Exception as ex:
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                _logger.error(tb_ex)
                _logger.error(
                    "online satsang cb2", exc_info=True)
                _logger.exception("postbacks: %s" % str(ex))
        elif request.httprequest.method == "POST":

            if event_type == 'pournami':
                return self._pournami_registration_process(language, event_type, utm_source, utm_medium, utm_campaign,
                                                           utm_term, utm_content, utm_referer, **post)
            if event_type == 'anandaalai':
                return self._anandaalai_registration_process(language, event_type, utm_source, utm_medium, utm_campaign,
                                                           utm_term, utm_content, utm_referer, **post)

            validation_errors = []
            values = {}
            try:
                qsurl = str(request.httprequest.url).split('?')[1]
                profileid = post.get('jsgstngfmprofileid')

                first_name = post.get('jsgstngfmfirstname')
                last_name = post.get('jsgstngfmlastname')
                countrycode=post.get('jsgstngfmcountrycode')
                phone = post.get('jsgstngfmphone')
                wtsappyesno = post.get('jsgstngfmwtsappyesno')
                wtsappcountrycode = post.get('jsgstngfmwtsappcountrycode')
                wtsappphone = post.get('jsgstngfmwtsappphone')
                if (wtsappyesno == 'YES' or not wtsappcountrycode or not wtsappphone):
                    wtsappcountrycode = countrycode
                    wtsappphone = phone
                participantemail = post.get('jsgstngfmparticipantemail')

                ccodetmp = post.get('jsgstngfmcountry')
                if (ccodetmp and type(ccodetmp)==str and ccodetmp.isnumeric()):
                    country = int(ccodetmp)
                else:
                    country = ccodetmp

                pincode = post.get('jsgstngfmpincode')
                city = post.get('jsgstngfmcity')
                state = post.get('jsgstngfmstate')
                question = post.get('pquestion')
                selected_sso_country = post.get('selected_sso_country')
                # event_type_id = post.get('event_id')
                isishangamonev = request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')
                if (state):
                    state = int(state)
                else:
                    state = 0
                preflang = post.get('jsgstngfmpreflang')
                preftime = post.get('jsgstngfmpreftime')

                name = first_name + ' ' + last_name
                if event_type == 'pournami':
                    pournami_trans_lang = post.get('language_preferred')
                    pournami_timezone = post.get('timezone_preferred')
                else:
                    pournami_trans_lang = ""
                    pournami_timezone = ""

                prgapplied = request.env['satsang.schedule'].sudo().search([('id', '=', preflang)])
                recdyn = {
                    'profileid': profileid,
                    'programapplied': preflang if (event_type != 'anandaalai') else preftime,
                    'name': name,
                    'countrycode': countrycode,
                    'phone': phone,
                    'wtsappyesno': wtsappyesno,
                    'wtsappcountrycode': wtsappcountrycode,
                    'wtsappphone': wtsappphone,
                    'participantemail': participantemail,
                    'country': country,
                    'pincode': pincode,
                    'city': city,
                    'state': state,
                    'preflang': prgapplied.pgmschedule_language,
                    'preflang1': pournami_trans_lang,
                    'preftime': prgapplied.pgmschedule_time
                }

                res = get_duplicaterecord(name, phone, participantemail, prgapplied.pgmschedule_startdate,
                                          prgapplied.pgmschedule_language, event_type,profileid)

                alreadyregprgdate = prgapplied.pgmschedule_startdate.strftime('%d-%m-%Y')
                if (res == True):
                    values = {'object': name, 'date': alreadyregprgdate, 'language': language}
                    if event_type == 'pournami':
                        return request.render("isha_satsang.pournamialreadyregistered", values)
                    else:
                        return request.render("isha_satsang.alreadyregistered", values)

                if event_type == 'pournami':
                    resofismed = {
                        'ishangam_id': 2,
                        'is_meditator': False,
                        'ieo_completed': False,
                        'matched_id': 2
                    }
                    is_meditator = False
                elif event_type == "shoonya":
                    resofismed = get_is_meditator_or_ieo(name,phone,participantemail,False,False,True,False,profileid)
                    is_meditator = resofismed['shoonya_completed']
                elif event_type == "shambhavi":
                    resofismed = get_is_meditator_or_ieo(name,phone,participantemail,True,False,False,False,profileid)
                    is_meditator = resofismed['is_meditator']
                elif event_type == "ieo":
                    resofismed = get_is_meditator_or_ieo(name,phone,participantemail,False,True,False,False,profileid)
                    is_meditator = resofismed['ieo_completed']
                elif event_type == "samyama":
                    resofismed = get_is_meditator_or_ieo(name,phone,participantemail,False,False,False,True,profileid)
                    is_meditator = resofismed['samyama_completed']
                else:
                    resofismed = {}

                rec_id = resofismed['ishangam_id']
                # res_rec = resofismed['matched_rec']
                # rec_id = resofismed.get('matched_id', "not_available")
                # res_rec = resofismed.get('matched_rec', "not_available")


                countryrec = request.env['res.country'].sudo().search([('id', '=', country)], order="code asc")


                # ToDo: try to render the countries from the js
                countries_scl = request.env['res.country'].sudo().search(
                    ['|', ('name', '=', 'India'), ('name', '=', 'Nepal')], order="code asc")
                countries = request.env['res.country'].sudo().search(
                    ['|', '|', '|', ('name', '=', 'Sri Lanka'), ('name', '=', 'Bangladesh'), ('name', '=', 'Pakistan'),
                     ('name', '=', 'Afghanistan')], order="code asc")
                result_countries = countries_scl | countries
                values = {
                    'object': recdyn,
                    'countries': result_countries,
                    'submitted': post.get('submitted', False),
                    'isoqs': qsurl,
                    'email': participantemail
                }
                if is_meditator == True and rec_id != None:
                    print('record found ', rec_id)
                    # print(res_rec)
                    # if('center_id' in res_rec and res_rec['center_id'] == None and 'center_manually_edited' in res_rec):
                    #     #if( res_rec.center_manually_edited == None):
                    #     #res_rec.sudo().write({'center_manually_edited': False})
                    #     goldenContact = isha_crm_importer.isha_crm.GoldenContactAdv.GoldenContactAdv()
                    #     goldenContact.setEnvValues(request.env)
                    #     center_dict = goldenContact.getCenterId(city, pincode, countryrec.code,
                    #                                             res_rec.center_manually_edited)
                    #     print('center calculated')
                    #     if(res_rec.center_manually_edited == False and 'center_id' in center_dict and  center_dict['center_id'] != None and 'region_name' in center_dict and  center_dict['region_name'] != None):
                    #         print('center found', center_dict['center_id'])
                    #         res_rec.sudo().write(
                    #             {'center_id': center_dict['center_id'], 'region_name': center_dict['region_name'],
                    #              'is_valid_zip': center_dict['is_valid_zip']})
                    # update respartner record
                    staterec = request.env['res.country.state'].sudo().search([('id', '=', state)])
                    rec_dict = {
                        'name': name,
                        'phone_country_code': countrycode,
                        'phone': phone,
                        'email': participantemail,
                        'city': city,
                        'state_id': state,
                        'state': staterec.name,
                        'zip': pincode,
                        'country_id': country,
                        'country': countryrec.code,
                        'whatsapp_country_code': wtsappcountrycode,
                        'whatsapp_number': wtsappphone,
                        'sso_id': profileid
                    }

                    # request.env['satsang.tempdebuglog'].sudo().create({
                    #     'logtext': 'meditator flag true and record found'
                    # })
                    # request.env['satsang.tempdebuglog'].sudo().create({
                    #     'logtext': rec_id
                    # })
                    # request.env['satsang.tempdebuglog'].sudo().create({
                    #     'logtext': rec_id#res_rec
                    # })
                    #
                    # request.env['satsang.tempdebuglog'].sudo().create({
                    #     'logtext': 'meditator flag true and record updated'
                    # })
                    # request.env['satsang.tempdebuglog'].sudo().create({
                    #     'logtext': rec
                    # })
                    pgm_schedule_info = {
                        'teacher_id': None,
                        'teacher_name': None,
                        'center_id': None,
                        'center_name': None,
                        'location': None,
                        'country': None,
                        'remarks': None,
                        'schedule_name': prgapplied.pgmschedule_programname,
                        'start_date': prgapplied.pgmschedule_startdate,
                        'end_date': prgapplied.pgmschedule_enddate,
                        'program_schedule_guid': None,
                        'localScheduleId': None,
                        'pii': rec_dict
                    }
                    request.env['program.lead'].sudo().create({
                        'program_schedule_info': pgm_schedule_info,
                        'program_name': prgapplied.pgmschedule_programtype.program_type,
                        'start_date': prgapplied.pgmschedule_startdate,
                        'reg_status': 'CONFIRMED',
                        'reg_date_time': datetime.datetime.now(),
                        'active': True,
                        'utm_medium': utm_medium,
                        'utm_campaign': utm_campaign,
                        'is_ie_done': True,
                        'record_name': name,
                        'record_phone': phone,
                        'record_email': participantemail,
                        'record_city': city,
                        'record_state': staterec.name,
                        'record_zip': pincode,
                        'record_country': countryrec.code,
                        'trans_lang': prgapplied.pgmschedule_language,
                        'pgm_type_master_id': prgapplied.pgmschedule_programtype.id,
                        'local_program_type_id': returneventprogramname(event_type),
                        'contact_id_fkey': 2,  # res_rec.id,
                        'pgm_remark': 'First timer',
                        'sso_id': profileid
                    })
                    # if event_type == "shoonya":
                    #     sso_set_entitlement("evergreen_shoonya_2020", profileid)
                    # elif event_type == "shambhavi":
                    #     sso_set_entitlement("evergreen_satsang_2020", profileid)
                    # elif event_type == "ieo":
                    #     sso_set_entitlement("evergreen_ieo_2020", profileid)
                    # elif event_type == "samyama":
                    #     sso_set_entitlement("evergreen_samyama_2020", profileid)
                    evententtitlmenttobeset = returnevententtitlement(event_type)
                    if event_type != "anandaalai":
                        sso_set_entitlement(evententtitlmenttobeset, profileid)
                    # if eventtype=='pournami':
                    #     transprogramlead_rec = request.env['transprogramlead'].sudo().create({
                    #         'record_name': name,
                    #         'record_phone': phone,
                    #         'record_email': participantemail,
                    #         'trans_lang': language_preferred,
                    #         'program_name': event_program_type
                    #     })
                    # else:
                    transprogramlead_rec = request.env['transprogramlead'].sudo().create({
                        'record_name': name,
                        'record_phone': phone,
                        'record_email': participantemail,
                        'trans_lang': prgapplied.pgmschedule_language,
                        'program_name': event_program_type
                    })
                    # request.env['satsang.tempdebuglog'].sudo().create({
                    #     'logtext': 'before send email'
                    # })
                    sendemailandsms(transprogramlead_rec)
                    # request.env['satsang.tempdebuglog'].sudo().create({
                    #     'logtext': 'after send email'
                    # })
                    #
                    # request.env['satsang.tempdebuglog'].sudo().create({
                    #     'logtext': 'update record in long form'
                    # })
                    # request.env['satsang.tempdebuglog'].sudo().create({
                    #     'logtext': rec
                    # })
                    # request.env['satsang.tempdebuglog'].sudo().create({
                    #     'logtext': "longform post qsurl" + str(qsurl)
                    # })
                    print('program lead confirmed record created')
                    values = {'object': name, 'language': prgapplied.pgmschedule_language, 'isoqs': qsurl}
                    cbm = cbm + "?" + "utm_source=" + utm_source + "utm_medium=" + utm_medium + "utm_campaign=" + utm_campaign
                    return werkzeug.utils.redirect(cbm)
                    # return request.render("isha_satsang.registrationsuccess2", values)
                if (is_meditator == False and event_type == "shambhavi"):
                    print('not meditator redirecting to alternate form')
                    if (language == 'english'):
                        return request.render('isha_satsang.onlinesatsangregnalternate', values)
                    elif (language == 'tamil'):
                        return request.render('isha_satsang.onlinesatsangregnalternatetamil', values)
                    elif (language == 'hindi'):
                        return request.render('isha_satsang.onlinesatsangregnalternatehindi', values)
                    elif (language == 'telugu'):
                        return request.render('isha_satsang.onlinesatsangregnalternatetelugu', values)
                    elif (language == 'kannada'):
                        return request.render('isha_satsang.onlinesatsangregnalternatekannada', values)
                    else:
                        return request.render('isha_satsang.onlinesatsangregnalternate', values)
                if (is_meditator == False and event_type != "shambhavi"):
                    staterec = request.env['res.country.state'].sudo().search([('id', '=', state)])
                    respartrec_dict = {
                        'name': name,
                        'phone_country_code': countrycode,
                        'phone': phone,
                        'email': participantemail,
                        'city': city,
                        'state_id': state,
                        'state': staterec.name,
                        'zip': pincode,
                        'country_id': country,
                        'country': countryrec.code,
                        'whatsapp_country_code': wtsappcountrycode,
                        'whatsapp_number': wtsappphone,
                        'sso_id': profileid,
                        'is_meditator': is_meditator
                    }
                    pgm_schedule_info = {
                        'teacher_id': None,
                        'teacher_name': None,
                        'center_id': None,
                        'center_name': None,
                        'location': None,
                        'country': None,
                        'remarks': None,
                        'schedule_name': prgapplied.pgmschedule_programname,
                        'start_date': prgapplied.pgmschedule_startdate,
                        'end_date': prgapplied.pgmschedule_enddate,
                        'program_schedule_guid': None,
                        'localScheduleId': None,
                        'pii': respartrec_dict
                    }
                    prgleadcreatrec = request.env['program.lead'].sudo().create({
                        'program_schedule_info': pgm_schedule_info,
                        'program_name': prgapplied.pgmschedule_programtype.program_type,
                        'start_date': prgapplied.pgmschedule_startdate,
                        'reg_status': 'PENDING',
                        'reg_date_time': datetime.datetime.now(),
                        'active': True,
                        'utm_medium': utm_medium,
                        'utm_campaign': utm_campaign,
                        # 'is_ie_done': False,
                        'record_name': name,
                        'record_phone': phone,
                        'record_email': participantemail,
                        'record_city': city,
                        'record_state': staterec.name,
                        'record_zip': pincode,
                        'record_country': countryrec.code,
                        'trans_lang': prgapplied.pgmschedule_language,
                        'pgm_type_master_id': prgapplied.pgmschedule_programtype.id,
                        'local_program_type_id': returneventprogramname(event_type),
                        'contact_id_fkey': 2,  # respartrec.id,
                        'pgm_remark': 'First timer',
                        'sso_id': profileid
                    })
                    if event_type == 'anandaalai':
                        prgleadcreatrec.bay_name = prgapplied.pgmschedule_time
                        return request.render('isha_satsang.anandaalairegistrationfailure')
                    values = {'object': name, 'language': prgapplied.pgmschedule_language}
                    cbnm = cbnm + "?" + "utm_source=" + utm_source + "utm_medium=" + utm_medium + "utm_campaign=" + utm_campaign
                    return werkzeug.utils.redirect(cbnm)

            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
                request.env['satsang.tempdebuglog'].sudo().create({
                    'logtext': ustr(exc)
                })
            except Exception as ex:
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                _logger.error(tb_ex)
                stacktrace = traceback.print_exc()
                # request.env['satsang.tempdebuglog'].sudo().create({
                #     'logtext': ex
                # })
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)

            if validation_errors:
                values['validation_errors'] = validation_errors
                _logger.log(25, validation_errors)
                logtext = 'Date:' + datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S") + ': ' + ' '.join(
                    [str(elem) for elem in validation_errors])
                # request.env['satsang.tempdebuglog'].sudo().create({
                #     'logtext': logtext
                # })
                values = {'language': language}
                if event_type == 'pournami':
                    tmpurl = request.httprequest.url.replace('registrationform', 'registrationform/failure')
                    return werkzeug.utils.redirect(tmpurl + "&tzvalue=" + str(language))
                else:
                    return request.render('isha_satsang.exception', values)

    def _anandaalai_alternate_form(self, language="", cbm="", cbnm="", utm_source="", utm_medium="",
                                       utm_campaign="", eventtype="shambhavi", **post):
        try:
            _logger.info('Post Data ' +str(post))
            profileid = post.get('jsgstngfmprofileid')
            programapplied = post.get('jsgstngfmprogramapplied')
            name = post.get('jsgstngfmname')
            fname= post.get('jsgstngfmfname')
            lname=post.get('jsgstngfmlname')
            jsgstngfmcountrycode = post.get('jsgstngfmcountrycode')
            jsgstngfmphone = post.get('jsgstngfmpage1phone')
            jsgstngfmwtsapyesno = post.get('jsgstngfmwtsapyesno')
            jsgstngfmwtsapcountrycode = post.get('jsgstngfmwtsapcountrycode')
            sgstngfmwtsapphone = post.get('sgstngfmwtsapphone')
            jsgstngfmemail = post.get('jsgstngfmemail')
            jsgstngfmcountry = post.get('jsgstngfmcountry')
            jsgstngfmpincode = post.get('jsgstngfmpincode')
            jsgstngfmcity = post.get('jsgstngfmcity')
            jsgstngfmstate = post.get('jsgstngfmstate')
            jsgstngfmpreflang = post.get('jsgstngfmpreflang')
            alternatephonecode = post.get('jsgstngfmaltcountrycode')
            # alternatephone = post.get('jsgstngfmalternatephone')
            alternateemail = post.get('jsgstngfmparticipantemail')
            isishaprg = True
            iemonth = post.get('jsgstngfmidmonth')
            ieyear = post.get('jsgstngfmieyear')
            iecenter = post.get('jsgstngfmiecenter')

            # if (checkIsNullOrEmpyt(alternatephone)):
            alternatephone = jsgstngfmphone
            if (checkIsNullOrEmpyt(alternateemail)):
                alternateemail = post.get('jsgstngfmparticipantemail')

            prgapplied = request.env['satsang.schedule'].sudo().search([('id', '=', programapplied)])
            res = get_duplicaterecord(name, alternatephone, alternateemail, prgapplied.pgmschedule_startdate,
                                    prgapplied.pgmschedule_language, "", profileid)
            alreadyregprgdate = prgapplied.pgmschedule_startdate.strftime('%d-%m-%Y')
            if (res == True):
                values = {'object': name, 'date': alreadyregprgdate, 'language': language}
                return request.render("isha_satsang.anandaalaialreadyregistered_m2", values)

            # resofismed = get_is_meditator_or_ieo(name, alternatephone, alternateemail, True, False, False, False, profileid)
            #
            # is_meditator = resofismed['is_meditator']
            # rec_id = resofismed['matched_id']
            # res_rec = resofismed['matched_rec']


            is_meditator = global_api_call(fname, lname, alternateemail, alternatephone, profileid)
            _logger.info('Global API Call ' + str({'first_name': fname, 'last_name': lname,
                                                   'participantemail': alternateemail,
                                                   'phone': alternatephone, 'profileid': profileid})
                         + ' output ' +
                         str({"is_meditator": is_meditator}))
            resofismed = {
                'ishangam_id': 2,
                'is_meditator': is_meditator,
                'ieo_completed': False,
                'matched_id': 2,
            }
            rec_id = resofismed['ishangam_id']

            print('record found', rec_id)
            if (jsgstngfmstate):
                staterec = request.env['res.country.state'].sudo().search([('id', '=', jsgstngfmstate)])
            else:
                staterec= ""
            countryrec = request.env['res.country'].sudo().search([('id', '=', jsgstngfmcountry)])
            reg_status = 'PENDING'

            if (is_meditator == True):
                reg_status = 'CONFIRMED'
                evententtitlmenttobeset = returnevententtitlement(eventtype)

            res_rec_dict = {
                'name': name,
                'phone_country_code': jsgstngfmcountrycode,
                'phone': jsgstngfmphone,
                'phone2_country_code': alternatephonecode,
                'phone2': alternatephone,
                'email': jsgstngfmemail,
                'email2': alternateemail,
                'city': jsgstngfmcity,
                'state_id': jsgstngfmstate if jsgstngfmstate else 0,
                'state': staterec.name if staterec else "",
                'zip': jsgstngfmpincode,
                'country_id': jsgstngfmcountry,
                'country': countryrec.code,
                'whatsapp_country_code': jsgstngfmwtsapcountrycode,
                'whatsapp_number': sgstngfmwtsapphone,
                'sso_id': profileid,
                'is_meditator': is_meditator
            }

            pgm_schedule_info = {
                'teacher_id': None,
                'teacher_name': None,
                'center_id': None,
                'center_name': None,
                'location': None,
                'country': None,
                'remarks': None,
                'schedule_name': prgapplied.pgmschedule_programname,
                'start_date': prgapplied.pgmschedule_startdate,
                'end_date': prgapplied.pgmschedule_enddate,
                'program_schedule_guid': None,
                'localScheduleId': None,
                'pii': res_rec_dict
            }

            rec = request.env['program.lead'].sudo().create({
                'program_schedule_info': pgm_schedule_info,
                'program_name': prgapplied.pgmschedule_programtype.program_type,
                'start_date': prgapplied.pgmschedule_startdate,
                'reg_status': reg_status,
                'reg_date_time': datetime.datetime.now(),
                'active': True,
                'utm_medium': utm_medium,
                'utm_campaign': utm_campaign,
                'is_ie_done': isishaprg,
                'record_name': name,
                'record_phone': jsgstngfmphone,
                'record_email': jsgstngfmemail,
                'record_city': jsgstngfmcity,
                'record_state': staterec.name if staterec else "",
                'record_zip': jsgstngfmpincode,
                'record_country': countryrec.code,
                'trans_lang': jsgstngfmpreflang,
                'bay_name': prgapplied.pgmschedule_time,
                'pgm_type_master_id': prgapplied.pgmschedule_programtype.id,
                'contact_id_fkey': 2,  # res_rec.id,
                'pgm_remark': 'First timer',
                'sso_id': profileid
            })

            if is_meditator == True:
                transprogramlead_rec = request.env['transprogramlead'].sudo().create({
                    'record_name': name,
                    'record_phone': jsgstngfmphone,
                    'record_email': jsgstngfmemail,
                    'trans_lang': prgapplied.pgmschedule_language,
                    'program_name': 'Online Ananda Alai',  # currently alternate form is only being used for online satsang
                    'tzlangregion': prgapplied.pgmschedule_time
                })
                sendemailandsms(transprogramlead_rec)
                values = {'object': name, 'language': prgapplied.pgmschedule_language}
                return request.render('isha_satsang.anandaalairegistrationsuccess_m2', values)
            else:
                values = {'object': name, 'language': prgapplied.pgmschedule_language}
                return request.render('isha_satsang.anandaalairegistrationfailure_m2', values)
        except Exception as ex:
            tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
            _logger.error(tb_ex)
            values = {'language': language}
            return request.render('isha_satsang.exception', values)

    @http.route('/onlinesatsang/alternateregistrationform', type='http', auth="public", methods=["GET", "POST"],
                website=True, csrf=False)
    def onlinesatsangregnalternateform(self, language="", cbm="", cbnm="", utm_source="", utm_medium="",
                                       utm_campaign="", eventtype="shambhavi", **post):

        if request.httprequest.method == "POST":
            if (eventtype=="anandaalai"):
                return self._anandaalai_alternate_form(language, cbm, cbnm, utm_source, utm_medium, utm_campaign, eventtype, **post)

            validation_errors = []
            values = {}
            try:
                # qsurl = str(request.httprequest.url).split('?')[1]
                # request.env['satsang.tempdebuglog'].sudo().create({
                #     'logtext': 'alternate form cbm ' + cbm
                # })
                # request.env['satsang.tempdebuglog'].sudo().create({
                #     'logtext': 'alternate form httprequest url ' + str(request.httprequest.url)
                # })
                profileid = post.get('jsgstngfmprofileid')
                programapplied = post.get('jsgstngfmprogramapplied')
                name = post.get('jsgstngfmname')
                jsgstngfmcountrycode = post.get('jsgstngfmcountrycode')
                jsgstngfmphone = post.get('jsgstngfmpage1phone')
                jsgstngfmwtsapyesno = post.get('jsgstngfmwtsapyesno')
                jsgstngfmwtsapcountrycode = post.get('jsgstngfmwtsapcountrycode')
                sgstngfmwtsapphone = post.get('sgstngfmwtsapphone')
                jsgstngfmemail = post.get('jsgstngfmemail')
                jsgstngfmcountry = post.get('jsgstngfmcountry')
                jsgstngfmpincode = post.get('jsgstngfmpincode')
                jsgstngfmcity = post.get('jsgstngfmcity')
                jsgstngfmstate = post.get('jsgstngfmstate')
                jsgstngfmpreflang = post.get('jsgstngfmpreflang')
                alternatephonecode = post.get('jsgstngfmaltcountrycode')
                alternatephone = post.get('jsgstngfmalternatephone')
                alternateemail = post.get('jsgstngfmparticipantemail')
                isishaprg = post.get('jsgstngfmanyishabool')
                iemonth = post.get('jsgstngfmidmonth')
                ieyear = post.get('jsgstngfmieyear')
                iecenter = post.get('jsgstngfmiecenter')

                print('calling to find if meditator with alternate mail and phone')
                print('calling to find if meditator with alternate mail and phone')
                if (checkIsNullOrEmpyt(alternatephone)):
                    alternatephone = jsgstngfmphone
                if (checkIsNullOrEmpyt(alternateemail)):
                    alternateemail = jsgstngfmemail

                prgapplied = request.env['satsang.schedule'].sudo().search([('id', '=', programapplied)])
                res = get_duplicaterecord(name, alternatephone, alternateemail, prgapplied.pgmschedule_startdate,
                                          prgapplied.pgmschedule_language, "", profileid)
                alreadyregprgdate = prgapplied.pgmschedule_startdate.strftime('%d-%m-%Y')
                if (res == True):
                    values = {'object': name, 'date': alreadyregprgdate, 'language': language}
                    return request.render("isha_satsang.alreadyregistered", values)

                # request.env['satsang.tempdebuglog'].sudo().create({
                #     'logtext': 'finding duplicate record complete'
                # })

                resofismed = get_is_meditator_or_ieo(name, alternatephone, alternateemail, True, False, False, False, profileid)
                is_meditator = resofismed['is_meditator']
                rec_id = resofismed['matched_id']
                res_rec = resofismed['matched_rec']
                print('record found', rec_id)

                staterec = request.env['res.country.state'].sudo().search([('id', '=', jsgstngfmstate)])
                countryrec = request.env['res.country'].sudo().search([('id', '=', jsgstngfmcountry)])
                reg_status = 'PENDING'

                if (is_meditator == True):
                    if (is_meditator == True):
                        print('alternate form meditator flag is true')
                        reg_status = 'CONFIRMED'
                        # request.env['satsang.tempdebuglog'].sudo().create({
                        #     'logtext': 'confirmed with alternate email and phone'
                        # })
                        # request.env['satsang.tempdebuglog'].sudo().create({
                        #     'logtext': jsgstngfmemail
                        # })
                        evententtitlmenttobeset = returnevententtitlement(eventtype)
                        if eventtype != "anandaalai":
                            sso_set_entitlement(evententtitlmenttobeset, profileid)  # "evergreen_satsang_2020"

                    # request.env['satsang.tempdebuglog'].sudo().create({
                    #     'logtext': reg_status
                    # })
                    print('registration status', reg_status)

                res_rec_dict = {
                    'name': name,
                    'phone_country_code': jsgstngfmcountrycode,
                    'phone': jsgstngfmphone,
                    'phone2_country_code': alternatephonecode,
                    'phone2': alternatephone,
                    'email': jsgstngfmemail,
                    'email2': alternateemail,
                    'city': jsgstngfmcity,
                    'state_id': jsgstngfmstate,
                    'state': staterec.name,
                    'zip': jsgstngfmpincode,
                    'country_id': jsgstngfmcountry,
                    'country': countryrec.code,
                    'whatsapp_country_code': jsgstngfmwtsapcountrycode,
                    'whatsapp_number': sgstngfmwtsapphone,
                    'sso_id': profileid,
                    'is_meditator': is_meditator
                }

                pgm_schedule_info = {
                    'teacher_id': None,
                    'teacher_name': None,
                    'center_id': None,
                    'center_name': None,
                    'location': None,
                    'country': None,
                    'remarks': None,
                    'schedule_name': prgapplied.pgmschedule_programname,
                    'start_date': prgapplied.pgmschedule_startdate,
                    'end_date': prgapplied.pgmschedule_enddate,
                    'program_schedule_guid': None,
                    'localScheduleId': None,
                    'pii': res_rec_dict
                }

                # if ('center_id' in res_rec and res_rec['center_id'] !=None and 'center_manually_edited' in res_rec):
                #     #if (res_rec.center_manually_edited == None):
                #     #res_rec.sudo().write({'center_manually_edited': False})
                #     goldenContact = isha_crm_importer.isha_crm.GoldenContactAdv.GoldenContactAdv()
                #     goldenContact.setEnvValues(request.env)
                #     center_dict = goldenContact.getCenterId(jsgstngfmcity, jsgstngfmpincode, countryrec.code,
                #                                             res_rec.center_manually_edited)
                #     print('center is calculated')
                #     if (res_rec.center_manually_edited == False and 'center_id' in center_dict and center_dict['center_id'] != None and 'region_name' in center_dict and
                #             center_dict['region_name'] != None):
                #         print('center found', center_dict['center_id'])
                #         res_rec.sudo().write(
                #             {'center_id': center_dict['center_id'], 'region_name': center_dict['region_name'],
                #              'is_valid_zip': center_dict['is_valid_zip']})

                # if (res_rec.sso_id == False):
                #     request.env['satsang.tempdebuglog'].sudo().create({
                #         'logtext': 'Update SSO'
                #     })
                #     res_rec.sudo().write({'sso_id': profileid})

                rec = request.env['program.lead'].sudo().create({
                    'program_schedule_info': pgm_schedule_info,
                    'program_name': prgapplied.pgmschedule_programtype.program_type,
                    'start_date': prgapplied.pgmschedule_startdate,
                    'reg_status': reg_status,
                    'reg_date_time': datetime.datetime.now(),
                    'active': True,
                    'utm_medium': utm_medium,
                    'utm_campaign': utm_campaign,
                    'is_ie_done': isishaprg,
                    'record_name': name,
                    'record_phone': jsgstngfmphone,
                    'record_email': jsgstngfmemail,
                    'record_city': jsgstngfmcity,
                    'record_state': staterec.name,
                    'record_zip': jsgstngfmpincode,
                    'record_country': countryrec.code,
                    'trans_lang': jsgstngfmpreflang,
                    'pgm_type_master_id': prgapplied.pgmschedule_programtype.id,
                    'contact_id_fkey': 2,  # res_rec.id,
                    'pgm_remark': 'First timer',
                    'sso_id': profileid
                })

                # request.env['satsang.tempdebuglog'].sudo().create({
                #     'logtext': 'update record in alternate'
                # })
                # request.env['satsang.tempdebuglog'].sudo().create({
                #     'logtext': rec
                # })
                if is_meditator == True:
                    # values = {'object': name, 'language': prgapplied.pgmschedule_language}
                    # values['validation_errors'] = validation_errors
                    transprogramlead_rec = request.env['transprogramlead'].sudo().create({
                        'record_name': name,
                        'record_phone': jsgstngfmphone,
                        'record_email': jsgstngfmemail,
                        'trans_lang': prgapplied.pgmschedule_language,
                        'program_name': 'Online Satsang'  # currently alternate form is only being used for online satsang
                    })
                    sendemailandsms(transprogramlead_rec)
                    cbm = cbm + "?" + "utm_source=" + utm_source + "utm_medium=" + utm_medium + "utm_campaign=" + utm_campaign
                    return werkzeug.utils.redirect(cbm)
                    # return request.render("isha_satsang.registrationsuccess2", values)
                else:
                    values = {'object': name, 'language': prgapplied.pgmschedule_language}
                    cbnm = cbnm + "?" + "utm_source=" + utm_source + "utm_medium=" + utm_medium + "utm_campaign=" + utm_campaign
                    return werkzeug.utils.redirect(cbnm)
                    # return request.render("isha_satsang.registrationsuccess1", values)
            except Exception as ex:
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                _logger.error(tb_ex)
                # request.env['satsang.tempdebuglog'].sudo().create({
                #     'logtext': "error inside alternate form controller "
                # })
                # request.env['satsang.tempdebuglog'].sudo().create({
                #     'logtext': tb_ex
                # })
                values = {'language': language}
                return request.render('isha_satsang.exception', values)

    @http.route('/onlinesatsang/registration', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def onlinesatsangregistration(self, **post):
        validation_errors = []
        values = {}
        try:
            ret = "c869c4242d9b55a91d13fdf58cab1377"
            sso_log = {'request_url': "https://registrations.sushumna.isha.in/onlinesatsang/registration",
                       "callback_url": "https://registrations.sushumna.isha.in/onlinesatsang/registrationform",
                       "api_key": "31d9c883155816d15f6f3a74dd79961b0577670ac",
                       "hash_value": "",
                       "action": "0",
                       "legal_entity": "IF",
                       "force_consent": "1"
                       }
            ret_list = {'request_url': "https://registrations.sushumna.isha.in/onlinesatsang/registration",
                        "callback_url": "https://registrations.sushumna.isha.in/onlinesatsang/registrationform",
                        "api_key": "31d9c883155816d15f6f3a74dd79961b0577670ac"
                        }
            data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
            data_enc = data_enc.replace("/", "\\/")
            signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'), digestmod=hashlib.sha256).hexdigest()
            sso_log["hash_value"] = signature
            values = {
                'sso_log': sso_log
            }
            return request.render('isha_satsang.onlinesatsangsso', values)
        except Exception as ex:
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': ex
            # })
            tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
            _logger.error(tb_ex)
            _logger.error(
                "Error caught during request creation", exc_info=True)
            validation_errors.append("Unknown server error. See server logs.")
            validation_errors.append(ex)

        if not validation_errors:
            values = {'object': 'Participant', 'language': 'English'}
            values['validation_errors'] = validation_errors
            return request.render("isha_satsang.registrationsuccess2", values)
        else:
            _logger.log(25, validation_errors)
            logtext = 'Date:' + datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S") + ': ' + ' '.join(
                [str(elem) for elem in validation_errors])
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': logtext
            # })
            values = {'language': 'English'}
            return request.render('isha_satsang.exception', values)

    @http.route('/onlinesatsang/sso', type='http', methods=['POST'], auth='none', csrf=False)
    def sso_auth_signin(self, **kw):
        try:
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': "inside onlinesatsang/sso"
            # })
            qcontext = request.params.copy()
            ret = "c869c4242d9b55a91d13fdf58cab1377"
            api_key = "31d9c883155816d15f6f3a74dd79961b0577670ac"
            session_id = request.httprequest.form['session_id']
            hash_val = hash_hmac({'session_id': session_id}, ret)
            data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
            request_url = "https://uat-sso.isha.in/getlogininfo"
            sso_user_profile = eval(sso_get_user_profile(request_url, data).text)
            # SSO_LOGIN_INFO_ENDPOINT
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': str(sso_user_profile)
            # })
            sso_user_profile['autologin_profile_id']
            return werkzeug.utils.redirect(
                '/onlinesatsang/registrationform?s=' + sso_user_profile['autologin_profile_id']);
        except Exception as ex:
            # signup error
            _logger.exception("sso_auth: %s" % str(ex))
            tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
            _logger.error(tb_ex)
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': "sso_auth:"
            # })
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': tb_ex
            # })

    @http.route('/onlinesatsang/shortregistrationform', type='http', methods=['POST'], auth='none', csrf=False)
    def onlinesatsangshortregistrationform(self, language="", cbm="", cbnm="", utm_source="", utm_medium="",
                                           utm_campaign="", eventtype="shambhavi", **post):
        validation_errors = []
        values = {}
        # language = 'English'
        try:
            satsangprogramname = returneventprogramname(eventtype)
            print('short form')
            profileid = post.get('jsgstngfmprofileid')
            first_name = post.get('jsgstngfmfirstname')
            last_name = post.get('jsgstngfmlastname')
            participantemail = post.get('jsgstngfmparticipantemail')
            phone = post.get('jsgstngfmshortphone')
            countrycode = post.get('jsgstngfmcountryshortcode')
            prgappliedId = post.get('jsgstngfmpreflang')
            prgapplied = request.env['satsang.schedule'].sudo().search([('id', '=', prgappliedId)])

            name = first_name + ' ' + last_name
            res = get_duplicaterecord(name, phone, participantemail, prgapplied.pgmschedule_startdate,
                                      prgapplied.pgmschedule_language, "",profileid)

            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': 'after duplicate method call res:' + str(res)
            # })
            alreadyregprgdate = prgapplied.pgmschedule_startdate.strftime('%d-%m-%Y')
            if (res == True):
                # request.env['satsang.tempdebuglog'].sudo().create({
                #     'logtext': 'result of duplicate record '+ str(res)
                # })
                values = {'object': name, 'date': alreadyregprgdate, 'language': language}
                return request.render("isha_satsang.alreadyregistered", values)

            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': 'finding duplicate record complete'
            # })
            #
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': 'create res partner '
            # })
            #
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': name
            # })
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext':  participantemail
            # })
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext':  profileid
            # })

            res_rec_dict = {
                'name': name,
                'email': participantemail,
                'sso_id': profileid,
                'phone': phone,
                'phone_country_code': countrycode
            }

            pgm_schedule_info = {
                'teacher_id': None,
                'teacher_name': None,
                'center_id': None,
                'center_name': None,
                'location': None,
                'country': None,
                'remarks': None,
                'schedule_name': prgapplied.pgmschedule_programname,
                'start_date': prgapplied.pgmschedule_startdate,
                'end_date': prgapplied.pgmschedule_enddate,
                'program_schedule_guid': None,
                'localScheduleId': None,
                'pii': res_rec_dict
            }

            request.env['program.lead'].sudo().create({
                'program_schedule_info': pgm_schedule_info,
                'program_name': prgapplied.pgmschedule_programtype.program_type,
                'start_date': prgapplied.pgmschedule_startdate,
                'reg_status': 'CONFIRMED',
                'reg_date_time': datetime.datetime.now(),
                'active': True,
                'utm_medium': utm_medium,
                'utm_campaign': utm_campaign,
                'is_ie_done': True,
                'record_name': name,
                'record_email': participantemail,
                # 'record_city': res_rec.city,
                # 'record_state': res_rec.state,
                # 'record_zip': res_rec.zip,
                # 'record_country': res_rec.country,
                'trans_lang': prgapplied.pgmschedule_language,
                'pgm_type_master_id': prgapplied.pgmschedule_programtype.id,
                'contact_id_fkey': 2,  # res_rec.id,
                'pgm_remark': 'RSVP',
                'sso_id': profileid
            })
            transprogramlead_rec = request.env['transprogramlead'].sudo().create({
                'record_name': name,
                'record_phone': phone,
                'record_email': participantemail,
                'trans_lang': prgapplied.pgmschedule_language,
                'program_name': satsangprogramname
            })
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': 'before send email'
            # })
            sendemailandsms(transprogramlead_rec)
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': 'after send email'
            # })
        except Exception as ex:
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': "error inside short form controller "
            # })
            tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
            _logger.error(tb_ex)
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': tb_ex
            # })
            # if (language == ""):
            #    language = "English"
            values = {'language': language}
            return request.render('isha_satsang.exception', values)

        if not validation_errors:
            values = {'object': name, 'language': prgapplied.pgmschedule_language}
            values['validation_errors'] = validation_errors
            cbm = cbm + "?" + "utm_source=" + utm_source + "utm_medium=" + utm_medium + "utm_campaign=" + utm_campaign
            return werkzeug.utils.redirect(cbm)
            # return request.render("isha_satsang.registrationsuccess2", values)
        else:
            _logger.log(25, validation_errors)
            logtext = 'Date:' + datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S") + ': ' + ' '.join(
                [str(elem) for elem in validation_errors])
            # request.env['satsang.tempdebuglog'].sudo().create({
            #     'logtext': logtext
            # })
            values = {'language': language}
            return request.render('isha_satsang.exception', values)

    def onlinestngkeyvalueinfo(self, cntrycode="", keyvalue=""):

        funcresult = None

        try:
            res_countryinfo = request.env['res.country'].sudo().search(
                [("code", "=", cntrycode)])

            if res_countryinfo:
                region_name = res_countryinfo.center_id.region_id.name

                kv_info = request.env['satsang.key.value.config'].sudo().search(
                    [('key', '=', keyvalue)])
                if kv_info:
                    for rec in kv_info:
                        # print('inside loop1')
                        for rgn in rec.regionname:
                            # print(rgn.name)
                            # print(region_name)
                            if (rgn.name == region_name):
                                funcresult = rec.value
                                # print(funcresult)
                                break
            return funcresult

        except Exception as ex:
            tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
            _logger.error(tb_ex)
            return None
