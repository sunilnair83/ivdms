function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function () {
            defer(method)
        }, 50);
    }
}
defer(document_ready);
function document_ready() {
    $(document).ready(function () {
        $('#phone_cc').change(function () { set_country_phone_code_rule('#phone','#phone_cc'); });
        $('#phone2_cc').change(function () { set_country_phone_code_rule('#phone2','#phone2_cc'); });
		$('#country_of_residence').change(function () {set_country_pin_code_rule(); });

		set_country_phone_code_rule('#phone','#phone_cc');
		set_country_phone_code_rule('#phone2','#phone2_cc');
		set_country_pin_code_rule();
	});
}
function validate_form(){
	  if(validateemptystring("#first_name")){
		 alert('First Name is mandatory');
		 return false;
	 }
	 if(validateemptystring("#last_name")){
		 alert('Last Name is Mandatory');
		 return false;
	 }
	 if(validateemptystring("#phone")){
		 alert('Phone is Mandatory');
		 return false;
	 }
	 if(validateemptystring("#email")){
		 alert('Email is Mandatory');
		 return false;
	 }
	 if(validateemptystring("#country_of_residence")){
		 alert('Country Of Residence is Mandatory');
		 return false;
	 }
	 if(validateemptystring("#pincode")){
		 alert('Pincode is Mandatory');
		 return false;
	 }
 return true;
}

function validateemptystring(element){
	return $.trim($(element).val()) == "";
}
function set_country_phone_code_rule(phone_elem,cc_elem){

     if( $(cc_elem).val() == '91'){
        $(phone_elem).attr('maxlength', '10')
        $(phone_elem).attr('minlength', '10')
        $(phone_elem).attr('pattern', '[0-9]+')
    }
    else{
        $(phone_elem).attr('maxlength', '15')
        $(phone_elem).attr('pattern', '[1-9]{1}[0-9]+')
    }
}
function set_country_pin_code_rule(){

  if(  $('#country_of_residence').val() == '104' ){
        $('#pincode').attr('maxlength', '6');
        $('#pincode').attr('minlength', '6');
        $('#pincode').attr('pattern', '[1-9]{1}[0-9]+');
    }
    else if(  $('#country_of_residence').val() == '233' ){
        $('#pincode').attr('maxlength', '5');
        $('#pincode').attr('minlength', '5');
        $('#pincode').attr('pattern', '[1-9]{1}[0-9]+');
    }
    else{
    $('#pincode').attr('maxlength', '10');
    $('#pincode').removeAttr('pattern');
    $('#pincode').removeAttr('minlength');
    }
}