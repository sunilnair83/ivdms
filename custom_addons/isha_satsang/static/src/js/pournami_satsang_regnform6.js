var country_var = [{"id": 3, "name": "Afghanistan", "code": "AF", "phone_code": 93, "display_name": "Afghanistan"}, {"id": 6, "name": "Albania", "code": "AL", "phone_code": 355, "display_name": "Albania"}, {"id": 62, "name": "Algeria", "code": "DZ", "phone_code": 213, "display_name": "Algeria"}, {"id": 11, "name": "American Samoa", "code": "AS", "phone_code": 1684, "display_name": "American Samoa"}, {"id": 1, "name": "Andorra", "code": "AD", "phone_code": 376, "display_name": "Andorra"}, {"id": 8, "name": "Angola", "code": "AO", "phone_code": 244, "display_name": "Angola"}, {"id": 5, "name": "Anguilla", "code": "AI", "phone_code": 1264, "display_name": "Anguilla"}, {"id": 9, "name": "Antarctica", "code": "AQ", "phone_code": 672, "display_name": "Antarctica"}, {"id": 4, "name": "Antigua and Barbuda", "code": "AG", "phone_code": 1268, "display_name": "Antigua and Barbuda"}, {"id": 10, "name": "Argentina", "code": "AR", "phone_code": 54, "display_name": "Argentina"}, {"id": 7, "name": "Armenia", "code": "AM", "phone_code": 374, "display_name": "Armenia"}, {"id": 14, "name": "Aruba", "code": "AW", "phone_code": 297, "display_name": "Aruba"}, {"id": 13, "name": "Australia", "code": "AU", "phone_code": 61, "display_name": "Australia"}, {"id": 12, "name": "Austria", "code": "AT", "phone_code": 43, "display_name": "Austria"}, {"id": 16, "name": "Azerbaijan", "code": "AZ", "phone_code": 994, "display_name": "Azerbaijan"}, {"id": 32, "name": "Bahamas", "code": "BS", "phone_code": 1242, "display_name": "Bahamas"}, {"id": 23, "name": "Bahrain", "code": "BH", "phone_code": 973, "display_name": "Bahrain"}, {"id": 19, "name": "Bangladesh", "code": "BD", "phone_code": 880, "display_name": "Bangladesh"}, {"id": 18, "name": "Barbados", "code": "BB", "phone_code": 1246, "display_name": "Barbados"}, {"id": 36, "name": "Belarus", "code": "BY", "phone_code": 375, "display_name": "Belarus"}, {"id": 20, "name": "Belgium", "code": "BE", "phone_code": 32, "display_name": "Belgium"}, {"id": 37, "name": "Belize", "code": "BZ", "phone_code": 501, "display_name": "Belize"}, {"id": 25, "name": "Benin", "code": "BJ", "phone_code": 229, "display_name": "Benin"}, {"id": 27, "name": "Bermuda", "code": "BM", "phone_code": 1441, "display_name": "Bermuda"}, {"id": 33, "name": "Bhutan", "code": "BT", "phone_code": 975, "display_name": "Bhutan"}, {"id": 29, "name": "Bolivia", "code": "BO", "phone_code": 591, "display_name": "Bolivia"}, {"id": 30, "name": "Bonaire, Sint Eustatius and Saba", "code": "BQ", "phone_code": 599, "display_name": "Bonaire, Sint Eustatius and Saba"}, {"id": 17, "name": "Bosnia and Herzegovina", "code": "BA", "phone_code": 387, "display_name": "Bosnia and Herzegovina"}, {"id": 35, "name": "Botswana", "code": "BW", "phone_code": 267, "display_name": "Botswana"}, {"id": 34, "name": "Bouvet Island", "code": "BV", "phone_code": 55, "display_name": "Bouvet Island"}, {"id": 31, "name": "Brazil", "code": "BR", "phone_code": 55, "display_name": "Brazil"}, {"id": 105, "name": "British Indian Ocean Territory", "code": "IO", "phone_code": 246, "display_name": "British Indian Ocean Territory"}, {"id": 28, "name": "Brunei Darussalam", "code": "BN", "phone_code": 673, "display_name": "Brunei Darussalam"}, {"id": 22, "name": "Bulgaria", "code": "BG", "phone_code": 359, "display_name": "Bulgaria"}, {"id": 21, "name": "Burkina Faso", "code": "BF", "phone_code": 226, "display_name": "Burkina Faso"}, {"id": 24, "name": "Burundi", "code": "BI", "phone_code": 257, "display_name": "Burundi"}, {"id": 116, "name": "Cambodia", "code": "KH", "phone_code": 855, "display_name": "Cambodia"}, {"id": 47, "name": "Cameroon", "code": "CM", "phone_code": 237, "display_name": "Cameroon"}, {"id": 38, "name": "Canada", "code": "CA", "phone_code": 1, "display_name": "Canada"}, {"id": 52, "name": "Cape Verde", "code": "CV", "phone_code": 238, "display_name": "Cape Verde"}, {"id": 123, "name": "Cayman Islands", "code": "KY", "phone_code": 1345, "display_name": "Cayman Islands"}, {"id": 40, "name": "Central African Republic", "code": "CF", "phone_code": 236, "display_name": "Central African Republic"}, {"id": 214, "name": "Chad", "code": "TD", "phone_code": 235, "display_name": "Chad"}, {"id": 46, "name": "Chile", "code": "CL", "phone_code": 56, "display_name": "Chile"}, {"id": 48, "name": "China", "code": "CN", "phone_code": 86, "display_name": "China"}, {"id": 54, "name": "Christmas Island", "code": "CX", "phone_code": 61, "display_name": "Christmas Island"}, {"id": 39, "name": "Cocos (Keeling) Islands", "code": "CC", "phone_code": 61, "display_name": "Cocos (Keeling) Islands"}, {"id": 49, "name": "Colombia", "code": "CO", "phone_code": 57, "display_name": "Colombia"}, {"id": 118, "name": "Comoros", "code": "KM", "phone_code": 269, "display_name": "Comoros"}, {"id": 42, "name": "Congo", "code": "CG", "phone_code": 243, "display_name": "Congo"}, {"id": 45, "name": "Cook Islands", "code": "CK", "phone_code": 682, "display_name": "Cook Islands"}, {"id": 50, "name": "Costa Rica", "code": "CR", "phone_code": 506, "display_name": "Costa Rica"}, {"id": 97, "name": "Croatia", "code": "HR", "phone_code": 385, "display_name": "Croatia"}, {"id": 51, "name": "Cuba", "code": "CU", "phone_code": 53, "display_name": "Cuba"}, {"id": 53, "name": "Cura\u00e7ao", "code": "CW", "phone_code": 599, "display_name": "Cura\u00e7ao"}, {"id": 55, "name": "Cyprus", "code": "CY", "phone_code": 357, "display_name": "Cyprus"}, {"id": 56, "name": "Czech Republic", "code": "CZ", "phone_code": 420, "display_name": "Czech Republic"}, {"id": 44, "name": "C\u00f4te d'Ivoire", "code": "CI", "phone_code": 225, "display_name": "C\u00f4te d'Ivoire"}, {"id": 41, "name": "Democratic Republic of the Congo", "code": "CD", "phone_code": 242, "display_name": "Democratic Republic of the Congo"}, {"id": 59, "name": "Denmark", "code": "DK", "phone_code": 45, "display_name": "Denmark"}, {"id": 58, "name": "Djibouti", "code": "DJ", "phone_code": 253, "display_name": "Djibouti"}, {"id": 60, "name": "Dominica", "code": "DM", "phone_code": 1767, "display_name": "Dominica"}, {"id": 61, "name": "Dominican Republic", "code": "DO", "phone_code": 1849, "display_name": "Dominican Republic"}, {"id": 63, "name": "Ecuador", "code": "EC", "phone_code": 593, "display_name": "Ecuador"}, {"id": 65, "name": "Egypt", "code": "EG", "phone_code": 20, "display_name": "Egypt"}, {"id": 209, "name": "El Salvador", "code": "SV", "phone_code": 503, "display_name": "El Salvador"}, {"id": 87, "name": "Equatorial Guinea", "code": "GQ", "phone_code": 240, "display_name": "Equatorial Guinea"}, {"id": 67, "name": "Eritrea", "code": "ER", "phone_code": 291, "display_name": "Eritrea"}, {"id": 64, "name": "Estonia", "code": "EE", "phone_code": 372, "display_name": "Estonia"}, {"id": 69, "name": "Ethiopia", "code": "ET", "phone_code": 251, "display_name": "Ethiopia"}, {"id": 72, "name": "Falkland Islands", "code": "FK", "phone_code": 500, "display_name": "Falkland Islands"}, {"id": 74, "name": "Faroe Islands", "code": "FO", "phone_code": 298, "display_name": "Faroe Islands"}, {"id": 71, "name": "Fiji", "code": "FJ", "phone_code": 679, "display_name": "Fiji"}, {"id": 70, "name": "Finland", "code": "FI", "phone_code": 358, "display_name": "Finland"}, {"id": 75, "name": "France", "code": "FR", "phone_code": 33, "display_name": "France"}, {"id": 79, "name": "French Guiana", "code": "GF", "phone_code": 594, "display_name": "French Guiana"}, {"id": 174, "name": "French Polynesia", "code": "PF", "phone_code": 689, "display_name": "French Polynesia"}, {"id": 215, "name": "French Southern Territories", "code": "TF", "phone_code": 262, "display_name": "French Southern Territories"}, {"id": 76, "name": "Gabon", "code": "GA", "phone_code": 241, "display_name": "Gabon"}, {"id": 84, "name": "Gambia", "code": "GM", "phone_code": 220, "display_name": "Gambia"}, {"id": 78, "name": "Georgia", "code": "GE", "phone_code": 995, "display_name": "Georgia"}, {"id": 57, "name": "Germany", "code": "DE", "phone_code": 49, "display_name": "Germany"}, {"id": 80, "name": "Ghana", "code": "GH", "phone_code": 233, "display_name": "Ghana"}, {"id": 81, "name": "Gibraltar", "code": "GI", "phone_code": 350, "display_name": "Gibraltar"}, {"id": 88, "name": "Greece", "code": "GR", "phone_code": 30, "display_name": "Greece"}, {"id": 83, "name": "Greenland", "code": "GL", "phone_code": 299, "display_name": "Greenland"}, {"id": 77, "name": "Grenada", "code": "GD", "phone_code": 1473, "display_name": "Grenada"}, {"id": 86, "name": "Guadeloupe", "code": "GP", "phone_code": 590, "display_name": "Guadeloupe"}, {"id": 91, "name": "Guam", "code": "GU", "phone_code": 1671, "display_name": "Guam"}, {"id": 90, "name": "Guatemala", "code": "GT", "phone_code": 502, "display_name": "Guatemala"}, {"id": 82, "name": "Guernsey", "code": "GG", "phone_code": 44, "display_name": "Guernsey"}, {"id": 85, "name": "Guinea", "code": "GN", "phone_code": 224, "display_name": "Guinea"}, {"id": 92, "name": "Guinea-Bissau", "code": "GW", "phone_code": 245, "display_name": "Guinea-Bissau"}, {"id": 93, "name": "Guyana", "code": "GY", "phone_code": 592, "display_name": "Guyana"}, {"id": 98, "name": "Haiti", "code": "HT", "phone_code": 509, "display_name": "Haiti"}, {"id": 95, "name": "Heard Island and McDonald Islands", "code": "HM", "phone_code": 672, "display_name": "Heard Island and McDonald Islands"}, {"id": 236, "name": "Holy See (Vatican City State)", "code": "VA", "phone_code": 379, "display_name": "Holy See (Vatican City State)"}, {"id": 96, "name": "Honduras", "code": "HN", "phone_code": 504, "display_name": "Honduras"}, {"id": 94, "name": "Hong Kong", "code": "HK", "phone_code": 852, "display_name": "Hong Kong"}, {"id": 99, "name": "Hungary", "code": "HU", "phone_code": 36, "display_name": "Hungary"}, {"id": 108, "name": "Iceland", "code": "IS", "phone_code": 354, "display_name": "Iceland"}, {"id": 104, "name": "India", "code": "IN", "phone_code": 91, "display_name": "India"}, {"id": 100, "name": "Indonesia", "code": "ID", "phone_code": 62, "display_name": "Indonesia"}, {"id": 107, "name": "Iran", "code": "IR", "phone_code": 98, "display_name": "Iran"}, {"id": 106, "name": "Iraq", "code": "IQ", "phone_code": 964, "display_name": "Iraq"}, {"id": 101, "name": "Ireland", "code": "IE", "phone_code": 353, "display_name": "Ireland"}, {"id": 103, "name": "Isle of Man", "code": "IM", "phone_code": 44, "display_name": "Isle of Man"}, {"id": 102, "name": "Israel", "code": "IL", "phone_code": 972, "display_name": "Israel"}, {"id": 109, "name": "Italy", "code": "IT", "phone_code": 39, "display_name": "Italy"}, {"id": 111, "name": "Jamaica", "code": "JM", "phone_code": 1876, "display_name": "Jamaica"}, {"id": 113, "name": "Japan", "code": "JP", "phone_code": 81, "display_name": "Japan"}, {"id": 110, "name": "Jersey", "code": "JE", "phone_code": 44, "display_name": "Jersey"}, {"id": 112, "name": "Jordan", "code": "JO", "phone_code": 962, "display_name": "Jordan"}, {"id": 124, "name": "Kazakhstan", "code": "KZ", "phone_code": 7, "display_name": "Kazakhstan"}, {"id": 114, "name": "Kenya", "code": "KE", "phone_code": 254, "display_name": "Kenya"}, {"id": 117, "name": "Kiribati", "code": "KI", "phone_code": 686, "display_name": "Kiribati"}, {"id": 250, "name": "Kosovo", "code": "XK", "phone_code": 383, "display_name": "Kosovo"}, {"id": 122, "name": "Kuwait", "code": "KW", "phone_code": 965, "display_name": "Kuwait"}, {"id": 115, "name": "Kyrgyzstan", "code": "KG", "phone_code": 996, "display_name": "Kyrgyzstan"}, {"id": 125, "name": "Laos", "code": "LA", "phone_code": 856, "display_name": "Laos"}, {"id": 134, "name": "Latvia", "code": "LV", "phone_code": 371, "display_name": "Latvia"}, {"id": 126, "name": "Lebanon", "code": "LB", "phone_code": 961, "display_name": "Lebanon"}, {"id": 131, "name": "Lesotho", "code": "LS", "phone_code": 266, "display_name": "Lesotho"}, {"id": 130, "name": "Liberia", "code": "LR", "phone_code": 231, "display_name": "Liberia"}, {"id": 135, "name": "Libya", "code": "LY", "phone_code": 218, "display_name": "Libya"}, {"id": 128, "name": "Liechtenstein", "code": "LI", "phone_code": 423, "display_name": "Liechtenstein"}, {"id": 132, "name": "Lithuania", "code": "LT", "phone_code": 370, "display_name": "Lithuania"}, {"id": 133, "name": "Luxembourg", "code": "LU", "phone_code": 352, "display_name": "Luxembourg"}, {"id": 147, "name": "Macau", "code": "MO", "phone_code": 853, "display_name": "Macau"}, {"id": 143, "name": "Macedonia, the former Yugoslav Republic of", "code": "MK", "phone_code": 389, "display_name": "Macedonia, the former Yugoslav Republic of"}, {"id": 141, "name": "Madagascar", "code": "MG", "phone_code": 261, "display_name": "Madagascar"}, {"id": 155, "name": "Malawi", "code": "MW", "phone_code": 265, "display_name": "Malawi"}, {"id": 157, "name": "Malaysia", "code": "MY", "phone_code": 60, "display_name": "Malaysia"}, {"id": 154, "name": "Maldives", "code": "MV", "phone_code": 960, "display_name": "Maldives"}, {"id": 144, "name": "Mali", "code": "ML", "phone_code": 223, "display_name": "Mali"}, {"id": 152, "name": "Malta", "code": "MT", "phone_code": 356, "display_name": "Malta"}, {"id": 142, "name": "Marshall Islands", "code": "MH", "phone_code": 692, "display_name": "Marshall Islands"}, {"id": 149, "name": "Martinique", "code": "MQ", "phone_code": 596, "display_name": "Martinique"}, {"id": 150, "name": "Mauritania", "code": "MR", "phone_code": 222, "display_name": "Mauritania"}, {"id": 153, "name": "Mauritius", "code": "MU", "phone_code": 230, "display_name": "Mauritius"}, {"id": 246, "name": "Mayotte", "code": "YT", "phone_code": 262, "display_name": "Mayotte"}, {"id": 156, "name": "Mexico", "code": "MX", "phone_code": 52, "display_name": "Mexico"}, {"id": 73, "name": "Micronesia", "code": "FM", "phone_code": 691, "display_name": "Micronesia"}, {"id": 138, "name": "Moldova", "code": "MD", "phone_code": 373, "display_name": "Moldova"}, {"id": 137, "name": "Monaco", "code": "MC", "phone_code": 377, "display_name": "Monaco"}, {"id": 146, "name": "Mongolia", "code": "MN", "phone_code": 976, "display_name": "Mongolia"}, {"id": 139, "name": "Montenegro", "code": "ME", "phone_code": 382, "display_name": "Montenegro"}, {"id": 151, "name": "Montserrat", "code": "MS", "phone_code": 1664, "display_name": "Montserrat"}, {"id": 136, "name": "Morocco", "code": "MA", "phone_code": 212, "display_name": "Morocco"}, {"id": 158, "name": "Mozambique", "code": "MZ", "phone_code": 258, "display_name": "Mozambique"}, {"id": 145, "name": "Myanmar", "code": "MM", "phone_code": 95, "display_name": "Myanmar"}, {"id": 159, "name": "Namibia", "code": "NA", "phone_code": 264, "display_name": "Namibia"}, {"id": 168, "name": "Nauru", "code": "NR", "phone_code": 674, "display_name": "Nauru"}, {"id": 167, "name": "Nepal", "code": "NP", "phone_code": 977, "display_name": "Nepal"}, {"id": 165, "name": "Netherlands", "code": "NL", "phone_code": 31, "display_name": "Netherlands"}, {"id": 160, "name": "New Caledonia", "code": "NC", "phone_code": 687, "display_name": "New Caledonia"}, {"id": 170, "name": "New Zealand", "code": "NZ", "phone_code": 64, "display_name": "New Zealand"}, {"id": 164, "name": "Nicaragua", "code": "NI", "phone_code": 505, "display_name": "Nicaragua"}, {"id": 161, "name": "Niger", "code": "NE", "phone_code": 227, "display_name": "Niger"}, {"id": 163, "name": "Nigeria", "code": "NG", "phone_code": 234, "display_name": "Nigeria"}, {"id": 169, "name": "Niue", "code": "NU", "phone_code": 683, "display_name": "Niue"}, {"id": 162, "name": "Norfolk Island", "code": "NF", "phone_code": 672, "display_name": "Norfolk Island"}, {"id": 120, "name": "North Korea", "code": "KP", "phone_code": 850, "display_name": "North Korea"}, {"id": 148, "name": "Northern Mariana Islands", "code": "MP", "phone_code": 1670, "display_name": "Northern Mariana Islands"}, {"id": 166, "name": "Norway", "code": "NO", "phone_code": 47, "display_name": "Norway"}, {"id": 171, "name": "Oman", "code": "OM", "phone_code": 968, "display_name": "Oman"}, {"id": 177, "name": "Pakistan", "code": "PK", "phone_code": 92, "display_name": "Pakistan"}, {"id": 184, "name": "Palau", "code": "PW", "phone_code": 680, "display_name": "Palau"}, {"id": 172, "name": "Panama", "code": "PA", "phone_code": 507, "display_name": "Panama"}, {"id": 175, "name": "Papua New Guinea", "code": "PG", "phone_code": 675, "display_name": "Papua New Guinea"}, {"id": 185, "name": "Paraguay", "code": "PY", "phone_code": 595, "display_name": "Paraguay"}, {"id": 173, "name": "Peru", "code": "PE", "phone_code": 51, "display_name": "Peru"}, {"id": 176, "name": "Philippines", "code": "PH", "phone_code": 63, "display_name": "Philippines"}, {"id": 180, "name": "Pitcairn Islands", "code": "PN", "phone_code": 64, "display_name": "Pitcairn Islands"}, {"id": 178, "name": "Poland", "code": "PL", "phone_code": 48, "display_name": "Poland"}, {"id": 183, "name": "Portugal", "code": "PT", "phone_code": 351, "display_name": "Portugal"}, {"id": 181, "name": "Puerto Rico", "code": "PR", "phone_code": 1939, "display_name": "Puerto Rico"}, {"id": 186, "name": "Qatar", "code": "QA", "phone_code": 974, "display_name": "Qatar"}, {"id": 188, "name": "Romania", "code": "RO", "phone_code": 40, "display_name": "Romania"}, {"id": 190, "name": "Russian Federation", "code": "RU", "phone_code": 7, "display_name": "Russian Federation"}, {"id": 191, "name": "Rwanda", "code": "RW", "phone_code": 250, "display_name": "Rwanda"}, {"id": 187, "name": "R\u00e9union", "code": "RE", "phone_code": 262, "display_name": "R\u00e9union"}, {"id": 26, "name": "Saint Barth\u00e9l\u00e9my", "code": "BL", "phone_code": 590, "display_name": "Saint Barth\u00e9l\u00e9my"}, {"id": 198, "name": "Saint Helena, Ascension and Tristan da Cunha", "code": "SH", "phone_code": 290, "display_name": "Saint Helena, Ascension and Tristan da Cunha"}, {"id": 119, "name": "Saint Kitts and Nevis", "code": "KN", "phone_code": 1869, "display_name": "Saint Kitts and Nevis"}, {"id": 127, "name": "Saint Lucia", "code": "LC", "phone_code": 1758, "display_name": "Saint Lucia"}, {"id": 140, "name": "Saint Martin (French part)", "code": "MF", "phone_code": 590, "display_name": "Saint Martin (French part)"}, {"id": 179, "name": "Saint Pierre and Miquelon", "code": "PM", "phone_code": 508, "display_name": "Saint Pierre and Miquelon"}, {"id": 237, "name": "Saint Vincent and the Grenadines", "code": "VC", "phone_code": 1784, "display_name": "Saint Vincent and the Grenadines"}, {"id": 244, "name": "Samoa", "code": "WS", "phone_code": 685, "display_name": "Samoa"}, {"id": 203, "name": "San Marino", "code": "SM", "phone_code": 378, "display_name": "San Marino"}, {"id": 192, "name": "Saudi Arabia", "code": "SA", "phone_code": 966, "display_name": "Saudi Arabia"}, {"id": 204, "name": "Senegal", "code": "SN", "phone_code": 221, "display_name": "Senegal"}, {"id": 189, "name": "Serbia", "code": "RS", "phone_code": 381, "display_name": "Serbia"}, {"id": 194, "name": "Seychelles", "code": "SC", "phone_code": 248, "display_name": "Seychelles"}, {"id": 202, "name": "Sierra Leone", "code": "SL", "phone_code": 232, "display_name": "Sierra Leone"}, {"id": 197, "name": "Singapore", "code": "SG", "phone_code": 65, "display_name": "Singapore"}, {"id": 210, "name": "Sint Maarten (Dutch part)", "code": "SX", "phone_code": 1721, "display_name": "Sint Maarten (Dutch part)"}, {"id": 201, "name": "Slovakia", "code": "SK", "phone_code": 421, "display_name": "Slovakia"}, {"id": 199, "name": "Slovenia", "code": "SI", "phone_code": 386, "display_name": "Slovenia"}, {"id": 193, "name": "Solomon Islands", "code": "SB", "phone_code": 677, "display_name": "Solomon Islands"}, {"id": 205, "name": "Somalia", "code": "SO", "phone_code": 252, "display_name": "Somalia"}, {"id": 247, "name": "South Africa", "code": "ZA", "phone_code": 27, "display_name": "South Africa"}, {"id": 89, "name": "South Georgia and the South Sandwich Islands", "code": "GS", "phone_code": 500, "display_name": "South Georgia and the South Sandwich Islands"}, {"id": 121, "name": "South Korea", "code": "KR", "phone_code": 82, "display_name": "South Korea"}, {"id": 207, "name": "South Sudan", "code": "SS", "phone_code": 211, "display_name": "South Sudan"}, {"id": 68, "name": "Spain", "code": "ES", "phone_code": 34, "display_name": "Spain"}, {"id": 129, "name": "Sri Lanka", "code": "LK", "phone_code": 94, "display_name": "Sri Lanka"}, {"id": 182, "name": "State of Palestine", "code": "PS", "phone_code": 970, "display_name": "State of Palestine"}, {"id": 195, "name": "Sudan", "code": "SD", "phone_code": 249, "display_name": "Sudan"}, {"id": 206, "name": "Suriname", "code": "SR", "phone_code": 597, "display_name": "Suriname"}, {"id": 200, "name": "Svalbard and Jan Mayen", "code": "SJ", "phone_code": 47, "display_name": "Svalbard and Jan Mayen"}, {"id": 212, "name": "Swaziland", "code": "SZ", "phone_code": 268, "display_name": "Swaziland"}, {"id": 196, "name": "Sweden", "code": "SE", "phone_code": 46, "display_name": "Sweden"}, {"id": 43, "name": "Switzerland", "code": "CH", "phone_code": 41, "display_name": "Switzerland"}, {"id": 211, "name": "Syria", "code": "SY", "phone_code": 963, "display_name": "Syria"}, {"id": 208, "name": "S\u00e3o Tom\u00e9 and Pr\u00edncipe", "code": "ST", "phone_code": 239, "display_name": "S\u00e3o Tom\u00e9 and Pr\u00edncipe"}, {"id": 227, "name": "Taiwan", "code": "TW", "phone_code": 886, "display_name": "Taiwan"}, {"id": 218, "name": "Tajikistan", "code": "TJ", "phone_code": 992, "display_name": "Tajikistan"}, {"id": 228, "name": "Tanzania", "code": "TZ", "phone_code": 255, "display_name": "Tanzania"}, {"id": 217, "name": "Thailand", "code": "TH", "phone_code": 66, "display_name": "Thailand"}, {"id": 223, "name": "Timor-Leste", "code": "TL", "phone_code": 670, "display_name": "Timor-Leste"}, {"id": 216, "name": "Togo", "code": "TG", "phone_code": 228, "display_name": "Togo"}, {"id": 219, "name": "Tokelau", "code": "TK", "phone_code": 690, "display_name": "Tokelau"}, {"id": 222, "name": "Tonga", "code": "TO", "phone_code": 676, "display_name": "Tonga"}, {"id": 225, "name": "Trinidad and Tobago", "code": "TT", "phone_code": 1868, "display_name": "Trinidad and Tobago"}, {"id": 221, "name": "Tunisia", "code": "TN", "phone_code": 216, "display_name": "Tunisia"}, {"id": 224, "name": "Turkey", "code": "TR", "phone_code": 90, "display_name": "Turkey"}, {"id": 220, "name": "Turkmenistan", "code": "TM", "phone_code": 993, "display_name": "Turkmenistan"}, {"id": 213, "name": "Turks and Caicos Islands", "code": "TC", "phone_code": 1649, "display_name": "Turks and Caicos Islands"}, {"id": 226, "name": "Tuvalu", "code": "TV", "phone_code": 688, "display_name": "Tuvalu"}, {"id": 232, "name": "USA Minor Outlying Islands", "code": "UM", "phone_code": 699, "display_name": "USA Minor Outlying Islands"}, {"id": 230, "name": "Uganda", "code": "UG", "phone_code": 256, "display_name": "Uganda"}, {"id": 229, "name": "Ukraine", "code": "UA", "phone_code": 380, "display_name": "Ukraine"}, {"id": 2, "name": "United Arab Emirates", "code": "AE", "phone_code": 971, "display_name": "United Arab Emirates"}, {"id": 231, "name": "United Kingdom", "code": "GB", "phone_code": 44, "display_name": "United Kingdom"}, {"id": 233, "name": "United States", "code": "US", "phone_code": 1, "display_name": "United States"}, {"id": 234, "name": "Uruguay", "code": "UY", "phone_code": 598, "display_name": "Uruguay"}, {"id": 235, "name": "Uzbekistan", "code": "UZ", "phone_code": 998, "display_name": "Uzbekistan"}, {"id": 242, "name": "Vanuatu", "code": "VU", "phone_code": 678, "display_name": "Vanuatu"}, {"id": 238, "name": "Venezuela", "code": "VE", "phone_code": 58, "display_name": "Venezuela"}, {"id": 241, "name": "Vietnam", "code": "VN", "phone_code": 84, "display_name": "Vietnam"}, {"id": 239, "name": "Virgin Islands (British)", "code": "VG", "phone_code": 1284, "display_name": "Virgin Islands (British)"}, {"id": 240, "name": "Virgin Islands (USA)", "code": "VI", "phone_code": 1340, "display_name": "Virgin Islands (USA)"}, {"id": 243, "name": "Wallis and Futuna", "code": "WF", "phone_code": 681, "display_name": "Wallis and Futuna"}, {"id": 66, "name": "Western Sahara", "code": "EH", "phone_code": 212, "display_name": "Western Sahara"}, {"id": 245, "name": "Yemen", "code": "YE", "phone_code": 967, "display_name": "Yemen"}, {"id": 248, "name": "Zambia", "code": "ZM", "phone_code": 260, "display_name": "Zambia"}, {"id": 249, "name": "Zimbabwe", "code": "ZW", "phone_code": 263, "display_name": "Zimbabwe"}, {"id": 15, "name": "\u00c5land Islands", "code": "AX", "phone_code": 358, "display_name": "\u00c5land Islands"}]

var country_var_test = [{"id": 3, "name": "Afghanistan", "code": "AF", "phone_code": 93, "display_name": {"en_GB": "Afghanistan", "ru_RU": "\u0410\u0444\u0433\u0430\u043d\u0438\u0441\u0442\u0430\u043d"}}, {"id": 6, "name": "Albania", "code": "AL", "phone_code": 355, "display_name": {"en_GB": "Albania", "ru_RU": "\u0410\u043b\u0431\u0430\u043d\u0438\u044f"}}, {"id": 62, "name": "Algeria", "code": "DZ", "phone_code": 213, "display_name": {"en_GB": "Algeria", "ru_RU": "\u0410\u043b\u0436\u0438\u0440"}}, {"id": 11, "name": "American Samoa", "code": "AS", "phone_code": 1684, "display_name": {"en_GB": "American Samoa", "ru_RU": "\u0410\u043c\u0435\u0440\u0438\u043a\u0430\u043d\u0441\u043a\u043e\u0435 \u0421\u0430\u043c\u043e\u0430"}}, {"id": 1, "name": "Andorra", "code": "AD", "phone_code": 376, "display_name": {"en_GB": "Andorra", "ru_RU": "\u0410\u043d\u0434\u043e\u0440\u0440\u0430"}}, {"id": 8, "name": "Angola", "code": "AO", "phone_code": 244, "display_name": {"en_GB": "Angola", "ru_RU": "\u0410\u043d\u0433\u043e\u043b\u0430"}}, {"id": 5, "name": "Anguilla", "code": "AI", "phone_code": 1264, "display_name": {"en_GB": "Anguilla", "ru_RU": "\u0410\u043d\u0433\u0438\u043b\u044c\u044f"}}, {"id": 9, "name": "Antarctica", "code": "AQ", "phone_code": 672, "display_name": {"en_GB": "Antarctica", "ru_RU": "\u0410\u043d\u0442\u0430\u0440\u043a\u0442\u0438\u0434\u0430"}}, {"id": 4, "name": "Antigua and Barbuda", "code": "AG", "phone_code": 1268, "display_name": {"en_GB": "Antigua and Barbuda", "ru_RU": "\u0410\u043d\u0442\u0438\u0433\u0443\u0430 \u0438 \u0411\u0430\u0440\u0431\u0443\u0434\u0430"}}, {"id": 10, "name": "Argentina", "code": "AR", "phone_code": 54, "display_name": {"en_GB": "Argentina", "ru_RU": "\u0410\u0440\u0433\u0435\u043d\u0442\u0438\u043d\u0430"}}, {"id": 7, "name": "Armenia", "code": "AM", "phone_code": 374, "display_name": {"en_GB": "Armenia", "ru_RU": "\u0410\u0440\u043c\u0435\u043d\u0438\u044f"}}, {"id": 14, "name": "Aruba", "code": "AW", "phone_code": 297, "display_name": {"en_GB": "Aruba", "ru_RU": "\u0410\u0440\u0443\u0431\u0430"}}, {"id": 13, "name": "Australia", "code": "AU", "phone_code": 61, "display_name": {"en_GB": "Australia", "ru_RU": "\u0410\u0432\u0441\u0442\u0440\u0430\u043b\u0438\u044f"}}, {"id": 12, "name": "Austria", "code": "AT", "phone_code": 43, "display_name": {"en_GB": "Austria", "ru_RU": "\u0410\u0432\u0441\u0442\u0440\u0438\u044f"}}, {"id": 16, "name": "Azerbaijan", "code": "AZ", "phone_code": 994, "display_name": {"en_GB": "Azerbaijan", "ru_RU": "\u0410\u0437\u0435\u0440\u0431\u0430\u0439\u0434\u0436\u0430\u043d"}}, {"id": 32, "name": "Bahamas", "code": "BS", "phone_code": 1242, "display_name": {"en_GB": "Bahamas", "ru_RU": "\u0411\u0430\u0433\u0430\u043c\u0441\u043a\u0438\u0435 \u043e\u0441\u0442\u0440\u043e\u0432\u0430"}}, {"id": 23, "name": "Bahrain", "code": "BH", "phone_code": 973, "display_name": {"en_GB": "Bahrain", "ru_RU": "\u0411\u0430\u0445\u0440\u0435\u0439\u043d"}}, {"id": 19, "name": "Bangladesh", "code": "BD", "phone_code": 880, "display_name": {"en_GB": "Bangladesh", "ru_RU": "\u0411\u0430\u043d\u0433\u043b\u0430\u0434\u0435\u0448"}}, {"id": 18, "name": "Barbados", "code": "BB", "phone_code": 1246, "display_name": {"en_GB": "Barbados", "ru_RU": "\u0411\u0430\u0440\u0431\u0430\u0434\u043e\u0441"}}, {"id": 36, "name": "Belarus", "code": "BY", "phone_code": 375, "display_name": {"en_GB": "Belarus", "ru_RU": "\u0411\u0435\u043b\u0430\u0440\u0443\u0441\u044c"}}, {"id": 20, "name": "Belgium", "code": "BE", "phone_code": 32, "display_name": {"en_GB": "Belgium", "ru_RU": "\u0411\u0435\u043b\u044c\u0433\u0438\u044f"}}, {"id": 37, "name": "Belize", "code": "BZ", "phone_code": 501, "display_name": {"en_GB": "Belize", "ru_RU": "\u0411\u0435\u043b\u0438\u0437"}}, {"id": 25, "name": "Benin", "code": "BJ", "phone_code": 229, "display_name": {"en_GB": "Benin", "ru_RU": "\u0411\u0435\u043d\u0438\u043d"}}, {"id": 27, "name": "Bermuda", "code": "BM", "phone_code": 1441, "display_name": {"en_GB": "Bermuda", "ru_RU": "\u0411\u0435\u0440\u043c\u0443\u0434\u044b"}}, {"id": 33, "name": "Bhutan", "code": "BT", "phone_code": 975, "display_name": {"en_GB": "Bhutan", "ru_RU": "\u0411\u0443\u0442\u0430\u043d"}}, {"id": 29, "name": "Bolivia", "code": "BO", "phone_code": 591, "display_name": {"en_GB": "Bolivia", "ru_RU": "\u0411\u043e\u043b\u0438\u0432\u0438\u044f"}}, {"id": 30, "name": "Bonaire, Sint Eustatius and Saba", "code": "BQ", "phone_code": 599, "display_name": {"en_GB": "Bonaire, Sint Eustatius and Saba", "ru_RU": "\u0411\u043e\u043d\u044d\u0439\u0440, \u0421\u0438\u043d\u0442-\u042d\u0441\u0442\u0430\u0442\u0438\u0443\u0441 \u0438 \u0421\u0430\u0431\u0430"}}, {"id": 17, "name": "Bosnia and Herzegovina", "code": "BA", "phone_code": 387, "display_name": {"en_GB": "Bosnia and Herzegovina", "ru_RU": "\u0411\u043e\u0441\u043d\u0438\u044f \u0438 \u0413\u0435\u0440\u0446\u0435\u0433\u043e\u0432\u0438\u043d\u0430"}}, {"id": 35, "name": "Botswana", "code": "BW", "phone_code": 267, "display_name": {"en_GB": "Botswana", "ru_RU": "\u0411\u043e\u0442\u0441\u0432\u0430\u043d\u0430"}}, {"id": 34, "name": "Bouvet Island", "code": "BV", "phone_code": 55, "display_name": {"en_GB": "Bouvet Island", "ru_RU": "\u041e\u0441\u0442\u0440\u043e\u0432 \u0411\u0443\u0432\u0435"}}, {"id": 31, "name": "Brazil", "code": "BR", "phone_code": 55, "display_name": {"en_GB": "Brazil", "ru_RU": "\u0411\u0440\u0430\u0437\u0438\u043b\u0438\u044f"}}, {"id": 105, "name": "British Indian Ocean Territory", "code": "IO", "phone_code": 246, "display_name": {"en_GB": "British Indian Ocean Territory", "ru_RU": "\u0411\u0440\u0438\u0442\u0430\u043d\u0441\u043a\u0430\u044f \u0442\u0435\u0440\u0440\u0438\u0442\u043e\u0440\u0438\u044f \u0432 \u0418\u043d\u0434\u0438\u0439\u0441\u043a\u043e\u043c \u043e\u043a\u0435\u0430\u043d\u0435"}}, {"id": 28, "name": "Brunei Darussalam", "code": "BN", "phone_code": 673, "display_name": {"en_GB": "Brunei Darussalam", "ru_RU": "\u0411\u0440\u0443\u043d\u0435\u0439 \u0414\u0430\u0440\u0443\u0441\u0441\u0430\u043b\u0430\u043c"}}, {"id": 22, "name": "Bulgaria", "code": "BG", "phone_code": 359, "display_name": {"en_GB": "Bulgaria", "ru_RU": "\u0411\u043e\u043b\u0433\u0430\u0440\u0438\u044f"}}, {"id": 21, "name": "Burkina Faso", "code": "BF", "phone_code": 226, "display_name": {"en_GB": "Burkina Faso", "ru_RU": "\u0411\u0443\u0440\u043a\u0438\u043d\u0430-\u0424\u0430\u0441\u043e"}}, {"id": 24, "name": "Burundi", "code": "BI", "phone_code": 257, "display_name": {"en_GB": "Burundi", "ru_RU": "\u0411\u0443\u0440\u0443\u043d\u0434\u0438"}}, {"id": 116, "name": "Cambodia", "code": "KH", "phone_code": 855, "display_name": {"en_GB": "Cambodia", "ru_RU": "\u041a\u0430\u043c\u0431\u043e\u0434\u0436\u0430"}}, {"id": 47, "name": "Cameroon", "code": "CM", "phone_code": 237, "display_name": {"en_GB": "Cameroon", "ru_RU": "\u041a\u0430\u043c\u0435\u0440\u0443\u043d"}}, {"id": 38, "name": "Canada", "code": "CA", "phone_code": 1, "display_name": {"en_GB": "Canada", "ru_RU": "\u041a\u0430\u043d\u0430\u0434\u0430"}}, {"id": 52, "name": "Cape Verde", "code": "CV", "phone_code": 238, "display_name": {"en_GB": "Cape Verde", "ru_RU": "\u041a\u0430\u0431\u043e-\u0412\u0435\u0440\u0434\u0435"}}, {"id": 123, "name": "Cayman Islands", "code": "KY", "phone_code": 1345, "display_name": {"en_GB": "Cayman Islands", "ru_RU": "\u041a\u0430\u0439\u043c\u0430\u043d\u043e\u0432\u044b \u043e\u0441\u0442\u0440\u043e\u0432\u0430"}}, {"id": 40, "name": "Central African Republic", "code": "CF", "phone_code": 236, "display_name": {"en_GB": "Central African Republic", "ru_RU": "\u0426\u0435\u043d\u0442\u0440\u0430\u043b\u044c\u043d\u043e-\u0410\u0444\u0440\u0438\u043a\u0430\u043d\u0441\u043a\u0430\u044f \u0420\u0435\u0441\u043f\u0443\u0431\u043b\u0438\u043a\u0430"}}, {"id": 214, "name": "Chad", "code": "TD", "phone_code": 235, "display_name": {"en_GB": "Chad", "ru_RU": "\u0427\u0430\u0434"}}, {"id": 46, "name": "Chile", "code": "CL", "phone_code": 56, "display_name": {"en_GB": "Chile", "ru_RU": "\u0427\u0438\u043b\u0438"}}, {"id": 48, "name": "China", "code": "CN", "phone_code": 86, "display_name": {"en_GB": "China", "ru_RU": "\u041a\u0438\u0442\u0430\u0439"}}, {"id": 54, "name": "Christmas Island", "code": "CX", "phone_code": 61, "display_name": {"en_GB": "Christmas Island", "ru_RU": "\u041e\u0441\u0442\u0440\u043e\u0432 \u0420\u043e\u0436\u0434\u0435\u0441\u0442\u0432\u0430"}}, {"id": 39, "name": "Cocos (Keeling) Islands", "code": "CC", "phone_code": 61, "display_name": {"en_GB": "Cocos (Keeling) Islands", "ru_RU": "\u041a\u043e\u043a\u043e\u0441\u043e\u0432\u044b\u0435 \u043e\u0441\u0442\u0440\u043e\u0432\u0430 (\u041e\u0441\u0442\u0440\u043e\u0432\u0430 \u041a\u0438\u043b\u0438\u043d\u0433)"}}, {"id": 49, "name": "Colombia", "code": "CO", "phone_code": 57, "display_name": {"en_GB": "Colombia", "ru_RU": "\u041a\u043e\u043b\u0443\u043c\u0431\u0438\u044f"}}, {"id": 118, "name": "Comoros", "code": "KM", "phone_code": 269, "display_name": {"en_GB": "Comoros", "ru_RU": "\u041a\u043e\u043c\u043e\u0440\u0441\u043a\u0438\u0435 \u041e\u0441\u0442\u0440\u043e\u0432\u0430"}}, {"id": 42, "name": "Congo", "code": "CG", "phone_code": 243, "display_name": {"en_GB": "Congo", "ru_RU": "\u041a\u043e\u043d\u0433\u043e"}}, {"id": 45, "name": "Cook Islands", "code": "CK", "phone_code": 682, "display_name": {"en_GB": "Cook Islands", "ru_RU": "\u041e\u0441\u0442\u0440\u043e\u0432\u0430 \u041a\u0443\u043a\u0430"}}, {"id": 50, "name": "Costa Rica", "code": "CR", "phone_code": 506, "display_name": {"en_GB": "Costa Rica", "ru_RU": "\u041a\u043e\u0441\u0442\u0430-\u0420\u0438\u043a\u0430"}}, {"id": 97, "name": "Croatia", "code": "HR", "phone_code": 385, "display_name": {"en_GB": "Croatia", "ru_RU": "\u0425\u043e\u0440\u0432\u0430\u0442\u0438\u044f"}}, {"id": 51, "name": "Cuba", "code": "CU", "phone_code": 53, "display_name": {"en_GB": "Cuba", "ru_RU": "\u041a\u0443\u0431\u0430"}}, {"id": 53, "name": "Cura\u00e7ao", "code": "CW", "phone_code": 599, "display_name": {"en_GB": "Cura\u00e7ao", "ru_RU": "\u041a\u044e\u0440\u0430\u0441\u0430\u043e"}}, {"id": 55, "name": "Cyprus", "code": "CY", "phone_code": 357, "display_name": {"en_GB": "Cyprus", "ru_RU": "\u041a\u0438\u043f\u0440"}}, {"id": 56, "name": "Czech Republic", "code": "CZ", "phone_code": 420, "display_name": {"en_GB": "Czech Republic", "ru_RU": "\u0427\u0435\u0448\u0441\u043a\u0430\u044f \u0440\u0435\u0441\u043f\u0443\u0431\u043b\u0438\u043a\u0430"}}, {"id": 44, "name": "C\u00f4te d'Ivoire", "code": "CI", "phone_code": 225, "display_name": {"en_GB": "C\u00f4te d'Ivoire", "ru_RU": "\u0411\u0435\u0440\u0435\u0433 \ud83d\udc18 \u0421\u043b\u043e\u043d\u043e\u0432\u043e\u0439 \u041a\u043e\u0441\u0442\u0438"}}, {"id": 41, "name": "Democratic Republic of the Congo", "code": "CD", "phone_code": 242, "display_name": {"en_GB": "Democratic Republic of the Congo", "ru_RU": "\u0414\u0435\u043c\u043e\u043a\u0440\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u0430\u044f \u0420\u0435\u0441\u043f\u0443\u0431\u043b\u0438\u043a\u0430 \u041a\u043e\u043d\u0433\u043e"}}, {"id": 59, "name": "Denmark", "code": "DK", "phone_code": 45, "display_name": {"en_GB": "Denmark", "ru_RU": "\u0414\u0430\u043d\u0438\u044f"}}, {"id": 58, "name": "Djibouti", "code": "DJ", "phone_code": 253, "display_name": {"en_GB": "Djibouti", "ru_RU": "\u0414\u0436\u0438\u0431\u0443\u0442\u0438"}}, {"id": 60, "name": "Dominica", "code": "DM", "phone_code": 1767, "display_name": {"en_GB": "Dominica", "ru_RU": "\u0414\u043e\u043c\u0438\u043d\u0438\u043a\u0430"}}, {"id": 61, "name": "Dominican Republic", "code": "DO", "phone_code": 1849, "display_name": {"en_GB": "Dominican Republic", "ru_RU": "\u0414\u043e\u043c\u0438\u043d\u0438\u043a\u0430\u043d\u0441\u043a\u0430\u044f \u0440\u0435\u0441\u043f\u0443\u0431\u043b\u0438\u043a\u0430"}}, {"id": 63, "name": "Ecuador", "code": "EC", "phone_code": 593, "display_name": {"en_GB": "Ecuador", "ru_RU": "\u042d\u043a\u0432\u0430\u0434\u043e\u0440"}}, {"id": 65, "name": "Egypt", "code": "EG", "phone_code": 20, "display_name": {"en_GB": "Egypt", "ru_RU": "\u0415\u0433\u0438\u043f\u0435\u0442"}}, {"id": 209, "name": "El Salvador", "code": "SV", "phone_code": 503, "display_name": {"en_GB": "El Salvador", "ru_RU": "\u042d\u043b\u044c \u0421\u0430\u043b\u044c\u0432\u0430\u0434\u043e\u0440"}}, {"id": 87, "name": "Equatorial Guinea", "code": "GQ", "phone_code": 240, "display_name": {"en_GB": "Equatorial Guinea", "ru_RU": "\u042d\u043a\u0432\u0430\u0442\u043e\u0440\u0438\u0430\u043b\u044c\u043d\u0430\u044f \u0413\u0432\u0438\u043d\u0435\u044f"}}, {"id": 67, "name": "Eritrea", "code": "ER", "phone_code": 291, "display_name": {"en_GB": "Eritrea", "ru_RU": "\u042d\u0440\u0438\u0442\u0440\u0438\u044f"}}, {"id": 64, "name": "Estonia", "code": "EE", "phone_code": 372, "display_name": {"en_GB": "Estonia", "ru_RU": "\u042d\u0441\u0442\u043e\u043d\u0438\u044f"}}, {"id": 69, "name": "Ethiopia", "code": "ET", "phone_code": 251, "display_name": {"en_GB": "Ethiopia", "ru_RU": "\u042d\u0444\u0438\u043e\u043f\u0438\u044f"}}, {"id": 72, "name": "Falkland Islands", "code": "FK", "phone_code": 500, "display_name": {"en_GB": "Falkland Islands", "ru_RU": "\u0424\u043e\u043b\u043a\u043b\u0435\u043d\u0434\u0441\u043a\u0438\u0435 (\u041c\u0430\u043b\u044c\u0432\u0438\u043d\u0441\u043a\u0438\u0435) \u043e\u0441\u0442\u0440\u043e\u0432\u0430"}}, {"id": 74, "name": "Faroe Islands", "code": "FO", "phone_code": 298, "display_name": {"en_GB": "Faroe Islands", "ru_RU": "\u0424\u0430\u0440\u0435\u0440\u0441\u043a\u0438\u0435 \u043e\u0441\u0442\u0440\u043e\u0432\u0430"}}, {"id": 71, "name": "Fiji", "code": "FJ", "phone_code": 679, "display_name": {"en_GB": "Fiji", "ru_RU": "\u0424\u0438\u0434\u0436\u0438"}}, {"id": 70, "name": "Finland", "code": "FI", "phone_code": 358, "display_name": {"en_GB": "Finland", "ru_RU": "\u0424\u0438\u043d\u043b\u044f\u043d\u0434\u0438\u044f"}}, {"id": 75, "name": "France", "code": "FR", "phone_code": 33, "display_name": {"en_GB": "France", "ru_RU": "\u0424\u0440\u0430\u043d\u0446\u0438\u044f"}}, {"id": 79, "name": "French Guiana", "code": "GF", "phone_code": 594, "display_name": {"en_GB": "French Guiana", "ru_RU": "\u0424\u0440\u0430\u043d\u0446\u0443\u0437\u0441\u043a\u0430\u044f \u0413\u0432\u0438\u0430\u043d\u0430"}}, {"id": 174, "name": "French Polynesia", "code": "PF", "phone_code": 689, "display_name": {"en_GB": "French Polynesia", "ru_RU": "\u0424\u0440\u0430\u043d\u0446\u0443\u0437\u0441\u043a\u0430\u044f \u041f\u043e\u043b\u0438\u043d\u0435\u0437\u0438\u044f"}}, {"id": 215, "name": "French Southern Territories", "code": "TF", "phone_code": 262, "display_name": {"en_GB": "French Southern Territories", "ru_RU": "\u0424\u0440\u0430\u043d\u0446\u0438\u0437\u043a\u0438\u0435 \u042e\u0436\u043d\u044b\u0435 \u0422\u0435\u0440\u0440\u0438\u0442\u043e\u0440\u0438\u0438"}}, {"id": 76, "name": "Gabon", "code": "GA", "phone_code": 241, "display_name": {"en_GB": "Gabon", "ru_RU": "\u0413\u0430\u0431\u043e\u043d"}}, {"id": 84, "name": "Gambia", "code": "GM", "phone_code": 220, "display_name": {"en_GB": "Gambia", "ru_RU": "\u0413\u0430\u043c\u0431\u0438\u044f"}}, {"id": 78, "name": "Georgia", "code": "GE", "phone_code": 995, "display_name": {"en_GB": "Georgia", "ru_RU": "\u0413\u0440\u0443\u0437\u0438\u044f"}}, {"id": 57, "name": "Germany", "code": "DE", "phone_code": 49, "display_name": {"en_GB": "Germany", "ru_RU": "\u0413\u0435\u0440\u043c\u0430\u043d\u0438\u044f"}}, {"id": 80, "name": "Ghana", "code": "GH", "phone_code": 233, "display_name": {"en_GB": "Ghana", "ru_RU": "\u0413\u0430\u043d\u0430"}}, {"id": 81, "name": "Gibraltar", "code": "GI", "phone_code": 350, "display_name": {"en_GB": "Gibraltar", "ru_RU": "\u0413\u0438\u0431\u0440\u0430\u043b\u0442\u0430\u0440"}}, {"id": 88, "name": "Greece", "code": "GR", "phone_code": 30, "display_name": {"en_GB": "Greece", "ru_RU": "\u0413\u0440\u0435\u0446\u0438\u044f"}}, {"id": 83, "name": "Greenland", "code": "GL", "phone_code": 299, "display_name": {"en_GB": "Greenland", "ru_RU": "\u0413\u0440\u0435\u043d\u043b\u0430\u043d\u0434\u0438\u044f"}}, {"id": 77, "name": "Grenada", "code": "GD", "phone_code": 1473, "display_name": {"en_GB": "Grenada", "ru_RU": "\u0413\u0440\u0435\u043d\u0430\u0434\u0430"}}, {"id": 86, "name": "Guadeloupe", "code": "GP", "phone_code": 590, "display_name": {"en_GB": "Guadeloupe", "ru_RU": "\u0413\u0432\u0430\u0434\u0435\u043b\u0443\u043f\u0430"}}, {"id": 91, "name": "Guam", "code": "GU", "phone_code": 1671, "display_name": {"en_GB": "Guam", "ru_RU": "\u0413\u0443\u0430\u043c"}}, {"id": 90, "name": "Guatemala", "code": "GT", "phone_code": 502, "display_name": {"en_GB": "Guatemala", "ru_RU": "\u0413\u0432\u0430\u0442\u0435\u043c\u0430\u043b\u0430"}}, {"id": 82, "name": "Guernsey", "code": "GG", "phone_code": 44, "display_name": {"en_GB": "Guernsey", "ru_RU": "\u0413\u0435\u0440\u043d\u0441\u0438"}}, {"id": 85, "name": "Guinea", "code": "GN", "phone_code": 224, "display_name": {"en_GB": "Guinea", "ru_RU": "\u0413\u0432\u0438\u043d\u0435\u044f"}}, {"id": 92, "name": "Guinea-Bissau", "code": "GW", "phone_code": 245, "display_name": {"en_GB": "Guinea-Bissau", "ru_RU": "\u0413\u0432\u0438\u043d\u0435\u044f-\u0411\u0438\u0441\u0430\u0443"}}, {"id": 93, "name": "Guyana", "code": "GY", "phone_code": 592, "display_name": {"en_GB": "Guyana", "ru_RU": "\u0413\u0443\u0430\u043d\u0430"}}, {"id": 98, "name": "Haiti", "code": "HT", "phone_code": 509, "display_name": {"en_GB": "Haiti", "ru_RU": "\u0413\u0430\u0438\u0442\u0438"}}, {"id": 95, "name": "Heard Island and McDonald Islands", "code": "HM", "phone_code": 672, "display_name": {"en_GB": "Heard Island and McDonald Islands", "ru_RU": "\u041e\u0441\u0442\u0440\u043e\u0432 \u0425\u0435\u0440\u0434 \u0438 \u043e\u0441\u0442\u0440\u043e\u0432\u0430 \u041c\u0430\u043a\u0434\u043e\u043d\u0430\u043b\u044c\u0434"}}, {"id": 236, "name": "Holy See (Vatican City State)", "code": "VA", "phone_code": 379, "display_name": {"en_GB": "Holy See (Vatican City State)", "ru_RU": "\u0412\u0430\u0442\u0438\u043a\u0430\u043d (\u041f\u0430\u043f\u0441\u043a\u0438\u0439 \u041f\u0440\u0435\u0441\u0442\u043e\u043b)"}}, {"id": 96, "name": "Honduras", "code": "HN", "phone_code": 504, "display_name": {"en_GB": "Honduras", "ru_RU": "\u0413\u043e\u043d\u0434\u0443\u0440\u0430\u0441"}}, {"id": 94, "name": "Hong Kong", "code": "HK", "phone_code": 852, "display_name": {"en_GB": "Hong Kong", "ru_RU": "\u0413\u043e\u043d\u043a\u043e\u043d\u0433"}}, {"id": 99, "name": "Hungary", "code": "HU", "phone_code": 36, "display_name": {"en_GB": "Hungary", "ru_RU": "\u0412\u0435\u043d\u0433\u0440\u0438\u044f"}}, {"id": 108, "name": "Iceland", "code": "IS", "phone_code": 354, "display_name": {"en_GB": "Iceland", "ru_RU": "\u0418\u0441\u043b\u0430\u043d\u0434\u0438\u044f"}}, {"id": 104, "name": "India", "code": "IN", "phone_code": 91, "display_name": {"en_GB": "India", "ru_RU": "\u0418\u043d\u0434\u0438\u044f"}}, {"id": 100, "name": "Indonesia", "code": "ID", "phone_code": 62, "display_name": {"en_GB": "Indonesia", "ru_RU": "\u0418\u043d\u0434\u043e\u043d\u0435\u0437\u0438\u044f"}}, {"id": 107, "name": "Iran", "code": "IR", "phone_code": 98, "display_name": {"en_GB": "Iran", "ru_RU": "\u0418\u0440\u0430\u043d"}}, {"id": 106, "name": "Iraq", "code": "IQ", "phone_code": 964, "display_name": {"en_GB": "Iraq", "ru_RU": "\u0418\u0440\u0430\u043a"}}, {"id": 101, "name": "Ireland", "code": "IE", "phone_code": 353, "display_name": {"en_GB": "Ireland", "ru_RU": "\u0418\u0440\u043b\u0430\u043d\u0434\u0438\u044f"}}, {"id": 103, "name": "Isle of Man", "code": "IM", "phone_code": 44, "display_name": {"en_GB": "Isle of Man", "ru_RU": "\u041e\u0441\u0442\u0440\u043e\u0432 \u041c\u044d\u043d"}}, {"id": 102, "name": "Israel", "code": "IL", "phone_code": 972, "display_name": {"en_GB": "Israel", "ru_RU": "\u0418\u0437\u0440\u0430\u0438\u043b\u044c"}}, {"id": 109, "name": "Italy", "code": "IT", "phone_code": 39, "display_name": {"en_GB": "Italy", "ru_RU": "\u0418\u0442\u0430\u043b\u0438\u044f"}}, {"id": 111, "name": "Jamaica", "code": "JM", "phone_code": 1876, "display_name": {"en_GB": "Jamaica", "ru_RU": "\u042f\u043c\u0430\u0439\u043a\u0430"}}, {"id": 113, "name": "Japan", "code": "JP", "phone_code": 81, "display_name": {"en_GB": "Japan", "ru_RU": "\u042f\u043f\u043e\u043d\u0438\u044f"}}, {"id": 110, "name": "Jersey", "code": "JE", "phone_code": 44, "display_name": {"en_GB": "Jersey", "ru_RU": "\u0414\u0436\u0435\u0440\u0441\u0438"}}, {"id": 112, "name": "Jordan", "code": "JO", "phone_code": 962, "display_name": {"en_GB": "Jordan", "ru_RU": "\u0418\u043e\u0440\u0434\u0430\u043d\u0438\u044f"}}, {"id": 124, "name": "Kazakhstan", "code": "KZ", "phone_code": 7, "display_name": {"en_GB": "Kazakhstan", "ru_RU": "\u041a\u0430\u0437\u0430\u0445\u0441\u0442\u0430\u043d"}}, {"id": 114, "name": "Kenya", "code": "KE", "phone_code": 254, "display_name": {"en_GB": "Kenya", "ru_RU": "\u041a\u0435\u043d\u0438\u044f"}}, {"id": 117, "name": "Kiribati", "code": "KI", "phone_code": 686, "display_name": {"en_GB": "Kiribati", "ru_RU": "\u041a\u0438\u0440\u0438\u0431\u0430\u0442\u0438"}}, {"id": 250, "name": "Kosovo", "code": "XK", "phone_code": 383, "display_name": {"en_GB": "Kosovo", "ru_RU": "\u041a\u043e\u0441\u043e\u0432\u043e"}}, {"id": 122, "name": "Kuwait", "code": "KW", "phone_code": 965, "display_name": {"en_GB": "Kuwait", "ru_RU": "\u041a\u0443\u0432\u0435\u0439\u0442"}}, {"id": 115, "name": "Kyrgyzstan", "code": "KG", "phone_code": 996, "display_name": {"en_GB": "Kyrgyzstan", "ru_RU": "\u041a\u0438\u0440\u0433\u0438\u0437\u0438\u044f"}}, {"id": 125, "name": "Laos", "code": "LA", "phone_code": 856, "display_name": {"en_GB": "Laos", "ru_RU": "\u041b\u0430\u043e\u0441"}}, {"id": 134, "name": "Latvia", "code": "LV", "phone_code": 371, "display_name": {"en_GB": "Latvia", "ru_RU": "\u041b\u0430\u0442\u0432\u0438\u044f"}}, {"id": 126, "name": "Lebanon", "code": "LB", "phone_code": 961, "display_name": {"en_GB": "Lebanon", "ru_RU": "\u041b\u0438\u0432\u0430\u043d"}}, {"id": 131, "name": "Lesotho", "code": "LS", "phone_code": 266, "display_name": {"en_GB": "Lesotho", "ru_RU": "\u041b\u0435\u0441\u043e\u0442\u043e"}}, {"id": 130, "name": "Liberia", "code": "LR", "phone_code": 231, "display_name": {"en_GB": "Liberia", "ru_RU": "\u041b\u0438\u0431\u0435\u0440\u0438\u044f"}}, {"id": 135, "name": "Libya", "code": "LY", "phone_code": 218, "display_name": {"en_GB": "Libya", "ru_RU": "\u041b\u0438\u0432\u0438\u044f"}}, {"id": 128, "name": "Liechtenstein", "code": "LI", "phone_code": 423, "display_name": {"en_GB": "Liechtenstein", "ru_RU": "\u041b\u0438\u0445\u0442\u0435\u043d\u0448\u0442\u0435\u0439\u043d"}}, {"id": 132, "name": "Lithuania", "code": "LT", "phone_code": 370, "display_name": {"en_GB": "Lithuania", "ru_RU": "\u041b\u0438\u0442\u0432\u0430"}}, {"id": 133, "name": "Luxembourg", "code": "LU", "phone_code": 352, "display_name": {"en_GB": "Luxembourg", "ru_RU": "\u041b\u044e\u043a\u0441\u0435\u043c\u0431\u0443\u0440\u0433"}}, {"id": 147, "name": "Macau", "code": "MO", "phone_code": 853, "display_name": {"en_GB": "Macau", "ru_RU": "\u041c\u0430\u043a\u0430\u043e"}}, {"id": 143, "name": "Macedonia, the former Yugoslav Republic of", "code": "MK", "phone_code": 389, "display_name": {"en_GB": "Macedonia, the former Yugoslav Republic of", "ru_RU": "\u041c\u0430\u043a\u0435\u0434\u043e\u043d\u0438\u044f, \u0431\u044b\u0432\u0448\u0430\u044f \u0440\u0435\u0441\u043f\u0443\u0431\u043b\u0438\u043a\u0430 \u042e\u0433\u043e\u0441\u043b\u0430\u0432\u0438\u0438"}}, {"id": 141, "name": "Madagascar", "code": "MG", "phone_code": 261, "display_name": {"en_GB": "Madagascar", "ru_RU": "\u041c\u0430\u0434\u0430\u0433\u0430\u0441\u043a\u0430\u0440"}}, {"id": 155, "name": "Malawi", "code": "MW", "phone_code": 265, "display_name": {"en_GB": "Malawi", "ru_RU": "\u041c\u0430\u043b\u0430\u0432\u0438"}}, {"id": 157, "name": "Malaysia", "code": "MY", "phone_code": 60, "display_name": {"en_GB": "Malaysia", "ru_RU": "\u041c\u0430\u043b\u0430\u0439\u0437\u0438\u044f"}}, {"id": 154, "name": "Maldives", "code": "MV", "phone_code": 960, "display_name": {"en_GB": "Maldives", "ru_RU": "\u041c\u0430\u043b\u044c\u0434\u0438\u0432\u044b"}}, {"id": 144, "name": "Mali", "code": "ML", "phone_code": 223, "display_name": {"en_GB": "Mali", "ru_RU": "\u041c\u0430\u043b\u0438"}}, {"id": 152, "name": "Malta", "code": "MT", "phone_code": 356, "display_name": {"en_GB": "Malta", "ru_RU": "\u041c\u0430\u043b\u044c\u0442\u0430"}}, {"id": 142, "name": "Marshall Islands", "code": "MH", "phone_code": 692, "display_name": {"en_GB": "Marshall Islands", "ru_RU": "\u041c\u0430\u0440\u0448\u0430\u043b\u043b\u043e\u0432\u044b \u041e\u0441\u0442\u0440\u043e\u0432\u0430"}}, {"id": 149, "name": "Martinique", "code": "MQ", "phone_code": 596, "display_name": {"en_GB": "Martinique", "ru_RU": "\u041c\u0430\u0440\u0442\u0438\u043d\u0438\u043a\u0430"}}, {"id": 150, "name": "Mauritania", "code": "MR", "phone_code": 222, "display_name": {"en_GB": "Mauritania", "ru_RU": "\u041c\u0430\u0432\u0440\u0438\u0442\u0430\u043d\u0438\u044f"}}, {"id": 153, "name": "Mauritius", "code": "MU", "phone_code": 230, "display_name": {"en_GB": "Mauritius", "ru_RU": "\u041c\u0430\u0432\u0440\u0438\u043a\u0438\u0439"}}, {"id": 246, "name": "Mayotte", "code": "YT", "phone_code": 262, "display_name": {"en_GB": "Mayotte", "ru_RU": "\u041c\u0430\u0439\u043e\u0442\u0442\u0430"}}, {"id": 156, "name": "Mexico", "code": "MX", "phone_code": 52, "display_name": {"en_GB": "Mexico", "ru_RU": "\u041c\u0435\u043a\u0441\u0438\u043a\u0430"}}, {"id": 73, "name": "Micronesia", "code": "FM", "phone_code": 691, "display_name": {"en_GB": "Micronesia", "ru_RU": "\u041c\u0438\u043a\u0440\u043e\u043d\u0435\u0437\u0438\u044f"}}, {"id": 138, "name": "Moldova", "code": "MD", "phone_code": 373, "display_name": {"en_GB": "Moldova", "ru_RU": "\u041c\u043e\u043b\u0434\u0430\u0432\u0438\u044f"}}, {"id": 137, "name": "Monaco", "code": "MC", "phone_code": 377, "display_name": {"en_GB": "Monaco", "ru_RU": "\u041c\u043e\u043d\u0430\u043a\u043e"}}, {"id": 146, "name": "Mongolia", "code": "MN", "phone_code": 976, "display_name": {"en_GB": "Mongolia", "ru_RU": "\u041c\u043e\u043d\u0433\u043e\u043b\u0438\u044f"}}, {"id": 139, "name": "Montenegro", "code": "ME", "phone_code": 382, "display_name": {"en_GB": "Montenegro", "ru_RU": "\u0427\u0435\u0440\u043d\u043e\u0433\u043e\u0440\u0438\u044f"}}, {"id": 151, "name": "Montserrat", "code": "MS", "phone_code": 1664, "display_name": {"en_GB": "Montserrat", "ru_RU": "\u041c\u043e\u043d\u0441\u0435\u0440\u0440\u0430\u0442"}}, {"id": 136, "name": "Morocco", "code": "MA", "phone_code": 212, "display_name": {"en_GB": "Morocco", "ru_RU": "\u041c\u0430\u0440\u043e\u043a\u043a\u043e"}}, {"id": 158, "name": "Mozambique", "code": "MZ", "phone_code": 258, "display_name": {"en_GB": "Mozambique", "ru_RU": "\u041c\u043e\u0437\u0430\u043c\u0431\u0438\u043a"}}, {"id": 145, "name": "Myanmar", "code": "MM", "phone_code": 95, "display_name": {"en_GB": "Myanmar", "ru_RU": "\u041c\u044c\u044f\u043d\u043c\u0430"}}, {"id": 159, "name": "Namibia", "code": "NA", "phone_code": 264, "display_name": {"en_GB": "Namibia", "ru_RU": "\u041d\u0430\u043c\u0438\u0431\u0438\u044f"}}, {"id": 168, "name": "Nauru", "code": "NR", "phone_code": 674, "display_name": {"en_GB": "Nauru", "ru_RU": "\u041d\u0430\u0443\u0440\u0443"}}, {"id": 167, "name": "Nepal", "code": "NP", "phone_code": 977, "display_name": {"en_GB": "Nepal", "ru_RU": "\u041d\u0435\u043f\u0430\u043b"}}, {"id": 165, "name": "Netherlands", "code": "NL", "phone_code": 31, "display_name": {"en_GB": "Netherlands", "ru_RU": "\u041d\u0438\u0434\u0435\u0440\u043b\u0430\u043d\u0434\u044b"}}, {"id": 160, "name": "New Caledonia", "code": "NC", "phone_code": 687, "display_name": {"en_GB": "New Caledonia", "ru_RU": "\u041d\u043e\u0432\u0430\u044f \u041a\u0430\u043b\u0435\u0434\u043e\u043d\u0438\u044f"}}, {"id": 170, "name": "New Zealand", "code": "NZ", "phone_code": 64, "display_name": {"en_GB": "New Zealand", "ru_RU": "\u041d\u043e\u0432\u0430\u044f \u0417\u0435\u043b\u0430\u043d\u0434\u0438\u044f"}}, {"id": 164, "name": "Nicaragua", "code": "NI", "phone_code": 505, "display_name": {"en_GB": "Nicaragua", "ru_RU": "\u041d\u0438\u043a\u0430\u0440\u0430\u0433\u0443\u0430"}}, {"id": 161, "name": "Niger", "code": "NE", "phone_code": 227, "display_name": {"en_GB": "Niger", "ru_RU": "\u041d\u0438\u0433\u0435\u0440"}}, {"id": 163, "name": "Nigeria", "code": "NG", "phone_code": 234, "display_name": {"en_GB": "Nigeria", "ru_RU": "\u041d\u0438\u0433\u0435\u0440\u0438\u044f"}}, {"id": 169, "name": "Niue", "code": "NU", "phone_code": 683, "display_name": {"en_GB": "Niue", "ru_RU": "\u041d\u0438\u0443\u044d"}}, {"id": 162, "name": "Norfolk Island", "code": "NF", "phone_code": 672, "display_name": {"en_GB": "Norfolk Island", "ru_RU": "\u041e\u0441\u0442\u0440\u043e\u0432 \u041d\u043e\u0440\u0444\u043e\u043b\u043a"}}, {"id": 120, "name": "North Korea", "code": "KP", "phone_code": 850, "display_name": {"en_GB": "North Korea", "ru_RU": "\u0421\u0435\u0432\u0435\u0440\u043d\u0430\u044f \u041a\u043e\u0440\u0435\u044f"}}, {"id": 148, "name": "Northern Mariana Islands", "code": "MP", "phone_code": 1670, "display_name": {"en_GB": "Northern Mariana Islands", "ru_RU": "\u0421\u0435\u0432\u0435\u0440\u043d\u044b\u0435 \u041c\u0430\u0440\u0438\u0430\u043d\u0441\u043a\u0438\u0435 \u043e\u0441\u0442\u0440\u043e\u0432\u0430"}}, {"id": 166, "name": "Norway", "code": "NO", "phone_code": 47, "display_name": {"en_GB": "Norway", "ru_RU": "\u041d\u043e\u0440\u0432\u0435\u0433\u0438\u044f"}}, {"id": 171, "name": "Oman", "code": "OM", "phone_code": 968, "display_name": {"en_GB": "Oman", "ru_RU": "\u041e\u043c\u0430\u043d"}}, {"id": 177, "name": "Pakistan", "code": "PK", "phone_code": 92, "display_name": {"en_GB": "Pakistan", "ru_RU": "\u041f\u0430\u043a\u0438\u0441\u0442\u0430\u043d"}}, {"id": 184, "name": "Palau", "code": "PW", "phone_code": 680, "display_name": {"en_GB": "Palau", "ru_RU": "\u041f\u0430\u043b\u0430\u0443"}}, {"id": 172, "name": "Panama", "code": "PA", "phone_code": 507, "display_name": {"en_GB": "Panama", "ru_RU": "\u041f\u0430\u043d\u0430\u043c\u0430"}}, {"id": 175, "name": "Papua New Guinea", "code": "PG", "phone_code": 675, "display_name": {"en_GB": "Papua New Guinea", "ru_RU": "\u041f\u0430\u043f\u0443\u0430 \u041d\u043e\u0432\u0430\u044f \u0413\u0432\u0438\u043d\u0435\u044f"}}, {"id": 185, "name": "Paraguay", "code": "PY", "phone_code": 595, "display_name": {"en_GB": "Paraguay", "ru_RU": "\u041f\u0430\u0440\u0430\u0433\u0432\u0430\u0439"}}, {"id": 173, "name": "Peru", "code": "PE", "phone_code": 51, "display_name": {"en_GB": "Peru", "ru_RU": "\u041f\u0435\u0440\u0443"}}, {"id": 176, "name": "Philippines", "code": "PH", "phone_code": 63, "display_name": {"en_GB": "Philippines", "ru_RU": "\u0424\u0438\u043b\u0438\u043f\u043f\u0438\u043d\u044b"}}, {"id": 180, "name": "Pitcairn Islands", "code": "PN", "phone_code": 64, "display_name": {"en_GB": "Pitcairn Islands", "ru_RU": "\u041e\u0441\u0442\u0440\u043e\u0432\u0430 \u041f\u0438\u0442\u043a\u044d\u0440\u043d"}}, {"id": 178, "name": "Poland", "code": "PL", "phone_code": 48, "display_name": {"en_GB": "Poland", "ru_RU": "\u041f\u043e\u043b\u044c\u0448\u0430"}}, {"id": 183, "name": "Portugal", "code": "PT", "phone_code": 351, "display_name": {"en_GB": "Portugal", "ru_RU": "\u041f\u043e\u0440\u0442\u0443\u0433\u0430\u043b\u0438\u044f"}}, {"id": 181, "name": "Puerto Rico", "code": "PR", "phone_code": 1939, "display_name": {"en_GB": "Puerto Rico", "ru_RU": "\u041f\u0443\u044d\u0440\u0442\u043e-\u0420\u0438\u043a\u043e"}}, {"id": 186, "name": "Qatar", "code": "QA", "phone_code": 974, "display_name": {"en_GB": "Qatar", "ru_RU": "\u041a\u0430\u0442\u0430\u0440"}}, {"id": 188, "name": "Romania", "code": "RO", "phone_code": 40, "display_name": {"en_GB": "Romania", "ru_RU": "\u0420\u0443\u043c\u044b\u043d\u0438\u044f"}}, {"id": 190, "name": "Russian Federation", "code": "RU", "phone_code": 7, "display_name": {"en_GB": "Russian Federation", "ru_RU": "\u0420\u043e\u0441\u0441\u0438\u0439\u0441\u043a\u0430\u044f \u0424\u0435\u0434\u0435\u0440\u0430\u0446\u0438\u044f"}}, {"id": 191, "name": "Rwanda", "code": "RW", "phone_code": 250, "display_name": {"en_GB": "Rwanda", "ru_RU": "\u0420\u0443\u0430\u043d\u0434\u0430"}}, {"id": 187, "name": "R\u00e9union", "code": "RE", "phone_code": 262, "display_name": {"en_GB": "R\u00e9union", "ru_RU": "\u0420\u0435\u044e\u043d\u044c\u043e\u043d"}}, {"id": 26, "name": "Saint Barth\u00e9l\u00e9my", "code": "BL", "phone_code": 590, "display_name": {"en_GB": "Saint Barth\u00e9l\u00e9my", "ru_RU": "\u0421\u0432\u044f\u0442\u043e\u0439 \u0412\u0430\u0440\u0444\u043e\u043b\u043e\u043c\u0435\u0439"}}, {"id": 198, "name": "Saint Helena, Ascension and Tristan da Cunha", "code": "SH", "phone_code": 290, "display_name": {"en_GB": "Saint Helena, Ascension and Tristan da Cunha", "ru_RU": "\u041e\u0441\u0442\u0440\u043e\u0432\u0430 \u0421\u0432\u044f\u0442\u043e\u0439 \u0415\u043b\u0435\u043d\u044b, \u0412\u043e\u0437\u043d\u0435\u0441\u0435\u043d\u0438\u044f \u0438 \u0422\u0440\u0438\u0441\u0442\u0430\u043d-\u0434\u0430-\u041a\u0443\u043d\u044c\u044f"}}, {"id": 119, "name": "Saint Kitts and Nevis", "code": "KN", "phone_code": 1869, "display_name": {"en_GB": "Saint Kitts and Nevis", "ru_RU": "\u0421\u0435\u043d\u0442-\u041a\u0438\u0442\u0441 \u0438 \u041d\u0435\u0432\u0438\u0441"}}, {"id": 127, "name": "Saint Lucia", "code": "LC", "phone_code": 1758, "display_name": {"en_GB": "Saint Lucia", "ru_RU": "\u0421\u0435\u043d\u0442-\u041b\u044e\u0441\u0438\u044f"}}, {"id": 140, "name": "Saint Martin (French part)", "code": "MF", "phone_code": 590, "display_name": {"en_GB": "Saint Martin (French part)", "ru_RU": "\u0421\u0435\u043d-\u041c\u0430\u0440\u0442\u0435\u043d (\u0424\u0440\u0430\u043d\u0446\u0438\u044f)"}}, {"id": 179, "name": "Saint Pierre and Miquelon", "code": "PM", "phone_code": 508, "display_name": {"en_GB": "Saint Pierre and Miquelon", "ru_RU": "\u0421\u0435\u043d-\u041f\u044c\u0435\u0440 \u0438 \u041c\u0438\u043a\u0435\u043b\u043e\u043d"}}, {"id": 237, "name": "Saint Vincent and the Grenadines", "code": "VC", "phone_code": 1784, "display_name": {"en_GB": "Saint Vincent and the Grenadines", "ru_RU": "\u0421\u0435\u043d\u0442-\u0412\u0438\u043d\u0441\u0435\u043d\u0442 \u0438 \u0413\u0440\u0435\u043d\u0430\u0434\u0438\u043d\u044b"}}, {"id": 244, "name": "Samoa", "code": "WS", "phone_code": 685, "display_name": {"en_GB": "Samoa", "ru_RU": "\u0421\u0430\u043c\u043e\u0430"}}, {"id": 203, "name": "San Marino", "code": "SM", "phone_code": 378, "display_name": {"en_GB": "San Marino", "ru_RU": "\u0421\u0430\u043d-\u041c\u0430\u0440\u0438\u043d\u043e"}}, {"id": 192, "name": "Saudi Arabia", "code": "SA", "phone_code": 966, "display_name": {"en_GB": "Saudi Arabia", "ru_RU": "\u0421\u0430\u0443\u0434\u043e\u0432\u0441\u043a\u0430\u044f \u0410\u0440\u0430\u0432\u0438\u044f"}}, {"id": 204, "name": "Senegal", "code": "SN", "phone_code": 221, "display_name": {"en_GB": "Senegal", "ru_RU": "\u0421\u0435\u043d\u0435\u0433\u0430\u043b"}}, {"id": 189, "name": "Serbia", "code": "RS", "phone_code": 381, "display_name": {"en_GB": "Serbia", "ru_RU": "\u0421\u0435\u0440\u0431\u0438\u044f"}}, {"id": 194, "name": "Seychelles", "code": "SC", "phone_code": 248, "display_name": {"en_GB": "Seychelles", "ru_RU": "\u0421\u0435\u0439\u0448\u0435\u043b\u044c\u0441\u043a\u0438\u0435 \u043e\u0441\u0442\u0440\u043e\u0432\u0430"}}, {"id": 202, "name": "Sierra Leone", "code": "SL", "phone_code": 232, "display_name": {"en_GB": "Sierra Leone", "ru_RU": "\u0421\u044c\u0435\u0440\u0440\u0430-\u041b\u0435\u043e\u043d\u0435"}}, {"id": 197, "name": "Singapore", "code": "SG", "phone_code": 65, "display_name": {"en_GB": "Singapore", "ru_RU": "\u0421\u0438\u043d\u0433\u0430\u043f\u0443\u0440"}}, {"id": 210, "name": "Sint Maarten (Dutch part)", "code": "SX", "phone_code": 1721, "display_name": {"en_GB": "Sint Maarten (Dutch part)", "ru_RU": "\u0421\u0438\u043d\u0442-\u041c\u0430\u0440\u0442\u0435\u043d (\u0433\u043e\u043b\u043b\u0430\u043d\u0434\u0441\u043a\u0430\u044f \u0447\u0430\u0441\u0442\u044c)"}}, {"id": 201, "name": "Slovakia", "code": "SK", "phone_code": 421, "display_name": {"en_GB": "Slovakia", "ru_RU": "\u0421\u043b\u043e\u0432\u0430\u043a\u0438\u044f"}}, {"id": 199, "name": "Slovenia", "code": "SI", "phone_code": 386, "display_name": {"en_GB": "Slovenia", "ru_RU": "\u0421\u043b\u043e\u0432\u0435\u043d\u0438\u044f"}}, {"id": 193, "name": "Solomon Islands", "code": "SB", "phone_code": 677, "display_name": {"en_GB": "Solomon Islands", "ru_RU": "\u0421\u043e\u043b\u043e\u043c\u043e\u043d\u043e\u0432\u044b \u041e\u0441\u0442\u0440\u043e\u0432\u0430"}}, {"id": 205, "name": "Somalia", "code": "SO", "phone_code": 252, "display_name": {"en_GB": "Somalia", "ru_RU": "\u0421\u043e\u043c\u0430\u043b\u0438"}}, {"id": 247, "name": "South Africa", "code": "ZA", "phone_code": 27, "display_name": {"en_GB": "South Africa", "ru_RU": "\u042e\u0436\u043d\u0430\u044f \u0410\u0444\u0440\u0438\u043a\u0430"}}, {"id": 89, "name": "South Georgia and the South Sandwich Islands", "code": "GS", "phone_code": 500, "display_name": {"en_GB": "South Georgia and the South Sandwich Islands", "ru_RU": "\u042e\u0436\u043d\u0430\u044f \u0414\u0436\u043e\u0440\u0434\u0436\u0438\u044f \u0438 \u042e\u0436\u043d\u044b\u0435 \u0421\u0430\u043d\u0434\u0432\u0438\u0447\u0435\u0432\u044b \u041e\u0441\u0442\u0440\u043e\u0432\u0430"}}, {"id": 121, "name": "South Korea", "code": "KR", "phone_code": 82, "display_name": {"en_GB": "South Korea", "ru_RU": "\u042e\u0436\u043d\u0430\u044f \u041a\u043e\u0440\u0435\u044f"}}, {"id": 207, "name": "South Sudan", "code": "SS", "phone_code": 211, "display_name": {"en_GB": "South Sudan", "ru_RU": "\u042e\u0436\u043d\u044b\u0439 \u0421\u0443\u0434\u0430\u043d"}}, {"id": 68, "name": "Spain", "code": "ES", "phone_code": 34, "display_name": {"en_GB": "Spain", "ru_RU": "\u0418\u0441\u043f\u0430\u043d\u0438\u044f"}}, {"id": 129, "name": "Sri Lanka", "code": "LK", "phone_code": 94, "display_name": {"en_GB": "Sri Lanka", "ru_RU": "\ufeff\u0428\u0440\u0438-\u041b\u0430\u043d\u043a\u0430"}}, {"id": 182, "name": "State of Palestine", "code": "PS", "phone_code": 970, "display_name": {"en_GB": "State of Palestine", "ru_RU": "\u0413\u043e\u0441\u0443\u0434\u0430\u0440\u0441\u0442\u0432\u043e \u041f\u0430\u043b\u0435\u0441\u0442\u0438\u043d\u0430"}}, {"id": 195, "name": "Sudan", "code": "SD", "phone_code": 249, "display_name": {"en_GB": "Sudan", "ru_RU": "\u0421\u0443\u0434\u0430\u043d"}}, {"id": 206, "name": "Suriname", "code": "SR", "phone_code": 597, "display_name": {"en_GB": "Suriname", "ru_RU": "\u0421\u0443\u0440\u0438\u043d\u0430\u043c"}}, {"id": 200, "name": "Svalbard and Jan Mayen", "code": "SJ", "phone_code": 47, "display_name": {"en_GB": "Svalbard and Jan Mayen", "ru_RU": "\u0428\u043f\u0438\u0446\u0431\u0435\u0440\u0433\u0435\u043d \u0438 \u042f\u043d-\u041c\u0430\u0439\u0435\u043d"}}, {"id": 212, "name": "Swaziland", "code": "SZ", "phone_code": 268, "display_name": {"en_GB": "Swaziland", "ru_RU": "\u0421\u0432\u0430\u0437\u0438\u043b\u0435\u043d\u0434"}}, {"id": 196, "name": "Sweden", "code": "SE", "phone_code": 46, "display_name": {"en_GB": "Sweden", "ru_RU": "\u0428\u0432\u0435\u0446\u0438\u044f"}}, {"id": 43, "name": "Switzerland", "code": "CH", "phone_code": 41, "display_name": {"en_GB": "Switzerland", "ru_RU": "\u0428\u0432\u0435\u0439\u0446\u0430\u0440\u0438\u044f"}}, {"id": 211, "name": "Syria", "code": "SY", "phone_code": 963, "display_name": {"en_GB": "Syria", "ru_RU": "\u0421\u0438\u0440\u0438\u044f"}}, {"id": 208, "name": "S\u00e3o Tom\u00e9 and Pr\u00edncipe", "code": "ST", "phone_code": 239, "display_name": {"en_GB": "S\u00e3o Tom\u00e9 and Pr\u00edncipe", "ru_RU": "\u0421\u0430\u043d-\u0422\u043e\u043c\u0435 \u0438 \u041f\u0440\u0438\u043d\u0441\u0438\u043f\u0438"}}, {"id": 227, "name": "Taiwan", "code": "TW", "phone_code": 886, "display_name": {"en_GB": "Taiwan", "ru_RU": "\u0422\u0430\u0439\u0432\u0430\u043d\u044c"}}, {"id": 218, "name": "Tajikistan", "code": "TJ", "phone_code": 992, "display_name": {"en_GB": "Tajikistan", "ru_RU": "\u0422\u0430\u0434\u0436\u0438\u043a\u0438\u0441\u0442\u0430\u043d"}}, {"id": 228, "name": "Tanzania", "code": "TZ", "phone_code": 255, "display_name": {"en_GB": "Tanzania", "ru_RU": "\u0422\u0430\u043d\u0437\u0430\u043d\u0438\u044f"}}, {"id": 217, "name": "Thailand", "code": "TH", "phone_code": 66, "display_name": {"en_GB": "Thailand", "ru_RU": "\u0422\u0430\u0438\u043b\u0430\u043d\u0434"}}, {"id": 223, "name": "Timor-Leste", "code": "TL", "phone_code": 670, "display_name": {"en_GB": "Timor-Leste", "ru_RU": "\u0412\u043e\u0441\u0442\u043e\u0447\u043d\u044b\u0439 \u0422\u0438\u043c\u043e\u0440"}}, {"id": 216, "name": "Togo", "code": "TG", "phone_code": 228, "display_name": {"en_GB": "Togo", "ru_RU": "\u0422\u043e\u0433\u043e"}}, {"id": 219, "name": "Tokelau", "code": "TK", "phone_code": 690, "display_name": {"en_GB": "Tokelau", "ru_RU": "\u0422\u043e\u043a\u0435\u043b\u0430\u0443"}}, {"id": 222, "name": "Tonga", "code": "TO", "phone_code": 676, "display_name": {"en_GB": "Tonga", "ru_RU": "\u0422\u043e\u043d\u0433\u0430"}}, {"id": 225, "name": "Trinidad and Tobago", "code": "TT", "phone_code": 1868, "display_name": {"en_GB": "Trinidad and Tobago", "ru_RU": "\u0422\u0440\u0438\u043d\u0438\u0434\u0430\u0434 \u0438 \u0422\u043e\u0431\u0430\u0433\u043e"}}, {"id": 221, "name": "Tunisia", "code": "TN", "phone_code": 216, "display_name": {"en_GB": "Tunisia", "ru_RU": "\u0422\u0443\u043d\u0438\u0441"}}, {"id": 224, "name": "Turkey", "code": "TR", "phone_code": 90, "display_name": {"en_GB": "Turkey", "ru_RU": "\u0422\u0443\u0440\u0446\u0438\u044f"}}, {"id": 220, "name": "Turkmenistan", "code": "TM", "phone_code": 993, "display_name": {"en_GB": "Turkmenistan", "ru_RU": "\u0422\u0443\u0440\u043a\u043c\u0435\u043d\u0438\u0441\u0442\u0430\u043d"}}, {"id": 213, "name": "Turks and Caicos Islands", "code": "TC", "phone_code": 1649, "display_name": {"en_GB": "Turks and Caicos Islands", "ru_RU": "\u041e\u0441\u0442\u0440\u043e\u0432\u0430 \u0422\u0451\u0440\u043a\u0441 \u0438 \u041a\u0430\u0439\u043a\u043e\u0441"}}, {"id": 226, "name": "Tuvalu", "code": "TV", "phone_code": 688, "display_name": {"en_GB": "Tuvalu", "ru_RU": "\u0422\u0443\u0432\u0430\u043b\u0443"}}, {"id": 232, "name": "USA Minor Outlying Islands", "code": "UM", "phone_code": 699, "display_name": {"en_GB": "USA Minor Outlying Islands", "ru_RU": "\u0412\u043d\u0435\u0448\u043d\u0438\u0435 \u043c\u0430\u043b\u044b\u0435 \u043e\u0441\u0442\u0440\u043e\u0432\u0430 \u0421\u0428\u0410"}}, {"id": 230, "name": "Uganda", "code": "UG", "phone_code": 256, "display_name": {"en_GB": "Uganda", "ru_RU": "\u0423\u0433\u0430\u043d\u0434\u0430"}}, {"id": 229, "name": "Ukraine", "code": "UA", "phone_code": 380, "display_name": {"en_GB": "Ukraine", "ru_RU": "\u0423\u043a\u0440\u0430\u0438\u043d\u0430"}}, {"id": 2, "name": "United Arab Emirates", "code": "AE", "phone_code": 971, "display_name": {"en_GB": "United Arab Emirates", "ru_RU": "\u041e\u0431\u044a\u0435\u0434\u0438\u043d\u0451\u043d\u043d\u044b\u0435 \u0410\u0440\u0430\u0431\u0441\u043a\u0438\u0435 \u042d\u043c\u0438\u0440\u0430\u0442\u044b"}}, {"id": 231, "name": "United Kingdom", "code": "GB", "phone_code": 44, "display_name": {"en_GB": "United Kingdom", "ru_RU": "\u0412\u0435\u043b\u0438\u043a\u043e\u0431\u0440\u0438\u0442\u0430\u043d\u0438\u044f"}}, {"id": 233, "name": "United States", "code": "US", "phone_code": 1, "display_name": {"en_GB": "United States", "ru_RU": "\u0421\u043e\u0435\u0434\u0438\u043d\u0451\u043d\u043d\u044b\u0435 \u0428\u0442\u0430\u0442\u044b \u0410\u043c\u0435\u0440\u0438\u043a\u0438"}}, {"id": 234, "name": "Uruguay", "code": "UY", "phone_code": 598, "display_name": {"en_GB": "Uruguay", "ru_RU": "\u0423\u0440\u0443\u0433\u0432\u0430\u0439"}}, {"id": 235, "name": "Uzbekistan", "code": "UZ", "phone_code": 998, "display_name": {"en_GB": "Uzbekistan", "ru_RU": "\u0423\u0437\u0431\u0435\u043a\u0438\u0441\u0442\u0430\u043d"}}, {"id": 242, "name": "Vanuatu", "code": "VU", "phone_code": 678, "display_name": {"en_GB": "Vanuatu", "ru_RU": "\u0412\u0430\u043d\u0443\u0430\u0442\u0443"}}, {"id": 238, "name": "Venezuela", "code": "VE", "phone_code": 58, "display_name": {"en_GB": "Venezuela", "ru_RU": "\u0412\u0435\u043d\u0435\u0441\u0443\u044d\u043b\u0430"}}, {"id": 241, "name": "Vietnam", "code": "VN", "phone_code": 84, "display_name": {"en_GB": "Vietnam", "ru_RU": "\u0412\u044c\u0435\u0442\u043d\u0430\u043c"}}, {"id": 239, "name": "Virgin Islands (British)", "code": "VG", "phone_code": 1284, "display_name": {"en_GB": "Virgin Islands (British)", "ru_RU": "\u0412\u0438\u0440\u0433\u0438\u043d\u0441\u043a\u0438\u0435 \u043e\u0441\u0442\u0440\u043e\u0432\u0430 (\u0412\u0435\u043b\u0438\u043a\u043e\u0431\u0440\u0438\u0442\u0430\u043d\u0438\u044f)"}}, {"id": 240, "name": "Virgin Islands (USA)", "code": "VI", "phone_code": 1340, "display_name": {"en_GB": "Virgin Islands (USA)", "ru_RU": "\u0412\u0438\u0440\u0433\u0438\u043d\u0441\u043a\u0438\u0435 \u043e\u0441\u0442\u0440\u043e\u0432\u0430 (\u0421\u0428\u0410)"}}, {"id": 243, "name": "Wallis and Futuna", "code": "WF", "phone_code": 681, "display_name": {"en_GB": "Wallis and Futuna", "ru_RU": "\u0423\u043e\u043b\u043b\u0438\u0441 \u0438 \u0424\u0443\u0442\u0443\u043d\u0430"}}, {"id": 66, "name": "Western Sahara", "code": "EH", "phone_code": 212, "display_name": {"en_GB": "Western Sahara", "ru_RU": "\u0417\u0430\u043f\u0430\u0434\u043d\u0430\u044f \u0421\u0430\u0445\u0430\u0440\u0430"}}, {"id": 245, "name": "Yemen", "code": "YE", "phone_code": 967, "display_name": {"en_GB": "Yemen", "ru_RU": "\u0419\u0435\u043c\u0435\u043d"}}, {"id": 248, "name": "Zambia", "code": "ZM", "phone_code": 260, "display_name": {"en_GB": "Zambia", "ru_RU": "\u0417\u0430\u043c\u0431\u0438\u044f"}}, {"id": 249, "name": "Zimbabwe", "code": "ZW", "phone_code": 263, "display_name": {"en_GB": "Zimbabwe", "ru_RU": "\u0417\u0438\u043c\u0431\u0430\u0431\u0432\u0435"}}, {"id": 15, "name": "\u00c5land Islands", "code": "AX", "phone_code": 358, "display_name": {"en_GB": "\u00c5land Islands", "ru_RU": "\u0410\u043b\u0430\u043d\u0434\u0441\u043a\u0438\u0435 \u043e\u0441\u0442\u0440\u043e\u0432\u0430"}}]

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function () {
            defer(method)
        }, 50);
    }
}
var jspgremstatecoll=[];
var jspgremcountrycoll=[];
defer(jspgmsatsangdocumentready);
function jspgmsatsangdocumentready(){
    $(document).ready(function () {
        jsgstngfmwtsapp();
        jspgmregfmpackagecost();
        jspgmreginitfunction();
        jsrendercountries();
        jspgmregfmselectstates();
        jspgmregfmcountryandcodechange();
        jspgmregfmcountrypincode();
        jspgmregfmcountryphonecode();
        jspgmregfmwatsappcountryphonecode();
        //  jstccheck();
        jspgmregsetlangpref();//onchangeofpournamitz();jspgmregfmpackagecost();
        //        jsgstngfmloadaddressforpincode()
        $("form :input").attr("autocomplete", "off");
    });
}

function jspgmregsetlangpref() {
    try {
        var paramsvars = new URLSearchParams(location.search);
        var langvar = paramsvars.get('language').toUpperCase()
        $("select#jsgstngfmpreflang option").each(function() {
            if ($(this).text().toUpperCase() == langvar) {
                $("select#jsgstngfmpreflang").val($(this).val());
                return false;
            }
            
        });
    } catch (e) {}
}

function jspgmreginitfunction() {
    var initObjvar;
    var addobjvar;
    try{
        jspgremstatecoll = $.parseJSON($('#jspgremstatecollpour').val());
        $('#jspgremstatecollpour').val('[]');
    }
    catch(exv){}
    //    $('#jsgstngfmstate option').each(function(ivar,jvar){
    //       var objvar ={
    //        "valueattr":$(jvar).attr("value"),
    //        "statenameattr":$(jvar).attr("statename"),
    //        "countryidattr":$(jvar).attr("countryid")
    //        };
    //        jspgremstatecoll.push(objvar);
    //    });
}
//function jstccheck()
//{
//$('#terms_and_conditions').click(function(){
//    if($(this).is(':checked')){
//        $('#submit_button').attr("disabled", false);
//    } else{
//        $('#submit_button').attr("disabled", true);
//    }
//});
//}

function jsrendercountries() {
    
    var sso_country= $('#selected_sso_country').val().trim();
    var form_lang = $('#lang_code').val().trim();
    var idvar = 0;
    $.each(country_var_test, function(ivar,jvar){
        var optdat={
            "value": jvar['id'],
            "text": jvar["display_name"]["en_GB"],
            "ccode":jvar["code"],
            "ccatr":jvar["code"],
        }
        
        if (form_lang in jvar['display_name']){
            console.log("This is sparta")
            optdat['text'] = jvar['display_name'][form_lang]
        }
        
        var ct_code_vals = {
            "value": jvar["phone_code"],
            "countryid": jvar['id'],
            "text": jvar["code"] + " |+ " + jvar["phone_code"]
        }
        
        if (ivar == 0 && !sso_country){
            $('#jsgstngfmcountry').attr("disabled", false)
            $('#jsgstngfmcountrycode').attr("disabled", false)
            $('#jsgstngfmwtsappcountrycode').attr("disabled", false)
            optdat["selected"]="selected";
            ct_code_vals["selected"]="selected";
        }else if (sso_country && sso_country == optdat['ccode']){
            $('#jsgstngfmcountry').attr("disabled", false)
            $('#jsgstngfmcountrycode').attr("disabled", false)
            $('#jsgstngfmwtsappcountrycode').attr("disabled", false)
            optdat["selected"]="selected";
            ct_code_vals["selected"]="selected";
        }
        
        $('<option/>', optdat).appendTo('#jsgstngfmcountry');
        $('<option/>', ct_code_vals).appendTo('#jsgstngfmcountrycode');
        $('<option/>', ct_code_vals).appendTo('#jsgstngfmwtsappcountrycode');
        
        
    });
    
    
}


function jspgmregfmselectstates() {
    var state = document.getElementById("jsgstngfmstate");
    var countryname = document.getElementById("jsgstngfmcountry");
    var countryid = countryname.value;
    var idvar = 0;
    countryid = countryid + '';
    $('#jsgstngfmstate [countryid="0"]').attr("disabled", true)
    $('#jsgstngfmstate option[countryid="0"]').hide();
    $('#jsgstngfmstate :not([countryid="0"])').remove();
    $.each(jspgremstatecoll, function(ivar,jvar){
        try{
            var ctryidattrstrvar = jvar["countryidattr"] + '';
            var valueidattrstrvar = jvar["valueattr"] + '';
            
            if(countryid == ctryidattrstrvar){
                var optdat ={}
                
                var optdat={
                    'value': valueidattrstrvar,
                    'text': jvar["statenameattr"],
                    "statename":jvar["statenameattr"],
                    "countryid":ctryidattrstrvar
                }
                if (idvar == 0){
                    $('#jsgstngfmstate').attr("disabled", false)
                    //        $('#jsgstngfmstate option[countryid="0"]').hide();
                    idvar = ivar;
                    optdat["selected"]="selected";
                }
                $('<option/>', optdat).appendTo('#jsgstngfmstate');
            }
        }
        catch(ex){}
    });
    if (idvar == 0) {
        $('#jsgstngfmstate [countryid="0"]').attr("disabled", false);
        //       $('#jsgstngfmstate option[countryid="0"]').show();
        $('#jsgstngfmstate').val("")
        $('#jsgstngfmstate option[countryid="0"]').attr("disabled", true)
        $('#jsgstngfmstate').attr("disabled", true)
    }
    
    
    
    //    var idvar = 0;
    //    Array.from(state.options).forEach(item => {
    //        var trend = item.getAttribute("countryid");
    //        if (trend == countryid) {
    //            item.style.display = "block";
    //            item.disabled = false;
    //            if (idvar == 0)
    //                idvar = item.index;
    //        } else
    //            {
    //            item.style.display = "none";
    //            item.disabled = true;
    //            }
    //    });
    //    if (idvar != 0) {
    //        state.options.selectedIndex = idvar;
    //        state.disabled = false;
    //        state.height = 35;
    //    }
    //     else if (idvar == 0) {
    //       state.options.selectedIndex = 0;
    //        state.disabled = true;
    //    }
    onchangeofcountryofresidence();
}

function jsgstngfmwtsapp(){
    $('#jsgstngfmwtsappyes').change(function () {
        if ($(this).is(":checked"))
        {
            $('.jsgstngfmwtsappcls').css("display","none")
            $(this).val('YES')
            //$('#jsgstngfmwtsappcountrycode').prop('required',  false)
            //$('#jsgstngfmwtsappphone').prop('required',  false)
            
        }
    });
    $('#jsgstngfmwtsappno').change(function () {
        if ($(this).is(":checked"))
        {
            $('.jsgstngfmwtsappcls').css("display","flex")
            $(this).val('NO')
            //$('#jsgstngfmwtsappcountrycode').prop('required',  true)
            //$('#jsgstngfmwtsappphone').prop('required',  true)
        }
    });
}
// check the country of residence and redirect to S1 /S2

function chck_redirect_url(){
    
    var is_proceed =true;
    
    var is_s1= $('#is_ishagam_one').val();
    
    var jsgstngfmcountry= $('#jsgstngfmcountry').val();
    
    var isha_one_url= $('#is_ishagam_one_url').val();
    
    var isha_two_url= $('#is_ishagam_two_url').val();
    
    var curr_url = window.location.href;
    
    try {
        
        
        if(is_s1=='ishangamone' && jsgstngfmcountry!=104){
            
            var split_url =(curr_url).split('?');
            
            var added_redirec_parm = split_url[1]+"&iscorredirect=True"
            
            var r_url = "/onlinesatsang/registrationform?"+(added_redirec_parm)
            
            var r_url =(isha_two_url)+""+(r_url);
            
            console.log(r_url);
            
            window.location.replace(r_url);
            
            
        }
        
        if(is_s1!='ishangamone' && jsgstngfmcountry==104){
            
            var split_url =(curr_url).split('?');
            
            var added_redirec_parm = split_url[1]+"&iscorredirect=True"
            
            var r_url = "/onlinesatsang/registrationform?"+(added_redirec_parm)
            
            var r_url =(isha_one_url)+""+(r_url);
            
            window.location.replace(r_url);
        }
    }
    catch(err){
        console.log(err)
    }
}

function jspgmregfmcountryandcodechange(){
    $('#jsgstngfmcountry').change(function () {
        chck_redirect_url();
        jspgmregfmcountrypincode();
    });
    
    $('#jsgstngfmcountrycode').change(function () {
        jspgmregfmcountryphonecode()
    });
    
    $('#jsgstngfmwtsappcountrycode').change(function () {
        jspgmregfmwatsappcountryphonecode()
    });
}

function jspgmregfmwatsappcountryphonecode(){
    if( $('#jsgstngfmwtsappcountrycode').val() == '91'){
        $('#jsgstngfmwtsappphone').attr('maxlength', '10')
        $('#jsgstngfmwtsappphone').attr('minlength', '10')
        $('#jsgstngfmwtsappphone').attr('pattern', '[0-9]+')
    }
    else{
        $('#jsgstngfmwtsappphone').attr('maxlength', '15')
        $('#jsgstngfmwtsappphone').attr('minlength', '5')
        $('#jsgstngfmwtsappphone').attr('pattern', '[1-9]{1}[0-9]+')
    }
}

function jspgmregfmcountrypincode(){
    if($('#jsgstngfmcountry').val() == '104'){
        $('#jsgstngfmpincode').attr('maxlength', '6')
        $('#jsgstngfmpincode').attr('minlength', '6')
        $('#jsgstngfmpincode').attr('pattern', '[1-9]{1}[0-9]+')
    }
    else if($('#jsgstngfmcountry').val() == '233' ){
        $('#jsgstngfmpincode').attr('maxlength', '5');
        $('#jsgstngfmpincode').attr('minlength', '5');
        $('#jsgstngfmpincode').attr('pattern', '[0-9]{1}[0-9]+');
    }
    else{
        $('#jsgstngfmpincode').attr('maxlength', '10')
        $('#jsgstngfmpincode').attr('minlength', '1')
        $('#jsgstngfmpincode').attr('pattern', '[A-Za-z0-9]*')
    }
}

function jspgmregfmcountryphonecode(){
    if( $('#jsgstngfmcountrycode').val() == '91'){
        $('#jsgstngfmphone').attr('maxlength', '10')
        $('#jsgstngfmphone').attr('minlength', '10')
        $('#jsgstngfmphone').attr('pattern', '[0-9]+')
    }
    else{
        $('#jsgstngfmphone').attr('maxlength', '15')
        $('#jsgstngfmphone').attr('minlength', '5')
        $('#jsgstngfmphone').attr('pattern', '[1-9]{1}[0-9]+')
    }
    $('#jsgstngfmphone').prop('required',  true)
}




function jspgmregfmvalidateiswatsapp(){
    if(!$('#jsgstngfmwtsappyes').is(':checked') && !$('#jsgstngfmwtsappno').is(':checked')){
        //alert($('#jsgstngfmwhatsappwarning'.val()))
        return false;
    }
    return true;
    
}
function jspgmregfmpackagecost() {
    $('#timezone_preferred').change(onchangeofpournamitz);
}
function onchangeofpournamitz()
{
    try{
        var packageselection = document.getElementById("timezone_preferred");
        var optselected = packageselection.options[packageselection.selectedIndex].value;
        if(optselected == 'None') return;
        var programfee = document.getElementById("language_preferred");
        
        Array.from(programfee.options).forEach(item => {
            var trend = item.getAttribute("time_zone_id");
            var displayname = item.getAttribute("pckgdisplayname");
            if (trend == optselected)
            {item.style.display = "block";
            item.disabled = false;
            item.selected = 'selected'}
            else
            {item.style.display = "none";
            item.disabled = true;}
        });
        $("#language_preferred option[value='1'][time_zone_id='"+ optselected +"']").prop("selected", true);
    }
    catch(e){debugger;console.log('error')}
}


function onchangeofcountryofresidence()
{
    try{
        var packageselection = document.getElementById("jsgstngfmcountry");
        var optselected = packageselection.options[packageselection.selectedIndex].value;
        //        var region_idofcountry = $("#jsgstngfmcountry option:selected").attr('region_id')
        //debugger;
        if(optselected == 'None') return;
        var programfee = document.getElementById("timezone_preferred");
        var idvar = 0;
        
        //    Array.from(programfee.options).forEach(item => {
        //        //console.log(item.value)
        //        //console.log(item.text)
        //        var trend = item.getAttribute("region_id");
        //        var displayname = item.getAttribute("pckgdisplayname");
        //        if (trend==-1)
        //            {
        //                item.style.display = "block";
        //                item.disabled = false;
        //                item.selected = 'selected'
        //            }
        //        else if (trend == region_idofcountry)
        //            {item.style.display = "block";
        //            item.disabled = false;
        //            if (idvar == 0)
        //            idvar = item.index;
        //            }
        //        else
        //            {item.style.display = "none";
        //            item.disabled = true;
        //            }
        //    });
        //    if (idvar != 0) {
        //        programfee.options.selectedIndex = idvar;
        //        //state.disabled = false;
        //        //state.height = 35;
        //    }
        //     else if (idvar == 0) {
        //       programfee.options.selectedIndex = 0;
        //        //state.disabled = true;
        //    }
        $('#timezone_preferred').trigger('change');
    }
    catch(e){debugger;console.log('error');}
}
function validateemptystring(element){
    return $.trim($(element).val()) == "";
}

$(document).ready(function(){

    
    var spinner = $('#living_loader');
    spinner.hide();
    $('#fmf_reg_form_submit').submit(function(e) {
        $('#fmf_reg_form_submit').css({"display":"none"});
        $('#living_loader').show();
        if(validateemptystring("#jsgstngfmfirstname")){
            alert($('#jsgstngfmfirstnamewarning').val())
            return false;
        }
        //        if(validateemptystring("#jsgstngfmlastname")){
        //             alert($('#jsgstngfmlastnamewarning').val())
        //             return false;
        //             }
        return true;
        //        setTimeout(function() {
        //               $('#fmf_reg_form_submit').css({"display":"block"});
        //               spinner.hide();
        //
        //            return true;
        //          },6000);
    });
    // $('#jsgstngfmpincode').focusout(async () => {
    //     //let all_countries = await fetch('https://cdi-gateway.isha.in/contactinfovalidation/api/countries');
    //     //let all_countries_json = await all_countries.json()
    //     let selected_country = $('#jsgstngfmcountry option:selected').text();
    //     //let selected_country_iso2_code = _.filter(all_countries_json, (country) => {
    //     //    return country.name === selected_country;
    //     //})[0].iso2;
    //     let selected_country_iso2_code = $('#jsgstngfmcountry option:selected').attr("ccode");
        
    //     let pincode = $('#jsgstngfmpincode').val();
    //     pincode = pincode.trim();
    //     if( pincode !="")
    //     {
    //         let address_details = await fetch(`https://cdi-gateway.isha.in/contactinfovalidation/api/countries/${selected_country_iso2_code}/pincodes/${pincode}`)
    //         let address_details_json = await address_details.json()
            
    //         if (! _.isEmpty(address_details_json)) {
    //             //let all_states = document.querySelectorAll('#stateid option')
    //             //let state = _.filter(all_states, (a) => a.innerText == address_details_json.state)[0]
    //             let statevar = _.filter(jspgremstatecoll, (a) => a.statenameattr == address_details_json.state)[0]
    //             //state.setAttribute('selected', 'selected')
    //             $('#jsgstngfmstate').val(statevar["valueattr"]);
                
    //             $('#jsgstngfmcity').val(address_details_json.defaultcity)
    //             //                $('#district').val(address_details_json.defaultcity)
    //         }
    //     }
    // })
})
