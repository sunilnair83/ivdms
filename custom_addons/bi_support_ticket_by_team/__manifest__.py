# -*- coding: utf-8 -*-
# Part of Browseinfo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Helpdesk Support Ticket By Team in Odoo',
    'version': '13.0.0.1',
    'category' : 'Website',
    'summary': 'App Website Helpdesk Support Ticket By Team Helpdesk ticket by Team support ticket by team support ticket helpdesk team support ticket issue by support team support team issue ustomer Helpdesk Support Ticket website support ticket Website Help Desk Support',
    'description': """Helpdesk Support Ticket By Team
	
	Helpdesk Support Ticket by Helpdesk Team
	support ticket by team 
	team support ticket 
	helpdesk team support ticket
	issue by support team 
	support team issue 
	
	
	
	""",
    'author': 'BrowseInfo',
    'website' : 'https://www.browseinfo.in',
    'price': 39,
    'currency': 'EUR',
    'depends': ['base','portal','website',
                # 'website_sale',
                'bi_website_support_ticket'],
    'data':[
        "views/support_team_templates.xml",
        "views/support_team_views.xml",
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'live_test_url':'https://youtu.be/qnk0eHRLKOo',
    'images':['static/description/Banner.png'],
}
