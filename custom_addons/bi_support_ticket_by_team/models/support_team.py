# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

import time
from datetime import datetime
from datetime import date, datetime
from odoo.exceptions import Warning, UserError
from odoo import models, fields, exceptions, api, SUPERUSER_ID,  _
from odoo.tools.translate import html_translate

class Website(models.Model):
    _inherit = "support.team"

    team_image = fields.Binary('Team Image', attachment=True)
    about_team = fields.Html('About Team', translate=html_translate)
    
class Website(models.Model):
    _inherit = "website"
    
    def get_helpdesk_team(self):            
        support_team_ids = self.env['support.team'].sudo().search([])
        return support_team_ids
        
