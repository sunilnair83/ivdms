# -*- encoding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'SP Call Queue',
    'version': '3.0',
    'description': """
        Create calling queues for different call types
    """,
    'summary': 'Create call_queues and analyze answers',
    'website': 'https://www.odoo.com/page/call_queue',
    'depends': [
        'auth_signup',
        'mail',
        'sms',
        'isha_sp_registration',
    ],
    'data': [
        'data/data.xml',
        'data/mail_template_data.xml',
        'data/ir_cron.xml',
        'security/call_queue_security.xml',
        'security/ir.model.access.csv',
        'views/call_queue_menus.xml',
        'views/call_queue_call_queue_views.xml',
        'views/call_queue_caller_views.xml',
        'views/call_queue_type_views.xml',
        'views/call_queue_user_views.xml',
        'views/call_queue_question_views.xml',
        'views/registration_override.xml',
        'views/call_queue_call_history.xml',
        'wizard/call_queue_allocate_views.xml',
        'wizard/call_queue_comment_views.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'application': True,
    'sequence': 105,
}
