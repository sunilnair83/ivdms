import logging

from odoo import fields, models

_logger = logging.getLogger(__name__)


class CallQueueType(models.Model):
    _name = 'call_queue.type'
    _description = 'Call Queue Type'

    _sql_constraints = [
        ('obj_name_uniq', 'unique (model_id)', 'Each model must be unique!'),
    ]

    name = fields.Char(required=True)
    active = fields.Boolean("Active", default=True)
    call_queue_ids = fields.One2many('call_queue.call_queue', 'call_type_id')
    model_id = fields.Many2one('ir.model', string='Model', required=True,
                               default=lambda self: self.env['ir.model'].sudo().search(
                                   [('model', '=', 'sp.registration')]))
