# -*- encoding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import call_history
from . import call_queue_caller
from . import call_queue_question
from . import call_queue_type
from . import call_queue_user
from . import call_queue_call_queue
from . import required_fields
from . import res_partner
from . import sp_registration
from . import sp_appointment
