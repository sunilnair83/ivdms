import logging

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class SpRegistration(models.Model):
    _inherit = 'sp.registration'

    def write(self, vals):
        if self._context.get('call_queue', False):
            call_record = self.env['call_queue.user_input'].search([('registration_id', '=', self.id),
                                                                    ('id', '=', self._context.get('userinput_id'))])
            if call_record:
                if self._context.get('did_not_pick', False):
                    call_record.state = 'did_not_pick'
                # elif self.sp_pre_reg_status == self.env.ref("isha_sp_registration.sp_pre_reg_status_new_no_answer") and \
                elif self.sp_pre_reg_status == self.env.ref("isha_sp_registration.sp_pre_reg_status_new") and \
                    call_record.registration_id.sp_call_status == \
                    self.env.ref("isha_sp_registration.sp_call_status_no_answer"):
                    call_record.state = 'did_not_pick'
                else:
                    call_record.state = 'done'
        return super(SpRegistration, self).write(vals)

    @api.model
    def _generate_order_by_inner(self, alias, order_spec, query, reverse_direction=False, seen=None):
        if self._context.get('skip_order_term_validation'):
            order_by_elements = []
            for order_part in order_spec.split(','):
                order_by_elements.append('%s' % order_part)
            return order_by_elements
        else:
            return super(SpRegistration, self)._generate_order_by_inner(alias, order_spec, query, reverse_direction,
                                                                        seen)

    def _add_domain(self):
        return [('state', 'in', ['done', 'did_not_pick'])]

    # call queue
    contact_call_queue_id_fkey = fields.One2many('call_queue.user_input', 'registration_id',
                                                 'Call Queue Contact', domain=_add_domain)

    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}

        if context.get('sp_id'):
            sp_id = context.get('sp_id')[0]
            sp_id = sp_id.split(',')
            search_sp_id = list(map(str.strip, sp_id))
            args += [('sp_id', 'in', search_sp_id)]

        if context.get('email'):
            email = context.get('email')[0]
            email = email.split(',')
            search_email_ids = list(map(str.strip, email))
            args += [('email', 'in', search_email_ids)]

        if context.get('phone'):
            phone_numbers = context.get('phone')[0]
            phone_numbers = phone_numbers.split(',')
            search_ids = list(map(str.strip, phone_numbers))
            args += [('phone', 'in', search_ids)]

        if not order:
            order = context.get('order')
        return super(SpRegistration, self)._search(args, offset, limit, order, count=count,
                                                   access_rights_uid=access_rights_uid)

    @api.model
    def check_condition_show_dialog(self, record_id, data_changed):
        if self._context.get('active_model') == 'call_queue.call_queue' and not self._context.get('call_queue_end') \
            or self._context.get('call_queue'):
            recs = self.env[self._context.get('active_model')].browse(self._context.get('active_ids'))
            or_mandatory_fields = True
            spr = self.browse(record_id)
            field_name = ''
            required_field_ids_browse = self.env['required.fields'].browse(self._context.get('required_field_ids'))
            for index, required_field_id in enumerate(required_field_ids_browse):
                for index_2, model_object_field_id in enumerate(required_field_id.model_object_field_ids):
                    field_name += model_object_field_id.display_name[:-18]
                    if index_2 < len(required_field_id.model_object_field_ids) - 1:
                        field_name += ', '
                if index < len(required_field_ids_browse) - 1:
                    field_name += ', '  # field_name += ' (or) '
            for index, required_field_id in enumerate(required_field_ids_browse):
                and_mandatory_fields = True
                for model_object_field_id in required_field_id.model_object_field_ids:
                    # print(model_object_field_id.name, data_changed.get(model_object_field_id.name))
                    currentVal = spr[model_object_field_id.name]
                    updatedVal = data_changed.get(model_object_field_id.name)
                    if not updatedVal:
                        and_mandatory_fields = False
                        break
                    elif model_object_field_id.ttype in ['one2many', 'many2many'] and \
                        len(currentVal) == len(updatedVal):
                        and_mandatory_fields = False
                        break
                    elif model_object_field_id.ttype in ['many2one'] and currentVal.id == updatedVal:
                        and_mandatory_fields = False
                        break
                    elif model_object_field_id.ttype in ['datetime', 'date'] and str(currentVal) == updatedVal:
                        and_mandatory_fields = False
                        break
                    elif model_object_field_id.ttype not in ['one2many', 'many2many', 'many2one'] and \
                        currentVal == updatedVal:
                        and_mandatory_fields = False
                        break

                if and_mandatory_fields:
                    or_mandatory_fields = True
                    break
                if not and_mandatory_fields:
                    or_mandatory_fields = False
                if index == len(required_field_ids_browse) - 1 and not and_mandatory_fields:
                    or_mandatory_fields = False
            context = dict(self.env.context)
            context.update({'call_queue_end': True})
            self.env.context = context
            if not or_mandatory_fields:
                raise ValidationError(_('Please fill these mandatory fields. %s.' % field_name))
        return super(SpRegistration, self).check_condition_show_dialog(record_id, data_changed)

    def create_appointment(self):
        userinput_id = self._context.get('userinput_id')
        call_record = self.env['call_queue.user_input'].browse(userinput_id)
        if call_record.registration_id.appointment_ids:
            res_id = call_record.registration_id.appointment_ids.id
        else:
            res_id = None
        return {
            'name': 'Create/Update Appointment',
            'context': {
                'default_registration_id': call_record.registration_id.id,
                'call_queue': True,
                'appointment': False,
            },
            'view_type': 'form',
            'res_id': res_id,
            'res_model': 'sp.appointment',
            'view_id': False,
            'view_mode': 'form',
            'type': 'ir.actions.act_window',
            'target': 'new'
        }

    def update_registration(self):
        userinput_id = self._context.get('userinput_id')
        call_record = self.env['call_queue.user_input'].browse(userinput_id)
        return {
            'name': 'Update Registration',
            'context': {
                'default_registration_id': call_record.registration_id.id,
                'required_field_ids': call_record.call_queue_id.required_field_ids.ids,
                'call_queue': True,
            },
            'view_type': 'form',
            'res_model': 'sp.registration',
            'res_id': call_record.registration_id.id,
            'view_id': self._context.get('record_view_id', False),
            'view_mode': 'form',
            'type': 'ir.actions.act_window',
            'target': 'new'
        }

    def applicant_need_time(self):
        userinput_id = self._context.get('userinput_id')
        call_record = self.env['call_queue.user_input'].browse(userinput_id)
        if call_record.registration_id:
            call_record.registration_id.sp_interview_status = 'participant_requested_for_time'
        return super(SpRegistration, self).applicant_need_time()

    # override call_queue_skip to update userinput state
    def call_queue_skip(self):
        userinput_id = self._context.get('userinput_id')
        call_record = self.env['call_queue.user_input'].browse(userinput_id)
        call_record.update({'state': 'skip'})
        return super(SpRegistration, self).call_queue_skip()

    # override call_queue_not_today to update userinput state
    def call_queue_not_today(self):
        userinput_id = self._context.get('userinput_id')
        call_record = self.env['call_queue.user_input'].browse(userinput_id)
        call_record.update({'state': 'cant_call'})
        return super(SpRegistration, self).call_queue_not_today()

    def did_not_pick(self):
        userinput_id = self._context.get('userinput_id')
        call_record = self.env['call_queue.user_input'].browse(userinput_id)
        # call_record.update({'state': 'did_not_pick'})

        context = dict(self.env.context)
        context.update({'did_not_pick': True})
        self.env.context = context

        if self.application_stage == self.env.ref('isha_sp_registration.application_stage_pre_registration_stage'):
            if self.call_count == 0:
                self.sp_call_status = self.env.ref("isha_sp_registration.sp_call_status_no_answer")
                # self.sp_pre_reg_status = self.env.ref("isha_sp_registration.sp_pre_reg_status_new_no_answer")
                self.sp_pre_reg_status = self.env.ref("isha_sp_registration.sp_pre_reg_status_new")
            else:
                self.call_count += 1
            if self.call_count == 3:
                self.sp_call_status = self.env.ref("isha_sp_registration.sp_call_status_unreachable")
                self.sp_pre_reg_status = self.env.ref("isha_sp_registration.sp_pre_reg_status_no_response")
        if self._context.get('active_model') == 'call_queue.call_queue':
            self.write({'person_called': self.env.user, 'date_called': fields.Datetime.now()})
        if call_record.registration_id:
            # if self.sp_pre_reg_status == self.env.ref("isha_sp_registration.sp_pre_reg_status_new_no_answer"):
            if self.sp_pre_reg_status == self.env.ref("isha_sp_registration.sp_pre_reg_status_new"):
                if call_record.registration_id.sp_call_status == \
                    self.env.ref("isha_sp_registration.sp_call_status_no_answer"):
                    if call_record.registration_id.call_count > 2:
                        pass
                if call_record.registration_id.sp_call_status == \
                    self.env.ref('isha_sp_registration.sp_call_status_not_called'):
                    call_record.registration_id.sp_call_status = self.env.ref(
                        "isha_sp_registration.sp_call_status_no_answer")
            if self.sp_pre_reg_status == self.env.ref("isha_sp_registration.sp_pre_reg_status_no_response"):
                if call_record.registration_id.sp_call_status == \
                    self.env.ref("isha_sp_registration.sp_call_status_unreachable"):
                    pass
        return {
            'name': "Didn't Pick Comment",
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'call.queue.comment',
            'view_id': False,
            'target': 'new',
            'context': {
                'default_queue_user_input_id': call_record.id,
            },
        }
        # return super(SpRegistration, self).did_not_pick()
