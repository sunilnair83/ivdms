# -*- coding: utf-8 -*-

import calendar
import datetime

from odoo import models, fields, api
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


class SpAppointment(models.Model):
    _inherit = 'sp.appointment'

    def write(self, vals):
        if self._context.get('call_queue', False):
            call_record = self.env['call_queue.user_input'].search([('id', '=', self._context.get('userinput_id'))])
            if call_record:
                call_record.state = 'done'
        return super(SpAppointment, self).write(vals)
