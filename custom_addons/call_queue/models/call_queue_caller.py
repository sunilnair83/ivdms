# -*- coding: utf-8 -*-
# Part of Isha. See LICENSE file for full copyright and licensing details.

import datetime
from datetime import date
import logging
import re
import uuid

from werkzeug import urls

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError

from dateutil.relativedelta import relativedelta

email_validator = re.compile(r"[^@]+@[^@]+\.[^@]+")
_logger = logging.getLogger(__name__)


class CallersMaster(models.Model):
    _name = 'call_queue.callers.master'
    _rec_name = 'user_id'
    _description = 'Call Queue Callers Master'

    user_id = fields.Many2one('res.users', 'User', required=True,
                              domain=lambda self: [
                                  ('groups_id', 'in', self.env.ref('call_queue.group_call_queue_user').id)])
    dialer_code = fields.Char('Aster Dialer Code')
    active = fields.Boolean("Active", default=True)
    language_ids = fields.Many2many('res.lang', string='Languages',
                                    domain=['|', ('active', '=', False), ('active', '=', True)])
    caller_allocation_ids = fields.One2many('call_queue.caller', 'caller_id', 'Call Queues')


class CallQueueCaller(models.Model):
    _name = 'call_queue.caller'
    _description = 'Queue Caller Rel'
    _rec_name = 'caller_id'

    _sql_constraints = [
        ('call_queue_caller_unique', 'unique (call_queue_id, caller_id)', 'Each Caller have one Call Queue!')
    ]

    call_queue_id = fields.Many2one('call_queue.call_queue', 'Queue', ondelete='cascade',
                                    domain=[('state', '=', 'open')])
    call_queue_current_count = fields.Integer('Current Queue Count', related='call_queue_id.current_record_count')
    call_queue_state = fields.Selection(string="Queue State", related='call_queue_id.state', store=True)
    caller_id = fields.Many2one('call_queue.callers.master', 'Caller', required=True)
    user_id = fields.Many2one(related='caller_id.user_id', store=True)
    language_ids = fields.Many2many(related='caller_id.language_ids')
    active = fields.Boolean("Active", default=True)

    lead_ids = fields.One2many('call_queue.user_input', 'caller_id', 'People contacted')
    contacts_assigned_count = fields.Integer('Target for today')
    contacts_static_assigned_count = fields.Integer('Static Assigned', compute='_compute_leads_count')
    contacts_completed_count = fields.Integer('Completed', compute='_compute_leads_count')
    contacts_skipped_count = fields.Integer('Skipped', compute='_compute_leads_count')
    contacts_did_not_pick_count = fields.Integer('Did not pick', compute='_compute_leads_count')
    contacts_cant_call_count = fields.Integer('Do not call', compute='_compute_leads_count')

    @api.model
    def create(self, vals):
        self.env['ir.rule'].clear_cache()
        return super(CallQueueCaller, self).create(vals)

    def write(self, vals):
        self.env['ir.rule'].clear_cache()
        return super(CallQueueCaller, self).write(vals)

    def unlink(self):
        self.env['ir.rule'].clear_cache()
        return super(CallQueueCaller, self).unlink()

    @api.depends('lead_ids', 'lead_ids.state')
    def _compute_leads_count(self):
        for rec in self:
            rec.contacts_static_assigned_count = len(
                rec.lead_ids.filtered(lambda x: x.state == 'new').ids)
            rec.contacts_completed_count = len(
                rec.lead_ids.filtered(lambda x: x.state == 'done' and x.create_date.date() == date.today()).ids)
            rec.contacts_skipped_count = len(
                rec.lead_ids.filtered(lambda x: x.state == 'skip' and x.create_date.date() == date.today()).ids)
            rec.contacts_did_not_pick_count = len(
                rec.lead_ids.filtered(lambda x: x.state == 'did_not_pick' and x.create_date.date() == date.today()).ids)
            rec.contacts_cant_call_count = len(
                rec.lead_ids.filtered(lambda x: x.state == 'cant_call' and x.create_date.date() == date.today()).ids)
