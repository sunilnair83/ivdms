from odoo import models, fields, api


class CallHistory(models.Model):
    _name = 'call_queue.call_history'
    _description = 'Call Queue Caller History'
    _rec_name = 'call_queue_id'

    call_queue_id = fields.Many2one('call_queue.call_queue', 'Call Queue')
    call_queue_state = fields.Selection(string="Queue State", related='call_queue_id.state', store=True)
    caller_id = fields.Many2one('call_queue.caller', 'Allocation')
    call_date = fields.Date(string='Call Queue Date')
    assigned_count = fields.Integer(string='Assigned Calls')
    completed_count = fields.Integer(string='Completed Calls')
    skipped_count = fields.Integer(string='Skipped Calls')
    did_not_pick_count = fields.Integer(string='Didn\'t Pickup')
    cant_call_count = fields.Integer(string='Can\'t Call')
