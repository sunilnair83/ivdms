import logging

from odoo import fields, models

_logger = logging.getLogger(__name__)


class ResUsers(models.Model):
    _inherit = 'res.users'

    call_queue_caller_ids = fields.One2many('call_queue.caller', 'user_id', 'Call Queue Caller Allocations')

    def get_call_queue_ids(self):
        res = []
        # res = self.call_queue_caller_ids.mapped('call_queue_id').ids
        for call_queue_caller in self.call_queue_caller_ids:
            if call_queue_caller.contacts_assigned_count > 0:
                res += call_queue_caller.call_queue_id.ids
        return res
