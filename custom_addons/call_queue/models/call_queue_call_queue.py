# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import traceback
import uuid

from collections import Counter, OrderedDict
from datetime import datetime, date
from itertools import product
from werkzeug import urls
import random
import logging
import json
from lxml import etree
from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError
from odoo.osv import expression
from odoo.tools import safe_eval

_logger = logging.getLogger(__name__)


class CallQueue(models.Model):
    """ Settings for a multi-page/multi-question call_queue. Each call_queue can have one or more attached pages
    and each page can display one or more questions. """
    _name = 'call_queue.call_queue'
    _description = 'Call Queue'
    _rec_name = 'title'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    # description
    title = fields.Char('CallQueue Title', required=True, translate=True)
    description = fields.Html("Description", translate=True,
                              help="The description will be displayed on the home page of the call_queue. You can use this to give the purpose and guidelines to your candidates before they start it.")
    call_description = fields.Text("Call Description")
    color = fields.Integer('Color Index', default=0)
    filter_condition = fields.Text("Filter Condition",
                                   help="Format [('field name','=/in/not in',''),(value/values)] refer ir.rule ui")
    order_condition = fields.Text("Order Condition", help="Example count asc, date desc")
    model_id = fields.Many2one('ir.model', string='Applies to', required=True,
                               domain=['|', ('model', '=', 'sp.appointment'), ('model', '=', 'sp.registration')],
                               default=lambda self: self.env['ir.model'].sudo().search(
                                   [('model', '=', 'sp.registration')]))
    required_field_ids = fields.Many2many('required.fields', string="Fields")
    record_view_id = fields.Many2one('ir.ui.view', domain=([('model', '=', 'sp.registration'), ('type', '=', 'form')]))
    active = fields.Boolean("Active", default=True)
    question_and_page_ids = fields.One2many('call_queue.question', 'call_queue_id', string='Sections and Questions',
                                            copy=True)
    page_ids = fields.One2many('call_queue.question', string='Order conditions',
                               compute="_compute_page_and_question_ids")
    question_ids = fields.One2many('call_queue.question', string='Questions', compute="_compute_page_and_question_ids")
    state = fields.Selection(
        string="CallQueue Stage",
        selection=[('draft', 'Draft'),
                   ('open', 'In Progress'),
                   ('closed', 'Closed')], default='draft', required=True, group_expand='_read_group_states')

    # content
    user_input_ids = fields.One2many('call_queue.user_input', 'call_queue_id', string='User responses',
                                     groups='call_queue.group_call_queue_user', copy=True)
    current_user_input_ids = fields.One2many('call_queue.user_input', 'call_queue_id', string='Current User responses',
                                             domain=lambda self: [('caller_user_id', '=', self.env.user.id)])

    # call details
    call_script = fields.Text(string='Call Script')
    coordinator_id = fields.Many2one('res.users', domain=[('share', '=', False)], string='Coordinator', required=False,
                                     ondelete='set null')
    callers = fields.One2many('call_queue.caller', 'call_queue_id', string='Callers', copy=True)
    caller_count = fields.Integer('Caller Count', compute='_compute_caller_count', store=True)

    avg_allocation_count = fields.Integer(string="Average contacts per caller")

    show_gender = fields.Boolean('Show Gender', default=True)
    show_occupation = fields.Boolean('Show Occupation', default=True)
    show_programs = fields.Boolean('Show Programs', default=True)
    show_feedback_selection = fields.Boolean('Show Feedback selection', default=True)
    get_unassigned_contacts = fields.Boolean('Get unassigned contacts', default=False)
    get_dynamic_contacts = fields.Boolean('Dynamic allocation', default=True)

    allow_follow_up_sms = fields.Boolean('Allow follow up SMS')

    follow_up_sms_text = fields.Text('Follow up SMS text')
    follow_up_sms_reg_link = fields.Text('Reg Link in the SMS', help='Replaces * in the SMS text')
    follow_up_sms_info_link = fields.Text('Video Link in the SMS', help='Appends to the SMS text')
    sms_test_number = fields.Char('Test Number',
                                  help='SMS from call page will be sent to this number. Clear this when queue starts.')

    follow_up_sms_template_id = fields.Many2one(
        'sms.template', string='SMS Template',
        domain=[('model', '=', 'call_queue.call_queue')], ondelete='restrict',
        default=lambda self: self.env.ref('call_queue.sms_template_call_queue_follow_ups_sms',
                                          raise_if_not_found=False),
        help='This field contains the template of the SMS that caller can send to the person')
    show_prev_remark = fields.Boolean('Show Previous Remark to caller', default=False)
    show_address = fields.Boolean('Show Address', default=False)

    # allow updates on res_partner
    update_profile = fields.Boolean('Allow Caller to Update Profile', default=False)

    is_sensitive = fields.Boolean('Sensitive Call Queue', default=False)

    queue_track_period = fields.Integer(string='Track Queue Period', default=7)

    current_record_count = fields.Integer('Apx records in queue', compute='_compute_current_record_count')

    current_caller_assigned_count = fields.Integer('Assigned for you', compute='_compute_current_user_assigned_count')
    current_caller_static_assigned_count = fields.Integer('Static Assigned',
                                                          compute='_compute_current_user_assigned_count')
    current_caller_completed_count = fields.Integer('Completed', compute='_compute_current_user_assigned_count')

    def _compute_call_type_id(self):
        try:
            default_call_queue = self.env.ref('call_queue.call_queue_type_registration')
        except Exception as ex:
            _logger.info(ex)
            default_call_queue = None
        return default_call_queue

    call_type_id = fields.Many2one('call_queue.type', required=True, default=_compute_call_type_id)
    # default=lambda self: self.env.ref('call_queue.call_queue_type_registration'))
    message_ids = fields.One2many(groups="isha_sp_registration.group_sp_registration_admin")

    def _compute_current_record_count(self):
        for rec in self:
            try:
                domain = eval(rec.filter_condition.strip()) if rec.filter_condition else []
                domain = expression.normalize_domain(domain)
                rec.current_record_count = self.env['sp.registration'].search_count(domain)
            except:
                rec.current_record_count = -1

    def _compute_current_user_assigned_count(self):
        for rec in self:
            caller_sudo = self.env['call_queue.caller'].sudo().search([
                ('call_queue_id', '=', rec.id), ('user_id', '=', self.env.user.id)], limit=1)
            rec.current_caller_assigned_count = caller_sudo.contacts_assigned_count
            rec.current_caller_completed_count = caller_sudo.contacts_completed_count
            rec.current_caller_static_assigned_count = caller_sudo.contacts_static_assigned_count

    @api.onchange('follow_up_sms_template_id', 'follow_up_sms_reg_link', 'follow_up_sms_info_link')
    def _onchange_template_id(self):
        """ UPDATE ME """
        if len(self.ids) == 0:  # create flow
            self.follow_up_sms_text = self.follow_up_sms_template_id.body
            return

        if self.follow_up_sms_template_id:
            subject = self.env['sms.template']._render_template(self.follow_up_sms_template_id.body,
                                                                'call_queue.call_queue', self.ids[0])
            self.follow_up_sms_text = subject
        else:
            self.follow_up_sms_text = None

    @api.depends('question_and_page_ids')
    def _compute_page_and_question_ids(self):
        for call_queue in self:
            call_queue.page_ids = call_queue.question_and_page_ids.filtered(lambda question: question.is_page)
            call_queue.question_ids = call_queue.question_and_page_ids - call_queue.page_ids

    @api.depends('callers')
    def _compute_caller_count(self):
        self.caller_count = len(self.callers)

    def _get_next_contact(self, call_queue_sudo, caller_sudo, skipped):
        answer_sudo = self.env['call_queue.user_input'].sudo()
        next_contact = None
        if skipped:
            next_contact = answer_sudo.search([
                ('call_queue_id', '=', call_queue_sudo.id), ('caller_id', '=', caller_sudo.id),
                ('state', '=', 'skip')], order='write_date', limit=1)
        if not next_contact:
            # check if caller left some contact previously or contact has been pre-assigned
            next_contact = answer_sudo.search(([
                ('call_queue_id', '=', call_queue_sudo.id), ('caller_id', '=', caller_sudo.id),
                ('state', '=', 'new')]), limit=1)
            if next_contact:
                # this contact might have been pre-assigned, assign questions now
                vals = {'question_ids': [(6, 0, call_queue_sudo._prepare_answer_questions().ids)],
                        'state': 'new'}
                next_contact.write(vals)
        if not next_contact and call_queue_sudo.get_unassigned_contacts:
            # get next unassigned contact and update questions and caller
            vals = {'question_ids': [(6, 0, call_queue_sudo._prepare_answer_questions().ids)],
                    'caller_id': caller_sudo.id, 'state': 'new'}
            next_contact = answer_sudo.search(([
                ('call_queue_id', '=', call_queue_sudo.id), ('caller_id', '=', False),
                ('write_date', '>=', datetime.now().strftime('%Y-%m-%d 00:00:00'))]), limit=1)
            if next_contact:
                next_contact.write(vals)
        if not next_contact and call_queue_sudo.get_dynamic_contacts:
            domain = eval(call_queue_sudo.filter_condition.strip()) if call_queue_sudo.filter_condition else []
            domain = expression.normalize_domain(domain)

            matching_list = self.env['sp.registration'].search(domain, order=call_queue_sudo.order_condition, limit=20)
            for partner_record in matching_list:
                # check that the record is not already being processed today
                matching_call_record = answer_sudo.search(
                    [('call_queue_id', '=', call_queue_sudo.id), ('registration_id', '=', partner_record.id),
                     ('write_date', '>=', datetime.now().strftime('%Y-%m-%d 00:00:00')),
                     ('state', 'not in', ['skip', 'done'])],
                    limit=1)
                if not matching_call_record:
                    # make an entry in user input table fresh
                    vals = {'call_queue_id': self.id, 'registration_id': partner_record.id,
                            'caller_id': caller_sudo.id, 'state': 'new'}
                    next_contact = answer_sudo.create(vals)
                    break
            if not next_contact and len(matching_list) > 0:
                raise AccessError(_("All items in the Queue are being processed."))
        return next_contact

    def action_add_records(self):
        domain = eval(self.filter_condition.strip()) if self.filter_condition else []
        domain = expression.normalize_domain(domain)
        form_view_id = self.record_view_id.id
        tree_view_id = self.env.ref('isha_sp_registration.sp_registration_list').id
        if self.call_type_id and self.call_type_id.model_id.model == 'sp.registration':
            form_view_id = self.env.ref('isha_sp_registration.sp_registration_update_registration').id
        return {
            'name': 'Queue',
            'view_type': 'form',
            'res_model': 'sp.registration',
            'views': [(tree_view_id, 'tree'), (form_view_id, 'form')],
            'view_mode': 'tree,form',
            'type': 'ir.actions.act_window',
            'context': {'order': self.order_condition, 'hide_footer': True, 'skip_order_term_validation': True},
            'target': 'current',
            'domain': domain
        }

    def action_get_next_contact(self):
        self.ensure_one()
        caller_sudo = self.env['call_queue.caller'].sudo().search([
            ('call_queue_id', '=', self.id), ('user_id', '=', self.env.user.id)], limit=1)
        if caller_sudo.contacts_completed_count >= caller_sudo.contacts_assigned_count:
            raise AccessError(_("Target for today completed " + str(caller_sudo.contacts_assigned_count)))

        userinput_id = self._get_next_contact(self.sudo(), caller_sudo, False)
        if not userinput_id:
            raise AccessError(_("Could not find items in the queue"))
        view_id = self.record_view_id.id
        if self.call_type_id and self.call_type_id.model_id.model == 'sp.registration':
            view_id = self.env.ref('isha_sp_registration.sp_registration_update_registration').id

        if self.record_view_id.id:
            record_view_id = self.record_view_id.id
        else:
            record_view_id = self.env.ref('isha_sp_registration.sp_registration_form_first_level_ie_complete').id

        context = dict(self.env.context)
        context.update(
            {
                'skip_order_term_validation': True,
                'userinput_id': userinput_id.id,
                'record_view_id': record_view_id,
                'call_queue': True,
                'call_queue_type': self.model_id,
            }
        )
        self.env.context = context
        if userinput_id:
            return {
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'view_id': view_id,
                'res_model': 'sp.registration',
                'res_id': userinput_id.registration_id.id,
                'target': 'new',
                'flags': {
                    "mode": 'readonly'
                },
                'context': context,
            }

    # statistics
    answer_count = fields.Integer("Registered", compute="_compute_call_queue_statistic", store=True)
    answer_done_count = fields.Integer("Calls made", compute="_compute_call_queue_statistic", store=True)

    @api.depends('user_input_ids.state')
    def _compute_call_queue_statistic(self):
        default_vals = {
            'answer_count': 0, 'answer_done_count': 0
        }
        stat = dict((cid, dict(default_vals)) for cid in self.ids)
        UserInput = self.env['call_queue.user_input']
        base_domain = [('call_queue_id', 'in', self.ids)]

        read_group_res = UserInput.read_group(base_domain, ['call_queue_id', 'state'], ['call_queue_id', 'state'],
                                              lazy=False)
        for item in read_group_res:
            stat[item['call_queue_id'][0]]['answer_count'] += item['__count']
            if item['state'] == 'done':
                stat[item['call_queue_id'][0]]['answer_done_count'] += item['__count']

        for call_queue in self:
            call_queue.update(stat.get(call_queue._origin.id, default_vals))

    def _read_group_states(self, values, domain, order):
        selection = self.env['call_queue.call_queue'].fields_get(allfields=['state'])['state']['selection']
        return [s[0] for s in selection]

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        result = super(CallQueue, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar,
                                                        submenu=submenu)
        readonly_manager = self.user_has_groups('call_queue.group_call_queue_manager_readonly')

        READONLY_FIELDS = ['filter_condition', 'order_condition', 'record_view_id'] if readonly_manager else []
        READONLY_FIELDS = []

        if view_type == 'kanban':
            for rfield in READONLY_FIELDS:
                if rfield in result['fields']:
                    result['fields'][rfield]['readonly'] = True
        if view_type == 'form':
            doc = etree.XML(result['arch'])
            for node in doc.xpath("//field"):
                name = node.get('name')
                if name in READONLY_FIELDS:
                    if node.attrib.get('modifiers'):
                        attr = json.loads(node.attrib.get('modifiers'))
                        attr['readonly'] = 1
                        node.set('modifiers', json.dumps(attr))
            result['arch'] = etree.tostring(doc)
        return result

    # ------------------------------------------------------------
    # CRUD
    # ------------------------------------------------------------
    def copy_data(self, default=None):
        title = _("%s (copy)") % (self.title)
        default = dict(default or {}, title=title)
        return super(CallQueue, self).copy_data(default)

    # ------------------------------------------------------------
    # TECHNICAL
    # ------------------------------------------------------------

    def _prepare_answer_questions(self):
        """ Will generate the questions for a randomized call_queue.
        It uses the random_questions_count of every sections of the call_queue to
        pick a random number of questions and returns the merged recordset """
        self.ensure_one()

        questions = self.env['call_queue.question']

        # First append questions without page
        for question in self.question_ids:
            if not question.page_id:
                questions |= question

        return questions

    # ------------------------------------------------------------
    # ACTIONS
    # ------------------------------------------------------------

    def action_draft(self):
        self.write({'state': 'draft'})

    def action_open(self):
        self.write({'state': 'open'})

    def action_close(self):
        self.write({'state': 'closed'})

    def action_send_call_queue(self):
        """ Open a window to compose an email, pre-filled with the call_queue message """
        # Ensure that this call_queue has at least one page with at least one question.
        if not self.callers or not self.user_input_ids:
            raise UserError(_('Please add at least one caller and one contact to the queue.'))

        if self.state == 'closed':
            raise UserError(_("You cannot send invitations for closed call_queues."))

        template = self.env.ref('call_queue.mail_template_call_queue_caller_invite', raise_if_not_found=False)

        local_context = dict(
            self.env.context,
            default_call_queue_id=self.id,
            default_use_template=bool(template),
            default_template_id=template and template.id or False,
            notif_layout='mail.mail_notification_light',
        )
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'call_queue.invite',
            'target': 'new',
            'context': local_context,
        }

    def action_call_queue_user_input_line_with_answers(self):
        action_rec = self.env.ref('call_queue.action_call_queue_user_input_line')
        action = action_rec.read()[0]
        ctx = dict(self.env.context)
        ctx.update({'search_default_call_queue_id': self.ids[0],
                    'search_default_not_test': 1})
        action['context'] = ctx
        return action

    def action_call_queue_user_input_completed(self):
        action_rec = self.env.ref('call_queue.action_call_queue_user_input')
        action = action_rec.read()[0]
        ctx = dict(self.env.context)
        ctx.update({'search_default_call_queue_id': self.ids[0],
                    'search_default_completed': 1,
                    'search_default_not_test': 1})
        action['context'] = ctx
        return action

    def action_call_queue_user_input(self):
        action_rec = self.env.ref('call_queue.action_call_queue_user_input')
        action = action_rec.read()[0]
        ctx = dict(self.env.context)
        ctx.update({'search_default_call_queue_id': self.ids[0],
                    'search_default_not_test': 1})
        action['context'] = ctx
        return action

    def compute_call_queue_call_history(self, call_queue):
        call_history = []
        call_queue_callers = self.env['call_queue.caller'].search([('call_queue_id', 'in', call_queue.ids)])
        for caller in call_queue_callers:
            call_history.append({
                'call_queue_id': caller.call_queue_id.id,
                'caller_id': caller.id,
                'call_date': date.today(),
                'assigned_count': caller.contacts_assigned_count,
                'completed_count': caller.contacts_completed_count,
                'skipped_count': caller.contacts_skipped_count,
                'did_not_pick_count': caller.contacts_did_not_pick_count,
                'cant_call_count': caller.contacts_cant_call_count,
            })

        # Delete old call history for the same day if already computed
        call_history_temp = self.env['call_queue.call_history'].search([('call_queue_id', 'in', call_queue.ids),
                                                                        ('call_date', '=', date.today())])
        call_history_temp.unlink()

        self.env['call_queue.call_history'].create(call_history)

    def call_history_single(self):
        self.compute_call_queue_call_history(self)

    def call_history_bulk(self):
        call_queues = self.env['call_queue.call_queue'].search([('active', '=', True)])
        self.compute_call_queue_call_history(call_queues)
