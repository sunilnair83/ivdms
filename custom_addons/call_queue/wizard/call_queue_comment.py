# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging

from odoo import fields, models

_logger = logging.getLogger(__name__)


class CallQueueComment(models.TransientModel):
    _name = 'call.queue.comment'
    _description = 'Call Queue Comment'

    name = fields.Text('Comment', required=True)
    queue_user_input_id = fields.Many2one('call_queue.user_input', string='Queue User Input', required=True)

    def comment_button(self):
        self.queue_user_input_id.write({
            'did_not_pick_comment': self.name,
            'state': 'did_not_pick',
        })
