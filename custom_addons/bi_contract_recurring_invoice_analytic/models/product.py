# -*- coding: utf-8 -*-
################################################################################
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
################################################################################
from odoo import api, fields, models, _
from datetime import datetime, timedelta


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    is_contract = fields.Boolean(string='Contract/Warranty Product ')
    interval_number = fields.Integer(default=1, string="Recurring Period")
    interval_type = fields.Selection([('minutes', 'Minutes'),
                                      ('hours', 'Hours'),
                                      ('days', 'Days'),
                                      ('weeks', 'Weeks'),
                                      ('months', 'Months')], string='Interval Unit', default='months')
