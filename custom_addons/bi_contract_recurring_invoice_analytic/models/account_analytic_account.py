# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
import datetime
import odoo.addons.decimal_precision as dp

class analytic_contract_line(models.Model):
    _name = 'analytic.contract.line'

    custom_analytic_id = fields.Many2one('account.analytic.account','analytic account')
    name = fields.Text(string='Description', required=True)
    price_unit = fields.Float('Unit Price', required=True, digits='Product Price', default=0.0)
    price_subtotal = fields.Float(string='Subtotal', default=0.0)
    tax_id = fields.Many2many('account.tax', string='Taxes', domain=['|', ('active', '=', False), ('active', '=', True)])
    discount = fields.Float(string='Discount (%)', digits='Discount', default=0.0)
    product_id = fields.Many2one('product.product', string='Product', domain=[('sale_ok', '=', True)], change_default=True, ondelete='restrict', required=True)
    product_uom_qty = fields.Float(string='Quantity', digits='Product Unit of Measure', required=True, default=1.0)
    product_uom = fields.Many2one('uom.uom', string='Unit of Measure', required=True)
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Tags')



class analytic_sale_order_line(models.Model):
    _name = 'analytic.sale.order.line'

    custom_analytic_id = fields.Many2one('account.analytic.account','analytic account')
    name = fields.Text(string='Description', required=True)
    price_unit = fields.Float('Unit Price', required=True, digits='Product Price', default=0.0)
    price_subtotal = fields.Float(string='Subtotal')
    tax_id = fields.Many2many('account.tax', string='Taxes', domain=['|', ('active', '=', False), ('active', '=', True)])
    discount = fields.Float(string='Discount (%)', digits='Discount', default=0.0)
    product_id = fields.Many2one('product.product', string='Product', domain=[('sale_ok', '=', True)], change_default=True, ondelete='restrict', required=True)
    product_uom_qty = fields.Float(string='Quantity', digits='Product Unit of Measure', required=True, default=1.0)
    product_uom = fields.Many2one('uom.uom', string='Unit of Measure', required=True)
    analytic_tag_ids = fields.Many2many('account.analytic.tag', string='Analytic Tags')

class account_analytic_account(models.Model):
    _inherit = 'account.analytic.account'

    def _get_invoiced(self):
        for record in self:
            inv_search = self.env['account.move'].search([('custom_contract_id','=',record.id)])
            record.update({
                'invoice_count': len(inv_search),
            })

    def _write(self, vals):
        for rec in self:
            if 'contract_remainder' in vals:
                if rec.contract_remainder_days <= vals['contract_remainder'] :
                    vals['stage'] = 'expires_soon'
        return  super(account_analytic_account, self)._write(vals)

 
    def _get_days(self):
        for record in self:
            date_format = "%Y-%m-%d"
            today_date = datetime.datetime.today().date()
            if record.renew_date:
                renew_date = datetime.datetime.strptime(str(record.renew_date), date_format)
                delta = renew_date.date() - today_date
                record.contract_remainder_days = delta.days
            record.contract_remainder_days = 0.0
            
    contract_lines = fields.One2many('analytic.contract.line','custom_analytic_id','Contract' , readonly=True)
    order_lines = fields.One2many('analytic.sale.order.line','custom_analytic_id','Order Lines', readonly=True)
    contract = fields.Boolean(string="Contract")
    interval_number = fields.Integer(default=1, string="Recurring Period")
    interval_type = fields.Selection([
                                      ('days', 'Days'),
                                      ('weeks', 'Weeks'),
                                      ('months', 'Months'),
                                      ('year', 'Years')], string='Interval Unit', default='months')
    
    contract_Remainder = fields.Integer('Contract Expiration Remainder (Days)', default=1)
    journal_id = fields.Many2one('account.journal',string='Journal')
    terms = fields.Html('Terms And Condition')
    stage = fields.Selection([('draft','Draft'),
                               ('expires_soon', 'Expires Soon'),
                                      ('running', 'Running'),
                                      ('lock', 'Lock'),
                                      ], string='Stage', default='draft')
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date')
    renew_date = fields.Date('Date Of Next Invoice')
    remember_stage = fields.Char('Stage Remember')
    contract_remainder = fields.Integer(string='Contract Expiration Remainder(days)', default=1)
    contract_remainder_days = fields.Integer(string='Contract Expiration Remainder days', compute='_get_days')
    invoice_count = fields.Integer(string='# of Invoices', compute='_get_invoiced', readonly=True)
    
    
    def check_contract(self):
        today_date = datetime.datetime.today().date()
        contrat_search_ids = self.env['account.analytic.account'].search([])
        for record in contrat_search_ids:
            if record.renew_date == today_date.strftime("%Y-%m-%d"):
                record.generate_invoice()
        return True

    def generate_invoice(self):
        invoice_obj  = self.env['account.move']
        invoice_lines = []
        if self.journal_id:
            journal = self.journal_id
        else:
            journal_id = self.env['account.journal'].search([('type','=','sale')])
            journal = journal_id[0]
        if self.partner_id.property_account_receivable_id:
            account_id = self.partner_id.property_account_payable_id
        else:
            account_search = self.env['ir.property'].search([('name', '=', 'property_account_expense_categ_id')])
            account_id = account_search.value_reference
            account_id = account_id.split(",")[1]
            account_id = self.env['account.account'].browse(account_id)
        invoice_id = invoice_obj.create({
                            'name':'/',
                            'partner_id':self.partner_id.id,
                            'date':self.renew_date,
                            'user_id':self._uid,
                            'journal_id':journal.id,
                            'type':'out_invoice',
                            'custom_contract_id':self.id
                            })
        for record in self.contract_lines:
            if record.product_id.property_account_income_id:
                account = record.product_id.property_account_income_id
            elif record.product_id.categ_id.property_account_income_categ_id:
                account = record.product_id.categ_id.property_account_income_categ_id
            else:
                account_search = self.env['ir.property'].search([('name', '=', 'property_account_income_categ_id')])
                account = account_search.value_reference
                account = account.split(",")[1]
                account = self.env['account.account'].browse(account)
            analytic_tag_ids = [a.id for a in record.analytic_tag_ids]
            tax_id = [a.id for a in record.tax_id]
            data = {
                    'name': record.name,
                    'price_unit':record.price_unit,
                    'tax_ids':[(6,0,tax_id)],
                    'discount':record.discount,
                    'product_id':record.product_id.id,
                    'quantity':record.product_uom_qty,
                    'product_uom_id':record.product_uom.id,
                    'analytic_tag_ids':[(6,0,analytic_tag_ids)],
                    'price_subtotal':record.price_subtotal,
                    'account_id' : account.id,
                }
            invoice_lines.append((0, 0, data))

        invoice_id.invoice_line_ids = invoice_lines
        self.stage = 'running'
        renew_date = ""
        if self.renew_date:
            if self.interval_type == 'year':
                yr = self.interval_number * 12
                renew_date = (datetime.datetime.strptime(str(self.renew_date), "%Y-%m-%d").date() + datetime.timedelta(yr*365/12)).isoformat()
            if self.interval_type == 'months':
                renew_date = (datetime.datetime.strptime(str(self.renew_date), "%Y-%m-%d").date() + datetime.timedelta(self.interval_number*365/12)).isoformat()
            if self.interval_type == 'weeks':
                renew_date = datetime.datetime.strptime(str(self.renew_date),"%Y-%m-%d") + datetime.timedelta(days=self.interval_number * 7)
            if self.interval_type == 'days':
                renew_date = datetime.datetime.strptime(str(self.renew_date),"%Y-%m-%d") + datetime.timedelta(days=self.interval_number)
        
        
        self.renew_date = renew_date

        imd = self.env['ir.model.data']
        action = imd.xmlid_to_object('account.action_move_out_invoice_type')
        list_view_id = imd.xmlid_to_res_id('account.view_invoice_tree')
        form_view_id = imd.xmlid_to_res_id('account.view_invoice_form')

        result = {
            'name': action.name,
            'help': action.help,
            'type': action.type,
            'views': [[list_view_id, 'tree'], [form_view_id, 'form'], [False, 'graph'], [False, 'kanban'], [False, 'calendar'], [False, 'pivot']],
            'target': action.target,
            'context': action.context,
            'res_model': action.res_model,
        }
        result['views'] = [(form_view_id, 'form')]
        result['res_id'] = invoice_id.id
        return result

    def action_view_invoice(self):
        inv_search = self.env['account.move'].search([('custom_contract_id','=',self.id)])
        imd = self.env['ir.model.data']
        action = imd.xmlid_to_object('account.action_move_out_invoice_type')
        list_view_id = imd.xmlid_to_res_id('account.view_invoice_tree')
        form_view_id = imd.xmlid_to_res_id('account.view_invoice_form')

        result = {
            'name': action.name,
            'help': action.help,
            'type': action.type,
            'views': [[list_view_id, 'tree'], [form_view_id, 'form'], [False, 'graph'], [False, 'kanban'], [False, 'calendar'], [False, 'pivot']],
            'target': action.target,
            'context': action.context,
            'res_model': action.res_model,
        }
        if len(inv_search) > 1:
            result['domain'] = "[('id','in',%s)]" % inv_search.ids
        elif len(inv_search) == 1:
            result['views'] = [(form_view_id, 'form')]
            result['res_id'] = inv_search.ids[0]
        else:
            result = {'type': 'ir.actions.act_window_close'}
        return result

    def lock(self):
        self.remember_stage = self.stage
        self.stage = 'lock'
        return True
    
    def unlock(self):
        self.stage = self.remember_stage
        return True
            
    
