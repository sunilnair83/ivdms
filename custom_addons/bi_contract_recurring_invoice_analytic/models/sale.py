# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
import datetime

class SaleQuoteTemplate(models.Model):
    _inherit = 'sale.order.template'

    interval_number = fields.Integer(default=1, string="Recurring Period")
    interval_type = fields.Selection([('days', 'Days'),
                                      ('weeks', 'Weeks'),
                                      ('months', 'Months'),
                                      ('year', 'Years')], string='Interval Unit', default='months')
    


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    template = fields.Boolean('Template')

class AccountInvoice(models.Model):
    _inherit = 'account.move'

    custom_contract_id = fields.Many2one('account.analytic.account', 'Contract')


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    interval_number = fields.Integer(default=1, string="Recurring Period")
    interval_type = fields.Selection([('days', 'Days'),
                                      ('weeks', 'Weeks'),
                                      ('months', 'Months'),
                                      ('year', 'Years')], string='Interval Unit', default='months')

    def action_confirm(self):
        super(SaleOrder, self).action_confirm()
        if not self.analytic_account_id:
            if self.interval_type == 'year':
                yr = self.interval_number * 12
                renew_date = (datetime.datetime.today().date() + datetime.timedelta(yr * 365 / 12)).isoformat()
            if self.interval_type == 'months':
                renew_date = (datetime.datetime.today().date() + datetime.timedelta(self.interval_number * 365 / 12)).isoformat()
            if self.interval_type == 'weeks':
                renew_date = datetime.datetime.strptime(datetime.datetime.today().date().strftime("%Y-%m-%d"), "%Y-%m-%d") + datetime.timedelta(days=self.interval_number * 7)
            if self.interval_type == 'days':
                renew_date = datetime.datetime.strptime(datetime.datetime.today().date().strftime("%Y-%m-%d"), "%Y-%m-%d") + datetime.timedelta(days=self.interval_number)
            
            account_analytic_account_id = self.env['account.analytic.account'].create({
                                                                                    'name':self.name + " - " + self.partner_id.name,
                                                                                    'partner_id':self.partner_id.id,
                                                                                    'contract':True,
                                                                                    'interval_number':self.interval_number,
                                                                                    'interval_type':self.interval_type,
                                                                                    'stage':'draft',
                                                                                    'contract_Remainder':1,
                                                                                    'start_date':datetime.datetime.today().date(),
                                                                                    'end_date':renew_date,
                                                                                    'renew_date':renew_date
                                                                                    })
            self.analytic_account_id = account_analytic_account_id.id
        
        for record in self.order_line:
            if record.template:
                analytic_tag_ids = [a.id for a in record.analytic_tag_ids]
                tax_id = [a.id for a in record.tax_id]
                val = {
                'custom_analytic_id':self.analytic_account_id.id,
                'name':record.name,
                'price_unit':record.price_unit,
                'tax_id':[(6, 0, tax_id)],
                'discount':record.discount,
                'product_id':record.product_id.id,
                'product_uom_qty':record.product_uom_qty,
                'product_uom':record.product_uom.id,
                'analytic_tag_ids':[(6, 0, analytic_tag_ids)],
                'price_subtotal':record.price_subtotal
                }
                analytic_contract_line = self.env['analytic.contract.line'].sudo().create(val)
                record.custom_analytic_id = self.analytic_account_id.id
            else:
                analytic_tag_ids = [a.id for a in record.analytic_tag_ids]
                tax_id = [a.id for a in record.tax_id]
                val = {
                'custom_analytic_id':self.analytic_account_id.id,
                'name':record.name,
                'price_unit':record.price_unit,
                'tax_id':[(6, 0, tax_id)],
                'discount':record.discount,
                'product_id':record.product_id.id,
                'product_uom_qty':record.product_uom_qty,
                'product_uom':record.product_uom.id,
                'analytic_tag_ids':[(6, 0, analytic_tag_ids)],
                'price_subtotal':record.price_subtotal
                }
                analytic_sale_order_line_id = self.env['analytic.sale.order.line'].sudo().create(val)
        return True
        

    @api.onchange('sale_order_template_id')
    def onchange_sale_order_template_id(self):
        if not self.sale_order_template_id:
            return
        if self.partner_id:
            self = self.with_context(lang=self.partner_id.lang)

        order_lines = [(5, 0, 0)]
        for line in self.sale_order_template_id.sale_order_template_line_ids:
            if self.pricelist_id:
                price = self.pricelist_id.with_context(uom=line.product_uom_id.id).get_product_price(line.product_id, 1, False)
            else:
                price = line.price_unit

            data = {
                'name': line.name,
                'price_unit': price,
                'discount': line.discount,
                'product_uom_qty': line.product_uom_qty,
                'product_id': line.product_id.id,
                'product_uom': line.product_uom_id.id,
                'customer_lead': self._get_customer_lead(line.product_id.product_tmpl_id),
                'template':True
            }
            order_lines.append((0, 0, data))

        self.order_line = order_lines
        self.order_line._compute_tax_id()

        option_lines = []
        for option in self.sale_order_template_id.sale_order_template_option_ids:
            if self.pricelist_id:
                price = self.pricelist_id.with_context(uom=option.uom_id.id).get_product_price(option.product_id, 1, False)
            else:
                price = option.price_unit
            data = {
                'name': option.name,
                'product_id': option.product_id.id,
                'quantity': option.quantity,
                'uom_id': option.uom_id.id,
                'price_unit': price,
                'discount': option.discount,
            }
            option_lines.append((0, 0, data))
        self.sale_order_option_ids = option_lines

        if self.sale_order_template_id.number_of_days > 0:
            self.validity_date = fields.Date.to_string(datetime.datetime.now() + datetime.timedelta(self.sale_order_template_id.number_of_days))

        self.require_payment = self.sale_order_template_id.require_payment

        if self.sale_order_template_id.note:
            self.note = self.sale_order_template_id.note

