# -*- coding: utf-8 -*-
# Part of Browseinfo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Helpdesk Support Ticket Maintenance Request in Odoo',
    'version': '13.0.0.1',
    'category' : 'Website',
    'summary': 'Customer Helpdesk Support Ticket website support ticket Website Help Desk Support Ticket Online ticketing system for customer support service desk helpdesk customer Maintenance helpdesk MRO support ticket MRO website Maintenance MRO construction helpdesk',
    'description': """bi_helpdesk_support_ticket_maintenance
	Maintenance Request from Helpdesk Support Ticket

This app allow you helpdesk team to create Maintenance Request from Ticket/Issue. For more details please see Video.
support request maintenance
maintenance support request
support request for maintenance

support ticket
maintenance support ticket
helpdesk support ticket

 

	
	""",
    'author': 'BrowseInfo',
    'website' : 'https://www.browseinfo.in',
    'price': 9,
    'currency': 'EUR',
    'depends': ['base','website','website_sale','bi_asset_mro_maintenance_management','bi_material_purchase_requisitions','bi_website_job_workorder','bi_website_support_ticket',],
    'data':[
        'security/ir.model.access.csv',
        'views/helpdesk_support_ticket_maintenance_views.xml',
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'live_test_url' : 'https://youtu.be/unX20wrBCxw',
    'images':['static/description/Banner.png'],
}
