from ...isha_crm.models.constants import CHARITABLE_GROUPS, RELIGIOUS_GROUPS
from odoo import models, fields, api


iec_attendance_status = [('Completed','Completed'),
                        ('No Show','No Show'),
                        ('Dropout','Dropout'),
                        ('Revoked','Revoked'),
                        ('Rescheduled','Rescheduled'),('Not Entitled','Not Entitled')]

class IECMegaProgram(models.Model):
    _name = 'iec.mega.pgm'
    _description = 'IEC Mega Program'

    partner_id = fields.Many2one('res.partner', string='Partner ID', required=False,auto_join=True)
    sso_id = fields.Char(string='SSO Id', required=False, index=True)
    prs_id = fields.Char(string='PRS Id', required=False, index=True)
    schedule_id = fields.Many2one('megapgms.program.schedule', string='Schedule')
    initiation_date = fields.Date(related='schedule_id.initiation_schedule.pgmschedule_startdate', store=True)
    session_id = fields.Many2one('megapgms.session.timings', string='Batch')
    reg_date = fields.Datetime(string='Registration Date')
    pgm_amt = fields.Float(string='Program Amount')
    pgm_curr = fields.Char(string='Program Currency')
    txn_id = fields.Char(string='TXN Id', required=False)
    quota_cat = fields.Char(string='Quota')
    ip_country = fields.Char(string='IP country')
    email = fields.Char(string='Email', required=False, index=True)
    name = fields.Char(string='Name', required=False, index=True)
    phone_country_code = fields.Char(string='Phone Country Code')
    phone = fields.Char(string='Phone', required=False, index=True)
    payment_completed = fields.Boolean(default=False, help="The boolean flag to indicate payment.")
    from_schedule_id = fields.Many2one('megapgms.program.schedule', string='From Schedule')
    from_session_id = fields.Many2one('megapgms.session.timings', string='From Batch')
    status = fields.Selection([('inq','Inquired'),('X', 'Transfer'), ('A', 'Confirmed'), ('C', 'Cancelled'),
                               ('Completed', 'Completed'), ('Absent', 'Absent'),
                               ('re_register','Re-Registered')], string='Status')
    volunteer_name = fields.Char(string='Nurturing Volunteer')
    whatsapp_group = fields.Char(string='Whatsapp Group')
    virtual_room = fields.Many2one('iec.virtual.room', string='Virtual Room')
    room_no = fields.Integer(string='Room Number')
    roll_no = fields.Integer(string='Roll Number')
    prog_id = fields.Integer(string='Program Id')
    check_in_attended = fields.Boolean(string='Check-in attended')
    class1_tentative_attendance = fields.Selection(iec_attendance_status,string='Class 1 Tentative Attendance')
    class2_tentative_attendance = fields.Selection(iec_attendance_status,string='Class 2 Tentative Attendance')
    class3_tentative_attendance = fields.Selection(iec_attendance_status,string='Class 3 Tentative Attendance')
    class4_tentative_attendance = fields.Selection(iec_attendance_status,string='Class 4 Tentative Attendance')

    class1_final_attendance = fields.Selection(iec_attendance_status,string='Class 1 Final Attendance')
    class2_final_attendance = fields.Selection(iec_attendance_status,string='Class 2 Final Attendance')
    class3_final_attendance = fields.Selection(iec_attendance_status,string='Class 3 Final Attendance')
    class4_final_attendance = fields.Selection(iec_attendance_status,string='Class 4 Final Attendance')



    # Related Fields
    pgm_tag_ids = fields.Many2many(related='partner_id.pgm_tag_ids')
    category_id = fields.Many2many(related='partner_id.category_id')
    partner_center_id = fields.Many2one(string='Center', related='partner_id.center_id', store=True)
    partner_region_id = fields.Many2one(string='Region', related='partner_center_id.region_id', store=True)
    partner_country_id = fields.Many2one(string='Contact Country', related='partner_id.country_id', store=True)
    partner_pincode = fields.Char(string='Pincode', related='partner_id.zip', store=True)
    partner_dob = fields.Date(string='Contact DOB', related='partner_id.dob', store=True)
    # related fields from res_partner for email marketing
    email_validity = fields.Selection(related='partner_id.email_validity', store=True)
    dnd_email = fields.Boolean(related='partner_id.dnd_email', store=True)
    deceased = fields.Boolean(related='partner_id.deceased', store=True)
    is_caca_donor = fields.Boolean(related='partner_id.is_caca_donor', store=True)
    last_txn_date = fields.Date(related='partner_id.last_txn_date', store=True)
    influencer_type = fields.Selection(string="Influencer Type", related='partner_id.influencer_type', store=True)
    is_meditator = fields.Boolean(related='partner_id.is_meditator')
    ie_date = fields.Date(string='IE Date', related='partner_id.ie_date')
    ieo_r_date = fields.Date(string='IEO Registered date', related='partner_id.ieo_r_date')
    ieo_date = fields.Date(string='IEO Completion date', related='partner_id.ieo_date')

    @api.model
    def read_group(self, domain, fields, groupby, offset=0, limit=None, orderby=False, lazy=True):
        if 'room_no' in fields:
            fields.remove('room_no')
        if 'roll_no' in fields:
            fields.remove('roll_no')
        if 'prog_id' in fields:
            fields.remove('prog_id')
        context = self._context or {}
        if context.get('center_scope'):
            domain += [('partner_center_id', 'in', [False]+self.env.user.centers.ids)]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('partner_id.has_charitable_txn', '=', True)]
            if religious:
                rel_char_domain = (['|', ('partner_id.has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('partner_id.has_religious_txn', '=', True)]
            domain = rel_char_domain + domain

        return super(IECMegaProgram, self).read_group(domain, fields, groupby, offset=offset, limit=limit,
                                                  orderby=orderby, lazy=lazy)


    @api.model
    def _search(self, args, offset=0, limit=None, order=None, count=False, access_rights_uid=None):
        context = self._context or {}
        if context.get('center_scope'):
            args += [('partner_center_id', 'in', [False]+self.env.user.centers.ids)]
            charitable = self.user_has_groups(CHARITABLE_GROUPS)
            religious = self.user_has_groups(RELIGIOUS_GROUPS)
            rel_char_domain = []
            if charitable:
                rel_char_domain += [('partner_id.has_charitable_txn', '=', True)]
            if religious:
                rel_char_domain = (['|', ('partner_id.has_religious_txn', '=', True)] + rel_char_domain) if rel_char_domain \
                    else [('partner_id.has_religious_txn', '=', True)]
            args = rel_char_domain + args

        # default - do not show any results
        if len(args) == 0:
            return []
        return super(IECMegaProgram, self)._search(args, offset, limit, order, count=count, access_rights_uid=access_rights_uid)


    def bulk_assign_eligible_subs(self):
        view = self.env.ref('isha_crm.bulk_eligible_subs_act_window_wizard_form')
        view_id = view and view.id or False
        context = dict(self._context or {},
                       region_scope=True,
                       mapped_field='partner_id',
                       mapped_model='res.partner',
                       restrict_fields='ml_subscription_ids',
                       restrict_values=[('child_ids', '=', False)])
        return {
            'name': 'Bulk Modify Eligible Subscriptions',
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'm2m.tag.manipulation',
            'views': [(view_id, 'form')],
            'view_id': view_id,
            'target': 'new',
            'context': context
        }

class IECResPartner(models.Model):
    _inherit = 'res.partner'

    # IECO Details: Defining this to access this from call campaign
    contact_ieco_id_fkey = fields.One2many('iec.mega.pgm', 'partner_id', 'IECO Record')
