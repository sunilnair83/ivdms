import logging
import traceback

from odoo import models

_logger = logging.getLogger(__name__)


class MailComposer(models.TransientModel):
    _inherit = 'mail.compose.message'

    def action_send_mail(self):
        try:
            if self.model == 'event.registration':
                registrations = self.env[self.model].browse(self._context.get('active_ids'))
                registrations.write(
                        {'state':'email_sent'}
                                    )
        except Exception as ex:
            tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
            print(tb_ex)
        finally:
            return super(MailComposer, self).action_send_mail()
