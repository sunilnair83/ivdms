from odoo import models, fields, api, exceptions
from . import commonmodels
import re


class MegaProgramType(models.Model):
	_name = 'mega.program.type'
	_description = 'Mega Program Type'
	_order = 'programtype'
	_rec_name = 'programtype'
	_sql_constraints = [('unique_programtype','unique(programtype)','Cannot have duplicate program type, give different name')]
		
	# program type information
	#lob = fields.Char(string = 'LOB', size=100, required=True, default='Rejuvenation')
	lob = fields.Many2one('master.lob',string = 'LOB', size=100, required=True)
	programtype = fields.Char(string = 'Program Type', size=100, required=True)
	programcategory = fields.Many2one('master.program.category', string = 'Program category',size=100, required=True)
	active = fields.Boolean('Active', default=True)
	workflowpattern = fields.Selection([('Rejuvenation','Rejuvenation'),('Ashram programs', 'Ashram programs'),('Retreat','Retreat'),('Mega programs','Mega programs')], string='Workflow Pattern', default='Rejuvenation', size=100, required=True)
	numberofdays = fields.Integer(string='Number Of Days', size=50, required=True)
	isresidential  = fields.Selection([('Yes','Yes'), ('No','No')], string='Residential ?', required=True)
	abbrevation = fields.Char(string = 'Abbreviation', size=100, required=True)
	description = fields.Text('Description', size=500, required=True)
	copyconfigparamfrom = fields.Many2one('mega.program.type', string = 'Copy config parameters from', size=500)
	pgmhistorydisplay = fields.Boolean('To display in pgm history',default=False)
	#registrationtype  = fields.Selection([('Register directly','Register directly'), ('Register from WL','Register from WL')], string='Registration type', required=False)

	# finance parameters	
	entity = fields.Many2one('megapgms.entity', string='Entity', required=True)
	entityisoverridable = fields.Boolean('Is Overridable', default=False)
	pgmpurpose = fields.Char(string = 'Program Purpose', size=100, required=True)
	pgmpurposeisoverridable = fields.Boolean('Is Overridable', default=False)
	paymentgateway =  fields.Many2one('megapgms.paymentgateway', string='Payment Gateway', required=True)
	paymentgatewayisoverridable = fields.Boolean('Is Overridable', default=False)
	
	# eligibility parameters
	minimumage = fields.Integer(string='Minimum Age', required=True)
	minimumageisoverridable = fields.Boolean('Is Overridable', default=False)
	gender = fields.Selection([('Male','Male'), ('Female','Female'), ('All','All')], string='Gender', required=True)
	genderisoverridable = fields.Boolean('Is Overridable', default=False)	
	language1 =  fields.Many2one('megapgms.language1', string='Language', required=True)
	languageisoverridable = fields.Boolean('Is Overridable', default=False)
#	prerequisitepractice = fields.Many2many('rejuvenation.practice', relation="ralation_practice_prgtype", string='Prerequisite Practice', required=True)
	prerequisiteprogram = fields.Many2many('master.program.category', relation="megapgm_relation_pgm_category3",string='Pre-requisite Program',size=100)
#	prerequisitepracticeisoverridable = fields.Boolean('Is Overridable', relation="ralation_practice_prgtype1", default=False)
	prerequisiteprogramisoverridable = fields.Boolean('Is Overridable',
													   default=False)
	repetitionallowed = fields.Integer(string='Repetition Allowed', required=False)
	repetitionallowedisoverridable = fields.Boolean('Is Overridable', default=False)
	gapbetweenprgm = fields.Integer(string='Gap Between Two Programs in months', required=True)
	gapbetweenprgmisoverridable = fields.Boolean('Is Overridable', default=False)

	# package parameters
	packageparameters = fields.One2many('megapgm.package.parameters','programtype', string="Package Parameters", required=True, ondelete='cascade')
	practicetaught = fields.Many2many('megapgms.practice',  relation="megampgm_relation_practice_taught",  string='Practices Taught')
	practicetaughtisoverridable = fields.Boolean('Is Overridable', default=False)


	# medical parameters (Health reports)
	#healthreportsneededflag = fields.Boolean(string="Is Health Reports Needed",default=False)
	#healthreportsneeded = fields.Selection([('Yes','Yes'), ('No','No')], string='Health Reports Needed?', required=True)
	#healthreports =  fields.Many2many('rejuvenation.healthreports', string='Health Reports')
	#healthreportsmandatory = fields.Selection([('Yes','Yes'), ('No','No')], string='Health Reports Mandatory?')
	#doctoremail = fields.Char(string = 'Doctor Email', size=100)
	registrationsupport  = fields.Char(string = 'Registration Support Email', size=100)
	#sendautoemailtodr  = fields.Selection([('Yes','Yes'), ('No','No')], string='Send auto email to Dr')

	# notification template
	notificationtemplates = fields.One2many('megapgms.notificationtemplate', 'programtype', string = 'Notification Templates', ondelete='cascade')

	# cancellation rules
	cancellationnoofdays1 = fields.Integer(string='Cancellation Rule 1', required=True)
	cancellationnoofdays2 = fields.Integer(string='Cancellation Rule 2', required=True)
	cancellationnoofdays3 = fields.Integer(string='Cancellation Rule 3', required=True)
	cancellationnoofdays1percent = fields.Float(string='Percentage 1', required=True)
	cancellationnoofdays2percent = fields.Float(string='Percentage 2', required=True)
	cancellationnoofdays3percent = fields.Float(string='Percentage 3', required=True)
	participantreplacement = fields.Float(string='Participant Replacement Charge Percentage', required=True, default=0)

	# note texts
	overridablenote  = fields.Char(string = ' ', size=500, readonly="1", default='Please click on the checkbox if the parameter is overridable at the schedule level')
	cancellationlabel1  = fields.Char(string = ' ', size=100, readonly="1", default='No. of days before')
	cancellationlabel2  = fields.Char(string = ' ', size=100, readonly="1", default='Percentage Of Refund')
	cancellationlabel3 = fields.Char(string = ' ', size=100, readonly="1", default='.')

	@api.model
	def fields_get(self, fields=None):
		fields_to_show = ['programtype','active','workflowpattern','numberofdays','isresidential','abbrevation','description']
		res = super(MegaProgramType, self).fields_get()
		for field in res:
			if (field in fields_to_show):
				res[field]['selectable'] = True	
				res[field]['sortable'] = True
			else:
				res[field]['selectable'] = False
				res[field]['sortable'] = False
		return res

	# basic validations

	@api.constrains('numberofdays')
	def	validate_numberofdays(self):
		if (self.numberofdays <= 0 or self.numberofdays > 99):
			raise exceptions.ValidationError("Field 'Number of Days' should be between 1 to 99")

	@api.constrains('minimumage')
	def	validate_minimumage(self):
		if (self.minimumage < 0 or self.minimumage > 120):
			raise exceptions.ValidationError("Field 'Minimum Age' should be between 0 to 120")

	@api.constrains('repetitionallowed')
	def	validate_repetitionallowed(self):
		if (self.repetitionallowed < 1 or self.repetitionallowed > 999):
			raise exceptions.ValidationError("Field 'Repetition Allowed' should be between 1 to 999")

	@api.constrains('gapbetweenprgm')
	def	validate_gapbetweenprgm(self):
		if (self.gapbetweenprgm < 1 or self.gapbetweenprgm > 12):
			raise exceptions.ValidationError("Field 'Gap Between Two Programs' should be between 1 to 12")

	@api.constrains('cancellationnoofdays1')
	def	validate_cancellationnoofdays1(self):
		if (self.cancellationnoofdays1 < 0 or self.cancellationnoofdays1 > 999):
			raise exceptions.ValidationError("Field 'Cancellation Number Of Days Rule 1' should be between 0 to 999")				

	@api.constrains('cancellationnoofdays2')
	def	validate_cancellationnoofdays2(self):
		if (self.cancellationnoofdays2 < 0 or self.cancellationnoofdays2 > 999):
			raise exceptions.ValidationError("Field 'Cancellation Number Of Days Rule 2' should be between 0 to 999")				

	@api.constrains('cancellationnoofdays3')
	def	validate_cancellationnoofdays3(self):
		if (self.cancellationnoofdays3 < 0 or self.cancellationnoofdays3 > 999):
			raise exceptions.ValidationError("Field 'Cancellation Number Of Days Rule 3' should be between 0 to 999")				


	@api.constrains('cancellationnoofdays1percent')
	def	validate_cancellationnoofdays1percent(self):
		if (self.cancellationnoofdays1percent < 0 or self.cancellationnoofdays1percent > 100):
			raise exceptions.ValidationError("Field 'Cancellation Rule 1 Percentage' should be greater than 0 and less than or equal to 100")				


	@api.constrains('cancellationnoofdays2percent')
	def	validate_cancellationnoofdays2percent(self):
		if (self.cancellationnoofdays2percent < 0 or self.cancellationnoofdays2percent > 100):
			raise exceptions.ValidationError("Field 'Cancellation Rule 2 Percentage' should be greater than 0 and less than or equal to 100")				


	@api.constrains('cancellationnoofdays3percent')
	def	validate_cancellationnoofdays3percent(self):
		if (self.cancellationnoofdays3percent < 0 or self.cancellationnoofdays3percent > 100):
			raise exceptions.ValidationError("Field 'Cancellation Rule 3 Percentage' should be greater than 0 and less than or equal to 100")


	@api.constrains('participantreplacement')
	def	validate_participantreplacement(self):
		if (self.participantreplacement < 0 or self.participantreplacement > 100):
			raise exceptions.ValidationError("Field 'Participant Replacement Charge Percentage' should be greater than 0 and less than or equal to to 100")


	@api.onchange('registrationsupport')
	def registrationsupport_change(self):
		if self.registrationsupport:
			match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$',self.registrationsupport)
			if match == None:
					raise exceptions.ValidationError('Not a valid email address (Registration support Email)')				
	
	@api.model
	def create(self, vals):
		rec = super(MegaProgramType, self).create(vals)
		if (not rec.packageparameters.exists()):
			raise exceptions.ValidationError("Atleast one package information is required")
		return rec
	
	def write(self, vals):
		res = super(MegaProgramType, self).write(vals)
		for rec in self:
			if (not rec.packageparameters.exists()):
				raise exceptions.ValidationError("Atleast one package information is required")
		return res

class MegaProgramPackageParameters(models.Model):
	_name = 'megapgm.package.parameters'
	_description = 'mega pgm Package Parameters'
	#_order = 'packagecode'
	_rec_name = 'packagecode'
	_sql_constraints = [('unique_pkgparameter_packagecode2','unique(programtype, packagecode)','Cannot have duplicate package code, give different code')]

	# package parameters
	programtype = fields.Many2one('mega.program.type',string = 'Program Type')
	packagecode = fields.Char(string = 'Package Code', size=100,required=True)
	packagedisplayname = fields.Char(string = 'Display Name', size=200, required=True)
	packagenoofseats = fields.Integer(string = 'No of Seats',  required=True)
	packageemergencyseats = fields.Integer(string = 'Emergency Seats', required=True)
	packagetotalseats = fields.Integer(string = 'Total Seats',  compute="_compute_packagetotalseats")

	# packagetotalseats = fields.Integer(string = 'Total Seats')
	packagecost = fields.Float(string = 'Cost', required=True)
	# packageseatspaid = fields.Integer(string = 'Booked Seats')
	# packagebalanceseats = fields.Integer(string = 'Balance Available Seats',  compute="_compute_packagebalanceseats")

	@api.constrains('packagenoofseats')
	def validate_packagenoofseats(self):
		for rec in self:
			if (rec.packagenoofseats <= 0 or rec.packagenoofseats > 500000):
				raise exceptions.ValidationError("Field 'Number of Seats' should be between 1 to 500000")

	@api.constrains('packageemergencyseats')
	def validate_packageemergencyseats(self):
		for rec in self:
			if (rec.packageemergencyseats < 0 or rec.packageemergencyseats > 9999):
				raise exceptions.ValidationError("Field 'Emergency Number of Seats' should be between 0 to 9999")

	@api.constrains('packagecost')
	def validate_packagecost(self):
		for rec in self:
			if (rec.packagecost <= 0 or rec.packagecost > 999999999):
				raise exceptions.ValidationError("Field 'Package cost' should be between 1 to 999999999")

	@api.depends('packagenoofseats','packageemergencyseats')
	def _compute_packagetotalseats(self):
		for rec in self:
			rec.packagetotalseats = (rec.packagenoofseats + rec.packageemergencyseats)





		
