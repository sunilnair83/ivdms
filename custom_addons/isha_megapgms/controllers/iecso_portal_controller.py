from odoo.addons.portal.controllers.portal import CustomerPortal

import odoo.http as http
from odoo import _
from odoo.http import request
from odoo.osv.expression import OR


class IecsoPortalController(CustomerPortal):

    def _prepare_portal_layout_values(self):
        values = super(IecsoPortalController, self)._prepare_portal_layout_values()
        return values

    @http.route(['/onlinemegaprograms/iecso'], type='http', auth="user", website=True)
    def query_iecso_data(self, page=1, date_begin=None, date_end=None, sortby=None, search_in='name', search='', **kw):
        if not request.env.user.has_group('isha_crm.group_portal_reg_search') and not request.env.user.has_group('base.group_user'):
            return request.redirect('/my/home')
        values = self._prepare_portal_layout_values()
        partner = request.env.user.partner_id
        searchbar_sortings = {
            'create_date_asc': {'label': _('Created Date (Asc)'), 'order': 'create_date'},
            'create_date_desc': {'label': _('Created Date (Desc)'), 'order': 'create_date desc'},
            'write_date_asc': {'label': _('Updated On (Asc)'), 'order': 'write_date'},
            'write_date_desc': {'label': _('Updated On (Desc)'), 'order': 'write_date desc'},
        }
        searchbar_inputs = {
            'name': {'input': 'name', 'label': _('Search in Contact Name')},
            'email': {'input': 'email', 'label': _('Search in Email')},
            'phone': {'input': 'phone', 'label': _('Search in Phone')},
            # 'ip_country': {'input': 'ip_country', 'label': _('Search in Country')},
            # 'all': {'input': 'all', 'label': _('Search in All')},
        }

        domain = [('status','!=','inq'),('initiation_date','!=', False)] if search else []
        # search
        if search and search_in:
            search_domain = []
            if search_in in ('email', 'all'):
                search_domain = OR([search_domain, [('email', 'ilike', search)]])
            if search_in in ('phone', 'all'):
                search_domain = OR([search_domain, [('phone', 'ilike', search)]])
            if search_in in ('name', 'all'):
                search_domain = OR([search_domain, [('name', 'ilike', search)]])

            domain += search_domain

        # default sort by value
        if not sortby:
            sortby = 'create_date_desc'
        order = searchbar_sortings[sortby]['order']

        # archive_groups = self._get_archive_groups('iec.mega.pgm', domain)
        iec_model_sudo = request.env['iec.mega.pgm'].sudo()

        # count for pager
        # recs_count = iec_model_sudo.search_count(domain)
        # values.update({
        #     'recs_count': recs_count,
        # })
        # make pager
        # pager = portal_pager(
        #     url="/onlinemegaprograms/iecso",
        #     url_args={'date_begin': date_begin, 'date_end': date_end, 'sortby': sortby, 'search_in': search_in,
        #               'search': search},
        #     total=recs_count,
        #     page=page,
        #     step=self._items_per_page
        # )
        # search the count to display, according to the pager data
        # iecso_recs = iec_model_sudo.search(domain, order=order, limit=self._items_per_page,offset=(page - 1) * self._items_per_page)
        iecso_recs = iec_model_sudo.search(domain, order=order, limit=5)

        values.update({
            'iecso_recs': iecso_recs,
            'page_name': 'iecso',
            # 'pager': pager,
            'searchbar_sortings': searchbar_sortings,
            'searchbar_inputs': searchbar_inputs,
            'search_in': search_in,
            'sortby': sortby,
            # 'archive_groups': archive_groups,
            'default_url': '/onlinemegaprograms/iecso',
        })
        return request.render("isha_megapgms.portal_iecso_view", values)
