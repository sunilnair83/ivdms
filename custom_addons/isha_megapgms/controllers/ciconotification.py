import json
import logging
import traceback
import sys
import werkzeug

from datetime import datetime
from dateutil.relativedelta import relativedelta
from math import ceil

from odoo import fields, http, _
from odoo.addons.base.models.ir_ui_view import keep_query
from odoo.exceptions import UserError
from odoo.http import request, content_disposition, Response
from odoo.tools import ustr

#_logger = logging.getLogger(__name__)

class CICONotification(http.Controller):

    @http.route('/megapgms/ciconotify', type='http', auth='public', csrf=False)
    def ciconotify(self, **post):

        print('inside ciconotify')

        try:

            print(post)
            
            requestkey = request.httprequest.headers.get('Authorization')
            apikey = request.env['ir.config_parameter'].sudo().get_param('megapgms.cico_apikey')
            
            if (requestkey != None and requestkey != "" and requestkey != False):
                if (requestkey != apikey):
                    response = {'status': "error", 'error': "Invalid authorization token"}
                    return json.dumps(response)
            else:
                response = {'status': "error", 'error': "Authorization token is missing"}
                return json.dumps(response)

            if (post['request'] != "notify"):
                response = {'status': "error", 'error': "Invalid request type"}
                return json.dumps(response)

            srcid = 0
            if (post["srcId"] != None and post["srcId"] != ""):
                srcid = int(post["srcId"])
            
            if (srcid == 0):
                response = { 'status': "error", 'error': "Source id is empty or missing"}
                return json.dumps(response)

            if (post["checkinDate"] == None or post["checkinDate"] == "" or post["checkinDate"] == False):
                response = { 'status': "error", 'error': "Check-in date is empty or missing"}
                return json.dumps(response)
            
            checkinDate = datetime.strptime(post["checkinDate"], '%Y-%m-%d %H:%M')

            count = request.env['megapgms.registration'].search_count([('id','=', srcid)])

            if (count != 1):
                response = { 'status': "error", 'error': "Invalid source id"}
                return json.dumps(response)

            data = request.env['megapgms.registration'].search([('id','=', srcid)])
            data.write({'checkindatetime': checkinDate})
            
            data = request.env['megapgms.registration'].search([('id','=', srcid)])
            print('date update: ')
            print(str(data.checkindatetime))
            
            response = { 'status': "ok", 'error': ""}
            return json.dumps(response)

        except Exception as e:
            print(e)
            response = {'status': "error", 'error': "Exception occured while processing"}
            return json.dumps(response)