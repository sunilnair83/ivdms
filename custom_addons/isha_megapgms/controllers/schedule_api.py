import datetime
import json
import logging
import traceback
from odoo.addons.muk_rest import tools

import odoo
from odoo.http import request, Response

_logger = logging.getLogger(__name__)
null = None


class SchedulesApiController(odoo.http.Controller):
    # @odoo.http.route(['/api/ieco/getschedule'], type='http', auth="public", website=True)

    @odoo.http.route([
        '/api/ieco/getschedule'
    ], auth="none", type='http', methods=['GET'], csrf=False)
    @tools.common.parse_exception
    @tools.common.ensure_database
    @tools.common.ensure_module()
    @tools.security.protected()
    def get_schedules(self, **kw):
        schedules = request.env['megapgms.program.schedule'].sudo().\
            search([('pgmschedule_startdate','>=',str(datetime.datetime.now().date()))]).sorted(lambda x: x.pgmschedule_startdate)
        response_params = dict()
        for schedule in schedules:
            if schedule.pgmschedule_programtype.programtype in response_params:
                response_params[schedule.pgmschedule_programtype.programtype].append(self.get_schedule_params(schedule))
            else:
                response_params[schedule.pgmschedule_programtype.programtype] = [self.get_schedule_params(schedule)]

        return json.dumps(response_params)

    def get_schedule_params(self, schedule):

        batches = self.get_batch_params(schedule.pgmschedule_session)

        return {
            "scheduleid": schedule.id,
            "programtype": schedule.pgmschedule_programtype.programtype,
            "initiationscheduleid": schedule.initiation_schedule.id,
            "startdate": str(schedule.pgmschedule_startdate),
            "enddate": str(schedule.pgmschedule_enddate),
            "batches": batches
        }


    def get_batch_params(self,batches):
        batch_list = []
        for batch in batches.sorted(lambda x: x.session_timing):
            batch_list.append(
                {
                    'id':batch.id,
                    'time': batch.session_timing,
                    'timezone': batch.timezone,
                    'numberofseats': batch.no_of_seats,
                    'blockedquota': batch.blocked_quota,
                    'generalquota': batch.general_quota
                }
            )
        return batch_list
