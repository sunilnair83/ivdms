import base64
import json
import logging
import traceback
import sys
import werkzeug

from datetime import datetime
from dateutil.relativedelta import relativedelta
from math import ceil

from odoo import fields, http, _
from odoo.addons.base.models.ir_ui_view import keep_query
from odoo.exceptions import UserError
from odoo.http import request, content_disposition, Response
from odoo.tools import ustr
from odoo.fields import Integer


# _logger = logging.getLogger(__name__)

class MegaprogramRegistration(http.Controller):

    # ------------------------------------------------------------
    # ACCESS
    # ------------------------------------------------------------

    def _get_access_data(self, access_token):

        count = request.env['megapgms.registration'].search_count([('access_token', '=', access_token)])

        if (count != 1):
            return request.render("isha_megapgms.403")

        access_data = request.env['megapgms.registration'].search([('access_token', '=', access_token)])

        if ((access_data.registration_status == 'Rejected') or
                (access_data.registration_status == 'Withdrawn') or
                (access_data.registration_status == 'Add To Waiting List') or
                (access_data.registration_status == 'Cancel Approved')):
            return request.render("isha_megapgms.expired")

        ''' has_call_campaign_access, can_caller = False, False

        call_campaign_sudo = request.env['call_campaign.call_campaign'].with_context(active_test=False).sudo().search(
            [('access_token', '=', call_campaign_token)])
        if not call_campaign_sudo.exists():
            return werkzeug.utils.redirect("/")

        if call_campaign_sudo.state == 'closed' or call_campaign_sudo.state == 'draft' or not call_campaign_sudo.active:
            return request.render("call_campaign.call_campaign_expired", {'call_campaign': call_campaign_sudo})

        # If token is not provided, check if user is logged in
        if not caller_token:
            if ensure_token or request.env.user._is_public():
                redirect_url = '/web/login?redirect=%s' % ('/call_campaign/start/%s' % (call_campaign_sudo.access_token))
                return request.render("call_campaign.auth_required",
                                  {'call_campaign': call_campaign_sudo, 'redirect_url': redirect_url})

            # check if user is assigned to the campaign, else throw
            caller_sudo = request.env.user.partner_id.get_caller_id(call_campaign_sudo.id)
        else:
            caller_sudo = request.env['call_campaign.caller'].sudo().search([
                ('call_campaign_id', '=', call_campaign_sudo.id),
                ('token', '=', caller_token)
            ], limit=1)

        if not caller_sudo or not caller_sudo.active:
            return request.render("call_campaign.403", {'call_campaign': call_campaign_sudo})

        return {
            'call_campaign_sudo': call_campaign_sudo,
            'caller_sudo': caller_sudo,
        } '''

        return access_data

    def reirect2haassessment(self, access_token):
        print('inside reirect2haassessment - ' + access_token)
        try:
            count = request.env['megapgms.registration'].search_count([('access_token', '=', access_token)])

            if (count != 1):
                print('invalid access token - count - ' + str(count))
                return request.render("isha_megapgms.403")

            access_data = request.env['megapgms.registration'].search([('access_token', '=', access_token)])

            if (access_data.ha_status != 'Request More Info'):
                print('invalid has status - ' + access_data.ha_status)
                return request.render("isha_megapgms.invalidrequest")

            # if (access_data.question_bank):
            print('redirecting to qa page')
            return request.render("isha_megapgms.registration_ha", {'object': access_data})
            # else:
            #    print('question bank not available, move to invalid request')
            #    return request.render("isha_megapgms.invalidrequest")



        except Exception as e:
            print(e)
            return request.render("isha_megapgms.exception")

    # ------------------------------------------------------------
    # TAKING ROUTES
    # ------------------------------------------------------------

    @http.route('/megaprograms/registration/<string:access_token>', type='http', auth='public', website=True,
                csrf=False)
    def registration_start(self, access_token, **post):
        print('inside registration start')
        print(access_token)
        access_data = self._get_access_data(access_token)
        if isinstance(access_data, Response):
            return access_data

        print('calling render')

        if (access_data.ha_status == 'Request More Info'):
            print('request info')
            access_data = self.reirect2haassessment(access_token)
            if isinstance(access_data, Response):
                return access_data
        else:
            mode = 'START'
            selected_package = request.env['megapgms.program.schedule.roomdata'].browse(
                access_data.packageselection.id)
            # selected_package.pgmschedule_packagebalanceseats = 0
            bal_seats=0
            if selected_package:
                if (access_data.use_emrg_quota == True):
                    bal_seats = selected_package.pgmschedule_packageemergencyseats - selected_package.pgmschedule_emergencyseatspaid
                else:
                    bal_seats = selected_package.pgmschedule_packagenoofseats - selected_package.pgmschedule_regularseatspaid

            newpackages = request.env['megapgms.program.schedule.roomdata'].search(
                [('pgmschedule_regularseatsbalance', '>', 0),
                 ('pgmschedule_programname', '=', access_data.programapplied.id), ('id', '!=', selected_package.id)])
            if (access_data.payment_status == 'Initiated'):
                mode = 'START'
            else:
                if (bal_seats <= 0):
                    print('seats not available')
                    if newpackages:
                        print('Package available')
                        mode = 'CHANGE PACKAGE'
                    else:
                        mode = 'NO PACKAGE'

            termlist = request.env['megapgms.terms_and_conditions'].search([])
            balanceamount = 0 # request.env['megapgms.payments']._getBalanceAmount(access_data, selected_package)
            return request.render("isha_megapgms.registration_general",
                                  {'object': access_data, 'terms': termlist[0],
                                   'countries': request.env['res.country'].search([]), 'packages': newpackages,
                                   'selectedpack': selected_package, 'mode': mode, 'balanceamount': balanceamount})

    @http.route('/megaprograms/registration/initiatepayment/<string:access_token>', type='http', auth='public',
                website=True, csrf=False)
    def registration_initiatepayment(self, access_token, **post):

        print('Terms Submit')
        print(access_token)
        print(self)
        print(post)

        try:
            count = request.env['megapgms.registration'].search_count([('access_token', '=', access_token)])

            if (count != 1):
                return request.render("isha_megapgms.403")

            access_data = request.env['megapgms.registration'].search([('access_token', '=', access_token)])

            if (access_data.registration_status != 'Payment Link Sent'):
                return request.render("isha_megapgms.invalidrequest")

            reg_ag = False
            btn_clk = False

            for rec in post:
                print(rec)
                print(post[rec])
                if (rec == 'registration_agreed' and post[rec] == 'true'):
                    reg_ag = True
                    continue
                elif (rec == 'button_submit' and post[rec] == 'terms'):
                    btn_clk = True
                    continue

            if (reg_ag == True and btn_clk == True):
                print('submit clicked')
                return request.render("isha_megapgms.registration_paymentpage",
                                      {'object': access_data, 'countries': request.env['res.country'].search([])})
            else:
                termlist = request.env['megapgms.terms_and_conditions'].search([])
                return request.render("isha_megapgms.registrationterms",
                                      {'object': access_data, 'terms': termlist[0]})

        except Exception as e:
            print(e)
            return request.render("isha_megapgms.exception")

    @http.route('/megaprograms/registration/confirm/<string:access_token>', type='http', auth='public', website=True,
                csrf=False)
    def registration_confirm_twinroom(self, access_token, **post):
        print('Terms Submit')
        print(access_token)
        print(self)
        print(post)
        try:
            count = request.env['megapgms.registration'].search_count([('access_token', '=', access_token)])

            if (count != 1):
                return request.render("isha_megapgms.403")

            access_data = request.env['megapgms.registration'].search([('access_token', '=', access_token)])

            if (access_data.registration_status != 'Paid'):
                return request.render("isha_megapgms.invalidrequest")

            acc_person = 0
            acc_person_text = ''
            acc_email = ''
            btn_clk = False

            for rec in post:
                print(rec)
                print(post[rec])
                if (rec == 'accompany_name'):
                    acc_person = post[rec]
                    continue
                elif (rec == 'accompny_email'):
                    acc_email = post[rec]
                    continue
                elif (rec == 'accompany_name_text'):
                    acc_person_text = post[rec]
                elif (rec == 'button_add' and post[rec] == 'add'):
                    btn_clk = True
                    continue

            if (btn_clk == True):
                print('add clicked')
                sel_person = request.env['megapgms.registration'].browse(int(acc_person))
                access_data.write({'accompanying_name': sel_person.id, 'accompanying_email': acc_email,
                                   'accompanying_name_text': acc_person_text})
                return request.render("isha_megapgms.registrationsuccess", {'object': access_data})
            else:
                return request.render("isha_megapgms.expired")

        except Exception as e:
            print(e)
            return request.render("isha_megapgms.exception")

    @http.route('/megaprograms/registration/refund/<string:access_token>', type='http', auth='public', website=True,
                csrf=False)
    def registration_cancel_refund(self, access_token, **post):
        print(post)

        try:
            count = request.env['megapgms.registration'].search_count([('access_token', '=', access_token)])

            if (count != 1):
                return request.render("isha_megapgms.403")

            access_data = request.env['megapgms.registration'].search([('access_token', '=', access_token)])

            if (access_data.registration_status != 'Paid' and access_data.registration_status != 'Confirmed'):
                return request.render("isha_megapgms.invalidrequest")

            if (post['button_submit'] == 'refund'):
                print('refund requst')
                access_data.write({'registration_status': 'Cancel Applied', 'date_withdrawn': fields.Date.today(),
                                   'cancelapplied_emailsms_pending': True})
                return request.render("isha_megapgms.cancelrefundsuccess")
            elif (post['button_submit'] == 'cop'):
                print('change of participant request')
                return request.redirect('/megaprograms/registrationform?iscop=True&cop=%s' % (access_data.access_token))
                # fullprofileresponsedatVar = request.env['megapgms.ssoapi'].get_fullprofileresponse_DTO()
                # return request.render('isha_megapgms.pgmregn', {
                #     'programregn': request.env['megapgms.registration'].search([]),
                #     'countries': request.env['res.country'].search([]),
                #     'doctypes': request.env['documenttype'].search([]),
                #     'states': request.env['res.country.state'].search([]),
                #     #               Changing to list the ishangam program categories instead of moksha program types
                #     #               'prgregnprogramnametypes': request.env['mega.program.type'].search([]),
                #     'prgregnprogramnamecategories': request.env['master.program.category'].search(
                #         [('keyprogram', '=', True)]),
                #     'programschedules': request.env['megapgms.program.schedule'].search([]),
                #     'programscheduleroomdatas': request.env['megapgms.program.schedule.roomdata'].search([]),
                #     'submitted': post.get('submitted', False),
                #     'current_page':1,
                #     'filled_pages':1,
                #     'isofname': fullprofileresponsedatVar,
                #     'ischangeofparticipant': True,
                #     'changeofparticipant_ref': access_data.access_token
                # })
            else:
                return request.render("isha_megapgms.invalidrequest")

        except Exception as e:
            print(e)
            return request.render("isha_megapgms.exception")

    @http.route('/megaprograms/registration/hapost/<string:access_token>', type='http', auth='public',
                methods=["GET", "POST"], website=True, csrf=False)
    def ha_assessment_submit(self, access_token, **post):
        print('inside ha_assessment_submit - %s' % access_token)

        postdata = post
        print(postdata)

        try:
            count = request.env['megapgms.registration'].search_count([('access_token', '=', access_token)])

            if (count != 1):
                return request.render("isha_megapgms.403")

            access_data = request.env['megapgms.registration'].search([('access_token', '=', access_token)])

            if (access_data.ha_status != 'Request More Info'):
                return request.render("isha_megapgms.invalidrequest")

            btn_clk = False
            qa_data = access_data.question_bank
            add_info = access_data.addinfo_needed_answer

            print('loop begins')

            filledotherattr = 0
            for rec in postdata:
                print(rec)
                if (rec == 'button_submit' and postdata[rec] == 'hasubmit'):
                    btn_clk = True
                elif (rec == 'addinfo_needed'):
                    add_info = postdata[rec]
                elif (
                        rec == 'pgmupldphoto' or rec == 'pgmupldphoto1' or rec == 'pgmupldphoto2' or rec == 'pgmupldphoto3' or rec == 'pgmupldphoto4'):
                    continue
                else:
                    if (filledotherattr == 0):
                        for id in qa_data:
                            stridstr = str(id.id)
                            if (stridstr in postdata):
                                id.answer_text = postdata[stridstr]
                        filledotherattr = 1

            print(postdata)

            attached_filephoto = request.httprequest.files.getlist('pgmupldphoto')
            attached_filephoto1 = request.httprequest.files.getlist('pgmupldphoto1')
            attached_filephoto2 = request.httprequest.files.getlist('pgmupldphoto2')
            attached_filephoto3 = request.httprequest.files.getlist('pgmupldphoto3')
            attached_filephoto4 = request.httprequest.files.getlist('pgmupldphoto4')

            doctyperec = request.env['documenttype'].search([('name', '=', 'HA Assessment')])
            doctypeid = 0
            if doctyperec:
                doctypeid = doctyperec.id

            attfile1 = False
            attname1 = False
            attfile2 = False
            attname2 = False
            attfile3 = False
            attname3 = False
            attfile4 = False
            attname4 = False
            attfile5 = False
            attname5 = False

            afile1 = False
            afile2 = False
            afile3 = False
            afile4 = False
            afile5 = False

            if attached_filephoto:
                read1 = attached_filephoto[0].read()
                datas1 = base64.b64encode(read1)

                if datas1:
                    attfile1 = datas1
                    attname1 = attached_filephoto[0].filename
                    afile1 = True
                    # new_attachment1 = request.env['ir.attachment'].sudo().create({
                    #     'name': attached_filephoto[0].filename,
                    #     'datas': datas1,
                    #     'res_model': 'megapgm.request.attachment',
                    #     'res_id': access_data.id
                    # })

                    # request.env['megapgm.request.attachment'].sudo().create({
                    #     'generic_request_id': access_data.id,
                    #     'attachment_type_id': doctypeid,
                    #     'attached_file': new_attachment1,
                    # })

                    ntmp = request.env['megapgms.participant.hadocs'].create({
                        'participant': access_data.id,
                        'ha_attachment': datas1,
                        'ha_filename': attached_filephoto[0].filename,
                    })

            if attached_filephoto1:
                read2 = attached_filephoto1[0].read()
                datas2 = base64.b64encode(read2)

                if datas2:
                    attfile2 = datas2
                    attname2 = attached_filephoto1[0].filename
                    afile2 = True
                    # new_attachment2 = request.env['ir.attachment'].sudo().create({
                    #     'name': attached_filephoto1[0].filename,
                    #     'datas': datas2,
                    #     'res_model': 'megapgm.request.attachment',
                    #     'res_id': access_data.id
                    # })

                    # request.env['megapgm.request.attachment'].sudo().create({
                    #     'generic_request_id': access_data.id,
                    #     'attachment_type_id': doctypeid,
                    #     'attached_file': new_attachment2,
                    # })     

                    request.env['megapgms.participant.hadocs'].create({
                        'participant': access_data.id,
                        'ha_attachment': datas2,
                        'ha_filename': attached_filephoto1[0].filename,
                    })

            if attached_filephoto2:
                read3 = attached_filephoto2[0].read()
                datas3 = base64.b64encode(read3)

                if datas3:
                    attfile3 = datas3
                    attname3 = attached_filephoto2[0].filename
                    afile3 = True
                    # new_attachment3 = request.env['ir.attachment'].sudo().create({
                    #     'name': attached_filephoto2[0].filename,
                    #     'datas': datas3,
                    #     'res_model': 'megapgm.request.attachment',
                    #     'res_id': access_data.id
                    # })

                    # request.env['megapgm.request.attachment'].sudo().create({
                    #     'generic_request_id': access_data.id,
                    #     'attachment_type_id': doctypeid,
                    #     'attached_file': new_attachment3,
                    # })   

                    request.env['megapgms.participant.hadocs'].create({
                        'participant': access_data.id,
                        'ha_attachment': datas3,
                        'ha_filename': attached_filephoto2[0].filename,
                    })

            if attached_filephoto3:
                read4 = attached_filephoto3[0].read()
                datas4 = base64.b64encode(read4)

                if datas4:
                    attfile4 = datas4
                    attname4 = attached_filephoto3[0].filename
                    afile4 = True
                    # new_attachment4 = request.env['ir.attachment'].sudo().create({
                    #     'name': attached_filephoto3[0].filename,
                    #     'datas': datas4,
                    #     'res_model': 'megapgm.request.attachment',
                    #     'res_id': access_data.id
                    # })

                    # request.env['megapgm.request.attachment'].sudo().create({
                    #     'generic_request_id': access_data.id,
                    #     'attachment_type_id': doctypeid,
                    #     'attached_file': new_attachment4,
                    # })

                    request.env['megapgms.participant.hadocs'].create({
                        'participant': access_data.id,
                        'ha_attachment': datas4,
                        'ha_filename': attached_filephoto3[0].filename,
                    })

            if attached_filephoto4:
                read5 = attached_filephoto4[0].read()
                datas5 = base64.b64encode(read5)

                if datas5:
                    attfile5 = datas5
                    attname5 = attached_filephoto4[0].filename
                    afile5 = True
                    # new_attachment5 = request.env['ir.attachment'].sudo().create({
                    #     'name': attached_filephoto4[0].filename,
                    #     'datas': datas5,
                    #     'res_model': 'megapgm.request.attachment',
                    #     'res_id': access_data.id
                    # })

                    # request.env['megapgm.request.attachment'].sudo().create({
                    #     'generic_request_id': access_data.id,
                    #     'attachment_type_id': doctypeid,
                    #     'attached_file': new_attachment5,
                    # })

                    request.env['megapgms.participant.hadocs'].create({
                        'participant': access_data.id,
                        'ha_attachment': datas5,
                        'ha_filename': attached_filephoto4[0].filename,
                    })

            print('a')
            print(str(btn_clk))
            if (btn_clk == True):
                access_data.write(
                    {'question_bank': qa_data, 'ha_status': 'HA Resubmitted', 'addinfo_needed_answer': add_info,
                     'ha_resubmitted': True, 'ha_resubmit_date': fields.Date.today()})
                return request.render("isha_megapgms.registration_hasuccess", {'object': access_data})

        except Exception as e:
            print(e)
            return request.render("isha_megapgms.exception")

    @http.route('/megaprograms/inquiry', type='http', auth='public', website=True, csrf=False)
    def rejuvenation_inquiry(self, **post):
        print('Inquiry')
        try:
            # availableprograms = request.env['megapgms.program.schedule'].search([('pgmschedule_registrationopen','=',True),('pgmschedule_startdate','>',fields.Date.today().strftime('%Y-%m-%d'))])
            availableprograms = request.env['megapgms.program.schedule'].search(
                [('pgmschedule_registrationopen', '=', True)])
            selectedprogram = False
            if availableprograms:
                print(availableprograms)
                return request.render("isha_megapgms.inquiry_open",
                                      {'availableprograms': availableprograms, 'selectedprogram': selectedprogram,
                                       'countries': request.env['res.country'].search([])})
            else:
                return request.render("isha_megapgms.inquiry_open",
                                      {'availableprograms': availableprograms, 'selectedprogram': selectedprogram,
                                       'countries': request.env['res.country'].search([])})

        except Exception as e:
            print(e)
            return request.render("isha_megapgms.exception")

    @http.route('/megaprograms/inquirysubmit', type='http', auth='public', website=True, csrf=False)
    def rejuvenation_inquirysubmit(self, **post):
        print('inquirysubmit')
        print(post)

        try:
            sel_prg = 0
            btn_clk = False
            btn_chg = False

            first_name = False
            last_name = False
            countrycode = False
            city = False
            country = False
            mobile = False
            email = False
            comments = False

            for rec in post:
                print(rec)
                print(post[rec])
                if (rec == 'selected_prg'):
                    sel_prg = post[rec]
                    continue
                elif (rec == 'button_submit' and post[rec] == 'submit'):
                    btn_clk = True
                    continue
                elif (rec == 'button_onchange' and post[rec] == 'change'):
                    btn_chg = True
                    continue
                elif (rec == 'first_name'):
                    first_name = post[rec]
                elif (rec == 'last_name'):
                    last_name = post[rec]
                elif (rec == 'countrycode'):
                    countrycode = post[rec]
                elif (rec == 'city'):
                    city = post[rec]
                elif (rec == 'country'):
                    country = post[rec]
                elif (rec == 'mobile'):
                    mobile = post[rec]
                elif (rec == 'email'):
                    email = post[rec]
                elif (rec == 'coments'):
                    comments = post[rec]

            # availableprograms = request.env['megapgms.program.schedule'].search([('pgmschedule_registrationopen','=',True),('pgmschedule_startdate','>',fields.Date.today().strftime('%Y-%m-%d'))])
            availableprograms = request.env['megapgms.program.schedule'].search(
                [('pgmschedule_registrationopen', '=', True)])
            if availableprograms:
                print('Programs available')
            else:
                return request.render("isha_megapgms.registrationnopackages")

            if (btn_chg == True):
                print('prg selected  clicked')

                selected_prg = request.env['megapgms.program.schedule'].browse(int(sel_prg))
                return request.render("isha_megapgms.inquiry_open",
                                      {'availableprograms': availableprograms, 'selectedprogram': selected_prg})

            elif (btn_clk == True):
                print('prg selected and submit clicked')
                selected_prg = request.env['megapgms.program.schedule'].browse(int(sel_prg))

                nrec = request.env['megapgms.inquiry'].create(
                    {'first_name': first_name, 'last_name': last_name, 'countrycode': countrycode, 'mobile': mobile,
                     'email': email, 'city': city, 'country': country, 'comments': comments,
                     'selected_program': selected_prg.id, 'inquiry_status': 'Inquiry'})
                if nrec:
                    return request.render("isha_megapgms.inquirysuccess")
                else:
                    return request.render("isha_megapgms.expired")
            else:
                return request.render("isha_megapgms.expired")

        except Exception as e:
            stacktrace = traceback.print_exc()
            sys.stderr.write(str(stacktrace))
            print(stacktrace)
            '''request.env['megapgms.tempdebuglog'].sudo().create({
            'logtext': str(stacktrace)
            })'''
            return request.render("isha_megapgms.exception")

    @http.route('/megaprograms/registration/changeofparticipant/<string:access_token>', type='http', auth='public',
                website=True, csrf=False)
    def registration_changeofparticipant(self, access_token, **post):
        print('inside registration_changeofparticipant')
        try:
            count = request.env['megapgms.registration'].search_count([('cop_access_token', '=', access_token)])
            if (count != 1):
                return request.render("isha_megapgms.403")

            access_data = request.env['megapgms.registration'].search([('cop_access_token', '=', access_token)])
            refund_data = request.env['megapgms.payments']._calculateRefundAmount(access_data, datetime.now())

            if ((not access_data.show_cop) or (refund_data['days_before'] <= 0) or refund_data['transfercharge'] <= 0):
                return request.render("isha_megapgms.invalidrequest")

            access_data.write({'refund_eligible_ondaysbefore': refund_data['days_before'],
                               'refund_eligible_onpercentage': refund_data['elg_percent'],
                               'refund_eligible': refund_data['elg_amt']})

            return request.render("isha_megapgms.changeofparticipant",
                                  {'object': access_data, 'object1': {'transfercharge': refund_data['transfercharge']}})

        except Exception as e:
            print(e)
            return request.render("isha_megapgms.exception")

    @http.route('/megaprograms/registration/changeofparticipant/apply/<string:access_token>', type='http',
                auth='public', website=True, csrf=False)
    def registration_changeofparticipant_apply(self, access_token, **post):
        print('inside registration_changeofparticipant_apply')
        try:
            print(access_token)
            count = request.env['megapgms.registration'].search_count([('cop_access_token', '=', access_token)])
            if (count != 1):
                print(count)
                return request.render("isha_megapgms.403")

            access_data = request.env['megapgms.registration'].search([('cop_access_token', '=', access_token)])
            refund_data = request.env['megapgms.payments']._calculateRefundAmount(access_data, datetime.now())

            if ((not access_data.show_cop) or (refund_data['days_before'] <= 0) or refund_data['transfercharge'] <= 0):
                return request.render("isha_megapgms.invalidrequest")

            access_data.write({'refund_eligible_ondaysbefore': refund_data['days_before'],
                               'refund_eligible_onpercentage': refund_data['elg_percent'],
                               'refund_eligible': refund_data['elg_amt']})

            return request.redirect('/megaprograms/registrationform?iscop=True&cop=%s' % (access_data.cop_access_token))

        except Exception as e:
            print(e)
            return request.render("isha_megapgms.exception")

    @http.route('/megaprograms/registration/packageupgrade/<string:access_token>', type='http', auth='public',
                website=True, csrf=False)
    def registration_packageupgrade(self, access_token, **post):
        print('inside registration_packageupgrade')
        try:
            count = request.env['megapgms.registration.transactions'].search_count(
                [('upgrade_access_token', '=', access_token)])
            if (count != 1):
                return request.render("isha_megapgms.403")

            access_data = request.env['megapgms.registration.transactions'].search(
                [('upgrade_access_token', '=', access_token)])

            if (access_data.isactive == False):
                return request.render("isha_megapgms.expired")

            result = request.env['megapgms.commonvalidations'].checkPackageUpgradeEligiblity(
                access_data.programregistration)
            if (result == False):
                return request.render("isha_megapgms.expired")

            balanceamount = request.env['megapgms.payments']._getBalanceAmountForPackageUpgrade(access_data)

            if balanceamount > 0:
                termlist = request.env['megapgms.terms_and_conditions'].search([])
                return request.render("isha_megapgms.packageupgrade",
                                      {'object': access_data, 'balanceamount': balanceamount, 'terms': termlist[0],
                                       'countries': request.env['res.country'].search([])})
            else:
                return request.render("isha_megapgms.exception")

        except Exception as e:
            return request.render("isha_megapgms.exception")

