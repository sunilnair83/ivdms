import base64
import json
from datetime import date, timedelta, datetime

from odoo import http
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.fields import Integer
from odoo.http import request
from odoo.tools import ustr
import logging
import traceback
import requests

_logger = logging.getLogger(__name__)


class MegapgmsRegistration(http.Controller):
    #@http.route(['/payment/authorize/return/', ], type='http', auth='public', csrf=False)
    #def authorize_form_feedback(self, **post):
    @http.route('/onlinemegaprograms/registrationform/', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def registrationform(self, legal_entity="", consent_grant_status="", id_token="", testsso="0", program_id="0", language="english", **post):
        fullprofileresponsedatVar = request.env['megapgms.ssoapi'].get_fullprofileresponse_DTO()
        consentgrantStatusflagvar = True
        if request.httprequest.method == "GET":
            validation_errors = []
            diseases_list = self.get_disease_list()
            values = {}
            try:
                result_countries = request.env['res.country'].sudo().search(
                    [ ('name', '=', 'India')])

                profresponse = json.dumps(fullprofileresponsedatVar)
                values = {
                    #'programregn': request.env['megapgms.registration'].search([]),
                    'countries': result_countries,
                    'doctypes': request.env['documenttype'].sudo().search([]),
                    'states': request.env['res.country.state'].sudo().search([]),
                    #Changing to list the ishangam program categories instead of moksha program types
                    #'prgregnprogramnametypes': request.env['mega.program.type'].search([]),
                    'prgregnprogramnamecategories': request.env['master.program.category'].sudo().search(
                        [('keyprogram', '=', True)]),
                    'programschedules': request.env['megapgms.program.schedule'].sudo().search([]),
                    'programscheduleroomdatas': request.env['megapgms.program.schedule.roomdata'].sudo().search([]),
                    'submitted': post.get('submitted', False),
                    'current_page': 1,
                    'filled_pages': 1,
                    'isofname': fullprofileresponsedatVar,
                    'ischangeofparticipant': post.get('iscop', False),
                    'changeofparticipant_ref': post.get('cop', ''),
                    'ssoadproofuri': '',
                    'isoidproofuri': '',
                    'inputobj': profresponse,
                    'enableishaautofill': consentgrantStatusflagvar,
                    'consent_grant_status': '1', #consent_grant_status,
                    'program_id': program_id,
                    'diseases': diseases_list,
                }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception:
                _logger.error(
                    "Error caught during request creation", exc_info=True)

            values['validation_errors'] = validation_errors
            if (language == 'english'):
                return request.render('isha_megapgms.pgmregn', values)
            elif (language == 'tamil'):
                return request.render('isha_megapgms.pgmregntamil', values)
            elif (language == 'hindi'):
                return request.render('isha_megapgms.pgmregnhindi', values)
        elif request.httprequest.method == "POST":
            validation_errors = []
            values = {}
            first_data = []
            pgmrecs = []
            try:
                programapplied = post.get('programapplied')
                packageselection = post.get('packageselection')
                first_name = post.get('first_name')
                last_name = post.get('last_name')
                name_called = post.get('name_called')
                participant_email = post.get('participant_email')
                gender = post.get('gender')
                marital_status = post.get('marital_status')
                dob = post.get('dob')
                address_line = post.get('address_line')
                occupation = post.get('occupation')
                country = post.get('country')
                nationality_id = post.get('nationality_id')
                state = post.get('state')
                if (state):
                    state = int(state)
                else:
                    state = ''
                pincode = post.get('pincode')
                city = post.get('city')
                countrycode = post.get('countrycode')
                phone = post.get('phone')
                education_qualification = post.get('education_qualification')
                passportnumber = post.get('passportnumber')
                isopnumber = 'YES' if post.get('jspgmregfmisopnumyesno', None) else 'NO'
                opnumber = post.get('opnumber')
                if post.get('passportexpirydate') == '':
                    passportexpirydate = False
                else:
                    passportexpirydate = post.get('passportexpirydate')
                visanumber = post.get('visanumber')
                if post.get('visaexpirydate') == '':
                    visaexpirydate = False
                else:
                    visaexpirydate = post.get('visaexpirydate')

                # start of change of participant

                print('checking cop..')

                ischangeofparticipant = False
                changeofparticipant_applied_dt = False
                changeofparticipant_ref = None
                transferred_amount = 0
                copapplied_emailsms_pending = False

                if post.get('ischangeofparticipant') != '':
                    if (post.get('ischangeofparticipant') == "True"):
                        access_data = request.env['megapgms.registration'].search(
                            [('cop_access_token', '=', post.get('changeofparticipant_ref'))])
                        if (access_data.exists()):
                            if (
                                    access_data.registration_status == 'Paid' or access_data.registration_status == 'Confirmed'):
                                print('applying change of participant')
                                access_data.write({
                                    'registration_status': 'Change Of Participant',
                                    'cop_access_token': ''
                                })
                                request.env['megapgms.payments']._reverseSeat(access_data)
                                ischangeofparticipant = True
                                changeofparticipant_applied_dt = datetime.now()
                                changeofparticipant_ref = access_data.id
                                if (access_data.amount_paid > 0):
                                    percentage = access_data.programapplied.pgmschedule_programtype.participantreplacement
                                    if (percentage > 0):
                                        charges = round(((access_data.amount_paid / 100) * percentage), 2)
                                        transferred_amount = (access_data.amount_paid - charges)
                                copapplied_emailsms_pending = True
                            else:
                                print('data status not matched..')
                                return request.render("isha_megapgms.invalidrequest")
                        else:
                            print('data doesnt exits..')
                            return request.render("isha_megapgms.invalidrequest")

                # end of change of participant

                print('checking other info')

                idproofid = post.get('pgmupldid'),
                addressproofid = post.get('pgmupldaddproofid'),
                attached_filephoto = request.httprequest.files.getlist('pgmupldphoto')
                attached_fileidproof = request.httprequest.files.getlist('pgmupldIdproof')
                attached_fileaddproof = request.httprequest.files.getlist('pgmupldAddressproof')
                # attached_prooftype = request.httprequest.form.getlist('new_prooftype[]')
                if consentgrantStatusflagvar == True:
                    # fullprofileresponsedatVar['basicProfile']['firstName'] = first_name
                    # fullprofileresponsedatVar['basicProfile']['lastName'] = last_name
                    # pmsfullresponse_dto['basicProfile']['dob'] = dob
                    fullprofileresponsedatVar['basicProfile']['gender'] = gender.upper()
                    # fullprofileresponsedatVar['basicProfile']['email'] = participant_email
                    # pmsfullresponse_dto['basicProfile']['phone']['countryCode'] = countrycode
                    # pmsfullresponse_dto['basicProfile']['phone']['number'] = phone
                    # pmsfullresponse_dto['basicProfile']['addresses']['pincode'] = pincode
                    # pmsfullresponse_dto['basicProfile']['addresses']['city'] = city
                    # isofname['basicProfile']['addresses']['state']
                    # pgregoutputobj = post.get('jspgregoutputobjid')
                    # request.env['megapgms.ssoapi'].post_fullprofile_info(pgregoutputobj, profileidvar, legal_entity)
                reg_dict = {
                    'programapplied': int(programapplied),
                    'packageselection': packageselection,
                    'first_name': first_name,
                    'last_name': last_name,
                    'name_called': name_called,
                    'participant_email': participant_email,
                    'gender': gender,
                    'marital_status': marital_status,
                    'dob': dob,
                    'address_line': address_line,
                    'occupation': occupation,
                    'city': city,
                    'country': int(country),
                    'nationality_id': int(nationality_id),
                    'state': state,
                    'pincode': pincode,
                    'countrycode': countrycode,
                    'phone': phone,
                    'opnumber': opnumber,
                    'isopnumber': isopnumber,
                    'education_qualification': education_qualification,
                    # 'passportnumber': passportnumber,
                    # 'passportexpirydate': passportexpirydate,
                    # 'visanumber': visanumber,
                    # 'visaexpirydate': visaexpirydate,
                    'registration_status': 'Payment Link Sent',
                    'incompleteregistration_pagescompleted': 1,
                    'incompleteregistration_email_send': False,
                    'incompleteregistration_sms_send': False,
                    'registration_email_send': True,
                    'registration_sms_send': True,
                    'ischangeofparticipant': ischangeofparticipant,
                    'changeofparticipant_applied_dt': changeofparticipant_applied_dt,
                    'changeofparticipant_ref': changeofparticipant_ref,
                    'transferred_amount': transferred_amount,
                    'copapplied_emailsms_pending': copapplied_emailsms_pending
                }
                disease_list = [x for x in post if 'is_disease' in x]
                if len(disease_list)>0:
                    for disease in disease_list:
                        reg_dict.update({disease:'YES'} )
                recdyn = request.env['megapgms.registration'].sudo().create(reg_dict)
                # for x in range(len(attached_file)):.sudo().create
                first_data = request.env['megapgms.registration'].search([('access_token', '=', recdyn.access_token)])
                progtypevar = first_data.programapplied.pgmschedule_programtype
# Change here to use the countries_allowed field from program type instead of  progtypevar.description
                consultationprog = progtypevar.description.upper().__contains__("ONLINE")
                rejuvenationtype = progtypevar.workflowpattern.upper() == 'REJUVENATION' or progtypevar.workflowpattern.upper() == 'MEGA PROGRAMS'
                documentsnotavaiableflg = consultationprog and rejuvenationtype
                read1 = attached_filephoto[0].read()
                datas1 = base64.b64encode(read1)
                if datas1:
                    new_attachment1 = request.env['ir.attachment'].sudo().create({
                        'name': 'photo_file',
                        'datas': datas1,
                        # 'datas_fname': attached.filename,
                        'res_field': 'photo_file',
                        'res_model': 'megapgms.registration',
                        'res_id': recdyn.id
                    })

                    # request.env['megapgm.request.attachment'].sudo().create({
                    #     'generic_request_id': recdyn.id,
                    #     'attachment_type_id': 1,
                    #     'attached_file': new_attachment1,
                    #     # 'idproof_file': attached_filename[x]
                    # })
                if not documentsnotavaiableflg:
                    isoidproofavailflg = False
                    isoidproofuri = ""
                    for objvar in fullprofileresponsedatVar["documents"]:
                        if objvar["docTag"] == "ID_PROOF" and objvar[
                            "uri"] is not None:  # jspgmregisoidproff is True and 'docType' in objvar:
                            isoidproofavailflg = True
                            isoidproofuri = objvar["uri"]
                    if attached_fileidproof.__len__() > 0:  # jspgmregisoidproff is not
                        read2 = attached_fileidproof[0].read()
                        datas2 = base64.b64encode(read2)
                    else:
                        read2 = request.env['megapgms.ssoapi'].get_ssoimage_froms3(isoidproofuri);
                        datas2 = base64.b64encode(read2)

                    if datas2:
                        new_attachment2 = request.env['ir.attachment'].sudo().create({
                            'name': 'idproof_file',
                            'datas': datas2,
                            # 'datas_fname': attached.filename,
                            'res_field': 'idproof_file',
                            'res_model': 'megapgms.registration',
                            'res_id': recdyn.id
                        })

                        # request.env['megapgm.request.attachment'].sudo().create({
                        #     'generic_request_id': recdyn.id,
                        #     'attachment_type_id': idproofid,
                        #     'attached_file': new_attachment2,
                        #     # 'idproof_file': attached_filename[x]
                        # })
                    ssoadproofavailflg = False
                    ssoadproofuri = ""
                    for objvar in fullprofileresponsedatVar["documents"]:
                        if objvar["docTag"] == "ADDRESS_PROOF" and objvar[
                            "uri"] is not None:  # jspgmregisoidproff is True and 'docType' in objvar:
                            ssoadproofavailflg = True
                            ssoadproofuri = objvar["uri"]
                    if attached_fileaddproof.__len__() > 0:  # jspgmregisoidproff is not
                        read3 = attached_fileaddproof[0].read()
                        datas3 = base64.b64encode(read3)
                    else:
                        read3 = request.env['megapgms.ssoapi'].get_ssoimage_froms3(ssoadproofuri);
                        datas3 = base64.b64encode(read3)

                    if datas3:
                        new_attachment3 = request.env['ir.attachment'].sudo().create({
                            'name': 'addrproof_file',
                            'datas': datas3,
                            # 'datas_fname': attached.filename,
                            'res_field': 'addrproof_file',
                            'res_model': 'megapgms.registration',
                            'res_id': recdyn.id
                        })

                        # request.env['megapgm.request.attachment'].sudo().create({
                        #     'generic_request_id': recdyn.id,
                        #     'attachment_type_id': addressproofid,
                        #     'attached_file': new_attachment3,
                        #     # 'idproof_file': attached_filename[x]
                        # })
                        # first_data = request.env['megapgms.registration'].search(
                        #   [('access_token', '=', recdyn.access_token)])

                contact_id = first_data.contact_id_fkey.id
                print('contact_id_fkey:', contact_id)
                program_category = ['Inner Engineering', 'Bhava Spandana', 'Shoonya', 'Hatha Yoga', 'Samyama']
                pgmrecs = first_data.getProgramAttendance(contact_id, program_category)
                print('pgmrecs from getProgramAttendance:', pgmrecs)
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception as ex:
                stacktrace = traceback.print_exc()
                request.env['megapgms.tempdebuglog'].sudo().create({
                    'logtext': ex
                })
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)

            if not validation_errors:
                if len(disease_list)==0:
                    return request.redirect(recdyn.public_url,code=302)
                progtype = request.env['megapgms.program.schedule'].search(
                    [('id', '=', int(programapplied))]).pgmschedule_programtype.programtype
                if (True):#progtype == 'Medical Consultation Review' or progtype == 'IECO - Preliminary class'
                    first_data.incompleteregistration_pagescompleted = 5
                    first_data.registration_status = "Payment Link Sent"
                    first_data.registration_email_send = False
                    first_data.registration_sms_send = False
                    values = {'object': first_data}
                    return request.render("isha_megapgms.registrationsuccess2", values)
                else:
                    state_onlyindia = request.env['res.country.state'].search([('country_id', '=', 104)])
                    values = {
                        'pgmrecs': pgmrecs,
                        'programregn': first_data,
                        'countries': request.env['res.country'].search([]),
                        'states': state_onlyindia,
                        'prgregncenters': request.env['isha.center'].search([]),
                        # 'prgregnprogramnametypes': request.env['mega.program.type'].search([]),
                        'prgregnprogramnamecategories': request.env['master.program.category'].search(
                            [('keyprogram', '=', True)]),
                        'programschedules': request.env['megapgms.program.schedule'].search([]),
                        'programscheduleroomdatas': request.env['megapgms.program.schedule.roomdata'].search([]),
                        'submitted': post.get('submitted', False),
                        'current_page': 2,
                        'filled_pages': 2,
                        'firstnamevar': first_data.first_name,
                    }
                    values['validation_errors'] = validation_errors
                    return request.render("isha_megapgms.pgmregn2", values)
            else:
                _logger.log(25, validation_errors)
                logtext = 'Date:' + datetime.now().strftime("%m/%d/%Y, %H:%M:%S") + ': ' + ' '.join(
                    [str(elem) for elem in validation_errors])
                request.env['megapgms.tempdebuglog'].sudo().create({
                    'logtext': logtext
                })
                return request.render('isha_megapgms.exception')

    @http.route('/onlinemegaprograms/registrationform2', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def registrationform2(self, access_token, **post):
        first_data = request.env['megapgms.registration'].search([('access_token', '=', access_token)])
        if (first_data.incompleteregistration_pagescompleted == 5):
            values = {'object': first_data}
            return request.render("isha_megapgms.registrationcompletion", values)

        if first_data.access_token != None and request.httprequest.method == "POST":
            values = {}
            validation_errors = []
            try:
                countrycode1 = post.get('countrycode1')
                countrycode2 = post.get('countrycode2')
                name1 = post.get('name1')
                name2 = post.get('name2')
                phone1 = post.get('phone1')
                phone2 = post.get('phone2')
                relationship1 = post.get('relationship1')
                relationship2 = post.get('relationship2')
                email1 = post.get('emergencyemail1')
                email2 = post.get('emergencyemail2')
                programnames = request.httprequest.form.getlist('programname')
                pgmdates = request.httprequest.form.getlist('pgmdate')
                teachernames = request.httprequest.form.getlist('teachername')
                pgmcountrys = request.httprequest.form.getlist('pgmcountry')
                pgmstates = request.httprequest.form.getlist('pgmstate')
                pgmlocations = request.httprequest.form.getlist('pgmlocation')
                pgmhistorycollec = [(0, 0, {
                    'programname': programname,
                    'pgmdate': pgmdate,
                    'teachername': teachername,
                    'pgmcountry': pgmcountry,
                    'pgmstate': pgmstate,
                    'pgmlocation': pgmlocation
                }) for programname, pgmdate, teachername, pgmcountry, pgmstate, pgmlocation in
                                    zip(programnames, pgmdates, teachernames, pgmcountrys, pgmstates, pgmlocations)]
                otherpractices = post.get('otherpractices')

                first_data.name1 = name1
                first_data.name2 = name2
                first_data.phone1 = countrycode1 + '-' + phone1
                first_data.phone2 = countrycode2 + '-' + phone2
                first_data.relationship1 = relationship1
                first_data.relationship2 = relationship2
                first_data.email1 = email1
                first_data.email2 = email2
                first_data.otherpractices = otherpractices
                if (pgmdates[0] != ''):
                    first_data.programhistory = pgmhistorycollec
                first_data = request.env['megapgms.registration'].search([('access_token', '=', access_token)])
                first_data.incompleteregistration_pagescompleted = 2

                values = {
                    'programregn': first_data,
                    'submitted': post.get('submitted', False),
                    'countries': request.env['res.country'].search([]),
                    'current_page': 3,
                    'filled_pages': 3,
                }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception as ex:
                _logger.error(
                    "Error caught during request creation", ex, exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")

            if not validation_errors:
                values['validation_errors'] = validation_errors
                return request.render("isha_megapgms.pgmregn3", values)
            else:
                _logger.log(25, validation_errors)
                logtext = 'Date:' + datetime.now().strftime("%m/%d/%Y, %H:%M:%S") + ': ' + ' '.join(
                    [str(elem) for elem in validation_errors])
                request.env['megapgms.tempdebuglog'].sudo().create({
                    'logtext': logtext
                })
                return request.render('isha_megapgms.exception')
        elif first_data.access_token != None and request.httprequest.method == "GET":
            validation_errors = []
            values = {}
            contact_id = first_data.contact_id_fkey.id
            program_category = request.env['master.program.category'].search(
                [('keyprogram', '=', True)])
            ishangamcategories = []
            i = 0
            for val in program_category:
                ishangamcategories.insert(i,val.programcategory)
                i = i + 1

        #    program_category = ['Inner Engineering', 'Bhava Spandana', 'Shoonya', 'Hatha Yoga', 'Samyama']
            pgmrecs = first_data.getProgramAttendance(contact_id, ishangamcategories)
            state_onlyindia = request.env['res.country.state'].search([('country_id', '=', 104)])

            try:
                values = {
                    'pgmrecs': pgmrecs,
                    'programregn': first_data,
                    'countries': request.env['res.country'].search([]),
                    'states': state_onlyindia,
                    'prgregncenters': request.env['isha.center'].search([]),
                    'prgregnprogramnamecategories': request.env['master.program.category'].search(
                        [('keyprogram', '=', True)]),
                    'programschedules': request.env['megapgms.program.schedule'].search([]),
                    'programscheduleroomdatas': request.env['megapgms.program.schedule.roomdata'].search([]),
                    'submitted': post.get('submitted', False),
                    'current_page': 2,
                    'filled_pages': 2,
                    'firstnamevar': first_data.first_name,
                }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception as ex:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")

            values['validation_errors'] = validation_errors
            return request.render("isha_megapgms.pgmregn2", values)

    @http.route('/onlinemegaprograms/registrationform3', type='http', auth="public", methods=["GET", "POST"], website=True,
                csrf=False)
    def registrationform3(self, access_token, **post):
        first_data = request.env['megapgms.registration'].search([('access_token', '=', access_token)])
        if (first_data.incompleteregistration_pagescompleted == 5):
            values = {'object': first_data}
            return request.render("isha_megapgms.registrationcompletion", values)

        if first_data.access_token != None and request.httprequest.method == "POST":
            values = {}
            validation_errors = []
            try:
                height = post.get('height')
                weight = post.get('weight')
                nameoffamilydr = post.get('nameoffamilydr')
                familydrcountrycode = post.get('drcountrycode')
                familydrcontact = post.get('familydrcontact')
                familydremail = post.get('familydremail')
                if post.get('dateofdrvisit') == '':
                    dateofdrvisit = False
                else:
                    dateofdrvisit = post.get('dateofdrvisit')
                isfood = post.get('isfood')
                isfamily = post.get('isfamily')
                iswork = post.get('iswork')
                isclimate = post.get('isclimate')
                food_details = post.get('food_details')
                family_details = post.get('family_details')
                work_details = post.get('work_details')
                climate_details = post.get('climate_details')
                foodallergy = post.get('foodallergy')
                chemicalallergy = post.get('chemicalallergy')
                environmentallergy = post.get('environmentallergy')
                otherallergy = post.get('otherallergy')
                noneofthebelow = post.get('jsprgregnonedisordersid')
                isheadandneckproblems = post.get('isheadandneckproblems')
                isheartdisorders = post.get('isheartdisorders')
                ishypertension = post.get('ishypertension')
                isdiabetesmellitus = post.get('isdiabetesmellitus')
                isthyroid = post.get('isthyroid')
                isotherendocrinologydisorders = post.get('isotherendocrinologydisorders')
                isliverdisorders = post.get('isliverdisorders')
                iskidneydisorders = post.get('iskidneydisorders')
                isbrainandnervousdisorders = post.get('isbrainandnervousdisorders')
                iseyedisorders = post.get('iseyedisorders')
                iseardisorders = post.get('iseardisorders')
                isnosedisorders = post.get('isnosedisorders')
                isthroatissues = post.get('isthroatissues')
                ischestlungissues = post.get('ischestlungissues')
                isintestinaldisorders = post.get('isintestinaldisorders')
                isstomachdisorders = post.get('isstomachdisorders')
                isurinaryproblem = post.get('isurinaryproblem')
                isbackissues = post.get('isbackissues')
                iscirculationproblem = post.get('iscirculationproblem')
                isskindisorders = post.get('isskindisorders')
                isjointdisorders = post.get('isjointdisorders')
                ispaintordiscomfort = post.get('ispaintordiscomfort')
                isinfertility = post.get('isinfertility')
                iscancer = post.get('iscancer')
                isautoimmunediesase = post.get('isautoimmunediesase')
                isulcersinthemouth = post.get('isulcersinthemouth')
                ishivaids = post.get('ishivaids')
                isherpestype1 = post.get('isherpestype1')
                isherpestype2 = post.get('isherpestype2')
                istuberculosis = post.get('istuberculosis')
                ishepatitis = post.get('ishepatitis')
                isanyothecommunicabledisease = post.get('isanyothecommunicabledisease')
                ispsychiatrydisorders = post.get('ispsychiatrydisorders')
                headandneck_details = post.get('headandneck_details')
                hypertension_details = post.get('hypertension_details')
                heartdisorder_details = post.get('heartdisorder_details')
                diabetesmellitus_details = post.get('diabetesmellitus_details')
                thyroid_details = post.get('thyroid_details')
                otherendocrinologydisorders_details = post.get('otherendocrinologydisorders_details')
                liverdisorders_details = post.get('liverdisorders_details')
                kidneydisorders_details = post.get('kidneydisorders_details')
                brainandnervousdisorders_details = post.get('brainandnervousdisorders_details')
                eyedisorders_details = post.get('eyedisorders_details')
                eardisorders_details = post.get('eardisorders_details')
                nosedisorders_details = post.get('nosedisorders_details')
                throatissues_details = post.get('throatissues_details')
                chestlungissues_details = post.get('chestlungissues_details')
                intestinaldisorders_details = post.get('intestinaldisorders_details')
                stomachdisorders_details = post.get('stomachdisorders_details')
                urinaryproblem_details = post.get('urinaryproblem_details')
                backissues_details = post.get('backissues_details')
                circulationproblem_details = post.get('circulationproblem_details')
                skindisorders_details = post.get('skindisorders_details')
                jointdisorders_details = post.get('jointdisorders_details')
                pain_details = post.get('pain_details')
                infertility_details = post.get('infertility_details')
                cancer_details = post.get('cancer_details')
                autoimmune_details = post.get('autoimmune_details')
                ulcersinmouth_details = post.get('ulcersinmouth_details')
                hivaids_details = post.get('hivaids_details')
                herpestype1_details = post.get('herpestype1_details')
                herpestype2_details = post.get('herpestype2_details')
                tuberculosis_details = post.get('tuberculosis_details')
                hepatitis_details = post.get('hepatitis_details')
                anyothecommunicabledisease_details = post.get('anyothecommunicabledisease_details')
                psychiatrydisorders_details = post.get('psychiatrydisorders_details')

                pgmishalifedates = request.httprequest.form.getlist('pgmishalifedate')
                pgmishalifenames = request.httprequest.form.getlist('pgmishalifename')
                pgmishalifeplaces = request.httprequest.form.getlist('pgmishalifeplace')
                pgmishalifetreatmentnames = request.httprequest.form.getlist('pgmishalifetreatmentname')
                if (len(pgmishalifedates) == 1 and pgmishalifedates[0] == ""):
                    ishalifetreatmentcollec = []
                else:
                    ishalifetreatmentcollec = [(0, 0, {
                        'treatmentdate': pgmishalifedate,
                        'treatmentplace': pgmishalifeplace,
                        'condition': pgmishalifename,
                        'treatmentname': pgmishalifetreatmentname,
                    }) for pgmishalifedate, pgmishalifename, pgmishalifeplace, pgmishalifetreatmentname in
                                               zip(pgmishalifedates, pgmishalifenames, pgmishalifeplaces,
                                                   pgmishalifetreatmentnames)]

                pgmmedicalhistoryconditions = request.httprequest.form.getlist('pgmmedicalhistorycondition')
                pgmmedicalhistorydurations = request.httprequest.form.getlist('pgmmedicalhistoryduration')
                pgmmedicalhistorytreatments = request.httprequest.form.getlist('pgmmedicalhistorytreatment')
                currentmedicalhistorycollect = [(0, 0, {
                    'current_nameofcomplaint': pgmcondition,
                    'current_duration': pgmduration,
                    'current_condition': pgmtreatment,
                }) for pgmcondition, pgmduration, pgmtreatment in
                                                zip(pgmmedicalhistoryconditions, pgmmedicalhistorydurations,
                                                    pgmmedicalhistorytreatments)]

                pgmallopathynames = request.httprequest.form.getlist('pgmallopathyname')
                pgmallopathydoses = request.httprequest.form.getlist('pgmallopathydose')
                pgmallopathydurations = request.httprequest.form.getlist('pgmallopathyduration')
                currentallopathymedicationscollect = [(0, 0, {
                    'current_allopathyname': pgmname,
                    'current_allopathydose': pgmdose,
                    'current_allopathyduration': pgmduration,
                }) for pgmname, pgmdose, pgmduration in
                                                      zip(pgmallopathynames, pgmallopathydoses, pgmallopathydurations)]
                pgmalternatenames = request.httprequest.form.getlist('pgmalternatename')
                pgmalternatedoses = request.httprequest.form.getlist('pgmalternatedose')
                pgmalternatedurations = request.httprequest.form.getlist('pgmalternateduration')
                currentalternatemedicationscollect = [(0, 0, {
                    'current_alternatename': pgmname,
                    'current_alternatedose': pgmdose,
                    'current_alternateduration': pgmduration,
                }) for pgmname, pgmdose, pgmduration in
                                                      zip(pgmalternatenames, pgmalternatedoses, pgmalternatedurations)]

                pgmallergydrugnames = request.httprequest.form.getlist('pgmallergydrugname')
                allergyreactions = request.httprequest.form.getlist('pgmallergyreaction')
                medicationallergiescollect = [(0, 0, {
                    'allergydrugname': pgmname,
                    'allergyreaction': pgmdose,
                }) for pgmname, pgmdose in zip(pgmallergydrugnames, allergyreactions)]

                first_data.familydremail = familydremail
                first_data.height = height
                first_data.weight = weight
                first_data.nameoffamilydr = nameoffamilydr
                first_data.familydrcountrycode = familydrcountrycode
                first_data.familydrcontact = familydrcontact
                first_data.dateofdrvisit = dateofdrvisit
                first_data.isfood = isfood
                first_data.isfamily = isfamily
                first_data.iswork = iswork
                first_data.isclimate = isclimate
                first_data.food_details = food_details
                first_data.work_details = work_details
                first_data.climate_details = climate_details
                first_data.foodallergy = foodallergy
                first_data.chemicalallergy = chemicalallergy
                first_data.family_details = family_details
                first_data.otherallergy = otherallergy
                first_data.environmentallergy = environmentallergy
                first_data.noneofthebelow = noneofthebelow
                first_data.isheadandneckproblems = isheadandneckproblems
                first_data.isheartdisorders = isheartdisorders
                first_data.ishypertension = ishypertension
                first_data.isdiabetesmellitus = isdiabetesmellitus
                first_data.isthyroid = isthyroid
                first_data.isotherendocrinologydisorders = isotherendocrinologydisorders
                first_data.isliverdisorders = isliverdisorders
                first_data.isbrainandnervousdisorders = isbrainandnervousdisorders
                first_data.iseyedisorders = iseyedisorders
                first_data.iseardisorders = iseardisorders
                first_data.isnosedisorders = isnosedisorders
                first_data.isthroatissues = isthroatissues
                first_data.ischestlungissues = ischestlungissues
                first_data.isintestinaldisorders = isintestinaldisorders
                first_data.isurinaryproblem = isurinaryproblem
                first_data.isbackissues = isbackissues
                first_data.iscirculationproblem = iscirculationproblem
                first_data.isskindisorders = isskindisorders
                first_data.iskidneydisorders = iskidneydisorders
                first_data.isjointdisorders = isjointdisorders
                first_data.ispaintordiscomfort = ispaintordiscomfort
                first_data.isinfertility = isinfertility
                first_data.iscancer = iscancer
                first_data.isautoimmunediesase = isautoimmunediesase
                first_data.isulcersinthemouth = isulcersinthemouth
                first_data.ishivaids = ishivaids
                first_data.isherpestype1 = isherpestype1
                first_data.isherpestype2 = isherpestype2
                first_data.isstomachdisorders = isstomachdisorders
                first_data.istuberculosis = istuberculosis
                first_data.ishepatitis = ishepatitis
                first_data.isanyothecommunicabledisease = isanyothecommunicabledisease
                first_data.ispsychiatrydisorders = ispsychiatrydisorders

                first_data.headandneck_details = headandneck_details
                first_data.hypertension_details = hypertension_details
                first_data.heartdisorder_details = heartdisorder_details
                first_data.diabetesmellitus_details = diabetesmellitus_details
                first_data.thyroid_details = thyroid_details
                first_data.otherendocrinologydisorders_details = otherendocrinologydisorders_details
                first_data.liverdisorders_details = liverdisorders_details
                first_data.kidneydisorders_details = kidneydisorders_details
                first_data.brainandnervousdisorders_details = brainandnervousdisorders_details
                first_data.eyedisorders_details = eyedisorders_details
                first_data.eardisorders_details = eardisorders_details
                first_data.nosedisorders_details = nosedisorders_details
                first_data.throatissues_details = throatissues_details
                first_data.chestlungissues_details = chestlungissues_details
                first_data.intestinaldisorders_details = intestinaldisorders_details
                first_data.urinaryproblem_details = urinaryproblem_details
                first_data.backissues_details = backissues_details
                first_data.circulationproblem_details = circulationproblem_details
                first_data.skindisorders_details = skindisorders_details
                first_data.jointdisorders_details = jointdisorders_details
                first_data.pain_details = pain_details
                first_data.infertility_details = infertility_details
                first_data.cancer_details = cancer_details
                first_data.autoimmune_details = autoimmune_details
                first_data.ulcersinmouth_details = ulcersinmouth_details
                first_data.hivaids_details = hivaids_details
                first_data.herpestype1_details = herpestype1_details
                first_data.herpestype2_details = herpestype2_details
                first_data.stomachdisorders_details = stomachdisorders_details
                first_data.tuberculosis_details = tuberculosis_details
                first_data.hepatitis_details = hepatitis_details
                first_data.anyothecommunicabledisease_details = anyothecommunicabledisease_details
                first_data.psychiatrydisorders_details = psychiatrydisorders_details
                if (ishalifetreatmentcollec != []):
                    first_data.ishalifetreatments = ishalifetreatmentcollec
                if (pgmmedicalhistoryconditions[0] != ""):
                    first_data.currentmedicalhistory = currentmedicalhistorycollect
                if (pgmallopathynames[0] != ""):
                    first_data.currentallopathymedications = currentallopathymedicationscollect
                if (pgmalternatenames[0] != ""):
                    first_data.currentalternatemedications = currentalternatemedicationscollect
                if (pgmallergydrugnames[0] != ""):
                    first_data.medicationallergies = medicationallergiescollect

                first_data.incompleteregistration_pagescompleted = 3
                first_data = request.env['megapgms.registration'].search([('access_token', '=', access_token)])
                values = {
                    'programregn': first_data,
                    'submitted': post.get('submitted', False),
                    'current_page': 4,
                    'filled_pages': 4,
                    'gender': first_data.gender,
                    'firstnamevar': nameoffamilydr,
                }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")

            if not validation_errors:
                values['validation_errors'] = validation_errors
                return request.render("isha_megapgms.pgmregn4", values)
            else:
                _logger.log(25, validation_errors)
                logtext = 'Date:' + datetime.now().strftime("%m/%d/%Y, %H:%M:%S") + ' ' + ' '.join(
                    [str(elem) for elem in validation_errors])
                request.env['megapgms.tempdebuglog'].sudo().create({
                    'logtext': logtext
                })
                return request.render('isha_megapgms.exception')

        elif first_data.access_token != None and request.httprequest.method == "GET":
            validation_errors = []
            values = {}
            try:
                values = {
                    'programregn': first_data,
                    'submitted': post.get('submitted', False),
                    'countries': request.env['res.country'].search([]),
                    'current_page': 3,
                    'filled_pages': 3,
                    'firstnamevar': first_data.name1,
                }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")

            values['validation_errors'] = validation_errors
            return request.render("isha_megapgms.pgmregn3", values)

    @http.route('/onlinemegaprograms/get_center', type='http', auth="public", methods=['GET'], website=True, sitemap=False)
    def scope_location_data(self, country_id=None, state_id=None, **post):

        state_ids = {}
        center_ids = {}
        if country_id:
            # For india return the possible states
            if int(country_id) == 104:
                state_data = request.env['res.country.state'].search_read(
                    domain=[('country_id', '=', int(country_id))],
                    fields=['id', 'name']
                )
                for state in state_data:
                    state_ids[str(state['id'])] = state['name']

            else:
                # For non india get the list of centers from i_p_c and r_c
                center_data = request.env['isha.pincode.center'].search_read(
                    domain=[('country_id', '=', int(country_id))],
                    fields=['center_id']
                )

                center_data += request.env['res.country'].search_read(
                    domain=[('id', '=', int(country_id))],
                    fields=['center_id']
                )

                for center in center_data:
                    if center['center_id']:
                        center_ids[str(center['center_id'][0])] = True
        elif state_id:
            # For direct state selection get the center from r_c_s directly
            center_data = request.env['res.country.state'].search_read(
                domain=[('id', '=', int(state_id))],
                fields=['center_ids']
            )
            for state in center_data:
                for center in state['center_ids']:
                    center_ids[str(center)] = True

        result = {'state_ids': state_ids, 'center_ids': center_ids}
        return json.dumps(result)

    def get_disease_list(self):
        mp_model = request.env['megapgms.registration']

        field_list = [f for f in mp_model._fields if f.startswith("is_disease")]

        result = [
            {
                'name':mp_model._fields[f].string,
                'key':f,
                'selected': 'Yes'
            }

            for f in field_list
        ]

        return result
