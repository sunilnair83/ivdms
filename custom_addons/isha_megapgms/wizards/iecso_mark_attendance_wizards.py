from odoo import api, fields, models
import csv, base64, logging, datetime

from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

class IecsoMarkAttendance(models.TransientModel):
	_name = 'iecso.mark.attendance'
	_description = 'IECSO Mark Attendance'

	file_selection = fields.Selection([('tentative','IEO + Proctor'),('final','Consolidated'),('checkin','Check-In')],string='File Source')
	file1 = fields.Binary(string='IEO File')
	file2 = fields.Binary(string='Proctor File')
	file3 = fields.Binary(string='Consolidated File')
	attendance_class = fields.Selection([('1','Class 1'),('2','Class 2'),('3','Class 3'),('4','Class 4')],string='Attendance class')
	door_open_ts = fields.Datetime(string='Door Open Time')
	class_end_ts = fields.Datetime(string='Class End Time')
	threshold = fields.Float(string='Threshold')


	def action_process_csv(self):
		sql = ''
		sso_dict = {}
		if self.file_selection == 'checkin':
			ieo_reader = csv.DictReader(base64.b64decode(self.file1).decode('utf-8').split('\n'))
			for row in ieo_reader:
				if float(row['percentage']) >= self.threshold:
					sql += "update iec_mega_pgm set check_in_attended = true where sso_id = \'%s\' and status in ('A','X');\n" % (
							row['sso_id'])
			_logger.info('update the check-in start')
			self.env.cr.execute(sql)
			self.env.cr.commit()
			_logger.info('update the check-in end')

		if self.file_selection == 'tentative':
			ieo_reader = csv.DictReader(base64.b64decode(self.file1).decode('utf-8').split('\n'))
			for row in list(ieo_reader):
				sso_dict[row['ssoId']] = {'ieo': row}

			proctor_reader = csv.DictReader(base64.b64decode(self.file2).decode('utf-8').split('\n'))
			for row in list(proctor_reader):
				try:
					sso_dict[row['ssoId']]['proc'] = row
				except Exception as e:
					pass
					#raise UserError(row['ssoId']+' is not present in IEO file. Plz cross verify.')


			for key in sso_dict:
				try:
					ieo = sso_dict[key]['ieo']
					proc = sso_dict[key]['proc']
				except Exception as e:
					continue
				if ieo['status'] == 'kickout':
					sql += "update iec_mega_pgm set class%s_tentative_attendance = \'Kick Out\', roll_no = \'%s\', room_no = \'%s\' where sso_id = \'%s\' and status !=\'inq\';" % (
						self.attendance_class, proc['rollNumber'], proc['roomId'], ieo['ssoId'])
					continue

				if ieo['status'] == 'dropout':
					sql += "update iec_mega_pgm set class%s_tentative_attendance = \'Drop Off\', roll_no = \'%s\', room_no = \'%s\'  where sso_id = \'%s\' and status !=\'inq\';" % (
						self.attendance_class, proc['rollNumber'], proc['roomId'], ieo['ssoId'])
					continue

				try:
					parsed_proc_ts = datetime.datetime.strptime(proc['lastLoggedInTime'],
																'%Y-%m-%dT%H:%M:%S.%fZ').strftime('%Y-%m-%d %H:%M:%S')
				except Exception as ex:
					sql += "update iec_mega_pgm set  roll_no = \'%s\', room_no = \'%s\'  where sso_id = \'%s\' and status !=\'inq\';" % (
						 proc['rollNumber'], proc['roomId'], ieo['ssoId'])
					continue

				try:
					parsed_ieo_ts = datetime.datetime.strptime(ieo['lastheartbeat_dt'],
															   '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d %H:%M:%S')
				except Exception as ex:
					sql += "update iec_mega_pgm set  roll_no = \'%s\', room_no = \'%s\'  where sso_id = \'%s\' and status !=\'inq\';" % (
						proc['rollNumber'], proc['roomId'], ieo['ssoId'])
					continue


				if (ieo['status'] == 'attended' or parsed_proc_ts > self.door_open_ts.strftime('%Y-%m-%d %H:%M:%S')
					) and parsed_ieo_ts > self.class_end_ts.strftime('%Y-%m-%d %H:%M:%S'):
					sql += "update iec_mega_pgm set class%s_tentative_attendance = \'Attended\', roll_no = \'%s\', room_no = \'%s\' where sso_id = \'%s\' and status !=\'inq\';" % (
						self.attendance_class, proc['rollNumber'], proc['roomId'], ieo['ssoId'])
					continue

				if (ieo['status'] == 'attended' or parsed_proc_ts > self.door_open_ts.strftime('%Y-%m-%d %H:%M:%S')
					) and parsed_ieo_ts < self.class_end_ts.strftime('%Y-%m-%d %H:%M:%S'):
					sql += "update iec_mega_pgm set class%s_tentative_attendance = \'Drop Off\', roll_no = \'%s\', room_no = \'%s\' where sso_id = \'%s\' and status !=\'inq\';" % (
						self.attendance_class, proc['rollNumber'], proc['roomId'], ieo['ssoId'])
					continue
				if (ieo['status'] == 'allowed' and parsed_proc_ts < self.door_open_ts.strftime('%Y-%m-%d %H:%M:%S')
					) and parsed_ieo_ts < self.door_open_ts.strftime('%Y-%m-%d %H:%M:%S'):
					sql += "update iec_mega_pgm set class%s_tentative_attendance = \'No Show\', roll_no = \'%s\', room_no = \'%s\' where sso_id = \'%s\' and status !=\'inq\';" % (
						self.attendance_class, proc['rollNumber'], proc['roomId'], ieo['ssoId'])
					continue

			_logger.info('IECSO mark attendance start')
			self.env.cr.execute(sql)
			self.env.cr.commit()
			_logger.info('IECSO mark attendance end')

		elif self.file_selection == 'final':
			f_reader = csv.DictReader(base64.b64decode(self.file3).decode('utf-8').split('\n'))
			for row in list(f_reader):
				sql += "update iec_mega_pgm set class%s_final_attendance = \'%s\', roll_no = \'%s\', room_no = \'%s\', prog_id = \'%s\' where sso_id = \'%s\' and status !=\'inq\';" % (self.attendance_class,row['Class '+str(self.attendance_class)+' Final Attendance'],row['Roll Number'],row['Room Number'],row['Program Id'],row['SSO Id'])

			_logger.info('IECSO mark attendance start')
			self.env.cr.execute(sql)
			self.env.cr.commit()
			_logger.info('IECSO mark attendance end')
