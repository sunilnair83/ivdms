from odoo import models, fields, api, exceptions
from odoo.exceptions import except_orm

class HAApproval(models.TransientModel):
	_name = 'megaprogram.registration.ha'
	_description = 'HA Assessment'

	participant_name = fields.Char(string = 'Participant Name')
	programapplied = fields.Many2one('megapgms.program.schedule',string = 'Applied Program')
	selected_package = fields.Many2one('megapgms.program.schedule.roomdata', string='Requested Package')
	program_startdate = fields.Date(string = 'Start Date')
	program_enddate = fields.Date(string = 'End Date')
	ha_status = fields.Selection([('Pending','Pending'),('HA Approved','HA Approved'),('Rejected','Rejected'),('Request More Info','Request More Info'),('HA Resubmitted','HA Resubmitted')], string='HA Status', default='Pending', size=100)
	registration_status = fields.Selection([('Pending','Pending'),('Rejected','Rejected'),('Payment Link Sent','Payment Link Sent'),('Add To Waiting List','Add To Waiting List'),('Cancel Applied','Cancel Applied'),('Cancel Approved','Cancel Approved'),('Paid','Paid'),('Confirmed','Confirmed'),('Approved','Approved'),('Withdrawn','Withdrawn')], string='Registration Status',  size=100)
    
	question_bank = fields.Many2many('megapgms.participant.qa', string = 'HA Assessment QA')
	addinfo_needed =  fields.Text(string = 'Additional Info Needed')
	addinfo_needed_answer =  fields.Text(string = 'Additional Info Needed')
	ha_resubmit_date = fields.Date()
	ha_resubmitted = fields.Boolean(string="HA Resubmitted") 
	show_submit = fields.Boolean(string="Show Submit", default=True)

	@api.onchange('participant_name')
	def participant_name_onchange(self):
		print('welcome')
		active_id = self.env.context.get('default_active_id')
		party = self.env["megapgms.registration"].browse(active_id)
		if party:
			self.programapplied = party.programapplied
			self.selected_package = party.packageselection
			self.question_bank = party.question_bank
			self.addinfo_needed = party.addinfo_needed
			self.addinfo_needed_answer = party.addinfo_needed_answer
			self.ha_status = party.ha_status
			self.registration_status = party.registration_status
			self.ha_resubmit_date = party.ha_resubmit_date
			self.ha_resubmitted = party.ha_resubmitted
			self.ha_resubmitted = party.ha_resubmitted
			self.ha_resubmitted = party.ha_resubmitted
			self.program_startdate = party.programapplied.pgmschedule_startdate
			self.program_enddate = party.programapplied.pgmschedule_enddate
