from odoo import models, fields, api, exceptions
from odoo.exceptions import except_orm

class MegaProgramAttendance(models.TransientModel):
	_name = 'megaprgattendance'
	_description = 'MegaProgram Attendance'

	programapplied = fields.Char(string = 'Program Schedule')
	programtype = fields.Integer(string = 'Program Type')
	selected_program = fields.Many2one('megapgms.program.schedule',string = 'Selected Program',
	domain="[('pgmschedule_programtype','=',programtype),('pgmschedule_totalseatspaid','>',0),'|',('pgmschedule_startdate','<',context_today().strftime('%Y-%m-%d')),('pgmschedule_startdate','=',context_today().strftime('%Y-%m-%d'))]")
	participant_list = fields.Many2many('megaprogram.participant', string = 'Participants')

	def load_data(self,a_id):
		print('load program')
		party = self.env["megapgms.registration"].search([('programapplied.id','=',a_id),'|',('registration_status','=','Paid'),('registration_status','=','Confirmed')])
		self.participant_list = [(5,0,0)]
		if party:
			print('party available')
			for rec in party:
				id = self.env['megaprogram.participant'].search([('participant_name','=',rec.id),('participant_programname','=',a_id)])
				print(id)
				if not id:
					nrec = self.env['megaprogram.participant'].create({'participant_name':rec.id,'participant_programname':a_id,'participant_attendance':'Present'})		
				else:
					if (id.participant_attendance == False):
						id.write({'participant_attendance':'Present'})

		self.participant_list = self.env["megaprogram.participant"].search([('participant_programname','=',a_id)])


	@api.onchange('programapplied')
	def programapplied_onchange(self):
		print('program applied')
		self.env['megaprogram.participant'].search([]).unlink()
		active_id = self.env.context.get('default_active_id')
		print(active_id)
		print(self.programtype)
		prgs = self.env["megapgms.program.schedule"].search([('pgmschedule_programtype','=',self.programtype)])
		print(prgs)
		self.selected_program = self.env["megapgms.program.schedule"].browse(active_id)	

	@api.onchange('selected_program')
	def selected_program_onchange(self):
		active_id = self.selected_program.id				
		self.load_data(active_id)


	def submit_changes(self):
		print('Save Submit')		
		for rec in self.participant_list:
			print(rec.participant_name)
			print(rec.participant_attendance)
			acc_data = self.env['megaprogram.participant'].browse(rec.participant_name.id)
			if acc_data:
				print('found 1')
				acc_data.write({'participant_attendance':rec.participant_attendance})

			acc_data = self.env['megapgms.registration'].browse(rec.participant_name.id)
			if acc_data:
				print('found 2')
				acc_data.write({'attendance_status':rec.participant_attendance})

class ProgramScheduleParticipants(models.TransientModel): 
	_name = 'megaprogram.participant'
	_description = 'Program Participants'
	_rec_name = 'participant_name'
	_sql_constraints = [('unique_program_schedule_participants','unique(participant_programname,participant_name)','Cannot have duplicate participant for the program schedule , give different data')]

	participant_programname = fields.Many2one('megapgms.program.schedule',string = 'Program Name')
	participant_name = fields.Many2one('megapgms.registration',string = 'Participant Name')
	participant_mobile = fields.Char(string='Mobile Number',related="participant_name.phone")
	participant_email = fields.Char(string='EMail Address',related="participant_name.participant_email")
	participant_attendance = fields.Selection([('Present','Present'),('Absent','Absent')], string='Attendance')
