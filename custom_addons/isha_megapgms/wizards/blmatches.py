from odoo import models, fields, api, exceptions
from odoo.exceptions import except_orm
from datetime import date, timedelta, datetime

class StarmarkMatches(models.TransientModel):
	_name = 'megapgm.starmark.matches'
	_description = 'Starmark Matches'

	name = fields.Char(string = 'Name')	
	mobile = fields.Char(string = 'Mobile')	
	starmark_type = fields.Selection([('Normal','Normal')], string='Starmark Type')

	def submit_selection(self):
		print('submit clicked')