function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function () {
            defer(method)
        }, 50);
    }
}
var jspgremstatecoll=[];
var jspgremcountrycoll=[];
defer(jspgmsatsangdocumentready);
function jspgmsatsangdocumentready(){
  $(document).ready(function () {
        jsgstngfmanyishaprg();
        jspgmregfmcountrycodechange();
        jspgmregfmcountryphonecode();
});
}

function jsgstngfmanyishaprg(){
    $('#jsgstngfmanyishaprgyes').change(function () {
     if ($(this).is(":checked"))
     {
         $(this).val('True')
         $('.jsgstngfmanyishaprgcls').css("display","block")
         $('#jsgstngfmidmonth').prop('required',  true)
         $('#jsgstngfmieyear').prop('required',  true)
         $('#jsgstngfmiecenter').prop('required',  true)
       }
    });
    $('#jsgstngfmanyishaprgno').change(function () {
     if ($(this).is(":checked"))
     {
         $(this).val('')
         $('.jsgstngfmanyishaprgcls').css("display","none")
         $('#jsgstngfmidmonth').prop('required',  false)
         $('#jsgstngfmieyear').prop('required',  false)
         $('#jsgstngfmiecenter').prop('required',  false)
       }
    });
}

function jspgmregfmcountryphonecode(){
 if( $('#jsgstngfmaltcountrycode').val() == '91'){
    $('#jsgstngfmphone').attr('maxlength', '10')
    $('#jsgstngfmphone').attr('minlength', '10')
    $('#jsgstngfmphone').attr('pattern', '[1-9]{1}[0-9]+')
}
else{
$('#jsgstngfmphone').attr('maxlength', '15')
$('#jsgstngfmphone').attr('minlength', '5')
$('#jsgstngfmphone').attr('pattern', '[1-9]{1}[0-9]+')
}
}

function jspgmregfmcountrycodechange(){
$('#jsgstngfmaltcountrycode').change(function () {
jspgmregfmcountryphonecode()
});

}

function jspgmsatsangfmvalidateformalternate(){

        return jspgmregfmvalidateanyotherishaprg();
}

function jspgmregfmvalidateanyotherishaprg(){
if(!$('#jsgstngfmanyishaprgyes').is(':checked') && !$('#jsgstngfmanyishaprgno').is(':checked')){
    alert($('#jsgstngfmishaprgwarning').val())
    return false;
}
if($('#jsgstngfmanyishaprgno').is(':checked')){
    alert($('#jsgstngfmishaprgnowarning').val())
    return false;
}

return true;

}