function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function () {
            defer(method)
        }, 50);
    }
}
var jspgremstatecoll=[];
var jspgremcountrycoll=[];
defer(jspgmregfmdcoumentready);

function jspgmregfmpackages() {
    var programapplied = document.getElementById("programapplied");
    var optselected = programapplied.options[programapplied.selectedIndex].text;
    var packageselection = document.getElementById("packageselection");
    var programfee = document.getElementById("programfee");

    packageselection.disabled = false;
    Array.from(packageselection.options).forEach(item => {
        //console.log(item.value)
        //console.log(item.text)
        var trend = item.getAttribute("prgname");
        if (trend == optselected)
            item.style.display = "block";
        else
            item.style.display = "none";
    });

    packageselection.options[0].selected = 'selected';
    packageselection.onchange = true;
    jspgmregfmeprogramchange();
    $('#programfee').val('');
}

function jspgmregfmpackagecost() {
 $('#packageselection').change(function ()
   {
        var programapplied = document.getElementById("programapplied");
    var optselectedprgname = programapplied.options[programapplied.selectedIndex].text;
    var packageselection = document.getElementById("packageselection");
    var optselected = packageselection.options[packageselection.selectedIndex].text;
    if(optselected == 'None') return;
    var programfee = document.getElementById("programfee");

    Array.from(programfee.options).forEach(item => {
        //console.log(item.value)
        //console.log(item.text)
        var trend = item.getAttribute("prgname");
        var displayname = item.getAttribute("pckgdisplayname");
        if (trend == optselectedprgname &&  displayname == optselected)
            {item.style.display = "block";
            item.selected = 'selected'}
        else
            item.style.display = "none";
    });

 })
}
function jspgmregsetupdatedformvalues(){
    var initObjvar;
    var addobjvar;
    try {
        initObjvar = $.parseJSON($('#jspgreginputobjid').val());
        //debugger;
        //if ('preferredName' in initObjvar['basicProfile'] && jspgmregisnotnullorempty(initObjvar['basicProfile']['preferredName'])) {
            initObjvar['basicProfile']['preferredName'] = $('#name_called').val();
        //}
         initObjvar['basicProfile']['gender'] = $('#gender').val().toUpperCase();
         initObjvar['basicProfile']['firstName'] = $('#first_name').val();
         initObjvar['basicProfile']['lastName'] = $('#last_name').val();
        if ('company' in initObjvar['extendedProfile']) {
            initObjvar['extendedProfile']['company'] = $('#occupation').val();
        }
        if ('profession' in initObjvar['extendedProfile']) {
            //$('#occupation').val($('#occupation').val() + " " + initObjvar['extendedProfile']['profession']);
        }
        //debugger;
        if ('nationality' in initObjvar['extendedProfile']) {
            initObjvar['extendedProfile']['nationality'] = $('select#nationality_id option:selected').attr('ccatr');
        }
        if ('phone' in initObjvar["basicProfile"]) {
            if ('countryCode' in initObjvar["basicProfile"]["phone"]) {
                initObjvar['basicProfile']['phone']['countryCode'] = $("select#countrycode").val();
            }
            if ('number' in initObjvar["basicProfile"]["phone"]) {
                initObjvar['basicProfile']['phone']['number'] = $("input#phone").val();
            }
        }
        addobjvar = initObjvar['addresses'];
        if (addobjvar.length > 0) {
            addobjvar = addobjvar[0];
            //debugger;
            if ('country' in addobjvar && jspgmregisnotnullorempty(addobjvar['country'])) {
                 addobjvar['country'] = $('#country option:selected').attr('ccatr')
            }
            if ('addressLine1' in addobjvar && jspgmregisnotnullorempty(addobjvar['addressLine1'])) {
                addobjvar['addressLine1'] = $('#address_line').val();
            }
            if ('state' in addobjvar && jspgmregisnotnullorempty(addobjvar['state'])) {
                    addobjvar['state'] = $('#stateid option:selected').text();
            }


            if ('addressLine2' in addobjvar && jspgmregisnotnullorempty(addobjvar['addressLine2'])) {
                //$('#address_line').val($('#address_line').val() + "  " + addobjvar['addressLine2']);
            }
            //debugger;
            if ('townVillageDistrict' in addobjvar && jspgmregisnotnullorempty(addobjvar['townVillageDistrict'])) {
                addobjvar['townVillageDistrict'] = $('#city').val();
            }
            if ('pincode' in addobjvar && jspgmregisnotnullorempty(addobjvar['pincode'])) {
                addobjvar['pincode'] = $('#pincode').val();
            }

        }
        $('#jspgregoutputobjid').val(JSON.stringify(initObjvar));
    } catch (exvar) {
    //console.log(exvar);
    }
}

function jspgmregfmvalidateForm() {
    debugger;
    jspgmregsetupdatedformvalues();
    if(validateemptystring()){
         alert("Please enter valid characters for first name(without numbers or special characters)")
         return false;
         }
    retval =   jspgmregfmvalidateage();
    retval = retval &&  jspgmregfmvalidateopnum();
    retval = retval &&  jspgmregfmvalidatecharlength();
return retval;
}

function validateemptystring(){
return $.trim($("#first_name").val()) == "";
}
function jspgmregfmvalidatecharlength(){

if($('#address_line').val().length < 10){
alert("Please enter minimum 10 characters for address")
return false;
}

if($('#city').val().length < 2){
alert("Please enter minimum characters for city")
return false;
}
    return true;
}

function jspgmregfmvalidateopnum(){
if(!$('#jpgmregmfmisopnumyes').is(':checked') && !$('#jspgmregfmisopnumno').is(':checked')){
    alert('Please select yes or no if you have taken consultation from Isha Rejuvenation / attended Rejuvenation programs before?')
//    $('#jpgmregmfmisopnumyes').focus()
    return false;
}
return true;

}

function jspgmregfmvalidateage() {
    var dob = document.getElementById("dob");
    if (dob.value == "") {
        alert("Please enter a valid date of birth");
        return false;
    }
    var today = new Date();
    var birthDate = new Date(dob.value);
    var age = ((Date.now() - birthDate) / (31557600000));
    var programapplied = document.getElementById("programapplied");
    var minage = programapplied.options[programapplied.selectedIndex].getAttribute("minage");
    if (age < minage) {
  alert("Minimum age to undergo this program is "+minage+". Sorry, you are not allowed to undergo this program");
        return false;
    } else if(age > 100)
    {
    alert("Please enter a valid date of birth");
        return false;
    }
    else
        return true;
}

function jspgmregfmvalidategender() {
    var gender = document.getElementById("gender");
    var gendervalue = gender.options[gender.selectedIndex].text;
    var programapplied = document.getElementById("programapplied");
    var gender = programapplied.options[programapplied.selectedIndex].getAttribute("gender");
    if (gender != gendervalue) {
        if (gender != "All"){
            alert("Selected gender is not allowed");
            return false;
            }
            else
                return false;
        }
    else
        return true;
}

function jspgmregfmchecknationality() {
    // Get the checkbox
    var checkBox = document.getElementById("nationality_id");
    // Get the output text
    var text1 = document.getElementById("passportnumberlabel");
    var text2 = document.getElementById("passportnumber");
    var text3 = document.getElementById("passportexpirydatelabel");
    var text4 = document.getElementById("passportexpirydate");
    var text5 = document.getElementById("visanumberlabel");
    var text6 = document.getElementById("visanumber");
    var text7 = document.getElementById("visaexpirydatelabel");
    var text8 = document.getElementById("visaexpirydate");
    var text9 = document.getElementById("visaheading");

    // If the checkbox is checked, display the output text
    if (checkBox.value == '104') {
    $('.jspgmregmpassportvisacls').css({"display":"none"});
        text2.required = false;
        text4.required = false;
        text6.required = false;
        text8.required = false;
           text2.value = "";
           text4.value = "";
           text6.value = "";
           text8.value = "";
    } else {
    $('.jspgmregmpassportvisacls').css({"display":"block"});
        text2.required = true;
        text4.required = true;
        text6.required = true;
        text8.required = true;
    }
}

function jspgmregsetdefaultprogramapplied(){
    var programapplied = document.getElementById("programapplied");

if($('#programapplied').val() == "" && $('#programapplied')[0].options.length > 1){
    $('#programapplied')[0].options.selectedIndex = 1;
    $('#programapplied').trigger('change');
    jspgmregfmpackages()
}
}

function jspgmregfmdcoumentready() {
    $(document).ready(function () {
        $('.fileerror').hide();
        jspgmreginitfunction();
        //jspgmregfmeprogramchange();
        jspgmregsetdefaultprogramapplied();
        jspgmregfmcountryandcodechange();
        jspgmregfmcountrypincode();
        jspgmregfmcountryphonecode();
        //jspgmregfmeprogramchange();
        jspgmregfmotherfunc();
        //jspgmregfmselectstates();
        // jspgmregfmchecknationality();
        // jspgmreggetcountries();
        $(document).on( 'click','button.programappliedcls', function(event) {
    try{
    var prgschedattvar = $(this).attr("prgschedatt");
    $("select.programappliedcls").val(prgschedattvar);
    event.preventDefault();
    $("button.programappliedcls").each(function(iteratorvar, jobject) {
        $(jobject).prop("disabled", true);
    })
    $(this).removeAttr('disabled');
    //$("button.programappliedcls").each(function(iteratorvar, jobject) {
    //    $(jobject).prop("disabled", true);
    //});
    var firsteleiterator = 0;
    $("button.programsesscls").each(function(iteratorvar, jobject) {
        if ($(jobject).attr("prgschedatt") == prgschedattvar) {
            $(jobject).show();
            if (firsteleiterator == 0) {
                $("select.programappliedcls").val($(jobject).attr('prgsessatt'));
                $(jobject).removeAttr('disabled');
            } else {
                $(jobject).prop("disabled", true);
            }
        }
        else {
            $(jobject).hide();
        }
    });
    }
    catch(e){
        debugger;
    }


})



$(document).on('click', 'button.programsesscls', function(event) {
    try
    {
    var prgschedattvar = $(this).attr("prgsessatt");
    $("select.programsesscls").val(prgschedattvar);
    event.preventDefault();
    //$(this).removeAttr('disabled');
    $("button.programappliedcls").each(function(iteratorvar, jobject) {
        $(jobject).prop("disabled", true);
    });
    $(this).removeAttr('disabled');
    }
    catch(e) {
        debugger;
    }
})
    });
}

function jspgmregfmcountryandcodechange(){
$('#country').change(function () {
jspgmregfmcountrypincode()
});

$('#countrycode').change(function () {
jspgmregfmcountryphonecode()
});
}
function jspgmregfmcountrypincode(){

  if(  $('#country').val() == '104'){
        $('#pincode').attr('maxlength', '6')
        $('#pincode').attr('minlength', '6')
        $('#pincode').attr('pattern', '[1-9]{1}[0-9]+')
    }
    else{
    $('#pincode').attr('maxlength', '10')
    $('#pincode').attr('pattern', '[A-Za-z0-9]*')
    }
}

function jspgmregfmcountryphonecode(){

 if( $('#countrycode').val() == '91'){
    $('#phone').attr('maxlength', '10')
    $('#phone').attr('minlength', '10')
    $('#phone').attr('pattern', '[0-9]+')
}
else{
$('#phone').attr('maxlength', '15')
$('#phone').attr('pattern', '[1-9]{1}[0-9]+')
}
}

function jspgmregfmeprogramchange(){
//$('#programapplied').change(function(){
        //$('#country option:selected').text()
        try{
            if($('#programapplied option:selected').attr('description').includes('online')){
                $('#country').val('104');
                $("select#country").trigger('change');
                $('#country option').hide().prop('disabled',true);
                $('#country option:selected').show().prop('disabled',false);

                $('#countrycode').val('91');
                $("select#countrycode").trigger('change');
                $('#countrycode option').hide().prop('disabled',true);
                $('#countrycode option:selected').show().prop('disabled',false);
                $("#jspgmregidproofblock,#jspgmregaddproofblock",".upload-documents-wrapper").hide();
                $("select#pgmupldid,select#pgmupldaddproofid,input#pgmupldIdproof,input#pgmupldAddressproof",".upload-documents-wrapper").each(function(){
                    if ($(this).length > 0){
                        $(this)[0].required = false;
                    }
                });
                $('.jspgmregfmonlinecosultationcls').show();
                $('#jspgmreggovtdoclabelid').hide();
            }
            else{
                $('#country option').show().prop('disabled',false);
                $('#countrycode option').show().prop('disabled',false);
                $("#jspgmregidproofblock,#jspgmregaddproofblock",".upload-documents-wrapper").show();
                $("select#pgmupldid,select#pgmupldaddproofid,select#pgmupldaddproofid,input#pgmupldIdproof,input#pgmupldAddressproof",".upload-documents-wrapper").each(function(){
                    if ($(this).length > 0){
                        $(this)[0].required = true;
                    }
                });
                $('.jspgmregfmonlinecosultationcls').hide();
                $('#jspgmreggovtdoclabelid').show();
            }
        }
        catch(exvar){
            //console.log(exvar)
        }
//});
}

function jspgmregphotoselectchangehandler(selectorsel,inputsel,imgsel) {
    $(selectorsel).change(function(){
        //$("input[name='pgmupldIdproof'] ,select#pgmupldid ").prop('required',true');
        //$("input[name='pgmupldIdproof'] ,select#pgmupldid ").attr('required','true');
        $(selectorsel)[0].required = true;
        $(inputsel)[0].required = true;
        $(inputsel).removeAttr("disabled");
        $(imgsel).attr('src', '').hide();
        });
}
function jspgmregphotoinputchangehandler(inputsel,imgsel){
$(inputsel).change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $(imgsel).attr('src', e.target.result).show();
            }
            reader.readAsDataURL(this.files[0]);
        }
    });
}
function jspgmregfmotherfunc() {
    $('.fileerror').hide();
    //jspgmregfmselectstates();
    jspgmregfmpackagecost();
     jspgmregfmchecknationality();
    // jspgmreggetcountries();
    //$("select#countrycode").val("91");
    //jspgmregphotoselectchangehandler("select#pgmupldid","input[name='pgmupldIdproof']",'#jspgmregimgssoidph');
    jspgmregphotoselectchangehandler("select#pgmupldid","input[name='pgmupldIdproof']",'#jspgmregimgssoidph');
    jspgmregphotoselectchangehandler("select#pgmupldaddproofid","input[name='pgmupldAddressproof']",'#jspgmregimgssoaddresph');
    jspgmregphotoinputchangehandler("input[name='pgmupldphoto']",'#jspgmregimgssophotoidph');
    jspgmregphotoinputchangehandler("input[name='pgmupldAddressproof']",'#jspgmregimgssoaddresph');
    jspgmregphotoinputchangehandler("input[name='pgmupldIdproof']",'#jspgmregimgssoidph');
    jspgmregfmisopnum();
}

function jspgmregfmisopnum()
{
$('#jpgmregmfmisopnumyes').change(function () {
 if ($(this).is(":checked"))
 {
  $('.jspgmregfmopnumcls').css("display","block")
  $(this).val('YES')
    }
});
$('#jspgmregfmisopnumno').change(function () {
 if ($(this).is(":checked"))
 {
  $('.jspgmregfmopnumcls').css("display","none")
  $(this).val('NO')
    }
});
}

function jspgmreggetcountries() {
    $("packageselection").click(function () {
        $.get("https://cdi-gateway.isha.in/contactinfovalidation/api/countries", function (data, status) {
            debugger;
            alert("Data: " + data + "\nStatus: " + status);
        });
    });
    //});
}

function jspgmregfmselectstates() {
    var state = document.getElementById("stateid");

    var countryname = document.getElementById("country");
    var countryid = countryname.value;

    var idvar = 0;
    Array.from(state.options).forEach(item => {
        var trend = item.getAttribute("countryid");
        if (trend == countryid) {
            item.style.display = "block";
            item.disabled = false;
            if (idvar == 0)
                idvar = item.index;
        } else
            {
            item.style.display = "none";
            item.disabled = true;
            }
    });
    if (idvar != 0) {
        state.options.selectedIndex = idvar;
        state.disabled = false;
        state.height = 35;
    }
     else if (idvar == 0) {
       state.options.selectedIndex = 0;
        state.disabled = true;
    }
}

function jspgmreginitfunction() {
    var initObjvar;
    var addobjvar;
    $('#stateid option').each(function(ivar,jvar){
       var objvar ={
        "valueattr":$(jvar).attr("value"),
        "statenameattr":$(jvar).attr("statename")
        };
        jspgremstatecoll.push(objvar);
    });
    $('#nationality_id option').each(function(ivar,jvar){
       var objvar ={
        "valueattr":$(jvar).attr("value"),
        "ccattr":$(jvar).attr("ccatr")
        };
        jspgremcountrycoll.push(objvar);
    });
    try {
        initObjvar = $.parseJSON($('#jspgreginputobjid').val());
        //debugger;
        if ('firstName' in initObjvar['basicProfile'] && jspgmregisnotnullorempty(initObjvar['basicProfile']['firstName'])) {
            $('#first_name').val(initObjvar['basicProfile']['firstName']);
            $('#first_name').prop("readonly",true);
        }
        if ('lastName' in initObjvar['basicProfile'] && jspgmregisnotnullorempty(initObjvar['basicProfile']['lastName'])) {
            $('#last_name').val(initObjvar['basicProfile']['lastName']);
             $('#last_name').prop("readonly",true);
        }
        if ('email' in initObjvar['basicProfile'] && jspgmregisnotnullorempty(initObjvar['basicProfile']['email'])) {
            $('#participant_email').val(initObjvar['basicProfile']['email']);
             $('#participant_email').prop("readonly",true);
        }
        if ('preferredName' in initObjvar['basicProfile'] && jspgmregisnotnullorempty(initObjvar['basicProfile']['preferredName'])) {
            $('#name_called').val(initObjvar['basicProfile']['preferredName']);
            $('#name_called').prop("readonly",true);
        }
        if ('company' in initObjvar['extendedProfile'] && jspgmregisnotnullorempty(initObjvar['extendedProfile']['company'])) {
            $('#occupation').val(initObjvar['extendedProfile']['company']);
        }
        if ('profession' in initObjvar['extendedProfile'] && jspgmregisnotnullorempty(initObjvar['extendedProfile']['profession'])) {
            $('#occupation').val($('#occupation').val() + " " + initObjvar['extendedProfile']['profession']);
        }
        //debugger;
        if ('nationality' in initObjvar['extendedProfile'] && jspgmregisnotnullorempty(initObjvar['extendedProfile']['nationality'])) {
            var countfiltvar = _.filter(jspgremcountrycoll, (a) => a.ccattr == initObjvar['extendedProfile']['nationality']);
                if(countfiltvar.length > 0)
                {
            $('select#nationality_id').val(countfiltvar[0]["valueattr"]);
            jspgmregfmchecknationality();
            }
        }
        if ('phone' in initObjvar["basicProfile"]) {
            if ('countryCode' in initObjvar["basicProfile"]["phone"] && jspgmregisnotnullorempty(initObjvar['basicProfile']['phone']['countryCode'])) {
                $("select#countrycode").val(initObjvar['basicProfile']['phone']['countryCode']);
                $("select#countrycode").trigger('change');
                 $("select#countrycode option").prop("disabled", true);
                 $("select#countrycode option:selected").prop("disabled", false);
            }
            if ('number' in initObjvar["basicProfile"]["phone"] && jspgmregisnotnullorempty(initObjvar['basicProfile']['phone']['number'])) {
                $("input#phone").val(initObjvar['basicProfile']['phone']['number']);
                $("input#phone").prop("readonly",true);
            }
        }
        addobjvar = initObjvar['addresses'];
        if (addobjvar.length > 0) {
            addobjvar = addobjvar[0];
            //debugger;
            if ('country' in addobjvar && jspgmregisnotnullorempty(addobjvar['country'])) {
                var countfiltvar = _.filter(jspgremcountrycoll, (a) => a.ccattr == addobjvar['country']);
                if(countfiltvar.length > 0)
                {
                $('#country').val(countfiltvar[0]["valueattr"]);
                jspgmregfmselectstates();
                }
            }
            if ('addressLine1' in addobjvar && jspgmregisnotnullorempty(addobjvar['addressLine1'])) {
                $('#address_line').val(addobjvar['addressLine1']);
            }
            //debugger
            if ('state' in addobjvar && jspgmregisnotnullorempty(addobjvar['state'])) {
                let statevar = _.filter(jspgremstatecoll, (a) => a.statenameattr == addobjvar['state'])
                if(statevar.length > 0){
                    $('#stateid').val(statevar[0]["valueattr"]);
                    $('#address_line').val(addobjvar['addressLine1']);
                }
            }


            if ('addressLine2' in addobjvar && jspgmregisnotnullorempty(addobjvar['addressLine2'])) {
                $('#address_line').val($('#address_line').val() + "  " + addobjvar['addressLine2']);
            }
            //debugger;
            if ('townVillageDistrict' in addobjvar && jspgmregisnotnullorempty(addobjvar['townVillageDistrict'])) {
                $('#city').val(addobjvar['townVillageDistrict']);
            }
            if ('pincode' in addobjvar && jspgmregisnotnullorempty(addobjvar['pincode'])) {
                $('#pincode').val(addobjvar['pincode']);
            }

        }
        jspgmregfmselectstates();
    } catch (exvar) {
    //console.log(exvar);
    }

}
function jspgmregisnotnullorempty(objtotestvar){
var flg = false;
 try{
  if(objtotestvar && objtotestvar !=null && objtotestvar !="") {
        flg = true;
  }

 }
catch(exvar){
}
finally{
return flg;
}
}
$(document).ready(function(){


    $('#pincode').focusout(async () => {
        //let all_countries = await fetch('https://cdi-gateway.isha.in/contactinfovalidation/api/countries');
        //let all_countries_json = await all_countries.json()
        let selected_country = $('#country option:selected').text();
        //let selected_country_iso2_code = _.filter(all_countries_json, (country) => {
        //    return country.name === selected_country;
        //})[0].iso2;
        let selected_country_iso2_code = $('#country option:selected').attr("ccode");

        let pincode = $('#pincode').val();
        pincode = pincode.trim();
        if( pincode !="")
        {
            let address_details = await fetch(`https://cdi-gateway.isha.in/contactinfovalidation/api/countries/${selected_country_iso2_code}/pincodes/${pincode}`)
            let address_details_json = await address_details.json()

            if (! _.isEmpty(address_details_json)) {
                //let all_states = document.querySelectorAll('#stateid option')
                //let state = _.filter(all_states, (a) => a.innerText == address_details_json.state)[0]
                let statevar = _.filter(jspgremstatecoll, (a) => a.statenameattr == address_details_json.state)[0]
                //state.setAttribute('selected', 'selected')
                $('#stateid').val(statevar["valueattr"]);

                $('#city').val(address_details_json.defaultcity)
                $('#district').val(address_details_json.defaultcity)
            }
        }
    })
})