
$(document).ready(function () {

  // Initialize select2
  $(".selUser").select2();

  // Read selected option
  $('#but_read').click(function () {
    var username = $('.selUser option:selected').text();
    var userid = $('.selUser').val();

    $('#result').html("id : " + userid + ", name : " + username);
  });

  $(".selUser").select2({
    placeholder: "None",
    allowClear: true
  });

  add_table_delete_behaviour('jsprgregaddishalifetrethistid','isha-life-treatments-table');
  add_table_delete_behaviour('jsprgregaddcurrentmedicaldetails','current-medical-details-table');
  add_table_delete_behaviour('allopathy-medication-table-add','allopathy-medication-table');
  add_table_delete_behaviour('alternate-medication-table-add','alternate-medication-table');
  add_table_delete_behaviour('allergies-to-medications-table-add','allergies-to-medications-table');

jsprgreghideaddbuttonfunction("#isha-life-treatments-table","#jsprgregaddishalifetrethistid")
//jsprgreghideaddbuttonfunction("#current-medical-details-table","#jsprgregaddcurrentmedicaldetails")
jsprgreghideaddbuttonfunction("#allopathy-medication-table","#allopathy-medication-table-add")
jsprgreghideaddbuttonfunction("#alternate-medication-table","#alternate-medication-table-add")
jsprgreghideaddbuttonfunction("#allergies-to-medications-table","#allergies-to-medications-table-add")

jsprgreghideaggravtingfactors("#current-medical-details-table","#jsprgregaddcurrentmedicaldetails")

  $('b[role="presentation"]').hide();
  $('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>');

  displayTextInputOnCheck("#aggravating-factors-food-details-checkbox", "#aggravating-factors-food-details")
  displayTextInputOnCheck("#aggravating-factors-family-details-checkbox", "#aggravating-factors-family-details")
  displayTextInputOnCheck("#aggravating-factors-work-details-checkbox", "#aggravating-factors-work-details")
  displayTextInputOnCheck("#aggravating-factors-climate-details-checkbox", "#aggravating-factors-climate-details")

  //jspgmregaddprghist()
  displayTextInputOnCheck("#head-and-neck-problems", "#head-and-neck-problems-details")


  displayTextInputOnCheck("#hyper-tension","#hyper-tension-details" )
  displayTextInputOnCheck("#heart-disorders","#heart-disorders-details" )
  displayTextInputOnCheck("#diabetes-mellitus","#diabetes-mellitus-details" )
  displayTextInputOnCheck("#thyroid","#thyroid-details" )
  displayTextInputOnCheck("#other-endocrinology-disorders","#other-endocrinology-disorders-details" )
  displayTextInputOnCheck("#liver-gallbladder-disorders","#liver-gallbladder-disorders-details" )
  displayTextInputOnCheck("#kidney-disorders","#kidney-disorders-details" )
  displayTextInputOnCheck("#brain-and-nervous-disorders","#brain-and-nervous-disorders-details" )
  displayTextInputOnCheck("#eye-disorders","#eye-disorders-details" )
  displayTextInputOnCheck("#ear-disorders","#ear-disorders-details" )
  displayTextInputOnCheck("#nose-disorders","#nose-disorders-details" )
  displayTextInputOnCheck("#throat-issues","#throat-issues-details" )
  displayTextInputOnCheck("#chest-lung-issues","#chest-lung-issues-details" )
  displayTextInputOnCheck("#intestinal-disorders","#intestinal-disorders-details" )
  displayTextInputOnCheck("#stomach-disorders","#stomach-disorders-details" )
  displayTextInputOnCheck("#urinary-problem","#urinary-problem-details" )
  displayTextInputOnCheck("#back-issues","#back-issues-details" )
  displayTextInputOnCheck("#circulation-problem","#circulation-problem-details" )
  displayTextInputOnCheck("#skin-disorders","#skin-disorders-details" )
  displayTextInputOnCheck("#joint-disorders","#joint-disorders-details" )
  displayTextInputOnCheck("#pain-discomfort","#pain-discomfort-details" )
  displayTextInputOnCheck("#infertility","#infertility-details" )
  displayTextInputOnCheck("#cancer","#cancer-details" )
  displayTextInputOnCheck("#autoimmune-disease","#autoimmune-disease-details" )
  displayTextInputOnCheck("#ulcers-dryness","#ulcers-dryness-details" )
  displayTextInputOnCheck("#hiv-aids","#hiv-aids-details" )
  displayTextInputOnCheck("#herpes-type-1","#herpes-type-1-details" )
  displayTextInputOnCheck("#herpes-type-2","#herpes-type-2-details" )
  displayTextInputOnCheck("#tuberculosis","#tuberculosis-details" )
  displayTextInputOnCheck("#hepatitis","#hepatitis-details" )
  displayTextInputOnCheck("#any-other-communicable-disease","#any-other-communicable-disease-details" )
  displayTextInputOnCheck("#psychiatry-disorders ","#psychiatry-disorders-details" )
  setValueInputOnCheck('#jsprgregnonedisordersid')

  $(".disorders-diseases-past-wrapper .form-check-input").click(function(e){
    if (e.target.checked === true) {
      $('#'+e.target.id+'-details').parent().removeClass('d-none');
    } else {
      $('#'+e.target.id+'-details').parent().addClass('d-none');
    }
  })
});

function setValueInputOnCheck(checkbox) {
  $(checkbox).change(function () {
    if ($(this).is(":checked"))
      {
        $(this).val("YES");
      }
    else
      {
      $(this).val("NO");
      }
  })
}
function displayTextInputOnCheck(checkbox, Element) {
  $(checkbox).change(function () {
    if ($(this).is(":checked"))
      {
        $(this).val("YES");
        $(Element).css("display", "block");
        $(Element).prop('required',true);
      }
    else
      {
      $(this).val("NO");
      $(Element).css("display", "none");
      $(Element).prop('required',false);
      }
  })
}

function displayTextBoxforDetails(name, clickElement, toggleElement) {
  $("input[name="+name+"]").click(function() {
    if ($(clickElement).is(":checked")) {
         $(clickElement).val("YES");
         $(toggleElement).css("display", "block");
         $(toggleElement).prop('required',true);
    } else {
        $(clickElement).val("NO");
        $(toggleElement).css("display", "none");
        $(toggleElement).prop('required',false);
    }
  });
}

function displayPopup (clickElement, displayElement, closeButon) {
  $(clickElement).click(function () {
    $(displayElement).show();
  })
  $(closeButon).click(function () {
    $(displayElement).hide();
  })
}

function jspgmregfm3validateForm()
{
    var validationflg;
    validationflg = jspgmvalidateishalifetable() && jspgmvalidatecurrentmedicaldetailstable() ;
    validationflg = validationflg && jspgmvalidateallopathymedicationtable();
    validationflg = validationflg && jspgmvalidatealternatemedicationtable() ;
    validationflg = validationflg && jspgmvalidateallergiestomedicationtable();
    validationflg = validationflg && jspgmvalidateotherdisorders();
    return validationflg;
}
function jspgmvalidateotherdisorders(){
  if($('div#jspgregotherdisoid input:checkbox:checked').length > 0)
    return true;
  else
    {$('#jspgregotherdisoid').focus()
    return false;
    }
}
function jspgmvalidateishalifetable()
{
var rows = document.getElementById("isha-life-treatments-table").rows;
for(i = 1; i <rows.length; i++)
{
          if(i==0) continue;

          var pgmishalifedate = $("td input[name='pgmishalifedate']",rows[i])[0].value;
          var pgmishalifename = $("td input[name='pgmishalifename']",rows[i])[0].value;
          var pgmishalifeplace = $("td input[name='pgmishalifeplace']",rows[i])[0].value;
          var pgmishalifetreatmentname = $("td input[name='pgmishalifetreatmentname']",rows[i])[0].value;

          var atleastoneotherisfilled = pgmishalifename!="" ||  pgmishalifeplace!="" ||
                      pgmishalifetreatmentname!="" ;

          var atleastoneotherisnotfilled =  pgmishalifename=="" ||  pgmishalifeplace == "" ||
                      pgmishalifetreatmentname=="";
          if(pgmishalifedate=="" && atleastoneotherisfilled)
           {
            alert("please, fill all the details of isha life treatment details");
            return false;
           }
          else if (pgmishalifedate!="" && atleastoneotherisnotfilled)
            {
             alert("please, fill all the details of isha life treatment details");
             return false;

            }

            if(i!=1 && pgmishalifedate=="" && atleastoneotherisnotfilled )
                {
                alert("please, delete the empty history record");
                return false;
                }

}
    return true;
}

function jspgmvalidatecurrentmedicaldetailstable()
{
var rows = document.getElementById("current-medical-details-table").rows;
for(i = 1; i <rows.length; i++)
{
          if(i==0) continue;

          var pgmmedicalhistorycondition = $("td input[name='pgmmedicalhistorycondition']",rows[i])[0].value;
          var pgmmedicalhistoryduration = $("td input[name='pgmmedicalhistoryduration']",rows[i])[0].value;
          var pgmmedicalhistorytreatment = $("td input[name='pgmmedicalhistorytreatment']",rows[i])[0].value;

          var atleastoneotherisfilled = pgmmedicalhistoryduration!="" ||  pgmmedicalhistorytreatment!="" ;

          var atleastoneotherisnotfilled =  pgmmedicalhistoryduration=="" ||  pgmmedicalhistorytreatment == "" ;

          if(pgmmedicalhistorycondition=="" && atleastoneotherisfilled)
           {
            alert("please, fill all the details of current medical details");
            return false;
           }
          else if (pgmmedicalhistorycondition!="" && atleastoneotherisnotfilled)
            {
             alert("please, fill all the details of current medical details");
             return false;

            }

            if(i!=1 && pgmmedicalhistorycondition=="" && atleastoneotherisnotfilled )
                {
                alert("please, delete the empty details record");
                return false;
                }

}
    return true;
}

function jspgmvalidateallopathymedicationtable()
{
var rows = document.getElementById("allopathy-medication-table").rows;
for(i = 1; i <rows.length; i++)
{
          if(i==0) continue;

          var pgmallopathyname = $("td input[name='pgmallopathyname']",rows[i])[0].value;
          var pgmallopathydose = $("td input[name='pgmallopathydose']",rows[i])[0].value;
          var pgmallopathyduration = $("td input[name='pgmallopathyduration']",rows[i])[0].value;

          var atleastoneotherisfilled = pgmallopathydose!="" ||  pgmallopathyduration!="" ;

          var atleastoneotherisnotfilled =  pgmallopathydose=="" ||  pgmallopathyduration == "" ;

          if(pgmallopathyname=="" && atleastoneotherisfilled)
           {
            alert("please, fill all the details of allopathy medication details");
            return false;
           }
          else if (pgmallopathyname!="" && atleastoneotherisnotfilled)
            {
             alert("please, fill all the details of allopathy medication details");
             return false;

            }

            if(i!=1 && pgmallopathyname=="" && atleastoneotherisnotfilled )
                {
                alert("please, delete the empty details record");
                return false;
                }

}
    return true;
}

function jspgmvalidatealternatemedicationtable()
{
var rows = document.getElementById("alternate-medication-table").rows;
for(i = 1; i <rows.length; i++)
{
          if(i==0) continue;

          var pgmalternatename = $("td input[name='pgmalternatename']",rows[i])[0].value;
          var pgmalternatedose = $("td input[name='pgmalternatedose']",rows[i])[0].value;
          var pgmalternateduration = $("td input[name='pgmalternateduration']",rows[i])[0].value;

          var atleastoneotherisfilled = pgmalternatedose!="" ||  pgmalternateduration!="" ;

          var atleastoneotherisnotfilled =  pgmalternatedose=="" ||  pgmalternateduration == "" ;

          if(pgmalternatename=="" && atleastoneotherisfilled)
           {
            alert("please, fill all the details of alternate medication details");
            return false;
           }
          else if (pgmalternatename!="" && atleastoneotherisnotfilled)
            {
             alert("please, fill all the details of alternate medication details");
             return false;

            }

            if(i!=1 && pgmalternatename=="" && atleastoneotherisnotfilled )
                {
                alert("please, delete the empty details record");
                return false;
                }

}
    return true;
}

function jspgmvalidateallergiestomedicationtable()
{
var rows = document.getElementById("allergies-to-medications-table").rows;
for(i = 1; i <rows.length; i++)
{
          if(i==0) continue;

          var pgmallergydrugname = $("td input[name='pgmallergydrugname']",rows[i])[0].value;
          var pgmallergyreaction = $("td input[name='pgmallergyreaction']",rows[i])[0].value;

          var atleastoneotherisfilled = pgmallergyreaction!="";

          var atleastoneotherisnotfilled =  pgmallergyreaction=="";

          if(pgmallergydrugname=="" && atleastoneotherisfilled)
           {
            alert("please, fill all the details of allergies to medication details");
            return false;
           }
          else if (pgmallergydrugname!="" && atleastoneotherisnotfilled)
            {
             alert("please, fill all the details of allergies to medication details");
             return false;

            }

            if(i!=1 && pgmallergydrugname=="" && atleastoneotherisnotfilled )
                {
                alert("please, delete the empty details record");
                return false;
                }

}
    return true;
}

 function jsprgreghideaggravtingfactors(tableselectorvar,buttonselectorvar)
 {
       $('.cspgrghidcurrentmedicaldetcls').prop('hidden', true);
$(tableselectorvar).on('change', 'tr select,tr input', function() {
    var emptyflg = false;
console.log(tableselectorvar+ " input"+", "+tableselectorvar+ " select");
    $(tableselectorvar+ " input"+", "+tableselectorvar+ " select").each(function() {
        if ($(this).val() == "") {
            emptyflg = true;
        }
    });
    if (emptyflg) {
      $('.cspgrghidcurrentmedicaldetcls').prop('hidden', true);

        $(buttonselectorvar)[0].hidden = true;
    } else {
      $('.cspgrghidcurrentmedicaldetcls').prop('hidden', false);
        $(buttonselectorvar)[0].hidden = false;
    }
 });
 }