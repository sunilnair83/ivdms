from odoo import api, models, _

class ReportRegistration(models.AbstractModel):
    _name = 'report.isha_megapgms.report_registration'
    _description = 'Registration Profile'


    @api.model
    def _get_report_values(self, docids, data=None):
        report_obj = self.env['ir.actions.report']
        print('reprt start')
        report = report_obj._get_report_from_name('isha_megapgms.report_registration')
        docargs = {
            'doc_ids':docids,
            'doc_model': report.model,
            'docs':self,
        }
        print('reprt ret')
        print(docargs)
        return docargs
        