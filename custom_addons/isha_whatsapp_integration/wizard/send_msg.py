# -*- coding: utf-8 -*-

import base64
import logging
import os
import subprocess
import time
import traceback
import urllib.parse

from lxml import etree

from odoo import api, fields, models, _
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

try:
    from selenium import webdriver
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.common.by import By
    from selenium.common.exceptions import NoSuchElementException, WebDriverException, InvalidSessionIdException,\
        TimeoutException

    from odoo import api, fields, models, modules, tools
    from selenium.webdriver import DesiredCapabilities
    _silenium_lib_imported = True
except ImportError:
    _silenium_lib_imported = False
    _logger.info(
        "The `selenium` Python module is not available. "
        "WhatsApp Automation will not work. "
        "Try `pip3 install selenium` to install it."
    )

try:
    import phonenumbers
    from phonenumbers.phonenumberutil import region_code_for_country_code
    _sms_phonenumbers_lib_imported = True

except ImportError:
    _sms_phonenumbers_lib_imported = False
    _logger.info(
        "The `phonenumbers` Python module is not available. "
        "Phone number validation will be skipped. "
        "Try `pip3 install phonenumbers` to install it."
    )

sender_script = 'var newEl = document.createElement("div");' \
         'newEl.innerHTML = "<a href=\'#\' id=\'sender\' class=\'executor\'> </a> ' \
         '<a href=\'#\' id=\'grp_sender\' class=\'grp_executor\'> </a>";' \
         'var ref = document.querySelector("div#pane-side");' \
         'ref.parentNode.insertBefore(newEl, ref.previousSibling);'


class SessionRemote(webdriver.Remote):
    def start_session(self, desired_capabilities=None, browser_profile=None):
        # Skip the NEW_SESSION command issued by the original driver
        # and set only some required attributes
        self.w3c = True

class SendWAMessage(models.TransientModel):
    _name = 'isha.whatsapp.msg'
    _description = 'Send WhatsApp Message'
    driver = {}
    wait={}
    wait5={}
    is_session_open = {}
    options = {}
    msg_sent = False
    dir_path = os.path.dirname(os.path.realpath(__file__))

    def _default_unique_user(self):
        IPC = self.env['ir.config_parameter'].sudo()
        dbuuid = IPC.get_param('database.uuid')
        return dbuuid + '_' + str(self.env.uid)

    partner_ids = fields.Many2many(
        'res.partner', 'isha_whatsapp_msg_res_partner_rel',
        'wizard_id', 'partner_id', 'Recipients')
    message = fields.Text('Message', required=True)
    attachment_ids = fields.Many2many(
        'ir.attachment', 'isha_whatsapp_msg_ir_attachments_rel',
        'wizard_id', 'attachment_id', 'Attachments')
    unique_user = fields.Char(default=_default_unique_user)

    def format_amount(self, amount, currency):
        fmt = "%.{0}f".format(currency.decimal_places)
        lang = self.env['res.lang']._lang_get(self.env.context.get('lang') or 'en_US')

        formatted_amount = lang.format(fmt, currency.round(amount), grouping=True, monetary=True)\
            .replace(r' ', u'\N{NO-BREAK SPACE}').replace(r'-', u'-\N{ZERO WIDTH NO-BREAK SPACE}')

        pre = post = u''
        if currency.position == 'before':
            pre = u'{symbol}\N{NO-BREAK SPACE}'.format(symbol=currency.symbol or '')
        else:
            post = u'\N{NO-BREAK SPACE}{symbol}'.format(symbol=currency.symbol or '')

        return u'{pre}{0}{post}'.format(formatted_amount, pre=pre, post=post)

    def _phone_get_country(self, partner):
        if 'country_id' in partner:
            return partner.country_id
        return self.env.user.company_id.country_id

    def _msg_sanitization(self, country_code, number):
        if number and _sms_phonenumbers_lib_imported:
            try:
                phone_nbr = phonenumbers.parse(number, region=country_code, keep_raw_input=True)
            except phonenumbers.phonenumberutil.NumberParseException:
                return number
            if not phonenumbers.is_possible_number(phone_nbr) or not phonenumbers.is_valid_number(phone_nbr):
                return number
            phone_fmt = phonenumbers.PhoneNumberFormat.E164
            return phonenumbers.format_number(phone_nbr, phone_fmt)
        else:
            return number

    def _get_records(self, model):

        records = None
        # if model == 'pos.order':
        #     records = self.with_context(active_id=res_id, active_ids=[res_id])._get_records(model)
        if model == 'ieo.record':
            records = [x.contact_id_fkey for x in self.env['ieo.record'].browse(self._context.get('active_ids'))]
        if model == 'sadhguru.exclusive':
            records = [x.contact_id_fkey for x in self.env['sadhguru.exclusive'].browse(self._context.get('active_ids'))]
        if model == 'program.lead':
            records = [x.contact_id_fkey for x in self.env['program.lead'].browse(self._context.get('active_ids'))]
        elif model == 'res.partner':
            records = self.env['res.partner'].browse(self._context.get('active_ids'))
        if records:
            records = list(filter(lambda p: p.whatsapp_number and p.whatsapp_country_code and not p.deceased, records))
            # records = records.filtered(lambda p: p.whatsapp_number and p.whatsapp_country_code)

        return records

        # if self.env.context.get('active_domain'):
        #     records = model.search(self.env.context.get('active_domain'))
        # if self.env.context.get('active_ids'):
        #     records = model.browse(self.env.context.get('active_ids', []))
        # else:
        #     records = model.browse(self.env.context.get('active_id', []))

    @api.model
    def default_get(self, fields):
        result = super(SendWAMessage, self).default_get(fields)
        active_model = self.env.context.get('active_model')
        Attachment = self.env['ir.attachment']
        receipt_data = self.env.context.get('receipt_data')
        if not active_model:
            return result
        res_id = self.env.context.get('active_id')
        if active_model == 'pos.order':
            rec = self.env[active_model].search([('pos_reference', '=', res_id)], order='id desc', limit=1)
            if not rec:
                return result
            res_id = rec.id
            res_name = rec.pos_reference and rec.pos_reference.replace('/', '_').replace(' ', '_')
            if receipt_data:
                try:
                    specific_paperformat_args = {
                        'data-report-margin-top': 10,
                        'data-report-margin-left': 40,
                    }
                    pdf = self.env['ir.actions.report']._run_wkhtmltopdf(
                        [receipt_data.encode()],
                        landscape=True,
                        specific_paperformat_args=specific_paperformat_args
                    )
                    res = base64.b64encode(pdf)
                    filename = res_name + '.pdf'
                    attachment_data = {
                        'name': filename,
                        'datas': res,
                        'type': 'binary',
                        'store_fname': filename,
                        'res_model': active_model,
                        'res_id': res_id,
                        'mimetype': 'application/pdf'
                    }
                    attachment_id = Attachment.create(attachment_data).id
                    result['attachment_ids'] = [(6, 0, [attachment_id])]
                except Exception:
                    traceback.print_exc()

                return result
        # else:
        #     rec = self.env[active_model].browse(res_id)
        #     res_name = 'Invoice_' + rec.name.replace('/', '_') if active_model == 'account.move' else rec.name.replace('/', '_')
        msg = result.get('message', '')
        result['message'] = msg

        # if not self.env.context.get('default_recipients') and active_model and \
        #         hasattr(self.env[active_model], '_get_default_whatsapp_recipients'):
            # model = self.env[active_model]
            # if active_model == 'pos.order':
            #     records = self.with_context(active_id=res_id, active_ids=[res_id])._get_records(model)
            # else:
            #     records = self._get_records(active_model)
            # if active_model == 'res.partner' and records and self.env.context.get('from_multi_action'):
            #     records = records.filtered(lambda p: p.whatsapp_number and p.whatsapp_country_code)
            # partners = records._get_default_whatsapp_recipients()

        partners = self._get_records(active_model)
        phone_numbers = []
        no_phone_partners = []
        if active_model == 'sale.order':
            is_attachment_exists = Attachment.search([('res_id', '=', res_id), ('name', 'like', res_name + '%'), ('res_model', '=', active_model)], limit=1)
            if not is_attachment_exists:
                attachments = []
                if active_model == 'sale.order':
                    template = self.env.ref('sale.email_template_edi_sale')
                elif active_model == 'account.move':
                    template = self.env.ref('account.email_template_edi_invoice')
                elif active_model == 'purchase.order':
                    if self.env.context.get('send_rfq', False):
                        template = self.env.ref('purchase.email_template_edi_purchase')
                    else:
                        template = self.env.ref('purchase.email_template_edi_purchase_done')
                elif active_model == 'stock.picking':
                    template = self.env.ref('stock.mail_template_data_delivery_confirmation')
                elif active_model == 'account.payment':
                    template = self.env.ref('account.mail_template_data_payment_receipt')
                elif active_model == 'pos.order':
                    if rec.mapped('account_move'):
                        report = self.env.ref('point_of_sale.pos_invoice_report')
                        report_service = res_name + '.pdf'
                    else:
                        return result

                if active_model != 'pos.order':
                    report = template.report_template
                    report_service = report.report_name

                if report.report_type not in ['qweb-html', 'qweb-pdf']:
                    raise UserError(_('Unsupported report type %s found.') % report.report_type)
                res, format = report.render_qweb_pdf([res_id])
                res = base64.b64encode(res)
                if not res_name:
                    res_name = 'report.' + report_service
                ext = "." + format
                if not res_name.endswith(ext):
                    res_name += ext
                attachments.append((res_name, res))
                attachment_ids = []
                for attachment in attachments:
                    attachment_data = {
                        'name': attachment[0],
                        'datas': attachment[1],
                        'type': 'binary',
                        'res_model': active_model,
                        'res_id': res_id,
                    }
                    attachment_ids.append(Attachment.create(attachment_data).id)
                if attachment_ids:
                    result['attachment_ids'] = [(6, 0, attachment_ids)]
            else:
                result['attachment_ids'] = [(6, 0, [is_attachment_exists.id])]

        for partner in partners:
            code_field_name = self.env.context.get('code_field_name')
            number_field_name = self.env.context.get('number_field_name')
            if not code_field_name or not number_field_name:
                code_field_name = 'whatsapp_country_code'
                number_field_name = 'whatsapp_number'
            number = self._msg_sanitization(partner[code_field_name], partner[number_field_name])
            if number:
                phone_numbers.append(number)
            else:
                no_phone_partners.append(partner.name)
        if len(partners) > 1:
            if no_phone_partners:
                raise UserError(_('Missing mobile number for %s.') % ', '.join(no_phone_partners))
        if partners:
            partner_ids = list(map(lambda x: x.id, partners))
            result['partner_ids'] = [(6, 0, partner_ids)]
        return result

    def send_whatsapp_msgs(self, number, msg):
        # global driver
        # global wait
        # global wait5
        # global msg_sent
        try:
            elements  = self.driver.get(self.unique_user).find_elements_by_class_name('_3fUe9')
            if not elements:
                try:
                    landing_wrapper_xpath = "//div[contains(@class, 'landing-wrapper')]"
                    landing_wrapper = self.wait5.get(self.unique_user).until(EC.presence_of_element_located((
                        By.XPATH, landing_wrapper_xpath)))
                    try:
                        # If QR code is not scanned for sometime, it will ask to reload. "CLICK TO RELOAD QR CODE"
                        # Find that element and click to reload QR.
                        elements = self.driver.get(self.unique_user).find_elements_by_class_name('_2pf2n')
                        for e in elements:
                            e.click()
                    except:
                        pass
                    qr_code_xpath = "//canvas[contains(@aria-label, 'Scan me!')]"
                    qr_code = self.wait5.get(self.unique_user).until(EC.presence_of_element_located((
                        By.XPATH, qr_code_xpath)))
                    base64_image = self.driver.get(self.unique_user).execute_script("return document.querySelector('canvas').toDataURL('image/png');")
                    return {"isLoggedIn": False, 'qr_image': base64_image}
                # except TimeoutException as e:
                #     pass
                except NoSuchElementException as e:
                    traceback.print_exc()
                except Exception as ex:
                    traceback.print_exc()
        except NoSuchElementException as e:
            traceback.print_exc()

        try:
            elements  = self.driver.get(self.unique_user).find_elements_by_class_name('FV2Qy')
            for e in elements:
                e.click()
                time.sleep(7)
        except Exception as e:
            traceback.print_exc()

        try:
            self.driver.get(self.unique_user).find_element_by_id('sender')
        except NoSuchElementException as e:
            self.msg_sent = False
            # script = 'var newEl = document.createElement("div");' \
            #          'newEl.innerHTML = "<a href=\'#\' id=\'sender\' class=\'executor\'> </a>";' \
            #          'var ref = document.querySelector("div#pane-side");' \
            #          'ref.parentNode.insertBefore(newEl, ref.previousSibling);'
            self.driver.get(self.unique_user).execute_script(sender_script)
        try:
            send_script = "var idx = document.getElementsByClassName('executor').length -1;" +\
                          " document.getElementsByClassName('executor')[idx].setAttribute(arguments[0], arguments[1]);"
            params = {'phone': number,
                      'text': msg}
            url_params = urllib.parse.urlencode(params)
            # msg = msg.replace('\n', '%0A')
            # msg = msg.replace('&', '%26')
            send_url = "https://api.whatsapp.com/send?" + url_params
            # Below code is a POC for sending message to a whatsapp group
            # send_script = "var idx = document.getElementsByClassName('grp_executor').length -1;" +\
            #               " document.getElementsByClassName('grp_executor')[idx].setAttribute(arguments[0], arguments[1]);"
            # send_url = "https://chat.whatsapp.com/Iq1Ahc0ReKiEX0o0SQacd2"
            # self.driver.get(self.unique_user).find_element_by_id('grp_sender').click()
            self.driver.get(self.unique_user).execute_script(send_script, "href", send_url)
            # time.sleep(2)
            self.driver.get(self.unique_user).find_element_by_id('sender').click()
            # wait for the send button to show. then, we can be sure that the text was entered.
            send_button_xpath = "//button[contains(@class, '_1U1xa')]"
            self.wait5.get(self.unique_user).until(EC.presence_of_element_located((By.XPATH, send_button_xpath)))
            # Find the message input Text box by class name. It will return two elements
            # one for the Search contacts input bar on left pane, other is the actual input box to type a msg.
            # Send 'Enter' key in the input to trigger the send.
            inp_elements = self.driver.get(self.unique_user).find_elements_by_class_name('_3FRCZ')
            inp_element = inp_elements and inp_elements[1]
            if inp_element:
                # use this to send msg to a group
                # inp_element.send_keys(msg + Keys.ENTER)
                inp_element.send_keys(Keys.ENTER)
                time.sleep(0.5)

            for attachment in self.attachment_ids:
                try:
                    time.sleep(1)
                    self.driver.get(self.unique_user).find_element_by_css_selector("span[data-icon='clip']").click()
                    time.sleep(1)
                    with open("/tmp/" + attachment.name, 'wb') as tmp:
                        tmp.write(base64.decodebytes(attachment.datas))
                        self.driver.get(self.unique_user).find_element_by_css_selector("input[type='file']").send_keys(tmp.name)

                    wait_upload_xpath = "//div[contains(@class, 'TZVqr')]"
                    wait_upload = self.wait.get(self.unique_user).until(EC.presence_of_element_located((
                        By.XPATH, wait_upload_xpath)))
                    time.sleep(1)
                    self.driver.get(self.unique_user).find_element_by_css_selector("span[data-icon='send']").click()
                except:
                    pass

            self.msg_sent = True
        except Exception as e:
            self.msg_sent = False
            raise e

    def get_status(self):
        # global is_session_open
        try:
            self.driver.get(self.unique_user).title
            return True
        except WebDriverException:
            self.is_session_open[self.unique_user] = False
            return False

    def browser_session_open(self, unique_user):
        # global is_session_open
        # global options
        # global dir_path
        self.options[unique_user] = webdriver.ChromeOptions()
        # self.options[unique_user].add_argument('--user-data-dir=' + self.dir_path + '/.user_data_uid_' + str(unique_user))
        self.options[unique_user].add_argument('--headless')
        self.options[unique_user].add_argument('--no-sandbox')
        self.options[unique_user].add_argument('--window-size=1366,768')
        self.options[unique_user].add_argument('--enable-logging=stderr')
        self.options[unique_user].add_argument('--disable-gpu')
        self.options[unique_user].add_argument('--disable-dev-shm-usage')

        # user_agent = '"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36"'
        user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3312.0 Safari/537.36'
        self.options[unique_user].add_argument('user-agent='+user_agent)
        capabilities = DesiredCapabilities.CHROME.copy()
        capabilities['acceptSslCerts'] = True
        capabilities['acceptInsecureCerts'] = True
        # try:
        #     e_path = self.dir_path + '/chromedriver_79'
        #     chromium_version = subprocess.check_output(['chromium-browser', '--version'], stderr=subprocess.STDOUT)
        #     chromium_version = chromium_version and chromium_version.split()[1]
        #     if chromium_version.startswith(b'79.'):
        #         e_path = self.dir_path + '/chromedriver_79'
        #     elif chromium_version.startswith(b'77.'):
        #         e_path = self.dir_path + '/chromedriver_77'
        #     elif chromium_version.startswith(b'78.'):
        #         e_path = self.dir_path + '/chromedriver_78'
        #     elif chromium_version.startswith(b'80.'):
        #         e_path = self.dir_path + '/chromedriver_80'
        # except (subprocess.CalledProcessError, Exception):
        #     e_path = self.dir_path + '/chromedriver_79'
        #     chrome_version = subprocess.check_output(['google-chrome', '--version'], stderr=subprocess.STDOUT)
        #     chrome_version = chrome_version and chrome_version.split()[2]
        #     if chrome_version.startswith(b'79.'):
        #         e_path = self.dir_path + '/chromedriver_79'
        #     elif chrome_version.startswith(b'77.'):
        #         e_path = self.dir_path + '/chromedriver_77'
        #     elif chrome_version.startswith(b'78.'):
        #         e_path = self.dir_path + '/chromedriver_78'
        #     elif chrome_version.startswith(b'80.'):
        #         e_path = self.dir_path + '/chromedriver_80'
        e_path = self.dir_path + '/chromedriver_79'
        self.options[unique_user].binary_location = self.dir_path + '/chrome/chrome'

        self.driver[unique_user] = webdriver.Chrome(executable_path=e_path, chrome_options=self.options.get(unique_user),
                                                    desired_capabilities=capabilities)
        self.wait[unique_user] = WebDriverWait(self.driver.get(self.unique_user), 10)
        self.wait5[unique_user] = WebDriverWait(self.driver.get(self.unique_user), 5)
        self.driver.get(unique_user).get("https://web.whatsapp.com")
        IPC = self.env['ir.config_parameter'].sudo()
        uid = str(self.env.uid)
        IPC.set_param('webdriver.session_id_' + uid, self.driver.get(unique_user).session_id)
        # if self.unique_user not in self.is_session_open or not self.is_session_open[self.unique_user]:
        #     self.driver.get(unique_user).session_id = 'dummy-id'
        url = self.driver.get(unique_user).command_executor._url
        IPC.set_param('webdriver.url_' + uid, url)
        ixpath = "//div[contains(@id, 'pane-side')]"
        self.is_session_open[self.unique_user] = True
        try:
            self.wait.get(unique_user).until(EC.presence_of_element_located((
                    By.XPATH, ixpath)))
            # script = 'var newEl = document.createElement("div");' \
            #          'newEl.innerHTML = "<a href=\'#\' id=\'sender\' class=\'executor\'> </a> ' \
            #          '<a href=\'#\' id=\'grp_sender\' class=\'grp_executor\'> </a>";' \
            #          'var ref = document.querySelector("div#pane-side");' \
            #          'ref.parentNode.insertBefore(newEl, ref.previousSibling);'
            self.driver.get(unique_user).execute_script(sender_script)
        except Exception as e:
            pass

    def action_send_msg(self):
        if not _silenium_lib_imported:
            raise UserError('Silenium is not installed. Please install it.')
        # global is_session_open
        # global msg_sent
        response = None

        try:
            response = self.action_send_msg_internal()
        except InvalidSessionIdException as ex:
            response = self.action_send_msg_internal(True)

        return response

    def action_send_msg_internal(self, open_new_session=False):
        session_not_open = False
        session_id = None
        error_open_browser = False

        # try:
        _logger.info(">>>>>>>>>>> PID %s" % os.getpid())
        if open_new_session:
            self.browser_session_open(self.unique_user)
        elif not self.is_session_open.get(self.unique_user) or not self.get_status():
                session_not_open = True
                p = subprocess.Popen(['pgrep', '-f', 'chromedriver_79'], stdout=subprocess.PIPE)
                out, err = p.communicate()
                if not out:
                    self.env.cr.execute("""DELETE FROM ir_config_parameter WHERE key LIKE 'webdriver%'""")
                _logger.info(">>>>>>>>>>>>>>>>> out %s" % out)
                _logger.info(">>>>>>>>>>>>>>>>> err %s" % err)
                uid = str(self.env.uid)
                IPC = self.env['ir.config_parameter'].sudo()
                ipc_session_id = IPC.get_param('webdriver.session_id_' + uid)
                if len(out.strip()) == 0 or not ipc_session_id:
                    self.browser_session_open(self.unique_user)
                else:
                    session_id = IPC.get_param('webdriver.session_id_' + uid)
                    url = IPC.get_param('webdriver.url_' + uid)
                    desired_capabilities = DesiredCapabilities.CHROME.copy()
                    desired_capabilities['acceptSslCerts'] = True
                    desired_capabilities['acceptInsecureCerts'] = True
                    try:
                        self.driver[self.unique_user] = SessionRemote(command_executor=url, desired_capabilities=desired_capabilities)
                        self.wait[self.unique_user] = WebDriverWait(self.driver.get(self.unique_user), 10)
                        self.wait5[self.unique_user] = WebDriverWait(self.driver.get(self.unique_user), 5)
                        self.driver[self.unique_user].session_id = session_id
                    except Exception as err:
                        traceback.print_exc()
                        _logger.warning('Error in SessionRemote %s' %str(err))
        # except Exception as e:
        #     error_open_browser = True
        #     _logger.warning('Error opening Browser %s' %str(e))

        for partner in self.partner_ids:
            code_field_name = self.env.context.get('code_field_name')
            number_field_name = self.env.context.get('number_field_name')
            if not code_field_name or not number_field_name:
                code_field_name = 'whatsapp_country_code'
                number_field_name = 'whatsapp_number'

            if not code_field_name or not number_field_name:
                _logger.warning('Complete number is not available. Either country code or number is missing')

            number = partner[code_field_name] + partner[number_field_name]
            number = ''.join(no for no in number if no.isdigit())
            check = {}
            try:
                # self.message = self.message.replace('&','%20')
                check = self.send_whatsapp_msgs(number, self.message.replace('PARTNER', partner.name))
            except (InvalidSessionIdException, WebDriverException) as ex:
                raise ex
            except Exception as ex:
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__)).replace('\n', ' ')
                _logger.warning('Failed to send Message to WhatsApp number: ' + str(number) +
                                ' session_not_open: ' + str(session_not_open) + ' session_id: ' + str(session_id) +
                                ' error_open_browser: ' + str(error_open_browser) + ' stack trace: ' + tb_ex)
                # raise UserError('Got exception: ' + tb_ex)
            if check and not check.get('isLoggedIn'):
                if check.get('qr_image'):
                    img_data = check.get('qr_image')
                    view_id = self.env.ref('isha_whatsapp_integration.whatsapp_qr_view_form').id
                    context = dict(self.env.context or {})
                    context.update(qr_image=img_data, wiz_id=self.id)
                    if self.env.context.get('from_pos'):
                        return {
                            'name': 'Scan Isha WhatsApp QR Code',
                            'qr_img': img_data,
                        }
                    return {
                            'name':_("Scan Isha WhatsApp QR Code"),
                            'view_mode': 'form',
                            'view_id': view_id,
                            'res_model': 'isha.whatsapp.scan.qr',
                            'type': 'ir.actions.act_window',
                            'target': 'new',
                            'context': context,
                        }
        if self.msg_sent:
            active_model = self.env.context.get('active_model')
            if not active_model:
                return True
            res_id = self.env.context.get('active_id')
            rec = self.env[active_model].browse(res_id)
            if active_model == 'sale.order':
                rec.message_post(body=_("%s %s sent via WhatsApp") % (_('Quotation') if rec.state in ('draft', 'sent', 'cancel') else _('Sales Order'), rec.name))
                if rec.state == 'draft':
                    rec.write({'state': 'sent'})
            elif active_model == 'account.move':
                rec.message_post(body=_("Invoice %s sent via WhatsApp") % (rec.name))
            elif active_model == 'purchase.order':
                rec.message_post(body=_("%s %s sent via WhatsApp") % (_('Request for Quotation') if rec.state in ['draft', 'sent'] else _('Purchase Order'), rec.name))
                if rec.state == 'draft':
                    rec.write({'state': 'sent'})
            elif active_model == 'stock.picking':
                rec.message_post(body=_("Delivery Order %s sent via WhatsApp") % (rec.name))
            elif active_model == 'account.payment':
                rec.message_post(body=_("Payment %s sent via WhatsApp") % (rec.name))
        else:
            view_id = self.env.ref('isha_whatsapp_integration.whatsapp_retry_msg_view_form').id
            context = dict(self.env.context or {})
            context.update(wiz_id=self.id)
            return {
                    'name':_("Retry to send"),
                    'view_mode': 'form',
                    'view_id': view_id,
                    'res_model': 'isha.whatsapp.retry.msg',
                    'type': 'ir.actions.act_window',
                    'target': 'new',
                    'context': context,
                }

        return True

    def _cron_kill_chromedriver(self):
        # global driver
        for w in self.search([]):
            try:
                self.driver.get(w.unique_user).close()
                self.driver.get(w.unique_user).quit()
                self.driver[w.unique_user] = None
                self.is_session_open[w.unique_user] = None
            except Exception as e:
                pass


class ScanWAQRCode(models.TransientModel):
    _name = 'isha.whatsapp.scan.qr'
    _description = 'Scan Isha WhatsApp QR Code'

    name = fields.Char()

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        res = super(ScanWAQRCode, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu
        )
        if view_type == 'form':
            doc = etree.XML(res['arch'])
            for node in doc.xpath("//img[hasclass('qr_img')]"):
                node.set('src', self.env.context.get('qr_image'))
            res['arch'] = etree.tostring(doc, encoding='unicode')
        return res

    def action_send_msg(self):
        res_id = self.env.context.get('wiz_id')
        if res_id:
            time.sleep(5)
            self.env['isha.whatsapp.msg'].browse(res_id).action_send_msg()
        return True


class RetryWAMsg(models.TransientModel):
    _name = 'isha.whatsapp.retry.msg'
    _description = 'Retry WhatsApp Message'

    name = fields.Char()

    def action_retry_send_msg(self):
        res_id = self.env.context.get('wiz_id')
        if res_id:
            time.sleep(5)
            self.env['isha.whatsapp.msg'].browse(res_id).action_send_msg()
        return True
