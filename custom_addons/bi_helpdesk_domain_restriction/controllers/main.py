# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.
import werkzeug
import json
import base64

import odoo.http as http
from odoo.http import request
from odoo import SUPERUSER_ID
from datetime import datetime, timedelta, time
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager
import odoo.http as http

class SupportTicketDomain(CustomerPortal):

    @http.route('/support_ticket/thanks', type="http", auth="public", website=True)
    def support_ticket_thanks(self, **post):
        """Displays a thank you page after the user submits a support ticket"""
        
        if not post:
            return super(SupportTicketDomain, self).support_ticket_thanks(**post)


class Extension(SupportTicketDomain):

    @http.route('/support_ticket/thanks', type="http", auth="public", website=True)
    def support_ticket_thanks(self, **post):
        if post:

            email_from = post.get('email_from')
            if not email_from:
                return request.render("bi_helpdesk_domain_restriction.support_domain_issue_you")
            else:
                email = str(email_from)
                email_domain = email.split('@')[1]

                if not email_domain:
                    return request.render("bi_helpdesk_domain_restriction.support_domain_issue_you")
                else:
                    domains = request.env['domain.restrict'].sudo().search([('is_active','=',True)])
                    domain_list = []
                    for domain in domains:
                        for name in domain.domain_line:
                            domain_list.append(name.domain_id.name)

                    if email_domain.lower() in domain_list:
                        return request.render("bi_helpdesk_domain_restriction.support_domain_issue_you")
                    else:
                        return super(SupportTicketDomain, self).support_ticket_thanks(**post)
