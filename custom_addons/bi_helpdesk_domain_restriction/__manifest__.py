# -*- coding: utf-8 -*-
# Part of Browseinfo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Helpdesk Domain website/url Restriction in Odoo',
    'version': '13.0.0.2',
    'category' : 'Website',
    'summary': 'Helpdesk Ticket Email Domain Restriction helpdesk block domains helpdesk ticket from website Helpdesk url Restriction helpdesk request block from domain Customer Helpdesk Support Ticket website support ticket Online ticketing system for support service',
    'description': '''Helpdesk Ticket Email Domain Restriction 
	Helpdesk Email Domain Restriction
THIS APP ONLY SUPPORT ODOO COMMUNITY EDITION
This module will allow you to configure list of domain which you want to block and listed domains will not be able to create ticket from website submit form. Please see below screeenshots and video for more details.

Menus Available:
Helpdesk Domain website/url Restriction in Odoo
Helpdesk
Helpdesk/Configuration
Helpdesk/Configuration/Domains to Restrict
Helpdesk/Configuration/Domain Restriction
Restriction domain
website restriction  
	block website 
	
	''',
    'author': 'BrowseInfo',
    'website' : 'https://www.browseinfo.in',
    'price': 19,
    'currency': 'EUR',
    'depends': ['base','website','website_sale','bi_website_support_ticket'],
    'data':[
        'security/ir.model.access.csv',
        'views/helpdesk_domain_views.xml',
        'views/helpdesk_ticket_template.xml',
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'live_test_url' : 'https://youtu.be/RFCsbgT1-5Y',
    'images':['static/description/Banner.png'],
}
