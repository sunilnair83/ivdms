# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError

class DomainRestrict(models.Model):
    _name = "domain.restrict"

    def domain_line_(self):
        for domain in self:
            count = 0
            count_list = []
            for value in self.domain_line:
                if self.domain_line:
                    count_list.append(value.id)
            count = len(list(set(count_list)))
            domain.restricted_record = str(count) + ' ' + 'records'

    name = fields.Char(string="Name")
    is_active = fields.Boolean(string="Active",default=False)
    domain_line = fields.One2many('domain.line','list_id',string="domains")
    restricted_record = fields.Char(string="Domains",compute='domain_line_')


class RestrictedDomains(models.Model):
    _name = "domain.list"

    name = fields.Char(string="Domain Name")


class DomainLines(models.Model):
    _name = "domain.line"

    list_id = fields.Many2one('domain.restrict',string='List')
    domain_id = fields.Many2one('domain.list',string='Domain')