# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

import logging
from datetime import datetime

from odoo import fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


class Help_support_ticket(models.Model):
	_inherit = "support.ticket"

	def set_to_unlock(self):
		for ticket in self:
			stage_obj = self.env['support.stage'].search([('name','=','Work In Progress')])
			if ticket.is_lock == True:
				if self.env.user.name == ticket.lock_by_user:
					ticket.write({
						'is_lock' : False,
						'stage_id' : stage_obj.id,
						'lock_by_user' : self.env.user.name,
						'unlock_by_user' : self.env.user.name,
						'lock_date' : None,
						'lock_end_date' : None,
						})
				else:
					raise UserError(_('This ticket is locked by {}'.format(ticket.lock_by_user)))

	def write(self,vals):
		res = super(Help_support_ticket,self).write(vals)
		if self._context.get('dashboard'):
			return res
		for rec in self:
			if rec.is_lock == True:
				if not (self.env.user.name == rec.lock_by_user):
					raise ValidationError(_('You can not edit this ticket {} b\'coz this ticket is locked by {}'.format(rec.sequence,rec.lock_by_user)))
	#crone method to unlock ticket after lock days are ended.
	def action_unlock_ticket(self):
		for ticket in self:
			res = self.env['res.config.settings'].search([],order="id desc",limit=1)
			stage_obj = self.env['support.stage'].search([('name','=','Unlocked')])
			if res.auto_cancel == True:
				ticket.is_lock = False
				today = datetime.today()
				lockdate = datetime.strptime(str(ticket.lock_end_date),DEFAULT_SERVER_DATETIME_FORMAT)
				if (lockdate <= today):
					ticket.write({
						'is_lock' : False,
						'stage_id' : stage_obj.id,
						'lock_date' : None,
						'lock_end_date' : None,
						})
		return True

	lock_date = fields.Datetime(string="Lock Date",)
	lock_end_date = fields.Datetime(string="Lock End Date",)
	is_lock = fields.Boolean(string="Locked",default=False,readonly=True)
	lock_by_user = fields.Char(string="Lock By",readonly=True)
	unlock_by_user = fields.Char(string="Unlock By",readonly=True)
	lock_note = fields.Char()