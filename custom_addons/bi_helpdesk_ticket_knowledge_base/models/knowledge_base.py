# -*- coding: utf-8 -*-
# Part of Browseinfo. See LICENSE file for full copyright and licensing details.

from odoo import models, fields, api, _
from datetime import date, datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError, ValidationError
from odoo.tools.translate import html_translate

class KnowledgeCategory(models.Model):
    _name = "knowledge.category"
    _description = "knowledge category"

    name = fields.Char(string="knowledge Category")


class KnowledgeTags(models.Model):
    _name = "knowledge.tags"
    _description = "knowledge tags"

    name = fields.Char(string="knowledge Tags")


class KnowledgeBases(models.Model):
    _name = "knowledge.bases"
    _description = "knowledge bases"

    name = fields.Char(string="Question")
    query_solution = fields.Html('Solution', translate=html_translate)
    tag_ids = fields.Many2many('knowledge.tags',string="Tags")
    category_id = fields.Many2one('knowledge.category',string="Category")
    question_url = fields.Char(string='URL')
    helpdest_id = fields.Many2one('support.ticket',string="Ticket")

    @api.model
    def create(self,vals):
        res = super(KnowledgeBases, self).create(vals)
        res.question_url = str('/knowledge/view/detail/') + str(res.id)
        return res
        

class Website(models.Model):

    _inherit = "website"
    
    def get_knowledge_category(self):            
        knowledge_category_ids = self.env['knowledge.category'].sudo().search([])
        return knowledge_category_ids