# -*- coding: utf-8 -*-
import logging

from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)
null = None
class ExotelIntegration(http.Controller):

    @http.route('/exotel_integration/', auth='public')
    def index(self, **kw):

        # 'Status' will not be there for the passthru applet. It is only for the popup passthru
        if not kw.get('Status',None):
            # Initial call to IVR will have calltype as call-attempt
            if kw.get('CallType',None) == 'call-attempt':
                _logger.info('Exotel ---> Create : ' + kw.get('CallSid', None))
                return self.create_support_ticket(**kw)

            # update the ticket if dailcallstatus is completed
            elif kw.get('DialCallStatus') == 'completed':
                _logger.info('Exotel ---> Completed : ' + kw.get('CallSid', None))
                return self.close_support_ticket(**kw)

            # delete the ticket if caller has cancelled
            elif kw.get('CallType',None) in ['client-hangup'] or \
                    kw.get('DialCallStatus') in ['failed', 'canceled', 'busy', 'no-answer']:
                _logger.info('Exotel ---> Failed : ' + kw.get('CallSid', None))
                # return self.update_support_ticket(**kw)
                return self.delete_support_ticket(**kw)

            # ignoring the other flows
            _logger.info('Exotel ---> No Flow')
            # return self.update_support_ticket(**kw)
            return self.delete_support_ticket(**kw)

        # relieve the agent if status is 'free'
        if kw.get('Status', None) == 'free':
            _logger.info('Exotel ---> Free : ' + kw.get('CallSid', None))
            return self.relieve_agent(**kw)

        # Assignment to an agent happens with status busy
        if kw.get('Status',None) == 'busy':
            _logger.info('Exotel ---> Busy : ' + kw.get('CallSid',None))
            return self.assign_agent(kw)

        _logger.info('Exotel ---> No Flow')
        return str({'status': 200})



    def relieve_agent(self, **kw):
        agent = request.env['exotel.agent.mapping'].sudo().search([('exotel_sid', '=', kw.get('CallSid'))])
        agent.write(
                        {
                            'user_id': False,
                            'prev_user_id':agent.user_id.id
                        }
                    )
        return str({'status': 200})

    def assign_agent(self, kw):
        user_phone = kw.get('DialWhomNumber')[1:]
        user = request.env['res.users'].sudo().search([('exotel_ivr_number', '=', user_phone)])
        mapping = request.env['exotel.agent.mapping'].sudo().search([('exotel_sid', '=', kw.get('CallSid'))])

        # FIXME: Out of order processing of the api
        if len(mapping) == 0:
            return self.create_n_assign_support_ticket(user, **kw)
        else:
            mapping.write({'user_id': user.id})

        return str({'status': 200})

    def create_n_assign_support_ticket(self, user, **kw):
        parsed_dict = {
            'exotel_sid': kw.get('CallSid'),
            'phone': kw.get('CallFrom')[1:],
            'ticket_source': 'ivr',
            'name': 'IVR support ticket'
        }

        caller_matches = request.env['res.partner'].sudo().search(
            ['|', '|', '|', ('phone', '=', parsed_dict['phone']), ('phone2', '=', parsed_dict['phone']),
             ('phone3', '=', parsed_dict['phone']),
             ('phone4', '=', parsed_dict['phone'])], limit=5
        )
        if len(caller_matches) > 0:
            parsed_dict['matched_partners'] = [(6, 0, caller_matches.ids)]

        ticket = request.env['support.ticket'].sudo().create(parsed_dict)

        # create agent mapping
        request.env['exotel.agent.mapping'].sudo().create(
            {
                'exotel_sid': kw.get('CallSid'),
                'ticket_id': ticket.id,
                'active': True,
                'user_id': user.id
            }
        )
        return str({'status': 200})

    def create_support_ticket(self, **kw):
        # FIXME: Out of order processing of api
        mapping = request.env['exotel.agent.mapping'].sudo().search([('exotel_sid', '=', kw.get('CallSid'))])
        if len(mapping) != 0:
            return str({'status': 200})

        parsed_dict = {
            'exotel_sid': kw.get('CallSid'),
            'phone': kw.get('CallFrom')[1:],
            'ticket_source': 'ivr',
            'name': 'IVR support ticket'
        }

        caller_matches = request.env['res.partner'].sudo().search(
            ['|', '|', '|', ('phone', '=', parsed_dict['phone']), ('phone2', '=', parsed_dict['phone']),
             ('phone3', '=', parsed_dict['phone']),
             ('phone4', '=', parsed_dict['phone'])], limit=5
        )
        if len(caller_matches) > 0:
            parsed_dict['matched_partners'] = [(6, 0, caller_matches.ids)]

        ticket = request.env['support.ticket'].sudo().create(parsed_dict)

        # create agent mapping
        request.env['exotel.agent.mapping'].sudo().create(
            {
                'exotel_sid': kw.get('CallSid'),
                'ticket_id': ticket.id,
                'active': True
            }
        )
        return str({'status': 200})

    def close_support_ticket(self, **kw):
        ticket = request.env['support.ticket'].sudo().search([('exotel_sid', '=', kw.get('CallSid'))])
        user_phone = kw.get('DialWhomNumber')[1:]
        user = request.env['res.users'].sudo().search([('exotel_ivr_number', '=', user_phone)])
        ticket.write(
            {
                'user_id':user.id,
                'dial_call_status': kw.get('DialCallStatus', None),
                'call_type': kw.get('CallType', None)

             }
        )
        # Delete the agent mapping
        request.env['exotel.agent.mapping'].sudo().search([('exotel_sid', '=', kw.get('CallSid'))]).unlink()
        return str({'status': 200})

    def update_support_ticket(self, **kw):
        # Removing auto assignment to the last agent logic
        # user_phone = kw.get('DialWhomNumber')[1:]
        # user = request.env['res.users'].sudo().search([('exotel_ivr_number', '=', user_phone)])

        agent_mapping = request.env['exotel.agent.mapping'].sudo().search([('exotel_sid', '=', kw.get('CallSid'))])
        ticket = agent_mapping.ticket_id
        ticket.sudo().write(
            {
                'dial_call_status':kw.get('DialCallStatus',None),
                'call_type':kw.get('CallType',None),
                # 'user_id':user.id
            }
        )

        agent_mapping.unlink()
        return str({'status': 200})

    def delete_support_ticket(self, **kw):
        agent_mapping = request.env['exotel.agent.mapping'].sudo().search([('exotel_sid', '=', kw.get('CallSid'))])
        ticket = agent_mapping.ticket_id
        ticket.unlink()

        # Delete the agent mapping
        agent_mapping.unlink()

        return str({'status': 200})



    @http.route('/isha_ivr_portal', auth='public', website=True)
    def ivr_portal_index(self, **kw):
        if request.env.user.has_group('base.group_public'):
            return request.redirect('/home')
        partners = None
        ticket = None

        if 'exotel_ivr_number' in kw:
            request.env.user.write({'exotel_ivr_number': kw['exotel_ivr_number']})
        elif 'user_id' in kw:
            mapping = request.env['exotel.agent.mapping'].sudo().search([('user_id', '=', int(kw['user_id']))])
            if len(mapping) > 0:
                partners = mapping[0].ticket_id.matched_partners
                ticket = mapping[0].ticket_id
        elif 'ticket_submit' in kw:
            ticket = request.env['support.ticket'].sudo().search([('id','=',int(kw['ticket_id']))])
            if ticket and (not(ticket.user_id) or ticket.user_id.id == request.env.user.id):
                update_vals = {'user_id':request.env.user.id}
                fields = ['description','contact_name','phone','email_from']
                for x in fields:
                    if kw.get(x,None):
                        update_vals[x] = kw[x]
                if 'contact_feedback' in kw and kw['contact_feedback']:
                    update_vals['contact_feedback'] = kw['contact_feedback']
                if 'stage_id' in kw and kw['stage_id']:
                    update_vals['stage_id'] = int(kw['stage_id'])
                if 'support_team_id' in kw and kw['support_team_id']:
                    update_vals['support_team_id'] = int(kw['support_team_id'])
                    update_vals['support_ticket_category'] = request.env['support.team'].sudo().browse(update_vals['support_team_id']).support_ticket_category.id
                ticket.write(update_vals)
            # Ensure the agent mapping is cleared for the ticket if the callback has failed
            mapping = request.env['exotel.agent.mapping'].sudo().search([('user_id', '=', request.env.user.id),('ticket_id','=',int(kw['ticket_id']))])
            if mapping:
                mapping.unlink()
            partners = None
            ticket = None


        return request.render('exotel_integration.exotel_integration',{'user':request.env.user,'partner_ids':partners,'ticket_id':ticket})


