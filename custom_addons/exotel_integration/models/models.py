# -*- coding: utf-8 -*-

from odoo import models, fields


class exotel_integration(models.Model):
    _name = 'exotel.agent.mapping'
    _description = 'Exotel Agent Mapping'
    _order = 'write_date desc'

    exotel_sid = fields.Char(string='Exotel SID',index=True)
    user_id = fields.Many2one('res.users',string='Agent',index=True)
    prev_user_id = fields.Integer(string='Previous user id')
    ticket_id = fields.Many2one('support.ticket',string='Ticket', index=True, ondelete='set null')
    active = fields.Boolean(string='active', index=True, default=True)

    def unlink(self):
        self.write({'active': False})
