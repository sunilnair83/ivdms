# -*- encoding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name': 'CallCampaigns',
    'version': '3.0',
    'category': 'Marketing/CallCampaign',
    'description': """
Create beautiful call_campaigns and visualize answers
==============================================

It depends on the answers or reviews of some questions by different users. A
call_campaign may have multiple pages. Each page may contain multiple questions and
each question may have multiple answers. Different users may give different
answers of question and according to that call_campaign is done. Partners are also
sent mails with personal token for the invitation of the call_campaign.
    """,
    'summary': 'Create call_campaigns and analyze answers',
    'website': 'https://www.odoo.com/page/call_campaign',
    'depends': [
        'auth_signup',
        'http_routing',
        'mail',
        'web_tour',
        'gamification','isha_crm'],
    'data': [
        'data/mail_template_data.xml',
        'data/ir_actions_data.xml',
        'security/call_campaign_security.xml',
        'security/ir.model.access.csv',
        'views/assets.xml',
        'views/call_campaign_menus.xml',
        'views/call_campaign_call_campaign_views.xml',
        'views/call_campaign_user_views.xml',
        'views/call_campaign_question_views.xml',
        'views/call_campaign_templates.xml',
        'views/res_partner.xml',
        'wizard/call_campaign_invite_views.xml',
        'wizard/call_campaign_allocate_views.xml',
    ],
    'demo': [

    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'sequence': 105,
}
