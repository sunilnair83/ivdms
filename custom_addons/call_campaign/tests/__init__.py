# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import common
from . import test_call_campaign
from . import test_call_campaign_flow
from . import test_certification_flow
from . import test_call_campaign_invite
from . import test_call_campaign_security
from . import test_call_campaign_randomize
from . import test_call_campaign_ui
from . import test_call_campaign_compute_pages_questions
from . import test_certification_badge
