# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
import traceback
import uuid

from werkzeug import urls

from ...isha_crm.models.constants import CHARITABLE_GROUPS, RELIGIOUS_GROUPS
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.http import request

_logger = logging.getLogger(__name__)
class CallCampaign(models.Model):
    """ Settings for a multi-page/multi-question call_campaign. Each call_campaign can have one or more attached pages
    and each page can display one or more questions. """
    _name = 'call_campaign.call_campaign'
    _description = 'CallCampaign'
    _rec_name = 'title'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    def _get_default_access_token(self):
        return str(uuid.uuid4())

    # description
    title = fields.Char('CallCampaign Title', required=True, translate=True)
    description = fields.Html("Description", translate=True,
        help="The description will be displayed on the home page of the call_campaign. You can use this to give the purpose and guidelines to your candidates before they start it.")

    center_id = fields.Many2one('isha.center', string='Center', required=True, index=True,
                                default=lambda self: self.env.user.centers.ids[0] if self.env.user.centers else False)
    region_id = fields.Many2one(string='Region', related='center_id.region_id', store=True)
    color = fields.Integer('Color Index', default=0)
    thank_you_message = fields.Html("Thanks Message", translate=True, help="This message will be displayed when call_campaign is completed")
    active = fields.Boolean("Active", default=True)
    question_and_page_ids = fields.One2many('call_campaign.question', 'call_campaign_id', string='Sections and Questions', copy=True)
    page_ids = fields.One2many('call_campaign.question', string='Pages', compute="_compute_page_and_question_ids")
    question_ids = fields.One2many('call_campaign.question', string='Questions', compute="_compute_page_and_question_ids")
    state = fields.Selection(
        string="CallCampaign Stage",
        selection=[
                ('draft', 'Draft'),
                ('open', 'In Progress'),
                ('closed', 'Closed'),
        ], default='draft', required=True,
        group_expand='_read_group_states'
    )

    # content
    user_input_ids = fields.One2many('call_campaign.user_input', 'call_campaign_id', string='User responses',
                                     groups='call_campaign.group_call_campaign_user', copy=True)
    # security / access
    access_mode = fields.Selection([
        ('public', 'Anyone with the link'),
        ('token', 'Invited people only')], string='Access Mode',
        default='token', required=False)
    access_token = fields.Char('Access Token', default=lambda self: self._get_default_access_token(), copy=False)
    users_login_required = fields.Boolean('Login Required', default=False,
                                          help="If checked, users have to login before answering even with a valid token.")
    # users_can_signup = fields.Boolean('Users can signup', compute='_compute_users_can_signup')
    users_can_signup = fields.Boolean('Users can signup', default=False)

    # call details
    call_script = fields.Text(string='Call Script')
    coordinator_id = fields.Many2one('res.users', domain=[('share', '=', False)], string='Coordinator', required=False,
                                     ondelete='set null')
    callers = fields.One2many('call_campaign.caller', 'call_campaign_id', string='Callers',copy=True)
    caller_count = fields.Integer('Caller Count', compute='_compute_caller_count', store=True)
    avg_allocation_count = fields.Integer(string="Average contacts per caller")

    show_gender = fields.Boolean('Show Gender', default=True)
    show_dob = fields.Boolean('Show Date Of Birth', default=False)
    show_occupation = fields.Boolean('Show Occupation', default=True)
    show_programs = fields.Boolean('Show Programs', default=True)
    show_feedback_selection = fields.Boolean('Show Feedback selection', default=True)
    get_unassigned_contacts = fields.Boolean('Get unassigned contacts', default=False)

    allow_follow_up_sms = fields.Boolean('Allow follow up SMS')

    follow_up_sms_text = fields.Text('Follow up SMS text')
    follow_up_sms_reg_link = fields.Text('Reg Link in the SMS', help='Replaces * in the SMS text')
    follow_up_sms_info_link = fields.Text('Video Link in the SMS', help='Appends to the SMS text')
    sms_test_number = fields.Char('Test Number', help='SMS from call page will be sent to this number. Clear this when campaign starts.')

    follow_up_sms_template_id = fields.Many2one(
        'sms.template', string='SMS Template',
        domain=[('model', '=', 'call_campaign.call_campaign')], ondelete='restrict',
        default=lambda self: self.env.ref('call_campaign.sms_template_call_campaign_follow_ups_sms',
                                          raise_if_not_found=False),
        help='This field contains the template of the SMS that caller can send to the person')
    show_prev_remark = fields.Boolean('Show Previous Remark to caller', default=False)
    show_address = fields.Boolean('Show Address', default=False)
    show_country = fields.Boolean('Show Country', default=False)
    show_city = fields.Boolean('Show City', default=False)
    show_state = fields.Boolean('Show State', default=False)
    show_mv_details = fields.Boolean('Show MV Details', default=False)
    show_sgex_details = fields.Boolean('Show Sadhguru Exclusive Details', default=False)

    # allow updates on res_partner
    update_profile = fields.Boolean('Allow Caller to Update Profile', default=False)

    is_sensitive = fields.Boolean('Sensitive Call Campaign', default=False)

    campaign_track_period = fields.Integer(string='Track Campaign Period', default=7)

    suppression_sync_interval = fields.Integer(string='Suppression Sync Interval (Hours)', default='1')
    last_suppression_sync_time = fields.Datetime(string='Last Suppression Sync Time')
    suppression_domains = fields.Many2many(comodel_name='ir.filters', relation='call_campaign_suppression_rel',
                                          string='Suppression Filters')

    parent_id = fields.Many2one(comodel_name='call_campaign.call_campaign',string='Parent Campaign')
    # def _compute_users_can_signup(self):
    #     signup_allowed = self.env['res.users'].sudo()._get_signup_invitation_scope() == 'b2c'
    #     for call_campaign in self:
    #         call_campaign.users_can_signup = signup_allowed

    @api.onchange('follow_up_sms_template_id','follow_up_sms_reg_link','follow_up_sms_info_link')
    def _onchange_template_id(self):
        """ UPDATE ME """
        if len(self.ids) == 0: #create flow
            self.follow_up_sms_text = self.follow_up_sms_template_id.body
            return

        if self.follow_up_sms_template_id:
            subject = self.env['sms.template']._render_template(self.follow_up_sms_template_id.body,
                                                                'call_campaign.call_campaign', self.ids[0])
            self.follow_up_sms_text = subject
        else:
            self.follow_up_sms_text = None

    @api.depends('question_and_page_ids')
    def _compute_page_and_question_ids(self):
        for call_campaign in self:
            call_campaign.page_ids = call_campaign.question_and_page_ids.filtered(
                lambda question: question.is_page)

            call_campaign.question_ids = call_campaign.question_and_page_ids - call_campaign.page_ids

    @api.depends('callers')
    def _compute_caller_count(self):
        self.caller_count = len(self.callers)

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        ret_val = super(CallCampaign, self).fields_view_get(
            view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)
        if 'center_id' in ret_val['fields']:
            ret_val['fields']['center_id']['domain'] = [('id', 'in', self.env.user.centers.ids)]
        return ret_val

    def action_allocate_callers(self):
        action = self.env.ref('isha_crm.volunteer_action_server').read()[0]
        request.session['campaign_id'] = self.id
        return action

    def action_add_people(self):
        action = self.env.ref('isha_crm.all_center_action_server').read()[0]
        request.session['campaign_id'] = self.id
        return action

    def action_add_people_from_event(self):
        action = self.env.ref('event.action_event_view').read()[0]
        request.session['campaign_id'] = self.id
        return action

    public_url = fields.Char("Public link", compute="_compute_call_campaign_url")

    # statistics
    # FIXME: removing the store due to perf issue
    answer_count = fields.Integer("Registered", compute="_compute_call_campaign_statistic") #, store=True)
    answer_done_count = fields.Integer("Calls made", compute="_compute_call_campaign_statistic") #, store=True)

    _sql_constraints = [
        ('access_token_unique', 'unique(access_token)', 'Access token should be unique')
    ]

    # @api.depends('user_input_ids.state', 'user_input_ids.test_entry')
    def _compute_call_campaign_statistic(self):
        default_vals = {
            'answer_count': 0, 'answer_done_count': 0
        }
        stat = dict((cid, dict(default_vals)) for cid in self.ids)
        UserInput = self.env['call_campaign.user_input']
        base_domain = ['&', ('call_campaign_id', 'in', self.ids), ('test_entry', '!=', True)]

        read_group_res = UserInput.read_group(base_domain, ['call_campaign_id', 'state'], ['call_campaign_id', 'state'], lazy=False)
        for item in read_group_res:
            stat[item['call_campaign_id'][0]]['answer_count'] += item['__count']
            if item['state'] == 'done':
                stat[item['call_campaign_id'][0]]['answer_done_count'] += item['__count']

        for call_campaign in self:
            call_campaign.update(stat.get(call_campaign._origin.id, default_vals))

    def _compute_call_campaign_url(self):
        """ Computes a public URL for the call_campaign """
        base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        for call_campaign in self:
            call_campaign.public_url = urls.url_join(base_url, "call_campaign/start/%s" % (call_campaign.access_token))

    def _read_group_states(self, values, domain, order):
        selection = self.env['call_campaign.call_campaign'].fields_get(allfields=['state'])['state']['selection']
        return [s[0] for s in selection]

    # ------------------------------------------------------------
    # CRUD
    # ------------------------------------------------------------

    def copy(self, default=None):
        new = super(CallCampaign, self).copy(default)

        charitable_user = self.user_has_groups(CHARITABLE_GROUPS)
        religious_user = self.user_has_groups(RELIGIOUS_GROUPS)

        if not charitable_user or not religious_user:
            callers_to_remove = []
            for caller in new.callers.sudo():
                if (caller.partner_id.has_charitable_txn and charitable_user) or (caller.partner_id.has_religious_txn
                    and religious_user):
                    continue
                else:
                    callers_to_remove.append(caller.id)
            if callers_to_remove:
                for caller in callers_to_remove:
                    new.write({'callers': [(3, caller)]})

            meditators_to_remove = []
            for meditator in new.user_input_ids.sudo():
                if (meditator.partner_id.has_charitable_txn and charitable_user) or (meditator.partner_id.has_religious_txn
                    and religious_user):
                    continue
                else:
                    meditators_to_remove.append(meditator.id)
            if meditators_to_remove:
                for med in meditators_to_remove:
                    new.write({'user_input_ids': [(3, med)]})


        # copy the assignments
        for x in new.user_input_ids:
            if x.caller_id:
                try:
                    matching_caller_ids = new.callers.filtered(lambda t: t.partner_id == x.caller_id.partner_id).ids
                    if len(matching_caller_ids) > 0:
                        x.caller_id = matching_caller_ids[0]
                    else:
                        _logger.warning("Could not find matching caller:" + str(x.caller_id.id) +
                                        " for user_input_id:" +str(x.id)+ "." +
                                        "Caller might have been deleted from the campaign.")
                        x.caller_id = None
                except Exception as ex:
                    tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                    _logger.warning(str(tb_ex) + str(x.caller_id) + str(new.callers))
                    #raise UserError(str(tb_ex) + str(x.caller_id) + str(new.callers))

        return new

    def copy_data(self, default=None):
        title = _("%s (copy)") % (self.title) if not default or 'title' not in default else default['title']
        default = dict(default or {}, title=title)
        return super(CallCampaign, self).copy_data(default)

    # ------------------------------------------------------------
    # TECHNICAL
    # ------------------------------------------------------------

    def _check_answer_creation(self, user, partner, email, test_entry=False, check_attempts=True, invite_token=False):
        """ Ensure conditions to create new tokens are met. """
        self.ensure_one()
        if test_entry:
            # the current user must have the access rights to call_campaign
            if not user.has_group('call_campaign.group_call_campaign_user'):
                raise UserError(_('Creating test token is not allowed for you.'))
        else:
            if not self.active:
                raise UserError(_('Creating token for archived call_campaigns is not allowed.'))
            elif self.state == 'closed':
                raise UserError(_('Creating token for closed call_campaigns is not allowed.'))
            # no signup possible -> should be a not public user (employee or portal users)

    def _prepare_answer_questions(self):
        """ Will generate the questions for a randomized call_campaign.
        It uses the random_questions_count of every sections of the call_campaign to
        pick a random number of questions and returns the merged recordset """
        self.ensure_one()

        questions = self.env['call_campaign.question']

        # First append questions without page
        for question in self.question_ids:
            if not question.page_id:
                questions |= question

        return questions

    # ------------------------------------------------------------
    # ACTIONS
    # ------------------------------------------------------------

    def action_draft(self):
        self.write({'state': 'draft'})

    def action_open(self):
        self.write({'state': 'open'})

    def action_close(self):
        self.write({'state': 'closed'})

    def action_start_call_campaign(self):
        """ Open the website page with the call_campaign form """
        self.ensure_one()
        token = self.env.context.get('call_campaign_token')
        trail = "?caller_token=%s" % token if token else ""
        return {
            'type': 'ir.actions.act_url',
            'name': "Start CallCampaign",
            'target': 'self',
            'url': self.public_url + trail
        }

    def action_send_call_campaign(self):
        """ Open a window to compose an email, pre-filled with the call_campaign message """
        # Ensure that this call_campaign has at least one page with at least one question.
        if not self.callers or not self.user_input_ids:
            raise UserError(_('Please add at least one caller and one contact to the campaign.'))

        if self.state == 'closed':
            raise UserError(_("You cannot send invitations for closed call_campaigns."))

        template = self.env.ref('call_campaign.mail_template_call_campaign_caller_invite', raise_if_not_found=False)

        local_context = dict(
            self.env.context,
            default_call_campaign_id=self.id,
            default_use_template=bool(template),
            default_template_id=template and template.id or False,
            notif_layout='mail.mail_notification_light',
        )
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'call_campaign.invite',
            'target': 'new',
            'context': local_context,
        }

    def action_test_call_campaign(self):
        ''' Open the website page with the call_campaign form into test mode'''
        self.ensure_one()
        return {
            'type': 'ir.actions.act_url',
            'name': "Test CallCampaign",
            'target': 'self',
            'url': '/call_campaign/test/%s' % self.access_token,
        }

    def action_call_campaign_user_input_line_with_answers(self):
        action_rec = self.env.ref('call_campaign.action_call_campaign_user_input_line')
        action = action_rec.read()[0]
        ctx = dict(self.env.context)
        ctx.update({'search_default_call_campaign_id': self.ids[0],
                    'search_default_not_test': 1})
        action['context'] = ctx
        return action

    def action_call_campaign_user_input_completed(self):
        action_rec = self.env.ref('call_campaign.action_call_campaign_user_input')
        action = action_rec.read()[0]
        ctx = dict(self.env.context)
        ctx.update({'search_default_call_campaign_id': self.ids[0],
                    'search_default_completed': 1,
                    'search_default_not_test': 1})
        action['context'] = ctx
        return action

    def action_call_campaign_user_input(self):
        action_rec = self.env.ref('call_campaign.action_call_campaign_user_input')
        action = action_rec.read()[0]
        ctx = dict(self.env.context)
        ctx.update({'search_default_call_campaign_id': self.ids[0],
                    'search_default_not_test': 1})
        action['context'] = ctx
        return action
