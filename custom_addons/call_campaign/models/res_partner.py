# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    callered_ids = fields.One2many('call_campaign.caller','partner_id','Campaigns callered for')

    def _add_domain(self):
        domain = [('state', '=', 'done')]
        return domain

    # call campaign
    contact_call_campaign_id_fkey = fields.One2many('call_campaign.user_input', 'partner_id',
                                                    'Call Campaign Contact', domain=_add_domain)
    def get_caller_id(self,campaign_id):
        caller_ids = self.callered_ids.filtered(lambda rec: rec.call_campaign_id.id == campaign_id)
        return caller_ids[0] if len(caller_ids) > 0 else None

class PartnerImportTempInherit(models.TransientModel):
    """ wizard to perform action on imported partner """

    _inherit = 'partner.import.temp.wizard'
    _description = 'wizard to perform action on imported partner'

    campaign_id = fields.Many2one('call_campaign.call_campaign', string='Campaign')
    caller_id = fields.Many2one('call_campaign.caller', string='Pre-assign Caller (Optional)', required=False)