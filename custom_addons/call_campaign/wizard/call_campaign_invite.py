# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import logging
import re

import requests

from odoo import api, fields, models, tools, _
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

emails_split = re.compile(r"[;,\n\r]+")


class CallCampaignInvite(models.TransientModel):
    _name = 'call_campaign.invite'
    _description = 'CallCampaign Invitation Wizard'

    @api.model
    def _get_default_from(self):
        if self.env.user.email:
            return tools.formataddr((self.env.user.name, self.env.user.email))
        raise UserError(_("Unable to post message, please configure the sender's email address."))

    @api.model
    def _get_default_author(self):
        return self.env.user.partner_id

    # composer content
    subject = fields.Char('Subject')
    body = fields.Html('Contents', default='', sanitize_style=True)
    attachment_ids = fields.Many2many(
        'ir.attachment', 'call_campaign_mail_compose_message_ir_attachments_rel', 'wizard_id', 'attachment_id',
        string='Attachments')
    template_id = fields.Many2one(
        'mail.template', 'Use template', index=True,
        domain="[('model', '=', 'call_campaign.caller')]")
    # origin
    email_from = fields.Char('From', default=_get_default_from, help="Email address of the sender.")
    author_id = fields.Many2one(
        'res.partner', 'Author', index=True,
        ondelete='set null', default=_get_default_author,
        help="Author of the message.")
    # recipients
    partner_ids = fields.Many2many(
        'res.partner', 'call_campaign_invite_partner_ids', 'invite_id', 'partner_id', string='Recipients')
    existing_partner_ids = fields.Many2many(
        'res.partner', compute='_compute_existing_partner_ids', readonly=True, store=False)
    emails = fields.Text(string='Additional emails', help="This list of emails of recipients will not be converted in contacts.\
        Emails must be separated by commas, semicolons or newline.")
    existing_mode = fields.Selection([
        ('new', 'New invite'), ('resend', 'Resend invite')],
        string='Handle existing', default='resend', required=True)
    existing_text = fields.Text('Resend Comment', compute='_compute_existing_text', readonly=True, store=False)
    # technical info
    mail_server_id = fields.Many2one('ir.mail_server', 'Outgoing mail server')
    # call_campaign
    call_campaign_id = fields.Many2one('call_campaign.call_campaign', string='CallCampaign', required=True)
    call_campaign_url = fields.Char(related="call_campaign_id.public_url", readonly=True)
    call_campaign_access_mode = fields.Selection(related="call_campaign_id.access_mode", readonly=True)
    call_campaign_users_login_required = fields.Boolean(related="call_campaign_id.users_login_required", readonly=True)
    deadline = fields.Datetime(string="Answer deadline")

    @api.depends('partner_ids', 'call_campaign_id')
    def _compute_existing_partner_ids(self):
        existing_callers = self.call_campaign_id.callers
        self.existing_partner_ids = existing_callers.mapped('partner_id') & self.partner_ids

    @api.depends('existing_partner_ids')
    def _compute_existing_text(self):
        existing_text = False
        if self.existing_partner_ids:
            existing_text = '%s: %s.' % (
                _('The following customers have already received an invite'),
                ', '.join(self.mapped('existing_partner_ids.name'))
            )

        self.existing_text = existing_text

    @api.onchange('emails')
    def _onchange_emails(self):
        if self.emails and (self.call_campaign_users_login_required and not self.call_campaign_id.users_can_signup):
            raise UserError(_('This call_campaign does not allow external people to participate. You should create user accounts or update call_campaign access mode accordingly.'))
        if not self.emails:
            return
        valid, error = [], []
        emails = list(set(emails_split.split(self.emails or "")))
        for email in emails:
            email_check = tools.email_split_and_format(email)
            if not email_check:
                error.append(email)
            else:
                valid.extend(email_check)
        if error:
            raise UserError(_("Some emails you just entered are incorrect: %s") % (', '.join(error)))
        self.emails = '\n'.join(valid)

    @api.onchange('call_campaign_users_login_required')
    def _onchange_call_campaign_users_login_required(self):
        if self.call_campaign_users_login_required and not self.call_campaign_id.users_can_signup:
            return {'domain': {
                    'partner_ids': [('user_ids', '!=', False)]
                    }}
        return {'domain': {
                'partner_ids': []
                }}

    @api.onchange('partner_ids')
    def _onchange_partner_ids(self):
        if self.call_campaign_users_login_required and self.partner_ids:
            if not self.call_campaign_id.users_can_signup:
                invalid_partners = self.env['res.partner'].search([
                    ('user_ids', '=', False),
                    ('id', 'in', self.partner_ids.ids)
                ])
                if invalid_partners:
                    raise UserError(
                        _('The following recipients have no user account: %s. You should create user accounts for them or allow external signup in configuration.' %
                            (','.join(invalid_partners.mapped('name')))))

    @api.onchange('template_id')
    def _onchange_template_id(self):
        """ UPDATE ME """
        if self.template_id:
            self.subject = self.template_id.subject
            self.body = self.template_id.body_html

    @api.model
    def create(self, values):
        if values.get('template_id') and not (values.get('body') or values.get('subject')):
            template = self.env['mail.template'].browse(values['template_id'])
            if not values.get('subject'):
                values['subject'] = template.subject
            if not values.get('body'):
                values['body'] = template.body_html
        return super(CallCampaignInvite, self).create(values)

    #------------------------------------------------------
    # Wizard validation and send
    #------------------------------------------------------

    def _get_answers_values(self):
        return {
            'input_type': 'link',
            'deadline': self.deadline,
        }

    def _send_mail(self, caller):
        """ Create mail specific for recipient containing notably its access token """
        subject = self.env['mail.template']._render_template(self.subject, 'call_campaign.caller', caller.id, post_process=True)
        body = self.env['mail.template']._render_template(self.body, 'call_campaign.caller', caller.id, post_process=True)

        # post the message
        mail_values = {
            'email_from': self.email_from,
            'author_id': self.author_id.id,
            'model': None,
            'res_id': None,
            'subject': subject,
            'body_html': body,
            'attachment_ids': [(4, att.id) for att in self.attachment_ids],
            'auto_delete': True,
        }

        mail_values['recipient_ids'] = [(4, caller.partner_id.id)]

        # optional support of notif_layout in context
        notif_layout = self.env.context.get('notif_layout', self.env.context.get('custom_layout'))
        if notif_layout:
            try:
                template = self.env.ref(notif_layout, raise_if_not_found=True)
            except ValueError:
                _logger.warning('QWeb template %s not found when sending call_campaign mails. Sending without layouting.' % (notif_layout))
            else:
                template_ctx = {
                    'message': self.env['mail.message'].sudo().new(dict(body=mail_values['body_html'], record_name=self.call_campaign_id.title)),
                    'model_description': self.env['ir.model']._get('call_campaign.call_campaign').display_name,
                    'company': self.env.company,
                }
                body = template.render(template_ctx, engine='ir.qweb', minimal_qcontext=True)
                mail_values['body_html'] = self.env['mail.thread']._replace_local_links(body)

        mail = self.env['mail.mail'].sudo().create(mail_values)
        return mail.send(False)

    def action_invite(self):
        """ Process the wizard content and proceed with sending the related
            email(s), rendering any template patterns on the fly if needed """
        caller_list = []
        if self.partner_ids:
            for partner in self.partner_ids:
                caller_id = partner.get_caller_id(self.call_campaign_id.id)
                caller_list.append(caller_id) if caller_id else None
        else:
            caller_list = self.call_campaign_id.callers

        for caller in caller_list:
            self._send_mail(caller)
            # self._action_send_sms_numbers(caller)

        return {'type': 'ir.actions.act_window_close'}

    # def _action_send_sms_numbers(self, caller):
    #     self.env['sms.api']._action_send_sms([{
    #         'number': number,
    #         'content': self.call_campaign_id.follow_up_sms_text,
    #     } for number in [caller.partner_id.phone]])
    #     return True
