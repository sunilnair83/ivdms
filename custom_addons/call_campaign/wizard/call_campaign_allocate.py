# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import datetime
import logging

from odoo import api, fields, models
from odoo.exceptions import UserError
from odoo.http import request

_logger = logging.getLogger(__name__)
"""
This mapper is used to find the field by with the campaigns centers are reallocated dynamically for the split campaign
For any new model getting added to call campaign, we need to add a mapper entry
Query reference:
    select model,name as orm,related from ir_model_fields where model = <new model> and ttype='many2one' and relation = 'isha.center'
"""
center_reallocation_mapper = {
    "call_campaign.user_input":{"orm":"partner_center_id","related":"partner_id.center_id"},
    "call_campaign.user_input_line":{"orm":"center_id","related":"user_input_id.partner_id.center_id"},
    "program.lead":{"orm":"center_id","related":"contact_id_fkey.center_id"},
    "program.attendance":{"orm":"center_id","related":"contact_id_fkey.center_id"},
    "ieo.record":{"orm":"partner_center_id","related":"contact_id_fkey.center_id"},
    "sadhguru.exclusive":{"orm":"center_id","related":"contact_id_fkey.center_id"},
    "res.partner":{"orm":"center_id","related":False},
    "res.users":{"orm":"center_id","related":"partner_id.center_id"},
    "iec.mega.pgm":{"orm":"partner_center_id","related":"partner_id.center_id"},
    "event.registration":{"orm":"partner_center_id","related":"partner_id.center_id"},
    "covid.support":{"orm":"center_id","related":"contact_id_fkey.center_id"},
    "programs.view.orm":{"orm":"center_id","related":False},
    "rudraksha.deeksha":{"orm":"center_id","related":"contact_id_fkey.center_id"},
    "completion.feedback":{"orm":"center_id","related":"contact_id_fkey.center_id"},
    "partner.import.temp":{"orm":"center_id","related":False}
}


class IshaCallCampaignContactAllocation (models.TransientModel):
    """ wizard to convert contacts into Campaign Leads """

    _name = 'isha.call_campaign.contact.allocation'
    _description = 'Contact convert to Lead'

    campaign_id = fields.Many2one('call_campaign.call_campaign', string='Campaign')
    caller_id = fields.Many2one('call_campaign.caller', string='Pre-assign Caller (Optional)', required=False)
    selected_count = fields.Integer(string='Selected count')
    selection_mode = fields.Selection([('selected_contact','Only Selected Contacts'),('selected_filter', 'All Filtered Contacts')],
                                      string='Selection Mode')

    spit_campaign = fields.Boolean(string='Split the campaign ?')
    base_campaign = fields.Many2one('call_campaign.call_campaign',string='Base campaign')
    def get_current_model(self):
        return [('model','=',self._context.get('active_model'))]
    split_fields = fields.Many2many('ir.model.fields', string='Split by',domain=get_current_model)
    center_reallocation = fields.Boolean(string="ReMap Campaign center")


    @api.onchange('selection_mode')
    def get_ids_for_selection(self):
        for x in self:
            if self.selection_mode:
                x.selected_count = len(x.get_valid_partners_set())

    def get_valid_partners_set(self):
        partner_recs = []
        if not self.selection_mode or self.selection_mode == 'selected_contact':
            base_ids = ','.join([str(x) for x in self._context.get('active_ids')])
            where_clause_params = []
        else:
            table_name = self.env[self.env.context['active_model']]._table
            from_clause, where_clause, where_clause_params = self.env[self.env.context['active_model']].get_query_params(self.env.context['active_domain'])
            where_str = where_clause and (" WHERE %s" % where_clause) or ''
            base_query = 'SELECT "%s".id FROM %s %s'
            base_ids = base_query % (table_name, from_clause, where_str)

        center_ids = ','.join([str(x) for x in self.env.user.centers.ids])
        charitable = self.user_has_groups('isha_crm.group_all_contacts_grp,isha_crm.group_ieo_grp,isha_crm.group_online_satsang_grp,isha_crm.group_online_pournami_grp,isha_crm.group_mandala_app_grp,isha_crm.group_sadhguru_exclusive_grp,isha_crm.group_webinars_grp,isha_crm.group_sannidhi_grp,isha_crm.group_reports_grp,isha_crm.group_forum_grp,isha_crm.group_santosha_regional_admin,isha_crm.group_graceofyoga,isha_crm.group_covid_support,isha_crm.group_rfr_youth,isha_crm.group_global_search')
        religious = self.user_has_groups('isha_crm.group_religious_santosha,isha_crm.group_all_contacts_religious,isha_crm.group_vanashree_grp,isha_crm.group_rudraksha_deeksha,isha_crm.group_global_search_religious')
        charitable_clause = ' (rp.has_charitable_txn = true) '
        religious_clause = ' (rp.has_religious_txn = true) '

        suffix_array = []
        if charitable:
            suffix_array.append(charitable_clause)
        if religious:
            suffix_array.append(religious_clause)

        suffix_clause = ''
        if suffix_array:
            suffix_clause = ' and (' + ' or '.join(suffix_array) + ')'

        if self._context.get('active_model') == 'call_campaign.user_input':
            base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,rp.influencer_type,
            rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id FROM res_partner rp, call_campaign_user_input ccui 
            where ccui.partner_id = rp.id and ccui.id in (%s) 
            /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'call_campaign.user_input_line':
             base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,rp.influencer_type,
             rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id FROM res_partner rp, call_campaign_user_input ccui, call_campaign_user_input_line ccuil  
             where ccui.partner_id = rp.id and ccuil.user_input_id = ccui.id and ccuil.id in (%s) 
             /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
             self.env.cr.execute(base_query, where_clause_params)
             partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'program.lead':
            base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,
                                 rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
                                 FROM res_partner rp, program_lead pl where rp.id = pl.contact_id_fkey and pl.id in (%s) 
                                 /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'program.attendance':
            base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
            FROM res_partner rp, program_attendance pa where pa.contact_id_fkey = rp.id and pa.id in (%s) 
            /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'ieo.record':
            base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
            FROM res_partner rp, ieo_record ir where ir.contact_id_fkey = rp.id and ir.contact_id_fkey is not null and ir.id in (%s) 
            /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'event.registration':
            base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
            FROM res_partner rp, event_registration er where er.partner_id = rp.id and er.id in (%s) 
            /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'res.users':
            base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,rp.influencer_type,
            rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
            FROM res_partner rp, res_users ru where ru.partner_id = rp.id and ru.id in (%s) 
            /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'iec.mega.pgm':
            base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
            FROM res_partner rp, iec_mega_pgm imp where imp.partner_id = rp.id and imp.partner_id is not null and imp.id in (%s) 
            /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'res.partner':
            base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,
            rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id FROM res_partner rp where rp.id in (%s) 
            /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') in('partner.import.temp.wizard','partner.import.temp'):
            base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,
                        rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id
                 FROM res_partner rp, partner_import_temp pit
                 where pit.partner_id = rp.id and pit.id in (%s) 
                 /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'sadhguru.exclusive':
            base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,
                             rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
                                FROM res_partner rp, sadhguru_exclusive se where se.contact_id_fkey = rp.id and se.id in (%s) 
                                /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'programs.view.orm':
            base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,
                            rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
                                FROM res_partner rp, programs_view_orm pvo where pvo.contact_id_fkey = rp.id and pvo.id in (%s) 
                                /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'rudraksha.deeksha':
            base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,
                            rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
                                   FROM res_partner rp, rudraksha_deeksha rd where rd.contact_id_fkey = rp.id and rd.id in (%s) 
                                   /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'covid.support':
            base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,
                            rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
                                 FROM res_partner rp, covid_support cs where cs.contact_id_fkey = rp.id and cs.id in (%s) 
                                 /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()
        elif self._context.get('active_model') == 'completion.feedback':
            base_query = '''SELECT rp.id, rp.phone_is_valid, rp.dnd_phone,rp.deceased,rp.phone,
                            rp.influencer_type,rp.is_caca_donor,rp.last_txn_date,rp.starmark_category_id 
                                 FROM res_partner rp, completion_feedback cf where cf.contact_id_fkey = rp.id and cf.id in (%s) 
                                 /* and (rp.no_marketing is false or rp.no_marketing is null) */ and rp.center_id in (%s) %s''' % (base_ids, center_ids, suffix_clause)
            self.env.cr.execute(base_query, where_clause_params)
            partner_recs = self.env.cr.dictfetchall()

        phone_set = set()
        valid_partners = []
        #suri: changes
        # DND check on res_partner
        for x in partner_recs:
            if x['dnd_phone'] is not True and x['phone_is_valid'] is True and x['deceased'] is not True and x['starmark_category_id'] is None:
                if x['phone'] not in phone_set:
                    # if self.env.user.has_group('isha_crm.group_sangam'):
                    #     if x.is_caca_donor is not True or (x.is_caca_donor is True and str(x.last_txn_date) != '1900-01-01'):
                    #         valid_partners.append(x.id)
                    #         phone_set.add(x.phone)
                    if (not x['influencer_type']) and (x['is_caca_donor'] is not True or (x['is_caca_donor'] is True and str(x['last_txn_date']) != '1900-01-01')):
                        valid_partners.append(x['id'])
                        phone_set.add(x['phone'])

        return set(valid_partners)

    @api.model
    def default_get(self, fields):
        result = super(IshaCallCampaignContactAllocation, self).default_get(fields)

        campaign_id = request.session.pop('campaign_id', None)
        if campaign_id:
            result['campaign_id'] = campaign_id
        # result['selected_count'] = len(self.get_valid_partners_set())
        return result

    def action_create_split_campaign(self):
        table_name = self.env[self.env.context['active_model']]._table
        base_domain = self.env.context['active_domain']
        split_fields = []
        orm_fields = []
        for x in self.split_fields:
            # partner_import_temp does not have a auto join by default
            if x.related and table_name != 'partner_import_temp':
                split_fields.append(table_name+'__'+x.related+' as '+x.name)
            else:
                split_fields.append(x.name)
            orm_fields.append(x.name)
        from_clause, where_clause, where_clause_params = self.env[self.env.context['active_model']].get_query_params(
            base_domain)

        if self.center_reallocation:
            center_field_orm = center_reallocation_mapper[self.env.context['active_model']]['orm']
            center_field_table = center_reallocation_mapper[self.env.context['active_model']]['related']
            if center_field_orm not in orm_fields:
                orm_fields.append(center_field_orm)
                split_fields.append(table_name+'__'+center_field_table+' as '+center_field_orm if center_field_table else center_field_orm)

        where_str = where_clause and (" WHERE %s" % where_clause) or ''
        base_query = 'SELECT %s FROM %s %s group by %s'
        base_ids = base_query % (' , '.join(split_fields), from_clause, where_str,' , '.join([x.split(' as ')[0] for x in split_fields]))
        self.env.cr.execute(base_ids, where_clause_params)
        split_campaigns = []
        rows = self.env.cr.dictfetchall()
        if len(rows) > 100:
            raise UserError("No. Of. Split Campaigns is more than 100. Plz reduce the granularity.")
        for row in rows:
            domain = base_domain + [(k,'=',v) for k,v in row.items()]
            sample_rec = self.env[self._context.get('active_model')].search_read(domain=domain,fields=orm_fields,limit=1)[0]
            split_field_values = []
            for x in orm_fields:
                if type(sample_rec[x]) == tuple:
                    split_field_values.append(sample_rec[x][1])
                else:
                    split_field_values.append(str(sample_rec[x]))

            campaign_name = self.base_campaign.title + ' - ' + '/'.join(split_field_values)
            if self.center_reallocation and type(sample_rec[center_field_orm]) == tuple:
                center_id = int(sample_rec[center_field_orm][0])
            else:
                center_id = self.base_campaign.center_id.id

            new_campaign = self.base_campaign.copy(default={'title':campaign_name,'user_input_ids':[(6,0,[])],'center_id':center_id,
                                                            'parent_id':self.base_campaign.id})
            split_campaigns.append(new_campaign.id)
            new_allocation_obj = self.env['isha.call_campaign.contact.allocation'].create({
                'campaign_id':new_campaign.id,
                'caller_id':self.caller_id.id,
                'selection_mode':'selected_filter'
            })
            new_allocation_obj.with_context(active_domain=domain).action_contacts_to_leads()
        return {
            'name': 'Split Call Campaign',
            "type": "ir.actions.act_window",
            "res_model": "call_campaign.call_campaign",
            "views": [[False, "tree"],[False,"form"]],
            "domain": [('id','in',split_campaigns)]
        }

    def action_contacts_to_leads(self):
        null = None
        create_list = []
        start = datetime.datetime.now()
        valid_partner_set = self.get_valid_partners_set()
        if valid_partner_set:
            _logger.info('[CC Debug] user_input allocate begin | campaign = ' + str(self.campaign_id.title) + ' | len -> ' + str(len(valid_partner_set)))
            userinput_recs = '''select ccui.id, ccui.partner_id, rp.phone from call_campaign_user_input ccui, res_partner rp where ccui.partner_id = rp.id and call_campaign_id = %s and  partner_id in (%s)''' % (self.campaign_id.id, ','.join([str(x) for x in valid_partner_set]))
            self.env.cr.execute(userinput_recs)
            userinput_id = self.env.cr.dictfetchall()
            set_partner_ids_not_present = set([x for x in valid_partner_set]) - set([x['partner_id'] for x in userinput_id])

            if self.caller_id:
                for x in set_partner_ids_not_present:
                    create_list.append(str((self.campaign_id.id,x,self.caller_id.id,self.env.user.id,str(datetime.datetime.now()),self.env.user.id,str(datetime.datetime.now()))))
            else:
                for x in set_partner_ids_not_present:
                    create_list.append(str((self.campaign_id.id,x,self.env.user.id,str(datetime.datetime.now()),self.env.user.id,str(datetime.datetime.now()))))

            if self.caller_id:
                update_query_caller_id = '''update call_campaign_user_input set caller_id = %s where call_campaign_id = %s and  partner_id in (%s)''' % (self.caller_id.id, self.campaign_id.id, ','.join([str(x) for x in valid_partner_set]))
                self.env.cr.execute(update_query_caller_id)
                self.env.cr.commit()

            if len(create_list) > 0:
                if self.caller_id:
                    columns = ['call_campaign_id','partner_id','caller_id','create_uid','create_date','write_uid','write_date']
                else:
                    columns = ['call_campaign_id', 'partner_id', 'create_uid', 'create_date', 'write_uid', 'write_date']
                query = "INSERT INTO call_campaign_user_input ({}) VALUES %s".format(','.join(columns))
                query = query % (','.join(create_list))
                self.env.cr.execute(query)
                self.env.cr.commit()
                query1 = '''UPDATE call_campaign_user_input ccui set campaign_status = cccc.state, is_sensitive = cccc.is_sensitive , center_id = cccc.center_id, state = 'new' from call_campaign_call_campaign cccc where ccui.call_campaign_id = cccc.id and ccui.call_campaign_id = %s and ccui.partner_id in (%s)''' % (self.campaign_id.id, ','.join([str(x) for x in set_partner_ids_not_present]))
                self.env.cr.execute(query1)
                self.env.cr.commit()
                if self.caller_id:
                    query2 = '''UPDATE call_campaign_user_input ccui set caller_partner_id = %s where ccui.partner_id in (%s) and ccui.call_campaign_id = %s''' % (self.caller_id.partner_id.id, ','.join([str(x) for x in set_partner_ids_not_present]), self.campaign_id.id)
                    self.env.cr.execute(query2)
                    self.env.cr.commit()
                query3 = '''UPDATE call_campaign_user_input ccui set  partner_ieo_status= rp.ieo_progress,partner_ieo_lang_pref = rp.ieo_lang_pref, partner_center_id = rp.center_id,partner_region_id = ir.id, partner_country = rp.country_id from res_partner rp, isha_region ir where ccui.partner_id = rp.id and rp.region_name = ir.name and ccui.call_campaign_id = %s and ccui.partner_id in (%s)''' % (self.campaign_id.id, ','.join([str(x) for x in set_partner_ids_not_present]))
                self.env.cr.execute(query3)
                self.env.cr.commit()

            _logger.info('[CC Debug] user_input allocate end | campaign = ' + str(self.campaign_id.title) + ' | len -> ' + str(len(valid_partner_set)))
            _logger.info('[CC Debug] user_input allocate end | campaign = ' + str(self.campaign_id.title) + ' | time_taken -> ' + str(datetime.datetime.now()-start))
            return {
                "type": "ir.actions.act_window",
                "res_model": "call_campaign.call_campaign",
                "views": [[False, "form"]],
                "res_id": self.campaign_id.id
            }
        else:
            return self.env["sh.message.wizard"].get_popup('No valid records found for the selection. Plz check the validity of the selected records')

class IshaCallCampaignCallerAllocation (models.TransientModel):
    """ wizard to convert contacts into Campaign Leads """

    _name = 'isha.call_campaign.caller.allocation'
    _description = 'Assign callers to campaign'

    campaign_id = fields.Many2one('call_campaign.call_campaign', string='Campaign', required=True)
    selected_count = fields.Integer(string='Selected count',readonly=True)

    def get_valid_callers_set(self):
        partner_recs = []
        if self._context.get('active_model') == 'res.partner':
            partner_recs = self.env['res.partner'].browse(self._context.get('active_ids'))
        #deceased Checking
        valid_partners = [x.id for x in partner_recs if x.deceased is False]
        return set(valid_partners)

    @api.model
    def default_get(self, fields):
        result = super(IshaCallCampaignCallerAllocation, self).default_get(fields)
        campaign_id = request.session.pop('campaign_id', None)
        if campaign_id:
            result['campaign_id'] = campaign_id

        result['selected_count'] = len(self.get_valid_callers_set())
        return result

    def action_callers_to_campaign(self):
        self.ensure_one()

        CallerEnv = self.env['call_campaign.caller']
        create_list = []
        for partner_id in self.get_valid_callers_set():
            # if not already exists, add it
            caller_id = CallerEnv.search([('call_campaign_id','=',self.campaign_id.id),('partner_id','=',partner_id)],
                                          limit=1)
            if not caller_id:
                create_list.append({'call_campaign_id':self.campaign_id.id,'partner_id':partner_id})
        CallerEnv.create(create_list)

        return {
            "type": "ir.actions.act_window",
            "res_model": "call_campaign.call_campaign",
            "views": [[False, "form"]],
            "res_id": self.campaign_id.id
        }
