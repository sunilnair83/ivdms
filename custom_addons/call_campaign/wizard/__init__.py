# -*- encoding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from . import call_campaign_invite
from . import call_campaign_allocate
