# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import json
import logging
import traceback
from datetime import datetime, timedelta

import werkzeug

from odoo import http
from odoo.http import request, Response
from .. import isha_crm_importer

_logger = logging.getLogger(__name__)


class CallCampaign(http.Controller):

    # ------------------------------------------------------------
    # ACCESS
    # ------------------------------------------------------------

    def _get_access_data(self, call_campaign_token, caller_token, ensure_token=True):
        """ Get back data related to call_campaign and caller, given the ID and access
        token provided by the route.

         : param ensure_token: whether user input existence should be enforced or not(see ``_check_validity``)
        """
        has_call_campaign_access, can_caller = False, False

        call_campaign_sudo = request.env['call_campaign.call_campaign'].with_context(active_test=False).sudo().search(
            [('access_token', '=', call_campaign_token)])
        if not call_campaign_sudo.exists():
            return werkzeug.utils.redirect("/")

        if call_campaign_sudo.state == 'closed' or call_campaign_sudo.state == 'draft' or not call_campaign_sudo.active:
            return request.render("call_campaign.call_campaign_expired", {'call_campaign': call_campaign_sudo})

        # If token is not provided, check if user is logged in
        if not caller_token:
            if ensure_token or request.env.user._is_public():
                redirect_url = '/web/login?redirect=%s' % ('/call_campaign/start/%s' % (call_campaign_sudo.access_token))
                return request.render("call_campaign.auth_required",
                                  {'call_campaign': call_campaign_sudo, 'redirect_url': redirect_url})

            # check if user is assigned to the campaign, else throw
            caller_sudo = request.env.user.partner_id.get_caller_id(call_campaign_sudo.id)
        else:
            caller_sudo = request.env['call_campaign.caller'].sudo().search([
                ('call_campaign_id', '=', call_campaign_sudo.id),
                ('token', '=', caller_token)
            ], limit=1)

        if not caller_sudo or not caller_sudo.active:
            return request.render("call_campaign.403", {'call_campaign': call_campaign_sudo})

        # Get all the skipped contacts for this caller
        skipped_contacts_sudo = request.env['call_campaign.user_input'].sudo().search([
            ('call_campaign_id', '=', call_campaign_sudo.id), ('caller_id', '=', caller_sudo.id),
            ('state', '=', 'skip')], order='write_date desc')

        return {
            'call_campaign_sudo': call_campaign_sudo,
            'caller_sudo': caller_sudo,
            'skipped_contacts_sudo':skipped_contacts_sudo
        }


    # ------------------------------------------------------------
    # TAKING ROUTES
    # ------------------------------------------------------------

    @http.route('/call_campaign/start/<string:call_campaign_token>', type='http', auth='public', website=True)
    def call_campaign_start(self, call_campaign_token, caller_token=None, email=False, **post):
        """ Start a call_campaign by providing
         * a token linked to a call_campaign;
         * a token linked to an caller or generate a new token if access is allowed;
        """
        access_data = self._get_access_data(call_campaign_token, caller_token, ensure_token=False)
        if isinstance(access_data, Response):
            return access_data

        call_campaign_sudo, caller_sudo = access_data['call_campaign_sudo'], access_data['caller_sudo']

        return request.redirect('/call_campaign/fill/%s/%s' % (call_campaign_sudo.access_token, caller_sudo.token))

    @http.route('/call_campaign/fill/<string:call_campaign_token>/<string:caller_token>', type='http', auth='public', website=True)
    def call_campaign_display_page(self, call_campaign_token, caller_token, skipped=False, **post):

        access_data = self._get_access_data(call_campaign_token, caller_token, ensure_token=True)
        if isinstance(access_data, Response):
            return access_data

        call_campaign_sudo, caller_sudo, skipped_contacts_sudo = access_data['call_campaign_sudo'], access_data['caller_sudo'], access_data['skipped_contacts_sudo']

        # get next contact and start calling
        answer_sudo = self._get_next_contact(call_campaign_sudo, caller_sudo, skipped)

        if answer_sudo:
            sgx_reg_status = self.get_sgx_details(answer_sudo) if call_campaign_sudo.show_sgex_details else ''

            data = {
                'call_campaign': call_campaign_sudo,
                'caller': caller_sudo,
                'answer': answer_sudo,
                'skipped': skipped_contacts_sudo,
                'sgx_reg_status': sgx_reg_status
            }
            return request.render('call_campaign.call_campaign', data)
        else:
            return request.render("call_campaign.call_campaign_completed", {'call_campaign': call_campaign_sudo})

    @http.route('/call_campaign/get_skipped/<string:call_campaign_token>/<string:caller_token>/<string:call_campaign_user_input_id>', type='http', auth='public', website=True)
    def call_campaign_get_skipped_page(self,call_campaign_token, caller_token, call_campaign_user_input_id, **post):

        access_data = self._get_access_data(call_campaign_token, caller_token, ensure_token=True)

        call_campaign_sudo, caller_sudo, skipped_contacts_sudo = access_data['call_campaign_sudo'], access_data[
            'caller_sudo'], access_data['skipped_contacts_sudo']

        skipped_contact = request.env['call_campaign.user_input'].sudo().browse(int(call_campaign_user_input_id))

        sgx_reg_status = self.get_sgx_details(skipped_contact) if skipped_contact.call_campaign_id.show_sgex_details else ''
        data = {
            'call_campaign': skipped_contact.call_campaign_id,
            'caller': skipped_contact.caller_id,
            'answer': skipped_contact,
            'skipped': skipped_contacts_sudo,
            'sgx_reg_status':sgx_reg_status
        }
        return request.render('call_campaign.call_campaign', data)

    @http.route('/call_campaign/submit/<string:call_campaign_token>/<string:caller_token>', type='http', methods=['POST'], auth='public', website=True)
    def call_campaign_submit(self, call_campaign_token, caller_token, **post):
        """ Submit a page from the call_campaign.
        This will take into account the validation errors and store the answers to the questions.
        call_campaign state will be forced to 'done'

        TDE NOTE: original comment: # AJAX submission of a page -> AJAX / http ?? """
        access_data = self._get_access_data(call_campaign_token, caller_token, ensure_token=True)
        if isinstance(access_data, Response):
            return json.dumps({})

        call_campaign_sudo, caller_sudo = access_data['call_campaign_sudo'], access_data['caller_sudo']
        answer_data = post['answer_id']
        if not answer_data or not answer_data.isdigit():
            return json.dumps({})

        answer_sudo = request.env['call_campaign.user_input'].sudo().browse(int(answer_data))

        ret = {}
        # if just send sms and return
        sendsms = post['button_submit'] == 'sendsms'
        if sendsms:
            answer_sudo.send_sms()
            return json.dumps({})

        # validations
        errors = {}

        # if skipped, mark it and get next contact
        skip = post['button_submit'] == 'skip'
        getskipped = post['button_submit'] == 'getskippedcontact'
        finish = post['button_submit'] == 'finish'

        call_status_tag = "%s_%s_status" % (call_campaign_sudo.id, answer_sudo.id)
        if finish:
            if call_status_tag not in post or post[call_status_tag] == '':
                errors.update({call_status_tag:'Status mandatory'})
                ret['errors'] = errors
                return json.dumps(ret)

        if call_status_tag in post:
            vals = {'call_status': post[call_status_tag]}

        call_feedback_tag = "%s_%s_feedback" % (call_campaign_sudo.id, answer_sudo.id)
        if call_feedback_tag in post:
            vals.update({'call_feedback': post[call_feedback_tag]})

        remark_tag = "%s_%s_contact_remark" % (call_campaign_sudo.id, answer_sudo.id)
        if remark_tag in post:
            vals.update({'contact_remark': post[remark_tag]})

        if finish or skip:
            questions = call_campaign_sudo.question_ids
            questions = questions & answer_sudo.question_ids

            if finish:
            # caller validation
                for question in questions:
                    answer_tag = "%s_%s_%s" % (call_campaign_sudo.id, answer_sudo.id, question.id)
                    errors.update(question.validate_question(post, answer_tag))

                if len(errors):
                    # Return errors messages to webpage
                    ret['errors'] = errors
                    return json.dumps(ret)

            for question in questions:
                answer_tag = "%s_%s_%s" % (call_campaign_sudo.id, answer_sudo.id, question.id)
                request.env['call_campaign.user_input_line'].sudo().save_lines(answer_sudo.id, question, post,
                                                                               answer_tag)

        if finish:
            vals.update({'done_time': datetime.now()})
            vals.update({'state': 'done'})
        if skip:
            vals.update({'done_time': datetime.now()})
            vals.update({'state': 'skip'})

        answer_sudo.write(vals)
        # print(post)
        self.update_res_partner_phone(request.env['res.partner'].sudo(),answer_sudo,post['current_phone_base'])
        ret['redirect'] = '/call_campaign/fill/%s/%s' % (call_campaign_sudo.access_token, caller_sudo.token)
        if getskipped:
            ret['redirect'] += '?skipped=true'
        return json.dumps(ret)

    def update_res_partner_phone(self, partner_model, answer_sudo,phone):
        partner_sudo = partner_model.sudo().browse(answer_sudo.partner_id.id)
        partner_sudo_dict = partner_sudo.read()[0]
        update_flag = False
        whatsapp_invalidation = False
        updated_phone_dict = dict()
        if answer_sudo.call_status == 'call_completed':
            update_flag = True
            partner_sudo_dict.update({phone+'_is_verified': True, phone+'_is_valid': True})
        elif answer_sudo.call_status == 'dnd':
            update_flag = True
            partner_sudo_dict.update({'dnd_phone': True})
        elif answer_sudo.call_status == 'wrong_num':
            update_flag = True
            partner_sudo_dict.update({phone+'_is_valid': False, phone+'_is_verified': False})
            if partner_sudo_dict[phone] == partner_sudo_dict['whatsapp_number']:
                whatsapp_invalidation = True
                updated_phone_dict.update({'whatsapp_number': None,'whatsapp_country_code': None})
        try:
            if update_flag:
                goldenContactAdv = isha_crm_importer.GoldenContactAdv.GoldenContactAdv()
                updated_phone_dict.update(goldenContactAdv.getPhonesDict(partner_sudo_dict,{}))
                if whatsapp_invalidation and updated_phone_dict['phone_is_valid']:
                    updated_phone_dict.update({'whatsapp_number': updated_phone_dict['phone'],
                                               'whatsapp_country_code': updated_phone_dict['phone_country_code']})
                return partner_sudo.sudo().write(updated_phone_dict)
        except Exception as ex:
            tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
            # print(tb_ex)
            _logger.info(tb_ex)


    def _get_next_contact(self,call_campaign_sudo, caller_sudo, skipped):
        answer_sudo = request.env['call_campaign.user_input'].sudo()
        if call_campaign_sudo.suppression_domains:
            if not call_campaign_sudo.last_suppression_sync_time or \
                (datetime.now() - call_campaign_sudo.last_suppression_sync_time) > \
                    timedelta(hours=call_campaign_sudo.suppression_sync_interval):

                suppression_query_set = []
                params_set = []
                for filter_rec in call_campaign_sudo.suppression_domains:
                    partner_mapping_field = None
                    if filter_rec.model_id in ['program.lead','program.attendance','sadhguru.exclusive','ieo.record',
                                               'programs.view.orm','rudraksha.deeksha','covid.support','completion.feedback']:
                        partner_mapping_field = 'contact_id_fkey'
                    elif filter_rec.model_id in ['call_campaign.user_input','call_campaign.user_input_line','iec.mega.pgm',
                                                 'event.registration','res.users','partner.import.temp.wizard',
                                                 'partner.import.temp']:
                        partner_mapping_field = 'partner_id'
                    elif filter_rec.model_id == 'res.partner':
                        partner_mapping_field = 'id'

                    if partner_mapping_field:
                        table_name = request.env[filter_rec.model_id]._table
                        from_clause, where_clause, where_clause_params = request.env[filter_rec.model_id].get_query_params(eval(filter_rec.domain))
                        where_str = where_clause and (" WHERE %s" % where_clause) or ''
                        base_query = 'SELECT "%s".%s FROM %s %s'
                        final_query = base_query % (table_name, partner_mapping_field, from_clause, where_str)
                        suppression_query_set.append(final_query)
                        params_set += where_clause_params

                if suppression_query_set:
                    suppression_query = """
                        update call_campaign_user_input set state = 'suppressed' where 
                        call_campaign_id = %s and state != 'suppressed' and
                        partner_id in (
                            select partner_id from call_campaign_user_input where call_campaign_id = %s and state != 'suppressed'
                            intersect
                            (
                                %s
                            )
                        )
                    """ % (call_campaign_sudo.id, call_campaign_sudo.id, ' UNION '.join(suppression_query_set))
                    request.env.cr.execute(suppression_query,params_set)
                    request.env.cr.commit()
                    call_campaign_sudo.last_suppression_sync_time = datetime.now()
        next_contact = None
        if skipped:
            next_contact = answer_sudo.search([
                ('call_campaign_id', '=', call_campaign_sudo.id), ('caller_id', '=', caller_sudo.id),
                ('state', '=', 'skip')], order='write_date', limit=1)
        if not next_contact:
            # check if caller left some contact previously or contact has been pre-assigned
            next_contact = answer_sudo.search(([
                ('call_campaign_id', '=', call_campaign_sudo.id), ('caller_id', '=', caller_sudo.id),
                ('state', '=', 'new')]), limit=1)
            if next_contact:
                # this contact might have been pre-assigned, assign questions now
                vals = {'question_ids': [(6, 0, call_campaign_sudo._prepare_answer_questions().ids)]}
                next_contact.write(vals)
        if not next_contact and call_campaign_sudo.get_unassigned_contacts:
            # get next unassigned contact and update questions and caller
            vals = {'question_ids': [(6, 0, call_campaign_sudo._prepare_answer_questions().ids)],
                    'caller_id': caller_sudo.id}

            # loop over till you find one valid
            while True:
                next_contact = answer_sudo.search(([
                    ('call_campaign_id', '=', call_campaign_sudo.id), ('caller_id', '=', False),('state','!=','suppressed')
                ]), limit=1)
                if not next_contact:
                    break;
                    # ensure the contact has phone, dnd, isvalid set properly
                if not next_contact.partner_phone \
                        or next_contact.partner_dnd_phone \
                        or not next_contact.partner_phone_valid:
                    next_contact.unlink()
                else:
                    break
            if next_contact:
                next_contact.write(vals)
        if not next_contact and call_campaign_sudo.get_unassigned_contacts:
            # get contact which might have been assigned to others but not processed yet
            next_contact = answer_sudo.search(([
                ('call_campaign_id', '=', call_campaign_sudo.id), ('state', '=', 'new')]), order='write_date', limit=1)
            if next_contact:
                next_contact.write(vals)
        if not next_contact:
            # get skipped contact
            next_contact = answer_sudo.search(([
                ('call_campaign_id', '=', call_campaign_sudo.id), ('caller_id', '=', caller_sudo.id),
                ('state', '=', 'skip')]), order='write_date', limit=1)

        return next_contact

    def get_ieo_details(self, answer_sudo):
        if len(answer_sudo.partner_id.contact_ieo_id_fkey) > 0:
            partner_ieo_emailf = answer_sudo.partner_id.contact_ieo_id_fkey[0].email
        else:
            partner_ieo_emailf = None
        return partner_ieo_emailf

    def get_sgx_details(self, answer_sudo):
        if len(answer_sudo.partner_id.contact_sgex_id_fkey) > 0:
            current_txn = answer_sudo.partner_id.contact_sgex_id_fkey.filtered(lambda x: x.current is True)
            if len(current_txn) > 0:
                return current_txn[0].reg_status
        return None

    @http.route('/call_campaign/<string:call_campaign_token>/<string:caller_token>/getalternate', type='http', auth="public", methods=['GET','POST'], website=True, sitemap=False)
    def get_alternate_phone(self,call_campaign_token,caller_token, partner_id='', try_alt_call_status='',current_phone='', **post):
        next_phone_dict = {
            'phone':'phone2',
            'phone2':'phone3',
            'phone3':'phone4',
            'phone4':False
        }
        goldenContactAdv = isha_crm_importer.GoldenContactAdv.GoldenContactAdv()
        partner_sudo = request.env['res.partner'].sudo().browse(int(partner_id))
        partner_sudo_dict = partner_sudo.read()[0]
        if try_alt_call_status == 'wrong_num':
            wrong_number = partner_sudo_dict[current_phone]
            partner_sudo_dict[current_phone+'_is_valid'] = False
            partner_sudo_dict[current_phone + '_is_verified'] = False
            updated_phone_dict = goldenContactAdv.getPhonesDict(partner_sudo_dict,{})
            if partner_sudo_dict['whatsapp_number'] == wrong_number:
                updated_phone_dict.update({'whatsapp_number': updated_phone_dict['phone'],
                                    'whatsapp_country_code': updated_phone_dict['phone_country_code']})
            partner_sudo.sudo().write(updated_phone_dict)
            partner_sudo = updated_phone_dict
        else:
            current_phone = next_phone_dict[current_phone]

        next_phone = next_phone_dict[current_phone] and partner_sudo[next_phone_dict[current_phone]+'_is_valid']

        phone_value = False
        phone_cc_value = False
        if current_phone and partner_sudo[current_phone+'_is_valid']:
            phone_value = partner_sudo[current_phone]
            phone_cc_value = partner_sudo[current_phone+'_country_code']
        else:
            current_phone = False

        # print({'phone':partner_sudo['phone2'],'current_phone':'phone2'})
        return json.dumps({'phone':phone_value,'phone_cc_value':phone_cc_value,'current_phone':current_phone,'next_phone':next_phone})

    @http.route('/call_campaign/<string:call_campaign_token>/<string:caller_token>/getprogramdetails', type='http',
                auth="public", methods=['GET', 'POST'], website=True, sitemap=False)
    def get_program_tab_content(self, call_campaign_token, caller_token, partner_id='', **post):
        call_campaign_sudo = request.env['call_campaign.call_campaign'].with_context(active_test=False).sudo().search(
            [('access_token', '=', call_campaign_token)])
        partner_sudo = request.env['res.partner'].sudo().browse(int(partner_id))
        result = {'partner_id': partner_sudo,'call_campaign':call_campaign_sudo}
        return request.render('call_campaign.programs_tab_content', result)

    @http.route('/call_campaign/get_tags', type='http', auth="public", methods=['GET'], website=True, sitemap=False)
    def tag_read(self, query='', limit=25, **post):
        data = request.env['isha.skill.tag'].search_read(
            domain=[('name', '=ilike', (query or '') + "%")],
            fields=['id', 'name'],
            limit=int(limit),
        )
        return json.dumps(data)
