import odoo
from odoo.http import request, Response
import logging, datetime

_logger = logging.getLogger(__name__)

# this api was written as part of POC and is not used currently in our code.
class CampaignAPI(odoo.http.Controller):

    def _get_access_data(self, call_campaign_token, caller_token):
        call_campaign_sudo = request.env["call_campaign.call_campaign"].with_context(active_test=False).sudo().search(
            [("access_token", "=", call_campaign_token)])
        if not call_campaign_sudo.exists():
            return {"status": "error", "reason": "campaign does not exist"}

        if call_campaign_sudo.state == "closed" or call_campaign_sudo.state == "draft" or not call_campaign_sudo.active:
            return {"status": "error", "reason": "campaign expired"}

        caller_sudo = request.env["call_campaign.caller"].sudo().search([
            ("call_campaign_id", "=", call_campaign_sudo.id),
            ("token", "=", caller_token)
        ], limit=1)

        if not caller_sudo or not caller_sudo.active:
            return {"status": "error", "reason": "caller does not exist"}

        # Get all the skipped contacts for this caller
        skipped_contacts_sudo = request.env["call_campaign.user_input"].sudo().search([
            ("call_campaign_id", "=", call_campaign_sudo.id), ("caller_id", "=", caller_sudo.id),
            ("state", "=", "skip")])

        return {
            "status": "success",
            "call_campaign_sudo": call_campaign_sudo,
            "caller_sudo": caller_sudo,
            "skipped_contacts_sudo": skipped_contacts_sudo
        }

    @odoo.http.route([
        "/api/call_campaign/fetch"
    ], auth="none", type="json", methods=["GET"], csrf=False)
    def fetch_details(self, **kwargs):
        start = datetime.datetime.now()
        cc_token = request.jsonrequest.get("campaign_token")
        caller_token = request.jsonrequest.get("caller_token")
        auth_data = self._get_access_data(cc_token, caller_token)

        if auth_data["status"] == "error":
            return auth_data

        campaign = auth_data["call_campaign_sudo"]
        caller = auth_data["caller_sudo"]

        answer_sudo = self._get_next_contact(campaign, caller, False)
        if answer_sudo:
            auth_data['answer_sudo'] = answer_sudo
            auth_data["partner_ieo_email"] = self.get_ieo_details(answer_sudo)
            auth_data["partner_ieco_email"] = self.get_ieco_details(answer_sudo)
            auth_data["sgx_reg_status"] = self.get_sgx_details(answer_sudo) if campaign.show_sgex_details else False
            response_date = self.construct_fetch_json_body(auth_data)
            _logger.info("cc_api time taken "+str(datetime.datetime.now()-start))
            return {
                "status": "success",
                "data": response_date
            }
        else:
            return {
                "status": "error",
                "reason": "campaign already completed"
            }

    def _get_next_contact(self, call_campaign_sudo, caller_sudo, skipped):
        answer_sudo = request.env["call_campaign.user_input"].sudo()
        next_contact = None
        if skipped:
            next_contact = answer_sudo.search([
                ("call_campaign_id", "=", call_campaign_sudo.id), ("caller_id", "=", caller_sudo.id),
                ("state", "=", "skip")], order="write_date", limit=1)
        if not next_contact:
            # check if caller left some contact previously or contact has been pre-assigned
            next_contact = answer_sudo.search(([
                ("call_campaign_id", "=", call_campaign_sudo.id), ("caller_id", "=", caller_sudo.id),
                ("state", "=", "new")]), limit=1)
            if next_contact:
                # this contact might have been pre-assigned, assign questions now
                vals = {"question_ids": [(6, 0, call_campaign_sudo._prepare_answer_questions().ids)]}
                next_contact.write(vals)
        if not next_contact and call_campaign_sudo.get_unassigned_contacts:
            # get next unassigned contact and update questions and caller
            vals = {"question_ids": [(6, 0, call_campaign_sudo._prepare_answer_questions().ids)],
                    "caller_id": caller_sudo.id}

            # loop over till you find one valid
            while True:
                next_contact = answer_sudo.search(([
                    ("call_campaign_id", "=", call_campaign_sudo.id), ("caller_id", "=", False)
                ]), limit=1)
                if not next_contact:
                    break
                    # ensure the contact has phone, dnd, isvalid set properly
                if not next_contact.partner_phone \
                        or next_contact.partner_dnd_phone \
                        or not next_contact.partner_phone_valid:
                    next_contact.unlink()
                else:
                    break
            if next_contact:
                next_contact.write(vals)
        if not next_contact and call_campaign_sudo.get_unassigned_contacts:
            # get contact which might have been assigned to others but not processed yet
            next_contact = answer_sudo.search(([
                ("call_campaign_id", "=", call_campaign_sudo.id), ("state", "=", "new")]), order="write_date", limit=1)
            if next_contact:
                next_contact.write(vals)
        if not next_contact:
            # get skipped contact
            next_contact = answer_sudo.search(([
                ("call_campaign_id", "=", call_campaign_sudo.id), ("caller_id", "=", caller_sudo.id),
                ("state", "=", "skip")]), order="write_date", limit=1)

        return next_contact

    def get_ieo_details(self, answer_sudo):
        if len(answer_sudo.partner_id.contact_ieo_id_fkey) > 0:
            partner_ieo_emailf = answer_sudo.partner_id.contact_ieo_id_fkey[0].email
        else:
            partner_ieo_emailf = False
        return partner_ieo_emailf

    def get_ieco_details(self, answer_sudo):
        if len(answer_sudo.partner_id.contact_ieco_id_fkey) > 0:
            partner_ieco_emailf = answer_sudo.partner_id.contact_ieco_id_fkey[0].email
        else:
            partner_ieco_emailf = False
        return partner_ieco_emailf

    def get_sgx_details(self, answer_sudo):
        if len(answer_sudo.partner_id.contact_sgex_id_fkey) > 0:
            current_txn = answer_sudo.partner_id.contact_sgex_id_fkey.filtered(lambda x: x.current is True)
            if len(current_txn) > 0:
                return current_txn[0].reg_status
        return None

    def construct_fetch_json_body(self, data):
        campaign = data["call_campaign_sudo"]
        answer = data["answer_sudo"]
        return {
            "campaign": {
                "name": campaign.title,
                "call_script": campaign.description,
            },
            "callee": {
                "name": answer.partner_id.name,
                "gender": answer.partner_id.gender if campaign.show_gender else False,
                "dob": answer.partner_id.dob if campaign.show_dob else False,
                "occupation": answer.partner_occupation if campaign.show_occupation else False,
                "email": answer.partner_id.email,
                "ieo_email": data["partner_ieo_email"],
                "center": answer.partner_id.center_id.name,
                "street1": answer.partner_id.street if campaign.show_address else False,
                "street2": answer.partner_id.street2 if campaign.show_address else False,
                "city": answer.partner_id.city if campaign.show_address or campaign.show_city else False,
                "state": answer.partner_id.state if campaign.show_address or campaign.show_state else False,
                "zip": answer.partner_id.zip if campaign.show_address else False,
                "country": answer.partner_id.country if campaign.show_address or campaign.show_country else False,
                "program_tags": answer.partner_id.pgm_tag_ids.mapped("name"),
                "sgx_status": data["sgx_reg_status"] if campaign.show_sgex_details else False,
                "prev_remark": answer.prev_remark if campaign.show_prev_remark else False,
                "past_campaigns": answer.track_campaign_details.split("^") if answer.track_count > 0 else [],
                "phones": {
                    "phone1": {
                        "number": answer.partner_id.phone,
                        "cc": answer.partner_id.phone_country_code,
                        "valid": answer.partner_id.phone_is_valid,
                        "verified": answer.partner_id.phone_is_verified,
                    },
                    "phone2": {
                        "number": answer.partner_id.phone2,
                        "cc": answer.partner_id.phone2_country_code,
                        "valid": answer.partner_id.phone2_is_valid,
                        "verified": answer.partner_id.phone2_is_verified,
                    },
                    "phone3": {
                        "number": answer.partner_id.phone3,
                        "cc": answer.partner_id.phone3_country_code,
                        "valid": answer.partner_id.phone3_is_valid,
                        "verified": answer.partner_id.phone3_is_verified,
                    },
                    "phone4": {
                        "number": answer.partner_id.phone4,
                        "cc": answer.partner_id.phone4_country_code,
                        "valid": answer.partner_id.phone4_is_valid,
                        "verified": answer.partner_id.phone4_is_verified,
                    },
                },
                "ieo_details": [
                    {
                        "class_id": rec.progress,
                        "reg_date": rec.start_date,
                        "expiry_date": rec.end_date,
                        "last_login_date": rec.last_login_date,
                        "lang_pref": rec.lang_pref,
                        "account_type": rec.cust_type
                    } for rec in answer.partner_id.contact_ieo_id_fkey
                ],
                "ieco_details": [
                    {
                        "session_id": rec.session_id,
                        "schedule_id": rec.schedule_id,
                        "check_in_attended": rec.check_in_attended,
                        "status": rec.status
                    } for rec in answer.partner_id.contact_ieco_id_fkey
                ],
                "program_details": [
                    {
                        "program": rec.pgm_type_master_id.category,
                        "start_date": rec.start_date,
                    } for rec in answer.partner_id.contact_program_attendance_id_fkey
                ],
                "mv_details": {
                    "availability": answer.partner_id.vol_duration,
                    "volunteering_options": answer.partner_id.rm_vol_ids.mapped("name"),
                    "skills": answer.partner_id.skill_tag_ids.mapped("name"),
                    "qualification": answer.partner_id.qualification_ids.mapped("name"),
                    "occupational_status": answer.partner_id.prof_status,
                    "work_experience": answer.partner_id.work_experience,
                    "organization_name": answer.partner_id.companies
                } if campaign.show_mv_details else {}
            }

        }
