# -*- coding: utf-8 -*-
# © 2014-2015 Grupo ESOC <www.grupoesoc.es>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Message Forward",
    "summary": "Add option to forward messages",
    "version": "13.0.0.0.0",
    "category": "Mail Forward",
    "website": "https://isha.sadhguru.org/",
    "author": "Janaki",
    "license": "AGPL-3",
    "application": False,
    "installable": True,
    "depends": [
        "mail",
        "web",
        "portal",
        "website_mail"
    ],
    "data": [
        "views/assets.xml",

    ],
    "qweb": [
        "static/src/xml/mail_forward.xml",
    ],
}
