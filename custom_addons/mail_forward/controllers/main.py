# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import http
from odoo.http import request


class MessageThread(http.Controller):

    @http.route(['/message_read'], type='json', auth="public", website=True)
    def mail_message_read(self, **post):
        # TDE FIXME: check this method with new followers
        mail_to_text = post.get("mail_to")[0].split(',')
        frwdmsg = post.get("frwdmsg")
        rec = request.env["mail.message"].sudo().search([("id", "=", int(post.get("id")))])

        partner_ids =[]
        for i in mail_to_text:
            partner = request.env["res.partner"].sudo().search([("email","=",i)])
            if partner:
                partner_ids.append(partner.id)

        if len(partner_ids)>0:
            header ="----------" + ("Forwarded message") + "----------"+("<p> From: ") \
                  + (str(rec.email_from) if rec.email_from else "")+("</p><p> Date: ") \
                  + (str(rec.date) if rec.date else "")+"</p>" \

            header = header+ "<p> Subject: "+rec.subject if rec.subject else '' +"</p>"

            composer_values = {
                'body': "<p>"+str(frwdmsg)+"</p><br/><p><i>" + header+ "</i></p><br/>"+rec.body if rec.body else '',
                'subject': "Fwd: "+rec.subject,
                'model': rec.model,
                'res_id':rec.res_id,
                'partner_ids':partner_ids,
                'composition_mode': 'comment'
            }

            try:
                composer = request.env['mail.compose.message'].sudo().with_context(forward_mail=True).create(composer_values)
                composer.send_mail()
                return {"message": "Mail sent", "status": "success",'title':"Success"}
            except Exception as e:
                return {"message":"Mail not sent","status":"error",'title':"Error"}
        else:
            return {"message": "No contact exists with that email address.", "status": "error",'title':"Error"}

