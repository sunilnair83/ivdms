odoo.define('website_chatter.thread', function(require) {
    'use strict';

    var portalChatter = require('portal.chatter');
    var core = require('web.core');
    var QWeb = core.qweb;
    var core = require('web.core');
    var QWeb = core.qweb;
    var session = require('web.session');
    var _t = core._t;
    var session = require('web.session');
    /**
     * Extends Frontend Chatter to handle rating
     */
    portalChatter.PortalChatter.include({
        xmlDependencies: (portalChatter.PortalChatter.prototype.xmlDependencies || [])
            .concat(['/mail_forward/static/src/xml/portal_chatter.xml']),
        events: {
            'click .o_forward': 'forward',
        },

        forward: function() {
            var message_obj = $('.o_portal_chatter_message').attr("id");
            var msg_id = (message_obj).split("-")
            var enter_input_email = ""
            var frwdmsg=""
            var rpc = this._rpc;
            swal({
                title: "Email Forwader",
                text: " ",
                confirmButtonText: "Forward",
                confirmButtonColor: "#17a2b8",
                cancelButtonColor: "#17a2b8",
                reverseButtons: true,
                showCancelButton: true,
                html: ' ' +
                    '<label style="float: left;margin-left: 6.5%;">Forward to</label><p>'+
                    '<textarea class="sweet__input" name="textarea" style="width:400px;height:90px;font-size: 17px;font-style: italic;"></textarea>' +
                    '<div class="hide sweet__error" style="display:none;"><p>please enter valid email </p></div>' +
                    '<div class="sweet_note" style="font-size: 13px;font-style: italic;">please enter the email id like this eg:admin@example.com,admin2@example.com</div></p>' +
                     '<label style="float: left;margin-left: 6.5%;">Additional Text</label><p>'+
                    '<textarea class="frwdmsg" name="fwdtextarea" style="width:400px;height:150px;font-size: 17px;font-style: italic;"></textarea>'+
                    '<div class="sweet_forwsrd_note" style="font-size: 12px;font-style: italic;">This text will be added before the forward header in the mailer.</></p>' +
                    '',
                preConfirm: function() {
                    return new Promise(function(resolve) {
                        var email_text = $(".sweet__input").val();
                        frwdmsg = $(".frwdmsg").val();
                        var email_arr = []
                        if (email_text.includes(",") || email_text.indexOf(",") > -1) {
                            email_arr = email_text.split(",")
                        } else {
                            email_arr.push(email_text)
                        }
                        var email_valid = true;
                        for (var i = 0; i < email_arr.length; i++) {
                            var text = email_arr[i];
                            var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
                            if (!text.match(mailformat)) {
                                email_valid = false;
                                $(".sweet__error").css({
                                    'display': 'block',
                                    'color': 'red'
                                });
                                break;
                            }
                        }
                        enter_input_email = email_text;
                        if (!email_valid) {
                            swal.enableButtons();
                        } else {
                            resolve();
                        }

                    });
                }
            }).then((result) => {
                if (result.value) {
                    this._rpc({
                        route: '/message_read',
                        params: {
                            id: msg_id[1],
                            mail_to: [enter_input_email],
                            frwdmsg:frwdmsg
                        },
                    }).then(function(result) {
                        var rec = result;
//                        console.log(rec);
                        swal({
                            text: rec['message'],
                            title:rec['title'],
                            type: rec['status']
                        }).then((result) => {
                        	window.location.reload();
                        });

                    });
                }
            });


        },

    });
});
