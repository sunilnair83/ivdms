odoo.define("mail_forward.forward", function(require) {
    "use strict";

    var config = require('web.config');
    var core = require('web.core');
    var QWeb = core.qweb;
    var session = require('web.session');
    var _t = core._t;
    var ThreadWidget = require("mail.widget.Thread");
    var Model = require('web.BasicModel');
    var MessageModel = new Model('mail.message', session.user_context);

    ThreadWidget.include({
        events: _.defaults({
                "click .o_forward": "_onClickMessageForward",
            },
            ThreadWidget.prototype.events
        ),

        _onClickMessageForward: function(event) {
            var self = this;
            // forward email logic
            var do_action = this.do_action,
                msg_id = $(event.currentTarget).data("message-id");
            var read_fields = ['record_name', 'parent_id', 'subject', 'attachment_ids', 'date',
                'email_to', 'email_from', 'email_cc', 'model', 'res_id', 'body'
            ];

            this._rpc({
                model: "mail.message",
                method: "read",
                args: [
                    [msg_id]
                ],
                context: session.user_context,
            }).then(function(result) {
                var message = result[0];
                var subject = [_t("FWD")];
                if (message.subject) {
                    subject.push(message.subject);

                } else if (message.record_name && message.parent_id) {
                    subject.push(message.record_name);

                }
				else if (subject.length < 2) {
                    subject.push(_t("(No subject)"));
                }
                // Get only ID from the attachments
                var attachment_ids = [];
                for (var n in message.attachment_ids) {
                    attachment_ids.push(message.attachment_ids[n]);
                }
                // Get necessary fields from the forwarded message
                var header = [
                    "----------" + _t("Forwarded message") + "----------",
                    _t("From: ") + message.email_from,
                    _t("Date: ") + message.date
                ];
                if (message.subject) {
                    header.push(_t("Subject: ") + message.subject);
                }
                if (message.email_to) {
                    header.push(_t("To: ") + message.email_to);
                }
                if (message.email_cc) {
                    header.push(_t("CC: ") + message.email_cc);
                }
                header = header.map(_.str.escapeHTML).join("<br/>");
                var context = {
                    default_attachment_ids: attachment_ids,
                    default_body: "<p><i>" + header + "</i></p><br/>" +
                        message.body,
                    default_model: message.model,
                    default_res_id: message.res_id,
                    override_template: true,
                    default_subject: subject.join(": "),
                    forward_mail:true
                };

                //
                var action = {
                    name: _t('Forward Email'),
                    type: 'ir.actions.act_window',
                    res_model: 'mail.compose.message',
                    views: [
                        [false, 'form']
                    ],
                    target: 'new',
                    context: context,
                };
                return do_action(action, {
                    on_close: function() {
                        self.trigger_up('reload');
                    }

                });

            })

        },

    });
});
