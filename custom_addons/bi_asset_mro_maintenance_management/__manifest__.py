# -*- coding: utf-8 -*-
# Part of Browseinfo. See LICENSE file for full copyright and licensing details.
{
    'name': 'Assets MRO Equipment Maintenance Management in Odoo',
    'version': '13.0.0.2',
    'category': 'Project',
    'summary': 'Construction Assets MRO Maintenance Equipment Maintenance Equipment MRO system Equipment repair Management in construction mro construction Equipment repair machine repair construction repair checklist repair material mro repair Asset Maintenance Equipment',
    'description': '''MRO - Equipment Maintenance Management
	
	MRO - Equipment Maintenance Management
	
	Equipment Maintenance
	construction Mro
	Equipment Management
	Equipment rrapair Management in construction 
	
	''',
    'author': 'BrowseInfo',
    'website' : 'https://www.browseinfo.in',
    'price': 9,
    'currency': 'EUR',
    'depends': ['base','website','hr_maintenance','maintenance','bi_odoo_job_costing_management','bi_material_purchase_requisitions'],
    'data':[
        'security/ir.model.access.csv',
        'views/maintenance_management_views.xml',   
    ],
    'qweb': [
    ],
    'auto_install': False,
    'installable': True,
    'live_test_url' : 'https://youtu.be/2eZjRGTvYck',
    'images':['static/description/Banner.png'],
}
