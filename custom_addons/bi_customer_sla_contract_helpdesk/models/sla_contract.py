# -*- coding: utf-8 -*-
# Part of BrowseInfo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _
from datetime import datetime, timedelta
from odoo.exceptions import UserError, ValidationError

class PartenrInherit(models.Model):
    _inherit = 'account.analytic.account'

    helpdesk_sla_id = fields.Many2one('service.level.config',string='Helpdesk SLA Level',related="partner_id.sla_level_id",store=True,readonly=False)