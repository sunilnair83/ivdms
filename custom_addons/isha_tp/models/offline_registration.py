#from questionary import password
#from scipy.constants import alpha
import json
import logging
import traceback

import requests

from odoo import fields
from odoo import models, _
from odoo.http import request

_logger = logging.getLogger(__name__)

class OfflineRegsitration(models.Model):
    _name ="offline.program.registration"

    registration_amount = fields.Float(string="Amount")
    participant_email = fields.Char(stirng="participant email")
    company_id = fields.Many2one('res.company', readonly=True)
    currency_id = fields.Many2one('res.currency')
    country_id = fields.Many2one('res.country')
    first_name = fields.Char(string="First Name")
    last_name = fields.Char(string="Last Name")

    # added Offline code,desc fields
    offline_code_id = fields.Many2one('tp.program.registration.offline.code')
    offline_code_desc = fields.Text(string="Offline Desc")

    programapplied = fields.Many2one('tp.program.schedule', string='Program Selected')
    def sso_create_prof(self, data):
        try:

            parents_dict = {
                "firstName": data.get("firstname"),
                "lastName": data.get("lastname"),
                "email": data.get("email"),
                "password": "",
                "countryOfResidence": data.get("countrycode"),
            }
            sd = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.SSO_PMS_ENDPOINT1')
            print("sd:;", sd)
            datavar2 = json.dumps(parents_dict)
            authreq = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SYSTEM_TOKEN1')
            headers2 = {
                "content-type": "application/json",
                "Authorization": "Bearer " + authreq,
                "X-User-Country": data.get("countrycode")
            }
            # requestconsent_url = "https://uat-profile.isha.in/services/pms/api/admin/user-accounts?skipMail=true" + profidvar
            requestconsent_url = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.SSO_PMS_ENDPOINT1') + "admin/user-accounts?skipMail=true"
            # d_url ="http://uat-profile.isha.in/services/pms/api/"+ "admin/user-accounts?skipMail=true"
            reqvar = requests.post(requestconsent_url, datavar2, headers=headers2)
            try:
                _logger.info("goy offline reg create prof"+ str(reqvar.text))
            except:
                pass
            reqvar.raise_for_status()
            return reqvar
        except Exception as ex2:
            tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
            _logger.error(tb_ex)
            return tb_ex

    def sso_checkif_profalreadyexists(self, data):
        try:
            emailidvar2 = data.get("email")
            authreq = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SYSTEM_TOKEN1')
            headers2 = {
                #"content-type": "application/json",
                "Authorization": "Bearer " + authreq,
                "x-user-country": data.get("countrycode")
            }
            # requestconsent_url = "https://uat-profile.isha.in/services/pms/api/admin/user-accounts?skipMail=true" + profidvar
            requestconsent_url = request.env['ir.config_parameter'].sudo().get_param(
                'satsang.SSO_PMS_ENDPOINT1') + "admin/user-accounts?email="+emailidvar2
            # d_url ="http://uat-profile.isha.in/services/pms/api/"+ "admin/user-accounts?skipMail=true"
            #_logger.info("sso_checkif_profalreadyexists input params" + str(headers2))
            #_logger.info("sso_checkif_profalreadyexists input params" + str(requestconsent_url))
            reqvar = requests.get(requestconsent_url, headers=headers2, verify=True)
            #reqvar.raise_for_status()
            _logger.info("sso_checkif_profalreadyexists" + str(reqvar.text))
            return reqvar
        except Exception as ex2:
            tb_ex = ''.join(traceback.format_exception(etype=type(ex2), value=ex2, tb=ex2.__traceback__))
            _logger.error(tb_ex)
            return tb_ex

    def create_offline_registration(self):
        """ Create Offline Registration  """
        country_rec = self.env['res.country'].sudo().search([('id', '=', self.country_id.id)],limit=1)
        domain = []
        if self.participant_email:
            domain.append(("participant_email", "=", self.participant_email))
        rec_with_email = self.env["tp.program.registration"].search(domain, limit=1)
        if self.programapplied:
            domain.append(("programapplied", "=", self.programapplied.id))
        domain.append(("sso_id", "!=",""))
        already_register_with_prgrm = self.env["tp.program.registration"].search(domain,limit=1)
        if False:
            message_id = self.env['message.wizard'].create(
                {'message': _(
                    "Already registration done with this email  " + already_register_with_prgrm.participant_email) })
            return {
                'name': _('Warning'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'message.wizard',
                # pass the id
                'res_id': message_id.id,
                'target': 'new'
            }

        else:
               data = {
                        'firstname': self.first_name,
                        'lastname': self.last_name,
                        'email': self.participant_email,
                        'countrycode': country_rec.code,
                    }
               create_ssoid = '-1'
               profile_id='-1'
               checkifprofexistis = self.sso_checkif_profalreadyexists(data)
               checkifprofexistisresp = checkifprofexistis.json()
               if 'profileId' in checkifprofexistisresp:
                   profile_id = checkifprofexistisresp['profileId']
               if profile_id == '-1':
                   create_ssoid_api_reponse = self.sso_create_prof(data)
                   create_ssoid_api_reponse_value = create_ssoid_api_reponse.json()
                   profile_id = create_ssoid_api_reponse_value.get('profileId')
               if profile_id =='-1':
                   message_id = self.env['message.wizard'].create(
                       {'message': _(
                           "Unable to create profile .Try again with")})
                   return {
                       'name': _('Warning'),
                       'type': 'ir.actions.act_window',
                       'view_mode': 'form',
                       'res_model': 'message.wizard',
                       # pass the id
                       'res_id': message_id.id,
                       'target': 'new'
                   }

               ountryres = self.env['res.country'].sudo().search([('id', '=', rec_with_email.country.id)])
               baserequrl = self.env['ir.config_parameter'].sudo().get_param('tp.isoapiurlpath')
               request_url = baserequrl + country_rec.code
               headers2 = {
                    "content-type": "application/json",
               }
               try:
                    req = requests.get(request_url, headers=headers2)
                    req.raise_for_status()
                    respone = req.json()
                    goy_idvar = self.env['tp.program.schedule'].sudo().search([('pgmschedule_programtype', '=', 'In the Grace of Yoga 3-Day Online Program')])[0]
                    pbk_idvar = self.env['tp.program.schedule'].sudo().search([('pgmschedule_programtype', '=', 'In the Grace of Yoga 1-Day Online Program')])[0]
                    selectidvar = self.programapplied.id
                    reg_url = ""
                    if selectidvar == pbk_idvar.id:
                        reg_url = respone.get("pbk_reg_url")
                    if selectidvar ==  goy_idvar.id:
                        reg_url = respone.get("goy_reg_url")
                    offine_req={
                        "offline_logic":True,
                        "profile_id":profile_id,
                        "amount":str(self.registration_amount),
                        "program":self.programapplied.id
                    }
                    # print(self.encrypt(offine_req))
                    # print(self.decrypt(offine_req))
                    # passing the upgrade logic and registrationid
                    #https://mokshatwo.sushumna.isha.in/programs/pbk/registrationform?legal_entity=IFINC&program=2&country=104&checksum=d56c4c501092c9a896c931ed4be0a219349f3c65891529cbfe00b8d6f5a4f313&handle=iso&offline_logic=True&profile_id=ja8XyZ1heUTeP5565o7oV0dYU9v2&program=3&amount=4564.0
                    #http://localhost:8079/programs/pbk/registrationform?legal_entity=IFINC&program=2&country=104&checksum=d56c4c501092c9a896c931ed4be0a219349f3c65891529cbfe00b8d6f5a4f313&handle=iso&offline_logic=True&profile_id=ja8XyZ1heUTeP5565o7oV0dYU9v2&program=3&amount=4564.0

                    redirect_url = reg_url+"&offline_logic=True&profile_id="+str(profile_id)+"&reg_program="+str(self.id)+"&amount="+str(self.registration_amount)+"&offline_code_id="+str(self.offline_code_id.name)+"&offline_code_desc="+str(self.offline_code_desc)
                    print(redirect_url)
                    return{
                        'type': 'ir.actions.act_url',
                        'name': "Offline Registration",
                        'target': 'new',
                        'url': redirect_url,
               }

               except Exception as e:
                    message_id = self.env['message.wizard'].create(
                        {'message': _(e)})
                    return {
                        'name': _('Successfull'),
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'message.wizard',
                        # pass the id
                        'res_id': message_id.id,
                        'target': 'new'
                    }
                    print(e)




