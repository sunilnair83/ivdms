import hashlib
import hmac
import json
import logging
import traceback
from datetime import timedelta, datetime

import requests
from Crypto.Cipher import AES

from odoo import api, models

_logger = logging.getLogger(__name__)

class PaymentStatus(models.AbstractModel):
    _name = 'tp.paymentstatus'
    _description = 'Isha TP Payment Status'

    @api.model
    def pad(self, data):
        length = 16 - (len(data) % 16)
        data += chr(length)*length
        return data

    @api.model
    def _unpad(self, data):
        data = data[:-data[-1]]
        return data

    @api.model
    def encrypt(self, plainText,workingKey):
        iv = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f'
        plainText = self.pad(plainText)
        encDigest = hashlib.md5()
        encDigest.update(workingKey.encode('utf-8'))
        enc_cipher = AES.new(encDigest.digest(), AES.MODE_CBC, iv)
        encryptedText = enc_cipher.encrypt(plainText).hex()
        return encryptedText

    @api.model
    def decrypt(self, cipherText,workingKey):
        iv = '\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f'
        decDigest = hashlib.md5()
        decDigest.update(workingKey.encode('utf-8'))
        encryptedText = bytes.fromhex(cipherText)
        dec_cipher = AES.new(decDigest.digest(), AES.MODE_CBC, iv)
        decryptedText = self._unpad(dec_cipher.decrypt(encryptedText)).decode('utf-8')
        return decryptedText

    @api.model
    def res(self, encResp):
        workingKey = self.env['ir.config_parameter'].sudo().get_param('tp.workingkey')
        decResp = self.decrypt(encResp,workingKey)
        return decResp

    def resdecbyworkingkey(self, encResp, workingKey):
        decResp = self.decrypt(encResp, workingKey)
        return decResp

    def templog(self, logtext):
        self.env['tp.tempdebuglog'].create({
            'logtext': logtext
        })

    @api.model
    def _checkPendingPaymentStatus(self):

        print('inside _checkPendingPaymentStatus')
        self.templog('inside _checkPendingPaymentStatus')
        _logger.info('inside _checkPendingPaymentStatus')
        foundrec = 0

        checkdatetime = datetime.now() - timedelta(minutes=15)
        
        print('current date: ')
        print(datetime.now())
        print('checkdatetime: ')
        print(checkdatetime)
        _logger.info(str(checkdatetime))
        self.templog(str(checkdatetime))
        plist1 = self.env['program.registration'].search([('registration_status','=','Payment Link Sent'),('payment_status','=','Initiated'),
        ('payment_initiated_dt','<', checkdatetime)])

        plist2 = self.env['program.registration'].search([('registration_status','=','Payment Link Sent'),('payment_status','=','Initiated'),
        ('payment_initiated_dt','=', False)])

        plist3 = self.env['program.registration'].search([('registration_status','=','Payment Link Sent'),('payment_status','=','Awaited'),
        ('payment_initiated_dt','<', checkdatetime)])

        plist4 = self.env['program.registration'].search([('registration_status','=','Payment Link Sent'),('payment_status','=','Awaited'),
        ('payment_initiated_dt','=', False)])

        plist = plist1 | plist2 | plist3 | plist4
        _logger.info(str(plist))
        self.templog(str(plist))
        accessCode = ""
        workingKey = ""
        ccavurl = ""

        for rec in plist:

            try:
                progtypevar = rec.programapplied.pgmschedule_programtype
                consultationprog = progtypevar.description.upper().__contains__("ONLINE")
                rejuvenationtype = progtypevar.programcategory.programcategory.upper() == 'REJUVENATION'
                foundrec = 1
                if consultationprog and rejuvenationtype:
                    accessCode = self.env['ir.config_parameter'].sudo().get_param('tp.accesscode2')
                    workingKey = self.env['ir.config_parameter'].sudo().get_param('tp.workingkey2')
                    ccavurl = self.env['ir.config_parameter'].sudo().get_param('tp.ccavstatuscheckurl')
                else:
                    accessCode = self.env['ir.config_parameter'].sudo().get_param('tp.accesscode')
                    workingKey = self.env['ir.config_parameter'].sudo().get_param('tp.workingkey')
                    ccavurl = self.env['ir.config_parameter'].sudo().get_param('tp.ccavstatuscheckurl')

                merchant_data = {'order_no' : rec.orderid}
                encryption = self.encrypt(json.dumps(merchant_data), workingKey)

                print('api request data preparation completed, going to call api..')

                self.env['tp.tempdebuglog'].create({
                    'logtext': str(encryption)
                })

                params = { 'version': "1.1",
                        'request_type': "JSON",
                        'access_code': accessCode,
                        'command': "orderStatusTracker",
                        'response_type': "JSON",
                        'enc_request': encryption
                }

                print(params)

                self.env['tp.tempdebuglog'].create({
                    'logtext': json.dumps(params)
                })

                try:
                    result = requests.post(ccavurl, params)
                    print(result)
                    print(result.text)
                    self.templog('paymentstatuscheck result')
                    self.templog(result.text)
                except Exception as e:
                    print('error in http call..')
                    print(e)
                    _logger.error('status response')
                    tb_ex2 = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                    _logger.error(tb_ex2)
                    self.templog('paymentstatuscheckerror')
                    self.templog(tb_ex2)
                    print('paymentstatuscheckerror')
                    print(tb_ex2)
                    _logger.info('paymentstatuscheckerror')
                    _logger.info(tb_ex2)


                self.env['tp.tempdebuglog'].create({
                    'logtext': str(result)
                })

                self.env['tp.tempdebuglog'].create({
                    'logtext': result.text
                })

                self.env['tp.tempdebuglog'].create({ 'logtext': result.text })

                # status=1&enc_response=Access_code: Invalid Parameter&enc_error_code=51407

                orderstatus = ''
                amountpaid = 0

                if (result.text != None and result.text != "" and len(result.text) > 0):
                    tresult = result.text.split('&')
                    if (tresult[0].split('=')[1] == "1" or tresult[0].split('=')[1] == "0"):
                        if (tresult[0].split('=')[1] == "1"):
                            self.templog(result.text)
                            self.env['tp.participant.paymenttransaction'].create({
                                'programregistration': rec.id,
                                'orderid': rec.orderid,
                                'payment_status': '',
                                'transactiontype': 'StatusCheck',
                                'comments': tresult[1].split('=')[1]
                            })
                            continue
                        print('error in api call..')
                        enc_response = tresult[1].split('=')[1]
                        plainText = self.resdecbyworkingkey(enc_response, workingKey)
                        print('encryption completed')
                        self.templog(plainText)
                        response = json.loads(plainText)
                        orderstatus = response['order_status']
                        amountpaid = float(response['order_amt'])
                        bank_response = response['order_bank_response']
                    else:
                        # status success
                        enc_response = tresult[1].split('=')[1]
                        self.templog(enc_response)
                        plainText = self.resdecbyworkingkey(enc_response, workingKey)
                        print('encryption completed')
                        self.templog(plainText)
                        response = json.loads(plainText)
                        orderstatus = response['order_status']
                        amountpaid = float(response['order_amt'])
                        bank_response = response['order_bank_response']
                        self.templog('orderstatus received: '+ orderstatus)
                else:
                    print('response is empty')
                    continue

                if (rec.payment_status != orderstatus or orderstatus == 'Initiated'):

                    if (orderstatus == 'Success' or orderstatus == 'Shipped'):
                        self.env['tp.participant.paymenttransaction'].create({
                                                'programregistration': rec.id,
                                                'orderid': rec.orderid,
                                                'payment_status': orderstatus,
                                                'transactiontype': 'StatusCheck',
                                                'comments': bank_response,
                                                'amount': amountpaid,
                                                'billing_name': rec.billing_name,
                                                'billing_address': rec.billing_address,
                                                'billing_city': rec.billing_city,
                                                'billing_state': rec.billing_state,
                                                'billing_zip': rec.billing_zip,
                                                'billing_country': rec.billing_country,
                                                'billing_tel': rec.billing_tel,
                                                'billing_email': rec.billing_email
                                            })
                    else:
                        self.env['tp.participant.paymenttransaction'].create({
                                                'programregistration': rec.id,
                                                'orderid': rec.orderid,
                                                'payment_status': orderstatus,
                                                'transactiontype': 'StatusCheck',
                                                'comments': bank_response,
                                                'amount': amountpaid
                                            })

                    # 'Auto-Cancelled'
                    # 'Reversed'
                    # 'Awaited'
                    # 'Cancelled'
                    # 'Invalid'
                    # 'Fraud'
                    # 'Initiated'
                    # 'Refunded'
                    # 'Shipped'
                    # 'Systemrefund'
                    # 'Unsuccessful'
                    # 'Failure'

                    if (orderstatus == 'Auto-Cancelled' or orderstatus == 'Reversed'  or
                    orderstatus == 'Cancelled' or orderstatus == 'Invalid' or orderstatus == 'Fraud'
                    or orderstatus == 'Refunded' or orderstatus == 'Systemrefund' or orderstatus == 'Unsuccessful'
                    or orderstatus == 'Initiated' or orderstatus == 'Aborted' or orderstatus == 'Failure'):
                        rec.write({
                            'payment_status': '',
                            'payment_initiated_dt': False
                        })
                        self.env['tp.payments']._reverseSeat(rec)
                    elif (orderstatus == 'Awaited'):
                        rec.write({
                            'payment_status': orderstatus
                        })
                    elif (orderstatus == 'Success' or orderstatus == 'Shipped'):
                        self.env['tp.payments']._updatePaidStatus(rec, amountpaid)
            except Exception as e:
                tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                self.templog('paymentstatuscheckerror2')
                self.templog(tb_ex)
                print('paymentstatuscheckerror2')
                print(tb_ex)
                _logger.info('paymentstatuscheckerror2')
                _logger.info(tb_ex)


        if foundrec == 1:
            print('payment status check: completed check and update for all request')
        else:
            print('payment status check: no records found, completed..')

    @api.model
    def generateEreceipt(self, request_type, rec):
        
        print('inside generateEreceipt')
        
        try:
            
            ereceipturl = self.env['ir.config_parameter'].sudo().get_param('tp.ereceipturl')
            ereceipttreasurer = self.env['ir.config_parameter'].sudo().get_param('tp.ereceipttreasurer')
            ereceiptsecretkey = self.env['ir.config_parameter'].sudo().get_param('tp.ereceiptsecretkey')
            ereceiptwho = self.env['ir.config_parameter'].sudo().get_param('tp.ereceiptwho')
            ereceiptcenter = self.env['ir.config_parameter'].sudo().get_param('tp.ereceiptcenter')
            ereceiptmode = self.env['ir.config_parameter'].sudo().get_param('tp.ereceiptmode')

            pgmschedule_online_code = rec.programapplied.pgmschedule_online_code
            if (rec.programapplied.pgmschedule_online_code == False):
                pgmschedule_online_code = ""

            packagecost = rec.packageselection.pgmschedule_packagetype.packagecost

            if (request_type == 'normal'):
                if (packagecost != rec.amount_paid):
                    if (rec.amount_paid + rec.transferred_amount == packagecost):
                        packagecost = rec.amount_paid

            paymenttrackingidvar = '0'
            try:
                caller_sudo = self.env['tp.participant.paymenttransaction'].sudo().search(
                    [('programregistration', '=', rec.id), ('payment_status', '=', 'Success'),
                     ('orderid', '=', rec.orderid), ('transactiontype', '=', 'Response')], limit=1, order='id desc')
                if caller_sudo:
                    for recvar in caller_sudo:
                        paymenttrackingidvar = recvar[0].paymenttrackingid

                if paymenttrackingidvar == '0':
                    caller_sudo2 = self.env['tp.participant.paymenttransaction'].sudo().search(
                        [('programregistration', '=', rec.id), ('payment_status', '=', 'Success'),
                         ('orderid', '=', rec.orderid), ('transactiontype', '=', 'StatusCheck')], limit=1, order='id desc')
                    for recvar2 in caller_sudo2:
                        paymenttrackingidvar = recvar2[0].paymenttrackingid

                if paymenttrackingidvar == '0':
                    caller_sudo3 = self.env['tp.participant.paymenttransaction'].sudo().search(
                        [('programregistration', '=', rec.id), ('payment_status', '=', 'Shipped'),
                         ('orderid', '=', rec.orderid), ('transactiontype', '=', 'StatusCheck')], limit=1, order='id desc')
                    for recvar3 in caller_sudo3:
                        paymenttrackingidvar = recvar3[0].paymenttrackingid

            except Exception as e2:
                self.templog('error inside paymenttrackingid block in generate ereceipt')
                self.templog(str(rec))
                tb_ex2 = ''.join(traceback.format_exception(etype=type(e2), value=e2, tb=e2.__traceback__))
                self.templog(tb_ex2)
                _logger.info(tb_ex2)

            req_data = {
                "request": "createEreceipt",
                "requestId":  rec.orderid,
                "emailid": ereceipttreasurer,
                "entity": rec.programapplied.pgmschedule_entity.entityname,
                "purpose": rec.programapplied.pgmschedule_pgmpurpose,
                "center": ereceiptcenter,
                "receiptDetails":
                    {
                        "mode": ereceiptmode,
                        "name": rec.billing_name,
                        "address": rec.billing_address,
                        "city": rec.billing_city,
                        "pincode": rec.billing_zip,
                        "state": rec.billing_state,
                        "mobile": rec.billing_tel,
                        "amount": rec.amount_paid,
                        "transref": rec.orderid,
                        "instdate": datetime.strftime(rec.date_paid, '%d %b %Y'),
                        "email": rec.billing_email,
                        "nationality": rec.nationality_id.name,
                        "country": rec.billing_country,
                        "passportNo": rec.passportnumber,
                        "programType": "",
                        "programLocation": "Isha Yoga Center",
                        "programCost": packagecost,
                        "programParticipantsCount": "",
                        "programDiscount": "",
                        "pan": "",
                        "orderRefNum": paymenttrackingidvar
                    },
                "requestKey": rec.orderid,
                "_who": ereceiptwho,
                "programcode": pgmschedule_online_code
            }

            print(req_data)
            self.env['tp.tempdebuglog'].create({'logtext': json.dumps(req_data)})

            # encoding the params
            message = bytes(json.dumps(req_data), 'utf-8')

            # Secret Key from ereceipts
            secret = bytes(ereceiptsecretkey, 'utf-8')

            signature = hmac.new(secret, message, digestmod=hashlib.sha256).hexdigest()
            print(json.dumps(signature))

            # Headers. If you are changing the secret plz replace 'devNurseryCC' with the corresponding identity value.
            headers = {
                'content-type': 'application/json',
                'Authorization': 'hmac ' + ereceiptwho + ':' + str(signature)
            }

            # Request
            r = requests.put(ereceipturl, data=json.dumps(req_data), headers=headers)

            print('ereceipt response')
            print(r)
            print(r.text)

            self.env['tp.tempdebuglog'].create({
                'logtext': r.text
            })

            result = json.loads(r.text)
            print(result)

            print(result['status'])

            if (result['status'] == 'ok'):
                rec.write({'ereceiptgenerated': True,
                 'ereceiptreference': result['eReceiptNumber'],
                 'ereceipturl': result['url'],
                })

                tran = self.env['tp.participant.paymenttransaction'].search([('orderid','=', rec.orderid),
                ('payment_status','=','Success')])
                
                if tran.exists():
                    tran.write({'ereceiptgenerated': True,
                    'ereceiptreference': result['eReceiptNumber'],
                    'ereceipturl': result['url'],
                    'ereceiptgenerated_dt': datetime.now()
                    })
                else:
                    tran = self.env['tp.participant.paymenttransaction'].search([('orderid','=', rec.orderid),
                    ('payment_status','=','Shipped')])
                    if tran.exists():
                        tran.write({'ereceiptgenerated': True,
                        'ereceiptreference': result['eReceiptNumber'],
                        'ereceipturl': result['url'],
                        'ereceiptgenerated_dt': datetime.now()
                        })
                
            else:
                print('error status received: ' + result['status'])

        except Exception as e:
            print(e)
            print('error inside generateEreceipt')
            self.templog('error inside generateEreceipt')
            _logger.info('error inside generateEreceipt')
            tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
            self.templog(tb_ex)
            _logger.info(tb_ex)

    @api.model
    def _checkAndGenerateEreceipt(self):
        
        print('inside checkAndGenerateEreceipt')
        
        foundrec = 0

        plist = self.env['program.registration'].search([('registration_status','=','Paid'),('ereceiptgenerated','=',False),('amount_paid','>',0)])
        for rec in plist:
            foundrec = 1
            self.generateEreceipt('normal', rec)


        if foundrec == 1:
            print('ereceipt generate: completed generate and update for all request')
        else:
            print('ereceipt generate: no records found, completed..')
