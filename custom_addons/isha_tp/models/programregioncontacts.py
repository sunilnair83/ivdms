from odoo import models, fields, api, exceptions

class Programregioncontacts(models.Model):
    _name = 'tp.program.region.contacts'
    _description = 'region wise contacts for PBK program'
    _rec_name = 'region_name'

    region_name = fields.Many2one('isha.region',required=True, string='Region Name')
    # region_name = fields.Char(string='Region Name', related='region_id.name', store=True)
    key = fields.Selection([('email','Email'),('phone','Phone'),('tnc','Terms & Conditions and Cancelation Policy'),('res_add_text','Residential Address Text'),('pbk_note_text','PBK Note Text'),('acct_terms_text','Acceptance Terms Text'),('thankyou_addtional_text','ThankYou Page Additional Text'),('confirmation_addtional_text','Confirmation Page Additional Text')], string='Key', required=True)
    value = fields.Text(string="Value", required=True)
