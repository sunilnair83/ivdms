from odoo import models, fields, api, exceptions

class Entity(models.Model): 
	_name = 'tp.entity'
	_description = 'Entity'
	_order = 'entityname'
	_rec_name = 'entityname'
	_sql_constraints = [('unique_entityname','unique(entityname)','Cannot have duplicate entity name, give different name')]

	entityname = fields.Char(string = 'Entity Name', size=100, required=True)

class PaymentGateway(models.Model): 
	_name = 'tp.paymentgateway'
	_description = 'Payment Gateway'
	_order = 'paymentgatewayname'
	_rec_name = 'paymentgatewayname'
	_sql_constraints = [('unique_paymentgatewayname','unique(paymentgatewayname)','Cannot have duplicate payment gateway, give different gateway')]

	paymentgatewayname = fields.Char(string = 'Payment Gateway', size=100, required=True)	

class TpLanguage(models.Model):
	_name = 'tp.language1'
	_description = 'Language Description'
	_order = 'languagedescription'
	_rec_name = 'languagedescription'
	_sql_constraints = [('unique_languagedescription','unique(languagedescription)','Cannot have duplicate language, give different name')]

	languagedescription = fields.Char(string = 'Language Description', size=100, required=True)	

class PracticeMaster(models.Model): 
	_name = 'tp.practice'
	_description = 'Practice Master'
	_order = 'practicename'
	_rec_name = 'practicename'
	_sql_constraints = [('unique_practicename','unique(practicename)','Cannot have duplicate practice name, give different practice name')]

	practicename = fields.Char(string = 'Practice Name', size=100, required=True)		

class NotificationMaster(models.Model): 
	_name = 'tp.notificationevent'
	_description = 'Notification Event'
	#_order = 'lob, programtype'
	_rec_name = 'notificationevent'
	_sql_constraints = [('unique_notificationevent','unique(notificationevent)','Cannot have duplicate notification event, give different name')]

	notificationevent = fields.Char(string = 'Notification Event Code', size=100, required=True)
	
class GlobalNotificationMaster(models.Model): 
	_name = 'tp.globalnotificationevent'
	_description = 'Global Notification Event'
	#_order = 'lob, programtype'
	_rec_name = 'notificationevent'
	_sql_constraints = [('unique_globalnotificationevent','unique(notificationevent)','Cannot have duplicate notification event, give different name')]

	notificationevent = fields.Char(string = 'Notification Event Code', size=100, required=True)

class GlobalNotificationTemplateMaster(models.Model): 
	_name = 'tp.globalnotificationtemplate'
	_description = 'Global Notification Template'
	#_order = 'lob, programtype'
	_rec_name = 'notificationevent'
	_sql_constraints = [('unique_globalnotificationevent1','unique(notificationevent)','Cannot have duplicate notification event')]
	
	notificationevent = fields.Many2one('tp.globalnotificationevent', string = 'Notification Event')
	sendemail = fields.Boolean(string='Send Email', default=False)
	emailtemplateid = fields.Many2one('mail.template',string='Email Template',track_visibility='onchange')
	sendsms = fields.Boolean(string='Send SMS', default=False)
	smstemplateid = fields.Many2one('sms.template',string='SMS Template',track_visibility='onchange')

class NotificationTemplateMaster(models.Model): 
	_name = 'tp.notificationtemplate'
	_description = 'Notification Template'
	#_order = 'lob, programtype'
	_rec_name = 'notificationevent'
	_sql_constraints = [('unique_notificationevent1','unique(programtype, notificationevent)','Cannot have duplicate notification event')]

	programtype = fields.Many2one('tp.package.parameters', string = 'Program Type')
	notificationevent = fields.Many2one('tp.notificationevent', string = 'Notification Event')
	sendemail = fields.Boolean(string='Send Email', default=False)
	emailtemplateid = fields.Many2one('mail.template',string='Email Template',track_visibility='onchange')
	sendsms = fields.Boolean(string='Send SMS', default=False)
	smstemplateid = fields.Many2one('sms.template',string='SMS Template',track_visibility='onchange')


class RejectionReason(models.Model): 
	_name = 'tp.rejection.reason'
	_description = 'Rejection Reason'
	#_order = 'lob, programtype'
	_rec_name = 'reason_description'
	_sql_constraints = [('unique_reason_description','unique(reason_description)','Cannot have duplicate reason description, give different reason description')]

	reason_description = fields.Char(string = 'Reason Description', size=100, required=True)


class TempDebugLog(models.Model): 
	_name = 'tp.tempdebuglog'
	_description = 'Temp debug log'
	
	logtext = fields.Text(string='logtext')

class MasterProgramCategory(models.Model):
		_name = 'tp.master.program.category'
		_description = "Master list of program categories"
		_rec_name = 'programcategory'

		programcategory = fields.Char('Program Category')
		keyprogram = fields.Boolean('Key Program', default=False)


class IshaProgramCategory(models.Model):
		_name = 'tp.program.category'
		_description = "Isha list of program categories"
		_rec_name = 'program_name'

		program_name = fields.Char('Program Name')
		keyprogram = fields.Boolean('Key Program', default=False)
