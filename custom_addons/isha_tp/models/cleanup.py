import json
from odoo import api, models
from odoo.exceptions import UserError
from datetime import date, timedelta, datetime

class Cleanup(models.AbstractModel):
    _name = 'tp.cleanup'
    _description = 'Isha Isha Celebrations DB Cleanup'

    @api.model
    def cleanup(self):
        templogdays = self.env['ir.config_parameter'].sudo().get_param('tp.templogdays')
        templogdays = int(templogdays)
        if (templogdays == 0 or templogdays == False):
            templogdays = 90
        diff = (datetime.now() - timedelta(days=templogdays))
        print (diff)
        self.env['tp.tempdebuglog'].search([('create_date','<', diff)]).unlink()
        print('cleanup completed..')
