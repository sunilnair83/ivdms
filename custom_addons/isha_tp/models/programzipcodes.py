from odoo import models, fields, api, exceptions


class programzipCodes(models.Model):
    _name = 'tp.program.zip.codes'
    _description = 'Zip codes where temple team can ship package'
    # _order = 'countryname'
    _rec_name = 'pincode'
    # _sql_constraints = [('unique_pincodepercountry','unique(countryname,pincode)','Cannot have duplicate pincode for the same country')]

    countryname = fields.Many2one('res.country', required=True)
    statename = fields.Many2one('res.country.state')
    pincode = fields.Char(string="Pincode/Zipcode", required=False, index=True)
    notserviceable = fields.Boolean('Not Serviceable', default=False)

# @api.model
# def fields_get(self, fields=None):
#     fields_to_show = ['countryname','statename','pincode']
#     res = super(programZipCodes, self).fields_get()
#     for field in res:
#         if (field in fields_to_show):
#             res[field]['selectable'] = True
#             res[field]['sortable'] = True
#         else:
#             res[field]['selectable'] = False
#             res[field]['sortable'] = False
#     return res
