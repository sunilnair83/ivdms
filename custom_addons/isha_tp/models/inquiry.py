from odoo import models, fields, api, exceptions
from . import commonmodels
from . import programtype
from . import programschedule
import time
import datetime

class CandidateInquiry(models.Model): 
	_name = 'tp.inquiry'
	_description = 'Isha Celebrations Inquiry'
	_rec_name = 'first_name'

	first_name = fields.Char(string = 'First Name')
	last_name = fields.Char(string = 'Last Name')
	countrycode = fields.Char(string='Country Tel code')
	mobile = fields.Char(string = 'Mobile')
	email = fields.Char(string = 'Email')
	city = fields.Char(string='City', required=False)
	country = fields.Many2one('res.country', 'Country of residence')
	comments =  fields.Text(string = 'Comments',required=False)
	selected_program = fields.Many2one('tp.program.schedule',string = 'Selected Program' ,required=False)
	selected_programtype = fields.Many2one(related='selected_program.pgmschedule_programtype',required=False)
	inquiry_status =  fields.Selection([('Inquiry','Inquiry'),('Registered','Registered')], string='Status')
	program_preference = fields.Selection([('three-day-online-program', '3-day online program (Sadhguru sessions + Pancha bhuta kriya)'), ('1-day-online-program', '1-day online program (Pancha bhuta kriya only)')], string='Select your program preference:')

	ack_email_send = fields.Boolean(string="Acknowledge Email Send", default=False)
	ack_sms_send = fields.Boolean(string="Acknowledge SMS Send", default=False)

	@api.model
	def fields_get(self, fields=None):
		fields_to_show = ['first_name','last_name','countrycode','mobile','email','city','country','comments','selected_program','selected_programtype','inquiry_status','program_preference','ack_email_send','ack_sms_send']
		res = super(CandidateInquiry, self).fields_get()
		for field in res:
			if (field in fields_to_show):
				res[field]['selectable'] = True	
				res[field]['sortable'] = True
			else:
				res[field]['selectable'] = False
				res[field]['sortable'] = False
		return res
		
	def update_registered(self):
		print('update triggered')
		nlist = self.env['tp.inquiry'].search([('inquiry_status','=','Inquiry')])
		if nlist:
			print('list found')
			for rec in nlist:
				print('rec found')
				print(rec)
				ids = self.env['program.registration'].search([('participant_email','=',rec.email),('participant_programtype','=',rec.selected_programtype.id),('create_date','>',rec.create_date)])
				if ids.exists():
					print('found party')
					rec.write({'inquiry_status':'Registered'})

	@api.model
	def create(self, vals):   
		rec = super(CandidateInquiry, self).create(vals)
		#self.env['tp.notification']._sendInquiryAcknowledgement(rec)
		return rec

	@api.model
	def sendInquiryAcknowledgement(self):
		reclist = self.env['tp.inquiry'].search([('inquiry_status','=','Inquiry'),
		('ack_email_send','=', False), ('ack_sms_send','=', False)])
		for rec in reclist:
			self.env['tp.notification']._sendInquiryAcknowledgement(rec)
		print('completed sending inquiry acknowledgement')
