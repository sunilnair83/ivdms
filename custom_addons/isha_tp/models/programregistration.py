import json
import logging
import traceback
import uuid
from datetime import datetime

import requests
from lxml import etree
from werkzeug import urls

from odoo import fields
from odoo import models, api, _, exceptions
from odoo.exceptions import except_orm
from odoo.http import request
from ..isha_crm_importer import isha_crm

# from .. import isha_crm_importer

_logger = logging.getLogger(__name__)
try:
    from html2text import HTML2Text
except ImportError as error:
    _logger.debug(error)


def html2text(html):
    """ covert html to text, ignoring images and tables
    """
    if not html:
        return ""

    ht = HTML2Text()
    ht.ignore_images = True
    ht.ignore_tables = True
    ht.ignore_emphasis = True
    ht.ignore_links = True
    return ht.handle(html)


class ValidationError(except_orm):
    """Violation of python constraints.

                 .. admonition:: Example

                     When you try to create a new user with a login which already exist in the db.
                 """

    def __init__(self, msg):
        super(ValidationError, self).__init__(msg)


class ProgramRegistration(models.Model):
    _name = 'tp.program.registration'
    _description = 'Program Registration Forms'
    _rec_name = 'first_name'

    # _order = 'last_modified_dt'
    # participant_id = fields.Integer(string="ParticipantId", default=lambda self: self.env['ir.sequence'].next_by_code('increment_your_field'))

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        result = super(ProgramRegistration, self).fields_view_get(view_id=view_id, view_type=view_type,
                                                                  toolbar=toolbar, submenu=submenu)
        backoffice_user= self.user_has_groups('isha_tp.group_tp_backofficeuser')
        backoffice_admin = self.user_has_groups('isha_tp.group_tp')
        readonly_grp = self.user_has_groups('isha_tp.group_tp_participant_grp')
        print(readonly_grp)

        EDITABLE_FIELDS = []
        if backoffice_user:
            EDITABLE_FIELDS = ['shipping_country', 'shipping_state', 'shipping_pincode',
                               'shipping_address_line1', 'shipping_address_line2',
                               'shipping_city']

        if not backoffice_admin:
            if view_type == 'form':
                    doc = etree.XML(result['arch'])
                    if readonly_grp:
                        for node in doc.xpath("//form"):
                            node.set('create','false')
                            node.set('import','false')
                            node.set('edit','false')
                    for node in doc.xpath("//field"):
                        name = node.get('name')
                        if name not in EDITABLE_FIELDS:
                            if node.attrib.get('modifiers'):
                                attr = json.loads(node.attrib.get('modifiers'))
                                attr['readonly'] = 1
                                node.set('modifiers', json.dumps(attr))
                    result['arch'] = etree.tostring(doc)
        return result


    def resend_mail(self):
        '''
        Resend the confirmation mail to participant if status is paid
        '''

        ir_model_data = self.env['ir.model.data']

        try:
            compose_form_id = ir_model_data.get_object_reference('mail', 'email_compose_message_wizard_form')[1]
        except ValueError:
            compose_form_id = False
        lang = self.env.context.get('lang')

        template = request.env.ref('isha_tp.tp_mail_template_program_registration')
        if template and template.lang:
            lang = template._render_template(template.lang, 'tp.program.registration', self.ids[0])
        compose_form = self.env.ref('mail.email_compose_message_wizard_form', False)
        active_ids = self._context.get('active_ids')
        reg_records = self.env['tp.program.registration'].sudo().search(
            [("id", "in", active_ids), ("registration_status", "=", "Paid")])
        fail_count = self.env['tp.program.registration'].sudo().search(
            [("id", "in", active_ids), ("registration_status", "!=", "Paid")])
        if fail_count:
            raise exceptions.ValidationError(
                'Confirmation email can be sent only to participants who have already PAID. Pls confirm if every participant selected have PAID.')
        ctx = dict(
            default_model='tp.program.registration',
            default_res_id=self.id,
            default_active_ids=reg_records.ids,
            default_use_template=bool(template),
            default_template_id=template.id,
            default_composition_mode='mass_mail',
            custom_layout='mail.mail_notification_light',
        )
        return {
            'name': _('Compose Email'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'mail.compose.message',
            'views': [(compose_form.id, 'form')],
            'view_id': compose_form.id,
            'target': 'new',
            'context': ctx,
        }


    def _get_default_access_token(self):
        return str(uuid.uuid4())

    # basic profile info
    first_name = fields.Char(string="First Name", required=True)
    last_name = fields.Char(string="Last Name", required=True)
    name_called = fields.Char(string="Name you prefer", required=False)
    gender = fields.Selection([('Male', 'Male'), ('Female', 'Female'), ('Others', 'Others')], string='Gender',
                              required=False)
    marital_status = fields.Selection(
        [('Single', 'Single'), ('Partnered', 'Partnered'), ('Married', 'Married'), ('Divorced', 'Divorced'),
         ('Widowed', 'Widowed')], string='Marital Status', required=False)
    dob = fields.Date(string='Date Of Birth', required=False)
    age = fields.Char('Age', compute="_set_age", store=True, required=False)
    # Added Regsistration amount
    registration_amount = fields.Float(string="Amount")
    registration_currency = fields.Char(string="Registration currency")
    # added Offline code,desc fields
    is_offline_reg = fields.Boolean(string="Is Offline Registration")
    offline_code_id = fields.Many2one('tp.program.registration.offline.code')
    offline_code_desc = fields.Text(string="Offline Desc")
    # courier_tracking_no = fields.Char(string="Courier Tracking No")
    # upgrade vie
    current_record_id = fields.Many2one('tp.program.registration', 'Current revision', readonly=True, copy=True)
    old_revision_ids = fields.One2many('tp.program.registration', 'current_record_id', 'Old revisions', readonly=True,
                                       context={'active_test': False})

    def fetch_program_inforegform(self,country_code=None, program_id=None):
        # _logger.info("start executing function fetch_program_inforegform country_code"+country_code)
        # _logger.info("program_id"+program_id)
        res = dict()
        seatsavailable = None
        res['openstatus'] = 'open'
        res['price'] = None
        res['curr'] = None
        try:
            country_data = request.env['res.country'].sudo().search(
                [('code', '=', country_code)])

            region_name = country_data.center_id.region_id.name
            progschedule = request.env['tp.program.schedule'].sudo().search(
                [('id', '=', int(program_id))])
            serviceinfo = request.env['tp.program.zip.codes'].sudo().search(
                [('notserviceable', '=', 'true')])
            # print('inside function')
            if progschedule:
                progscheduleroom = request.env['tp.program.schedule.roomdata'].sudo().search(
                    [('pgmschedule_is_active', '=', 'True'), ('pgmschedule_regularseatsbalance', '>', 0),
                     ('pgmschedule_programname', '=', int(program_id))])
                for pkg in progscheduleroom:
                    # print('Inside progscheduleroom loop')
                    for ctry in pkg.pgmschedule_country:
                        if (ctry.name == country_data.name):
                            seatsavailable = True
                            # print('Inside Country Srch')
                            break
                    for rgn in pkg.pgmschedule_region:
                        if (rgn.name == region_name):
                            seatsavailable = True
                            # print('Inside Region Srch')
                            break

            # print('seatsavailable')
            if progschedule:
                pricinginfo = progschedule.pgmpricing_data.sudo().search(
                    [('pgmpricing_programname', '=', int(program_id))])
                try:
                    for prc in pricinginfo:
                        for cntry in prc.pgmpricing_countryname:
                            if (country_data.name == cntry.name):
                                res['price'] = prc.pgmpricing_countrycost
                                res['curr'] = prc.pgmpricing_countrycur.name
                                raise StopIteration
                except StopIteration:
                    pass

            if seatsavailable:
                if progschedule.pgmschedule_registrationopen:
                    res['openstatus'] = 'open'
                else:
                    res['openstatus'] = 'closed'
                for sinfo in serviceinfo:
                    if (country_data.name == sinfo.countryname.name):
                        res['openstatus'] = 'NA'
                        break

            # # print(program_id)
            # pricinginfo = progschedule.pgmpricing_data.sudo().search(
            #     [('pgmpricing_programname', '=', program_id)])
            #
            # #print(pricinginfo)
            # try:
            #     for prc in pricinginfo:
            #         for cntry in prc.pgmpricing_countryname:
            #             if (country_data.name == cntry.name):
            #                 #print(country_data.name)
            #                 #print(cntry.name)
            #                 #print('inside loop')
            #                 res['price'] = prc.pgmpricing_countrycost
            #                 res['curr'] = prc.pgmpricing_countrycur.name
            #                 raise StopIteration
            # except StopIteration:
            #     pass
            else:
                # print('no active program')
                res['openstatus'] = 'closed'
            return res
        except Exception as e:
            tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
            _logger.info(tb_ex)
            res['openstatus'] = None
            res['price'] = None
            res['curr'] = None
            return res

    # def offline_submit(self):
    #     """ Offline submit view
    #         @:return: email template """
    #     if self:
    #         print("self")
    #         ir_model_data = self.env['ir.model.data']
    #         try:
    #             compose_form_id = ir_model_data.get_object_reference('mail', 'offline_payment_template')[1]
    #         except ValueError:
    #             compose_form_id = False
    #         for p_registration_rec in self:
    #
    #             countryres = request.env['res.country'].sudo().search([('id', '=', self.country.id)])
    #             program_details = self.fetch_program_inforegform(countryres.code,
    #                                                                       int(p_registration_rec.programapplied.id))
    #
    #             headers2 = {
    #                 "content-type": "application/json",
    #             }
    #             request_url = "http://mokshatwo.sushumna.isha.in/programs/pbk/getprograminfo?nitro_country_code" + countryres.code
    #             try:
    #                 req = requests.get(request_url, headers=headers2)
    #                 req.raise_for_status()
    #                 respone = req.json()
    #                 reg_url = respone.get("pbk_reg_url")
    #                 # passing the upgrade logic and registrationid
    #                 redirect_url = reg_url + "&registration_id=" + str(self.id) + "&offline_logic=true"
    #                 mail_template_id = request.env.ref('isha_tp.tp_mail_program_upgrade_template')
    #                 html_content = mail_template_id.body_html
    #                 # display the amount in email
    #                 html_content = html_content.replace('<a href="link"', "<a href='" + redirect_url + "'")
    #                 mail_template_id.body_html = html_content
    #                 try:
    #                     self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(self.id,
    #                                                                                            force_send=True)
    #                 except Exception as e:
    #                     print(e)
    #
    #             except Exception as e:
    #                 self._raise_query_error(e)

    def upgrade_participant_id(self,reg_records):
        """ Upgrade the program by records """

        for p_registration_rec in reg_records:
            goy_idvar = self.env['tp.program.schedule'].sudo().search([('pgmschedule_programtype', '=', 'In the Grace of Yoga 3-Day Online Program')])[0]
            # schedule = self.env['tp.program.schedule'].search([("id", "!=", p_registration_rec.programapplied.id)])
            unqid = p_registration_rec.sso_id if p_registration_rec.sso_id else ""
            # orderid = unqid[0]+"-"+str(schedule.id)
            check_rec_exit = self.env['tp.program.registration'].sudo().search([("sso_id","=",unqid),("programapplied","=",goy_idvar.id)],limit=1)
            if check_rec_exit:
                if check_rec_exit.registration_status=="payment_pending"  or check_rec_exit.registration_status=="Paid" or  check_rec_exit.registration_status=="payment_inprogress":
                    message_id = self.env['message.wizard'].create(
                        {'message': _(
                            "Already Registration is done for this program " + p_registration_rec.participant_email +"and payment is also done")})
                    return {
                        'name': _('Successfull'),
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'message.wizard',
                        # pass the id
                        'res_id': message_id.id,
                        'target': 'new'
                    }
                else:
                    upgraded_rec = check_rec_exit[0]
            else:
            # Upgrade the registration from higher one
                upgraded_rec = self.with_context(upgrade_program_id=p_registration_rec.id).sudo().copy()
                upgraded_rec.write({"programapplied": goy_idvar.id, "registration_status": "Pending"})
            countryres = request.env['res.country'].sudo().search([('id', '=', upgraded_rec.ip_country.id)])
            program_details = self.fetch_program_inforegform(countryres.code, int(upgraded_rec.programapplied.id))
            previous_program_details = self.fetch_program_inforegform(countryres.code, int(p_registration_rec.programapplied.id))
            headers2 = {
                "content-type": "application/json",
            }
            upgraded_rec.write({"previous_programapplied": p_registration_rec.programapplied.id})
            baserequrl = self.env['ir.config_parameter'].sudo().get_param('tp.isoapiurlpath')
            request_url =baserequrl+countryres.code
            try:
                req = requests.get(request_url, headers=headers2)
                req.raise_for_status()
                respone = req.json()
                # reg_url = respone.get("pbk_reg_url").replace("https://mokshatwo.sushumna.isha.in/",base_url+"/")
                reg_url = respone.get("goy_reg_url")
                current_program_amount = program_details['price'] - previous_program_details['price']
                previous_program_amount = previous_program_details['price']
                # passing the upgrade logic and registrationid

                redirect_url = reg_url+"&registration_id="+str(upgraded_rec.id)+"&upgrade_logic=true&amount="+str(current_program_amount)
                orderidvariable =""
                if upgraded_rec.sso_id:
                    orderidvariable = upgraded_rec.sso_id + "-" + str(goy_idvar.id)
                upgraded_rec.write({"programapplied": goy_idvar.id, "registration_status": "Pending", "orderid": orderidvariable, "created_by":redirect_url, "previous_programapplied": p_registration_rec.programapplied.id, "previous_upgraded_fromprogreg":p_registration_rec.id})
                try:
                    upgradecurrencyvar = program_details['curr'] if program_details['curr'] else ""
                    upgraded_rec.write({"registration_amount":str(current_program_amount),"registration_currency":upgradecurrencyvar})
                except:
                    pass
                mail_template_id = request.env.ref('isha_tp.tp_mail_program_upgrade_template')
                # html_content = mail_template_id.body_html

                # display the amount in email
                # html_content = html_content.replace('<a href="link"', "<a href='" + redirect_url + "'")
                # html_content = html_content.replace('previous_amount', str(previous_program_amount))
                # html_content = html_content.replace('current_amount', str(current_program_amount))
                # mail_template_id.body_html = html_content
                try:
                    self.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(upgraded_rec.id,
                                                                                           force_send=True)

                    message_id = self.env['message.wizard'].create(
                            {'message': _("Upgrade email has been sent to the participant "+p_registration_rec.participant_email)})
                    return {
                            'name': _('Successfull'),
                            'type': 'ir.actions.act_window',
                            'view_mode': 'form',
                            'res_model': 'message.wizard',
                            # pass the id
                            'res_id': message_id.id,
                            'target': 'new'
                        }
                except Exception as e:
                    message_id = self.env['message.wizard'].create(
                        {'message': _(
                            e)})
                    return {
                        'name': _('Successfull'),
                        'type': 'ir.actions.act_window',
                        'view_mode': 'form',
                        'res_model': 'message.wizard',
                        # pass the id
                        'res_id': message_id.id,
                        'target': 'new'
                    }
                    print(e)

            except Exception as e:
                print(e)
                message_id = self.env['message.wizard'].create(
                    {'message': _(
                        e)})
                return {
                    'name': _('Successfull'),
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'message.wizard',
                    # pass the id
                    'res_id': message_id.id,
                    'target': 'new'
                }
                self._raise_query_error(e)


    def cancel_participant_id(self,reg_records):
        for p_registration_rec in reg_records:
            p_registration_rec.write({'registration_status': 'Cancel Applied'})
            message_id = self.env['message.wizard'].create(
                {'message': _("Registration Canceled Successfully")})
            return {
                'name': _('Warning'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'message.wizard',
                # pass the id
                'res_id': message_id.id,
                'target': 'new'
            }

    def cancel_participant(self):
        active_ids = self._context.get('active_ids')
        if (len(active_ids) > 1):
            message_id = self.env['message.wizard'].create(
                {'message': _("You can cancel only one program at a time")})
            return {
                'name': _('Warning'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'message.wizard',
                # pass the id
                'res_id': message_id.id,
                'target': 'new'
            }
        elif (len(active_ids) == 1):
            reg_records = self.env['tp.program.registration'].sudo().search(
                [("id", "=", active_ids), ("registration_status", "=", "Paid")])
            failed_count = self.env['tp.program.registration'].sudo().search(
                [("id", "=", active_ids), ("registration_status", "!=", "Paid")])
            if len(failed_count) > 0:
                message_id = self.env['message.wizard'].create(
                    {'message': _("Un Paid registration cannot be Canceled")})
                return {
                    'name': _('Warning'),
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'message.wizard',
                    # pass the id
                    'res_id': message_id.id,
                    'target': 'new'
                }
            message_id = self.env['confirm.wizard'].create(
                {'message': _("Please confirm that you wish to cancel")})
            return {
                'name': _('Confirmation'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'confirm.wizard',
                'context':{'active_ids': int(active_ids[0])},
                # pass the id
                'res_id': message_id.id,
                'target': 'new'
            }

    def upgrade_participant(self):
        """ Upgrade the program"""
        active_ids = self._context.get('active_ids')
        if(len(active_ids)>1):
            message_id = self.env['message.wizard'].create(
                {'message': _("You can Upgrade only one program at a time")})
            return {
                'name': _('Warning'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'message.wizard',
                # pass the id
                'res_id': message_id.id,
                'target': 'new'
            }

        reg_records = self.env['tp.program.registration'].sudo().search(
            [("id", "in", active_ids), ("registration_status", "=", "Paid")])
        failed_count = self.env['tp.program.registration'].sudo().search(
            [("id", "in", active_ids), ("registration_status", "!=", "Paid")])
        if len(failed_count)>0:
            message_id = self.env['message.wizard'].create(
                {'message': _("Un Paid registration cannot be Upgrade")})
            return {
                'name': _('Warning'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'message.wizard',
                # pass the id
                'res_id': message_id.id,
                'target': 'new'
            }
        goy_id = request.env['tp.program.schedule'].sudo().search([('pgmschedule_programtype', '=',
                                                                    'In the Grace of Yoga 3-Day Online Program')])[0]
        pbk_id = request.env['tp.program.schedule'].sudo().search([('pgmschedule_programtype', '=',
                                                                    'In the Grace of Yoga 1-Day Online Program')])[0]
        for p_registration_rec in reg_records:
            schedule = self.env['tp.program.schedule'].search([("id", "!=", p_registration_rec.programapplied.id)])
            if (int(goy_id.id) == int(p_registration_rec.programapplied.id)):
                message_id = self.env['message.wizard'].create(
                    {'message': _("Upgrade is not applicable for this program 'In the Grace of Yoga 3-Day Online Program'")})
                return {
                    'name': _('Warning'),
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'message.wizard',
                    # pass the id
                    'res_id': message_id.id,
                    'target': 'new'
                }
            else:
                message_id = self.env['confirm.wizard'].create(
                    {'message': _(
                        "Please confirm that you wish to upgrade " + p_registration_rec.participant_email + " from 'In the Grace of Yoga 1-Day Online Program' to 'In the Grace of Yoga 3-Day Online Program'")})

                return {
                    'name': _('Confirmation'),
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'confirm.wizard',
                    'context':{'active_ids': p_registration_rec.id},
                    # pass the id
                    'res_id': message_id.id,
                    'target': 'new'
                }



    @api.returns('self', lambda value: value.id)
    def copy(self, defaults=None):
        if not defaults:
            defaults = {}
        if self.env.context.get('upgrade_program_id'):
            defaults.update({'current_record_id': self.id})
        return super(ProgramRegistration, self).copy(defaults)


    @api.depends('dob')
    def _set_age(self):
        for rec in self:
            if rec.dob != False:
                rec.age = int((datetime.now().date() - self.dob).days / 365.25);
            else:
                rec.age = 0

    # contact details and Residential Address

    countrycode = fields.Char(string='Country Tel code', required=False, index=False)
    phone = fields.Char(string='Mobile Number', required=True, index=True)
    country = fields.Many2one('res.country', string='Country of residence', required=False)
    ip_country = fields.Many2one('res.country', string='IP Country', required=False)
    support_email_id = fields.Char(string='support email',compute='_get_support_email',store=False)
    regional_content = fields.Text(string='Regional Content', compute='_get_regional_content', store=False)
    state = fields.Many2one('res.country.state', string='State/province', required=False)
    pincode = fields.Char(string="Pincode/Zipcode", required=False, index=True)
    address_line = fields.Char(string='Address Line', required=False)
    city = fields.Char(string='City/Town/District', required=False)


    # UTM parameters

    utm_medium = fields.Char(string='UTM Medium')
    utm_campaign = fields.Char(string='UTM Campaign')
    utm_source = fields.Char(string='UTM Source')
    utm_term = fields.Char(string='UTM Term')
    utm_content = fields.Char(string='UTM Content')


    # shipping address

    shipping_country = fields.Many2one('res.country', string='Shipping Country', required=False)
    shipping_state = fields.Many2one('res.country.state', string='Shipping State/province', required=False)
    shipping_pincode = fields.Char(string="Shipping Pincode/Zipcode", required=False)
    shipping_address_line1 = fields.Char(string='Shipping Address Line1', required=False)
    shipping_address_line2 = fields.Char(string='Shipping Address Line2', required=False)
    shipping_city = fields.Char(string='Shipping City/Town/District', required=False)


    participant_email = fields.Char(string='Email', size=500, required=False)
    registration_status = fields.Selection(
        [('Pending', 'Pending'), ('Rejected', 'Rejected'), ('Rejection Review', 'Rejection Review'),
         ('Payment Link Sent', 'Payment Link Sent'), ('Add To Waiting List', 'Add To Waiting List'),
         ('Cancel Applied', 'Cancel Applied'), ('Cancel Approved', 'Cancel Approved'), ('Paid', 'Paid'),
         ('Confirmed', 'Confirmed'),  ('Withdrawn', 'Withdrawn'), ('Refunded', 'Refunded'),
         ('Incomplete', 'Incomplete'),('payment_pending', 'Payment Pending'),('payment_upgraded', 'Payment Upgraded'),
         ('Enquiry', 'Enquiry'),('payment_failed', 'Payment Failed'),('payment_inprogress', 'Payment Progress'),('payment_initiated', 'Payment Initiated') ],
        string='Registration Status', size=100, default='Pending')

    change_seatallocation = fields.Boolean(compute="_compute_seatallocation")

    attendance_status = fields.Selection([('Present', 'Present'), ('Absent', 'Absent'), ('Not Marked', 'Not Marked')],
                                         string='Attendance')

    # Program and package details

    programapplied = fields.Many2one('tp.program.schedule', string='Program Selected')
    previous_upgraded_fromprogreg = fields.Integer(string="Previous upgraded from Registration ID", required=False)
    previous_programapplied = fields.Many2one('tp.program.schedule', string='Previous Program Selected')
    language_preferred = fields.Many2one('res.lang',string="Preferred Language")
    # , domain=[('tp.program.schedule.pgmschedule_programtype', '=', programapplied.pgmschedule_programtype)])
    # language_preferred = fields.Selection(
    #     [('English', 'Enlgish'), ('Tamil', 'Tamil'), ('Hindi', 'Hindi'), ('Malayalam', 'Malayalam'),
    #      ('Telugu', 'Telugu'), ('Kannada', 'Kannada')], string='Preferred Program Language', required=True)

    show_change_pgm = fields.Boolean(compute="_compute_programdateflag")

    compute_field = fields.Boolean(string="check field", compute='get_user')


    #   Program status
    program_status = fields.Selection([('Started', 'Started'), ('Stage1', 'Stage1'), ('Stage2', 'Stage2'), ('Stage3', 'Stage3'), ('Stage4', 'Stage4'), ('Completed', 'Completed')], string="Program Status", required=False)
    sadhana_video_sts = fields.Selection([('Started', 'Started'), ('PartiallyWatched', 'PartiallyWatched'), ('Completed', 'Completed')],string="Sadhana Video Status", required=False)
    orientation_video_sts = fields.Selection([('Started', 'Started'), ('PartiallyWatched', 'PartiallyWatched'), ('Completed', 'Completed')],string="Orientation Video Status", required=False)
    courier_received_ack_date = fields.Date(string="Courier Received Ack Date", required=False)
    courier_tracking_no = fields.Char(string="Courier Tracking No", required=False)
    user_ack_courier_no = fields.Char(string="User Ack Courier No", required=False)
    kit_requirements = fields.Char(string="Kit requirements", required=False)

    # amount_paid = fields.Integer(string="Amount Paid", required=False)
    ereceipt_number = fields.Char(string="Ereceipt Number", required=False)
    online_ereceipt_number = fields.Char(string="Online Ereceipt Number", required=False)
    online_transaction_id = fields.Char(string="Online Transaction Id", required=False)
    created_by = fields.Char(string="Created By", required=False)

    @api.depends('compute_field')
    def get_user(self):
        retval = True;
        try:
            res_user = self.env['res.users'].search([('id', '=', self._uid)])
            if res_user.has_group('isha_tp.group_tp_backofficeuser'):
                retval = False
            else:
                retval = True
        except Exception as e:
            retval = True
            _logger.debug(error)
        finally:
            self.compute_field = retval

    participant_programtype = fields.Many2one(related='programapplied.pgmschedule_programtype')
    selected_package = fields.Many2one('tp.package.parameters', string='Requested Package')

    seats_available = fields.Char(string="Seats Available", default='No', store=True)
    oco_status = fields.Char(string="OCO Status", default='Pending')
    oco_id = fields.Integer(string="OCO Id")

    # other status fields
    incompleteregistration_pagescompleted = fields.Integer(string='Registration pages completed', default=1)
    incompleteregistration_email_send = fields.Boolean(string="Incomplete Registration Email Send", default=False)
    incompleteregistration_sms_send = fields.Boolean(string="Incomplete Registration SMS Send", default=False)
    registration_email_send = fields.Boolean(string="Registration Email Send", default=False)
    registration_sms_send = fields.Boolean(string="Registration SMS Send", default=False)
    rejection_email_send = fields.Boolean(string="Rejection Email Send", default=False)
    rejection_sms_send = fields.Boolean(string="Rejection SMS Send", default=False)
    cancel_email_send = fields.Boolean(string="cancel_email_send")
    access_token = fields.Char('Access Token', default=lambda self: self._get_default_access_token(), copy=False)
    public_url = fields.Char("Public link", compute="_compute_call_public_url")
    #
    addtowaitlist_email_send = fields.Boolean(string="Add To WaitList Email Send", default=False)
    addtowaitlist_sms_send = fields.Boolean(string="Add To WaitList SMS Send", default=False)
    waitinglist_date = fields.Date()
    waitinglist_email_count = fields.Integer(default=0)
    waitinglist_period_inmonth = fields.Integer(default=0)
    waitinglist_last_communicate_sch = fields.Many2one('tp.program.schedule')
    waitinglist_last_communicate_dt = fields.Date(string="Future Programs Last Comm Date")
    waitinglist_email_send = fields.Boolean(string="Future Programs Email Send", default=False)
    waitinglist_sms_send = fields.Boolean(string="Future Programs SMS Send", default=False)
    #
    paymentlink_email_send = fields.Boolean(string="Payment Link Email Send", default=False)
    paymentlink_sms_send = fields.Boolean(string="Payment Link SMS Send", default=False)
    #
    registration_agreed = fields.Boolean(string="I Agree", default=False)
    # amount_paid  = fields.Float(string = 'Amount Paid')
    # date_paid =  fields.Date()
    date_confirm = fields.Date()

    #
    date_withdrawn = fields.Date()
    date_cancelapproved = fields.Datetime()
    refund_eligible_ondaysbefore = fields.Integer(string='Days Before')
    refund_eligible_onpercentage = fields.Float(string='Eligible Percentage')
    refund_eligible = fields.Float(string='Refund Amount Eligible')

    #
    date_refund = fields.Datetime(string="Refund Done On")
    refund_amount = fields.Float(string='Actual Refunded Amount')
    refund_comments = fields.Text(string='Comments')

    contact_id_fkey = fields.Many2one(comodel_name='res.partner', string='Contact')

    confirmation_email_send = fields.Boolean(string="Confirmation Email Send", default=False)
    confirmation_sms_send = fields.Boolean(string="Confirmation SMS Send", default=False)

    pushedtocico = fields.Boolean(string="Pushed to CICO", default=False)
    pushedtosaso = fields.Text(string="Pushed to Sadhana portal", default=False)

    last_modified_dt = fields.Datetime(string="Last Modified DT")
    checkindatetime = fields.Datetime(string="Check-in Date Time from CICO")

    cancelapplied_email_send = fields.Boolean(string="Cancellation Applied Email Send", default=False)
    cancelapproved_email_send = fields.Boolean(string="Cancellation Approved Email Send", default=False)
    cancelrefund_email_send = fields.Boolean(string="Cancellation Refunded Email Send", default=False)

    cancelapplied_sms_send = fields.Boolean(string="Cancellation Applied Email Send", default=False)
    cancelapproved_sms_send = fields.Boolean(string="Cancellation Approved Email Send", default=False)
    cancelrefund_sms_send = fields.Boolean(string="Cancellation Refunded Email Send", default=False)

    # payment info
    orderid = fields.Char(string='Order ID', size=80)
    billinginfoconfirmation = fields.Boolean(string="Billing Info Confirmation", default=False)
    billing_name = fields.Char(string='Billing Name', size=60)
    billing_address = fields.Char(string='Billing Address', size=150)
    billing_city = fields.Char(string='Billing City', size=30)
    billing_state = fields.Char(string='Billing State', size=30)
    billing_zip = fields.Char(string='Billing Zip', size=15)
    billing_country = fields.Char(string='Billing Country', size=50)
    billing_tel = fields.Char(string='Billing Tel', size=20)
    billing_email = fields.Char(string='Billing Email', size=70)
    payment_status = fields.Char(string='Payment Status', size=70)
    total_paid = fields.Float(string='Total Amount Paid', default=0)
    amount_paid = fields.Float(string='Amount Paid', default=0)
    date_paid = fields.Datetime()
    ereceiptgenerated = fields.Boolean(string="eReceipt Generated", default=False)
    ereceiptreference = fields.Char(string="eReceipt Reference", size=50)
    ereceipturl = fields.Char(string="eReceipt URL", size=500)
    paymenttransactions = fields.One2many('tp.participant.paymenttransaction', 'programregistration',
                                          string="Payment Transaction", required=False)
    payment_initiated_dt = fields.Datetime()

    transferred_amount = fields.Float(string="COP Transfered Amount")

    cancelapplied_emailsms_pending = fields.Boolean(default=False)
    active = fields.Boolean(string='active', default=True)
    last_email_sent_status = fields.Char(string='Last Email sent')
    last_email_sent_ts = fields.Datetime(string='Last Email sent timestamp')
    sso_id = fields.Char(string="SSO Id", required=False, index=True)
    othertransactions = fields.One2many('tp.program.registration.transactions', 'programregistration',
                                        string="Transactions", required=False)
    is_checkin_completed = fields.Boolean(string='Check-In Completed')
    time_zone = fields.Char(string='Time Zone')
    stream_lang = fields.Char(string='Stream Language')

    @api.model
    def fields_get(self, fields=None):
        fields_to_show = ['first_name', 'last_name', 'phone', 'country', 'state', 'pincode', 'city',
                          'programapplied', 'participant_email', 'oco_id', 'access_token', 'pushedtocico',
                          'checkindatetime', 'ereceiptgenerated', 'ereceiptreference', 'ereceipturl',
                          'registration_status', 'id','is_checkin_completed','time_zone','stream_lang']
        fields_to_export = ['first_name', 'last_name', 'id','courier_tracking_no']
        res = super(ProgramRegistration, self).fields_get()
        for field in res:
            if self.env.user.has_group('isha_tp.group_tp_lp'):
                if (field not in fields_to_export):
                    res[field]['exportable'] = False
            if (field in fields_to_show):
                res[field]['selectable'] = True
                res[field]['sortable'] = True
            else:
                res[field]['selectable'] = False
                res[field]['sortable'] = False
        return res

    @api.onchange('registration_status')
    def _compute_seatallocation(self):
        # print('Inside method')
        for rec in self:
            # print(rec.programapplied.exists())
            # print(rec.programapplied.pgmschedule_enddate)
            # print(fields.Date.today())
            # print(rec.shipping_country)
            if (rec.programapplied.exists()):
                if ( rec.programapplied.pgmschedule_enddate > fields.Date.today() and
                     rec.registration_status == 'Paid'):
                    rec.change_seatallocation = True
                    for pkg in rec.programapplied.pgmschedule_roomdata:
                        for ctry in pkg.pgmschedule_country:

                            print(ctry.name)
                            print(' --- country check')
                            print(rec.ip_country.name)
                            if (rec.ip_country.name == ctry.name):
                                print('country_matched')
                                paid_seats = 0

                                # to use emergency quota
                                #request.env['tp.payments']._updateSeat(pkg)

                                sel_pack = self.env['tp.program.schedule.roomdata'].browse(pkg.id)
                                print('fetched sell pack info')
                                if sel_pack:
                                    if (rec.programapplied.use_emrg_quota):
                                        paid_seats = sel_pack.pgmschedule_emergencyseatspaid
                                    else:
                                        paid_seats = sel_pack.pgmschedule_regularseatspaid

                                paid_seats = paid_seats + 1
                                print(paid_seats)

                                # package update
                                if (rec.programapplied.use_emrg_quota):
                                    sel_pack.write({'pgmschedule_emergencyseatspaid': paid_seats})
                                else:
                                    sel_pack.write({'pgmschedule_regularseatspaid': paid_seats})

                                rec.change_seatallocation = True
                                return

                    country = self.env['res.country'].search([('code', '=', rec.ip_country.code)])
                    region_name = country.center_id.region_id.name

                    # region_obj = self.env['isha.region'].search([('id', '=', country.center_id.region_id)])


                    for pkg in rec.programapplied.pgmschedule_roomdata:
                        for rgn in pkg.pgmschedule_region:
                            print(rgn.name)
                            print(' --- shipping region name')
                            print('region_name')
                            if (rgn.name == region_name):
                                print('regions_matched')
                                paid_seats = 0
                               # request.env['tp.payments']._updateSeat(pkg)

                                sel_pack = self.env['tp.program.schedule.roomdata'].browse(pkg.id)
                                if sel_pack:
                                    if (rec.programapplied.use_emrg_quota):
                                        paid_seats = sel_pack.pgmschedule_emergencyseatspaid
                                    else:
                                        paid_seats = sel_pack.pgmschedule_regularseatspaid

                                paid_seats = paid_seats + 1

                                # package update
                                if (rec.programapplied.use_emrg_quota):
                                    sel_pack.write({'pgmschedule_emergencyseatspaid': paid_seats})
                                else:
                                    sel_pack.write({'pgmschedule_regularseatspaid': paid_seats})

                                rec.change_seatallocation = True
                                return

                    # request.env['tp.payments']._updateSeat(rec)

                elif ( rec.programapplied.pgmschedule_enddate > fields.Date.today() and
                       (rec.registration_status == 'Withdrawn' or
                        rec.registration_status == 'Cancel Approved' or
                        rec.registration_status == 'Refunded' or
                        rec.registration_status == 'Rejected' )):

                    rec.change_seatallocation = True

                    for pkg in rec.programapplied.pgmschedule_roomdata:
                        for ctry in pkg.pgmschedule_country:

                            print(ctry.name)
                            print(' --- country check')
                            print(rec.ip_country.name)
                            if (rec.ip_country.name == ctry.name):
                                print('country_matched')
                                paid_seats = 0

                                # to use emergency quota
                                #request.env['tp.payments']._updateSeat(pkg)

                                sel_pack = self.env['tp.program.schedule.roomdata'].browse(pkg.id)
                                print('fetched sell pack info')
                                if sel_pack:
                                    if (rec.programapplied.use_emrg_quota):
                                        paid_seats = sel_pack.pgmschedule_emergencyseatspaid
                                    else:
                                        paid_seats = sel_pack.pgmschedule_regularseatspaid

                                if paid_seats > 0:
                                    paid_seats = paid_seats - 1
                                    print(paid_seats)

                                # package update
                                if (rec.programapplied.use_emrg_quota):
                                    sel_pack.write({'pgmschedule_emergencyseatspaid': paid_seats})
                                else:
                                    sel_pack.write({'pgmschedule_regularseatspaid': paid_seats})

                                rec.change_seatallocation = True
                                return

                    country = self.env['res.country'].search([('code', '=', rec.ip_country.code)])
                    region_name = country.center_id.region_id.name

                    # region_obj = self.env['isha.region'].search([('id', '=', country.center_id.region_id)])

                    for pkg in rec.programapplied.pgmschedule_roomdata:
                        for rgn in pkg.pgmschedule_region:
                            print(rgn.name)
                            print(' --- shipping region name')
                            print('region_name')
                            if (rgn.name == region_name):
                                print('regions_matched')
                                paid_seats = 0
                               # request.env['tp.payments']._updateSeat(pkg)

                                sel_pack = self.env['tp.program.schedule.roomdata'].browse(pkg.id)
                                if sel_pack:
                                    if (rec.programapplied.use_emrg_quota):
                                        paid_seats = sel_pack.pgmschedule_emergencyseatspaid
                                    else:
                                        paid_seats = sel_pack.pgmschedule_regularseatspaid

                                if paid_seats > 0:
                                    paid_seats = paid_seats - 1

                                # package update
                                if (rec.programapplied.use_emrg_quota):
                                    sel_pack.write({'pgmschedule_emergencyseatspaid': paid_seats})
                                else:
                                    sel_pack.write({'pgmschedule_regularseatspaid': paid_seats})

                                rec.change_seatallocation = True
                                return

                else:
                    print('Inside else1')
                    rec.change_seatallocation = False
            else:
                print('Inside else2')
                rec.change_seatallocation = False


    @api.depends('programapplied')
    def _compute_programdateflag(self):
        for rec in self:
            rec.show_change_pgm = False
            if rec.create_date == False:
                rec.show_change_pgm = False
            else:
                if (rec.programapplied.exists()):
                    if (rec.programapplied.pgmschedule_startdate < fields.Date.today() or
                        rec.registration_status == 'Withdrawn' or
                        rec.registration_status == 'Cancel Applied' or
                        rec.registration_status == 'Cancel Approved' or
                        rec.registration_status == 'Refunded' or
                        rec.registration_status == 'Rejected' or
                        rec.registration_status == 'Incomplete' or
                        rec.registration_status == 'Change Of Participant' or
                        rec.payment_status == 'Initiated'):
                        rec.show_change_pgm = False
                    else:
                        rec.show_change_pgm = True
                else:
                    rec.show_change_pgm = False

    def _get_support_email(self):
        for rec in self:
            if rec.ip_country:
                email = None
                region_name = rec.ip_country.center_id.region_id.name
                contact_info = request.env['tp.program.region.contacts'].sudo().search(
                    [('region_name', '=', region_name)])
                if contact_info:
                    for x in contact_info:
                        if x.key == 'email':
                            email = x.value
                            break
                rec.support_email_id = email
            else:
                rec.support_email_id = None

    def _get_regional_content(self):
        for rec in self:
            if rec.ip_country:
                content = None
                region_name = rec.ip_country.center_id.region_id.name
                contact_info = request.env['tp.program.region.contacts'].sudo().search(
                    [('region_name', '=', region_name)])
                if contact_info:
                    for x in contact_info:
                        if x.key == 'thankyou_addtional_text':
                            content = x.value
                            break
                rec.regional_content = content
            else:
                rec.regional_content = None

    #
    def _compute_call_public_url(self):
        """ Computes a public URL  """
        base_url = self.env['ir.config_parameter'].sudo().get_param('tp.ccavcallbackurl')
        for rec in self:
            rec.public_url = urls.url_join(base_url, "tp/registration/%s" % (rec.access_token))

    # def callReportRegistration(self):
    #    return self.env.ref('isha_tp.report_registration').report_action(self)

    def open_changeprogramdate(self):
        print('Open Change Program')
        print(self.id)
        if (self.payment_status == 'Initiated'):
            raise exceptions.ValidationError(
                'Payment have been initiated by the participant. Please try after some time')
        else:
            return {
                'name': 'Change Program Date',
                'context': {'default_active_id': self.id, 'default_participant_name': self.first_name,
                            'default_programapplied': self.programapplied.id,
                            'default_participant_programtype': self.participant_programtype.id},
                'view_type': 'form',
                'res_model': 'tp.program.registration.changeprogramdate',
                'view_id': False,
                'view_mode': 'form',
                'type': 'ir.actions.act_window',
                'target': 'new'
            }

    def load_resend_mail(self):
        print('call method')
        return {
            'name': 'Resend Email/SMS To Participant',
            'context': {'default_active_id': self.id, 'default_participant_name': self.first_name},
            'view_type': 'form',
            'res_model': 'tp.program.registration.resendmail',
            'view_id': False,
            'view_mode': 'form',
            'type': 'ir.actions.act_window',
            'target': 'new'
        }

    def get_contact_dict(self, vals):

        return {
            'name': vals['first_name'] + ' ' + vals['last_name'],
            'phone_country_code': vals['countrycode'] if 'countrycode' in vals else None,
            'phone': vals['phone'] if 'phone' in vals else None,
            'email': vals['participant_email'] if 'participant_email' in vals else None,
            'gender': vals['gender'] if 'gender' in vals else None,
            'dob': vals['dob'] if 'dob' in vals else None,
            'street': vals['address_line'] if 'address_line' in vals else None,
            'city': vals['city'] if 'city' in vals else None,
            'state_id': vals['state'] if 'state' in vals else None,
            'state': self.env['res.country.state'].sudo().browse(vals['state']).name if 'state' in vals and vals[
                'state'] else None,
            'zip': vals['pincode'] if 'pincode' in vals else None,
            'country_id': vals['country'] if 'country' in vals else None,
            'country': self.env['res.country'].sudo().browse(vals['country']).code if 'country' in vals and vals[
                'country'] else None,
        }


    def load_print(self):
        self.sendPaymentLinkEmail()

    def load_print1(self):
        _logger.info('************************ load print is called ************************ ')

        '''rec = self.env['tp.program.registration'].sudo().search([('id','=',5)])
        source = self.env['tp.program.registration'].sudo().search([('id','=',4)])

        refund_data = self.env['tp.payments']._calculateRefundAmount(source, datetime.now())

        print(refund_data)'''

        list = self.env['tp.program.registration'].sudo().search([('id', '=', 4)])

        for rec in list:
            rec.write({'registration_status': 'Confirmed',
                       'paymentlink_email_send': False,
                       'paymentlink_sms_send': False
                       })

        '''rec.write({'ereceiptgenerated': False})
        rec.write({'registration_status': 'Confirmed'})
        rec.write({'amount_paid': 10})
        rec.write({ 'transferred_amount': 0, 'payment_status': 'Success', 'ha_status': 'HA Approved'})
        '''
        # rec.write({'registration_status': 'Confirmed', 'payment_status': 'Success', 'pushedtocico': True })
        # 'amount_paid': 100, 'date_paid': datetime.now()})
        # rec.write({'registration_status': 'Change Of Participant'})
        # rec.write({ 'ischangeofparticipant': True,
        #             'changeofparticipant_applied_dt': datetime.now(),
        #             'changeofparticipant_ref': 1 })
        # rec.write({'payment_status': ''})
        # return self.env.ref('isha_tp.report_registration').report_action(self)

    def call_all_jobs(self):
        for rec in self:
            print(rec.public_url)
        self.checkPaymentStatusForPendingRecords()


    def changetrigger(self):
        print('trigger')

    #
    def checkPaymentStatusForPendingRecords(self):
        self.env['tp.paymentstatus'].sudo()._checkPendingPaymentStatus()

    #

    def checkAndGenerateEreceipt(self):
        self.env['tp.paymentstatus'].sudo()._checkAndGenerateEreceipt()

    #

    #
    def updateAndSendProgramConfirmation(self):
        self.env['tp.notification'].sudo()._updateAndSendProgramConfirmation()

    def sendCancelAppliedConfirmation(self):
        self.env['tp.notification'].sudo()._sendCancelAppliedConfirmation()

    #
    def statusUpdateandSendPaymentLinkEmail(self):
        foundrec = 0
        rejlist = self.env['tp.program.registration'].sudo().search(
            [ ('registration_status', '=', 'Pending')])
        print(rejlist)
        for rec in rejlist:
            foundrec = 1
            try:
                self.statusUpdateandSendPaymentLinkEmailCore(rec)
            except Exception as e:
                print(e)

        if (foundrec == 1):
            print('registration status update completed, calling email and sms program')
        else:
            print('no records found to update registration status, calling email and sms program')
        self.sendPaymentLinkEmail()

    #

    def sendPaymentLinkEmail(self):
        foundrec = 0
        rejlist = self.env['tp.program.registration'].sudo().search([('registration_status', '=', 'Approved'),
                                                                     '|', ('paymentlink_email_send', '=', False),
                                                                     ('paymentlink_sms_send', '=', False),
                                                                     ('country.name', '=', 'India')])
        print(rejlist)
        for rec in rejlist:
            foundrec = 1
            self.env['tp.notification'].sudo()._sendPaymentLinkEmailCore(rec)
        if foundrec == 1:
            print('paymentlink: sending email and sms completed for all request')
        else:
            print('paymentlink: no records found for sending email and sms..')

    #
    def sendRegistrationEmail(self):
        foundrec = 0
        rejlist = self.env['tp.program.registration'].sudo().search([('registration_status', '=', 'Pending'),
                                                                     '|', ('registration_email_send', '=', False),
                                                                     ('registration_sms_send', '=', False),
                                                                     ('country.name', '=', 'India')])
        print(rejlist)
        for rec in rejlist:
            foundrec = 1
            self.env['tp.notification'].sudo()._sendRegistrationEmailSMSCore(rec)
        if foundrec == 1:
            print('registrations: sending email and sms completed for all request')
        else:
            print('registrations: no records found for sending email and sms..')

    #
    def sendPartialRegistrationEmail(self):
        foundrec = 0
        rejlist = self.env['tp.program.registration'].sudo().search([('registration_status', '=', 'Incomplete'),
                                                                     '|',
                                                                     ('incompleteregistration_email_send', '=', False),
                                                                     ('incompleteregistration_sms_send', '=', False),
                                                                     ('country.name', '=', 'India')])
        print(rejlist)
        _logger.info("Inside sendPartialRegistrationEmail")
        _logger.info(str(rejlist))
        for rec in rejlist:
            foundrec = 1
            self.env['tp.notification'].sudo()._sendRegistrationPartiallyCompletedFormEmailSMS(rec)
        if foundrec == 1:
            print('partial registrations: sending email and sms completed for all request')
            _logger.info('partial registrations: sending email and sms completed for all request')
        else:
            print('partial registrations: no records found for sending email and sms..')
            _logger.info('partial registrations: no records found for sending email and sms..')

    def sendWaitingListScheduledProgram(self):
        self.env['tp.notification'].sudo()._sendWaitingListScheduledProgram()

    #
    def sendRegistrationRejectionEmail(self):
        foundrec = 0
        rejlist = self.env['tp.program.registration'].sudo().search([('registration_status', '=', 'Rejected'),
                                                                     '|', ('rejection_email_send', '=', False),
                                                                     ('rejection_sms_send', '=', False),
                                                                     ('country.name', '=', 'India')])
        print(rejlist)
        for rec in rejlist:
            foundrec = 1
            self.env['tp.notification'].sudo()._sendRegistrationRejectionEmailCore(rec)
        if foundrec == 1:
            print('rejections: sending email and sms completed for all request')
        else:
            print('rejections: no records found for sending email and sms..')



    def onchange_package(self, val):
        print('package')
        print(self.id)

    def getProgramAttendance(self, contact_id, pgm_category=None):
        pgm_recs = self.env['program.attendance'].getProgramAttendance(contact_id, pgm_category)
        print("pgm-recs", pgm_recs)
        return pgm_recs

    def populate_contact_id(self,limit):
        contact_model = self.env['res.partner'].sudo()
        recs = self.sudo().search([('contact_id_fkey','=',False)],limit=limit)
        for rec in recs:
            source_rec = {
                'system_id':42,
                'local_contact_id':rec.id,
                'name': ' '.join(filter(None,[rec.first_name,rec.last_name])),
                'gender':isha_crm.ContactProcessor.getgender(rec.gender),
                'dob':str(rec.dob) if rec.dob else None,
                'phone_country_code':rec.countrycode,
                'phone':rec.phone,
                'email':rec.participant_email,
                'street':rec.address_line,
                'city':rec.city,
                'state_id':rec.state.id,
                'state':rec.state.name,
                'zip':rec.pincode,
                'country_id':rec.country.id,
                'country':rec.country.code,
                'sso_id':rec.sso_id
            }
            if rec.registration_status == 'Paid':
                source_rec.update({'goy_reg': True})
            contact = contact_model.create(source_rec)
            rec.contact_id_fkey = contact[0].id
            self.env.cr.commit()
            _logger.info('isha_tp -> '+str(rec.id)+' contact assignment')




class MasterLob(models.Model):
    _name = 'tp.master.lob'
    _description = "Master list of line of businesses"
    _rec_name = 'lobname'

    lobname = fields.Char(string='Lob Name', required=False)


class PaymentTransaction(models.Model):
    _name = "tp.participant.paymenttransaction"
    _description = "Temple Program Participant Payment Transaction"

    programregistration = fields.Many2one('tp.program.registration', string='program registration')
    first_name = fields.Char(string='first_name')
    last_name = fields.Char(string='last_name')
    email = fields.Char(string='email')
    programtype = fields.Char(string='programtype')
    programname = fields.Char(string='programname')
    # ereceiptgenerated = fields.Boolean(related="programregistration.ereceiptgenerated",store=False)
    # ereceiptreference = fields.Char(related="programregistration.ereceiptreference",store=False)
    # ereceipturl = fields.Char(related="programregistration.ereceipturl",store=False)
    ereceiptgenerated = fields.Boolean(default=False)
    ereceiptreference = fields.Char(string='Receipt No', size=50)
    ereceipturl = fields.Char(string="eReceipt URL", size=700)
    ereceiptgenerated_dt = fields.Datetime(string="Receipt No Raised Date")
    orderid = fields.Char(string='Transaction ID', size=150)
    billing_name = fields.Char(string='Participant Name', size=60)
    billing_address = fields.Char(string='Address', size=150)
    billing_city = fields.Char(string='City', size=30)
    billing_state = fields.Char(string='State', size=30)
    billing_zip = fields.Char(string='Zip', size=15)
    billing_country = fields.Char(string='Country', size=50)
    billing_tel = fields.Char(string='Mobile Number', size=20)
    billing_email = fields.Char(string='Email', size=70)
    payment_status = fields.Char(string='Payment Status', size=150)
    amount = fields.Float(string='Amount')
    comments = fields.Char(string='Comments', size=200)
    transactiontype = fields.Char(string='Transaction Type', size=50)
    paymenttrackingid = fields.Char(string='PaymentTracking', size=200, default='0')
    transaction_dt = fields.Datetime(string="Transaction Date", compute="_compute_transaction_dt", store=True)
    ebook = fields.Char(string='E-Book', compute="_compute_ebook", store=True)
    status = fields.Char(string='Status', compute="_compute_status", store=True)
    program_purpose = fields.Char(string='Purpose_StatDate_EndDate_ProgramPlace', compute="_compute_programpurpose",
                                  store=True)
    gateway = fields.Char(string='Gateway', compute="_compute_gateway", store=True)
    billing_tel1 = fields.Char(string='Phone No1', size=20)
    billing_tel2 = fields.Char(string='Phone No2', size=20)
    billing_pan = fields.Char(string='PAN Number', size=20)
    pre_regid = fields.Char(string='Pre Reg Id', size=20)

    currency = fields.Char(string='Currency', size=10)
    errorcode = fields.Char(string='Error Code', size=100)
    errorreason = fields.Char(string='Error Reason', size=200)
    errormsg = fields.Char(string='Error Msg', size=400)
    addlinfo = fields.Char(string='Additional Info', size=700)
    paymentmethod = fields.Char(string='Payment Method', size=50)
    paymentdata = fields.Char(string='Payment Data', size=50)
    paymentid = fields.Char(string='Payment Id', size=200)
    invoiceid = fields.Char(string='Invoice Id', size=100)


    # nooftrees = fields.Char(string='noOfTrees', size=20)
    # recurringdonations = fields.Char(string='recurringDonation', size=20)

    @api.depends('payment_status')
    def _compute_transaction_dt(self):
        for rec in self:
            if rec.create_date != False:
                rec.transaction_dt = rec.create_date
            else:
                rec.transaction_dt = False

    @api.depends('ereceiptreference')
    def _compute_ebook(self):
        for rec in self:
            if rec.ereceiptreference != None and rec.ereceiptreference != '' and rec.ereceiptreference != False:
                rec.ebook = rec.ereceiptreference[5:8]
            else:
                rec.ebook = ''

    @api.depends('payment_status')
    def _compute_status(self):
        for rec in self:
            if rec.payment_status != None and rec.payment_status != False and rec.payment_status == 'Success':
                rec.status = 'Confirmed'
            else:
                rec.status = ''

    @api.depends('payment_status')
    def _compute_programpurpose(self):
        try:
            for rec in self:
                if rec.payment_status != None and rec.payment_status != False and rec.payment_status == 'Success':
                    rec.program_purpose = rec.programregistration.programapplied.pgmschedule_programname
                else:
                    rec.program_purpose = ''
        except:
            rec.program_purpose = ''

    @api.depends('payment_status')
    def _compute_gateway(self):
        for rec in self:
            if rec.payment_status != None and rec.payment_status != False and rec.payment_status == 'Success':
                rec.gateway = 'ccavenue'
            else:
                rec.gateway = ''

    @api.model
    def fields_get(self, fields=None):
        fields_notto_show = ['programregistration']
        res = super(PaymentTransaction, self).fields_get()
        for field in res:
            if (field in fields_notto_show):
                res[field]['selectable'] = False
                res[field]['sortable'] = False
            else:
                res[field]['selectable'] = True
                res[field]['sortable'] = True

        return res


class RegistrationTransactions(models.Model):
    _name = "tp.program.registration.transactions"
    _description = "Program Registration Transactions"

    programregistration = fields.Many2one('tp.program.registration', string='program registration')
    transactiontype = fields.Char(string='Transaction Type', size=50)
    isactive = fields.Boolean(default=True)
    comments = fields.Char(string='Comments')
    last_modified_dt = fields.Datetime(string="Last Modified DT")


class RegistrationOfflineCode(models.Model):
    _name = "tp.program.registration.offline.code"
    _description = "Program Registration Offline code"

    name = fields.Char(string="name")
    code = fields.Char(string="Code")
