import json
import logging
import traceback

import requests
import werkzeug
import werkzeug.wrappers

from odoo import fields, http
from odoo.http import request, Response
#from .registrationform import hash_url_params

_logger = logging.getLogger(__name__)


class TempleProgramegistration(http.Controller):

	# ------------------------------------------------------------
	# ACCESS
	# ------------------------------------------------------------

	def _get_access_data(self, access_token):

		count = request.env['tp.program.registration'].search_count([('access_token', '=', access_token)])

		if (count != 1):
			return request.render("isha_tp.403")

		access_data = request.env['tp.program.registration'].search([('access_token', '=', access_token)])

		if ((access_data.registration_status == 'Rejected') or
			(access_data.registration_status == 'Withdrawn') or
			(access_data.registration_status == 'Add To Waiting List') or
			(access_data.registration_status == 'Cancel Approved')):
			return request.render("isha_tp.expired")

		return access_data

	# ------------------------------------------------------------
	# TAKING ROUTES
	# ------------------------------------------------------------

	@http.route('/templeprograms/registration/<string:access_token>', type='http', auth='public', website=True,
				csrf=False)
	def registration_start(self, access_token, **post):
		print('inside registration start')
		print(access_token)
		access_data = self._get_access_data(access_token)
		if isinstance(access_data, Response):
			return access_data

		print('calling render')

		mode = 'START'
		selected_package = request.env['tp.program.schedule.roomdata'].browse(access_data.packageselection.id)

		if selected_package:
			if (access_data.use_emrg_quota == True):
				bal_seats = selected_package.pgmschedule_packageemergencyseats - selected_package.pgmschedule_emergencyseatspaid
			else:
				bal_seats = selected_package.pgmschedule_packagenoofseats - selected_package.pgmschedule_regularseatspaid

		newpackages = request.env['tp.program.schedule.roomdata'].search([('pgmschedule_regularseatsbalance', '>', 0), (
		'pgmschedule_programname', '=', access_data.programapplied.id), ('id', '!=', selected_package.id)])
		if (access_data.payment_status == 'Initiated'):
			mode = 'START'
		else:
			if (bal_seats <= 0):
				print('seats not available')
				if newpackages:
					print('Package available')
					mode = 'CHANGE PACKAGE'
				else:
					mode = 'NO PACKAGE'

		termlist = request.env['tp.terms_and_conditions'].search([])
		balanceamount = request.env['tp.payments']._getBalanceAmount(access_data, selected_package)
		return request.render("isha_tp.registration_general", {'object': access_data, 'terms': termlist[0],
															   'countries': request.env['res.country'].search([]),
															   'packages': newpackages,
															   'selectedpack': selected_package, 'mode': mode,
															   'balanceamount': balanceamount})

	@http.route('/templeprograms/registration/initiatepayment/<string:access_token>', type='http', auth='public',
				website=True, csrf=False)
	def registration_initiatepayment(self, access_token, **post):

		print('Terms Submit')
		print(access_token)
		print(self)
		print(post)

		try:
			count = request.env['program.registration'].search_count([('access_token', '=', access_token)])

			if (count != 1):
				return request.render("isha_tp.403")

			access_data = request.env['program.registration'].search([('access_token', '=', access_token)])

			if (access_data.registration_status != 'Payment Link Sent'):
				return request.render("isha_tp.invalidrequest")

			reg_ag = False
			btn_clk = False

			for rec in post:
				print(rec)
				print(post[rec])
				if (rec == 'registration_agreed' and post[rec] == 'true'):
					reg_ag = True
					continue
				elif (rec == 'button_submit' and post[rec] == 'terms'):
					btn_clk = True
					continue

			if (reg_ag == True and btn_clk == True):
				print('submit clicked')
				return request.render("isha_tp.registration_paymentpage",
									  {'object': access_data, 'countries': request.env['res.country'].search([])})

		except Exception as e:
			print(e)
			return request.render("isha_tp.exception")

	@http.route('/templeprograms/registration/confirm/<string:access_token>', type='http', auth='public', website=True,
				csrf=False)
	def registration_confirm_twinroom(self, access_token, **post):
		print('Terms Submit')
		print(access_token)
		print(self)
		print(post)
		try:
			count = request.env['program.registration'].search_count([('access_token', '=', access_token)])

			if (count != 1):
				return request.render("isha_tp.403")

			access_data = request.env['program.registration'].search([('access_token', '=', access_token)])

			if (access_data.registration_status != 'Paid'):
				return request.render("isha_tp.invalidrequest")

			acc_person = 0
			acc_person_text = ''
			acc_email = ''
			btn_clk = False

			for rec in post:
				print(rec)
				print(post[rec])
				if (rec == 'accompany_name'):
					acc_person = post[rec]
					continue
				elif (rec == 'accompny_email'):
					acc_email = post[rec]
					continue
				elif (rec == 'accompany_name_text'):
					acc_person_text = post[rec]
				elif (rec == 'button_add' and post[rec] == 'add'):
					btn_clk = True
					continue

			if (btn_clk == True):
				print('add clicked')
				sel_person = request.env['program.registration'].browse(int(acc_person))
				access_data.write({'accompanying_name': sel_person.id, 'accompanying_email': acc_email,
								   'accompanying_name_text': acc_person_text})
				return request.render("isha_tp.registrationsuccess", {'object': access_data})
			else:
				return request.render("isha_tp.expired")

		except Exception as e:
			print(e)
			return request.render("isha_tp.exception")

	@http.route('/templeprograms/registration/refund/<string:access_token>', type='http', auth='public', website=True,
				csrf=False)
	def registration_cancel_refund(self, access_token, **post):
		print(post)

		try:
			count = request.env['program.registration'].search_count([('access_token', '=', access_token)])

			if (count != 1):
				return request.render("isha_tp.403")

			access_data = request.env['program.registration'].search([('access_token', '=', access_token)])

			if (access_data.registration_status != 'Paid' and access_data.registration_status != 'Confirmed'):
				return request.render("isha_tp.invalidrequest")

			if (post['button_submit'] == 'refund'):
				print('refund requst')
				access_data.write({'registration_status': 'Cancel Applied', 'date_withdrawn': fields.Date.today(),
								   'cancelapplied_emailsms_pending': True})
				return request.render("isha_tp.cancelrefundsuccess")
			else:
				return request.render("isha_tp.invalidrequest")

		except Exception as e:
			print(e)
			return request.render("isha_tp.exception")

	@http.route('/programs/pbk/enquiryform', type='http', auth='public', website=True, csrf=False)
	def tp_inquiry(self, **post):
		print('Inquiry')
		try:
			# availableprograms = request.env['tp.program.schedule'].search([('pgmschedule_registrationopen','=',True),('pgmschedule_startdate','>',fields.Date.today().strftime('%Y-%m-%d'))])
			availableprograms = request.env['tp.program.schedule'].search([('pgmschedule_registrationopen', '=', True)])
			selectedprogram = False
			if availableprograms:
				print(availableprograms)
				return request.render("isha_tp.inquiry_open",
									  {'availableprograms': availableprograms, 'selectedprogram': selectedprogram,
									   'countries': request.env['res.country'].search([])})
			else:
				return request.render("isha_tp.inquiry_open",
									  {'availableprograms': availableprograms, 'selectedprogram': selectedprogram,
									   'countries': request.env['res.country'].search([])})

		except Exception as e:
			tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
			_logger.info(tb_ex)
			return request.render("isha_tp.exception")

	@http.route('/templeprograms/inquirysubmit', type='http', auth='public', website=True, csrf=False)
	def tp_inquirysubmit(self, **post):
		print('inquirysubmit')
		print(post)

		try:
			sel_prg = 0
			btn_clk = False
			btn_chg = False

			first_name = False
			last_name = False
			countrycode = False
			city = False
			country = False
			mobile = False
			email = False
			comments = False

			for rec in post:
				print(rec)
				print(post[rec])
				if (rec == 'selected_prg'):
					sel_prg = post[rec]
					continue
				elif (rec == 'button_submit' and post[rec] == 'submit'):
					btn_clk = True
					continue
				elif (rec == 'button_onchange' and post[rec] == 'change'):
					btn_chg = True
					continue
				elif (rec == 'first_name'):
					first_name = post[rec]
				elif (rec == 'last_name'):
					last_name = post[rec]
				elif (rec == 'countrycode'):
					countrycode = post[rec]
				elif (rec == 'city'):
					city = post[rec]
				elif (rec == 'country'):
					country = post[rec]
				elif (rec == 'mobile'):
					mobile = post[rec]
				elif (rec == 'email'):
					email = post[rec]
				elif (rec == 'coments'):
					comments = post[rec]

			# availableprograms = request.env['tp.program.schedule'].search([('pgmschedule_registrationopen','=',True),('pgmschedule_startdate','>',fields.Date.today().strftime('%Y-%m-%d'))])
			availableprograms = ''#request.env['tp.program.schedule'].search([('pgmschedule_registrationopen', '=', True)])
			# if availableprograms:
			# 	print('Programs available')
			# else:
			# 	return request.render("isha_tp.registrationnopackages")

			if (btn_chg == True):
				print('prg selected  clicked')

				# selected_prg = request.env['tp.program.schedule'].browse(int(sel_prg))
				return request.render("isha_tp.inquiry_open",
									  {'availableprograms': availableprograms, 'selectedprogram': 'selected_prg'})

			elif (btn_clk == True):
				print('prg selected and submit clicked')
				# selected_prg = request.env['tp.program.schedule'].browse(int(sel_prg))
				recs = request.env['tp.inquiry'].sudo().search(
					['&', ('program_preference', '=', sel_prg), ('email', '=', email)])

				# nrec = request.env['tp.inquiry'].create({'first_name':first_name,'last_name':last_name,'countrycode':countrycode,'mobile':mobile,'email':email,'city':city,'country':country,'comments':comments,'program_preference':sel_prg,'inquiry_status':'Inquiry'})
				if (len(recs) == 0):
					nrec = request.env['tp.inquiry'].create(
						{'first_name': first_name, 'last_name': last_name, 'countrycode': countrycode, 'mobile': mobile,
						 'email': email, 'city': city, 'country': country, 'comments': comments,
						 'program_preference': sel_prg, 'inquiry_status': 'Inquiry'})
					return request.render("isha_tp.inquirysuccess")
				else:
					return request.render("isha_tp.recordexists")
			else:
				return request.render("isha_tp.expired")

		except Exception as e:
			# stacktrace=traceback.print_exc()
			# sys.stderr.write(str(stacktrace))
			# print(stacktrace)
			tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
			_logger.info(tb_ex)
			'''request.env['tp.tempdebuglog'].sudo().create({
            'logtext': str(stacktrace)
            })'''
			return request.render("isha_tp.exception")

	@http.route([
		'/programs/pbk/getprograminfomokshaone'
	], auth="public", type='http', methods=['GET'], csrf=False, website=True, cors='*')
	def getpimokshaone(self, **kw):
		# Disabling ip2location becoz of nitrogen caching inconsistency and taking the direct value from the api
		country_code = kw.get('nitro_country_code', 'NULL')
		_logger.info("moksha one goy iso api request getprograminfomokshaone " + str(kw))
		try:
			email = 'graceofyoga.online@ishafoundation.org'
			phno = ''
			country = request.env['res.country'].sudo().search(
				[('code', '=', country_code)])
			region_name = country.center_id.region_id.name

			contact_info = request.env['tp.program.region.contacts'].sudo().search(
				[('region_name', '=', region_name)])

			if contact_info:
				for x in contact_info:
					if x.key == 'email':
						email = x.value
					elif x.key == 'phone':
						phno = x.value

		except Exception as e:
			tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
			_logger.info(tb_ex)
			country = request.env['res.country'].sudo()
			email = None
			phno = None

		try:
			result = {'ipcountry': country_code,
					  'goy_price': None,
					  'pbk_price': None,
					  'currency': None,
					  'goy_status': 'open',
					  'pbk_status': 'open',
					  'email': email,
					  'phone': phno
					  }

			# FIXME: Need to revisit this logic to get the program schedules dynamically. current implementaion assumes there is only 1 active schedule
			goy_id = request.env['tp.program.schedule'].sudo().search([('pgmschedule_programtype', '=',
																		'In the Grace of Yoga 3-Day Online Program')])[0]
			pbk_id = request.env['tp.program.schedule'].sudo().search([('pgmschedule_programtype', '=',
																		'In the Grace of Yoga 1-Day Online Program')])[0]

			goy = fetch_program_info(country_code, goy_id.id)
			pbk = fetch_program_info(country_code, pbk_id.id)
			if goy['curr']:
				currency = goy['curr']
			else:
				currency = pbk['curr']
			result.update({'goy_price': goy['price'],
						   'pbk_price': pbk['price'],
						   'currency': currency
						   })
			if True:
				temp_goy_url = request.env['ir.config_parameter'].sudo().get_param('tp.goyonline_in_url') + str(
					goy_id.id) + "&country=" + str(country.id) + '&checksum=' #+ hash_url_params(str(goy_id.id),str(country.id)) + '&handle=iso'
				temp_pbk_url = request.env['ir.config_parameter'].sudo().get_param('tp.pbkonline_in_url') + str(
					pbk_id.id) + "&country=" + str(country.id) + '&checksum=' #+ hash_url_params(str(pbk_id.id),str(country.id)) + '&handle=iso'
				result.update({
					'ipcountry':country_code,
					'goy_price':goy['price'],
					'pbk_price': pbk['price'],
					'currency':currency,
					'goy_reg_url': temp_goy_url,
					'pbk_reg_url': temp_pbk_url,
					'email':email,
					'phone':phno,
					'goy_status':goy['openstatus'],
					'pbk_status':pbk['openstatus']
				})

			# result.update({
			#	 'goy_reg_url': temp_goy_url,
			#	 'pbk_reg_url': temp_pbk_url
			# })
			_logger.info("moksha one goy iso api result logged")
			_logger.info(str(result))
			return json.dumps(result)
		except Exception as e:
			tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
			_logger.info("error in moksha one goy iso api")
			_logger.info(tb_ex)
			pass

		return json.dumps(result)

	@http.route([
		'/programs/pbk/getprograminfo'
	], auth="public", type='http', methods=['GET'], csrf=False, website=True, cors='*')
	def getprograminfofunction(self, **kw):

		# Disabling ip2location becoz of nitrogen caching inconsistency and taking the direct value from the api
		country_code = kw.get('nitro_country_code', 'NULL')
		try:
			_logger.info("goy iso api input"+str(kw))
		except:
			pass
		# else:
		# 	country_dict = get_ip_country(kw.get('ip', 'IN'))
		# 	country_code = country_dict['country_iso2']

		try:
			isoapiroutetoishangamone = request.env['ir.config_parameter'].sudo().get_param('tp.isoapiroutetoishangamone')
			if country_code == 'IN' and isoapiroutetoishangamone == 'yes':
				return s2_api_transfer_tos1()
			email = 'graceofyoga.online@ishafoundation.org'
			phno = ''
			country = request.env['res.country'].sudo().search(
				[('code', '=', country_code)])
			region_name = country.center_id.region_id.name

			contact_info = request.env['tp.program.region.contacts'].sudo().search(
				[('region_name', '=', region_name)])

			if contact_info:
				for x in contact_info:
					if x.key == 'email':
						email = x.value
					elif x.key == 'phone':
						phno = x.value

		except Exception as e:
			tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
			_logger.info(tb_ex)
			country = request.env['res.country'].sudo()
			email = None
			phno = None

		result = {'ipcountry': country_code,
				  'goy_price': None,
				  'pbk_price': None,
				  'currency': None,
				  'goy_status': 'open',
				  'pbk_status': 'open',
				  'email': email,
				  'phone': phno
				  }


		# FIXME: Need to revisit this logic to get the program schedules dynamically. current implementaion assumes there is only 1 active schedule
		goy_id = request.env['tp.program.schedule'].sudo().search([('pgmschedule_programtype', '=',
																	'In the Grace of Yoga 3-Day Online Program')])[0]
		pbk_id = request.env['tp.program.schedule'].sudo().search([('pgmschedule_programtype', '=',
																	'In the Grace of Yoga 1-Day Online Program')])[0]

		goy = fetch_program_info(country_code, goy_id.id)
		pbk = fetch_program_info(country_code, pbk_id.id)
		if goy['curr']:
			currency = goy['curr']
		else:
			currency = pbk['curr']
		result.update({'goy_price': goy['price'],
					   'pbk_price': pbk['price'],
					   'currency': currency
					   })
		if country_code == 'IN':
			temp_goy_url = request.env['ir.config_parameter'].sudo().get_param('tp.goyonline_in_url')
			temp_pbk_url = request.env['ir.config_parameter'].sudo().get_param('tp.pbkonline_in_url')
			result.update({
				'goy_reg_url': temp_goy_url,
				'pbk_reg_url': temp_pbk_url,
			})
		elif country_code in ['US', 'CA']:
			temp_goy_url = request.env['ir.config_parameter'].sudo().get_param('tp.goyonline_usca_url')
			temp_pbk_url = request.env['ir.config_parameter'].sudo().get_param('tp.pbkonline_usca_url')
			result.update({
				'goy_reg_url': temp_goy_url,
				'pbk_reg_url': temp_pbk_url,
			})
		elif country_code in ['AI', 'AG', 'AR', 'AW', 'BS', 'BB', 'BZ', 'BM', 'BO', 'BQ', 'BV', 'BR', 'CA', 'KY', 'CL',
							  'CO', 'CR', 'CU', 'CW', 'DM', 'DO', 'EC', 'SV', 'FK', 'GF', 'GL', 'GD', 'GP', 'GT', 'GY',
							  'HT', 'HN', 'JM', 'MQ', 'MX', 'MS', 'NI', 'PA', 'PY', 'PE', 'PR', 'BL', 'KN', 'LC', 'MF',
							  'PM', 'VC', 'SX', 'GS', 'SR', 'TT', 'TC', 'US', 'UY', 'VE', 'VG', 'VI']:
			temp_goy_url = request.env['ir.config_parameter'].sudo().get_param('tp.goyonlineothersurl')
			temp_pbk_url = request.env['ir.config_parameter'].sudo().get_param('tp.pbkonlineothersurl')
			result.update({
				'goy_reg_url': temp_goy_url,
				'pbk_reg_url': temp_pbk_url,
			})
		else:
			temp_goy_url = request.env['ir.config_parameter'].sudo().get_param('tp.goyonlineotherselseurl') + str(
				goy_id.id) + "&country=" + str(country.id) + '&checksum=' #+ hash_url_params(str(goy_id.id),str(country.id)) + '&handle=iso'
			temp_pbk_url = request.env['ir.config_parameter'].sudo().get_param('tp.pbkonlineotherselseurl') + str(
				pbk_id.id) + "&country=" + str(country.id) + '&checksum=' #+ hash_url_params(str(pbk_id.id),str(country.id)) + '&handle=iso'
			result.update({
				'ipcountry':country_code,
				'goy_price':goy['price'],
				'pbk_price': pbk['price'],
				'currency':currency,
				'goy_reg_url': temp_goy_url,
				'pbk_reg_url': temp_pbk_url,
				'email':email,
				'phone':phno,
				'goy_status':goy['openstatus'],
				'pbk_status':pbk['openstatus']
			})

		# result.update({
		#	 'goy_reg_url': temp_goy_url,
		#	 'pbk_reg_url': temp_pbk_url
		# })
		try:
			_logger.info("goy iso api response" + str(result))
		except:
			pass
		return json.dumps(result)

	@http.route([
		'/programs/pbk/testroutetosone'
	], auth="public", type='http', methods=['GET'], csrf=False, website=True, cors='*')
	def getprogtestroutetosone(self, **kw):

		# Disabling ip2location becoz of nitrogen caching inconsistency and taking the direct value from the api
		country_code = kw.get('nitro_country_code', 'NULL')
		# else:
		# 	country_dict = get_ip_country(kw.get('ip', 'IN'))
		# 	country_code = country_dict['country_iso2']

		try:
			return s2_api_transfer_tos1()
		except Exception as e:
			tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
			_logger.info(tb_ex)

def fetch_program_info(country_code=None, program_id=None):
	res = dict()
	seatsavailable = None
	res['openstatus'] = 'open'
	res['price'] = None
	res['curr'] = None

	try:

		country_data = request.env['res.country'].sudo().search(
			[('code', '=', country_code)])

		region_name = country_data.center_id.region_id.name
		progschedule = request.env['tp.program.schedule'].sudo().search(
			[('id', '=', program_id)])
		serviceinfo = request.env['tp.program.zip.codes'].sudo().search(
			[('notserviceable', '=', 'true')])
		#print('inside function')
		if progschedule:
			progscheduleroom = request.env['tp.program.schedule.roomdata'].sudo().search(
				[('pgmschedule_is_active', '=', 'True'), ('pgmschedule_regularseatsbalance', '>', 0),
				 ('pgmschedule_programname', '=', program_id)])
			for pkg in progscheduleroom:
				# print('Inside progscheduleroom loop')
				for ctry in pkg.pgmschedule_country:
					if (ctry.name == country_data.name):
						seatsavailable = True
						# print('Inside Country Srch')
						break
				for rgn in pkg.pgmschedule_region:
					if (rgn.name == region_name):
						seatsavailable = True
						# print('Inside Region Srch')
						break

		# print('seatsavailable')
		if progschedule:
			pricinginfo = progschedule.pgmpricing_data.sudo().search(
				[('pgmpricing_programname', '=', program_id)])
			try:
				for prc in pricinginfo:
					for cntry in prc.pgmpricing_countryname:
						if(country_data.name == cntry.name):
							res['price'] = prc.pgmpricing_countrycost
							res['curr'] = prc.pgmpricing_countrycur.name
							raise StopIteration
			except StopIteration:
				pass


		if seatsavailable:
			if progschedule.pgmschedule_registrationopen:
				res['openstatus'] = 'open'
			else:
				res['openstatus'] = 'closed'
			for sinfo in serviceinfo:
				if (country_data.name == sinfo.countryname.name):
					res['openstatus'] = 'NA'
					break

			# # print(program_id)
			# pricinginfo = progschedule.pgmpricing_data.sudo().search(
			#	 [('pgmpricing_programname', '=', program_id)])
			#
			# #print(pricinginfo)
			# try:
			#	 for prc in pricinginfo:
			#		 for cntry in prc.pgmpricing_countryname:
			#			 if (country_data.name == cntry.name):
			#				 #print(country_data.name)
			#				 #print(cntry.name)
			#				 #print('inside loop')
			#				 res['price'] = prc.pgmpricing_countrycost
			#				 res['curr'] = prc.pgmpricing_countrycur.name
			#				 raise StopIteration
			# except StopIteration:
			#	 pass
		else:
			#print('no active program')
			res['openstatus'] = 'closed'
		return res
	except Exception as e:
		tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
		_logger.info(tb_ex)
		res['openstatus'] = None
		res['price'] = None
		res['curr'] = None
		return res


def s2_api_transfer_tos1():
	headersvar = {key: value for (key, value) in request.httprequest.headers if key != 'Host'}
	# del headersvar['Upgrade-Insecure-Requests']
	# urlvar = request.httprequest.url.replace(request.httprequest.host_url,'https://mokshatwo.sushumna.isha.in')
	urlvar = request.httprequest.url.replace(request.httprequest.base_url,
											 request.env['ir.config_parameter'].sudo().get_param(
												 'tp.ishangamone_programinfo_url'))
	# urlvar = 'https://registrations.sushumna.isha.in/api/ieco/getdetailstwomoksha?name=Mitchell&phone=9876543210&email=harishkumar.nakshatram%40ishafoundation.org&check_meditator=True&check_ieo=False&check_shoonya=False&check_samyama=False&lenient_search=true&ssoid=H2llKFJS09X0MrJqYn1Jfeq3puG2&ipadd=138.132.9.102'
	_logger.info("inside moksha2 goy iso api function to transfer to moksha1")
	_logger.info(urlvar)
	print('urlvar')
	resp = requests.request(
		method=request.httprequest.method,
		url=urlvar,
		headers=headersvar,
		data=request.httprequest.get_data(),
		cookies=request.httprequest.cookies,
		verify=False,
		allow_redirects=False)

	excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
	headers = [(name, value) for (name, value) in resp.raw.headers.items()
			   if name.lower() not in excluded_headers]

	# response = werkzeug.wrappers.Response()
	responsev = werkzeug.wrappers.Response(resp.content, resp.status_code, headers)
	return responsev
