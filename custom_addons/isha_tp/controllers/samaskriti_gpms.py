import hashlib
import hmac
import json

try:
    from odoo.http import request
except:
    pass
import logging
import traceback
from datetime import timedelta, datetime, date

import requests
import werkzeug
import werkzeug.utils
import werkzeug.wrappers

from odoo import http
from odoo.http import request
_logger = logging.getLogger(__name__)
null = None
false = False
true = True
def errorlog(temptext):
    request.env['tp.tempdebuglog'].sudo().create({
        'logtext': temptext
    })

def samaskriti_validatepayment(self, **post):
    _logger.info('samaskriti inner validatepayment post' + str(post))
    req_obj = eval(request.httprequest.data)
    resvar = {
        "allow": False,
        "amount": 0,
        'error': {
            "code": 0,
            "message": "validate payment failed"
        }
    }
    try:
        _logger.info(str(eval(request.httprequest.data)))
        req_obj = eval(request.httprequest.data)
        requestId = req_obj.get('requestId', '')
        amountvar = req_obj.get('amount')
        currency = req_obj.get('currency')
        time_ts = datetime.now() - timedelta(seconds=120)
        # res = {
        #     "allow": True,
        #     "amount": amountvar,
        # }
        # return res
        # valid_txn = request.env['smaskriti.program.registration'].sudo().search([('order_id', '=', requestId)])
        _logger.info("*********Inside GPMS Status" + str(requestId))
        valid_txn = request.env['program.lead'].sudo().search([("local_trans_id", "=", requestId)], limit=1)

        _logger.info("********* Inside GPMS Status validate TXN" + str(len(valid_txn)))

        if len(valid_txn) > 0:
            req_payment_rec = request.env['samaskriti.participant.paymenttransaction'].search(
                [('orderid', '=', valid_txn.local_trans_id), ("transactiontype", "=", "Request to Gateway")], limit=1)
            if not req_payment_rec:
                req_payment_rec = request.env['samaskriti.participant.paymenttransaction'].search(
                    [('orderid', '=', valid_txn.local_trans_id), ('billing_email', '!=', False)], limit=1)
            payment_Rec =req_payment_rec
            _logger.info('Samaskriti -Inside Amount ' + str(payment_Rec.amount))
            _logger.info('Samaskriti -Inside Currency ' + str(payment_Rec.currency))
            if payment_Rec.amount == 0:
                if valid_txn.record_country:
                    country = valid_txn.record_country
                elif valid_txn.contact_id_fkey:
                    country = valid_txn.contact_id_fkey.country_id.name
                else:
                    state = valid_txn.record_state
                    state_rec = request.env['res.country.state'].sudo().search([('id', '=', int(state))], limit=1)
                    country = state_rec.country_id.name
                _logger.info('Samaskriti -Inside true validate Price Update' + str(country))
                price_rec = request.env['samaskriti.pricing.parameters'].sudo().search(
                    [('countryname', '=', country),
                     ('programtype', '=', valid_txn.sm_scheduleId.program_type_id.id)])
                _logger.info('Samaskriti -Inside true validate Price Update' + str(price_rec.countrycost))

            if payment_Rec.amount == amountvar and payment_Rec.currency == currency:
                try:
                    resvar = {
                        "allow": True,
                        "amount": amountvar,
                        "currency": currency,
                    }
                    _logger.info('Samaskriti -Inside true validate' + str(resvar))
                except Exception as e:
                    _logger.info('Samaskriti - error in validatepayment Amount and Currency Mismatch')
                    tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
                    _logger.info(tb_ex)
                    return resvar
        else:
            res = {
                "allow": False,
                "amount": 0,
                'error': {
                    "code": 0,
                    "message": "validate payment failed"
                }
            }
            # _logger.info('inside false validate' + str(res))
        return resvar
    except Exception as e:
        _logger.info('samaskriti - error in validatepayment')
        tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
        _logger.info(tb_ex)
        return resvar
        # return request.render("isha_tp.pgmregn")


def samaskriti_gpmsstatus(self, **post):
    _logger.info('samaskriti - inside gmpsstatus post' + str(post))
    errorlog('samaskriti - inside gmpsstatus post' + str(post))
    try:
        # raise Exception
        _logger.info(str(eval(request.httprequest.data)))
        # _logger.info(str(post))
        posteddata = eval(request.httprequest.data)
        try:
            _logger.info('samaskriti - inside gmpsstatus post' + str(posteddata))
        except:
            pass

        # c_value_gpms = dict()
        ereceipturlvar = "na"
        if posteddata.get("receiptInfo") is None:
            ereceipturlvar = "na"
        elif "receipt_url" in posteddata.get("receiptInfo"):
            ereceipturlvar = posteddata['receiptInfo']['receipt_url']

        c_value_gpms = {
            'paymentid': posteddata['paymentId'],
            'invoiceid': posteddata['invoiceId'],
            'addlinfo': posteddata['addlInfo'],
            'paymentmethod': posteddata['paymentMethod'],
            'paymentdata': posteddata['paymentData'],
            'ereceipturl': ereceipturlvar,
            'payment_status': posteddata['paymentStatus'],
            'paymenttrackingid': posteddata['transactionId'],
            'orderid': posteddata['requestId']
        }

        samaskriti_update_payment_transaction(self,c_value_gpms,posteddata)

        return json.dumps({'message': 'Success'})
    except Exception as e:
        _logger.info('error in gmpsstatus')
        tb_ex = 'error in gmpsstatus '.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
        _logger.info(tb_ex)
        errorlog('error in gmpsstatus')
        errorlog(tb_ex)
        datavar = {}
        datavar['requestId'] = 1000
        datavar['error'] = tb_ex
        return json.dumps({'message': 'Fail'})
        # return werkzeug.wrappers.Response(datavar, content_type='application/json;charset=utf-8', status=400)

# def samaskriti_paystatus(self, status="", amount="", transactionId="", requestId="", currency="", errorCode="",
#                         errorReason="", errorMsg="", errCode="", errReason="", errMsg="", **post):
#     _logger.info("********** Payment Statsu (Isha TP) ************")
#     c_value_pr = {'payment_status': status,
#                   'amount': amount,
#                   'paymenttrackingid': transactionId,
#                   'orderid': requestId,
#                   'currency': currency,
#                   'errorcode': errCode,
#                   'errorreason': errReason,
#                   'errormsg': errMsg
#                   }
#     return self.samaskriti_paymentresponse_returntemplate(c_value_pr)
#
# def samaskriti_paymentsuccess(self, status="", amount="", transactionId="", requestId="", currency="", errorCode="",
#                              errorReason="", errorMsg="", errCode="", errReason="", errMsg="", **post):
#     _logger.info("********** Payment Success (Isha TP) ************")
#     c_value_pr = {'payment_status': status,
#                   'amount': amount,
#                   'paymenttrackingid': transactionId,
#                   'orderid': requestId,
#                   'currency': currency,
#                   'errorcode': errCode,
#                   'errorreason': errReason,
#                   'errormsg': errMsg
#                   }
#     return self.samaskriti_paymentresponse_returntemplate(c_value_pr)

def samaskriti_paymentresponse(self, status="", amount="", transactionId="", requestId="", currency="", errorCode="",
                                errorReason="", errorMsg="", errCode="", errReason="", errMsg="", **post):
    _logger.info('----------------->    Samaskriti - Inside paymentresponse')
    _logger.info('status ' + str(status))
    _logger.info('amount ' + str(amount))
    _logger.info('currency ' + str(amount))
    _logger.info('transactionId ' + str(transactionId))
    _logger.info('requestId ' + str(requestId))
    _logger.info('post ' + str(post))
    errorlog('paymentresponse post' + str(post))
    try:
        # c_value_pr = dict()

        c_value_pr = {'payment_status': status,
                      'amount': amount,
                      'paymenttrackingid': transactionId,
                      'orderid': requestId,
                      'currency': currency,
                      'errorcode': errCode,
                      'errorreason': errReason,
                      'errormsg': errMsg
                      }
        urlvar = request.httprequest.url.replace("/programs/pbk/paymentresponse", "/samaskriti/paystatus")
        if status == "COMPLETED":
            urlvar = request.httprequest.url.replace("/programs/pbk/paymentresponse",
                                                     "/samaskriti/paymentsuccess")
            return werkzeug.utils.redirect(urlvar)
        if status == "ERROR":
            urlvar = request.httprequest.url.replace("/programs/pbk/paymentresponse",
                                                     "/samaskriti/paystatus")
            return werkzeug.utils.redirect(urlvar)

        return werkzeug.utils.redirect(urlvar)  # INPROGRESSself.paymentresponse_returntemplate(c_value_pr)
    except Exception as e:
        _logger.info('error in gmpsstatus')
        tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
        _logger.info(tb_ex)
        errorlog('paymentresponse error' + str(tb_ex))
        datavar = {}
        datavar['requestId'] = 1000
        datavar['error'] = tb_ex
        return json.dumps({'message': 'Fail'})
    # return werkzeug.wrappers.Response(datavar, content_type='application/json;charset=utf-8', status=400)


def samaskriti_update_payment_transaction(self, c_value, postdata):
    """ Update the Payment Transaction on every hits
    :param:@array of values
    @:return: display the msg in HTML template"""
    _logger.info("********** Samaskriti Update Payment Transaction (Isha TP) ************")
    _logger.info("startinng execution update_payment_transation c_value" + str(c_value['orderid']))
    errorlog("startinng execution update_payment_transation c_value" + str(c_value['orderid']))
    orderid = str(c_value['orderid'])
    err_msg = ""
    person_name = ""
    support_email = 'graceofyoga.online@ishafoundation.org'
    p_registration_rec = request.env['program.lead'].sudo().search([("local_trans_id", "=", orderid)],limit=1)

    _logger.info("********** Samaskriti Payment Transaction ID (Isha TP) ************"+str(p_registration_rec.sm_scheduleId.program_type_id.program_type))
    msg = "Transaction_Failure"
    mail_template_id = None
    if c_value.get("payment_status") == "ERROR":
        err_msg = c_value.get("errMsg")
        reg_Status = "Failed"
    # if p_registration_rec.previous_programapplied:
    #     prev_registration_rec = request.env['tp.program.registration'].sudo().search(
    #         [("id", "=", p_registration_rec.previous_programapplied)])
    #     prev_registration_rec.write({"registration_status": "payment_upgraded"})
    c_value['transactiontype'] = 'asyncresponse'

    # try:
    #     if p_registration_rec and p_registration_rec.previous_programapplied and c_value['payment_status'] == "COMPLETED":
    #         prev_registration_rec = request.env['tp.program.registration'].sudo().search([("id", "=", p_registration_rec.previous_upgraded_fromprogreg)])
    #         prev_registration_rec.write({"registration_status": "payment_upgraded"})
    # except:
    #     pass

    payment_transaction = request.env['samaskriti.participant.paymenttransaction'].sudo().create(c_value)
    # payment_transaction_mail_check = request.env['samaskriti.participant.paymenttransaction'].sudo().search([('paymenttrackingid','=',payment_transaction.paymenttrackingid),('billing_email','!=',False)])
    # payment_transaction_mail_check = request.env['samaskriti.participant.paymenttransaction'].sudo().search(
    #     [('orderid', '=', p_registration_rec.local_trans_id), ('billing_email', '!=', False)],limit=1)

    req_payment_rec = request.env['samaskriti.participant.paymenttransaction'].search(
        [('orderid', '=', p_registration_rec.local_trans_id), ("transactiontype", "=", "Request to Gateway")], limit=1)
    if not req_payment_rec:
        req_payment_rec = request.env['samaskriti.participant.paymenttransaction'].search(
            [('orderid', '=', p_registration_rec.local_trans_id), ('billing_email', '!=', False)], limit=1)
    payment_transaction_mail_check =req_payment_rec

    if payment_transaction:
        _logger.info(
            "********** Inside Payment Transaction (Isha TP) ************")
        payment_transaction_mail_check.write({
            'programregistration':p_registration_rec.id,
            'paymenttrackingid': payment_transaction.paymenttrackingid,
            'gpms_transaction_id':payment_transaction.paymenttrackingid
        })
        state = "payment_failed"
        if c_value['payment_status'] == "COMPLETED":
            msg = "Transaction_Success"
            state = "Paid"
            reg_Status = "Confirmed"
            try:
                if postdata.get('receiptInfo'):
                    raised_date = postdata['receiptInfo'].get('raised_date')
                    # receipt_number = postdata['receiptInfo'].get('receipt_number')
                    purpose = postdata['receiptInfo'].get('purpose')
                program_type = p_registration_rec.sm_scheduleId.program_type_id.program_type
                _logger.info("********** payment_transaction.programtype (Isha TP) ************" + str(program_type))
                if program_type == 'Kalaripayattu':
                    mail_template_id = request.env.ref('isha_samskriti.mail_template_payment_succes_kalari')
                else:
                    mail_template_id = request.env.ref('isha_samskriti.mail_template_payment_succes_chant')

                if mail_template_id:
                    _logger.info("************* Email Send ***********" + str(payment_transaction_mail_check.billing_email))
                    payment_transaction_mail_check.sudo().write({
                        "raised_date": raised_date,
                        "paymenttrackingid": payment_transaction_mail_check.paymenttrackingid,
                        "purpose": purpose
                    })
                    mail_rec = request.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(
                        payment_transaction_mail_check.id,
                        force_send=True)
                    _logger.info("***** Mail record ID (Isha Samaskriti) ******" + str(mail_rec))
                    if mail_rec:
                        p_registration_rec.sudo().write(
                            {"is_confirmation_email": True, "confirmation_email_send_date": datetime.now()})

            except Exception as ex:
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                _logger.error('Samskriti mail send error '+tb_ex+'\n'+str(c_value))

            # interview_mail_template_id = request.env.ref('isha_livingspaces.ishaliving_interview_schedule_template')

        elif c_value['payment_status'] == "INPROGRESS":
            msg = "Transaction_InProgress"
            # state = "payment_inprogress"
            reg_Status = "Inprogress"
        elif c_value['payment_status'] == "ERROR":
            msg = "Transaction_failed"
            # state = "payment_failed"
            reg_Status = "Failed"

            # Seats value updated while getting update from GPMS
            sm_batch_id_rec = request.env['program.schedule.batch.master'].search([('id','=',p_registration_rec.sm_batchId.id)],limit=1)
            if sm_batch_id_rec:
                current_seats_left = sm_batch_id_rec.seats_left
                seats_left = current_seats_left+ 1
                sm_batch_id_rec.write({'seats_left':seats_left})
                _logger.info("********** payment_transaction.programtype (Isha TP) ************"+str(sm_batch_id_rec.seats_left))

            # Failed Mail Transfer Logic
            _logger.info("********** payment_transaction.programtype (Isha TP) ************" + str(payment_transaction.programtype))
            mail_template_id = request.env.ref('isha_samskriti.mail_template_program_payment_failed')
            if mail_template_id:
                request.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(
                       payment_transaction_mail_check.id,
                        force_send = True)

            if not p_registration_rec.transfer_rec:
                # Auto Email Payment Resend logic
                _logger.info("***** Resend Infor (Isha Samaskriti) *******" + str(payment_transaction_mail_check))
                for i in payment_transaction_mail_check:
                    base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
                    redirect_url = base_url + "/samaskriti/registrationform/paymentpage?registration_id=" + str(
                        p_registration_rec.id)
                    # redirect_url = base_url + "/samaskriti/registrationform/paymentpage?registration_id=" + str(program_lead_id.id)+'&trans_id='+str(i.id)
                    mail_template_id = request.env.ref('isha_samskriti.samaskriti_resend_payment_template')
                    if mail_template_id:
                        try:
                            i.sudo().write({"payment_link": redirect_url, "payment_link_sent_date": date.today()})
                            mail_rec = request.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(i.id,
                                                                                                                 force_send=True)
                            _logger.info("***** Mail record ID (Isha Samaskriti) ******" + str(mail_rec))
                            # if mail_rec:
                            #     p_registration_rec.sudo().write(
                            #         {"is_payment_email": True, "payment_email_send_date": datetime.now()})

                        except Exception as e:
                            print(e)

        # if mail_template_id:
        #     _logger.info("************* Email Send ***********"+str(payment_transaction_mail_check.billing_email))
        #     request.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(payment_transaction_mail_check.id,
        #                                                                               force_send=True)
        _logger.info("********** Transfer rec - lead ************" + str(p_registration_rec.transfer_rec))
        # if p_registration_rec.transfer_rec:
        #     old_Rec = request.env['program.lead'].sudo().search([('id','=',p_registration_rec.transfer_rec.id)])
        #     _logger.info("********** Old rec - lead ************" + str(old_Rec))
        #     if old_Rec:
        #         reg_Status = 'Program Transfer Confirmed'
        #         old_Rec.sudo().write({"reg_status": reg_Status})
        # else:
        #     reg_Status = 'Confirmed'

        if p_registration_rec:
            p_registration_rec.sudo().write({"reg_status":reg_Status})

            # p_registration_rec.sudo().write(
            #     {"payment_status": state, 'online_transaction_id': c_value.get("paymenttrackingid")})
            # support_email = p_registration_rec.support_email_id if p_registration_rec.support_email_id else support_email
            # c_value['programregistration'] = int(p_registration_rec.id)
            # p_registration_rec.sudo().write({"payment_status": state,'online_transaction_id':c_value.get("paymenttrackingid"),'date_paid': datetime.now().strftime("%Y-%m-%d %H:%M:%S")})
            # # person_name = p_registration_rec.first_name
            # p_registration_rec._compute_seatallocation()

                # p_registration_rec.sudo().write({
                #     "last_email_sent_status": p_registration_rec.payment_status,
                #     "last_email_sent_ts": datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                # })

# def samaskriti_paymentresponse_returntemplate(self, c_value):
#     """ Update the Payment Transaction on every hits
#     :param:@array of values
#     @:return: display the msg in HTML template"""
#     _logger.info("********** samaskriti_paymentresponse_returntemplate (Isha Samaskriti) ************")
#     _logger.info("startinng execution paymentresponse_returntemplate c_value" + str(c_value))
#     errorlog("startinng execution paymentresponse_returntemplate c_value" + str(c_value))
#     rec_id = str(c_value['orderid'])
#     # below line need to removed - enable only for testing
#     # self.put_user_sso_info(rec_id)
#     err_msg = ""
#     person_name = ""
#     support_email = 'graceofyoga.online@ishafoundation.org'
#     p_registration_rec = request.env['smaskriti.program.registration'].sudo().search([("order_id", "=", rec_id)])
#     if p_registration_rec:
#         person_name = p_registration_rec.first_name
#     msg = "Transaction_Failure"
#     mail_template_id = None
#     if c_value.get("payment_status") == "ERROR":
#         err_msg = c_value.get("errMsg")
#
#     c_value['transactiontype'] = 'synccallback'
#     progreg = request.env['smaskriti.program.registration'].sudo().search([("order_id", "=", rec_id)])
#     if c_value.get("payment_status") == 'COMPLETED':
#         progreg.write({'payment_status': 'Paid'})
#     if c_value.get("payment_status") == 'ERROR':
#         progreg.write({'payment_status': 'payment_failed'})
#
#     programtype = request.env['ir.config_parameter'].sudo().get_param('samaskriti.programtype')
#     programname = request.env['ir.config_parameter'].sudo().get_param('samaskriti.programcode')
#
#     if len(progreg) > 0:
#         try:
#             c_value['programname'] = programname
#             c_value['first_name'] = progreg.first_name
#             c_value['last_name'] = progreg.last_name
#             c_value['email'] = progreg.email
#             c_value['programtype'] = programtype
#         except:
#             pass
#
#     payment_transaction = request.env['samaskriti.participant.paymenttransaction'].sudo().create(c_value)
#     _logger.info("****** Payment Transaction ********** (Isha Samaskriti) " + str(payment_transaction))
#
#     values = {"support_email": support_email,
#               "status": c_value.get("payment_status"),
#               "transactionId": c_value.get("paymenttrackingid"),
#               "err_msg": c_value.get("errormsg") if c_value.get("errormsg") else err_msg,
#               "order_id": c_value.get("orderid") if c_value.get("orderid") else "",
#               "error_code": c_value.get("errorcode") if c_value.get("errorcode") else "",
#               "person_name": "   " + person_name,
#               "object": p_registration_rec,
#               "region_support_email_id": "p_registration_rec.support_email_id",
#               "is_ishangamone": request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')
#               }
#     return request.render("isha_samskriti.payumoney_status_template", values)
