import hashlib
import hmac
import json

try:
    from odoo.http import request
except:
    pass
import logging
import traceback
from datetime import timedelta, datetime

import requests
import werkzeug
import werkzeug.utils
import werkzeug.wrappers

from odoo import http
from odoo.http import request
_logger = logging.getLogger(__name__)
null = None
false = False
true = True
def errorlog(temptext):
    request.env['tp.tempdebuglog'].sudo().create({
        'logtext': temptext
    })

def livingspace_validatepayment(self, **post):
    _logger.info('Livingspace inner validatepayment post' + str(post))
    req_obj = eval(request.httprequest.data)
    resvar = {
        "allow": False,
        "amount": 0,
        'error': {
            "code": 0,
            "message": "validate payment failed"
        }
    }
    try:
        _logger.info(str(eval(request.httprequest.data)))
        req_obj = eval(request.httprequest.data)
        requestId = req_obj.get('requestId', '')
        amountvar = req_obj.get('amount')
        time_ts = datetime.now() - timedelta(seconds=120)
        res = {
            "allow": True,
            "amount": amountvar,
        }
        return res
        valid_txn = request.env['kshetragna.registration'].sudo().search([('order_id', '=', requestId),
                                                                          ('payment_status', '=',
                                                                           'payment_initiated')
                                                                          ])
        _logger.info(requestId)
        if len(valid_txn) > 0:
            res = {
                "allow": True,
                "amount": amountvar,
            }
            _logger.info('Livingspace -inside true validate' + str(res))
        else:
            resvar = {
                "allow": False,
                "amount": 0,
                'error': {
                    "code": 0,
                    "message": "validate payment failed"
                }
            }
            # _logger.info('inside false validate' + str(res))
        return res
    except Exception as e:
        _logger.info('Livingspace - error in validatepayment')
        tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
        _logger.info(tb_ex)
        return resvar
        # return request.render("isha_tp.pgmregn")


def livingspace_gpmsstatus(self, **post):
    _logger.info('Livingspace - inside gmpsstatus post' + str(post))
    errorlog('Livingspace - inside gmpsstatus post' + str(post))
    try:
        # raise Exception
        _logger.info(str(eval(request.httprequest.data)))
        # _logger.info(str(post))
        posteddata = eval(request.httprequest.data)
        try:
            _logger.info('Livingspace - inside gmpsstatus post' + str(posteddata))
        except:
            pass

        # c_value_gpms = dict()
        ereceipturlvar = "na"
        try:
            ereceipturlvar = posteddata['receiptInfo']['receipt_url']
        except:
            pass
        c_value_gpms = {
            'paymentid': posteddata['paymentId'],
            'invoiceid': posteddata['invoiceId'],
            'addlinfo': posteddata['addlInfo'],
            'paymentmethod': posteddata['paymentMethod'],
            'paymentdata': posteddata['paymentData'],
            'ereceipturl': ereceipturlvar,
            'payment_status': posteddata['paymentStatus'],
            'paymenttrackingid': posteddata['transactionId'],
            'orderid': posteddata['requestId']
        }

        livingspace_update_payment_transaction(self,c_value_gpms)

        return json.dumps({'message': 'Success'})
    except Exception as e:
        _logger.info('error in gmpsstatus')
        tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
        _logger.info(tb_ex)
        errorlog('error in gmpsstatus')
        errorlog(tb_ex)
        datavar = {}
        datavar['requestId'] = 1000
        datavar['error'] = tb_ex
        return json.dumps({'message': 'Fail'})
        # return werkzeug.wrappers.Response(datavar, content_type='application/json;charset=utf-8', status=400)


def livingspace_paystatus(self, status="", amount="", transactionId="", requestId="", currency="", errorCode="",
                          errorReason="", errorMsg="", errCode="", errReason="", errMsg="", **post):
    c_value_pr = {'payment_status': status,
                  'amount': amount,
                  'paymenttrackingid': transactionId,
                  'orderid': requestId,
                  'currency': currency,
                  'errorcode': errCode,
                  'errorreason': errReason,
                  'errormsg': errMsg
                  }
    return self.paymentresponse_returntemplate(c_value_pr)


def livingspace_paymentsuccess(self, status="", amount="", transactionId="", requestId="", currency="", errorCode="",
                               errorReason="", errorMsg="", errCode="", errReason="", errMsg="", **post):
    c_value_pr = {'payment_status': status,
                  'amount': amount,
                  'paymenttrackingid': transactionId,
                  'orderid': requestId,
                  'currency': currency,
                  'errorcode': errCode,
                  'errorreason': errReason,
                  'errormsg': errMsg
                  }
    return self.paymentresponse_returntemplate(c_value_pr)


def livingspace_paymentresponse(self, status="", amount="", transactionId="", requestId="", currency="", errorCode="",
                                errorReason="", errorMsg="", errCode="", errReason="", errMsg="", **post):
    _logger.info('----------------->    inside paymentresponse')
    _logger.info('status' + str(status))
    _logger.info('amount' + str(amount))
    _logger.info('transactionId' + str(transactionId))
    _logger.info('requestId' + str(requestId))
    _logger.info('post' + str(post))
    errorlog('paymentresponse post' + str(post))
    try:
        # c_value_pr = dict()

        c_value_pr = {'payment_status': status,
                      'amount': amount,
                      'paymenttrackingid': transactionId,
                      'orderid': requestId,
                      'currency': currency,
                      'errorcode': errCode,
                      'errorreason': errReason,
                      'errormsg': errMsg
                      }
        urlvar = request.httprequest.url.replace("/programs/pbk/paymentresponse", "/kshetragna/paystatus")
        if status == "COMPLETED":
            urlvar = request.httprequest.url.replace("/programs/pbk/paymentresponse",
                                                     "/kshetragna/paymentsuccess")
            return werkzeug.utils.redirect(urlvar)
        if status == "ERROR":
            urlvar = request.httprequest.url.replace("/programs/pbk/paymentresponse",
                                                     "/kshetragna/paystatus")
            return werkzeug.utils.redirect(urlvar)

        return werkzeug.utils.redirect(urlvar)  # INPROGRESSself.paymentresponse_returntemplate(c_value_pr)
    except Exception as e:
        _logger.info('error in gmpsstatus')
        tb_ex = ''.join(traceback.format_exception(etype=type(e), value=e, tb=e.__traceback__))
        _logger.info(tb_ex)
        errorlog('paymentresponse error' + str(tb_ex))
        datavar = {}
        datavar['requestId'] = 1000
        datavar['error'] = tb_ex
        return json.dumps({'message': 'Fail'})
    # return werkzeug.wrappers.Response(datavar, content_type='application/json;charset=utf-8', status=400)


def livingspace_update_payment_transaction(self, c_value):
    """ Update the Payment Transaction on every hits
    :param:@array of values
    @:return: display the msg in HTML template"""
    _logger.info("startinng execution update_payment_transation c_value" + str(c_value['orderid']))
    errorlog("startinng execution update_payment_transation c_value" + str(c_value['orderid']))
    rec_id = str(c_value['orderid'])
    err_msg = ""
    person_name = ""
    support_email = 'graceofyoga.online@ishafoundation.org'
    p_registration_rec = request.env['kshetragna.registration'].sudo().search([("order_id", "=", rec_id)])
    msg = "Transaction_Failure"
    mail_template_id = None
    interview_mail_template_id = None
    if c_value.get("payment_status") == "ERROR":
        err_msg = c_value.get("errMsg")
    # if p_registration_rec.previous_programapplied:
    #     prev_registration_rec = request.env['tp.program.registration'].sudo().search(
    #         [("id", "=", p_registration_rec.previous_programapplied)])
    #     prev_registration_rec.write({"registration_status": "payment_upgraded"})
    c_value['transactiontype'] = 'asyncresponse'
    # try:
    #     if p_registration_rec and p_registration_rec.previous_programapplied and c_value['payment_status'] == "COMPLETED":
    #         prev_registration_rec = request.env['tp.program.registration'].sudo().search([("id", "=", p_registration_rec.previous_upgraded_fromprogreg)])
    #         prev_registration_rec.write({"registration_status": "payment_upgraded"})
    # except:
    #     pass

    payment_transaction = request.env['kshetragna.participant.paymenttransaction'].sudo().create(c_value)
    if payment_transaction:
        state = "payment_failed"
        if c_value['payment_status'] == "COMPLETED":
            msg = "Transaction_Success"
            state = "Paid"
            mail_template_id = request.env.ref('isha_livingspaces.mail_template_kshetragna_registration')
            interview_mail_template_id = request.env.ref('isha_livingspaces.ishaliving_interview_schedule_template')

        elif c_value['payment_status'] == "INPROGRESS":
            msg = "Transaction_InProgress"
            state = "payment_inprogress"
        elif c_value['payment_status'] == "ERROR":
            msg = "Transaction_failed"
            state = "payment_failed"
            mail_template_id = request.env.ref('isha_livingspaces.Kshetragna_mail_template_program_payment_failed')

        if p_registration_rec:
            prescreenedregistrants = request.env['kshetragna.prescreenedregistrants'].sudo().search(
                [("email", "=", p_registration_rec.email)])
            if prescreenedregistrants:
                prescreenedregistrants.sudo().write({"payment_status": state})

            p_registration_rec.sudo().write(
                {"payment_status": state, 'online_transaction_id': c_value.get("paymenttrackingid")})
            # support_email = p_registration_rec.support_email_id if p_registration_rec.support_email_id else support_email
            c_value['programregistration'] = int(p_registration_rec.id)
            p_registration_rec.sudo().write({"payment_status": state,'online_transaction_id':c_value.get("paymenttrackingid"),'date_paid': datetime.now().strftime("%Y-%m-%d %H:%M:%S")})
            # person_name = p_registration_rec.first_name
            # p_registration_rec._compute_seatallocation()
            if mail_template_id:
                if p_registration_rec.payment_status != p_registration_rec.last_email_sent_status:
                    request.env['mail.template'].sudo().browse(mail_template_id.id).send_mail(p_registration_rec.id,
                                                                                              force_send=True)
                    p_registration_rec.sudo().write({
                        "last_email_sent_status": p_registration_rec.payment_status,
                        "last_email_sent_ts": datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    })
            # Once payment is done , sent interview schedule in mail
            if interview_mail_template_id:
                request.env['mail.template'].sudo().browse(interview_mail_template_id.id).send_mail(
                    p_registration_rec.id,
                    force_send=True)
                p_registration_rec.sudo().write({
                    "is_interview_mail_sent": True,
                })


def livingspace_paymentresponse_returntemplate(self, c_value):
    """ Update the Payment Transaction on every hits
    :param:@array of values
    @:return: display the msg in HTML template"""
    _logger.info("startinng execution paymentresponse_returntemplate c_value" + str(c_value))
    errorlog("startinng execution paymentresponse_returntemplate c_value" + str(c_value))
    rec_id = str(c_value['orderid'])
    # below line need to removed - enable only for testing
    # self.put_user_sso_info(rec_id)
    err_msg = ""
    person_name = ""
    support_email = 'graceofyoga.online@ishafoundation.org'
    p_registration_rec = request.env['kshetragna.registration'].sudo().search([("order_id", "=", rec_id)])
    if p_registration_rec:
        person_name = p_registration_rec.first_name
    msg = "Transaction_Failure"
    mail_template_id = None
    if c_value.get("payment_status") == "ERROR":
        err_msg = c_value.get("errMsg")

    c_value['transactiontype'] = 'synccallback'
    progreg = request.env['kshetragna.registration'].sudo().search([("order_id", "=", rec_id)])
    programtype = request.env['ir.config_parameter'].sudo().get_param('kshetragna.programtype')
    programname = request.env['ir.config_parameter'].sudo().get_param('kshetragna.programcode')

    if len(progreg) > 0:
        try:
            c_value['programname'] = programname
            c_value['first_name'] = progreg.first_name
            c_value['last_name'] = progreg.last_name
            c_value['email'] = progreg.email
            c_value['programtype'] = programtype
        except:
            pass

    payment_transaction = request.env['kshetragna.participant.paymenttransaction'].sudo().create(c_value)

    values = {"support_email": support_email,
              "status": c_value.get("payment_status"),
              "transactionId": c_value.get("paymenttrackingid"),
              "err_msg": c_value.get("errormsg") if c_value.get("errormsg") else err_msg,
              "order_id": c_value.get("orderid") if c_value.get("orderid") else "",
              "error_code": c_value.get("errorcode") if c_value.get("errorcode") else "",
              "person_name": "   " + person_name,
              "object": p_registration_rec,
              "region_support_email_id": "p_registration_rec.support_email_id",
              "is_ishangamone": request.env['ir.config_parameter'].sudo().get_param('tp.goy_is_ishangamone')
              }
    return request.render("isha_livingspaces.payumoney_status_template", values)
