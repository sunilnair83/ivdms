from odoo import models, fields, _

class ConfirmationWizard(models.TransientModel):
    _name = "confirm.wizard"

    message = fields.Text()

    def yes(self):
        if self.message=="Please confirm that you wish to cancel":
            active_ids = self._context.get('active_ids')
            reg_records = self.env['tp.program.registration'].sudo().search(
                [("id", "=", active_ids), ("registration_status", "=", "Paid")])
            failed_count = self.env['tp.program.registration'].sudo().search(
                [("id", "=", active_ids), ("registration_status", "!=", "Paid")])
            if len(failed_count) > 0:
                message_id = self.env['message.wizard'].create(
                    {'message': _("Un Paid registration cannot be Canceled")})
                return {
                    'name': _('Warning'),
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'message.wizard',
                    # pass the id
                    'res_id': message_id.id,
                    'target': 'new'
                }
            return reg_records.cancel_participant_id(reg_records)
        else:
            active_ids = self._context.get('active_ids')
            print(active_ids)
            reg_records = self.env['tp.program.registration'].sudo().search(
                [("id", "=", active_ids), ("registration_status", "=", "Paid")])[0]
            failed_count = self.env['tp.program.registration'].sudo().search(
                [("id", "=", active_ids), ("registration_status", "!=", "Paid")])
            if len(failed_count) > 0:
                message_id = self.env['message.wizard'].create(
                    {'message': _("Un Paid registration cannot be Upgrade")})
                return {
                    'name': _('Warning'),
                    'type': 'ir.actions.act_window',
                    'view_mode': 'form',
                    'res_model': 'message.wizard',
                    # pass the id
                    'res_id': message_id.id,
                    'target': 'new'
                }
            return reg_records.upgrade_participant_id(reg_records)


    def no(self):
        """ cancel the popup """
        return {'type': 'ir.actions.act_window_close'}

