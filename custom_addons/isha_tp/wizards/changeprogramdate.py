from odoo import models, fields, api, exceptions
from odoo.exceptions import except_orm
from datetime import date, timedelta, datetime
import json

class ChangeProgramDate(models.TransientModel):
	_name = 'tp.program.registration.changeprogramdate'
	_description = 'Change Program Date'

	#participant_id = fields.Integer(string = 'ID')
	participant_name = fields.Char(string = 'Participant Name', required=True)
	programapplied = fields.Many2one('tp.program.schedule',string = 'Applied Program', required=True)
	prg_date_now = fields.Date()
	selected_program = fields.Many2one('tp.program.schedule',string = 'Available Programs', required=True ,
	domain="[('id','!=',programapplied),('pgmschedule_registrationopen','=',True),('pgmschedule_totalbalanceseats','>',0),('pgmschedule_startdate','>',context_today().strftime('%Y-%m-%d'))]")
	selected_package = fields.Many2one('tp.program.schedule.roomdata', string='Requested Package',
	domain="[('pgmschedule_programname','=',selected_program)]", required=True)

	@api.constrains('selected_program')
	def	validate_selected_program(self):
		if (self.selected_program == self.programapplied):
			raise exceptions.ValidationError("Applied program and selected program should not be the same")
	
	@api.onchange('programapplied')
	def participant_name_onchange(self):
		print('onchange')
		print(self.programapplied.pgmschedule_programtype.id)
		id = self.programapplied.pgmschedule_programtype.id
		print(id)
		recs = self.env['tp.program.schedule'].search([('pgmschedule_programtype', '=', id)])
		print(recs)			
		# self.selected_program = recs

	@api.onchange('selected_program')
	def selected_program_onchange(self):
		print('prog change')
	
	def submit_changeprogramdate(self):
		print('field generate')
		#fields = self.env['tp.program.registration'].fields_get()

		active_id = self.env.context.get('default_active_id')
		ids = self.env["tp.program.registration"].browse(active_id)
		for id in ids:

			if (id.payment_status == 'Initiated'):
				raise exceptions.ValidationError('Payment have been initiated by the participant. Please try after some time')
			elif (id.registration_status != 'Paid' and id.registration_status != 'Confirmed'):
				print('change initiated')
				id.write({'programapplied':self.selected_program, 'packageselection':self.selected_package})
			else:

				#

				balanceseatcount = self.selected_package.pgmschedule_packagenoofseats - self.selected_package.pgmschedule_regularseatspaid
				balanceemergencyseatcount = self.selected_package.pgmschedule_packageemergencyseats - self.selected_package.pgmschedule_emergencyseatspaid
			
				if (balanceseatcount <= 0 and balanceemergencyseatcount <= 0):
					raise exceptions.ValidationError('Schedule change not allowed, no seats are available in the selected package.')
				
				#

				print('initiate cancellation')

				self.env['tp.payments']._reverseSeat(id)

				refund_data = self.env['tp.payments']._calculateRefundAmount(id, datetime.now())
				
				id.write({
					'cancelapproved_email_send':False,
					'cancelapproved_sms_send':False,
					'date_cancelapproved':datetime.now(),
					'registration_status':'Cancel Approved',
					'refund_eligible_ondaysbefore': refund_data['days_before'],
					'refund_eligible_onpercentage': refund_data['elg_percent'],
					'refund_eligible': refund_data['elg_amt'],
					'last_modified_dt': datetime.now()				
				})

				#email and sms trigger
				self.env['tp.notification']._sendCancellationMailAndSMS(id.id,'Cancel Approved')

				print('do the new registration')

				nrec = self.env["tp.program.registration"].create({'first_name':id.first_name,
																'last_name':id.last_name,
																'name_called':id.name_called,
																'gender':id.gender,
																'marital_status':id.marital_status,
																'dob':id.dob,
																'countrycode':id.countrycode,
																'phone':id.phone,
																'pincode':id.pincode,
																'address_line':id.address_line,
																'city':id.city,
																'registration_status':'Pending',
																'programapplied':self.selected_program.id,
																'packageselection':self.selected_package.id,
																'participant_comments':id.participant_comments,
																'participant_email':id.participant_email,
																'prerequisite_pgm':id.prerequisite_pgm,
																'display_name':id.display_name
																})
								
				if nrec:

					nrec.write({'country':id.country,
								'state':id.state,
								'photo_file':id.photo_file,
								'idproof_file':id.idproof_file,
								'addrproof_file':id.addrproof_file,
								'bloodreports_file':id.bloodreports_file,
								'echoreports_file':id.echoreports_file,
								'nationality_id':id.nationality_id,
								'idproof_type':id.idproof_type
								})

