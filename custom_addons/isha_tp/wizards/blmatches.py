from odoo import models, fields, api, exceptions
from odoo.exceptions import except_orm
from datetime import date, timedelta, datetime

class BlocklistMatches(models.TransientModel):
	_name = 'tp.bl.matches'
	_description = 'Blocklist Matched'

	name = fields.Char(string = 'Name')	
	mobile = fields.Char(string = 'Mobile')	
	blacklist_type = fields.Selection([('Normal','Normal')], string='Blacklist Type')	

	def submit_selection(self):
		print('submit clicked')
