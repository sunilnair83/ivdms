from odoo import models, fields, api, exceptions, _
from odoo.exceptions import except_orm


class ResendMailToParticipant(models.TransientModel):
    _name = 'tp.program.registration.resendmail'
    _description = 'Resend Email/SMS To Participant'

    participant_name = fields.Char(string='Participant Name')
    participant = fields.Many2one('tp.program.registration', string='Participants')
    programapplied = fields.Many2one('tp.program.schedule', string='Applied Program',
                                     related="participant.programapplied")
    programtype = fields.Many2one('tp.program.type', string='Program Type',
                                  related="programapplied.pgmschedule_programtype")
    participant_email = fields.Char(string='Email', related="participant.participant_email")
    participant_phone = fields.Char(string='Mobile Number', related="participant.phone")
    participant_email_alt = fields.Char(string='Alternate Email ID')
    participant_phone_alt = fields.Char(string='Alternate Mobile No.')
    notificationevent = fields.Many2one('tp.notificationevent', string='Notification Event')
    sendemail = fields.Boolean(string='Send Email', default=True)
    emailtemplateid = fields.Many2one('mail.template', string='Email Template', track_visibility='onchange')
    sendsms = fields.Boolean(string='Send SMS', default=False)
    smstemplateid = fields.Many2one('sms.template', string='SMS Template', track_visibility='onchange')

    emailseperatornote = fields.Char(string=' ', size=500, readonly="1",
                                     default='Please use ; (semi-colon) for multiple email addresses')
    mobileseperatornote = fields.Char(string=' ', size=500, readonly="1",
                                      default='Please use ; (semi-colon) for multiple mobile numbers')

    @api.constrains('selected_program')
    def validate_selected_program(self):
        if (self.selected_program == self.programapplied):
            raise exceptions.ValidationError("Applied program and selected program should not be the same")

    @api.onchange('programapplied')
    def participant_name_onchange(self):
        print('inside participant_name_onchange')
        active_id = self.env.context.get('default_active_id')
        id = self.env["tp.program.registration"].browse(active_id)
        if id:
            self.participant = id
            if ( id.registration_status == 'Incomplete'):
                self.notificationevent = self.env['tp.notificationevent'].search(
                    [('notificationevent', '=', 'PartialRegistration')])
            elif ( id.registration_status == 'Pending'):
                self.notificationevent = self.env['tp.notificationevent'].search(
                    [('notificationevent', '=', 'Registration')])
            elif ( id.registration_status == 'Rejected'):
                self.notificationevent = self.env['tp.notificationevent'].search(
                    [('notificationevent', '=', 'Rejection')])
            elif (id.registration_status == 'Add To Waiting List'):
                self.notificationevent = self.env['tp.notificationevent'].search([('notificationevent', '=', 'Add to Waitlist')])
            elif (id.registration_status == 'Payment Link Sent'):
                self.notificationevent = self.env['tp.notificationevent'].search([('notificationevent', '=', 'Payment Link Send')])
            elif (id.registration_status == 'Cancel Approved' or id.registration_status == 'Cancel Applied'):
                self.notificationevent = self.env['tp.notificationevent'].search([('notificationevent', '=', id.registration_status)])
            elif (id.registration_status == 'Refunded'):
                self.notificationevent = self.env['tp.notificationevent'].search([('notificationevent', '=', 'Cancel Refunded')])
            elif (id.registration_status == 'Confirmed'):
                self.notificationevent = self.env['tp.notificationevent'].search([('notificationevent', '=', 'Confirmation')])


@api.onchange('notificationevent')
def notificationevent_onchange(self):
    print('onchange notificationevent')
    print(self.notificationevent)
    print(self.programtype)
    if (self.programtype and self.notificationevent):
        recs = self.env['tp.notificationtemplate'].search(
            [('programtype.id', '=', self.programtype.id), ('notificationevent.id', '=', self.notificationevent.id)])
        if recs.exists():
            print(recs)
            self.emailtemplateid = recs.emailtemplateid
            self.smstemplateid = recs.smstemplateid
        else:
            self.emailtemplateid = None
            self.smstemplateid = None


def submit_resendmail(self):
    print('mail resend')

    active_id = self.env.context.get('default_active_id')
    id = self.env["tp.program.registration"].browse(active_id)
    if id:
        if (self.sendemail == True):
            print('sending email enabled')

            if (not self.emailtemplateid.exists()):
                raise exceptions.ValidationError("Email template not configured")

            if (self.participant_email_alt):
                id.write({'participant_email_alt': self.participant_email_alt})
            templateid = self.emailtemplateid.id
            self.env['mail.template'].browse(templateid).send_mail(id.id, force_send=True)

        if (self.sendsms == True and id.country.name == 'India'):
            print('sending sms enabled')

            if (not self.smstemplateid.exists()):
                raise exceptions.ValidationError("SMS template not configured")

            templateid = self.smstemplateid.id
            smstemplate = self.env['sms.template'].browse(templateid)
            subject = self.env['sms.template']._render_template(smstemplate.body,
                                                                'tp.program.registration', id.id)
            print(subject)
            self.env['tp.smsapi']._send_smcountry_sms({
                'number': id.phone,
                'message': subject
            })
            if (self.participant_phone_alt):
                mobiles = self.participant_phone_alt.split(";")
                for x in mobiles:
                    self.env['tp.smsapi']._send_smcountry_sms({
                        'number': x,
                        'message': subject
                    })

        if (self.sendemail == False and self.sendsms == False):
            message = {
                'type': 'ir.actions.client',
                'tag': 'display_notification',
                'params':
                    {
                        'title': _('Warning!'),
                        'message': 'Select either email or SMS checkbox and try again',
                        'sticky': False
                    }
            }
            return message
