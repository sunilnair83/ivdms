var country = ""
var nationality = ""
var isoverseas = false;

$(document).ready(function () {

     $('.jswebbillingctrl').prop('hidden', true);

    country = $('#country').val();
    nationality = $('#nationality').val();

    if (country != 'India' || nationality != 'India')
    {
        isoverseas = true
    }

    var ele = document.getElementById('h_billinginfoconfirmation')
    try {
    if (ele != null)
    {

       bname = document.getElementById('h_billing_name').value

        if (  ( (ele.value == "False" || ele.value == "false" || ele.value == "" || ele.value == null || ele.value == undefined )
         && bname != undefined && bname != null && bname != "") )
        {
            document.getElementById('billinginfoconfirmation').checked = false
            disableEnableInputs(false)
            document.getElementById('billing_name').value = document.getElementById('h_billing_name').value
            document.getElementById('billing_address').value = document.getElementById('h_billing_address').value
            document.getElementById('billing_city').value = document.getElementById('h_billing_city').value
            document.getElementById('billing_country').value = document.getElementById('h_billing_country').value
            document.getElementById('r_billing_country').value = document.getElementById('h_billing_country').value
            document.getElementById('billing_state').value = document.getElementById('h_billing_state').value
            document.getElementById('billing_zip').value = document.getElementById('h_billing_zip').value
            document.getElementById('billing_tel').value = document.getElementById('h_billing_tel').value
            document.getElementById('billing_email').value = document.getElementById('h_billing_email').value

        }
        else{
           // disableEnableInputs(true)
//            document.getElementById('billinginfoconfirmation').checked = true
            document.getElementById('billinginfoconfirmation').value = "True"
            document.getElementById('billing_name').value = document.getElementById('first_name').value
            document.getElementById('billing_address').value = document.getElementById('address_line').value
            document.getElementById('billing_city').value = document.getElementById('city').value
            document.getElementById('billing_country').value = document.getElementById('country').value
            document.getElementById('r_billing_country').value = document.getElementById('country').value
            document.getElementById('billing_state').value = document.getElementById('state').value
            document.getElementById('billing_zip').value = document.getElementById('pincode').value
            document.getElementById('billing_tel').value = document.getElementById('phone').value
            document.getElementById('billing_email').value = document.getElementById('participant_email').value

            if (isoverseas)
            {
                $('#billinginfoconfirmation').hide()
                $('#billinginfoconfirmationlabel').hide()
				$('#billinginfoconfirmationlabel1').hide()
				$('#billinginfoconfirmationlabel2').hide()
            }
			else
			{
				$('#billinginfoconfirmation').show()
                $('#billinginfoconfirmationlabel').show()
				$('#billinginfoconfirmationlabel1').show()
				$('#billinginfoconfirmationlabel2').show()
			}
        }
    }
   }
   catch(ex){
   console.log(ex.message);
   }
    //countrychange()

});

function disableEnableInputs(disableEnableFlag)
{
    $('#billing_name').prop('readonly', disableEnableFlag);
    $('#billing_address').prop('readonly', disableEnableFlag);
    $('#billing_city').prop('readonly', disableEnableFlag);
    $('#billing_country').prop('readonly', disableEnableFlag);
    $('#billing_state').prop('readonly', disableEnableFlag);
    $('#billing_zip').prop('readonly', disableEnableFlag);
    $('#billing_tel').prop('readonly', disableEnableFlag);
    $('#billing_email').prop('readonly', disableEnableFlag);

    if (disableEnableFlag)
    {
        $('#r_billing_country').prop('hidden', false);
        $('#billing_country').prop('hidden', true);


        $('#label1').hide()
        $('#label2').hide()
        $('#label3').hide()
        $('#label4').hide()
        $('#label5').hide()
        $('#label6').hide()
    }
    else
    {
        $('#r_billing_country').prop('hidden', true);
        $('#billing_country').prop('hidden', false);

        $('#label1').show()
        $('#label2').show()
        $('#label3').show()
        $('#label4').show()
        $('#label5').show()
        $('#label6').show()
    }

}

function bilingconfirmationchange()
{

    var ele = document.getElementById('billinginfoconfirmation')
    try{
    if (ele != null)
    {
        if (!ele.checked)
        {

//            disableEnableInputs(true)
            $('.jswebbillingctrl').prop('hidden', true);
            $('.jswebbillingctrl').css('style', 'display:none');
            document.getElementById('billinginfoconfirmation').value = "True"
            document.getElementById('billing_name').value = document.getElementById('first_name').value
            document.getElementById('billing_address').value = document.getElementById('address_line').value
            document.getElementById('billing_city').value = document.getElementById('city').value
            document.getElementById('billing_country').value = document.getElementById('country').value
            document.getElementById('r_billing_country').value = document.getElementById('country').value
            document.getElementById('billing_state').value = document.getElementById('state').value
            document.getElementById('billing_zip').value = document.getElementById('pincode').value
            document.getElementById('billing_tel').value = document.getElementById('phone').value
            document.getElementById('billing_email').value = document.getElementById('participant_email').value
            removevalidation()

        }
        else
        {
//            disableEnableInputs(false)
            $('.jswebbillingctrl').prop('hidden', false);
            $('.jswebbillingctrl').css('style', 'display:block');

            document.getElementById('billinginfoconfirmation').value = "False"
            document.getElementById('billing_name').value = ""
            document.getElementById('billing_address').value = ""
            document.getElementById('billing_city').value = ""
            document.getElementById('billing_country').value = ""
            document.getElementById('r_billing_country').value = ""
            document.getElementById('billing_state').value = ""
            document.getElementById('billing_zip').value = ""
            document.getElementById('billing_tel').value = ""
            document.getElementById('billing_email').value = ""
            addvalidation()
        }
    }
    }
    catch(ex){
    console.log(ex.message)
    }
}

function removevalidation(){
$('#billing_name').removeAttr('minlength')
$('#billing_name').removeAttr('maxlength')
$('#billing_name').removeAttr('pattern')
$('#billing_name').removeAttr('required')

$('#billing_address').removeAttr('minlength')
$('#billing_address').removeAttr('maxlength')
$('#billing_address').removeAttr('pattern')
$('#billing_address').removeAttr('required')

$('#billing_city').removeAttr('minlength')
$('#billing_city').removeAttr('maxlength')
$('#billing_city').removeAttr('pattern')
$('#billing_city').removeAttr('required')

$('#billing_state').removeAttr('minlength')
$('#billing_state').removeAttr('maxlength')
$('#billing_state').removeAttr('pattern')
$('#billing_state').removeAttr('required')

$('#billing_zip').removeAttr('minlength')
$('#billing_zip').removeAttr('maxlength')
$('#billing_zip').removeAttr('pattern')
$('#billing_zip').removeAttr('required')

$('#billing_tel').removeAttr('maxlength' )
$('#billing_tel').removeAttr('pattern' )
$('#billing_tel').removeAttr('required' )

$('#billing_email').removeAttr('maxlength' )
$('#billing_email').removeAttr('pattern' )
$('#billing_email').removeAttr('required' )
}

function addvalidation(){
$('#billing_name').attr('minlength','2')
$('#billing_name').attr('maxlength','60')
$('#billing_name').attr('pattern','[A-Za-z ]*')
$('#billing_name').attr('required','true')

$('#billing_address').attr('minlength','10')
$('#billing_address').attr('maxlength','150')
$('#billing_address').attr('pattern','[A-Za-z0-9 ,-/(/)]*')
$('#billing_address').attr('required','true')

$('#billing_city').attr('minlength','2')
$('#billing_city').attr('maxlength','30')
$('#billing_city').attr('pattern','[A-Za-z ]*')
$('#billing_city').attr('required','true')

$('#billing_state').attr('minlength','2')
$('#billing_state').attr('maxlength','30')
$('#billing_state').attr('pattern','[A-Za-z ]*')
$('#billing_state').attr('required','true')

$('#billing_zip').attr('minlength','4')
$('#billing_zip').attr('maxlength','15')
$('#billing_zip').attr('pattern','[A-Za-z0-9 ]*')
$('#billing_zip').attr('required','true')

$('#billing_email').attr('maxlength','70')
$('#billing_email').attr('pattern','[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$')
$('#billing_email').attr('required','true')

$('#billing_tel').attr('maxlength','20')
$('#billing_tel').attr('pattern','[0-9]*')
$('#billing_tel').attr('required','true')
}

// function countrychange()
// {

//     var ele = document.getElementById('billing_country')

//     if (ele != null)
//     {
//         if (ele.value == 'India')
//         {
//             $('#billing_tel').prop('required', true);
//             document.getElementById('billing_tel_label').innerHTML = 'Billing Tel *'
//         }
//         else
//         {
//             $('#billing_tel').prop('required', false);
//             document.getElementById('billing_tel_label').innerHTML = 'Billing Tel'
//         }
//     }
// }
