import logging
from odoo import models, fields, api, _, exceptions



class rejuvenationpdfversion(models.Model):
    _name = 'rejuvenation.pdfversion'
    _description = 'Rejuvenation pdfversion'
    _rec_name = 'pdfrec_version'

#
    pdfrec_version=fields.Char(string="Form version")
    # fields_involved = fields.Many2many('ir.model.fields', 'mo_field_rel', 'prg_id', 'field_id',
    #               string = 'Fields Involved', help = "Choose the field",
    #               domain = "[('model', '=','program.registration')]")
    pdf_template=fields.Char(string='PDF Template Name')
    active_version=fields.Boolean('Active')


