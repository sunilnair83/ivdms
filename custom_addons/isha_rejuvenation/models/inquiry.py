from odoo import models, fields, api, exceptions
from . import commonmodels
from . import programtype
from . import programschedule
import time
import datetime

class CandidateInquiry(models.Model): 
	_name = 'rejuvenation.inquiry'
	_description = 'Rejuvenation Program Inquiry'
	_rec_name = 'first_name'

	first_name = fields.Char(string = 'First Name')
	last_name = fields.Char(string = 'Last Name')
	countrycode = fields.Char(string='Country Tel code')
	mobile = fields.Char(string = 'Mobile')
	email = fields.Char(string = 'Email')
	city = fields.Char(string='City', required=False)
	country = fields.Many2one('res.country', 'Country of residence')
	comments =  fields.Text(string = 'Comments')
	selected_program = fields.Many2one('rejuvenation.program.schedule',string = 'Selected Program')
	selected_programtype = fields.Many2one(related='selected_program.pgmschedule_programtype')
	inquiry_status =  fields.Selection([('Inquiry','Inquiry'),('Registered','Registered')], string='Status')

	ack_email_send = fields.Boolean(string="Acknowledge Email Send", default=False)
	ack_sms_send = fields.Boolean(string="Acknowledge SMS Send", default=False)

	@api.model
	def fields_get(self, fields=None):
		fields_to_show = ['first_name','last_name','mobile','email','comments','selected_program']
		res = super(CandidateInquiry, self).fields_get()
		for field in res:
			if (field in fields_to_show):
				res[field]['selectable'] = True	
				res[field]['sortable'] = True
			else:
				res[field]['selectable'] = False
				res[field]['sortable'] = False
		return res
		
	def update_registered(self):
		print('update triggered')
		nlist = self.env['rejuvenation.inquiry'].search([('inquiry_status','=','Inquiry')])
		if nlist:
			print('list found')
			for rec in nlist:
				print('rec found')
				print(rec)
				ids = self.env['program.registration'].search([('participant_email','=',rec.email),('participant_programtype','=',rec.selected_programtype.id),('create_date','>',rec.create_date)])
				if ids.exists():
					print('found party')
					rec.write({'inquiry_status':'Registered'})

	@api.model
	def create(self, vals):   
		rec = super(CandidateInquiry, self).create(vals)
		#self.env['rejuvenation.notification']._sendInquiryAcknowledgement(rec)
		return rec

	@api.model
	def sendInquiryAcknowledgement(self):
		reclist = self.env['rejuvenation.inquiry'].search([('inquiry_status','=','Inquiry'), 
		('ack_email_send','=', False), ('ack_sms_send','=', False)])
		for rec in reclist:
			self.env['rejuvenation.notification']._sendInquiryAcknowledgement(rec)
		print('completed sending inquiry acknowledgement')
		