from odoo import models, fields, api, exceptions
from odoo.exceptions import except_orm
from datetime import date, timedelta, datetime

class ProgramMatches(models.TransientModel):
	_name = 'program.matches'
	_description = 'Program Matches'

	programregistration = fields.Many2one('program.registration', string='program registration')
	programname = fields.Many2one('program.type', string='Program Type', required=True)
	pgmdate = fields.Date(string='Program Date',required=True)
	teachername = fields.Many2one('program.teachernames',string = 'Teacher Name', required=False)
	pgmcountry = fields.Many2one('res.country', string = 'Program Country', required=True)
	pgmstate = fields.Many2one('res.country.state', 'State/province')
	pgmlocation = fields.Many2one('isha.center', string = 'Program Location', required=True)

	def submit_selection(self):
		print('submit clicked')