from odoo import models, fields, api, exceptions
from odoo.exceptions import except_orm

class AllocateRooms(models.TransientModel):
	_name = 'allocaterooms'
	_description = 'Allocate Rooms'	
	#_inherit = 'program.registration'

	on_entry = fields.Char(default='Yes', store=True)

	programapplied = fields.Char(string = 'Program Schedule')
	selected_program = fields.Many2one('rejuvenation.program.schedule',string = 'Selected Program', required=True)
	list_seq = fields.Selection([('Room Allocation By Participant','Room Allocation By Participant'),('Room Allocation By Room Type','Room Allocation By Room Type')],required=True,default='Room Allocation By Participant')
	showbyroomtype = fields.Boolean('By Room Type', default=False)

	participant_list = fields.Many2many('program.party.data', string = 'Participants')
	parti_list = fields.Many2many('program.room.data', string = 'Participants')


	def load_roombaseddata(self,a_id):
		print('load program')
		#self.env['program.room.data'].search([('programname','=',a_id)]).unlink()
		rooms = self.env['rejuvenation.program.schedule.roomdata'].search([('pgmschedule_programname','=',a_id)])
		self.parti_list = [(5,0,0)]			
		if rooms:
			for rec in rooms:
				print(rec.pgmschedule_noofrooms)
				if (rec.pgmschedule_noofrooms == 1):
					id = self.env['program.registration'].search([('programapplied.id','=',a_id),('packageselection.id','=',rec.id),('allocated_roomnumber','=',rec.pgmschedule_roomnofrom1)])
					id1 = self.env['program.room.data'].search([('programname','=',a_id),('packageselection','=',rec.id),('allocated_roomnumber','=',rec.pgmschedule_roomnofrom1)])
					if id:
						if id1:
							id1.write({'primary_guest':id.id,'accompanying_guest':id.accompanying_name.id,'accompanying_guest_text':id.accompanying_name_text})
						else:
							nrec = self.env['program.room.data'].create({'programname':a_id,'packageselection':rec.id,'allocated_roomnumber':rec.pgmschedule_roomnofrom1,'room_no_from':rec.pgmschedule_roomnofrom1,'room_no_to':rec.pgmschedule_roomnoto,'primary_guest':id.id,'accompanying_guest':id.accompanying_name.id,'accompanying_guest_text':id.accompanying_name_text })
					else:
						if not id1:
							nrec = self.env['program.room.data'].create({'programname':a_id,'packageselection':rec.id,'allocated_roomnumber':rec.pgmschedule_roomnofrom1,'room_no_from':rec.pgmschedule_roomnofrom1,'room_no_to':rec.pgmschedule_roomnoto })
						else:
							id1.write({'primary_guest':None,'accompanying_guest':None,'accompanying_guest_text':None})
				else:
					for x in range(rec.pgmschedule_roomnofrom1,(rec.pgmschedule_roomnoto + 1)):
						id = self.env['program.registration'].search([('programapplied.id','=',a_id),('packageselection.id','=',rec.id),('allocated_roomnumber','=',x)])
						id1 = self.env['program.room.data'].search([('programname','=',a_id),('packageselection','=',rec.id),('allocated_roomnumber','=',x)])
						if id:
							if id1:
								id1.write({'primary_guest':id.id,'accompanying_guest':id.accompanying_name.id,'accompanying_guest_text':id.accompanying_name_text})
							else:
								nrec = self.env['program.room.data'].create({'programname':a_id,'packageselection':rec.id,'allocated_roomnumber':x,'room_no_from':rec.pgmschedule_roomnofrom1,'room_no_to':rec.pgmschedule_roomnoto,'primary_guest':id.id,'accompanying_guest':id.accompanying_name.id,'accompanying_guest_text':id.accompanying_name_text })
						else:
							if not id1:
								nrec = self.env['program.room.data'].create({'programname':a_id,'packageselection':rec.id,'allocated_roomnumber':x,'room_no_from':rec.pgmschedule_roomnofrom1,'room_no_to':rec.pgmschedule_roomnoto })
							else:
								id1.write({'primary_guest':None,'accompanying_guest':None,'accompanying_guest_text':None})

		self.parti_list = self.env["program.room.data"].search([('programname','=',a_id)])

		#self.env['program.party.data'].search([('programname','=',a_id)]).unlink()
		party = self.env["program.registration"].search([('programapplied.id','=',a_id),'|','|',('registration_status','=','Paid'),('registration_status','=','Confirmed'),('registration_status','=','Cancel Applied')])
		self.participant_list = [(5,0,0)]
		if party:
			print('party available')
			for rec in party:
				id = self.env['program.party.data'].search([('primary_guest','=',rec.id),('programname','=',a_id)])
				if not id:
					nrec = self.env['program.party.data'].create({'primary_guest':rec.id,'programname':a_id,'packageselection':rec.packageselection.id,'accompanying_guest':rec.accompanying_name.id,'accompanying_guest_text':rec.accompanying_name_text,'allocated_roomnumber':rec.allocated_roomnumber})		
				else:
					id.write({'packageselection':rec.packageselection.id,'accompanying_guest':rec.accompanying_name.id,'accompanying_guest_text':rec.accompanying_name_text,'allocated_roomnumber':rec.allocated_roomnumber})							

		self.participant_list = self.env["program.party.data"].search([('programname','=',a_id)])
				

	@api.onchange('programapplied')
	def programapplied_onchange(self):
		print('program applied')
		self.env['program.room.data'].search([]).unlink()
		self.env['program.party.data'].search([]).unlink()
		self.list_seq = 'Room Allocation By Participant'
		active_id = self.env.context.get('default_active_id')
		self.selected_program = self.env["rejuvenation.program.schedule"].browse(active_id)		

	@api.onchange('selected_program')
	def selected_program_onchange(self):
		active_id = self.selected_program.id				
		# self.participant_list = self.env["program.party.data"].search([('programname','=',active_id)])
		# self.parti_list = self.env["program.room.data"].search([('programname','=',active_id)])
		# if not self.participant_list or not self.parti_list:
		# 	print('no data') 
		self.load_roombaseddata(active_id)

	def apply_changes(self):
		print('Apply Submit')		
		print(self.list_seq)
		active_id = self.selected_program.id	
		if (self.list_seq == 'Room Allocation By Participant'):
			print('party type')
			for rec in self.participant_list:
				print(rec.primary_guest)
				acc_party = self.env["program.party.data"].search([('programname','=',active_id),('primary_guest','=',rec.primary_guest.id)])
				if acc_party:
					acc_party.write({'accompanying_guest':rec.accompanying_guest.id,'accompanying_guest_text':rec.accompanying_guest_text,'allocated_roomnumber':rec.allocated_roomnumber})
		else:
			print('room type')
			for rec in self.parti_list:
				print(rec.primary_guest)
				print(rec.allocated_roomnumber)
				acc_data = self.env["program.room.data"].search([('programname','=',active_id),('packageselection','=',rec.packageselection.id),('allocated_roomnumber','=',rec.allocated_roomnumber)])
				if acc_data:
					acc_data.write({'accompanying_guest':rec.accompanying_guest.id,'accompanying_guest_text':rec.accompanying_guest_text,'primary_guest':rec.primary_guest.id})					


	def transfer_changes(self):
		print('Transfer Submit')		
		print(self.list_seq)
		active_id = self.selected_program.id	
		if (self.list_seq == 'Room Allocation By Participant'):
			print('party type')
			self.parti_list = self.env["program.room.data"].search([('programname','=',active_id)])
			for rec in self.parti_list:
				print(rec.primary_guest)
				print(rec.allocated_roomnumber)
				print(rec.primary_guest.id)
				if rec.primary_guest.id:
					for x in self.participant_list:
						if (x.packageselection.id == rec.packageselection.id and x.primary_guest.id == rec.primary_guest.id):
							print('Yes')
							x.allocated_roomnumber = rec.allocated_roomnumber
							x.accompanying_guest = rec.accompanying_guest
							x.accompanying_guest_text = rec.accompanying_guest_text			
				else:
					for x in self.participant_list:
						if (x.packageselection.id == rec.packageselection.id and x.allocated_roomnumber == rec.allocated_roomnumber):
							x.allocated_roomnumber = None
							x.accompanying_guest = None
							x.accompanying_guest_text = None
		else:
			print('room type')
			self.participant_list = self.env["program.party.data"].search([('programname','=',active_id)])
			for rec in self.participant_list:
				print(rec.primary_guest)
				print(rec.allocated_roomnumber)
				if rec.allocated_roomnumber:
					for x in self.parti_list:
						if (x.packageselection.id == rec.packageselection.id and x.allocated_roomnumber == rec.allocated_roomnumber):
							print('Yes')
							x.primary_guest = rec.primary_guest
							x.accompanying_guest = rec.accompanying_guest
							x.accompanying_guest_text = rec.accompanying_guest_text
				else:
					for x in self.parti_list:
						if (x.packageselection.id == rec.packageselection.id and x.primary_guest.id == rec.primary_guest.id):
							x.primary_guest = None
							x.accompanying_guest = None
							x.accompanying_guest_text = None



	@api.onchange('list_seq')
	def list_seq_onchange(self):
		print(self.list_seq)
		print(self.on_entry)
		active_id = self.selected_program.id	
		self.apply_changes()
		self.transfer_changes()

		if (self.list_seq == 'Room Allocation By Participant'):
			print('by participant')
			self.showbyroomtype = False
		else:
			print('by room type')
			self.showbyroomtype = True

	def duplicate_check(self):
		print('Save Submit')		
		print(self.list_seq)
		active_id = self.selected_program.id
		if (self.list_seq == 'Room Allocation By Participant'):
			print('party type')
			for rec in self.participant_list:
				print(rec.primary_guest)
				dup_chk = False
				if rec.allocated_roomnumber:
					print(rec.room_no_from)
					print(rec.room_no_to)
					print(rec.allocated_roomnumber)
					
					if (rec.allocated_roomnumber >= rec.room_no_from and rec.allocated_roomnumber < (rec.room_no_to + 1)):
						for x in self.participant_list:
							if (x.id != rec.id and rec.allocated_roomnumber == x.allocated_roomnumber):
								print('duplicate found')
								dup_chk = True

						if 	dup_chk == True:			
							raise exceptions.ValidationError("Duplicate room number entered")
							return {
								"type": "ir.actions.do_nothing",
							}
					else:
						raise exceptions.ValidationError("Invalid room number entered")
						return {
							"type": "ir.actions.do_nothing",
						}
		else:
			print('room type')
			for rec in self.parti_list:
				print(rec.primary_guest)
				dup_chk = False
				if rec.primary_guest.id:
					for x in self.parti_list:
						if (x.id != rec.id and rec.primary_guest.id == x.primary_guest.id):
							print('duplicate found')
							dup_chk = True

					if 	dup_chk == True:			
						raise exceptions.ValidationError("Duplicate primary guest entered")
						return {
							"type": "ir.actions.do_nothing",
						}


	def submit_changes(self):
		print('Save Submit')		
		print(self.list_seq)
		active_id = self.selected_program.id
		self.duplicate_check()
		if (self.list_seq == 'Room Allocation By Participant'):
			print('party type')
			for rec in self.participant_list:
				print(rec.primary_guest)
				dup_chk = False
				if rec.allocated_roomnumber:
					for x in self.participant_list:
						if (x.id != rec.id and rec.allocated_roomnumber == x.allocated_roomnumber):
							print('duplicate found')
							dup_chk = True

					if 	dup_chk == False:			
						acc_data = self.env['program.registration'].browse(rec.primary_guest.id)
						if acc_data:
							acc_data.write({'accompanying_name':rec.accompanying_guest.id,'accompanying_name_text':rec.accompanying_guest_text,'allocated_roomnumber':rec.allocated_roomnumber})
				else:
					acc_data = self.env['program.registration'].browse(rec.primary_guest.id)
					if acc_data:
						acc_data.write({'accompanying_name':rec.accompanying_guest.id,'accompanying_name_text':rec.accompanying_guest_text,'allocated_roomnumber':rec.allocated_roomnumber})
							
		else:
			print('room type')
			for rec in self.parti_list:
				print(rec.primary_guest)
				print(rec.allocated_roomnumber)
				acc_data = self.env['program.registration'].browse(rec.primary_guest.id)
				if acc_data:
					acc_data.write({'accompanying_name':rec.accompanying_guest.id,'accompanying_name_text':rec.accompanying_guest_text,'allocated_roomnumber':rec.allocated_roomnumber})					
				else:
					print('no')
					acc_data = self.env['program.registration'].search([('programapplied','=',active_id),('packageselection','=',rec.packageselection.id),('allocated_roomnumber','=',rec.allocated_roomnumber)])
					for x in acc_data:
						print(x)
						x.write({'accompanying_name':None,'accompanying_name_text':None,'allocated_roomnumber':None})


 

class AllocateRoomModel(models.TransientModel):
	_name = 'allocateroommodel'
	_description = 'Allocate Rooms'	
	_inherit = 'program.registration'

	programapplied1 = fields.Char(string = 'Program Schedule')
	selected_program = fields.Many2one('rejuvenation.program.schedule',string = 'Selected Program', required=True)
	list_seq = fields.Selection([('Room Allocation By Participant','Room Allocation By Participant'),('Room Allocation By Room Type','Room Allocation By Room Type')],required=True,default='Room Allocation By Participant')


class ProgramRoomData(models.TransientModel):
	_name = 'program.room.data'
	_description = 'Program Room Data'
	_rec_name = 'allocated_roomnumber'
	#_sql_constraints = [('unique_program_room_data','unique(programname,primary_guest)','Multiple rooms allocated for a single person')]

	programname =  fields.Many2one('rejuvenation.program.schedule',string='Program Schedule')
	packageselection = fields.Many2one('rejuvenation.program.schedule.roomdata',string='Room Type')
	primary_guest = fields.Many2one('program.registration', string = 'Primary Guest',
	domain="[('programapplied','=',context.get('default_programname')),'|','|',('registration_status','=','Paid'),('registration_status','=','Confirmed'),('registration_status','=','Cancel Applied')]")
	accompanying_guest = fields.Many2one('program.registration', string = 'Accompanying Guest',
	domain="[('programapplied','=',context.get('default_programname')),'|','|',('registration_status','=','Paid'),('registration_status','=','Confirmed'),('registration_status','=','Cancel Applied')]")
	accompanying_guest_text = fields.Char(string = 'Accompanying Guest')
	allocated_roomnumber = fields.Integer(string = 'Room Number')
	room_no_from = fields.Integer(string = 'Room Number From')
	room_no_to = fields.Integer(string = 'Room Number To')
	room_allocated = fields.Boolean(compute="_compute_allocationflag",store=True)
	package_code = fields.Char(string="Package Code for Sorting",related="packageselection.pgmschedule_packagetype.packagecode")
	noofpersons = fields.Integer(string = 'No Of Persons', related="packageselection.pgmschedule_roomtype.noofpersons")

	@api.depends('allocated_roomnumber')
	def _compute_allocationflag(self):
		for rec in self:
			if rec.primary_guest:
				rec.room_allocated = True
			else:
				rec.room_allocated = False

	@api.constrains('primary_guest')
	def validate_room_primary_guest(self):
		for rec in self:
			if (rec.primary_guest.id and rec.accompanying_guest.id and rec.primary_guest.id == rec.accompanying_guest.id):
				raise exceptions.ValidationError("Primary guest and Accompanying guest should not be the same")


class ProgramParticipantData(models.TransientModel):
	_name = 'program.party.data'
	_description = 'Program Participant Data'
	_rec_name = 'primary_guest'
	#_sql_constraints = [('unique_program_party_data','unique(programname,allocated_roomnumber)','Single room allocated for multiple persons')]

	programname =  fields.Many2one('rejuvenation.program.schedule',string='Program Schedule')
	packageselection = fields.Many2one('rejuvenation.program.schedule.roomdata',string='Room Type')
	primary_guest = fields.Many2one('program.registration', string = 'Primary Guest',	
	domain="[('programapplied','=',context.get('default_programname')),'|','|',('registration_status','=','Paid'),('registration_status','=','Confirmed'),('registration_status','=','Cancel Applied')]")
	accompanying_guest = fields.Many2one('program.registration', string = 'Accompanying Guest',
	domain="[('programapplied','=',context.get('default_programname')),'|','|',('registration_status','=','Paid'),('registration_status','=','Confirmed'),('registration_status','=','Cancel Applied')]")
	accompanying_guest_text = fields.Char(string = 'Accompanying Guest')
	allocated_roomnumber = fields.Integer(string = 'Room Number')
	package_code = fields.Char(string="Package Code for Sorting",related="packageselection.pgmschedule_packagetype.packagecode")
	noofpersons = fields.Integer(string = 'No Of Persons', related="packageselection.pgmschedule_roomtype.noofpersons")
	room_no_from = fields.Integer(string = 'Room Number From',related="packageselection.pgmschedule_roomnofrom1")
	room_no_to = fields.Integer(string = 'Room Number To',related="packageselection.pgmschedule_roomnoto")

	@api.constrains('primary_guest')
	def validate_room_primary_guest(self):
		for rec in self:
			if (rec.primary_guest.id and rec.accompanying_guest.id and rec.primary_guest.id == rec.accompanying_guest.id):
				raise exceptions.ValidationError("Primary guest and Accompanying guest should not be the same in room")