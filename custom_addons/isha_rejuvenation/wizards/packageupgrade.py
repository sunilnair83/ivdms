from odoo import models, fields, api, exceptions, _
from odoo.exceptions import except_orm
from datetime import date, timedelta, datetime
import json
from werkzeug import urls

class Packageupgrade(models.TransientModel):
	_name = 'program.registration.packageupgrade'
	_description = 'Package Upgrade'

	participant_name = fields.Char(string = 'Participant Name', required=True)
	programapplied = fields.Many2one('rejuvenation.program.schedule',string = 'Applied Program', required=True)
	existing_package = fields.Char(string = 'Existing Package', required=True)
	existing_package_cost = fields.Float(string = 'Package Cost', required=True)
	new_package = fields.Many2one('rejuvenation.program.schedule.roomdata', string='Select New Package',
	domain="[('pgmschedule_programname','=',programapplied),('pgmschedule_packagetype.packagecost','>', context.get('default_package_cost'))]", required=True)
	new_package_cost = fields.Float(string = 'New Package Cost', required=True)

	seats_available_count = fields.Integer(string="Seats Available", default=0, store=True)
	seats_booked_count = fields.Integer(string="Seats Booked", default=0)
	seats_blocked_count = fields.Integer(string="Seats Blocked", default=0)
	seats_total = fields.Integer(string="Total", default=0)
	
	emergency_available_count = fields.Integer(string="Emergency Seats Available", default=0)
	emergency_booked_count = fields.Integer(string="Emergency Seats Booked", default=0)
	emergency_blocked_count = fields.Integer(string="Emergency Seats Blocked", default=0)
	emergency_total = fields.Integer(string="Emergency Seats Total", default=0)

	use_emrg_quota = fields.Boolean(string="Use Emergency Quota")
	show_seats = fields.Boolean(string="Show Seats", default=False)

	
	@api.onchange('participant_name')
	def participant_name_onchange(self):
		print('inside participant_name_onchange')
		active_id = self.env.context.get('default_active_id')
		rec = self.env["program.registration"].browse(active_id)
		if rec:
			self.programapplied = rec.programapplied
			self.existing_package = rec.packageselection.pgmschedule_packagetype.packagecode
			self.existing_package_cost = rec.packageselection.pgmschedule_packagetype.packagecost
			
	@api.onchange('new_package')
	def new_package_onchange(self):
		if (self.new_package):
			print('inside new_package_onchange')
			self.new_package_cost = self.new_package.pgmschedule_packagetype.packagecost
			
			self.show_seats = True
			self.use_emrg_quota = False
			result = self.env['rejuvenation.payments']._getSeatsAvailability(self.new_package)

			self.seats_available_count = result['seats_available_count']
			self.seats_booked_count = result['seats_booked_count']
			self.seats_blocked_count = result['seats_blocked_count']
			self.seats_total = result['seats_total']
			
			self.emergency_available_count = result['emergency_available_count']
			self.emergency_booked_count = result['emergency_booked_count']
			self.emergency_blocked_count = result['emergency_blocked_count']
			self.emergency_total = result['emergency_total']

	#
	def submit_packageupgrade(self):

		active_id = self.env.context.get('default_active_id')
		ids = self.env["program.registration"].browse(active_id)

		for id in ids:

			count = self.env['program.registration.transactions'].search_count([('programregistration.id','=', active_id),('isactive','=', True)])
			if (count != 0):
				raise exceptions.ValidationError('Previous package upgrade request is pending for this participant..')

			if (id.registration_status != 'Paid' and id.registration_status != 'Confirmed'):
				raise exceptions.ValidationError('Invalid registration status, package upgrade is not allowed..')
			elif (id.payment_status == 'Initiated'):
				raise exceptions.ValidationError('Payment have been initiated by the participant. Please try after some time')
			elif (not self.new_package):
				raise exceptions.ValidationError('New package not selected')
			else:

				result = self.env['rejuvenation.commonvalidations'].checkPackageUpgradeEligiblity(id)
				if (result == False):
					raise exceptions.ValidationError('Package upgrade not allowed')
				
				result = self.env['rejuvenation.payments']._getSeatsAvailability(self.new_package)

				if (result['seats_available_count'] <= 0):
					if(self.use_emrg_quota):
						if (result['emergency_available_count'] <= 0):
							raise exceptions.ValidationError('Seats not available')	
					else:
						raise exceptions.ValidationError('Seats not available')

				base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
				upgrade_access_token = self.env['rejuvenation.payments']._get_default_access_token()
				upgrade_url = urls.url_join(base_url, "ishahealthsolutions/registration/packageupgrade/%s" % (upgrade_access_token))

				self.env['rejuvenation.payments']._blockSeat(self.use_emrg_quota, self.new_package)

				#

				newtransaction = self.env['program.registration.transactions'].create(
					{
						'programregistration': active_id,
						'transactiontype': 'Upgrade',
						'upgrade_url':upgrade_url,
						'upgrade_access_token' : upgrade_access_token,
						'upgrade_packageselection': self.new_package.id,
						'upgrade_applied' : True,
						'upgrade_applied_dt': datetime.now(),
						'upgrade_email_send': False,
						'upgrade_sms_send' : False,
						'upgrade_use_emrg_quota': self.use_emrg_quota,
						'isactive': True,
						'comments': 'New upgrade request',
						'last_modified_dt': datetime.now()
					}
				)

				#

				id.write({
					'orderid': None,
					'billinginfoconfirmation': None,
					'billing_name': None,
					'billing_address': None,
					'billing_city': None,
					'billing_state': None,
					'billing_zip': None,
					'billing_country': None,
					'billing_tel': None,
					'billing_email': None,
					'payment_status' : None,
					'amount_paid': 0,
					'date_paid': None,
					'ereceiptgenerated': None,
					'ereceiptreference' : None,
					'ereceipturl' : None,
					'payment_initiated_dt': None
				})
				
				self.env['rejuvenation.notification']._sendRegistrationPackageUpgradeEmailSMSSend(newtransaction)
				
				print('completed placing the new request')
				
		return {'type':'ir.actions.act_window_close'}
