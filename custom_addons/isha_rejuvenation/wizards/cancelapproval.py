from odoo import models, fields, api, exceptions
from odoo.exceptions import except_orm
from datetime import date, timedelta, datetime

class CancelApproval(models.TransientModel):
	_name = 'program.registration.cancel'
	_description = 'Cancel Approval'

	participant_name = fields.Char(string = 'Participant Name')
	programapplied = fields.Many2one('rejuvenation.program.schedule',string = 'Applied Program')
	program_startdate = fields.Date(string = 'Start Date')
	program_enddate = fields.Date(string = 'End Date')
	selected_program = fields.Many2one('rejuvenation.program.schedule',string = 'Available Programs')
	selected_package = fields.Many2one('rejuvenation.program.schedule.roomdata', string='Requested Package')
	country = fields.Many2one('res.country', 'Country of residence')

	ha_status = fields.Selection([('Pending','Pending'),('HA Approved','HA Approved'),('Rejected','Rejected'),('Request More Info','Request More Info'),('HA Resubmitted','HA Resubmitted')], string='HA Status', default='Pending', size=100)
	registration_status = fields.Selection([('Pending','Pending'),('Rejected','Rejected'),('Payment Link Sent','Payment Link Sent'),('Add To Waiting List','Add To Waiting List'),('Cancel Applied','Cancel Applied'),('Cancel Approved','Cancel Approved'),('Paid','Paid'),('Confirmed','Confirmed'),('Approved','Approved'),('Withdrawn','Withdrawn'),('Refunded','Refunded')], string='Registration Status',  size=100, default='Pending')

	#approval fields
	seats_available = fields.Char(string="Seats Available")
	oco_status = fields.Char(string="OCO Status")
	oco_id = fields.Integer(string="OCO Id")
	bl_status = fields.Char(string="Blacklisted")
	prerequisite_pgm = fields.Char(string="Pre-requisite Practices")
	package_availablity = fields.Integer(string="Room Availablity")

	use_emrg_quota =  fields.Boolean(string="Use Emergency Quota")
	oco_approval_overide =  fields.Boolean(string="Override")
	blacklisted_overide =  fields.Boolean(string="Override")
	prerequisite_pgm_overide =  fields.Boolean(string="Override")

	show_submit = fields.Boolean(string="Show Submit", default=True)
	show_refund = fields.Boolean(string="Show Submit", default=False)
	
    # other status fields
	registration_email_send = fields.Boolean(string="registration_email_send")
	rejection_email_send = fields.Boolean(string="rejection_email_send")
	harequest_email_send = fields.Boolean(string="harequest_email_send")
	cancel_email_send = fields.Boolean(string="cancel_email_send")
    #
	waitinglist_date = fields.Date()
	waitinglist_email_count = fields.Integer(default=0)
	waitinglist_period_inmonth = fields.Integer(default=0)
	waitinglist_last_communicate_sch = fields.Many2one('rejuvenation.program.schedule')
    #
	paymentlink_email_send = fields.Boolean(string="paymentlink_email_send")
	auto_approval_comments = fields.Char("auto_approval_comments", size=100)
    #
	registration_agreed = fields.Boolean(string="I Agree")    
	amount_paid  = fields.Float(string = 'Amount Paid')
	date_paid =  fields.Date(string="Date Of Payment")
	date_confirm  =  fields.Date()

    #
	date_withdrawn = fields.Date(string="Cancel Requested On")
	date_cancelapproved = fields.Date("Cancel Approved On")
	refund_eligible_ondaysbefore = fields.Integer(string = 'Days Before')
	refund_eligible_onpercentage = fields.Float(string = 'Refund Eligible Percentage') 
	refund_eligible = fields.Float(string = 'Refund Amount Eligible')   
	refund_finance_approval = fields.Boolean(string='Refund has been approved by finance team')

	#
	date_refund = fields.Date(string="Refund Done On")
	refund_amount = fields.Float(string = 'Actual Refunded Amount')
	refund_comments = fields.Text(string = 'Comments')

	#
	prog_id = fields.Integer()

	@api.onchange('participant_name')
	def participant_name_onchange(self):
		print('welcome to cancel approval')
		active_id = self.env.context.get('default_active_id')
		party = self.env["program.registration"].browse(active_id)
		if party:
			self.programapplied = party.programapplied
			self.selected_package = party.packageselection
			self.addinfo_needed = party.addinfo_needed
			self.country = party.country
			self.ha_status = party.ha_status
			self.registration_status = party.registration_status
			self.rejection_reason = party.rejection_reason
			self.status_comments = party.status_comments
			self.waitinglist_date = party.waitinglist_date
			self.registration_email_send = party.registration_email_send
			self.rejection_email_send = party.rejection_email_send
			self.harequest_email_send = party.harequest_email_send
			self.paymentlink_email_send = party.paymentlink_email_send
			self.amount_paid = party.amount_paid
			self.date_paid = party.date_paid
			self.date_cancelapproved = party.date_cancelapproved
			self.date_withdrawn = party.date_withdrawn
			self.refund_eligible_ondaysbefore = party.refund_eligible_ondaysbefore
			self.refund_eligible_onpercentage = party.refund_eligible_onpercentage
			self.refund_eligible = party.refund_eligible
			self.cancel_email_send = party.cancel_email_send
			self.program_startdate = party.programapplied.pgmschedule_startdate
			self.program_enddate = party.programapplied.pgmschedule_enddate
			self.prog_id = party.programapplied.id
			self.date_refund = party.date_refund
			self.refund_amount = party.refund_amount
			self.refund_comments = party.refund_comments
			
			if (party.registration_status == 'Cancel Approved'):				
				self.show_submit = False
				self.show_refund = True
			elif (party.registration_status == 'Refunded'):
				print('cancel refund screen')
				self.show_submit = False
				self.show_refund = True

		#prg_id = self.env['rejuvenation.program.schedule'].search([('pgmschedule_programname','=',self.programapplied)])

	def submit_cancelapproval(self):	
		active_id = self.env.context.get('default_active_id')
		ids = self.env["program.registration"].browse(active_id)
		for id in ids:
			id.write({
				'registration_status':'Cancel Approved',
				'cancelapproved_email_send':False,
				'cancelapproved_sms_send':False,
				'date_cancelapproved':fields.Date.today(),
				'last_modified_dt': datetime.now()
			})
			self.env['rejuvenation.payments']._reverseSeat(id)
			#email and sms trigger
			self.env['rejuvenation.notification']._sendCancellationMailAndSMS(id.id,'Cancel Approved')	

	def submit_refund(self):
		if (self.refund_amount > self.amount_paid):
			raise exceptions.ValidationError("Refund amount is more than the amount paid")
		
		active_id = self.env.context.get('default_active_id')
		ids = self.env["program.registration"].browse(active_id)
		for id in ids:
			id.write({
				'registration_status':'Refunded',
				'refund_amount':self.refund_amount,
				'refund_comments':self.refund_comments,
				'cancelrefund_email_send':False,
				'cancelrefund_sms_send':False,				
				'date_refund':fields.Date.today(),
				'last_modified_dt': datetime.now()
			})

			#email and sms trigger
			self.env['rejuvenation.notification']._sendCancellationMailAndSMS(id.id,'Cancel Refunded')	

