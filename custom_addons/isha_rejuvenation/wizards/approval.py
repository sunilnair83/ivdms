from odoo import models, fields, api, exceptions
from odoo.exceptions import except_orm
from datetime import date, timedelta, datetime

class Approval(models.TransientModel):
	_name = 'program.registration.approval'
	_description = 'Approval'

	participant_name = fields.Char(string = 'Participant Name')
	programapplied = fields.Many2one('rejuvenation.program.schedule',string = 'Applied Program')
	selected_program = fields.Many2one('rejuvenation.program.schedule',string = 'Available Programs')
	selected_package = fields.Many2one('rejuvenation.program.schedule.roomdata', string='Requested Package',store=True)
	country = fields.Many2one('res.country', 'Country of Residence')
	nationality_id = fields.Many2one('res.country', 'Nationality')
	payment_needed=fields.Boolean(string="Payment Relaxation",default=False)
	ha_status = fields.Selection([('Pending','Pending'),('HA Approved','HA Approved'),('Rejected','Rejected'),('Request More Info','Request More Info'),('HA Resubmitted','HA Resubmitted')], string='HA Status', default='Pending', size=100)
	registration_status = fields.Selection([('Pending','Pending'),('Rejected','Rejected'),('Rejection Review','Rejection Review'),('Payment Link Sent','Payment Link Sent'),('Add To Waiting List','Add To Waiting List'),('Cancel Applied','Cancel Applied'),('Cancel Approved','Cancel Approved'),('Paid','Paid'),('Confirmed','Confirmed'),('Approved','Approved'),('Withdrawn','Withdrawn'),('Refunded','Refunded'), ('Change Of Participant', 'Change Of Participant')], string='Registration Status',  size=100, default='Pending')

	#approval fields
	seats_available = fields.Char(string="Seats Available", store=True)
	seats_available_count = fields.Integer(string="Seats Available", default=0, store=True)
	seats_booked_count = fields.Integer(string="Seats Booked", default=0)
	emergency_booked_count = fields.Integer(string="Emergency Seats Booked", default=0)
	oco_status = fields.Char(string="OCO Status", store=True)
	oco_id = fields.Integer(string="OCO Id", store=True)
	bl_status = fields.Char(string="Blacklisted", store=True)
	prerequisite_pgm = fields.Char(string="Pre-req practices as per user", store=True)
	prerequisite_ishadb_pgm = fields.Char(string="Pre-req practices as per ishaDB", default="Not-Matched", store=True)
	room_availablity = fields.Integer(string="Rooms Availablity", store=True)
	room_reserved = fields.Integer(string="Rooms Reserved", default=0, store=True)

	use_emrg_quota =  fields.Boolean(string="Use Emergency Quota")
	oco_approval_overide =  fields.Boolean(string="Override")
	blacklisted_overide =  fields.Boolean(string="Override")
	prerequisite_pgm_overide =  fields.Boolean(string="Override")
	prerequisite_pgm_overide_hide =  fields.Boolean(string="Override")

	show_oco_approval = fields.Boolean(string="Show OCO Approval", default=True)
	show_submit = fields.Boolean(string="Show Submit", default=True)
	show_qa = fields.Boolean(string="Show QA", default=False)
	show_submit_group = fields.Boolean(string="Show Submit Group", default=True)
	show_rejection = fields.Boolean(string="Show Rejection", default=True)
	
	#labels
	lbl_useemrquota  = fields.Char(string = ' ', size=100, readonly="1", default='Use Emergency Quota')
	lbl_override  = fields.Char(string = ' ', size=100, readonly="1", default='Override')
	selected_status = fields.Selection([('HA Approved','HA Approved'),('Rejected','Rejected'), ('Request More Info','Request More Info'),('Add To Waiting List','Add To Waiting List')], string='Change Status', size=100, required=True)

	#question_bank = fields.One2many('participant.qa', 'participant_name', string = 'HA Assessment QA')
	question_ids = fields.Many2many('rejuvenation.questionbank',string='Questions To The Participant')
	question_bank = fields.Many2many('participant.qa',string = 'HA Assessment QA')
	addinfo_needed =  fields.Text(string = 'Additional Info Needed', size=500)
	addinfo_needed_answer =  fields.Text(string = 'Additional Info Needed')
	ha_attachment_ids = fields.One2many('request.attachment', 'generic_request_id', string="HA Report(s)")
	rejection_reason =  fields.Many2one('rejuvenation.rejection.reason', string='Rejection Reason', size=500)
	status_comments =  fields.Text(string = 'Comments', size=500)

	waitinglist_date = fields.Date()

	registration_email_send = fields.Boolean(string="registration_email_send", default=False)
	rejection_email_send = fields.Boolean(string="rejection_email_send", default=False)
	harequest_email_send = fields.Boolean(string="harequest_email_send", default=False)
	paymentlink_email_send = fields.Boolean(string="paymentlink_email_send", default=False)

	auto_approval_comments = fields.Char("Auto Approval Comments", size=100)
	show_allocate_room = fields.Boolean(compute="_compute_roomallocateflag")

	## HA Reports
	ha_reports = fields.Many2many('participant.hadocs',string="HA Report")
	emergency_seatsavailable = fields.Integer(string="Emergency Seats Available", default=0)

	@api.depends('programapplied')
	def _compute_roomallocateflag(self):
		print('room allocate validate')
		self.show_allocate_room = False
		parties = self.env["program.registration"].search([('programapplied','=',self.programapplied.id),'|','|',('registration_status','=','Paid'),('registration_status','=','Confirmed'),('registration_status','=','Cancel Applied')])
		if parties:
			print(parties)
			self.show_allocate_room = True
		# active_id = self.env.context.get('default_active_id')
		# ids = self.env["program.registration"].browse(active_id)
		# for rec in ids:
		# 	print(rec)
		# 	self.show_allocate_room = False
		# 	parties = self.env["program.registration"].search([('programapplied','=',rec.programapplied.id),'|','|',('registration_status','=','Paid'),('registration_status','=','Confirmed'),('registration_status','=','Cancel Applied')])
		# 	if parties:
		# 		print(parties)
		# 		rec.show_allocate_room = True


	def open_room_allocate_act_from_approval(self):
		#self.submit_approval()
		print('room allocate')
		active_id = self.env.context.get('default_active_id')
		ids = self.env["program.registration"].browse(active_id)
		prgid = False
		for rec in ids:
			prgid = rec.programapplied

		print(prgid)

		return {
            'name':'Allocate Rooms',
			'context':{'default_active_id': active_id, 'default_programapplied': prgid },
            'view_type': 'form',
            'res_model': 'allocaterooms',
			'src_model': 'program.registration.approval',
            'view_id': False,
            'view_mode': 'form',
            'type': 'ir.actions.act_window',
			'target':'new'
        }

	def load_bl_possible_matches(self):
		return {
            'name':'Blacklisted - Matched',
            'view_type': 'form',
            'res_model': 'bl.matches',
			'src_model': 'program.registration.approval',
            'view_id': False,
            'view_mode': 'tree',
            'type': 'ir.actions.act_window',
			'target':'new'
        }

	def load_prg_possible_matches(self):
		return {
            'name':'Programs - Matched',
            'view_type': 'form',
            'res_model': 'program.matches',
			'src_model': 'program.registration.approval',
            'view_id': False,
            'view_mode': 'tree',
            'type': 'ir.actions.act_window',
			'target':'new'
        }

	@api.onchange('participant_name')
	def participant_name_onchange(self):
		print('inside participant_name_onchange')
		active_id = self.env.context.get('default_active_id')
		party = self.env["program.registration"].browse(active_id)
		if party:
			self.programapplied = party.programapplied
			self.selected_package = party.packageselection
			self.seats_available = party.seats_available
			self.oco_status = party.oco_status
			self.bl_status = party.bl_status
			self.prerequisite_pgm = party.prerequisite_pgm
			self.use_emrg_quota = party.use_emrg_quota
			self.oco_approval_overide = party.oco_approval_overide
			self.blacklisted_overide = party.blacklisted_overide
			self.prerequisite_pgm_overide = party.prerequisite_pgm_overide
			self.question_bank = party.question_bank
			self.addinfo_needed_answer = party.addinfo_needed_answer
			self.addinfo_needed = party.addinfo_needed
			self.country = party.country
			self.nationality_id  = party.nationality_id
			self.ha_status = party.ha_status
			self.registration_status = party.registration_status
			self.rejection_reason = party.rejection_reason
			self.status_comments = party.status_comments
			self.waitinglist_date = party.waitinglist_date
			self.registration_email_send = party.registration_email_send
			self.rejection_email_send = party.rejection_email_send
			self.harequest_email_send = party.harequest_email_send
			self.paymentlink_email_send = party.paymentlink_email_send
			self.auto_approval_comments = party.auto_approval_comments
			self.question_ids = party.question_ids

			self.ha_reports = self.env['participant.hadocs'].search([('participant','=',party.id)])

			if (self.country.name != 'India' or self.nationality_id.name != 'India'):
				self.show_oco_approval = True
			else:
				self.show_oco_approval = False

			# if (self.registration_status == 'Rejection Review'):
			# 	self.selected_status = 'Rejection Review'
			# elif (self.ha_status == 'HA Approved'):
			# 	self.selected_status = 'HA Approved'
			# elif (self.ha_status == 'Rejected'):
			# 	self.selected_status = 'Rejected'
			if (self.ha_status == 'Request More Info'):
				#self.selected_status = 'Request More Info'
				self.show_qa = True
			# elif (self.registration_status == 'Add To Waiting List'):
			# 	self.selected_status = 'Add To Waiting List'
			elif (self.ha_status == 'HA Resubmitted'):
				#self.selected_status = 'HA Resubmitted'
				self.show_qa = True

			if (party.ischangeofparticipant == True):
				if (party.ha_status == 'Rejected' or party.registration_status == 'Add To Waiting List'):
					self.show_submit = False

			if (self.registration_status == 'Paid' or self.registration_status == 'Confirmed'
				or self.registration_status == 'Change Of Participant'
				or self.registration_status == 'Cancel Applied'
				or self.registration_status == 'Cancel Approved'
				or self.registration_status == 'Refunded'
				or self.registration_status == 'Withdrawn'
				or self.registration_status == 'Incomplete'
				):
				self.show_submit = False
			elif (party.programapplied.pgmschedule_startdate < fields.Date.today()):
				print('date part')
				print(fields.Date.today())
				print(party.programapplied.pgmschedule_startdate)
				self.show_submit = False

			if (self.ha_status == 'Rejected'):
				self.show_rejection = True
			else:
				self.show_rejection = False
			
			#seat availability check
			seatcount = party.packageselection.pgmschedule_packagenoofseats - party.packageselection.pgmschedule_regularseatspaid
			#if (party.packageselection.pgmschedule_regularseatsbalance > 0):
			self.seats_available_count = seatcount
			self.seats_booked_count = party.packageselection.pgmschedule_regularseatspaid
			if (seatcount > 0):
				self.seats_available = 'Yes'
			else:
				self.seats_available = 'No'

			#emergency seats availability check
			self.emergency_seatsavailable = (party.packageselection.pgmschedule_packageemergencyseats - party.packageselection.pgmschedule_emergencyseatspaid)
			self.emergency_booked_count = party.packageselection.pgmschedule_emergencyseatspaid
			#room availablity check
			#self.room_availablity = party.packageselection.pgmschedule_balancerooms
			tot_rooms_needed = party.packageselection.pgmschedule_regularseatspaid + party.packageselection.pgmschedule_emergencyseatspaid
			tot_rooms_inpackage = party.packageselection.pgmschedule_noofrooms
			if (tot_rooms_inpackage >= tot_rooms_needed):
				self.room_availablity = tot_rooms_inpackage - tot_rooms_needed
			else:
				self.room_availablity = 0

			self.room_reserved = tot_rooms_needed

			practices_done = []
			practices_needed = []
			ncount = 0
			dcount = 0
			isnone = 0

			# get required PRE-REQUISITE programs from program type definition
			required_programs = [
				x.programcategory for x in
				party.programapplied.pgmschedule_programtype.prerequisiteprogram
			]

			# get actual programs from participant's program history
			actual_user_given_programs = [
				x.programcategory for x in
				party.programhistory.programname
			]

			ishangam_attendance_records = party.getProgramAttendance(party.contact_id_fkey.id)

			actual_ishangam_programs = [
				x.pgm_type_master_id.category for x in
				ishangam_attendance_records
			]

			# check if required and actuals tally
			matched = "Matched"
			if required_programs:
				match_result = set(required_programs) - set(actual_user_given_programs)

				if match_result:
					matched = "Not-Matched"

			self.prerequisite_pgm = matched

			# check if required and actual Ishangam programs tally
			matched = "Matched"
			if required_programs:
				match_result = set(required_programs) - set(actual_ishangam_programs)

				if match_result:
					matched = "Not-Matched"

			self.prerequisite_ishadb_pgm = matched

			if party.programapplied:
				prgid = self.env['rejuvenation.program.schedule'].browse(party.programapplied.id)
				if prgid:
					if prgid.attcompletionflag == False:
						self.show_allocate_room = True
			
			if (self.show_submit == False and self.show_qa == False and
				self.ha_status != 'Request More Info' and self.ha_status != 'Rejection Review' 
				and self.ha_status != 'Rejected' and self.ha_status != 'HA Resubmitted'):
				self.show_submit_group = False
			else:
				self.show_submit_group = True

		print('completed participant_name_onchange')


	@api.onchange('prerequisite_ishadb_pgm')
	def allow_ha_approval(self):
		if(self.prerequisite_ishadb_pgm != 'Matched'):
			self.prerequisite_pgm_overide_hide = False;
		else:
			self.prerequisite_pgm_overide_hide = True;

	@api.onchange('selected_status')
	def selected_status_onchange(self):
		print('status on change - ' + str(self.selected_status))
		if (self.selected_status == 'Request More Info' or self.selected_status == 'HA Resubmitted'):
			self.show_qa = True
		else:
			self.show_qa = False

		if (self.selected_status == 'Rejected' or (self.selected_status == False and self.ha_status == 'Rejected')):
			self.show_rejection = True
		else:
			self.show_rejection = False


	def validate_approval(self):
		print('inside validate approval')
		active_id = self.env.context.get('default_active_id')
		ids = self.env["program.registration"].browse(active_id)
		can_allow = True
		seatsavailable = True
		no_room = False
		for rec in ids:
			self.programapplied = rec.programapplied
			print(self.programapplied)
			sts_change = False
			if (rec.ha_status != self.selected_status):
				sts_change = True
			elif (self.selected_status == 'HA Approved' and (rec.registration_status == 'Rejection Review' or rec.registration_status == 'Pending')):
				print('yes ')
				sts_change = True
				#if (self.use_emrg_quota != rec.use_emrg_quota or self.oco_approval_overide != rec.oco_approval_overide or self.blacklisted_overide != rec.blacklisted_overide or self.prerequisite_pgm_overide != rec.prerequisite_pgm_overide):

			if (sts_change == False):
				continue
			elif (self.selected_status == 'HA Approved'):
				print(self.programapplied)
				if (self.use_emrg_quota == True):
					print('emg quota check')
					bal_seats = rec.packageselection.pgmschedule_packageemergencyseats - rec.packageselection.pgmschedule_emergencyseatspaid
					print(rec.packageselection.pgmschedule_packageemergencyseats)
					print(rec.packageselection.pgmschedule_emergencyseatspaid)
					print(rec.packageselection.pgmschedule_emergencyseatsbalance)
					if (bal_seats == 0):
						can_allow = False
				else:
					seatcount = rec.packageselection.pgmschedule_packagenoofseats - rec.packageselection.pgmschedule_regularseatspaid
					if (seatcount <= 0):
						seatsavailable = False

				tot_rooms_needed = rec.packageselection.pgmschedule_regularseatspaid + rec.packageselection.pgmschedule_emergencyseatspaid
				tot_rooms_inpackage = rec.packageselection.pgmschedule_noofrooms
				if (tot_rooms_inpackage >= tot_rooms_needed):
					self.room_availablity = tot_rooms_inpackage - tot_rooms_needed
				else:
					self.room_availablity = 0

				print(self.programapplied)
				if (self.room_availablity == 0):
					no_room = True

		if (self.use_emrg_quota == True):
			if (can_allow == False):
				raise exceptions.ValidationError("HA Approval failed. Emegency quota is full..")
		else:
			if (seatsavailable == False):
				raise exceptions.ValidationError("HA Approval failed. Seats are full..")

		if (self.selected_status == 'Request More Info' ):
			if (not self.question_ids and not self.addinfo_needed):
				raise exceptions.ValidationError("Select atleast one health assessment question")

		#elif (no_room == True):
			#raise exceptions.ValidationError("HA Approval failed. Rooms not available..")

	def submit_approval(self):
		print('inside submit approval')

		self.validate_approval()

		print('after succeeding validation')
		active_id = self.env.context.get('default_active_id')
		ids = self.env["program.registration"].browse(active_id)

		statuschanged = False
		registration_status = ''

		for rec in ids:

			print('current status: ' + rec.ha_status)
			print('new status: ' + self.selected_status)
			print('reg status: ' + rec.registration_status)
			print('seats_available: ' + self.seats_available)

			sts_change = False
			if (rec.ha_status != self.selected_status):
				sts_change = True
			elif (rec.ha_status == self.selected_status and self.selected_status != 'Request More Info' and rec.registration_status != 'Rejection Review' and rec.registration_status != 'Add To Waiting List'):
				if (self.use_emrg_quota != rec.use_emrg_quota or self.oco_approval_overide != rec.oco_approval_overide or self.blacklisted_overide != rec.blacklisted_overide or self.prerequisite_pgm_overide != rec.prerequisite_pgm_overide):
					sts_change = True
			elif ((self.selected_status == 'HA Approved' and rec.registration_status == 'Rejection Review') or
				(self.selected_status == 'HA Approved' and rec.registration_status == 'Add To Waiting List')) :
				sts_change = True
			elif (self.selected_status == 'Request More Info'):
				sts_change = True

			if (sts_change == False):
				print('ha status not changed')
				rec.write({
				'seats_available':self.seats_available,
				'prerequisite_pgm':self.prerequisite_pgm,
				'use_emrg_quota':self.use_emrg_quota,
				'oco_approval_overide':self.oco_approval_overide,
				'blacklisted_overide':self.blacklisted_overide,
				'prerequisite_pgm_overide':self.prerequisite_pgm_overide,
				'addinfo_needed':self.addinfo_needed,
				'rejection_reason':self.rejection_reason,
				'status_comments':self.status_comments,
				'question_ids':self.question_ids
				})
			else:
				print('status changed')


				if (rec.payment_status == 'Initiated'):
					raise exceptions.ValidationError('Payment have been initiated by the participant. Please try after some time..')

				statuschanged = True
				registration_status = 'Pending'
				ha_status = rec.ha_status
				waitinglist_date = rec.waitinglist_date
				auto_approval_comments = ''
				rejection_reason = None
				status_comments = ''

				if (self.selected_status == 'HA Approved'):
					ha_status = 'HA Approved'
					registration_status = 'Approved'

					if (rec.country.name != 'India' or rec.nationality_id.name != 'India'):
						print('checking for overseas')
						if (rec.oco_status == 'Pending' and self.oco_approval_overide == False):
							registration_status = 'Pending'
						elif (rec.oco_status != 'Approved' and self.oco_approval_overide == False):
							auto_approval_comments = 'Rejected due to oco rejection'
							registration_status = 'Rejection Review',
							print(auto_approval_comments)

					if (registration_status == 'Approved'):
						if (rec.bl_status == 'Pending' and self.blacklisted_overide == False):
							registration_status = 'Pending'
						elif (rec.bl_status != 'Verified' and self.blacklisted_overide == False):
							registration_status = 'Rejection Review'
							auto_approval_comments = 'Rejected due to bl rejection'
							print(auto_approval_comments)

					if (registration_status == 'Approved'):
						if (self.prerequisite_pgm != 'Matched' and self.prerequisite_pgm_overide == False):
							print('pre request not matched marking rejection review')
							auto_approval_comments = 'Rejected due to pre-requisite not matched'
							registration_status = 'Rejection Review'

					if (registration_status == 'Approved'):
						if (self.seats_available != 'Yes' and self.use_emrg_quota == False):
							auto_approval_comments = 'Rejected due to seats not available'
							registration_status = 'Rejection Review'
							print('seats not available rejection review marked')
						elif (self.seats_available != 'Yes' and self.use_emrg_quota == True):
							bal_seats = rec.packageselection.pgmschedule_packageemergencyseats - rec.packageselection.pgmschedule_emergencyseatspaid
							if (bal_seats == 0):
								auto_approval_comments = 'Rejected due to emergency seats not available'
								registration_status = 'Rejection Review'
								print('emergency seats not available rejection review marked')

					'''if (registration_status == 'Approved'):
						if (self.room_availablity == 0):
							auto_approval_comments = 'Rejected due to rooms not available'
							registration_status = 'Rejection Review'
							print('rooms not available rejection review marked')'''

				elif (self.selected_status == 'Rejected'):
					ha_status = 'Rejected'
					registration_status = 'Rejected'
					rejection_reason = self.rejection_reason
					status_comments = self.status_comments
				elif (self.selected_status == 'Request More Info'):
					ha_status = 'Request More Info'
				elif (self.selected_status == 'Add To Waiting List'):
					registration_status = 'Add To Waiting List'
					waitinglist_date = fields.Date.today()
				pay_need=False
				if (self.payment_needed == True):
					pay_need=True
				rec.write({
					'seats_available':self.seats_available,
					'prerequisite_pgm':self.prerequisite_pgm,
					'use_emrg_quota':self.use_emrg_quota,
					'oco_approval_overide':self.oco_approval_overide,
					'blacklisted_overide':self.blacklisted_overide,
					'prerequisite_pgm_overide':self.prerequisite_pgm_overide,
					'addinfo_needed':self.addinfo_needed,
					'ha_status':ha_status,
					'registration_status':registration_status,
					'rejection_reason':rejection_reason,
					'status_comments':status_comments,
					'waitinglist_date':waitinglist_date,
					'question_ids':self.question_ids,
					'rejection_email_send': False,
					'rejection_sms_send': False,
					'harequest_email_send': False,
					'harequest_sms_send': False,
					'cancel_email_send': False,
					'waitinglist_email_count': 0,
					'waitinglist_period_inmonth': 0,
					'waitinglist_email_send': False,
					'waitinglist_sms_send': False,
					'waitinglist_last_communicate_sch': None,
					'waitinglist_last_communicate_dt': None,
					'auto_approval_comments' : auto_approval_comments,
					'paymentlink_email_send': False,
					'paymentlink_sms_send' : False,
					'addtowaitlist_email_send': False,
					'addtowaitlist_sms_send': False,
					'cancelapplied_email_send':False,
					'cancelapproved_email_send':False,
					'cancelrefund_email_send':False,
					'cancelapplied_sms_send':False,
					'cancelapproved_sms_send':False,
					'cancelrefund_sms_send':False,
					'last_modified_dt' : datetime.now(),
					'payment_relax':pay_need,
				})

				# incase of change of participant and
				# new registration is rejection or add to wait list, then initiate the refund

				if (rec.ischangeofparticipant == True and (self.selected_status == 'Rejected' or self.selected_status == 'Add To Waiting List')):
					source = self.env["program.registration"].browse(rec.changeofparticipant_ref)
					refund_data = self.env['rejuvenation.payments']._calculateRefundAmount(source, rec.changeofparticipant_applied_dt)
					source.write({
						'refund_eligible_ondaysbefore': refund_data['days_before'],
						'refund_eligible_onpercentage': refund_data['elg_percent'],
						'refund_eligible': refund_data['elg_amt'],
						'registration_status':'Cancel Approved',
						'cancelapproved_email_send':False,
						'cancelapproved_sms_send':False,
						'date_cancelapproved': fields.Date.today(),
						'date_withdrawn': rec.changeofparticipant_applied_dt,
						'last_modified_dt': datetime.now(),
						'auto_approval_comments': 'moved from change of participant'
						})
					self.env['rejuvenation.notification']._sendCancellationMailAndSMS(source.id,'Cancel Approved')

				# end of change of participant

				if (ha_status == 'Request More Info'):
					print('recreating participant qa')
					self.env['participant.qa'].search([('first_name','=',active_id)]).unlink()
					if self.question_ids:
						for rec1 in self.question_ids:
							nrec = self.env['participant.qa'].create({'first_name':active_id,'question_bank':rec1.id})

					# ndata = self.env['participant.qa'].search([('first_name','=',active_id)])
					# if ndata:
					# 	print('data available')
					# 	id.write({'question_bank':ndata})

			print('update completed, start sending email/sms')

			if (statuschanged == True):
				if (self.selected_status == 'HA Approved' and registration_status == 'Approved'):
					print('trigger _sendPaymentLinkEmailCore')
					self.env['rejuvenation.notification']._sendPaymentLinkEmailCore(rec)
				elif (self.selected_status == 'Rejected'):
					print('trigger _sendRegistrationRejectionEmailCore')
					self.env['rejuvenation.notification']._sendRegistrationRejectionEmailCore(rec)
				elif (self.selected_status == 'Request More Info'):
					print('trigger _sendRegistrationHAAdditionalInfoEmailCore')
					self.env['rejuvenation.notification']._sendRegistrationHAAdditionalInfoEmailCore(rec)
				elif (self.selected_status == 'Add To Waiting List'):
					print('trigger _sendAddToWaitListEmailCore')
					self.env['rejuvenation.notification']._sendAddToWaitListEmailCore(rec)
				print('completed sending email/sms')

