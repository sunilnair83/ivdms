from odoo import models, fields, api, exceptions
from odoo.exceptions import except_orm
from datetime import date, timedelta, datetime
import json

class ChangeProgramDate(models.TransientModel):
	_name = 'program.registration.changeprogramdate'
	_description = 'Change Program Date'

	#participant_id = fields.Integer(string = 'ID')
	participant_name = fields.Char(string = 'Participant Name', required=True)
	programapplied = fields.Many2one('rejuvenation.program.schedule',string = 'Applied Program', required=True)
	prg_date_now = fields.Date()
	selected_program = fields.Many2one('rejuvenation.program.schedule',string = 'Available Programs', required=True , 
	domain="[('id','!=',programapplied),('pgmschedule_registrationopen','=',True),('pgmschedule_totalbalanceseats','>',0),('pgmschedule_startdate','>',context_today().strftime('%Y-%m-%d'))]")
	selected_package = fields.Many2one('rejuvenation.program.schedule.roomdata', string='Requested Package',
	domain="[('pgmschedule_programname','=',selected_program)]", required=True)

	@api.constrains('selected_program')
	def	validate_selected_program(self):
		if (self.selected_program == self.programapplied):
			raise exceptions.ValidationError("Applied program and selected program should not be the same")
	
	@api.onchange('programapplied')
	def participant_name_onchange(self):
		print('onchange')
		print(self.programapplied.pgmschedule_programtype.id)
		id = self.programapplied.pgmschedule_programtype.id
		print(id)
		recs = self.env['rejuvenation.program.schedule'].search([('pgmschedule_programtype', '=', id)])
		print(recs)			
		# self.selected_program = recs

	@api.onchange('selected_program')
	def selected_program_onchange(self):
		print('prog change')
	
	def submit_changeprogramdate(self):
		print('field generate')
		#fields = self.env['program.registration'].fields_get()		

		active_id = self.env.context.get('default_active_id')
		ids = self.env["program.registration"].browse(active_id)
		for id in ids:

			if (id.ischangeofparticipant == True):
				if (id.registration_status == 'Paid' or id.registration_status == 'Confirmed'):
					raise exceptions.ValidationError('Schedule change not allowed, this registration have done through change of participant.')

			if (id.payment_status == 'Initiated'):
				raise exceptions.ValidationError('Payment have been initiated by the participant. Please try after some time')
			elif (id.registration_status != 'Paid' and id.registration_status != 'Confirmed'):
				print('change initiated')
				id.write({'programapplied':self.selected_program, 'packageselection':self.selected_package})
			else:

				#

				balanceseatcount = self.selected_package.pgmschedule_packagenoofseats - self.selected_package.pgmschedule_regularseatspaid
				balanceemergencyseatcount = self.selected_package.pgmschedule_packageemergencyseats - self.selected_package.pgmschedule_emergencyseatspaid
			
				if (balanceseatcount <= 0 and balanceemergencyseatcount <= 0):
					raise exceptions.ValidationError('Schedule change not allowed, no seats are available in the selected package.')
				
				#

				print('initiate cancellation')

				self.env['rejuvenation.payments']._reverseSeat(id)

				refund_data = self.env['rejuvenation.payments']._calculateRefundAmount(id, datetime.now())
				
				id.write({
					'cancelapproved_email_send':False,
					'cancelapproved_sms_send':False,
					'date_cancelapproved':datetime.now(),
					'registration_status':'Cancel Approved',
					'refund_eligible_ondaysbefore': refund_data['days_before'],
					'refund_eligible_onpercentage': refund_data['elg_percent'],
					'refund_eligible': refund_data['elg_amt'],
					'last_modified_dt': datetime.now()				
				})

				#email and sms trigger
				self.env['rejuvenation.notification']._sendCancellationMailAndSMS(id.id,'Cancel Approved')

				print('do the new registration')

				nrec = self.env["program.registration"].create({'first_name':id.first_name,
																'last_name':id.last_name,
																'name_called':id.name_called,
																'gender':id.gender,
																'marital_status':id.marital_status,
																'dob':id.dob,
																'countrycode':id.countrycode,
																'phone':id.phone,
																'pincode':id.pincode,
																'address_line':id.address_line,
																'city':id.city,
																'education_qualification':id.education_qualification,
																'occupation':id.occupation,
																'name1':id.name1,
																'relationship1':id.relationship1,
																'phone1':id.phone1,
																'name2':id.name2,
																'relationship2':id.relationship2,
																'phone2':id.phone2,
																'file_name':id.file_name,
																'idfile_name':id.idfile_name,
																'addrfile_name':id.addrfile_name,
																'programhistorynote':id.programhistorynote,
																'height':id.height,
																'weight':id.weight,
																'bmi':id.bmi,
																'nameoffamilydr':id.nameoffamilydr,
																'familydrcontact':id.familydrcontact,
																'familydremail':id.familydremail,
																'dateofdrvisit':id.dateofdrvisit,
																'passportnumber':id.passportnumber,
																'passportexpirydate':id.passportexpirydate,
																'visanumber':id.visanumber,
																'visaexpirydate':id.visaexpirydate,
																'is_disease_1':id.is_disease_1,
																'is_disease_2':id.is_disease_2,
																'otherpractices':id.otherpractices,
																'show_change_pgm':id.show_change_pgm,
																'isfood':id.isfood,
																'isfamily':id.isfamily,
																'iswork':id.iswork,
																'isclimate':id.isclimate,
																'food_details':id.food_details,
																'family_details':id.family_details,
																'work_details':id.work_details,
																'climate_details':id.climate_details,
																'allopathynote':id.allopathynote,
																'alternatenote':id.alternatenote,
																'foodallergy':id.foodallergy,
																'chemicalallergy':id.chemicalallergy,
																'environmentallergy':id.environmentallergy,
																'otherallergy':id.otherallergy,
																'isheadandneckproblems':id.isheadandneckproblems,
																'headandneck_details':id.headandneck_details,
																'ishypertension':id.ishypertension,
																'hypertension_details':id.hypertension_details,
																'isheartdisorders':id.isheartdisorders,
																'heartdisorder_details':id.heartdisorder_details,
																'isdiabetesmellitus':id.isdiabetesmellitus,
																'diabetesmellitus_details':id.diabetesmellitus_details,
																'isthyroid':id.isthyroid,
																'thyroid_details':id.thyroid_details,
																'isotherendocrinologydisorders':id.isotherendocrinologydisorders,
																'otherendocrinologydisorders_details':id.otherendocrinologydisorders_details,
																'isliverdisorders':id.isliverdisorders,
																'liverdisorders_details':id.liverdisorders_details,
																'iskidneydisorders':id.iskidneydisorders,
																'kidneydisorders_details':id.kidneydisorders_details,
																'isbrainandnervousdisorders':id.isbrainandnervousdisorders,
																'brainandnervousdisorders_details':id.brainandnervousdisorders_details,
																'iseyedisorders':id.iseyedisorders,
																'eyedisorders_details':id.eyedisorders_details,
																'iseardisorders':id.iseardisorders,
																'eardisorders_details':id.eardisorders_details,
																'isnosedisorders':id.isnosedisorders,
																'nosedisorders_details':id.nosedisorders_details,
																'isthroatissues':id.isthroatissues,
																'throatissues_details':id.throatissues_details,
																'ischestlungissues':id.ischestlungissues,
																'chestlungissues_details':id.chestlungissues_details,
																'isintestinaldisorders':id.isintestinaldisorders,
																'intestinaldisorders_details':id.intestinaldisorders_details,
																'isstomachdisorders':id.isstomachdisorders,
																'stomachdisorders_details':id.stomachdisorders_details,
																'isurinaryproblem':id.isurinaryproblem,
																'urinaryproblem_details':id.urinaryproblem_details,
																'isbackissues':id.isbackissues,
																'backissues_details':id.backissues_details,
																'iscirculationproblem':id.iscirculationproblem,
																'circulationproblem_details':id.circulationproblem_details,
																'isskindisorders':id.isskindisorders,
																'skindisorders_details':id.skindisorders_details,
																'isjointdisorders':id.isjointdisorders,
																'jointdisorders_details':id.jointdisorders_details,
																'ispaintordiscomfort':id.ispaintordiscomfort,
																'pain_details':id.pain_details,
																'isinfertility':id.isinfertility,
																'infertility_details':id.infertility_details,
																'iscancer':id.iscancer,
																'cancer_details':id.cancer_details,
																'isautoimmunediesase':id.isautoimmunediesase,
																'autoimmune_details':id.autoimmune_details,
																'isulcersinthemouth':id.isulcersinthemouth,
																'ulcersinmouth_details':id.ulcersinmouth_details,
																'ishivaids':id.ishivaids,
																'hivaids_details':id.hivaids_details,
																'isherpestype1':id.isherpestype1,
																'herpestype1_details':id.herpestype1_details,
																'isherpestype2':id.isherpestype2,
																'herpestype2_details':id.herpestype2_details,
																'istuberculosis':id.istuberculosis,
																'tuberculosis_details':id.tuberculosis_details,
																'ishepatitis':id.ishepatitis,
																'hepatitis_details':id.hepatitis_details,
																'isanyothecommunicabledisease':id.isanyothecommunicabledisease,
																'anyothecommunicabledisease_details':id.anyothecommunicabledisease_details,
																'ispsychiatrydisorders':id.ispsychiatrydisorders,
																'psychiatrydisorders_details':id.psychiatrydisorders_details,
																'istrauma':id.istrauma,
																'trauma_details':id.trauma_details,
																'issurgery':id.issurgery,
																'isbloodtransfusion':id.isbloodtransfusion,
																'ismood':id.ismood,
																'mood_details':id.mood_details,
																'isenergylevel':id.isenergylevel,
																'energylevel_details':id.energylevel_details,
																'isappetite':id.isappetite,
																'appetite_details':id.appetite_details,
																'issleep':id.issleep,
																'sleep_details':id.sleep_details,
																'isweight':id.isweight,
																'weight_details':id.weight_details,
																'issexualfunction':id.issexualfunction,
																'sexualfunction_details':id.sexualfunction_details,
																'isbowel':id.isbowel,
																'bowel_details':id.bowel_details,
																'ispersonalsafety':id.ispersonalsafety,
																'mobility':id.mobility,
																'mobility_details':id.mobility_details,
																'falling':id.falling,
																'falling_details':id.falling_details,
																'memoryprob':id.memoryprob,
																'memoryprob_details':id.memoryprob_details,
																'visionloss':id.visionloss,
																'visionloss_details':id.visionloss_details,
																'hearingloss':id.hearingloss,
																'hearingloss_details':id.hearingloss_details,
																'requirementofassis':id.requirementofassis,
																'requirementofassis_details':id.requirementofassis_details,
																'isphysicalcondition':id.isphysicalcondition,
																'ispsychologicalcondition':id.ispsychologicalcondition,
																'ischildhoodillness':id.ischildhoodillness,
																'immunizationdates':id.immunizationdates,
																'menstruationage':id.menstruationage,
																'menstruationlastdate':id.menstruationlastdate,
																'menstruationperiod':id.menstruationperiod,
																'menstuationflow':id.menstuationflow,
																'normalpregnancies':id.normalpregnancies,																
																'caeseareans':id.caeseareans,
																'lastrectalwomen':id.lastrectalwomen,
																'womendisorders':id.womendisorders,
																'isheavyperiod':id.isheavyperiod,
																'sexuallyactive':id.sexuallyactive,
																'tryingpregnancy':id.tryingpregnancy,
																'diabetesinpregnancy':id.diabetesinpregnancy,
																'deliverycomplications':id.deliverycomplications,
																'hysterectomy':id.hysterectomy,
																'urinationProblem':id.urinationProblem,
																'sweating':id.sweating,
																'menstrualtension':id.menstrualtension,
																'breastlumps':id.breastlumps,
																'menprostate':id.menprostate,
																'sedentary':id.sedentary,
																'mildexercise':id.mildexercise,
																'vigorousexercise':id.vigorousexercise,
																'regularexercise':id.regularexercise,
																'dieting':id.dieting,
																'fasted':id.fasted,
																'areyouonmedication':id.areyouonmedication,
																'mealperday':id.mealperday,
																'dietinfast':id.dietinfast,
																'fasttime':id.fasttime,
																'vegetarian':id.vegetarian,
																'coffee':id.coffee,
																'cupscoffeaday':id.cupscoffeaday,
																'alcohol':id.alcohol,
																'alcoholkind':id.alcoholkind,
																'alcoholperweek':id.alcoholperweek,
																'tobacco':id.tobacco,
																#'tobaccostage':id.tobaccostage,
																'streetdrugs':id.streetdrugs,
																'needledrugs':id.needledrugs,
																'areyoudiabetic':id.areyoudiabetic,
																'yeardiagnosisofdiabetes':id.yeardiagnosisofdiabetes,
																'lastvisittophysician':id.lastvisittophysician,
																'increasedappetite':id.increasedappetite,
																'increasedthirst':id.increasedthirst,
																'increasedurination':id.increasedurination,
																'symptomslowsugar':id.symptomslowsugar,
																'badbreath':id.badbreath,
																'compulsiveeating':id.compulsiveeating,
																'numbness':id.numbness,
																'footulcers':id.footulcers,
																'genitalinfection':id.genitalinfection,
																'breathatrest':id.breathatrest,
																'breathatphysicalactivity':id.breathatphysicalactivity,
																'chestpainatrest':id.chestpainatrest,
																'chestpainatphysicalactivity':id.chestpainatphysicalactivity,
																'palpiation':id.palpiation,
																'giddiness':id.giddiness,
																'swellingoflegs':id.swellingoflegs,
																'bloodreportfile_name':id.bloodreportfile_name,
																'areyousufferingheart':id.areyousufferingheart,
																'echoreportfile_name':id.echoreportfile_name,
																'areyoueyeproblem':id.areyoueyeproblem,
																'lasteyeexamination':id.lasteyeexamination,
																'opthalmologistdiagnosis':id.opthalmologistdiagnosis,
																'refractivedate':id.refractivedate,
																'eyepressure':id.eyepressure,
																'whatuuse':id.whatuuse,
																'doyoumusculoskeletal':id.doyoumusculoskeletal,
																'condition':id.condition,
																'yearofdiagnosiscondition':id.yearofdiagnosiscondition,
																#'painnature':id.painnature,
																#'frequency':id.frequency,
																'duration':id.duration,
																#'painworse':id.painworse,
																'painrelief':id.painrelief,
																'comfortableposture':id.comfortableposture,
																'medicationalleviatepain':id.medicationalleviatepain,
																'durationofmedication':id.durationofmedication,
																'jointswelling':id.jointswelling,
																'jointswellingreason':id.jointswellingreason,
																#'restrictedmovement':id.restrictedmovement,
																'weakness':id.weakness,
																'weaknesreason':id.weaknesreason,
																'fever':id.fever,
																'feverreason':id.feverreason,
																'sleepposture':id.sleepposture,
																'fallen':id.fallen,
																'fallreason':id.fallreason,
																'ha_status':'Pending',
																'registration_status':'Pending',
																'programapplied':self.selected_program.id,
																'packageselection':self.selected_package.id,
																'participant_comments':id.participant_comments,
																'participant_email':id.participant_email,
																'prerequisite_pgm':id.prerequisite_pgm,
																'display_name':id.display_name
																})
								
				if nrec:

					nrec.write({'country':id.country,
								'state':id.state,
								'photo_file':id.photo_file,
								'idproof_file':id.idproof_file,
								'addrproof_file':id.addrproof_file,
								'bloodreports_file':id.bloodreports_file,
								'echoreports_file':id.echoreports_file,
								'nationality_id':id.nationality_id,
								'idproof_type':id.idproof_type
								})

					print('Program History Update')
					ntmp = self.env['program.history'].search([('programregistration','=',id.id)])
					if ntmp:
						print(ntmp)
						for n in ntmp:
							x = self.env['program.history'].create({'programregistration':nrec.id,'programname':n.programname.id,'pgmdate':n.pgmdate,'teachername':n.teachername,'pgmcountry':n.pgmcountry.id,'pgmstate':n.pgmstate.id,'pgmlocation':n.pgmlocation.id})

					#idproof.attachment not updated due to not clear about the attributes

					print('ishalifetreatments - ishalife.treatments')
					ntmp = self.env['ishalife.treatments'].search([('programregistration','=',id.id)])
					if ntmp:
						print(ntmp)
						for n in ntmp:
							x = self.env['ishalife.treatments'].create({'programregistration':nrec.id,
																	'treatmentdate':n.treatmentdate,
																	'treatmentplace':n.treatmentplace,
																	'condition':n.condition,
																	'treatmentname':n.treatmentname
																	})

					print('current.medicalhistory')
					ntmp = self.env['current.medicalhistory'].search([('programregistration','=',id.id)])
					if ntmp:
						print(ntmp)
						for n in ntmp:
							x = self.env['current.medicalhistory'].create({'programregistration':nrec.id,
																	'current_nameofcomplaint':n.current_nameofcomplaint,
																	'current_duration':n.current_duration,
																	'current_condition':n.current_condition
																	})

					print('current.allopathymedications')
					ntmp = self.env['current.allopathymedications'].search([('programregistration','=',id.id)])
					if ntmp:
						print(ntmp)
						for n in ntmp:
							x = self.env['current.allopathymedications'].create({'programregistration':nrec.id,
																	'current_allopathyname':n.current_allopathyname,
																	'current_allopathydose':n.current_allopathydose,
																	'current_allopathyduration':n.current_allopathyduration
																	})

					print('current.alternatemedications')
					ntmp = self.env['current.alternatemedications'].search([('programregistration','=',id.id)])
					if ntmp:
						print(ntmp)
						for n in ntmp:
							x = self.env['current.alternatemedications'].create({'programregistration':nrec.id,
																	'current_alternatename':n.current_alternatename,
																	'current_alternatedose':n.current_alternatedose,
																	'current_alternateduration':n.current_alternateduration
																	})
				
					print('medication.allergies')
					ntmp = self.env['medication.allergies'].search([('programregistration','=',id.id)])
					if ntmp:
						print(ntmp)
						for n in ntmp:
							x = self.env['medication.allergies'].create({'programregistration':nrec.id,
																	'allergydrugname':n.allergydrugname,
																	'allergyreaction':n.allergyreaction
																	})
				
					print('treatment.details')
					ntmp = self.env['treatment.details'].search([('programregistration','=',id.id)])
					if ntmp:
						print(ntmp)
						for n in ntmp:
							x = self.env['treatment.details'].create({'programregistration':nrec.id,
																	'treatment_year':n.treatment_year,
																	'treatment_condition':n.treatment_condition,
																	'treatment_details':n.treatment_details
																	})

					print('family.history')
					ntmp = self.env['family.history'].search([('programregistration','=',id.id)])
					if ntmp:
						print(ntmp)
						for n in ntmp:
							x = self.env['family.history'].create({'programregistration':nrec.id,
																	'family_relation':n.family_relation,
																	'family_gender':n.family_gender,
																	'family_age':n.family_age,
																	'family_health':n.family_health
																	})

					print('refractive.error')
					ntmp = self.env['refractive.error'].search([('programregistration','=',id.id)])
					if ntmp:
						print(ntmp)
						for n in ntmp:
							x = self.env['refractive.error'].create({'programregistration':nrec.id,
																	'select_eye':n.select_eye,
																	'dvsph':n.dvsph,
																	'dvcyl':n.dvcyl,
																	'dvaxis':n.dvaxis,
																	'dvva':n.dvva,
																	'nvsph':n.nvsph,
																	'nvcyl':n.nvcyl,
																	'nvaxis':n.nvaxis,
																	'nvva':n.nvva
																	})

					print('duration.sensation')
					ntmp = self.env['duration.sensation'].search([('programregistration','=',id.id)])
					if ntmp:
						print(ntmp)
						for n in ntmp:
							x = self.env['duration.sensation'].create({'programregistration':nrec.id,
																	'doespainincrease':n.doespainincrease,
																	'durationofincreasedsensations':n.durationofincreasedsensations
																	})

					print('joints.affected')
					ntmp = self.env['joints.affected'].search([('programregistration','=',id.id)])
					if ntmp:
						print(ntmp)
						for n in ntmp:
							x = self.env['joints.affected'].create({'programregistration':nrec.id,
																	'joints':n.joints,
																	'paindegree':n.paindegree,
																	'right':n.right,
																	'left':n.left
																	})																																															
