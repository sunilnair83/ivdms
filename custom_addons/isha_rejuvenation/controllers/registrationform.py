import base64
import json
from datetime import date, timedelta, datetime
import hashlib
import hmac
from odoo import http
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.fields import Integer
from odoo.http import request
from odoo.tools import ustr
import logging
import traceback
import requests
from ..isha_crm_importer import Configuration
_logger = logging.getLogger(__name__)

# Api to get full profile data
def sso_get_full_data(sso_id):
	# request_url = "https://uat-profile.isha.in/services/pms/api/full-profiles/" + sso_id
	request_url = request.env['ir.config_parameter'].sudo().get_param(
		'satsang.SSO_PMS_ENDPOINT1') + "full-profiles/" + sso_id
	authreq = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SYSTEM_TOKEN1')
	headers = {
		"Authorization": "Bearer " + authreq,
		'X-legal-entity': 'IF',
		'x-user': sso_id
	}
	profile = requests.get(request_url, headers=headers)
	return profile.json()


def get_fullprofileresponse_DTO():
	return {
		"profileId": "",
		"basicProfile": {
			"createdBy": "",
			"createdDate": "",
			"lastModifiedBy": "",
			"lastModifiedDate": "",
			"id": 0,
			"profileId": "",
			"email": "",
			"firstName": "",
			"lastName": "",
			"phone": {
				"countryCode": "",
				"number": ""
			},
			"gender": "",
			"dob": "",
			"countryOfResidence": ""
		},
		"extendedProfile": {
			"createdBy": "",
			"createdDate": "",
			"lastModifiedBy": "",
			"lastModifiedDate": "",
			"id": 0,
			"nationality": "",
			"profileId": ""
		},
		"documents": [],
		"attendedPrograms": [],
		"profileSettingsConfig": {
			"createdBy": "",
			"createdDate": "",
			"lastModifiedBy": "",
			"lastModifiedDate": "",
			"id": 0,
			"nameLocked": False,
			"isPhoneVerified": False,
			"phoneVerificationDate": None,
			"profileId": ""
		},
		"addresses": [
			{
				"createdBy": "",
				"createdDate": "",
				"lastModifiedBy": "",
				"lastModifiedDate": "",
				"id": 0,
				"townVillageDistrict": "",
				"city": "",
				"state": "",
				"country": "",
				"pincode": "",
				"profileId": ""
			}
		]
	}


# Api to get the profile data
def sso_get_user_profile(url, payload):
	dp = {"data": json.dumps(payload)}
	url = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_INFO_ENDPOINT1')
	# request.env['satsang.tempdebuglog'].sudo().create({
	#     'logtext': "sso_get_user_profile config " + url
	# })
	return requests.post(url, data=dp)


def hash_hmac(data, secret):
	data_enc = json.dumps(data['session_id'], sort_keys=True, separators=(',', ':'))
	signature = hmac.new(bytes(secret, 'utf-8'), bytes(data_enc, 'utf-8'), digestmod=hashlib.sha256).hexdigest()
	return signature

class RejuvenationRegistration(http.Controller):
   # @http.route(['/payment/authorize/return/', ], type='http', auth='public', csrf=False)
 #   def authorize_form_feedback(self, **post):


    @http.route('/ishahealthsolutions/termsandconditions/', type='http', auth="public", methods=["GET", "POST"], website=True, csrf=False)
    def tcsform(self, **post):
        if request.httprequest.method=="GET":
            termlist = request.env['rejuvenation.terms_and_conditions'].sudo().search([('tc_active', '=', True)])
            print(termlist.tc_description)
            values={
                'terms':termlist,
            }
            return request.render("isha_rejuvenation.termsandconditionsview",values)

    @http.route('/ishahealthsolutions/registrationform/', type='http', auth="public", methods=["GET","POST"], website=True, csrf=False)
    def registrationform(self, legal_entity="", consent_grant_status="", id_token="", testsso="0",program_id="0", **post):

        fullprofileresponsedatVar = request.env['rejuvenation.ssoapi'].sudo().get_fullprofileresponse_DTO()
        consentgrantStatusflagvar = True
        if (request.httprequest.method == "GET"):
            validation_errors = []
            try:
                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                # ret = request.env['ir.config_parameter'].sudo().get_param('rejuvenation.SSO_API_SECRET1')
                cururls = str(request.httprequest.url).replace("http://", "https://", 1)
                sso_log = {'request_url': str(cururls),
                           "callback_url": str(cururls),
                           "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                           "hash_value": "",
                           "action": "0",
                           "legal_entity": "IF",
                           "force_consent": "1"
                           }
                ret_list = {'request_url': str(cururls),
                            "callback_url": str(cururls),
                            "api_key": request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_KEY1'),
                            }
                data_enc = json.dumps(ret_list, sort_keys=True, separators=(',', ':'))
                data_enc = data_enc.replace("/", "\\/")
                signature = hmac.new(bytes(ret, 'utf-8'), bytes(data_enc, 'utf-8'),
                                     digestmod=hashlib.sha256).hexdigest()
                sso_log["hash_value"] = signature
                sso_log["sso_logurl"] = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_LOGIN_URL1')
                values = {
                    'sso_log': sso_log
                       }
                return request.render('isha_rejuvenation.rejuvenationsso', values)
            except Exception as ex:
                tb_ex = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
                _logger.error(tb_ex)
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)
                values = {}
                return request.render('isha_rejuvenation.exception', values)
        # if request.httprequest.method == "GET":
        #     validation_errors = []
        #     values ={}
        #     ssoadproofuri2 = []
        #     isoidproofuri2 = []
        #     ssoadproofuri2 = ""
        #     isoidproofuri2 = ""
        #     try:
        #         fetch_incomplete_application = request.env['program.registration'].sudo().search([('incompleteregistration_pagescompleted', '!=', '5'),
        #                                                                                           ('participant_email', '=', 'ramesh.dhanakkodi@ishafoundation.org')])
        #         # Starting of Code to forward the request to respected page
        #         if (fetch_incomplete_application):
        #             if (fetch_incomplete_application['incompleteregistration_pagescompleted'] == 1):
        #                 first_data = request.env['program.registration'].sudo().search([('access_token', '=', fetch_incomplete_application["access_token"])])
        #                 contact_id = first_data.contact_id_fkey.id
        #                 program_category = ['Inner Engineering', 'Bhava Spandana', 'Shoonya', 'Hatha Yoga', 'Samyama']
        #                 pgmrecs = first_data.getProgramAttendance(contact_id, program_category)
        #                 state_onlyindia = request.env['res.country.state'].sudo().search([('country_id', '=', 104)])
        #                 values = {
        #                     'pgmrecs': pgmrecs,
        #                     'programregn': first_data,
        #                     'countries': request.env['res.country'].sudo().search([]),
        #                     'states': state_onlyindia,
        #                     'prgregncenters': request.env['isha.center'].sudo().search([]),
        #                     # 'prgregnprogramnametypes': request.env['program.type'].search([]),
        #                     'prgregnprogramnamecategories': request.env['master.program.category'].sudo().search(
        #                         [('keyprogram', '=', True)]),
        #                     'programschedules': request.env['rejuvenation.program.schedule'].sudo().search([]),
        #                     'programscheduleroomdatas': request.env['rejuvenation.program.schedule.roomdata'].sudo().search([]),
        #                     'submitted': post.get('submitted', False),
        #                     'current_page': 2,
        #                     'filled_pages': 2,
        #                     'firstnamevar': first_data.first_name,
        #                 }
        #                 values['validation_errors'] = validation_errors
        #                 return request.render("isha_rejuvenation.pgmregn2", values)
        #             if (fetch_incomplete_application['incompleteregistration_pagescompleted'] == 2):
        #                 first_data = request.env['program.registration'].sudo().search([('access_token', '=', fetch_incomplete_application["access_token"])])
        #                 values = {
        #                     'programregn': first_data,
        #                     'submitted': post.get('submitted', False),
        #                     'countries': request.env['res.country'].sudo().search([]),
        #                     'current_page': 3,
        #                     'filled_pages': 3,
        #                 }
        #                 return request.render("isha_rejuvenation.pgmregn3", values)
        #             if (fetch_incomplete_application['incompleteregistration_pagescompleted'] == 3):
        #                 first_data = request.env['program.registration'].sudo().search([('access_token', '=', fetch_incomplete_application["access_token"])])
        #                 values = {
        #                     'programregn': first_data,
        #                     'submitted': post.get('submitted', False),
        #                     'current_page': 4,
        #                     'filled_pages': 4,
        #                     'gender': first_data.gender,
        #                     'firstnamevar': first_data.nameoffamilydr,
        #                 }
        #                 return request.render("isha_rejuvenation.pgmregn4", values)
        #             if (fetch_incomplete_application['incompleteregistration_pagescompleted'] == 4):
        #                 first_data = request.env['program.registration'].sudo().search([('access_token', '=', fetch_incomplete_application["access_token"])])
        #                 selected_consent = request.env['rejuvenation.consent.versioning'].sudo().search([('consent_active', '=', 'True')])
        #                 values = {
        #                     'programregn': first_data,
        #                     'programname': programname,
        #                     'submitted': post.get('submitted', False),
        #                     'current_page': 5,
        #                     'filled_pages': 5,
        #                     'consentversion': selected_consent,
        #                 }
        #                 return request.render("isha_rejuvenation.pgmregn5", values)
        #             if (fetch_incomplete_application['incompleteregistration_pagescompleted'] == 5):
        #                 first_data = request.env['program.registration'].sudo().search([('access_token', '=', fetch_incomplete_application["access_token"])])
        #                 values = {'object': first_data}
        #                 return request.render("isha_rejuvenation.registrationcompletion", values)
        #
        #         for objvar in fullprofileresponsedatVar["documents"]:
        #             if objvar["docTag"] == "ADDRESS_PROOF" and objvar["uri"] is not None:  # jspgmregisoidproff is True and 'docType' in objvar:
        #                 ssoadproofuri2 = objvar["uri"]#ssoadproofuri2.append(objvar["uri"])
        #
        #         for objvar in fullprofileresponsedatVar["documents"]:
        #             if objvar["docTag"] == "ID_PROOF" and objvar["uri"] is not None:  # jspgmregisoidproff is True and 'docType' in objvar:
        #                 isoidproofuri2 = objvar["uri"] #isoidproofuri2.append(objvar["uri"])
        #
        #         countries_scl = request.env['res.country'].sudo().search(['|',('name','=','India'),('name','=','United States')])
        #         countries =  request.env['res.country'].sudo().search([('name','!=','India'),('name','!=','United States')])
        #         result_countries = countries_scl | countries
        #         profresponse = json.dumps(fullprofileresponsedatVar)
        #         values = {
        #                 'programregn': request.env['program.registration'].sudo().search([]),
        #                 'countries':result_countries,
        #                 'doctypes': request.env['documenttype'].sudo().search([]),
        #                 'states': request.env['res.country.state'].sudo().search([]),
        #                 #               Changing to list the ishangam program categories instead of moksha program types
        #                 #               'prgregnprogramnametypes': request.env['program.type'].search([]),
        #                 'prgregnprogramnamecategories': request.env['master.program.category'].sudo().search(
        #                     [('keyprogram', '=', True)]),
        #                 'programschedules': request.env['rejuvenation.program.schedule'].sudo().search([]),
        #                 'programscheduleroomdatas': request.env['rejuvenation.program.schedule.roomdata'].sudo().search([]),
        #                 'submitted': post.get('submitted', False),
        #                 'current_page':1,
        #                 'filled_pages':1,
        #                 'isofname': fullprofileresponsedatVar,
		#                 'ischangeofparticipant': post.get('iscop', False),
		#                 'changeofparticipant_ref': post.get('cop', ''),
        #                 'ssoadproofuri':ssoadproofuri2,
        #                 'isoidproofuri':isoidproofuri2,
        #                 'inputobj': profresponse,
        #                 'enableishaautofill':consentgrantStatusflagvar,
        #                 'consent_grant_status':consent_grant_status,
        #                 'program_id': program_id
        #                }
        #     except (UserError, AccessError, ValidationError) as exc:
        #         validation_errors.append(ustr(exc))
        #     except Exception:
        #         _logger.error(
        #             "Error caught during request creation", exc_info=True)
        #
        #     values['validation_errors'] = validation_errors
        #     return request.render('isha_rejuvenation.pgmregn',values)
        elif request.httprequest.method == "POST" and "session_id" in request.httprequest.form:
            validation_errors = []
            values ={}
            ssoadproofuri2 = []
            isoidproofuri2 = []
            ssoadproofuri2 = ""
            isoidproofuri2 = ""
            try:

                ret = request.env['ir.config_parameter'].sudo().get_param('satsang.SSO_API_SECRET1')
                api_key = request.env['ir.config_parameter'].sudo().get_param(
                    'satsang.SSO_API_KEY1')  # "31d9c883155816d15f6f3a74dd79961b0577670ac",
                session_id = request.httprequest.form['session_id']
                hash_val = hash_hmac({'session_id': session_id}, ret)
                data = {"api_key": api_key, "session_id": session_id, 'hash_value': hash_val}
                request_url = "https://uat-sso.isha.in/getlogininfo"
                sso_user_profile = eval(sso_get_user_profile(request_url, data).text)

                fullprofileresponsedatVar = get_fullprofileresponse_DTO()
                if "autologin_email" in sso_user_profile:
                    fullprofileresponsedatVar["basicProfile"]["email"] = sso_user_profile["autologin_email"]
                    fullprofileresponsedatVar["profileId"] = sso_user_profile["autologin_profile_id"]
                if consent_grant_status == "1":
                    fullprofileresponsedatVar = sso_get_full_data(sso_user_profile['autologin_profile_id']);

                fetch_incomplete_application=request.env['program.registration'].sudo().search([('incompleteregistration_pagescompleted','!=','5'),
                                                                                                ('participant_email','=',sso_user_profile["autologin_email"])])
                # Starting of Code to forward the request to respected page
                if (fetch_incomplete_application):
                    first_data = request.env['program.registration'].sudo().search([('access_token', '=', fetch_incomplete_application["access_token"])])
                    if (fetch_incomplete_application['incompleteregistration_pagescompleted'] == 1):
                        contact_id = first_data.contact_id_fkey.id
                        program_category = ['Inner Engineering', 'Bhava Spandana', 'Shoonya', 'Hatha Yoga', 'Samyama']
                        pgmrecs = first_data.getProgramAttendance(contact_id, program_category)
                        state_onlyindia = request.env['res.country.state'].sudo().search([('country_id', '=', 104)])
                        values = {
                            'pgmrecs': pgmrecs,
                            'programregn': first_data,
                            'countries': request.env['res.country'].sudo().search([]),
                            'states': state_onlyindia,
                            'prgregncenters': request.env['isha.center'].sudo().search([]),
                            # 'prgregnprogramnametypes': request.env['program.type'].search([]),
                            'prgregnprogramnamecategories': request.env['master.program.category'].sudo().search(
                                [('keyprogram', '=', True)]),
                            'programschedules': request.env['rejuvenation.program.schedule'].sudo().search([]),
                            'programscheduleroomdatas': request.env['rejuvenation.program.schedule.roomdata'].sudo().search([]),
                            'submitted': post.get('submitted', False),
                            'current_page': 2,
                            'filled_pages': 2,
                            'firstnamevar': first_data.first_name,
                        }
                        values['validation_errors'] = validation_errors
                        return request.render("isha_rejuvenation.pgmregn2", values)
                    if (fetch_incomplete_application['incompleteregistration_pagescompleted'] == 2):
                        # first_data = request.env['program.registration'].sudo().search([('access_token', '=', fetch_incomplete_application["access_token"])])
                        values = {
                            'programregn': first_data,
                            'submitted': post.get('submitted', False),
                            'countries': request.env['res.country'].sudo().search([]),
                            'current_page': 3,
                            'filled_pages': 3,
                        }
                        return request.render("isha_rejuvenation.pgmregn3", values)
                    if (fetch_incomplete_application['incompleteregistration_pagescompleted'] == 3):
                        # first_data = request.env['program.registration'].sudo().search([('access_token', '=', fetch_incomplete_application["access_token"])])
                        values = {
                            'programregn': first_data,
                            'submitted': post.get('submitted', False),
                            'current_page': 4,
                            'filled_pages': 4,
                            'gender': first_data.gender,
                            'firstnamevar': first_data.nameoffamilydr,
                        }
                        return request.render("isha_rejuvenation.pgmregn4", values)
                    if (fetch_incomplete_application['incompleteregistration_pagescompleted'] == 4):
                        # first_data = request.env['program.registration'].sudo().search([('access_token', '=', fetch_incomplete_application["access_token"])])
                        selected_consent = request.env['rejuvenation.consent.versioning'].sudo().search([('consent_active', '=', 'True')])
                        selected_formvers = request.env['rejuvenation.pdfversion'].sudo().search([('active_version', '=', 'True')])
                        values = {
                            'programregn': first_data,
                            'programname': first_data.programapplied.pgmschedule_programtype.programtype,
                            'submitted': post.get('submitted', False),
                            'current_page': 5,
                            'filled_pages': 5,
                            'consentversion': selected_consent,
                            'formversion': selected_formvers,
                        }
                        return request.render("isha_rejuvenation.pgmregn5", values)
                    if (fetch_incomplete_application['incompleteregistration_pagescompleted'] == 5):
                        # first_data = request.env['program.registration'].sudo().search([('access_token', '=', fetch_incomplete_application["access_token"])])
                        values = {'object': first_data}
                        return request.render("isha_rejuvenation.registrationcompletion", values)
                # Ending of Code to forward the request to respected page

                for objvar in fullprofileresponsedatVar["documents"]:
                    if objvar["docTag"] == "ADDRESS_PROOF" and objvar["uri"] is not None:  # jspgmregisoidproff is True and 'docType' in objvar:
                        ssoadproofuri2 = objvar["uri"]#ssoadproofuri2.append(objvar["uri"])

                for objvar in fullprofileresponsedatVar["documents"]:
                    if objvar["docTag"] == "ID_PROOF" and objvar["uri"] is not None:  # jspgmregisoidproff is True and 'docType' in objvar:
                        isoidproofuri2 = objvar["uri"] #isoidproofuri2.append(objvar["uri"])

                countries_scl = request.env['res.country'].sudo().search(['|',('name','=','India'),('name','=','United States')])
                countries =  request.env['res.country'].sudo().search([('name','!=','India'),('name','!=','United States')])
                result_countries = countries_scl | countries
                profresponse = json.dumps(fullprofileresponsedatVar)
                values = {
                        'programregn': request.env['program.registration'].sudo().search([]),
                        'countries':result_countries,
                        'doctypes': request.env['documenttype'].sudo().search([]),
                        'states': request.env['res.country.state'].sudo().search([]),
                        #               Changing to list the ishangam program categories instead of moksha program types
                        #               'prgregnprogramnametypes': request.env['program.type'].search([]),
                        'prgregnprogramnamecategories': request.env['master.program.category'].sudo().search(
                            [('keyprogram', '=', True)]),
                        'programschedules': request.env['rejuvenation.program.schedule'].sudo().search([]),
                        'programscheduleroomdatas': request.env['rejuvenation.program.schedule.roomdata'].sudo().search([]),
                        'submitted': post.get('submitted', False),
                        'current_page':1,
                        'filled_pages':1,
                        'isofname': fullprofileresponsedatVar,
		                'ischangeofparticipant': post.get('iscop', False),
		                'changeofparticipant_ref': post.get('cop', ''),
                        'ssoadproofuri':ssoadproofuri2,
                        'isoidproofuri':isoidproofuri2,
                        'inputobj': profresponse,
                        'enableishaautofill':consentgrantStatusflagvar,
                        'consent_grant_status':consent_grant_status,
                        'program_id': program_id
                       }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception:
                _logger.error(
                    "Error caught during request creation", exc_info=True)

            values['validation_errors'] = validation_errors
            return request.render('isha_rejuvenation.pgmregn',values)
        elif request.httprequest.method == "POST":
            validation_errors = []
            values = {}
            first_data=[]
            pgmrecs = []
            try:
                programapplied = post.get('programapplied')
                appliedschedule = request.env['rejuvenation.program.schedule'].sudo().search(
                    [('id', '=', programapplied)])
                packageselection = post.get('packageselection')
                first_name = post.get('first_name')
                last_name = post.get('last_name')
                name_called = post.get('name_called')
                participant_email = post.get('participant_email')
                gender = post.get('gender')
                marital_status = post.get('marital_status')
                dob = post.get('dob')
                address_line = post.get('address_line')
                occupation = post.get('occupation')
                country = post.get('country')
                if (appliedschedule.pgmschedule_programtype.isresidential=='Yes'):
                    nationality_id =int(post.get('nationality_id'))
                else:
                    nationality_id=104
                state = post.get('state')
                if (state):
                    state = int(state)
                else:
                    state = ''
                pincode = post.get('pincode')
                city = post.get('city')
                countrycode = post.get('countrycode')
                phone = post.get('phone')
                education_qualification = post.get('education_qualification')
                isopnumber = post.get('jspgmregfmisopnumyesno')
                opnumber = post.get('opnumber')
                passportnumber=""
                passportexpirydate=False
                visanumber=""
                visaexpirydate=False
                if (appliedschedule.pgmschedule_programtype.isresidential == 'Yes' and nationality_id!=104):
                    passportnumber = post.get('passportnumber')
                    if post.get('passportexpirydate') == '':
                        passportexpirydate = False
                    else:
                        passportexpirydate = post.get('passportexpirydate')
                    visanumber = post.get('visanumber')
                    if post.get('visaexpirydate') == '':
                        visaexpirydate = False
                    else:
                        visaexpirydate = post.get('visaexpirydate')


                # start of change of participant

                print ('checking cop..')

                ischangeofparticipant = False
                changeofparticipant_applied_dt = False
                changeofparticipant_ref = None
                transferred_amount = 0
                copapplied_emailsms_pending = False
                
                if post.get('ischangeofparticipant') != '':
                    if (post.get('ischangeofparticipant') == "True"):
                        access_data = request.env['program.registration'].sudo().search([('cop_access_token','=', post.get('changeofparticipant_ref'))])
                        if (access_data.exists()):
                            if (access_data.registration_status == 'Paid' or access_data.registration_status == 'Confirmed'):
                                print('applying change of participant')
                                access_data.write({
                                    'registration_status': 'Change Of Participant',
                                    'cop_access_token': ''
                                })
                                request.env['rejuvenation.payments'].sudo()._reverseSeat(access_data)
                                ischangeofparticipant = True
                                changeofparticipant_applied_dt = datetime.now()
                                changeofparticipant_ref = access_data.id
                                if (access_data.amount_paid > 0):
                                    percentage = access_data.programapplied.pgmschedule_programtype.participantreplacement
                                    if (percentage > 0):
                                        charges = round(((access_data.amount_paid / 100) * percentage), 2)
                                        transferred_amount = (access_data.amount_paid - charges)
                                copapplied_emailsms_pending = True
                            else:
                                print('data status not matched..')
                                return request.render("isha_rejuvenation.invalidrequest")
                        else:
                            print('data doesnt exits..')
                            return request.render("isha_rejuvenation.invalidrequest")

                # end of change of participant
                attached_filephoto = request.httprequest.files.getlist('pgmupldphoto')
                print ('checking other info')
                if (appliedschedule.pgmschedule_programtype.isresidential == 'Yes'):
                    idproofid= post.get('pgmupldid'),
                    addressproofid= post.get('pgmupldaddproofid'),
                    attached_fileidproof = request.httprequest.files.getlist('pgmupldIdproof')
                    attached_fileaddproof = request.httprequest.files.getlist('pgmupldAddressproof')
                    if (idproofid == None or addressproofid == None or attached_fileidproof == None or
                    attached_fileaddproof == None):
                        return request.render("isha_rejuvenation.exception")

                # attached_prooftype = request.httprequest.form.getlist('new_prooftype[]')
                if consentgrantStatusflagvar == True:
                    #fullprofileresponsedatVar['basicProfile']['firstName'] = first_name
                    #fullprofileresponsedatVar['basicProfile']['lastName'] = last_name
                    # pmsfullresponse_dto['basicProfile']['dob'] = dob
                    fullprofileresponsedatVar['basicProfile']['gender'] = gender.upper()
                    #fullprofileresponsedatVar['basicProfile']['email'] = participant_email
                    # pmsfullresponse_dto['basicProfile']['phone']['countryCode'] = countrycode
                    # pmsfullresponse_dto['basicProfile']['phone']['number'] = phone
                    # pmsfullresponse_dto['basicProfile']['addresses']['pincode'] = pincode
                    # pmsfullresponse_dto['basicProfile']['addresses']['city'] = city
                    # isofname['basicProfile']['addresses']['state']
                    #pgregoutputobj = post.get('jspgregoutputobjid')
                    #request.env['rejuvenation.ssoapi'].post_fullprofile_info(pgregoutputobj, profileidvar, legal_entity)

                recdyn = request.env['program.registration'].sudo().create({
                    'programapplied': int(programapplied),
                    'packageselection': packageselection,
                    'first_name': first_name,
                    'last_name': last_name,
                    'name_called': name_called,
                    'participant_email': participant_email,
                    'gender': gender,
                    'marital_status': marital_status,
                    'dob': dob,
                    'address_line': address_line,
                    'occupation':occupation,
                    'city': city,
                     'country': int(country),
                     'nationality_id': nationality_id,
                     'state': state,
                     'pincode': pincode,
                    'countrycode': countrycode,
                     'phone': phone,
                    'opnumber': opnumber,
                    'isopnumber': isopnumber,
                    'education_qualification': education_qualification,
                    'passportnumber': passportnumber,
                    'passportexpirydate': passportexpirydate,
                    'visanumber': visanumber,
                    'visaexpirydate': visaexpirydate,
                    'registration_status': 'Incomplete',
                    'incompleteregistration_pagescompleted': 1,
                    'incompleteregistration_email_send': False,
                    'incompleteregistration_sms_send': False,
                    'registration_email_send': True,
                    'registration_sms_send': True,
                    'ischangeofparticipant': ischangeofparticipant,
                    'changeofparticipant_applied_dt': changeofparticipant_applied_dt,
                    'changeofparticipant_ref': changeofparticipant_ref,
                    'transferred_amount': transferred_amount,
                    'copapplied_emailsms_pending': copapplied_emailsms_pending
                })
                # for x in range(len(attached_file)):.sudo().create
                first_data = request.env['program.registration'].sudo().search([('access_token', '=', recdyn.access_token)])
                progtypevar = first_data.programapplied.pgmschedule_programtype
                consultationprog = progtypevar.description.upper().__contains__("ONLINE")
                rejuvenationtype = progtypevar.programcategory.programcategory.upper() == 'REJUVENATION'
                documentsnotavaiableflg = consultationprog and rejuvenationtype
                read1 = attached_filephoto[0].read()
                datas1 = base64.b64encode(read1)
                if datas1:
                    new_attachment1 = request.env['ir.attachment'].sudo().create({
                        'name': 'photo_file',
                        'datas': datas1,
                        # 'datas_fname': attached.filename,
                        'res_field': 'photo_file',
                        'res_model': 'program.registration',
                        'res_id': recdyn.id
                    })

                    # request.env['request.attachment'].sudo().create({
                    #     'generic_request_id': recdyn.id,
                    #     'attachment_type_id': 1,
                    #     'attached_file': new_attachment1,
                    #     # 'idproof_file': attached_filename[x]
                    # })
                if not documentsnotavaiableflg:
                    isoidproofavailflg = False
                    isoidproofuri = ""
                    for objvar in fullprofileresponsedatVar["documents"]:
                        if objvar["docTag"] == "ID_PROOF" and objvar["uri"] is not None:  # jspgmregisoidproff is True and 'docType' in objvar:
                            isoidproofavailflg = True
                            isoidproofuri = objvar["uri"]
                    if attached_fileidproof.__len__() > 0:#jspgmregisoidproff is not
                        read2 = attached_fileidproof[0].read()
                        datas2 = base64.b64encode(read2)
                    else:
                        read2 = request.env['rejuvenation.ssoapi'].sudo().get_ssoimage_froms3(isoidproofuri);
                        datas2 = base64.b64encode(read2)

                    if datas2:
                        new_attachment2 = request.env['ir.attachment'].sudo().create({
                            'name': 'idproof_file',
                            'datas': datas2,
                            # 'datas_fname': attached.filename,
                            'res_field': 'idproof_file',
                            'res_model': 'program.registration',
                            'res_id': recdyn.id
                        })

                        # request.env['request.attachment'].sudo().create({
                        #     'generic_request_id': recdyn.id,
                        #     'attachment_type_id': idproofid,
                        #     'attached_file': new_attachment2,
                        #     # 'idproof_file': attached_filename[x]
                        # })
                    ssoadproofavailflg = False
                    ssoadproofuri = ""
                    for objvar in fullprofileresponsedatVar["documents"]:
                        if objvar["docTag"] == "ADDRESS_PROOF" and objvar["uri"] is not None:  # jspgmregisoidproff is True and 'docType' in objvar:
                            ssoadproofavailflg = True
                            ssoadproofuri = objvar["uri"]
                    if attached_fileaddproof.__len__() > 0:#jspgmregisoidproff is not
                        read3 = attached_fileaddproof[0].read()
                        datas3 = base64.b64encode(read3)
                    else:
                        read3 = request.env['rejuvenation.ssoapi'].sudo().get_ssoimage_froms3(ssoadproofuri);
                        datas3 = base64.b64encode(read3)

                    if datas3:
                        new_attachment3 = request.env['ir.attachment'].sudo().create({
                            'name': 'addrproof_file',
                            'datas': datas3,
                            # 'datas_fname': attached.filename,
                            'res_field': 'addrproof_file',
                            'res_model': 'program.registration',
                            'res_id': recdyn.id
                        })

                        # request.env['request.attachment'].sudo().create({
                        #     'generic_request_id': recdyn.id,
                        #     'attachment_type_id': addressproofid,
                        #     'attached_file': new_attachment3,
                        #     # 'idproof_file': attached_filename[x]
                        # })
                        #first_data = request.env['program.registration'].search(
                        #   [('access_token', '=', recdyn.access_token)])

                contact_id = first_data.contact_id_fkey.id
                print('contact_id_fkey:', contact_id)
                program_category = ['Inner Engineering', 'Bhava Spandana', 'Shoonya', 'Hatha Yoga', 'Samyama']
                pgmrecs = first_data.getProgramAttendance(contact_id, program_category)
                print('pgmrecs from getProgramAttendance:', pgmrecs)
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception as ex:
                stacktrace=traceback.print_exc()
                request.env['rejuvenation.tempdebuglog'].sudo().create({
                'logtext': ex
                })
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")
                validation_errors.append(ex)

            if not validation_errors:
                progtype = request.env['rejuvenation.program.schedule'].sudo().search(
                    [('id', '=', int(programapplied))]).pgmschedule_programtype.programtype
                if (progtype == 'Medical Consultation Review'):
                    first_data.incompleteregistration_pagescompleted = 5
                    first_data.registration_status = "Pending"
                    first_data.registration_email_send = False
                    first_data.registration_sms_send = False
                    values = {'object': first_data}
                    return request.render("isha_rejuvenation.registrationsuccess2", values)
                else:
                    state_onlyindia = request.env['res.country.state'].sudo().search([('country_id', '=', 104)])
                    values = {
                        'pgmrecs': pgmrecs,
                        'programregn': first_data,
                        'countries': request.env['res.country'].sudo().search([]),
                        'states': state_onlyindia,
                        'prgregncenters': request.env['isha.center'].sudo().search([]),
                        # 'prgregnprogramnametypes': request.env['program.type'].search([]),
                        'prgregnprogramnamecategories': request.env['master.program.category'].sudo().search(
                            [('keyprogram', '=', True)]),
                        'programschedules': request.env['rejuvenation.program.schedule'].sudo().search([]),
                        'programscheduleroomdatas': request.env['rejuvenation.program.schedule.roomdata'].sudo().search([]),
                        'submitted': post.get('submitted', False),
                        'current_page': 2,
                        'filled_pages': 2,
                        'firstnamevar': first_data.first_name,
                    }
                    values['validation_errors'] = validation_errors
                    return request.render("isha_rejuvenation.pgmregn2", values )
            else:
                _logger.log(25,validation_errors)
                logtext = 'Date:'+ datetime.now().strftime("%m/%d/%Y, %H:%M:%S")+': ' + ' '.join([str(elem) for elem in validation_errors])
                request.env['rejuvenation.tempdebuglog'].sudo().create({
                    'logtext':logtext
                })
                return request.render('isha_rejuvenation.exception')

    @http.route('/ishahealthsolutions/registrationform2', type='http', auth="public", methods=["GET","POST"], website=True, csrf=False)
    def registrationform2(self, access_token, **post):
        first_data = request.env['program.registration'].sudo().search([('access_token', '=', access_token)])
        if (first_data.incompleteregistration_pagescompleted == 5):
            values = {'object': first_data}
            return request.render("isha_rejuvenation.registrationcompletion", values)

        if first_data.access_token != None and request.httprequest.method == "POST":
            values = {}
            validation_errors = []
            try:
                countrycode1 = post.get('countrycode1')
                countrycode2 = post.get('countrycode2')
                name1 = post.get('name1')
                name2 = post.get('name2')
                phone1 = post.get('phone1')
                phone2 = post.get('phone2')
                relationship1 = post.get('relationship1')
                relationship2 = post.get('relationship2')
                email1 = post.get('emergencyemail1')
                email2 = post.get('emergencyemail2')
                programnames = request.httprequest.form.getlist('programname')
                pgmdates = request.httprequest.form.getlist('pgmdate')
                teachernames = request.httprequest.form.getlist('teachername')
                pgmcountrys = request.httprequest.form.getlist('pgmcountry')
                pgmstates = request.httprequest.form.getlist('pgmstate')
                pgmlocations = request.httprequest.form.getlist('pgmlocation')
                pgmhistorycollec = [(0, 0, {
                    'programname': programname,
                    'pgmdate': pgmdate,
                    'teachername': teachername,
                    'pgmcountry': pgmcountry,
                    'pgmstate': pgmstate,
                    'pgmlocation': pgmlocation
                }) for programname, pgmdate, teachername, pgmcountry, pgmstate, pgmlocation in zip(programnames, pgmdates, teachernames, pgmcountrys, pgmstates, pgmlocations)]
                otherpractices = post.get('otherpractices')

                first_data.name1 = name1
                first_data.name2 = name2
                first_data.phone1 = countrycode1+ '-' + phone1
                first_data.phone2 = countrycode2 + '-' + phone2
                first_data.relationship1 = relationship1
                first_data.relationship2 = relationship2
                first_data.email1 = email1
                first_data.email2 = email2
                first_data.otherpractices = otherpractices
                if(pgmdates[0]!=''):
                    first_data.programhistory = pgmhistorycollec
                first_data = request.env['program.registration'].sudo().search([('access_token', '=', access_token)])
                first_data.incompleteregistration_pagescompleted = 2

                values = {
                    'programregn': first_data,
                    'submitted': post.get('submitted', False),
                    'countries': request.env['res.country'].sudo().search([]),
                    'current_page': 3,
                    'filled_pages': 3,
                }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception as ex:
                _logger.error(
                    "Error caught during request creation", ex, exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")

            if not validation_errors:
                values['validation_errors'] = validation_errors
                return request.render("isha_rejuvenation.pgmregn3", values)
            else:
                _logger.log(25, validation_errors)
                logtext = 'Date:' + datetime.now().strftime("%m/%d/%Y, %H:%M:%S")+ ': ' + ' '.join(
                    [str(elem) for elem in validation_errors])
                request.env['rejuvenation.tempdebuglog'].sudo().create({
                    'logtext': logtext
                })
                return request.render('isha_rejuvenation.exception')
        elif  first_data.access_token != None and request.httprequest.method == "GET":
            validation_errors = []
            values ={}
            contact_id = first_data.contact_id_fkey.id
            program_category = ['Inner Engineering', 'Bhava Spandana', 'Shoonya', 'Hatha Yoga', 'Samyama']
            pgmrecs = first_data.getProgramAttendance(contact_id, program_category)
            state_onlyindia = request.env['res.country.state'].sudo().search([('country_id', '=', 104)])

            try:
                values ={
                    'pgmrecs': pgmrecs,
                    'programregn': first_data,
                    'countries': request.env['res.country'].sudo().search([]),
                    'states': state_onlyindia,
                    'prgregncenters': request.env['isha.center'].sudo().search([]),
                    'prgregnprogramnamecategories': request.env['master.program.category'].sudo().search(
                        [('keyprogram', '=', True)]),
                    'programschedules': request.env['rejuvenation.program.schedule'].sudo().search([]),
                    'programscheduleroomdatas': request.env['rejuvenation.program.schedule.roomdata'].sudo().search([]),
                    'submitted': post.get('submitted', False),
                    'current_page': 2,
                    'filled_pages': 2,
                    'firstnamevar': first_data.first_name,
                }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception as ex:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")

            values['validation_errors'] = validation_errors
            return request.render("isha_rejuvenation.pgmregn2", values)

    @http.route('/ishahealthsolutions/registrationform3', type='http', auth="public", methods=["GET","POST"], website=True, csrf=False)
    def registrationform3(self, access_token, **post):
        first_data = request.env['program.registration'].sudo().search([('access_token', '=', access_token)])
        if (first_data.incompleteregistration_pagescompleted == 5):
            values = {'object': first_data}
            return request.render("isha_rejuvenation.registrationcompletion", values)

        if first_data.access_token != None and request.httprequest.method == "POST":
            values = {}
            validation_errors = []
            try:
                height = post.get('height')
                weight = post.get('weight')
                nameoffamilydr = post.get('nameoffamilydr')
                familydrcountrycode = post.get('drcountrycode')
                familydrcontact = post.get('familydrcontact')
                familydremail = post.get('familydremail')
                if post.get('dateofdrvisit') == '':
                    dateofdrvisit = False
                else:
                    dateofdrvisit = post.get('dateofdrvisit')
                isfood = post.get('isfood')
                isfamily = post.get('isfamily')
                iswork = post.get('iswork')
                isclimate = post.get('isclimate')
                food_details = post.get('food_details')
                family_details = post.get('family_details')
                work_details = post.get('work_details')
                climate_details = post.get('climate_details')
                foodallergy = post.get('foodallergy')
                chemicalallergy = post.get('chemicalallergy')
                environmentallergy = post.get('environmentallergy')
                otherallergy = post.get('otherallergy')
                noneofthebelow = post.get('jsprgregnonedisordersid')
                isheadandneckproblems = post.get('isheadandneckproblems')
                isheartdisorders=post.get('isheartdisorders')
                ishypertension= post.get('ishypertension')
                isdiabetesmellitus=post.get('isdiabetesmellitus')
                isthyroid=post.get('isthyroid')
                isotherendocrinologydisorders=post.get('isotherendocrinologydisorders')
                isliverdisorders=post.get('isliverdisorders')
                iskidneydisorders=post.get('iskidneydisorders')
                isbrainandnervousdisorders= post.get('isbrainandnervousdisorders')
                iseyedisorders=post.get('iseyedisorders')
                iseardisorders=post.get('iseardisorders')
                isnosedisorders=post.get('isnosedisorders')
                isthroatissues=post.get('isthroatissues')
                ischestlungissues=post.get('ischestlungissues')
                isintestinaldisorders= post.get('isintestinaldisorders')
                isstomachdisorders=  post.get('isstomachdisorders')
                isurinaryproblem=post.get('isurinaryproblem')
                isbackissues=post.get('isbackissues')
                iscirculationproblem=post.get('iscirculationproblem')
                isskindisorders=post.get('isskindisorders')
                isjointdisorders= post.get('isjointdisorders')
                ispaintordiscomfort= post.get('ispaintordiscomfort')
                isinfertility= post.get('isinfertility')
                iscancer= post.get('iscancer')
                isautoimmunediesase=post.get('isautoimmunediesase')
                isulcersinthemouth=post.get('isulcersinthemouth')
                ishivaids= post.get('ishivaids')
                isherpestype1= post.get('isherpestype1')
                isherpestype2= post.get('isherpestype2')
                istuberculosis=post.get('istuberculosis')
                ishepatitis= post.get('ishepatitis')
                isanyothecommunicabledisease=post.get('isanyothecommunicabledisease')
                ispsychiatrydisorders=post.get('ispsychiatrydisorders')
                headandneck_details = post.get('headandneck_details')
                hypertension_details = post.get('hypertension_details')
                heartdisorder_details = post.get('heartdisorder_details')
                diabetesmellitus_details = post.get('diabetesmellitus_details')
                thyroid_details = post.get('thyroid_details')
                otherendocrinologydisorders_details = post.get('otherendocrinologydisorders_details')
                liverdisorders_details = post.get('liverdisorders_details')
                kidneydisorders_details = post.get('kidneydisorders_details')
                brainandnervousdisorders_details = post.get('brainandnervousdisorders_details')
                eyedisorders_details = post.get('eyedisorders_details')
                eardisorders_details = post.get('eardisorders_details')
                nosedisorders_details = post.get('nosedisorders_details')
                throatissues_details = post.get('throatissues_details')
                chestlungissues_details = post.get('chestlungissues_details')
                intestinaldisorders_details = post.get('intestinaldisorders_details')
                stomachdisorders_details = post.get('stomachdisorders_details')
                urinaryproblem_details = post.get('urinaryproblem_details')
                backissues_details = post.get('backissues_details')
                circulationproblem_details = post.get('circulationproblem_details')
                skindisorders_details = post.get('skindisorders_details')
                jointdisorders_details = post.get('jointdisorders_details')
                pain_details = post.get('pain_details')
                infertility_details = post.get('infertility_details')
                cancer_details = post.get('cancer_details')
                autoimmune_details = post.get('autoimmune_details')
                ulcersinmouth_details = post.get('ulcersinmouth_details')
                hivaids_details = post.get('hivaids_details')
                herpestype1_details = post.get('herpestype1_details')
                herpestype2_details = post.get('herpestype2_details')
                tuberculosis_details = post.get('tuberculosis_details')
                hepatitis_details = post.get('hepatitis_details')
                anyothecommunicabledisease_details = post.get('anyothecommunicabledisease_details')
                psychiatrydisorders_details = post.get('psychiatrydisorders_details')

                pgmishalifedates = request.httprequest.form.getlist('pgmishalifedate')
                pgmishalifenames = request.httprequest.form.getlist('pgmishalifename')
                pgmishalifeplaces = request.httprequest.form.getlist('pgmishalifeplace')
                pgmishalifetreatmentnames = request.httprequest.form.getlist('pgmishalifetreatmentname')
                if(len(pgmishalifedates)==1 and pgmishalifedates[0]==""):
                    ishalifetreatmentcollec = []
                else:
                    ishalifetreatmentcollec = [(0, 0, {
                        'treatmentdate': pgmishalifedate,
                        'treatmentplace': pgmishalifeplace,
                        'condition': pgmishalifename,
                        'treatmentname': pgmishalifetreatmentname,
                    }) for pgmishalifedate, pgmishalifename, pgmishalifeplace, pgmishalifetreatmentname in
                                        zip(pgmishalifedates, pgmishalifenames, pgmishalifeplaces, pgmishalifetreatmentnames)]

                pgmmedicalhistoryconditions = request.httprequest.form.getlist('pgmmedicalhistorycondition')
                pgmmedicalhistorydurations = request.httprequest.form.getlist('pgmmedicalhistoryduration')
                pgmmedicalhistorytreatments = request.httprequest.form.getlist('pgmmedicalhistorytreatment')
                currentmedicalhistorycollect = [(0, 0, {
                    'current_nameofcomplaint': pgmcondition,
                    'current_duration': pgmduration,
                    'current_condition': pgmtreatment,
                }) for pgmcondition, pgmduration, pgmtreatment in
                                           zip(pgmmedicalhistoryconditions, pgmmedicalhistorydurations, pgmmedicalhistorytreatments)]

                pgmallopathynames = request.httprequest.form.getlist('pgmallopathyname')
                pgmallopathydoses = request.httprequest.form.getlist('pgmallopathydose')
                pgmallopathydurations = request.httprequest.form.getlist('pgmallopathyduration')
                currentallopathymedicationscollect = [(0, 0, {
                    'current_allopathyname': pgmname,
                    'current_allopathydose': pgmdose,
                    'current_allopathyduration': pgmduration,
                }) for pgmname, pgmdose, pgmduration in
                                                zip(pgmallopathynames, pgmallopathydoses, pgmallopathydurations)]
                pgmalternatenames = request.httprequest.form.getlist('pgmalternatename')
                pgmalternatedoses = request.httprequest.form.getlist('pgmalternatedose')
                pgmalternatedurations = request.httprequest.form.getlist('pgmalternateduration')
                currentalternatemedicationscollect = [(0, 0, {
                    'current_alternatename': pgmname,
                    'current_alternatedose': pgmdose,
                    'current_alternateduration': pgmduration,
                }) for pgmname, pgmdose, pgmduration in
                                                      zip(pgmalternatenames, pgmalternatedoses, pgmalternatedurations)]

                pgmallergydrugnames = request.httprequest.form.getlist('pgmallergydrugname')
                allergyreactions = request.httprequest.form.getlist('pgmallergyreaction')
                medicationallergiescollect = [(0, 0, {
                    'allergydrugname': pgmname,
                    'allergyreaction': pgmdose,
                }) for pgmname, pgmdose in zip(pgmallergydrugnames, allergyreactions)]

                first_data.familydremail = familydremail
                first_data.height = height
                first_data.weight = weight
                first_data.nameoffamilydr = nameoffamilydr
                first_data.familydrcountrycode = familydrcountrycode
                first_data.familydrcontact = familydrcontact
                first_data.dateofdrvisit = dateofdrvisit
                first_data.isfood = isfood
                first_data.isfamily = isfamily
                first_data.iswork = iswork
                first_data.isclimate = isclimate
                first_data.food_details = food_details
                first_data.work_details = work_details
                first_data.climate_details = climate_details
                first_data.foodallergy = foodallergy
                first_data.chemicalallergy = chemicalallergy
                first_data.family_details = family_details
                first_data.otherallergy = otherallergy
                first_data.environmentallergy = environmentallergy
                first_data.noneofthebelow = noneofthebelow
                first_data.isheadandneckproblems = isheadandneckproblems
                first_data.isheartdisorders = isheartdisorders
                first_data.ishypertension = ishypertension
                first_data.isdiabetesmellitus = isdiabetesmellitus
                first_data.isthyroid = isthyroid
                first_data.isotherendocrinologydisorders = isotherendocrinologydisorders
                first_data.isliverdisorders = isliverdisorders
                first_data.isbrainandnervousdisorders = isbrainandnervousdisorders
                first_data.iseyedisorders = iseyedisorders
                first_data.iseardisorders = iseardisorders
                first_data.isnosedisorders = isnosedisorders
                first_data.isthroatissues = isthroatissues
                first_data.ischestlungissues = ischestlungissues
                first_data.isintestinaldisorders = isintestinaldisorders
                first_data.isurinaryproblem = isurinaryproblem
                first_data.isbackissues = isbackissues
                first_data.iscirculationproblem = iscirculationproblem
                first_data.isskindisorders = isskindisorders
                first_data.iskidneydisorders = iskidneydisorders
                first_data.isjointdisorders = isjointdisorders
                first_data.ispaintordiscomfort = ispaintordiscomfort
                first_data.isinfertility = isinfertility
                first_data.iscancer = iscancer
                first_data.isautoimmunediesase = isautoimmunediesase
                first_data.isulcersinthemouth = isulcersinthemouth
                first_data.ishivaids = ishivaids
                first_data.isherpestype1 = isherpestype1
                first_data.isherpestype2 = isherpestype2
                first_data.isstomachdisorders = isstomachdisorders
                first_data.istuberculosis = istuberculosis
                first_data.ishepatitis = ishepatitis
                first_data.isanyothecommunicabledisease = isanyothecommunicabledisease
                first_data.ispsychiatrydisorders = ispsychiatrydisorders

                first_data.headandneck_details =headandneck_details
                first_data.hypertension_details = hypertension_details
                first_data.heartdisorder_details =heartdisorder_details
                first_data.diabetesmellitus_details = diabetesmellitus_details
                first_data.thyroid_details =thyroid_details
                first_data.otherendocrinologydisorders_details = otherendocrinologydisorders_details
                first_data.liverdisorders_details =liverdisorders_details
                first_data.kidneydisorders_details =kidneydisorders_details
                first_data.brainandnervousdisorders_details = brainandnervousdisorders_details
                first_data.eyedisorders_details = eyedisorders_details
                first_data.eardisorders_details = eardisorders_details
                first_data.nosedisorders_details =nosedisorders_details
                first_data.throatissues_details = throatissues_details
                first_data.chestlungissues_details = chestlungissues_details
                first_data.intestinaldisorders_details = intestinaldisorders_details
                first_data.urinaryproblem_details = urinaryproblem_details
                first_data.backissues_details = backissues_details
                first_data.circulationproblem_details = circulationproblem_details
                first_data.skindisorders_details = skindisorders_details
                first_data.jointdisorders_details = jointdisorders_details
                first_data.pain_details = pain_details
                first_data.infertility_details = infertility_details
                first_data.cancer_details = cancer_details
                first_data.autoimmune_details =autoimmune_details
                first_data.ulcersinmouth_details= ulcersinmouth_details
                first_data.hivaids_details = hivaids_details
                first_data.herpestype1_details =herpestype1_details
                first_data.herpestype2_details = herpestype2_details
                first_data.stomachdisorders_details =stomachdisorders_details
                first_data.tuberculosis_details =tuberculosis_details
                first_data.hepatitis_details = hepatitis_details
                first_data.anyothecommunicabledisease_details = anyothecommunicabledisease_details
                first_data.psychiatrydisorders_details = psychiatrydisorders_details
                if(ishalifetreatmentcollec!=[]):
                    first_data.ishalifetreatments = ishalifetreatmentcollec
                if(pgmmedicalhistoryconditions[0]!=""):
                    first_data.currentmedicalhistory = currentmedicalhistorycollect
                if(pgmallopathynames[0]!=""):
                    first_data.currentallopathymedications = currentallopathymedicationscollect
                if(pgmalternatenames[0]!=""):
                    first_data.currentalternatemedications = currentalternatemedicationscollect
                if(pgmallergydrugnames[0]!=""):
                    first_data.medicationallergies = medicationallergiescollect

                first_data.incompleteregistration_pagescompleted = 3
                first_data = request.env['program.registration'].sudo().search([('access_token', '=', access_token)])
                values = {
                'programregn': first_data,
                'submitted': post.get('submitted', False),
                'current_page':4,
                'filled_pages':4,
                'gender': first_data.gender,
                'firstnamevar': nameoffamilydr,
                }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")

            if not validation_errors:
                values['validation_errors'] = validation_errors
                return request.render("isha_rejuvenation.pgmregn4", values)
            else:
                _logger.log(25, validation_errors)
                logtext = 'Date:' + datetime.now().strftime("%m/%d/%Y, %H:%M:%S")+ ' ' + ' '.join(
                    [str(elem) for elem in validation_errors])
                request.env['rejuvenation.tempdebuglog'].sudo().create({
                    'logtext': logtext
                })
                return request.render('isha_rejuvenation.exception')

        elif  first_data.access_token != None and request.httprequest.method == "GET":
            validation_errors = []
            values ={}
            try:
                values ={
                'programregn': first_data,
                'submitted': post.get('submitted', False),
                'countries': request.env['res.country'].sudo().search([]),
                'current_page': 3,
                'filled_pages': 3,
                'firstnamevar': first_data.name1,
                }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")

            values['validation_errors'] = validation_errors
            return request.render("isha_rejuvenation.pgmregn3", values)

    @http.route('/ishahealthsolutions/registrationform4', type='http', auth="public", methods=["GET","POST"], website=True, csrf=False)
    def registrationform4(self, access_token, **post):
        first_data = request.env['program.registration'].sudo().search([('access_token', '=', access_token)])
        if (first_data.incompleteregistration_pagescompleted == 5):
            values = {'object': first_data}
            return request.render("isha_rejuvenation.registrationcompletion", values)

        if first_data.access_token != None and request.httprequest.method == "POST":
            validation_errors = []
            values ={}
            try:
                pgmtreatmentyear = request.httprequest.form.getlist('pgmtreatmentyear')
                pgmtreatmentcondition = request.httprequest.form.getlist('pgmtreatmentcondition')
                pgmtreatmentdetails = request.httprequest.form.getlist('pgmtreatmentdetails')
                treamentdetailscollect = [(0, 0, {
                    'treatment_year': pgmyear,
                    'treatment_condition': pgmcondition,
                    'treatment_details': pgmtreatmentdetails,
                }) for pgmyear, pgmcondition, pgmtreatmentdetails in
                                                      zip(pgmtreatmentyear, pgmtreatmentcondition, pgmtreatmentdetails)]
                istrauma = {True: 'YES', False: 'NO'}[post.get('istrauma') == 'istrauma']
                issurgery = {True: 'YES', False: 'NO'}[post.get('issurgery') == 'issurgery']
                isbloodtransfusion = {True: 'YES', False: 'NO'}[post.get('isbloodtransfusion') == 'isbloodtransfusion']
                mobility = post.get('mobility')
                mobility_details = post.get('mobility_details')
                falling = post.get('falling')
                falling_details = post.get('falling_details')
                memoryprob = post.get('memoryprob')
                memoryprob_details = post.get('memoryprob_details')
                visionloss = post.get('visionloss')
                visionloss_details = post.get('visionloss_details')
                hearingloss = post.get('hearingloss')
                hearingloss_details = post.get('hearingloss_details')
                requirementofassis = post.get('requirementofassis')
                requirementofassis_details = post.get('requirementofassis_details')
                otherchildhoodilness = post.get('otherchildhoodilness')
                immunizationdates = post.get('immunizationdates')
                pgmfamilyrelations = ['mother','father','sibling','sibling','sibling','sibling','sibling','sibling','children','children','children','children','gmmat','gfmat','gmpat','gfpat']
                pgmfamilygenders = request.httprequest.form.getlist('pgmfamilygender')
                pgmfamilygenders = [i for i in pgmfamilygenders if i]
                pgmfamilyages = request.httprequest.form.getlist('pgmfamilyage')
                pgmfamilyages = [i for i in pgmfamilyages if i]
                pgmfamilyhealths = request.httprequest.form.getlist('pgmfamilyhealth')
                pgmfamilyhealths = [i for i in pgmfamilyhealths if i]
                familyhistorycollect = [(0, 0, {
                    'family_relation': pgmfamilyrelation,
                    'family_gender': pgmfamilygender,
                    'family_age': pgmfamilyage,
                    'family_health': pgmfamilyhealth,
                }) for pgmfamilyrelation, pgmfamilygender, pgmfamilyage, pgmfamilyhealth in
                                          zip(pgmfamilyrelations, pgmfamilygenders, pgmfamilyages,pgmfamilyhealths)]
                menstruationage = post.get('menstruationage')
                menstruationlastdate = post.get('menstruationlastdate')
                menstruationperiod = post.get('menstruationperiod')
                menstuationflow = post.get('menstuationflow')
                isheavyperiod = post.get('isheavyperiod')
                sexuallyactive = post.get('sexuallyactive')
                tryingpregnancy = post.get('tryingpregnancy')
                normalpregnancies = post.get('normalpregnancies')
                caeseareans = post.get('caeseareans')
                diabetesinpregnancy = post.get('diabetesinpregnancy')
                deliverycomplications = post.get('deliverycomplications')
                urinationProblem = post.get('urinationProblem')
                sweating = post.get('sweating')
                menstrualtension = post.get('menstrualtension')
                breastlumps = post.get('breastlumps')
                lastrectalwomen = post.get('lastrectalwomen')
                womendisorders = post.get('womendisorders')
                menprostate = post.get('menprostate')
                sedentary = post.get('sedentary')
                mildexercise = post.get('mildexercise')
                vigorousexercise = post.get('vigorousexercise')
                regularexercise = post.get('regularexercise')
                measles = post.get('measles')
                mumps = post.get('mumps')
                rubella = post.get('rubella')
                chickenpox = post.get('chickenpox')
                rheumatic_fevers = post.get('rheumatic_fevers')
                polio = post.get('polio')
                hepatitis = post.get('hepatitis')
                hepatitisimmunizationdate = post.get('hepatitisimmunizationdate')
                tetanus = post.get('tetanus')
                tetanusimmunizationdate = post.get('tetanusimmunizationdate')
                influenza = post.get('influenza')
                influenzaimmunizationdate = post.get('influenzaimmunizationdate')
                pneumonia = post.get('pneumonia')
                pneumoniaimmunizationdate = post.get('pneumoniaimmunizationdate')
                immunechickenpox = post.get('immunechickenpox')
                chickenpoximmunizationdate = post.get('chickenpoximmunizationdate')
                mmr = post.get('mmr')
                mmrimmunizationdate = post.get('mmrimmunizationdate')


                if(post.get('mood-yes-no') == "YES"):
                    ismood = "YES";
                else:
                    ismood = "NO";

                mood_details= post.get('mood_details')

                if (post.get('energy-level-yes-no') == "YES"):
                    isenergylevel = "YES";
                else:
                    isenergylevel = "NO";

                energylevel_details= post.get('energylevel_details')

                if (post.get('appetite-yes-no') == "YES"):
                    isappetite = "YES";
                else:
                    isappetite = "NO";

                appetite_details = post.get('appetite_details')

                if (post.get('sleep-yes-no') == "YES"):
                    issleep = "YES";
                else:
                    issleep = "NO";
                sleep_details = post.get('sleep_details')

                if (post.get('weight-yes-no') == "YES"):
                    isweight = "YES";
                else:
                    isweight = "NO";
                weight_details = post.get('weight_details')

                if (post.get('sexual-function-yes-no') == "YES"):
                    issexualfunction = "YES";
                else:
                    issexualfunction = "NO";
                sexualfunction_details = post.get('sexualfunction_details')

                if (post.get('bowel-yes-no') == "YES"):
                    isbowel = "YES";
                else:
                    isbowel = "NO";
                bowel_details = post.get('bowel_details')


                ispsychologicalcondition = post.get("current-psychological-condition-yes-no")
                isphysicalcondition = post.get("current-physical-condition-yes-no")

                first_data.istrauma = istrauma
                first_data.issurgery = issurgery
                first_data.isbloodtransfusion = isbloodtransfusion
                if (pgmtreatmentyear[0] != ""):
                    first_data.treatment_details = treamentdetailscollect
                first_data.mobility = mobility
                first_data.mobility_details = mobility_details
                first_data.falling = falling
                first_data.falling_details = falling_details
                first_data.memoryprob = memoryprob
                first_data.memoryprob_details = memoryprob_details
                first_data.visionloss = visionloss
                first_data.visionloss_details = visionloss_details
                first_data.hearingloss = hearingloss
                first_data.hearingloss_details = hearingloss_details
                first_data.requirementofassis = requirementofassis
                first_data.requirementofassis_details = requirementofassis_details
                first_data.immunizationdates = immunizationdates
                first_data.otherchildhoodilness = otherchildhoodilness
                if (pgmfamilyrelations[0] != ""):
                    first_data.family_history = familyhistorycollect
                first_data.menstruationage = menstruationage
                first_data.menstruationlastdate = menstruationlastdate
                first_data.menstruationperiod = menstruationperiod
                first_data.menstuationflow = menstuationflow
                first_data.isheavyperiod = isheavyperiod
                first_data.sexuallyactive = sexuallyactive
                first_data.tryingpregnancy = tryingpregnancy
                first_data.normalpregnancies = normalpregnancies
                first_data.caeseareans = caeseareans
                first_data.diabetesinpregnancy = diabetesinpregnancy
                first_data.urinationProblem = urinationProblem
                first_data.sweating = sweating
                first_data.menstrualtension = menstrualtension
                first_data.breastlumps = breastlumps
                first_data.lastrectalwomen = lastrectalwomen
                first_data.womendisorders = womendisorders
                first_data.menprostate = menprostate
                first_data.sedentary = sedentary
                first_data.mildexercise = mildexercise
                first_data.regularexercise = regularexercise
                first_data.vigorousexercise = vigorousexercise
                first_data.deliverycomplications = deliverycomplications
                first_data.ismood = ismood
                first_data.mood_details = mood_details
                first_data.isenergylevel = isenergylevel
                first_data.energylevel_details = energylevel_details
                first_data.isappetite = isappetite
                first_data.appetite_details = appetite_details
                first_data.issleep = issleep
                first_data.sleep_details = sleep_details
                first_data.isweight = isweight
                first_data.weight_details = weight_details
                first_data.issexualfunction = issexualfunction
                first_data.sexualfunction_details = sexualfunction_details
                first_data.isbowel = isbowel
                first_data.bowel_details = bowel_details

                first_data.ispsychologicalcondition =ispsychologicalcondition
                first_data.isphysicalcondition =isphysicalcondition

                first_data.measles = measles
                first_data.mumps = mumps
                first_data.rubella = rubella
                first_data.chickenpox = chickenpox
                first_data.rheumaticfever = rheumatic_fevers
                first_data.polio = polio
                first_data.hepatitis = hepatitis
                first_data.hepatitisimmunizationdate = hepatitisimmunizationdate
                first_data.tetanus = tetanus
                first_data.tetanusimmunizationdate = tetanusimmunizationdate
                first_data.pneumonia = pneumonia
                first_data.pneumoniaimmunizationdate = pneumoniaimmunizationdate
                first_data.influenza = influenza
                first_data.influenzaimmunizationdate = influenzaimmunizationdate
                first_data.immunizationchickenpox = immunechickenpox
                first_data.chickenpoximmunizationdate = chickenpoximmunizationdate
                first_data.mmr = mmr
                first_data.mmrimmunizationdate = mmrimmunizationdate
                first_data.incompleteregistration_pagescompleted = 4
                programname = ''
                if(first_data.programapplied.pgmschedule_programtype.programtype):
                    programname = first_data.programapplied.pgmschedule_programtype.programtype;

                first_data = request.env['program.registration'].sudo().search([('access_token', '=', access_token)])
                values = {
                        'programregn': first_data,
                        'programname': programname,
                        'submitted': post.get('submitted', False),
                        'current_page': 5,
                        'filled_pages': 5,
                            }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")

            if not validation_errors:
                values['validation_errors'] = validation_errors
                print('programname: ', values['programname'])

                selected_consent = request.env['rejuvenation.consent.versioning'].sudo().search([('consent_active', '=', 'True')])
                selected_formvers = request.env['rejuvenation.pdfversion'].sudo().search([('active_version', '=', 'True')])
                values = {
                    'programregn': first_data,
                    'programname': programname,
                    'submitted': post.get('submitted', False),
                    'current_page': 5,
                    'filled_pages': 5,
                    'consentversion': selected_consent,
                    'formversion': selected_formvers,
                }
                return request.render("isha_rejuvenation.pgmregn5", values)
            else:
                _logger.log(25, validation_errors)
                logtext = 'Date:' + datetime.now().strftime("%m/%d/%Y, %H:%M:%S")+ ': ' +  ' '.join(
                    [str(elem) for elem in validation_errors])
                request.env['rejuvenation.tempdebuglog'].sudo().create({
                    'logtext': logtext
                })
                return request.render('isha_rejuvenation.exception')

        elif first_data.access_token !=None and request.httprequest.method == "GET":
            validation_errors = []
            values ={}
            try:
                values={
                'programregn': first_data,
                'submitted': post.get('submitted', False),
                'current_page': 4,
                'filled_pages': 4,
                'gender': first_data.gender,
                'firstnamevar': first_data.nameoffamilydr,
                }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")

            values['validation_errors'] = validation_errors
            return request.render("isha_rejuvenation.pgmregn4", values)

    @http.route('/ishahealthsolutions/registrationform5', type='http', auth="public", methods=["GET","POST"], website=True, csrf=False)
    def registrationform5(self, access_token, **post):
        first_data = request.env['program.registration'].sudo().search([('access_token', '=', access_token)])
        if(first_data.incompleteregistration_pagescompleted == 5):
            values = {'object': first_data}
            return request.render("isha_rejuvenation.registrationcompletion",values)

        if first_data.access_token != None and request.httprequest.method == "POST":
            validation_errors = []
            values = {}
            try:
                dieting = post.get('dieting')
                fasted = post.get('fasted')
                areyouonmedication = post.get('areyouonmedication')
                mealperday = post.get('mealperday')
                vegetarian = post.get('vegetarian')
                dietinfast = post.get('dietinfast')
                fasttime = post.get('fasttime')

                coffee = post.get('coffee')
                cupscoffeaday = post.get('cupscoffeaday')
                alcohol = post.get('alcohol')
                alcoholkind = post.get('alcoholkind')
                alcoholperweek = post.get('alcoholperweek')
                tobacco = post.get('tobacco')
                cigarattesusage = post.get('cigarattesusage')
                tobaccousage = post.get('tobaccousage')
                pipeusage = post.get('pipeusage')
                cigarusage = post.get('cigarusage')
                tobaccostageyrsofuse = post.get('tobaccostageyrsofuse')
                tobaccostageyrsquit = post.get('tobaccostageyrsquit')
                cigarattespacksOccassional = post.get('cigarattespacksOccassional')
                cigarattespacksday = post.get('cigarattespacksday')
                tobaccoleaves = post.get('tobaccoleaves')
                tobaccoleavesday = post.get('tobaccoleavesday')
                pipeselection = post.get('pipeselection')
                pipeday = post.get('pipeday')
                cigarselection = post.get('cigarselection')
                cigarday = post.get('cigarday')
                tobaccoothers = post.get('tobaccoothers')
                streetdrugs = post.get('streetdrugs')
                needledrugs = post.get('needledrugs')
                areyoudiabetic = post.get('areyoudiabetic')
                yeardiagnosisofdiabetes = post.get('yeardiagnosisofdiabetes')
                lastvisittophysician = post.get('lastvisittophysician')
                increasedappetite = post.get('increasedappetite')
                increasedthirst = post.get('increasedthirst')
                increasedurination = post.get('increasedurination')
                symptomslowsugar = post.get('symptomslowsugar')
                badbreath = post.get('badbreath')
                compulsiveeating = post.get('compulsiveeating')
                numbness = post.get('numbness')
                footulcers = post.get('footulcers')
                genitalinfection = post.get('genitalinfection')
                breathatrest = post.get('breathatrest')
                breathatphysicalactivity = post.get('breathatphysicalactivity')
                chestpainatrest = post.get('chestpainatrest')
                chestpainatphysicalactivity = post.get('chestpainatphysicalactivity')
                palpiation = post.get('palpiation')
                giddiness = post.get('giddiness')
                swellingoflegs = post.get('swellingoflegs')
                areyousufferingheart = post.get('areyousufferingheart')
                areyoueyeproblem = post.get('areyoueyeproblem')
                lasteyeexamination = post.get('lasteyeexamination')
                opthalmologistdiagnosis = post.get('opthalmologistdiagnosis')
                refractivedate = post.get('refractivedate')
                whatuuse = post.get('whatuuse')
                eyepressure = post.get('eyepressure')
                doyoumusculoskeletal = post.get('doyoumusculoskeletal')
                condition = post.get('condition')
                yearofdiagnosiscondition = post.get('yearofdiagnosiscondition')
                aching = post.get('aching')
                dull = post.get('dull')
                tingling = post.get('tingling')
                heavy = post.get('heavy')
                crushing = post.get('crushing')
                radiating = post.get('radiating')
                cramping = post.get('cramping')
                sharp = post.get('sharp')
                stabbing = post.get('stabbing')
                electric = post.get('electric')
                continuous = post.get('continuous')
                intermittent = post.get('intermittent')
                occasional = post.get('occasional')
                duration = post.get('duration')
                morning = post.get('morning')
                afternoon = post.get('afternoon')
                evening = post.get('evening')
                night = post.get('night')
                painrelief = post.get('painrelief')
                comfortableposture = post.get('comfortableposture')
                medicationalleviatepain = post.get('medicationalleviatepain')
                durationofmedication = post.get('durationofmedication')
                jointswellingreason = post.get('jointswellingreason')
                weaknesreason = post.get('weaknesreason')
                feverreason = post.get('feverreason')
                sleepposture = post.get('sleepposture')
                fallreason = post.get('fallreason')
                unabl2walk = post.get('unabl2walk')
                unabl2move = post.get('unabl2move')
                unabl2work = post.get('unabl2work')
                noneoftheabove = post.get('noneoftheabove')
                if (post.get('swelling-yes-no') == "YES"):
                    jointswelling = "YES";
                else:
                    jointswelling = "NO";
                if (post.get('weakness-yes-no') == "YES"):
                    weakness = "YES";
                else:
                    weakness = "NO";
                if (post.get('fever-yes-no') == "YES"):
                    fever = "YES";
                else:
                    fever = "NO";
                if (post.get('fallen-without-being-pushed-yes-no') == "YES"):
                    fallen = "YES";
                else:
                    fallen = "NO";
                consent_agreed_status=post.get('jspgmregfmiagree')
                consent_timestamp=""
                tmp_consent_version_id=""
                form_version_id=""
                ipaddress=""
                if consent_agreed_status:
                    tmp_consent_version_id=post.get('cons_ver')
                    form_version_id = post.get('form_ver')
                    consent_timestamp=datetime.now()
                    ipaddress=post.get('ipvalue')
                selecteyes = ['left','right']
                pgmsph1s = request.httprequest.form.getlist('pgmsph1')
                pgmcyl1s = request.httprequest.form.getlist('pgmcyl1')
                pgmaxis1s = request.httprequest.form.getlist('pgmaxis1')
                pgmva1s = request.httprequest.form.getlist('pgmva1')
                pgmsph2s = request.httprequest.form.getlist('pgmsph2')
                pgmcyl2s = request.httprequest.form.getlist('pgmcyl2')
                pgmaxis2s = request.httprequest.form.getlist('pgmaxis2')
                pgmva2s = request.httprequest.form.getlist('pgmva2')
                if(all('' == s or s.isspace() for s in pgmsph1s) and  all('' == s or s.isspace() for s in pgmcyl1s)
                 and all('' == s or s.isspace() for s in pgmaxis1s) and  all('' == s or s.isspace() for s in pgmva1s)
                        and all('' == s or s.isspace() for s in pgmsph2s) and all('' == s or s.isspace() for s in pgmcyl2s)
                        and all('' == s or s.isspace() for s in pgmaxis2s) and all('' == s or s.isspace() for s in pgmva2s)):
                    refractive_errorcollect=[]
                else:
                    refractive_errorcollect = [(0, 0, {
                        'select_eye':selecteye,
                        'dvsph': pgmsph1,
                        'dvcyl': pgmcyl1,
                        'dvaxis': pgmaxis1,
                        'dvva': pgmva1,
                        'nvsph': pgmsph2,
                        'nvcyl': pgmcyl2,
                        'nvaxis': pgmaxis2,
                        'nvva': pgmva2,
                    }) for selecteye, pgmsph1, pgmcyl1, pgmaxis1, pgmva1,pgmsph2,pgmcyl2,pgmaxis2,pgmva2 in
                                            zip(selecteyes,pgmsph1s, pgmcyl1s, pgmaxis1s, pgmva1s,pgmsph2s,pgmcyl2s,pgmaxis2s,pgmva2s)]

                pgmjoints = ['neck','back','shoulder','elbow','wrist','hips','Knees','ankle','Handsfeet ','temporojoint','sciatica','others']
                pgmdegreeofpainsright = request.httprequest.form.getlist('pgmdegreeofpainright')
                pgmdegreeofpainsleft = request.httprequest.form.getlist('pgmdegreeofpainleft')
                pgmrights = request.httprequest.form.getlist('pgmright')
                pgmlefts = request.httprequest.form.getlist('pgmleft')
                if(all('' == s or s.isspace() for s in pgmrights) and  all('' == s or s.isspace() for s in pgmlefts)):
                    jointsaffectedcollect=[]
                else:
                    jointsaffectedcollect = [(0, 0, {
                        'joints': pgmjoint,
                        'paindegreeright': pgmdegreeofpainright,
                        'right': pgmright,
                        'paindegreeleft': pgmdegreeofpainleft,
                        'left': pgmleft,
                    }) for pgmjoint, pgmdegreeofpainright, pgmright, pgmdegreeofpainleft, pgmleft in
                                            zip(pgmjoints, pgmdegreeofpainsright, pgmrights,pgmdegreeofpainsleft, pgmlefts)]

                painincreases = ['sitting','standing','bendingf','bendingb','lyingbed','stress','weatherchanges','others']
                durationofincreasedsensations = request.httprequest.form.getlist('durationofincreasedsensation')
                if (all('' == s or s.isspace() for s in durationofincreasedsensations)):
                    sensationdurationcollect = []
                else:
                    sensationdurationcollect = [(0, 0, {
                        'doespainincrease': painincrease,
                        'durationofincreasedsensations': durationofincreasedsensation,
                    }) for painincrease, durationofincreasedsensation  in
                                            zip(painincreases, durationofincreasedsensations )]

                pgmupldinves = request.httprequest.files.getlist('pgmupldinves')
                pgmupldecho = request.httprequest.files.getlist('pgmupldecho')


                if (pgmsph1s[0] != ""):
                    first_data.refractive_error = refractive_errorcollect

                if (pgmjoints[0] != ""):
                    first_data.jointsaffected = jointsaffectedcollect

                if (durationofincreasedsensations[0] != ""):
                    first_data.sensationduration = sensationdurationcollect
                first_data.dieting = dieting
                first_data.fasted = fasted
                first_data.areyouonmedication = areyouonmedication
                first_data.mealperday = mealperday
                first_data.vegetarian = vegetarian
                first_data.dietinfast = dietinfast
                first_data.fasttime = fasttime
                first_data.consent_agreed=consent_agreed_status
                first_data.ct_timestamp=consent_timestamp
                first_data.ip_address = ipaddress
                first_data.coffee = coffee
                first_data.cupscoffeaday = cupscoffeaday
                first_data.alcohol = alcohol
                first_data.alcoholkind = alcoholkind
                first_data.alcoholperweek = alcoholperweek
                first_data.tobacco = tobacco
                first_data.tobaccostageyrsofuse = tobaccostageyrsofuse
                first_data.tobaccostageyrsquit = tobaccostageyrsquit
                first_data.tobaccousage = tobaccousage
                first_data.cigarattesusage = cigarattesusage
                first_data.pipeusage = pipeusage
                first_data.cigarusage = cigarusage
                first_data.cigarattespacksOccassional = cigarattespacksOccassional
                first_data.cigarattespacksday = cigarattespacksday
                first_data.tobaccoleaves = tobaccoleaves
                first_data.tobaccoleavesday = tobaccoleavesday
                first_data.pipeselection = pipeselection
                first_data.pipeday = pipeday
                first_data.cigarselection = cigarselection
                first_data.cigarday = cigarday
                first_data.tobaccoothers = tobaccoothers
                first_data.streetdrugs = streetdrugs
                first_data.needledrugs = needledrugs
                first_data.yeardiagnosisofdiabetes = yeardiagnosisofdiabetes
                first_data.lastvisittophysician = lastvisittophysician
                first_data.increasedappetite = increasedappetite
                first_data.increasedthirst = increasedthirst
                first_data.increasedurination = increasedurination
                first_data.symptomslowsugar = symptomslowsugar
                first_data.badbreath = badbreath
                first_data.compulsiveeating = compulsiveeating
                first_data.numbness = numbness
                first_data.footulcers = footulcers
                first_data.genitalinfection = genitalinfection
                first_data.breathatrest = breathatrest
                first_data.breathatphysicalactivity = breathatphysicalactivity
                first_data.chestpainatrest = chestpainatrest
                first_data.chestpainatphysicalactivity = chestpainatphysicalactivity
                first_data.palpiation = palpiation
                first_data.giddiness = giddiness
                first_data.swellingoflegs = swellingoflegs
                first_data.areyousufferingheart = areyousufferingheart
                first_data.areyoueyeproblem = areyoueyeproblem
                first_data.lasteyeexamination = lasteyeexamination
                first_data.opthalmologistdiagnosis = opthalmologistdiagnosis
                first_data.refractivedate = refractivedate
                first_data.whatuuse = whatuuse
                first_data.eyepressure = eyepressure
                first_data.areyoudiabetic = areyoudiabetic
                first_data.doyoumusculoskeletal = doyoumusculoskeletal
                first_data.condition = condition
                first_data.yearofdiagnosiscondition = yearofdiagnosiscondition
                first_data.aching = aching
                first_data.dull = dull
                first_data.tingling = tingling
                first_data.heavy = heavy
                first_data.crushing = crushing
                first_data.radiating = radiating
                first_data.cramping = cramping
                first_data.sharp = sharp
                first_data.stabbing = stabbing
                first_data.electric = electric
                first_data.continuous = continuous
                first_data.intermittent = intermittent
                first_data.occasional = occasional
                first_data.duration = duration
                first_data.morning = morning
                first_data.afternoon = afternoon
                first_data.evening = evening
                first_data.night = night
                first_data.painrelief = painrelief
                first_data.comfortableposture = comfortableposture
                first_data.medicationalleviatepain = medicationalleviatepain
                first_data.durationofmedication = durationofmedication
                first_data.jointswelling = jointswelling
                first_data.jointswellingreason = jointswellingreason
                first_data.weakness = weakness
                first_data.weaknesreason = weaknesreason
                first_data.fever = fever
                first_data.feverreason = feverreason
                first_data.sleepposture = sleepposture
                first_data.fallen = fallen
                first_data.fallreason = fallreason
                first_data.unabl2walk = unabl2walk
                first_data.unabl2move = unabl2move
                first_data.unabl2work = unabl2work
                first_data.noneoftheabove = noneoftheabove
                first_data.consent_version=tmp_consent_version_id
                first_data.form_version=form_version_id

                if(len(pgmupldinves)!=0):
                    read1 = pgmupldinves[0].read()
                    datas1 = base64.b64encode(read1)
                    if datas1:
                        new_attachment1 = request.env['ir.attachment'].sudo().create({
                            'name': 'bloodreports_file',
                            'datas': datas1,
                            'res_field': 'bloodreports_file',
                            'res_model': 'program.registration',
                            'res_id': first_data.id
                        })


                if (len(pgmupldecho) != 0):
                    read2 = pgmupldecho[0].read()
                    datas2 = base64.b64encode(read2)
                    if datas2:
                        new_attachment2 = request.env['ir.attachment'].sudo().create({
                            'name': 'echoreports_file',
                            'datas': datas2,
                            'res_field': 'echoreports_file',
                            'res_model': 'program.registration',
                            'res_id': first_data.id
                        })

                first_data.incompleteregistration_pagescompleted = 5
                first_data.registration_status = "Pending"
                first_data.registration_email_send= False
                first_data.registration_sms_send= False
                first_data = request.env['program.registration'].sudo().search([('access_token', '=', access_token)])
                values =    {'object': first_data}
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")

            if not validation_errors:
                values['validation_errors']= validation_errors
                return request.render("isha_rejuvenation.registrationsuccess2",  values)
            else:
                _logger.log(25, validation_errors)
                logtext = 'Date:' + datetime.now().strftime("%m/%d/%Y, %H:%M:%S") + ': ' + ' '.join(
                    [str(elem) for elem in validation_errors])

                request.env['rejuvenation.tempdebuglog'].sudo().create({
                    'logtext': logtext
                })
                return request.render('isha_rejuvenation.exception')
        elif first_data.access_token !=None and request.httprequest.method == "GET":
            validation_errors = []
            values ={}
            try:
                values ={
                    'programregn': first_data,
                    'programname': first_data.programapplied.pgmschedule_programtype.programtype,
                    'submitted': post.get('submitted', False),
                    'current_page': 5,
                    'filled_pages': 5,
                        }
            except (UserError, AccessError, ValidationError) as exc:
                validation_errors.append(ustr(exc))
            except Exception:
                _logger.error(
                    "Error caught during request creation", exc_info=True)
                validation_errors.append("Unknown server error. See server logs.")

            values['validation_errors'] = validation_errors
            selected_consent=request.env['rejuvenation.consent.versioning'].sudo().search([('consent_active','=','True')])
            selected_formvers = request.env['rejuvenation.pdfversion'].sudo().search([('active_version', '=', 'True')])
            values = {
                'consentversion': selected_consent,
                'formversion': selected_formvers,
            }
            return request.render("isha_rejuvenation.pgmregn5", values)


    @http.route('/ishahealthsolutions/get_center', type='http', auth="public", methods=['GET'], website=True, sitemap=False)
    def scope_location_data(self, country_id=None, state_id=None, **post):

        state_ids = {}
        center_ids = {}
        if country_id:
            # For india return the possible states
            if int(country_id) == 104:
                state_data = request.env['res.country.state'].sudo().search_read(
                    domain=[('country_id', '=', int(country_id))],
                    fields=['id','name']
                )
                for state in state_data:
                    state_ids[str(state['id'])] = state['name']

            else:
                # For non india get the list of centers from i_p_c and r_c
                center_data = request.env['isha.pincode.center'].sudo().search_read(
                    domain=[('country_id', '=', int(country_id))],
                    fields=['center_id']
                )

                center_data += request.env['res.country'].sudo().search_read(
                        domain=[('id', '=', int(country_id))],
                        fields=['center_id']
                    )

                for center in center_data:
                    if center['center_id']:
                        center_ids[str(center['center_id'][0])] = True
        elif state_id:
            # For direct state selection get the center from r_c_s directly
            center_data = request.env['res.country.state'].sudo().search_read(
                domain=[('id', '=', int(state_id))],
                fields=['center_ids']
            )
            for state in center_data:
                for center in state['center_ids']:
                    center_ids[str(center)] = True

        result = {'state_ids': state_ids, 'center_ids':center_ids}
        return json.dumps(result)
