function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function () {
            defer(method)
        }, 50);
    }
}

function getip(url){
    return fetch(url).then(res=>res.text());
}

//https://checkip.amazonaws.com/
//https://www.cloudflare.com/cdn-cgi/trace
getip('https://www.cloudflare.com/cdn-cgi/trace').then(data=> {
    ip = data.split("\n")[2].split("=")[1];
    $('#ipvalue').val(ip);
})

defer(jspgmregfmdcoumentready);
function jspgmregfmdcoumentready() {
$(document).ready(function () {

//    tmpip=fetch("https://checkip.amazonaws.com/", {mode: 'no-cors'})
//    tmpip1=tmpip.text;
//    console.log(tmpip1);


  // Initialize select2
  $(".selUser").select2();

  // Read selected option
  $('#but_read').click(function () {
    var username = $('.selUser option:selected').text();
    var userid = $('.selUser').val();

    $('#result').html("id : " + userid + ", name : " + username);
  });

  $(".selUser").select2({
    placeholder: "None",
    allowClear: true
  });


  $('b[role="presentation"]').hide();
  $('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>');

  add_table_delete_behaviour('refractive-error-table-add','refractive-error-table');
  add_table_delete_behaviour('joints-affected-table-add','joints-affected-table');
  add_table_delete_behaviour('sensationduration-table-add','sensationduration-table');

  displayTextInputOnCheck("#display-echo-report-upload-checkox","#echo-report-upload-popup")


  displayTextInputOnCheck("#display-eye-problems-details-checkbox","#eye-problems-details-popup")


  displayTextBoxforDetails("swelling-yes-no", "#swelling-yes","#swelling-details" )
  displayTextBoxforDetails("restricted-movements-yes-no", "#restricted-movements-yes","#restricted-movements-details" )
  displayTextBoxforDetails("weakness-yes-no", "#weakness-yes","#weakness-details" )
  displayTextBoxforDetails("fever-yes-no", "#fever-yes","#fever-details" )
  displayTextBoxforDetails("posture-during-sleep-yes-no", "#posture-during-sleep-yes","#posture-during-sleep-details" )
  displayTextBoxforDetails("fallen-without-being-pushed-yes-no", "#fallen-without-being-pushed-yes","#fallen-without-being-pushed-details" )

  displayTextInputOnCheck("#display-joint-musculoskeletal-disorders-checkbox","#joint-musculoskeletal-disorders-popup")

  makeTextDetailsMandt("#jointswellingreason","#swelling-yes","#swelling-no")
  makeTextDetailsMandt("#weaknesreason","#weakness-yes","#weakness-no")
  makeTextDetailsMandt("#feverreason","#fever-yes","#fever-no")
  makeTextDetailsMandt("#fallreason","#fallen-without-being-pushed-yes","#fallen-without-being-pushed-no")

  makedietingelementsrequired()
  makecoffeeelementsrequired()
  makealcoholelementsrequired()
  maketobaccoelementsrequired()
  makediabetesmandtOnCheck("#display-diabetes-details-popup-checkbox")
  makediabetesmandtOnload()
  makeeyesmandtOnCheck("#display-eye-problems-details-checkbox")
  makejointmandtOnCheck("#display-joint-musculoskeletal-disorders-checkbox")
  makedietmandatOnCheck('#jspgmregdieting')

  setCurrentConditions('#current-physical-condition-good','Good')
  setCurrentConditions('#current-physical-condition-fair','Fair')
  setCurrentConditions('#current-physical-condition-poor','Poor')
  setCurrentConditions('#current-psychological-condition-good','Good')
  setCurrentConditions('#current-psychological-condition-fair','Fair')
  setCurrentConditions('#current-psychological-condition-poor','Poor')

  setperdayvisible('#jspgmregcigarattespacks','#jspgmrecigarettespacksdaylabel', '#jspgmregcigarattespacksday')
  setperdayvisible('#jspgmregtobaccoleaves','#jspgmregtobaccoleavesdaylabel', '#jspgmregtobaccoleavesday')
  setperdayvisible('#jspgmregpipeselection','#jspgmregpipedaylabel', '#jspgmregpipeday')
  setperdayvisible('#jspgmregcigarselection','#jspgmregcigardaylabel', '#jspgmregcigarday')

enabletobacco('#jspgmregfmpipeusage','.jspgmregpipescls','#jspgmregpipeselection', '#jspgmregpipeday','#jspgmregpipedaylabel')
enabletobacco('#jspgmregfmcigarusage','.jspgmregcigarscls','#jspgmregcigarselection', '#jspgmregcigarday','#jspgmregcigardaylabel')
enabletobacco('#jspgmregfmtobaccousage','.jspgmregtobaccoleavesscls','#jspgmregtobaccoleaves', '#jspgmregtobaccoleavesday','#jspgmregtobaccoleavesdaylabel')
enabletobacco('#jspgmregfmcigarettesusage','.jspgmregcigarettescls','#jspgmregcigarattespacks', '#jspgmregcigarattespacksday','#jspgmrecigarettespacksdaylabel')
maketobaccomandtOnCheck('#jspgmregtobacco')
});
}

function enabletobacco(checkbox, tobaccoclass,tobaccoselection,tobaccoperday,tobaccoperdaylabel){
 $(checkbox).change(function () {
 if ($(this).is(":checked"))
 {
       $(tobaccoclass).css("display","inline-block")
       $(tobaccoperday).css("display","none")
       $(tobaccoperdaylabel).css("display","none")
 }
 else
 {
       $(tobaccoclass).css("display","none")
       $(tobaccoselection).empty();
       $(tobaccoselection).append('<option></option>')
       $(tobaccoselection).append('<option value="perday">packs/day</option>')
       $(tobaccoselection).append('<option value="Occasionally">Occasionally</option>')
       $(tobaccoperday).val('')
 }
 })
}

function resetvaluesoftobaccoonuncheck(checkbox, tobaccoclass,tobaccoselection,tobaccoperday,tobaccoperdaylabel)
{
       $(checkbox).prop("checked", false);
       $(tobaccoclass).css("display","none")
       $(tobaccoselection).empty();
       $(tobaccoselection).append('<option></option>')
       $(tobaccoselection).append('<option value="perday">packs/day</option>')
       $(tobaccoselection).append('<option value="Occasionally">Occasionally</option>')
       $(tobaccoperday).val('')
       $(tobaccoperday).prop('required',  false)

}


function maketobaccomandtOnCheck(checkbox){
 $(checkbox).change(function () {
 if ($(this).val()=='YES')
 {
       $('.jspgmregtobaccocls').css("display","block")
 }
 else
{
     $('.jspgmregtobaccocls').css("display","none")
     $('#jspgmregtobaccoothers').val('')
     $('#jspgmregtobaccostage').val('')
     $('#jspgmregtobaccostagequit').val('')
     resetvaluesoftobaccoonuncheck('#jspgmregfmpipeusage','.jspgmregpipescls','#jspgmregpipeselection', '#jspgmregpipeday','#jspgmregpipedaylabel')
     resetvaluesoftobaccoonuncheck('#jspgmregfmcigarusage','.jspgmregcigarscls','#jspgmregcigarselection', '#jspgmregcigarday','#jspgmregcigardaylabel')
     resetvaluesoftobaccoonuncheck('#jspgmregfmtobaccousage','.jspgmregtobaccoleavesscls','#jspgmregtobaccoleaves', '#jspgmregtobaccoleavesday','#jspgmregtobaccoleavesdaylabel')
     resetvaluesoftobaccoonuncheck('#jspgmregfmcigarettesusage','.jspgmregcigarettescls','#jspgmregcigarattespacks', '#jspgmregcigarattespacksday','#jspgmrecigarettespacksdaylabel')

}
})
}

function setperdayvisible(tobaccopack, tobaccoperdaylabel,tobaccoperday)
{
$(tobaccopack).change(function(){
if($(tobaccopack).val()=='perday')
    {
        $(tobaccoperdaylabel).css("display","block")
        $(tobaccoperday).css("display","block")
        $(tobaccoperdaylabel).prop('class',  'required')
        $(tobaccoperday).prop('required',  true)
    }
    else
        {
            $(tobaccoperdaylabel).css("display","none")
            $(tobaccoperday).css("display","none")
            $(tobaccoperdaylabel).prop('class',  '')
            $(tobaccoperday).prop('required',  false)
            $(tobaccoperday).val('')
        }

})
}

function setCurrentConditions(radiobutton,value){
$(radiobutton).change(function () {
 if ($(radiobutton).is(":checked"))
    $(radiobutton).val(value)
  })
}

function makedietmandatOnCheck(dropdown){
  $(dropdown).change(function () {
         if($(dropdown).val() == 'YES')
 {
       $('.cspgrgmandtdietcls').prop('required', true);
       $('#jspgmregareyouonmedicationlabel').prop('class',  'required');
       $('#jspgmregmealperdaylabel').prop('class',  'required');
       $('#jspgmregvegetarianlabel').prop('class',  'required');
 }
 else
{
     $('.cspgrgmandtdietcls').prop('required', false);
          $('#jspgmregareyouonmedicationlabel').prop('class',  '');
       $('#jspgmregmealperdaylabel').prop('class',  '');
       $('#jspgmregvegetarianlabel').prop('class',  '');
}
})
}

function makejointmandtOnCheck(checkbox){
$(checkbox).change(function () {
 if ($(this).is(":checked"))
 {
       $('.cspgrgmandtjointdiscls').prop('required', true);
 }
 else
{
     $('.cspgrgmandtjointdiscls').prop('required', false);
}
})
}

function makeeyesmandtOnCheck(checkbox){
 $(checkbox).change(function () {
 if ($(this).is(":checked"))
 {
       $('.cspgrgmandteyecls').prop('required', true);
 }
 else
{
     $('.cspgrgmandteyecls').prop('required', false);
}
})
}

function makediabetesmandtOnload()
{
if ($('#display-diabetes-details-popup-checkbox').is(":checked"))
 {
    $("#diabetes-details-popup").css("display", "block");
    $('#yeardiagnosisofdiabetes').prop('required', true);
    $('#lastvisittophysician').prop('required', true);
    $('#increasedappetite').prop('required', true);
    $('#increasedthirst').prop('required', true);
    $('#increasedurination').prop('required', true);
    $('#symptomslowsugar').prop('required', true);
    $('#badbreath').prop('required', true);
    $('#compulsiveeating').prop('required', true);
    $('#numbness').prop('required', true);
    $('#footulcers').prop('required', true);
    $('#genitalinfection').prop('required', true);
    $('#breathatrest').prop('required', true);
    $('#breathatphysicalactivity').prop('required', true);
    $('#chestpainatrest').prop('required', true);
    $('#chestpainatphysicalactivity').prop('required', true);
    $('#palpiation').prop('required', true);
    $('#giddiness').prop('required', true);
    $('#swellingoflegs').prop('required', true);
 }
}

function makediabetesmandtOnCheck(checkbox){
 $(checkbox).change(function () {
 if ($(this).is(":checked"))
 {

   $("#diabetes-details-popup").css("display", "block");
            $('#yeardiagnosisofdiabetes').prop('required', true);
            $('#lastvisittophysician').prop('required', true);
            $('#increasedappetite').prop('required', true);
            $('#increasedthirst').prop('required', true);
            $('#increasedurination').prop('required', true);
            $('#symptomslowsugar').prop('required', true);
            $('#badbreath').prop('required', true);
            $('#compulsiveeating').prop('required', true);
            $('#numbness').prop('required', true);
            $('#footulcers').prop('required', true);
            $('#genitalinfection').prop('required', true);
            $('#breathatrest').prop('required', true);
            $('#breathatphysicalactivity').prop('required', true);
            $('#chestpainatrest').prop('required', true);
            $('#chestpainatphysicalactivity').prop('required', true);
            $('#palpiation').prop('required', true);
            $('#giddiness').prop('required', true);
            $('#swellingoflegs').prop('required', true);
 }
 else
 {
     $("#diabetes-details-popup").css("display", "none");
      $('#yeardiagnosisofdiabetes').prop('required', false);
        $('#lastvisittophysician').prop('required', false);
        $('#increasedappetite').prop('required', false);
        $('#increasedthirst').prop('required', false);
        $('#increasedurination').prop('required', false);
        $('#symptomslowsugar').prop('required', false);
        $('#badbreath').prop('required', false);
        $('#compulsiveeating').prop('required', false);
        $('#numbness').prop('required', false);
        $('#footulcers').prop('required', false);
        $('#genitalinfection').prop('required', false);
        $('#breathatrest').prop('required', false);
        $('#breathatphysicalactivity').prop('required', false);
        $('#chestpainatrest').prop('required', false);
        $('#chestpainatphysicalactivity').prop('required', false);
        $('#palpiation').prop('required', false);
        $('#giddiness').prop('required', false);
        $('#swellingoflegs').prop('required', false);
 }
 })
}

function maketobaccoelementsrequired(){
  $('#jspgmregtobacco').change(function () {
         if($('#jspgmregtobacco').val() == 'YES')
           {
            $('#jspgmregtobaccostage').prop('required', true);
            $('#jspgmregtobaccostagelabel').prop('class',  'required');
            $('.jspgmregtobaccocls').css("display","block")
           }
         else
           {
           $('#jspgmregtobaccostage').prop('required', false);
           $('#jspgmregtobaccostagelabel').prop('class',  '');
           $('.jspgmregtobaccocls').css("display","none")
           } })
}
function makealcoholelementsrequired(){
  $('#jspgmregalcohol').change(function () {
         if($('#jspgmregalcohol').val() == 'YES')
           {
            $('#jspgmregalcoholkind').prop('required', true);
            $('#jspgmregalcoholperweek').prop('required', true);
            $('#jspgmregalcholkind').prop('class',  'required');
            $('#jspgmregalcoholpwklabel').prop('class',  'required');
            }
         else
           {
            $('#jspgmregalcoholkind').prop('required', false);
            $('#jspgmregalcoholperweek').prop('required', false);
            $('#jspgmregalcholkind').prop('class',  '');
             $('#jspgmregalcoholpwklabel').prop('class',  '');
             } })
}

function makecoffeeelementsrequired(){
  $('#jspgmregcoffee').change(function () {
         if($('#jspgmregcoffee').val() == 'None' || $('#jspgmregcoffee').val() == '')
            {
            $('#jspgmregcupscoffeaday').prop('required', false);
             $('#jspgmregnumcoffeelabel').prop('class',  '');
             }
         else
           { $('#jspgmregcupscoffeaday').prop('required', true);
             $('#jspgmregnumcoffeelabel').prop('class',  'required');
           }
                })
}

function makedietingelementsrequired(){
        $('#jspgmregfasted').change(function () {
         if($('#jspgmregfasted').val() == 'YES')
           {
            $('#jspgmregdietinfast').prop('required', true);
             $('#jspgmregfasttime').prop('required', true);
             $('#fastwith').prop('class',  'required');
             $('#fasttime').prop('class',  'required');
             }
         else
         { $('#jspgmregdietinfast').prop('required', false);
             $('#jspgmregfasttime').prop('required', false);
               $('#fastwith').prop('class',  '');
             $('#fasttime').prop('class',  '');
             }
                })
}


function makeTextDetailsMandt(textelement, radioyes, radiono){
$(radioyes).change(function () {
    if($(this).val() =='YES')
        $(textelement).prop('required',true)
})
$(radiono).change(function () {
    if($(radioyes).val() =='NO')
        $(textelement).prop('required',false)
})
}

 function displayTextInputOnCheck(checkbox, Element) {
  $(checkbox).change(function () {
    if ($(this).is(":checked"))
      {
      $(Element).css("display", "block");
      $(Element).prop('required',true);
      }
    else
      {$(Element).css("display", "none");
      $(Element).prop('required',false);
      }
  })
}

function displayTextBoxforDetails(name, clickElement, toggleElement) {
  $("input[name="+name+"]").click(function() {
    if ($(clickElement).is(":checked")) {
    $(clickElement).val("YES");
      $(toggleElement).css("display", "block");
    } else {
      $(toggleElement).css("display", "none");
    $(clickElement).val("NO");
    }
  });
}


function jspgmregfm4validateForm()
{
retval =  jspgmvalidateothers()
retval = retval && jspgmvalidatetobacco()
retval = retval && jspgmvalidatefileupload()
retval = retval && jspgmvalidateiagree()

return retval;
}

function jspgmvalidateiagree(){
if($('#jspgmregfmiagree').is(':checked'))
    {return true;}
else
    {
    alert('Please agree to the declaration by selecting the checkbox')
    return false;
    }
}

function jspgmvalidatetobacco(){
    if($('#jspgmregtobacco').val()=='YES')
        if(!$('#jspgmregfmcigarettesusage').is(':checked') )
            if(!$('#jspgmregfmtobaccousage').is(':checked')  )
                if(!$('#jspgmregfmpipeusage').is(':checked') )
                    if(!$('#jspgmregfmcigarusage').is(':checked'))
                        if($('#jspgmregtobaccoothers').val() == '' )
                            {
                            $('#jspgmregtobaccoothers').focus()
                            alert('Please, enter atleast any other tobacco usage')
                                return false;}

return true;
}

function jspgmvalidatefileupload(){
 if ($('#display-diabetes-details-popup-checkbox').is(":checked")){
    if($('#pgmupldinves')[0].files.length === 0)
         {   alert("Blood reports attachment required");
            $('#pgmupldinves').focus();
                            return false;
            }
    if ($('#display-echo-report-upload-checkox').is(":checked"))
      {
       if($('#pgmupldecho')[0].files.length === 0){
               alert("Echo reports attachment required");
                $('#pgmupldecho').focus();
                return false;
        }}
}
 return true;
}

function jspgmvalidateothers()
{
 if ($('#display-joint-musculoskeletal-disorders-checkbox').is(":checked")){


       if($('#aching').is(":not(:checked)") &&$('#dull').is(":not(:checked)") &&$('#tingling').is(":not(:checked)") &&
        $('#heavy').is(":not(:checked)") &&$('#crushing').is(":not(:checked)") &&$('#radiating').is(":not(:checked)") &&$('#cramping').is(":not(:checked)") &&
        $('#sharp').is(":not(:checked)")&&$('#stabbing').is(":not(:checked)")&&$('#electric').is(":not(:checked)") )
        {    alert('Please, provide nature of pain');
        $('#aching').focus();
            return false;
        }

       if ($('#continuous').is(":not(:checked)") && $('#intermittent').is(":not(:checked)") && $('#occasional').is(":not(:checked)"))
        {    alert('Please, provide frequency of your pain');
        $('#continuous').focus();
            return false;
        }

         if ($('#morning').is(":not(:checked)") && $('#afternoon').is(":not(:checked)") && $('#evening').is(":not(:checked)") && $('#night').is(":not(:checked)"))
        {
          alert('Please, provide when is your pain worse');
               $('#morning').focus();
            return false;
        }

 var rows = document.getElementById("sensationduration-table").rows;
j=0;
for(i = 1; i <rows.length; i++)
{
          if(i==0) continue;

         var painincrease = $("td input[name='painincrease']",rows[i])[0].value;
         var durationofincreasedsensation = $("td input[name='durationofincreasedsensation']",rows[i])[0].value;

         if(durationofincreasedsensation!='')
         {
          if($('#painincrease').val() == 'Sitting')
            $('#painincrease').val('sitting')
          if($('#painincrease').val() == 'Standing')
            $('#painincrease').val('standing')
          if($('#painincrease').val() == 'Bending Forward')
            $('#painincrease').val('bendingf')
          if($('#painincrease').val() == 'Bending Backwards')
            $('#painincrease').val('bendingb')
           if($('#painincrease').val() == 'Lying in bed')
            $('#painincrease').val('lyingbed')
          if($('#painincrease').val() == 'Stress')
            $('#painincrease').val('stress')
          if($('#painincrease').val() == 'Weather Changes')
            $('#painincrease').val('weatherchanges')
          if($('#painincrease').val() == 'Others')
            $('#painincrease').val('others')
         }
         else
          {
          $('#painincrease').val('');
                  j++;
          }
        if(j==8)
          {
           alert('Please, fill atleast one activity sensation duration');
           $('#durationofincreasedsensation').focus()
           return false;
          }
}

        if ($('#unabl2walk').is(":not(:checked)") && $('#unabl2move').is(":not(:checked)") && $('#unabl2work').is(":not(:checked)") && $('#noneoftheabove').is(":not(:checked)"))
        {    alert('Please, provide restricted activity due to pain');
               $('#unabl2walk').focus();
            return false;
        }

 return validatejointstable();

}
return true;
}

function validatejointstable(){

var rows = document.getElementById("joints-affected-table").rows;
joint=0;
for(i = 1; i <rows.length; i++)
{
          if(i==0) continue;

         var pgmjoints = $("td input[name='pgmjoints']",rows[i])[0];
         var pgmdegreeofpainright = $("td select[name='pgmdegreeofpainright']",rows[i])[0].value;
         var pgmdegreeofpainright = $("td select[name='pgmdegreeofpainright']",rows[i])[0].value;
         var pgmright1 = $("td input[name='pgmright1']",rows[i])[0];
         var pgmright = $("td input[name='pgmright']",rows[i])[0];
         var pgmdegreeofpainleft = $("td select[name='pgmdegreeofpainleft']",rows[i])[0].value;
         var pgmleft1 = $("td input[name='pgmleft1']",rows[i])[0];
         var pgmleft = $("td input[name='pgmleft']",rows[i])[0];

         if(pgmright1.checked || pgmleft1.checked)
         {
          if(pgmjoints.value == 'Neck')
            pgmjoints.value ='neck';
          if(pgmjoints.value == 'Back')
            pgmjoints.value ='back';
          if(pgmjoints.value == 'Shoulder')
            pgmjoints.value ='shoulder';
        if(pgmjoints.value == 'Elbow')
             pgmjoints.value ='elbow';
        if(pgmjoints.value == 'Wrist')
              pgmjoints.value ='wrist';
        if(pgmjoints.value == 'Hips')
              pgmjoints.value ='hips';
        if(pgmjoints.value == 'Knees')
              pgmjoints.value ='Knees';
        if(pgmjoints.value == 'Ankle')
              pgmjoints.value ='ankle';
        if(pgmjoints.value == 'Hands and feet ')
              pgmjoints.value ='Handsfeet';
        if(pgmjoints.value == 'Temporo Mandibular joint')
              pgmjoints.value ='temporojoint';
        if(pgmjoints.value == 'Sciatica')
              pgmjoints.value ='sciatica';
        if(pgmjoints.value == 'Others')
              pgmjoints.value ='others';
            }
         else
          {
                  joint++;
            pgmjoints.value="";

          }
          if(pgmright1.checked)
            pgmright.value='YES';
          else
            pgmright.value='NO';

         if(pgmleft1.checked)
            pgmleft.value='YES';
         else
            pgmleft.value='NO';

        if(joint==12)
          {
           alert('Please, fill atleast one joint affected');
           $('#joints-affected-table').focus();
           return false;
          }
}

return true;
}


