do 
$$
declare

lv_name text :=  'Rejuvenation: Program Category Change Request';
lv_model_name text :=  'program.registration.transactions';
lv_body text := 'Namaskaram, thanks for contacting us. For program category change, Please do the payment by clicking the below link : ${object.upgrade_url | safe}. Pranam, Isha Rejuvenation';

lv_model_id integer := 0;
lv_count integer := 0;

begin
	
	select count(*) into lv_count from sms_template 
	where name = lv_name;
	
	if (lv_count > 0) then
		RAISE EXCEPTION 'SMS template already exists with this name';
	end if;
	
	
	select id into lv_model_id from ir_model
	where model = lv_model_name;
	
	if (lv_model_id > 0) then
	
		INSERT INTO public.sms_template(
		name, model_id, model, body, 
		lang, sidebar_action_id, create_uid, create_date, write_uid, write_date)
		VALUES (lv_name, lv_model_id, lv_model_name, lv_body,
		null, null, 1, now(), 1, now());
	
		raise notice 'SMS template created successfully..';
		
	else
		RAISE EXCEPTION 'Invalid model';
	end if;
end
$$;
