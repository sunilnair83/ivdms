do 
$$
declare

lv_name text :=  'Isha Health Solutions: Program Category Change Completed';
lv_model_name text :=  'program.registration.transactions';
lv_subject text :=  'Program Category Change Completed - ${object.programregistration.programapplied.pgmschedule_programname}';
lv_email_from text := 'isha.registrations@ishafoundation.org';
lv_email_reply_to text := 'isha.registrations@ishafoundation.org';

lv_email_to text := '${(object.programregistration.participant_email) |safe}';
lv_email_cc text := '${(object.programregistration.participant_email_alt) |safe}';

lv_body text := '<div style="margin: 0px; padding: 0px; font-size: 13px;">
                    <p style="margin: 0px; padding: 0px; font-size: 13px;">
                        Namaskaram ${object.programregistration.first_name or "participant"}<br/><br/>
                        Thank you for contacting us.
                        <br/><br/>
                        As per your request, program category changed to ${object.programregistration.packageselection.pgmschedule_packagetype.packagecode}
                        <br/><br/>
                        <br/><br/>
                        Pranam<br/>
                        Rejuvenation Team
                    </p>
                </div>';

lv_model_id integer := 0;
lv_count integer := 0;

begin
	
	select count(*) into lv_count from mail_template 
	where name = lv_name;
	
	if (lv_count > 0) then
		RAISE EXCEPTION 'Email template already exists with this name';
	end if;
	
	
	select id into lv_model_id from ir_model
	where model = lv_model_name;
	
	if (lv_model_id > 0) then
	
		INSERT INTO public.mail_template(
		name, model_id, model, lang, user_signature, 
		subject, email_from, use_default_to, email_to, partner_to, email_cc, reply_to, 
		mail_server_id, body_html, report_name, report_template, ref_ir_act_window, 
		auto_delete, model_object_field, sub_object, sub_model_object_field, null_value, 
		copyvalue, scheduled_date, create_uid, create_date, write_uid, write_date)
		VALUES (lv_name, lv_model_id, lv_model_name, null, false, 
		lv_subject, lv_email_from, null, lv_email_to, null, lv_email_cc, lv_email_reply_to, 
		null, lv_body, null, null, null, 
		true, null, null, null, null,
		null, null, 1, now(), 1, now());
	
		raise notice 'Email template created successfully..';
		
	else
		RAISE EXCEPTION 'Invalid model';
	end if;
end
$$;
