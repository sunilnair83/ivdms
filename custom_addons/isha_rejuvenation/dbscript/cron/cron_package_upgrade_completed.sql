do 
$$
declare

lv_name text :=  'Rejuvenation: Program Package Upgrade Completion Email and SMS';
lv_model_name text :=  'program.registration.transactions';
lv_code text := 'model.sendPackageUpgradeConfirmation()';

lv_model_id integer := 0;
lv_ir_actions_server_id integer := 0;
lv_interval_number integer := 5;
lv_interval_type text := 'minutes';

lv_count integer := 0;

begin
	
	select count(*) into lv_count from ir_cron 
	where cron_name = lv_name;
	
	if (lv_count > 0) then
		RAISE EXCEPTION 'Cron job already exists with this name';
	end if;
	
	
	select id into lv_model_id from ir_model
	where model = lv_model_name;
	
	
	if (lv_model_id > 0) then
	
		INSERT INTO public.ir_act_server(
		name, type, help, binding_model_id, binding_type, binding_view_types, create_uid, create_date, write_uid, write_date, 
		usage, state, sequence, model_id, model_name, code, crud_model_id, link_field_id, template_id, activity_type_id, activity_summary, 
		activity_note, activity_date_deadline_range, activity_date_deadline_range_type, activity_user_type, activity_user_id, 
		activity_user_field_name, sms_template_id, sms_mass_keep_log, website_path, website_published)
		VALUES 
		(lv_name, 'ir.actions.server', null, null, 'action', 'list,form', 1, now(), 1, now(),
		 'ir_cron', 'code', 5, lv_model_id, lv_model_name, lv_code, null, null, null, null, null,
		null, null, 'days', 'specific', null, 'user_id', null, true, null, null);

		SELECT LASTVAL() into lv_ir_actions_server_id;

		INSERT INTO public.ir_cron(
		ir_actions_server_id, cron_name, user_id, active, 
		interval_number, interval_type, numbercall, doall, nextcall, 
		lastcall, priority, create_uid, create_date, write_uid, write_date)
		VALUES (lv_ir_actions_server_id, lv_name, 1, true, 
		lv_interval_number, lv_interval_type, -1, null, now(), 
		null, 5, 1, now(), 1, now());

		raise notice 'cron job created successfully..';
		
	else
		RAISE EXCEPTION 'Invalid model';
	end if;
end
$$;
