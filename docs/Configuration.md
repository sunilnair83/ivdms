# Configuration Management

Isha add-ons can take the benefit of centralized configuration management features
offered by the `isha_base` add-on.

## About Configuration

#### File
The configuration file is `etc/conf/<environment>/sushumna.conf`. The project structure
enables different configuration files to be present for different environments.

Configuration for each environment is independent and bear no relationship with 
others.  There is no nesting or inheritance.

The application requires the environment variable `SUSHUMNA_CONFIG` to point to the 
correct Sushumna configuration.

#### Syntax
The file uses extended INI syntax supported by the [Python configparser library](https://docs.python.org/3/library/configparser.html).
Advanced features such as Interpolation are not yet supported.

#### Secrets
There are cases to store keys and other sensitive information in the configuration file.
To ensure security it is important to keep them encrypted in the configuration file.

The key secrets encryption key must be specified using the environment variable
`SUSHUMNA_CONFIG_SECRET`.  The application will automatically decrypted encrypted 
values using the secret.

#### Overriding using Environment Variables

Any configuration key can be overridden using environment variables.  To override
a key, declare an environment variable as `SUSHUMNA_<key-name-in-config>`.  

* The `SUSHUMNA_` prefix ensures there are no conflicts with existing variables.  
* Environment variable declarations are given priority over configuration value declarations.

## Development Guide

### Structuring
Configuration keys should be classified in sections in the configuration file - since
the keys can only be retrieved by section. This ensures logical grouping of 
settings that belong together.

### Naming Conventions

* Allowed character sets: `A-Z`, `0-9`, `_`
    * Use `ALL_CAPS`. May contain `NUMBERS_123`
    * There should be no whitespace. Use `_` character to denote space

* Key name should be self-explanatory
    * `TOKEN` is a bad name - it's too general
    * `KEYCLOAK_ACCCESS_TOKEN` is much better because it is clear and specific

* Keys must be unique throughout the configuration file

    
### Examples 

##### Configuration File

Sample File: `~/test.conf`
```ini
[S3]

S3_BUCKET_NAME = qa-bucket
S3_ACCESS_KEY = enc-data://gAAAAABd34IUABN8OXPBnFCBEqziuG2oblWjKImHoXgnTBKjU2z-HxaDBcf9nnesSO6iQX79w6OnkGlCK4auW-oxTyUI-ctyWw==

[KAFKA]

KAFKA_CLUSTER_ADDRESS = 127.0.0.1
```

##### Environment Variables

###### Mandatory Variables
* `SUSHUMNA_CONFIG` : `~/test.conf`
* `SUSHUMNA_CONFIG_SECRET` : `enc-key://zmwuKYBCN85G_zm9XCQKLPaEXa7HnDI4TJjNs5jed9w\\=`

###### Overriding configuration value
* `SUSHUMNA_KAFKA_CLUSTER_ADDRESS` : `192.168.1.20`

##### Reading Configuration

```python

# Import the configuration class from isha_base
from odoo.addons.isha_base.configuration import Configuration

# Create a configuration for the S3 section
s3_config = Configuration('S3')

# Retrieve a configuration value
bucket_name = s3_config['S3_BUCKET_NAME']

# Value: bucket name = qa-bucket

# The configuration values is automatically decrypted and returned
access_key = s3_config['S3_ACCESS_KEY']

# Value: access_key = Hello world

#######################################

# Create a configuration for the Kafka section
kafka_config = Configuration('KAFKA')

# The environment variables are checked and an override is returned if exists
kafka_cluster_address = kafka_config['KAFKA_CLUSTER_ADDRESS']

```

## Configuration Secret Utility
The utility `./bin/config_secrets.py` is made available for administrator to generate 
keys and secrets to embed in configurations.

#### Usage

The utility supports 3 basic functions:
* Generate Key
* Encrypt
* Decrypt

```bash
$ ./config_secrets.py --help

usage: config_secrets.py [-h] [--data DATA] [--key KEY]
                         {generate-key,encrypt,decrypt}

This is a utility for preparing secrets for the Sushumna Configuration.

positional arguments:
  {generate-key,encrypt,decrypt}
                        Choose the action

optional arguments:
  -h, --help            show this help message and exit
  --data DATA           Data for encryption / decryption
  --key KEY             Key for encryption / decryption

```

#### Generating a key

```bash
$ ./config_secrets.py generate-key

========================= CONFIGURATION SECRETS UTILITY ========================================
Action: generate-key
----------------------------------- RESULT -----------------------------------------------------

enc-key://fw5qru95pN67IA_2SsWKCXQHqLcLseBsvHhT3jMES0s=

================================================================================================
```

#### Encrypting data

```bash
$ ./config_secrets.py encrypt --data "Hello World!" --key enc-key://fw5qru95pN67IA_2SsWKCXQHqLcLseBsvHhT3jMES0s=

========================= CONFIGURATION SECRETS UTILITY ========================================
Action: encrypt
Key: enc-key://fw5qru95pN67IA_2SsWKCXQHqLcLseBsvHhT3jMES0s=
Data: Hello World!
----------------------------------- RESULT -----------------------------------------------------

enc-data://gAAAAABd4QHJ5TC7ZhAvwoRQIhRiTTeM84NNbj7PPmKWnsHBegKeRt85bDroCdoTi5MBHqXCljx8IX_QHC5hvDvRxmuvo_pWLg==

================================================================================================
```

#### Decrypting data

```bash
$ ./config_secrets.py decrypt \
 --data enc-data://gAAAAABd4QHJ5TC7ZhAvwoRQIhRiTTeM84NNbj7PPmKWnsHBegKeRt85bDroCdoTi5MBHqXCljx8IX_QHC5hvDvRxmuvo_pWLg== \
 --key enc-key://fw5qru95pN67IA_2SsWKCXQHqLcLseBsvHhT3jMES0s=

========================= CONFIGURATION SECRETS UTILITY ========================================
Action: decrypt
Key: enc-key://fw5qru95pN67IA_2SsWKCXQHqLcLseBsvHhT3jMES0s=
Data: enc-data://gAAAAABd4QHJ5TC7ZhAvwoRQIhRiTTeM84NNbj7PPmKWnsHBegKeRt85bDroCdoTi5MBHqXCljx8IX_QHC5hvDvRxmuvo_pWLg==
----------------------------------- RESULT -----------------------------------------------------

Hello World!

================================================================================================
```