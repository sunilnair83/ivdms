# Deployment Guide

## Working With Docker

We use `docker-compose` to build and run application images.

### Running Locally

* **Compose file:** `docker-compose.yml`
* **Build:** `docker-compose build`
* **Run:** `docker-compose up`

##### Notes:
* In the `odoo.conf`, ensure `addons-path` does NOT include the path `odoo/addons`
* To avoid permission denied errors, run `sudo chmod -R 777 ~/isha/sushumna/scratch` regularly to ensure that the container can 
modify any newly added files to scratch

### Running In Production
* **Compose file:** `docker-compose.prod.yml`
* **Build:** `docker-compose -f docker-compose.prod.yml build`
* **Run:** `docker-compose -f docker-compose.prod.yml up`


# Setting up `docker-compose`

## Config Files

The Sushumna application requires two configuration files:

Type | Env Variable | Template | Description
--- | --- | --- | ----
Odoo | `ODOO_RC` | `odoo.conf` | Odoo Application Config
Sushumna | `SUSHUMNA_CONFIG` | `sushumna.conf` | Shared configuration for Isha modules

Default configuration paths inside the container:
* `ODOO_RC=/sushumna/conf/odoo.conf`
* `SUSHUMNA_CONFIG=/sushumna/conf/sushumna.conf`

There are multiple ways of providing configuration to the container:
1. Mount a local directory to `/sushumna/conf` that contains the two `.conf` files.
1. Set the value to the two environment variables to any file accessible to the container

#### Configuration Templates
Templates for Odoo Configuration for different environments are available at `~/sushumna/etc/conf/`

### Sushumna Configuration

This is a central shared configuration for all Isha modules.  Each feature
can define a configuration section for their own use.

#### Overriding using Environment Variables

All keys by default can be overridden by declaring an environment variable by
the same name prefixed with 'SUSHUMNA_'.  This is to avoid unintended conflicts.

E.g. Key: S3_BUCKET_NAME  in the config file will become SUSHUMNA_S3_BUCKET_NAME when
declared as an Environment Variable.

This is useful when we want to use a common `sushumna.conf` with little changes at runtime.

#### Config Value Encryption

To keep credentials committed in the repository, the values must be encrypted so
as to protect them from abuse.  Any value in the `sushumna.conf` can have
an encrypted value.

Please refer to [Configuration](Configuration.md) for details.

**Note**: If you are using encrypted values in the configuration, it is essential
that the environment variable `SUSHUMNA_CONFIG_SECRET` is set with the decryption key.

## Volume Mounting

The Odoo Application stores temporary data to file system.  When that directory
is mounted to the host filesystem (instead of a volume), correct permissions must be set
for the container to write to that directory.  Make sure to run:
```bash
chmod 777 <path-for-odoo-data>
````

## Google SSO Configuration

**Note**: To allow only users of Isha Foundation login use @ishafoundation to configure the Google OAuth.

* First go to [Google Console](https://console.developers.google.com).
* Go to APIs & Services -> OAuth consent screen.
* Give Application type as Internal and give a Application name and save(No need to configure any domain links).
* Now go to APIs & Services -> Credentials -> Create credentials -> OAuth client ID.
* Select application type as Web Application.
* Now add the Authorized redirect URIs to point to http://**domain_name**/auth_oauth/signin and click create.
* Once done, copy Client ID and Client Secret and configure in you OAuth screen(In **Debug Mode** goto Settings -> Users & Companies -> OAuth Providers).


## Configuring Workers

**Note**: To enable workers we need to change both odoo.conf and nginx conf

* Follow the [Server Config Insturctions](https://www.odoo.com/documentation/13.0/setup/deploy.html#builtin-server)
 to setup nginx and odoo.conf
* Then follow the [Redirection Fix](https://stackoverflow.com/questions/32712443/why-does-nginx-keep-redirecting-me-to-localhost)
 to add `proxy_set_header Host $http_host;` to the `location /` in nginx config to fix redirection errors
* Then restart nginx
* Then restart odoo
