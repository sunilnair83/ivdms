#!/bin/bash

set -e
if [[ $# -gt 1 ]]; then
    echo "Executing Entrypoint with parameters: " "$@"
fi

# Check for the configurations
if [[ (-z $ODOO_RC) || (! -f $ODOO_RC)  ]]; then
  echo "Invalid Odoo Configuration.  Please set the environment variable 'ODOO_RC' correctly."
fi

if [[ (-z $SUSHUMNA_CONFIG) || (! -f $SUSHUMNA_CONFIG)  ]]; then
  echo "Invalid Sushumna Configuration.  Please set the environment variable 'SUSHUMNA_CONFIG' correctly."
fi


# If fully formed odoo parametes specified, then run the full command ass is
if [[ ( $1 == "odoo" ) && ( $# -gt 1 ) ]]; then
  exec "$@"

# Assume that all other conditions are where flags are passed to Odoo execution
else
  echo "Executing: odoo" "$@"
  exec odoo "$@"

fi

exit 1