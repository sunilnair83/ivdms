
from json import loads, dumps
from collections import OrderedDict

import pandas as pd
import xmltodict
from pandas.io.json import json_normalize
from itertools import chain
import os
import glob

#pd.set_option('display.height',1000)
pd.set_option('display.max_rows',500)
pd.set_option('display.max_columns',500)
pd.set_option('display.width',1000)

#ff_files = ['/home/kirthi/Books.xml','/home/kirthi/Cottages.xml']
#pf_files = ['/home/kirthi/Books.xml','/home/kirthi/Cottages.xml']

##################################################################
#
#
# While running this program, an error about special character will appear.
# To solve this, open each file in vim editor and find/replace the special chacracters using
#   the command %s/&#4;//g      ####   &#4; is the special chacracter that creates problem
# After replacing, use :wn to save the changes and move to net file
#  ref: https://vim.fandom.com/wiki/Search_and_replace
#
##################################################################


ff_files = glob.glob("/opt/vrs/tally_vendors/prod/FF/*.xml")
pf_files = glob.glob("/opt/vrs/tally_vendors/prod/PF/*.xml")
print(ff_files)
print(pf_files)
reqd_columns = ['@NAME','ALTEREDDATE','ALTEREDBY','CREATEDBY','CREATEDDATE', \
                'EMAIL','GSTAPPLICABLE','GSTDETAILS.LIST','GSTREGISTRATIONTYPE','GSTTYPE', \
                'GSTTYPEOFSUPPLY','GUID','INCOMETAXNUMBER','LEDGERMOBILE','LEDGERPHONE', \
                'LEDSTATENAME','PARTYGSTIN','PINCODE','STARTINGFROM','UDF:_UDF_805309430.LIST', \
                'PAYMENTFAVOURING','DEFAULTTRANSCTIONTYPE','PAYMENTDETAILS.LIST','ADDRESS.LIST']

def to_dict(input_ordered_dict):
    return loads(dumps(input_ordered_dict))

def get_recursively(search_dict, field):
    """
    Takes a dict with nested lists and dicts,
    and searches all dicts for a key of the field
    provided.
    """
    fields_found = []

    for key, value in search_dict.items():

        if key == field:
            fields_found.append(value)

        elif isinstance(value, dict):
            results = get_recursively(value, field)
            for result in results:
                fields_found.append(result)

        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    more_results = get_recursively(item, field)
                    for another_result in more_results:
                        fields_found.append(another_result)

    return fields_found

def xml_pipeline(xml_file, char_encoding):
    content = None
    with open(xml_file, "r", encoding=char_encoding) as f:
        content = f.read()
    content.replace(",","/")
#    content = '<data>\n     ' + content.strip() + '\n     ' + '</data>'
    
    dict_content_nested = xmltodict.parse(content)
    
    return to_dict(dict_content_nested)

def process_vendors(converted_dict, finentity, fc):
  boa = get_recursively(converted_dict, 'SVCURRENTCOMPANY')
  print("*****************BOA - {0}".format(boa[0]))
  op = []
  op_exceptions = []


  final_dict= get_recursively(converted_dict, 'REQUESTDATA')
  df = pd.DataFrame()
  for tally_record in final_dict[0]['TALLYMESSAGE']:
      ledger = get_recursively(tally_record, 'LEDGER')
      if ledger:
#        print(ledger[0]['PAYMENTDETAILS.LIST'])
         df = df.append(ledger,ignore_index=True)
#        op.append(tally_record['LEDGER'])
      else:
         print("ledger not found")
#        op_exceptions.append(tally_record['COMPANY'])
#df2 = json_normalize(df_op['PAYMENTDETAILS.LIST'])
  non_reqd_columns = list(set(df.columns) - set(reqd_columns))

  df.drop(non_reqd_columns, inplace=True, axis=1)
  df['BOOKOFACCOUNTS'] = boa[0]
  df['FINANCEENTITY'] = finentity
  df1 = pd.concat([df, df['PAYMENTDETAILS.LIST'].apply(pd.Series)], axis = 1).drop('PAYMENTDETAILS.LIST', axis = 1)
  df2 = pd.concat([df1, df1['ADDRESS.LIST'].apply(pd.Series)], axis = 1).drop('ADDRESS.LIST', axis = 1)

  print(df2.count)
  return df2
  
#  if fc == 0:
#     df2.to_csv('FF.csv', header=True)
#  else:
#     df2.to_csv('FF.csv', mode='a', header=True)
 


file_count = 0
converted_dict = {}
#for f in ff_files:
#  file_data = {}
#  print("*****************Processing file - {0}".format(f))
#  file_data.update(xml_pipeline(f, 'utf-16'))
#  process_vendors(file_data, 'FF', file_count)
#  file_count += 1
result = pd.DataFrame()

for f in ff_files:
  file_data = {}
  print("*****************Processing file - {0}".format(f))
  file_data.update(xml_pipeline(f, 'utf-16'))
  temp = process_vendors(file_data, 'FF', file_count)
  if file_count == 0:
     result = temp.copy()
  else:
     result = pd.concat([result,temp],ignore_index=True)
  file_count += 1

for f in pf_files:
  file_data = {}
  print("*****************Processing file - {0}".format(f))
  file_data.update(xml_pipeline(f, 'utf-16'))
  temp = process_vendors(file_data, 'PF', file_count)
  if file_count == 0:
    result = temp.copy()
  else:
    # print(result.columns == temp.columns)
    list_of_dfs = [result, temp]
    list_of_dicts = [cur_df.T.to_dict().values() for cur_df in list_of_dfs]    
    result = pd.DataFrame(list(chain(*list_of_dicts)))
    # result = pd.concat([result,temp],ignore_index=True)
  file_count += 1


result.to_csv('prod_data.csv', header=True)

