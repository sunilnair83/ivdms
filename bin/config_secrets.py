#!/usr/bin/python

##########################################################################
# This is a utility designed to manage secrets in Sushumna Configuration
#
# Author : Indradeep Biswas
# Date: Nov 28, 2019
##########################################################################

##########################################################################
# The following section is copied from isha_base/crypto.py
##########################################################################

from cryptography.fernet import Fernet
import argparse

"""
This is a simple cryptography utility that can be used to secure strings with keys.

Steps of operation:
    1. Generate key -> Produces a key as a string
    2. Encrypt some plain text with generated key -> produces encrypted text as string
    3. Decrypt some encrypted text with generated key -> produces plain text as string
"""

ENCODING = 'utf-8'
DATA_PREFIX = 'enc-data://'
KEY_PREFIX = 'enc-key://'

TYPE_DATA = 'data'
TYPE_KEY = 'key'


def is_key(key):
    """ Checks if a string is valid crypto key """
    return key.startswith(KEY_PREFIX)


def is_encrypted(data):
    """ Checks if some data item is encrypted """
    return data.startswith(DATA_PREFIX)


def generate_key():
    """
    Generates a random key
    :return: Base 64 encoded key string
    """

    return _pack(Fernet.generate_key(), KEY_PREFIX)


def encrypt(clear_text, key):
    """
    Encrypts some plain text with key
    :param clear_text: data as string
    :param key: crypto key generated using this utility
    :return: encrypted plain_text data as string
    """

    f = Fernet(_unpack(key, TYPE_KEY, KEY_PREFIX))
    encrypted_bytes = f.encrypt(clear_text.encode(ENCODING))
    return _pack(encrypted_bytes, DATA_PREFIX)


def decrypt(encrypted_text, key):
    """
    Decrypts some encrypted data as string with key
    :param encrypted_text: encrypted data as string
    :param key: crypto key generated using this utility
    :return: plain text
    """
    f = Fernet(_unpack(key, TYPE_KEY, KEY_PREFIX))
    return f.decrypt(_unpack(encrypted_text, TYPE_DATA, DATA_PREFIX)).decode(ENCODING)


def _pack(encrypted_bytes, prefix):
    """
    Packs a set of encrypted bytes to a string with prefix
    """
    return prefix + encrypted_bytes.decode(ENCODING)


def _unpack(encrypted_string, crypto_type, prefix):
    """
    Unpacks an encrypted string to byte array by removing the prefix and encoding it to UTF-8 bytes
    """
    if not encrypted_string.startswith(prefix):
        raise ValueError(
            'Invalid crypto ' + crypto_type + ' format. Must follow pattern [' + prefix + '<' + crypto_type + '>]')

    return encrypted_string.replace(prefix, '', 1).encode(ENCODING)

##########################################################################
# The following section provides the CLI for the script
##########################################################################


parser = argparse.ArgumentParser(description='This is a utility for preparing secrets for the Sushumna Configuration.')
parser.add_argument('action', choices=['generate-key', 'encrypt', 'decrypt'], help='Choose the action')
parser.add_argument('--data', type=str, help='Data for encryption / decryption')
parser.add_argument('--key', type=str, help='Key for encryption / decryption')

args = parser.parse_args()

result = 'Missing arguments.  Please use -h or --help flag to learn how to use this utility.'

if args.action == 'generate-key':
    result = generate_key()
elif args.action == 'encrypt':
    if args.data and args.key:
        result = encrypt(args.data, args.key)
elif args.action == 'decrypt':
    if args.data and args.key:
        result = decrypt(args.data, args.key)

print("\n========================= CONFIGURATION SECRETS UTILITY ========================================")
print("Action: " + args.action)
if args.key:
    print("Key: " + args.key)
if args.data:
    print("Data: " + args.data)
print("----------------------------------- RESULT -----------------------------------------------------\n")
print(result)
print("\n================================================================================================\n")
