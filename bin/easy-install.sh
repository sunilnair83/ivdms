#!/bin/bash

################################################################################
# Easy Installer is intended to be a one-stop setup solution for Odoo
# Development.
#
# Author: Indradeep Biswas
# Date: October 31, 2019
################################################################################

# Initialize base values
APP_NAME=sushumna
APP_HOME=~/isha
export SUSHUMNA_ROOT=$APP_HOME/$APP_NAME
SUSHUMNA_GIT=https://bitbucket.org/ishafoundation/sushumna.git
SUSHUMNA_PORT=8069
SUSHUMNA_DB=odoo
ENVIRONMENT=local
SECRET=enc-key://zmwuKYBCN85G_zm9XCQKLPaEXa7HnDI4TJjNs5jed9w=

echo
echo "--------------------------------------------------------------------------"
echo "  Welcome to the Odoo Easy Installer"
echo "--------------------------------------------------------------------------"
echo
echo " This installer will perform the following steps:"
echo
echo "    1. Install applications (Python / Node / Docker / Postgres / etc.)"
echo "    2. Install package dependenceis for Python & Node"
echo "    3. Initialize Odoo services for first use"
echo
echo "--------------------------------------------------------------------------"
echo " The project '$APP_NAME' will be installed at $SUSHUMNA_ROOT"
echo "--------------------------------------------------------------------------"
echo
read -p " I have 'sudo' access (required to continue)? (Y/n): " -r
echo
if [[ $REPLY =~ ^[Nn]$ ]]
then
    echo
    echo " Odoo Easy Installer has quit."
    echo
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi

read -p " Install the PyCharm IDE and other Dev Tools as well? (y/N): " -r
echo
if [[ $REPLY =~ ^[Yy]$ ]];then
  INSTALL_DEVTOOLS=1
fi

#--------------------------------------------------
# Set up GIT if not configured
#--------------------------------------------------

# Install GIT if not installed

if ! [ -x "$(command -v git)" ]; then
  echo -e "\n---- Installing Git ----"
  sudo apt-get install -y git
fi

#--------------------------------------------------
# Get Project from GIT and Run Setup
#--------------------------------------------------

# Create the HOME directory
mkdir -p $APP_HOME && cd $APP_HOME

# Caching GIT credentials for ease
git config --global credential.helper 'cache --timeout 360000'

# Get APP repository from Git
if [ ! -d $SUSHUMNA_ROOT ]; then
  echo -e "\n---- Cloning remote repository ----"
  git clone $SUSHUMNA_GIT $SUSHUMNA_ROOT

elif [ -d $SUSHUMNA_ROOT/.git ]; then
  echo -e "\n---- Updating existing repository ----"
  cd $SUSHUMNA_ROOT
  git pull

elif [ ! -d $SUSHUMNA_ROOT/.git ]; then
  echo "$SUSHUMNA_ROOT already exists. Please delete the directory and try again."
  exit 1
fi

echo
echo "--------------------------------------------------------------------------"
echo "  Installing Applications"
echo "--------------------------------------------------------------------------"
echo
bash $SUSHUMNA_ROOT/bin/install/install-apt-packages.sh

echo
echo "--------------------------------------------------------------------------"
echo "  Initialing PostgreSQL Database"
echo "--------------------------------------------------------------------------"
echo
bash $SUSHUMNA_ROOT/bin/install/initialize-postgres.sh odoo

echo
echo "--------------------------------------------------------------------------"
echo "  Downloding & Installing Odoo"
echo "--------------------------------------------------------------------------"
echo
bash $SUSHUMNA_ROOT/bin/install/install-odoo.sh $SUSHUMNA_ROOT "https://github.com/odoo/odoo/archive/13.0.zip" "odoo-13.0"

echo
echo "--------------------------------------------------------------------------"
echo "  Initializing Odoo For First-Use"
echo "--------------------------------------------------------------------------"
echo

# Create initial Odoo configuration
bash $SUSHUMNA_ROOT/bin/install/initialize-odoo.sh $SUSHUMNA_ROOT $SUSHUMNA_ROOT $ENVIRONMENT $SUSHUMNA_DB

# Change permissions of the bash scripts to make them executable
chmod +x $SUSHUMNA_ROOT/bin/*.sh
chmod +x $SUSHUMNA_ROOT/bin/*/*.sh

# Give all permissions to scratch so that this directory can be modified from within a docker app
chmod 777 -R --silent $SUSHUMNA_ROOT/scratch/

# Create Runner
echo "#!/bin/bash" > $SUSHUMNA_ROOT/run-odoo
echo "$SUSHUMNA_ROOT/bin/run-odoo.sh $SUSHUMNA_ROOT $SUSHUMNA_ROOT $ENVIRONMENT $SUSHUMNA_PORT $SUSHUMNA_DB $SECRET \$@" >> $SUSHUMNA_ROOT/run-odoo
sudo chmod +x $SUSHUMNA_ROOT/run-odoo


if [[ -n $INSTALL_DEVTOOLS ]]; then

  echo
  echo "--------------------------------------------------------------------------"
  echo "  Installing Develpment Applications"


  echo "--------------------------------------------------------------------------"
  echo
  bash $SUSHUMNA_ROOT/bin/install/install-ide.sh
fi

echo
echo "--------------------------------------------------------------------------"
echo "  Odoo Easy Installer has completed successfully."
echo "--------------------------------------------------------------------------"
echo
echo "  * First-Time Git Setup: "
echo
echo "        $SUSHUMNA_ROOT/bin/git-setup.sh"
echo
echo "--------------------------------------------------------------------------"
echo
echo "  * Run Odoo:"
echo "        cd $SUSHUMNA_ROOT"
echo "        ./run-odoo"
echo "    OR"
echo "        $SUSHUMNA_ROOT/run-odoo"
echo
echo "  * Launch in Browser (Odoo Developer Mode):"
echo
echo "        http://localhost:$SUSHUMNA_PORT/web/login?debug=1"
echo
echo "--------------------------------------------------------------------------"
echo