#!/bin/bash

################################################################################
# Easy Installer is intended to be a one-stop setup solution for Odoo
# Development.
#
# Author: Indradeep Biswas
# Date: October 31, 2019
################################################################################

# Initialize base values
APP_NAME=legacy-12
APP_HOME=~/isha
SUSHUMNA_ROOT=~/isha/sushumna
SUSHUMNA_GIT=https://bitbucket.org/ishafoundation/sushumna.git
LEGACY_ROOT=$APP_HOME/$APP_NAME
LEGACY_PORT=8059
LEGACY_DB=odoo12

echo
echo "--------------------------------------------------------------------------"
echo "  Welcome to the Odoo 12.0 Legacy Installer"
echo "--------------------------------------------------------------------------"
echo
echo " PRE-REQUISITE: Please ensure that you have istalled Sushumna."
echo
echo " This installer will install and initialize Odoo 12."
echo
echo "--------------------------------------------------------------------------"
echo " The project '$APP_NAME' will be installed at $LEGACY_ROOT"
echo "--------------------------------------------------------------------------"
echo
read -p " To continue, you will need to have 'sudo' access.  Ready? (Y/n): " -r
echo
if [[ $REPLY =~ ^[Nn]$ ]]
then
    echo
    echo " Odoo Easy Installer has quit."
    echo
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi

#--------------------------------------------------
# Get Project from GIT and Run Setup
#--------------------------------------------------

# Get APP repository from Git
if [ ! -d $SUSHUMNA_ROOT ]; then
  echo "--------------------------------------------------------------------------"
  echo "  **ERROR: SUSHUMNA NOT INSTALLED.**"
  echo "--------------------------------------------------------------------------"
  echo
  echo " Plese install Sushumna using easy-installer to conitnue."
  echo
  echo "--------------------------------------------------------------------------"
  exit 1
fi

# Create basic directories needed for app to function
mkdir -p $LEGACY_ROOT
cd $LEGACY_ROOT
mkdir -p custom_addons third_party_addons scratch bin etc/conf

echo
echo "--------------------------------------------------------------------------"
echo "  Downlod & Installing Odoo"
echo "--------------------------------------------------------------------------"
echo
bash $SUSHUMNA_ROOT/bin/install/install-odoo.sh $LEGACY_ROOT "https://github.com/odoo/odoo/archive/12.0.zip" "odoo-12.0"


echo
echo "--------------------------------------------------------------------------"
echo "  Initializing Odoo For First-Use"
echo "--------------------------------------------------------------------------"
echo

cp -r $SUSHUMNA_ROOT/etc/conf/legacy-12 etc/conf/
cp $SUSHUMNA_ROOT/bin/run-odoo.sh bin/

bash $SUSHUMNA_ROOT/bin/install/initialize-odoo.sh $SUSHUMNA_ROOT $LEGACY_ROOT $APP_NAME $LEGACY_DB


# Change permissions of the bash scripts to make them executable
chmod +x $LEGACY_ROOT/bin/*.sh

# Create Runner

echo "#!/bin/bash" > $LEGACY_ROOT/run-odoo
echo "$LEGACY_ROOT/bin/run-odoo.sh $SUSHUMNA_ROOT $LEGACY_ROOT $APP_NAME $LEGACY_PORT $LEGACY_DB" >> $LEGACY_ROOT/run-odoo

sudo chmod +x $LEGACY_ROOT/run-odoo

echo
echo "--------------------------------------------------------------------------"
echo "  Odoo Easy Installer has completed successfully."
echo "--------------------------------------------------------------------------"
echo
echo "  * Run Odoo:"
echo "        cd $LEGACY_ROOT"
echo "        ./run-odoo"
echo "    OR"
echo "        $LEGACY_ROOT/run-odoo"
echo
echo "  * Launch in Browser (Odoo Developer Mode):"
echo
echo "        http://localhost:$LEGACY_PORT/web/login?debug=1"
echo
echo "--------------------------------------------------------------------------"
echo