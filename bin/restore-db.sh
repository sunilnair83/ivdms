#!/usr/bin/expect -f

set timeout -1;

set DB_HOST [lindex $argv 0];
set DB_PORT [lindex $argv 1];
set DB_USER [lindex $argv 2];
set DB_PASSWORD [lindex $argv 3];
set DB_NAME [lindex $argv 4];
set DB_DUMP_FILENAME [lindex $argv 5];
set SUSHUMNA_DOCKER_COMPOSE [lindex $argv 6];



send_user "\n\n"
send_user "== Creating Database '$DB_NAME' =="
send_user "\n\n"
spawn createdb -h $DB_HOST -p $DB_PORT -U $DB_USER $DB_NAME
expect "Password:" {
  send -- $DB_PASSWORD\r
  expect eof
}

send_user "\n\n"
send_user "== Granting all privileges to '$DB_USER' on '$DB_NAME' =="
send_user "\n\n"
spawn psql -a -e -h $DB_HOST -p $DB_PORT -U $DB_USER -c "grant all privileges on database $DB_NAME to $DB_USER;"
expect "Password for user odoo:" {
  send -- $DB_PASSWORD\r
  expect eof
}

send_user "\n\n"
send_user "== RESTORING '$DB_DUMP_FILENAME' to '$DB_NAME' =="
send_user "\n\n"
spawn pg_restore -v -h $DB_HOST -p $DB_PORT -U $DB_USER -d $DB_NAME $DB_DUMP_FILENAME
expect "Password:" {
  send -- $DB_PASSWORD\r
  expect eof
}

send_user "\n\n"
send_user "==  RESETTING  assets in '$DB_NAME' database =="
send_user "\n\n"
spawn psql -a -e -h $DB_HOST -p $DB_PORT -U $DB_USER -d $DB_NAME -c "delete from ir_attachment where res_model = 'ir.ui.view' and name like '%assets_%';UPDATE ir_attachment set store_fname = '';"
expect "Password for user odoo:" {
  send -- $DB_PASSWORD\r
  expect eof
}
