#!/bin/bash

################################################################################
# Author: Indradeep Biswas
# Date: October 31, 2019
################################################################################
# Configures the projct for it's run:
#     1. Installs Python depenencies in all it's nested modules
#     2. Assigns the necessary permissions to different files in the project
#     3. Creates RUN configuration
################################################################################

APP_ROOT=$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." >/dev/null 2>&1 && pwd )

if [[ ( -n $1 ) && ( -d $1 ) ]]; then
  APP_ROOT=$1
fi

echo
echo "--------------------------------------------------------------------------"
echo "  Installing Python Module dependencies for Addons"
echo "--------------------------------------------------------------------------"
echo "      App Root: $APP_ROOT"
echo "--------------------------------------------------------------------------"
echo

cd $APP_ROOT
source env/bin/activate

# Find all requirements.txt in the addons and install the requirements
find custom_addons/ -name requirements.txt -exec pip3 install -r {} \;
find third_party_addons/ -name requirements.txt -exec pip3 install -r {} \;