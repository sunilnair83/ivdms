#!/bin/bash

################################################################################
# Author: Nanda Kumar Mangati
# Date: Nov 13, 2020
# Note: Adopted from initialize-postgres.sh
################################################################################

DB_USER=$1

echo
echo "--------------------------------------------------------------------------"
echo "  >> Initializing postgres with parameters:"
echo "--------------------------------------------------------------------------"
echo "      User: $DB_USER"
echo "--------------------------------------------------------------------------"
echo

if [[ ( -z $DB_USER ) ]]; then
  echo
  echo "Mandatory parameters missing. Usage:"
  echo
  echo "  bash ${BASH_SOURCE[0]} <odoo-user>"
  echo

  exit 1
fi

# Start Postgres service in case it is not alrady stated
brew services start postgresql@12

tries=0

# Wait for postgres to be ready
until pg_isready -h localhost -p 5432 -U postgres
do
  echo "Waiting for postgres database to be ready on port 5432..."

  ((tries++))
  if [[ "$tries" == '3' ]]; then
    echo
    echo "-------------------------------------------------------------------------------------"
    echo "** Postgres is not running on Port 5432.  Please resolve and restart installation. **"
    echo "-------------------------------------------------------------------------------------"
    echo
    exit 1
  fi
  sleep 3;
done

# Create 'odoo' with password 'odoo' as superuser
sudo -u $USER psql -d postgres -v ON_ERROR_STOP=1 -q <<-EOSQL
      CREATE USER $DB_USER WITH PASSWORD '$DB_USER';
      ALTER USER $DB_USER WITH LOGIN SUPERUSER;
EOSQL

echo "Postgres has been initialized."
