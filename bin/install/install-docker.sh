#!/bin/bash

################################################################################
# Author: Indradeep Biswas
# Date: November 13, 2019
################################################################################


# Packages required to add repositories
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common

# If Docker APT repository is not in list, add it

if ! grep "^deb .*docker.com*" /etc/apt/sources.list /etc/apt/sources.list.d/*; then
  echo -e "\n---- Adding Docker repository ----"
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo add-apt-repository -y "deb [arch=amd64] https://download.docker.com/linux/ubuntu `lsb_release -cs` stable"
fi

echo -e "\n---- Installing Docker and Docker Compose ----"
sudo apt-get install -y docker.io docker-compose


# Ensure docker permissions are set correctly
sudo chmod 666 /var/run/docker.sock
sudo usermod -aG docker $USER

# Start Docker services (if not started)
sudo service docker start


# Pull Postgres docker container
#if [[ -z $(docker images -q postgres:latest) ]]; then
#  echo "Downloading postgres image from Docker Hub"
#  docker pull postgres:latest
#fi
#
#if [[ -z $(docker images -q dadarek/wait-for-dependencies) ]]; then
#  echo "Downloading dadarek/wait-for-dependencies image from Docker Hub"
#  docker pull dadarek/wait-for-dependencies
#fi
