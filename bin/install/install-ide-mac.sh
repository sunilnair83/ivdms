#!/bin/bash

################################################################################
# Author: Nanda Kumar Mangati
# Date: Nov 14, 2020
################################################################################

#--------------------------------------------------
# Install IDE
#--------------------------------------------------

echo
echo "Installing PyCharm..."
echo
brew cask install pycharm-ce-with-anaconda-plugin
echo
echo "Installing Visual Studio Code..."
echo
brew cask install visual-studio-code
echo
echo "Installing PgAdmin4..."
echo
brew cask install PgAdmin4
