#!/bin/bash

################################################################################
# Author: Indradeep Biswas
# Date: October 31, 2019
################################################################################

# If a parameter is passed to the scirpt, assume that it is the correct root
if [[ ( -n $1 ) && ( -d $1 )]]; then
  APP_ROOT=$1
fi

ODOO_ARCHIVE_URL=$2
ODOO_EXTRACTED_FOLDER_NAME=$3

if [[ ( -z $APP_ROOT ) || ( -z $ODOO_ARCHIVE_URL ) || ( -z $ODOO_EXTRACTED_FOLDER_NAME ) ]]; then
  echo
  echo "Usage:"
  echo
  echo "  bash ${BASH_SOURCE[0]} <installation-path> <odoo-archive-url> <odoo-exracted-folder-name>"
  echo

  exit 1
fi

#--------------------------------------------------
# Install Python Dependencies
#--------------------------------------------------

# Navigate to root application directory
cd $APP_ROOT

if [ ! -d "odoo" ]; then

    echo -e "\n---- Downloading Odoo source from Github Repository ----"
    echo

    ODOO_ARCHIVE_PATH=/tmp/odoo

    mkdir -p $ODOO_ARCHIVE_PATH

    ODOO_ARCHIVE_ZIP=$ODOO_ARCHIVE_PATH/$ODOO_EXTRACTED_FOLDER_NAME.zip

    # If the archive doesn't exist, or exists, but is corrupted, downlaod
    if zip -T $ODOO_ARCHIVE_ZIP | grep -q 'zip error'; then

      wget --verbose -O $ODOO_ARCHIVE_ZIP -t 5 -c $ODOO_ARCHIVE_URL
      echo -e "\n---- Download complete ----"
      echo
    else
      echo -e "\n---- Odoo already downloaded. Skipping download. ----"
    fi

    echo -e "\n---- Extracting Odoo from archive ----"
    echo
    # Inflate the contents
    unzip -q $ODOO_ARCHIVE_ZIP -d $APP_ROOT

    # Move it to odoo
    mv $ODOO_EXTRACTED_FOLDER_NAME odoo

fi

# Make Python 3.7 Default for all practical purposes
# This is necessary for the debugger and other tools to work properly with Odoo
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.7 1
sudo update-alternatives  --set python /usr/bin/python3.7

# Create Virtual Environment if it doesn't exist
# shellcheck disable=SC1028
if [[ ( ! -d "env" ) || ( ! -f "env/bin/activate" ) ]]; then
    # Remove virual environment if exists but doesn't containt the necesasary files
    rm -rf env

    echo -e "\n---- Creating Python virutal Environment ----"
    virtualenv -p python3.7 env

else
    echo -e "\n---- Virtual Environment already created. Skipping virtual environment creation. ----"
fi

echo -e "\n---- Installer Python packages ----"

# Activating the newly created virtual environment
source env/bin/activate

# Fix Odoo Requirements for Python 3.7
# Change version of libsass for
sed -i "s/^libsass==0.12.3$/libsass==0.12.3 ; python_version < '3.7' \nlibsass==0.17.0 ; python_version >= '3.7'/g" odoo/requirements.txt

# Getting all required python packages
pip3 install -r odoo/requirements.txt

# Install PIP dependenceis for addons
bash $APP_ROOT/bin/install/install-addon-pip-requirements.sh $APP_ROOT

# Watchdog allows auto-refreshing python changes
pip3 install watchdog


#--------------------------------------------------
# Install Node Dependencies
#--------------------------------------------------

echo -e "\n---- Installing Node packages ----"
sudo npm install -g rtlcss
