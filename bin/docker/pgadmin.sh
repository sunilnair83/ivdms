#!/bin/bash

################################################################################
# Author: Indradeep Biswas
# Date: October 31, 2019
################################################################################

APP_ROOT=$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." >/dev/null 2>&1 && pwd )

if [[ $1 == "start" ]]; then
  # Launch browser with the Odoo start delay
  sleep 5 && echo "Launching ..." &
  sleep 4 && echo "Launching Browser in 1 ...  Email: odoo  |  Password: odoo" &
  sleep 3 && echo "Launching Browser in 2 ...  Email: odoo  |  Password: odoo" &
  sleep 2 && echo "Launching Browser in 3 ...  Email: odoo  |  Password: odoo" &
  sleep 8 && xdg-open http://localhost:5050 &
fi
bash $APP_ROOT/bin/docker/docker-compose-service.sh PgAdmin pgadmin $1

sleep 3

exit 0