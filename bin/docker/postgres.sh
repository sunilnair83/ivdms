#!/bin/bash

################################################################################
# Author: Indradeep Biswas
# Date: October 31, 2019
################################################################################

APP_ROOT=$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." >/dev/null 2>&1 && pwd )

bash $APP_ROOT/bin/docker/docker-compose-service.sh Postgres postgres $1