#!/bin/bash

################################################################################
# This script restores the sushumna application:
# - Restores a Postgres Odoo Database
# - Resets and regenerates assets
#
# Author: Indradeep Biswas & Sutharson Mathialagan
# Date: January 2, 2020
################################################################################

APP_ROOT=$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )

if [[ ( -z $1 ) || ( -z $2 ) || ( -z $3 ) || ( -z $4 ) || ! (-f $4)  || ( -z $5 ) || ! (-f $5)]]; then

  if [[ ! -f $5 ]]; then
    echo
    echo "Database dump is an invalid file. Please retry."
  fi

  echo
  echo "Usage:"
  echo
  echo "  bash ${BASH_SOURCE[0]} <db_host> <db_port> <db_name> <db_dump_file> <docker-compose-file>"
  echo
  echo "  Actions:"
  echo "    db_host             Postgres host"
  echo "    db_port             Postgres port"
  echo "    db_name             New name for the restored database"
  echo "    db_dump_file        Dump of the Sushumna postgres database"
  echo "    docker-compose-file Compose file for the Sushumna Application"
  echo

  exit 1
fi

DB_HOST=$1
DB_PORT=$2
DB_NAME=$3
DB_DUMP_FILENAME=$4
SUSHUMNA_DOCKER_COMPOSE=$5

echo
echo "--------------------------------------------------------------------------"
echo "  Welcome to the Sushumna Restore Utility"
echo "--------------------------------------------------------------------------"
echo
echo " The RESTORE utility will:"
echo
echo "    1. Drop existing database: $DB_NAME"
echo "    2. Restore a Sushumna database dump: $DB_DUMP_FILENAME as $DB_NAME"
echo "    3. Reset ir_attachments in $DB_NAME"
echo "    4. Update Odoo aplication with all installed apps to rebuild assets"
echo
echo "NOTE: Please udate the odoo.conf to set 'db_name=$DB_NAME'"
echo
echo "--------------------------------------------------------------------------"
echo
read -p " Proceed with RESTORE? You may need 'sudo' access (Y/n): " -r
echo

if [[ $REPLY =~ ^[Nn]$ ]];then


    echo
    echo " Sushumna RESTORE has quit."
    echo
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell

fi

# Install Expect for password injection
if ! [[ -x "$(command -v expect)" ]]; then
  sudo apt install -y expect
fi

# Ensure that we can connct to the postgres instance
if ! pg_isready -h localhost -p 5432 -U postgres | grep "accepting connections"; then
  echo
  echo "Cannot reach database.  Please verify the parameters and try again."
  exit 1
fi


echo
echo "--------------------------------------------------------------------------"
echo "  Provide Database Credentails for '$DB_NAME' "
echo "--------------------------------------------------------------------------"
echo
read -p ' Database User: ' DB_USER
echo
read -s -p ' Database Password: ' DB_PASSWORD
echo
echo

echo
echo "--------------------------------------------------------------------------"
echo "  Checking for existing database: '$DB_NAME' "
echo "--------------------------------------------------------------------------"
echo

if psql -lqt -h $DB_HOST -p $DB_PORT -U $DB_USER | cut -d \| -f 1 | grep -wq $DB_NAME; then
    echo
    echo " Database exists.  Plaese drop the database and re-run restore."
    echo
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi

echo "--------------------------------------------------------------------------"
echo "  RESTORING '$DB_NAME' from $DB_DUMP_FILENAME"
echo "--------------------------------------------------------------------------"
echo

# Invoke the databse actions (with password injection using expect)
$APP_ROOT/bin/restore-db.sh $DB_HOST $DB_PORT $DB_USER $DB_PASSWORD $DB_NAME $DB_DUMP_FILENAME $SUSHUMNA_DOCKER_COMPOSE

echo
echo

# Temporarily modify the compose file to update all modules
cp $SUSHUMNA_DOCKER_COMPOSE $SUSHUMNA_DOCKER_COMPOSE.tmp
sed -i 's/entrypoint:.*/entrypoint: \/sushumna\/entrypoint.sh -u all --stop-after-init/' $SUSHUMNA_DOCKER_COMPOSE.tmp

# Run docker compose with the upgrade command
docker-compose -f $SUSHUMNA_DOCKER_COMPOSE.tmp up

rm -f $SUSHUMNA_DOCKER_COMPOSE.tmp

echo "--------------------------------------------------------------------------"
echo "  Sushumna RESTORE is complete."
echo "--------------------------------------------------------------------------"
