#!/bin/bash

################################################################################
# This script resets the sushumna application:
# - Drop Odoo Database
# - Re-initialize Odoo
#
# Author: Indradeep Biswas
# Date: December 7, 2019
################################################################################

echo
echo "--------------------------------------------------------------------------"
echo "  Welcome to the Sushumna RESET Utility"
echo "--------------------------------------------------------------------------"
echo
echo " The RESET utility will:"
echo
echo "    1. Drop database 'odoo'"
echo "    2. Get the latest master (this will stash unsaved changes)"
echo "    3. Re-initialize Sushumna Odoo application with base apps"
echo
echo "--------------------------------------------------------------------------"
echo
read -p " Proceed with RESET? ('sudo' access required to continue)? (y/N): " -r
echo
if [[ $REPLY =~ ^[Yy]$ ]];then

    echo
    echo " Dropping Database..."
    echo

    APP_NAME=sushumna
    APP_HOME=~/isha
    export SUSHUMNA_ROOT=$APP_HOME/$APP_NAME
    SUSHUMNA_DB=odoo
    ENVIRONMENT=local
    SECRET=enc-key://zmwuKYBCN85G_zm9XCQKLPaEXa7HnDI4TJjNs5jed9w=

    echo
    echo "--------------------------------------------------------------------------"
    echo "  Resetting '$SUSHUMNA_DB' Database"
    echo "--------------------------------------------------------------------------"
    echo

    sudo -u postgres psql -c "DROP DATABASE $SUSHUMNA_DB"

    echo
    echo "--------------------------------------------------------------------------"
    echo "  Getting the latest 'master' branch"
    echo "--------------------------------------------------------------------------"
    echo

    echo " Stashing uncommitted changes... "
    echo " Please use 'git stash pop' command to retrieved unsaved changes."
    echo
    git stash
    echo
    echo " Checking out master... "
    git checkout -f master
    echo
    echo " Pulling lastest changes... "
    git pull


    # Create initial Odoo configuration
    bash $SUSHUMNA_ROOT/bin/install/initialize-odoo.sh $SUSHUMNA_ROOT $SUSHUMNA_ROOT $ENVIRONMENT $SUSHUMNA_DB

    echo "--------------------------------------------------------------------------"
    echo "  Sushumna RESET is complete."
    echo "--------------------------------------------------------------------------"

else
    echo "--------------------------------------------------------------------------"
    echo "  Sushumna RESET Utility has exited."
    echo "--------------------------------------------------------------------------"
fi
echo